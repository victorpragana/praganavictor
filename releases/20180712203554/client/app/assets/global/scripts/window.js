/***
Helper Class for window based events.
***/
var Window = function() {
  return {
    confirmationMessage: "Todos os dados serão perdidos. Deseja sair sem salvar?",

    hasUnsavedData: function() {
      return $('form.ng-dirty').length > 0;
    },

    verifyUnsavedData: function() {
      window.onbeforeunload = function (e) {
        if (Window.hasUnsavedData()) {
          var message = Window.confirmationMessage,

          e = e || window.event;

          if (e) { // For IE and Firefox
            e.returnValue = message;
          }

          return message;// For Safari
        }
      }
    }
  };
}();
