'use strict';

angular.module('lussApp')
  .directive('kitContentPopover', function () {
    return {
      templateUrl: '/views/directives/kit_content_popover.html',
      restrict: 'E',
      scope: {
        component: "="
      },
      link: function(scope) {
        var self = scope;

        self.content = self.component.children.reduce(function(html, child) {
          return html + child.component.name + "<br>";
        }, "");

        self.init = function() {
          angular.element(".popovers").popover();
        };

        self.init();
      }
    };
  });