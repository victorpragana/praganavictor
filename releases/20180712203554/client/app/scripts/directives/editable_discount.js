'use strict';

angular.module('xeditable')
  .directive('editableDiscount', ['editableDirectiveFactory', '$compile', '$timeout', function(editableDirectiveFactory, $compile, $timeout) {
    return editableDirectiveFactory({
      directiveName: 'editableDiscount',
      inputTpl: '<input class="form-control input-xsmall" placeholder="Informe o desconto" disabled>',
      render: function() {
        var self = this;

        this.parent.render.call(this);

        this.inputEl.wrap('<div class="input-group"></div>');

        var elementHtml = '<span class="input-group-addon"><input type="radio" name="radioDiscount" value="percentage">%</span>';
        var element = $compile(elementHtml)(this.scope);

        this.inputEl.parent().prepend(element);

        elementHtml = '<span class="input-group-addon"><input type="radio" name="radioDiscount" value="value">$</span>';
        element = $compile(elementHtml)(this.scope);

        this.inputEl.parent().prepend(element);

        this.inputEl.parent().find('input[type=radio]').bind('change', function(e) {
          var firstTime = false;

          if (self.inputEl.is(':disabled')) {
            self.inputEl.removeAttr('disabled');
            firstTime = true;
          }

          var value = e.currentTarget.value;

          if (value === 'value') {
            self.scope.$type = 'value';

            angular.element(self.inputEl).inputmask("decimal", {
              radixPoint: ',',
              digits: 2,
              rightAlign: false,
              autoUnmask: true,
              unmaskAsNumber: true,
              allowMinus: false,
              min: 0,
              max: self.scope.$eval(self.attrs.eTotal)
            });

            var discountPercentage = self.scope.$data;

            if (discountPercentage !== '' && discountPercentage !== 0) {
              self.scope.$apply(function() {
                var totalValue = self.scope.$eval(self.attrs.eTotal);

                self.scope.$data = parseFloat(((totalValue * discountPercentage) / 100).toFixed(2));
              });
            }
          } else if (value === 'percentage') {
            self.scope.$type = 'percentage';

            angular.element(self.inputEl).inputmask("decimal", {
              radixPoint: ',',
              digits: 2,
              rightAlign: false,
              autoUnmask: true,
              unmaskAsNumber: true,
              allowMinus: false,
              min: 0,
              max: 100
            });

            var discountValue = self.scope.$data;

            if (discountValue === null) {
              discountValue = self.scope.$eval(self.attrs.eSuggested || 0);
            }

            if (discountValue !== '' && discountValue !== 0) {
              self.scope.$apply(function() {
                if (firstTime === false) {
                  var totalValue = self.scope.$eval(self.attrs.eTotal);

                  self.scope.$data = parseFloat(((discountValue * 100) / totalValue).toFixed(2));
                } else {
                  self.scope.$data = discountValue;
                }
              });
            }
          }
        });

        $timeout(function() {
          self.inputEl.parent().find('input[type=radio][value=percentage]').attr('checked', true).trigger('change');
        });
      }
    });
  }]);