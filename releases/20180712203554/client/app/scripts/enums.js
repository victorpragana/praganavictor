'use strict';

angular
  .module("lussApp")
  .constant('Enums', {
    ControlMethod: {
      findById: function(id) {
        return this.enums.find(function(element) {
          return element.id === id;
        });
      },
      findByEnumeration: function(enumeration) {
        return this.enums.find(function(element) {
          return element.enumeration === enumeration;
        });
      },

      WIDTH:      { id: 0, enumeration: 'width', name: 'Largura', color: 'bg-blue' },
      HEIGHT:     { id: 1, enumeration: 'height', name: 'Altura', color: 'bg-green' },
      AREA:       { id: 2, enumeration: 'area', name: 'Área', color: 'bg-red' },
      UNIT:       { id: 3, enumeration: 'unit', name: 'Unidade', color: 'bg-yellow' },
      TOTAL_AREA: { id: 4, enumeration: 'total_area', name: 'Área total', color: 'bg-purple' },

      init: function() {
        this.enums = [this.WIDTH, this.HEIGHT, this.AREA, this.UNIT, this.TOTAL_AREA];

        return this;
      }
    }.init(),

    CalculationMethod: {
      findById: function(id) {
        return this.enums.find(function(element) {
          return element.id === id;
        });
      },

      WIDTH:                { id: 0, enum: 'width', name: 'Largura', color: 'bg-blue', variable: '_LARGURA_' },
      HEIGHT:               { id: 1, enum: 'height', name: 'Altura', color: 'bg-green', variable: '_ALTURA_' },
      AREA:                 { id: 2, enum: 'area', name: 'Área', color: 'bg-red', variable: '_AREA_' },
      BASE_COST:            { id: 3, enum: 'base_cost', name: 'Custo Base', color: 'bg-yellow', variable: '_CUSTO_BASE_' },
      SALE_PRICE:           { id: 4, enum: 'sale_price', name: 'Preço Venda', color: 'bg-purple' },
      WIDTH_CONSUMPTION:    { id: 5, enum: 'width_consumption', name: 'Cons Larg', color: 'bg-purple', variable: '_CONSUMO_LARGURA_' },
      HEIGHT_CONSUMPTION:   { id: 6, enum: 'height_consumption', name: 'Cons Alt', color: 'bg-purple', variable: '_CONSUMO_ALTURA_' },
      QUANTITY_CONSUMPTION: { id: 7, enum: 'quantity_consumption', name: 'Cons Qtd', color: 'bg-purple', variable: '_CONSUMO_QUANTIDADE_' },
      USER_INFORMED:        { id: 8, enum: 'user_informed', name: 'Informado', color: 'bg-red-thunderbird', variable: '_INFORMADO_' },
      FIX_COST:             { id: 9, enum: 'fix_cost', name: 'Custo Fixo', color: 'bg-blue-chambray', variable: '_CUSTO_FIXO_' },

      init: function() {
        this.enums = [this.WIDTH, this.HEIGHT, this.AREA, this.UNIT, this.BASE_COST, this.SALE_PRICE, this.WIDTH_CONSUMPTION, this.HEIGHT_CONSUMPTION, this.QUANTITY_CONSUMPTION, this.USER_INFORMED, this.FIX_COST];

        this.forTotalConsumption = [this.WIDTH, this.HEIGHT, this.AREA, this.WIDTH_CONSUMPTION, this.HEIGHT_CONSUMPTION, this.QUANTITY_CONSUMPTION, this.USER_INFORMED];
        this.forCost = [this.WIDTH, this.HEIGHT, this.AREA, this.BASE_COST, this.FIX_COST, this.QUANTITY_CONSUMPTION];
        this.forHeightConsumption = [this.WIDTH, this.HEIGHT, this.AREA];
        this.forWidthConsumption = [this.WIDTH, this.HEIGHT, this.AREA];
        this.forQuantityConsumption = [this.WIDTH, this.HEIGHT, this.AREA];

        return this;
      }
    }.init(),

    Unit: {
      findById: function(id) {
        return this.enums.find(function(element) {
          return element.id === id;
        });
      },
      findByEnumeration: function(enumeration) {
        return this.enums.find(function(element) {
          return element.enumeration === enumeration;
        });
      },

      METERS:       { id: 0, enumeration: 'm', name: 'Metro', color: 'bg-blue', unit: 'm' },
      MILLIMETERS:  { id: 1, enumeration: 'mm', name: 'Milímetros', color: 'bg-green', unit: 'mm' },
      UNIT:         { id: 2, enumeration: 'un', name: 'Unidade', color: 'bg-red', unit: 'UN' },
      SQUAREMETERS: { id: 3, enumeration: 'm2', name: 'Metros quadrados', color: 'bg-yellow', unit: 'm²' },
      CENTIMETERS:  { id: 4, enumeration: 'cm', name: 'Centímetros', color: 'bg-purple', unit: 'cm' },
      KILOMETERS:   { id: 5, enumeration: 'km', name: 'Quilômetro', color: 'bg-blue-chambray', unit: 'km' },
      PLEAT:        { id: 6, enumeration: 'pleat', name: 'Pleat', color: 'bg-green-meadow', unit: 'pleat' },

      init: function() {
        this.enums = [this.KILOMETERS, this.METERS, this.CENTIMETERS, this.MILLIMETERS, this.SQUAREMETERS, this.UNIT, this.PLEAT];

        return this;
      }
    }.init(),

    EnvironmentCategory: {
      findById: function(id) {
        return this.enums.find(function(element) {
          return element.id === id;
        });
      },
      findByEnumeration: function(enumeration) {
        return this.enums.find(function(element) {
          return element.enumeration === enumeration;
        });
      },

      ROOM: { id: 0, enumeration: 'room', name: 'Quarto' },
      KITCHEN: { id: 1, enumeration: 'kitchen', name: 'Cozinha' },
      BALCONY: { id: 2, enumeration: 'balcony', name: 'Varanda' },
      LIVING_ROOM: { id: 3, enumeration: 'living_room', name: 'Sala de estar' },
      DINING_ROOM: { id: 4, enumeration: 'dining_room', name: 'Sala de jantar' },
      HALL: { id: 5, enumeration: 'hall', name: 'Hall' },
      HOME_THEATER: { id: 6, enumeration: 'home_theater', name: 'Home theater' },
      OFFICE: { id: 7, enumeration: 'office', name: 'Escritório' },
      LAVATORY: { id: 8, enumeration: 'lavatory', name: 'Lavabo' },
      LAUNDRY: { id: 9, enumeration: 'laundry', name: 'Lavanderia' },
      OTHER: { id: 10, enumeration: 'other', name: 'Outros' },

      init: function() {
        this.enums = [this.ROOM, this.KITCHEN, this.BALCONY, this.LIVING_ROOM, this.DINING_ROOM, this.HALL, this.HOME_THEATER, this.OFFICE, this.LAVATORY, this.LAUNDRY, this.OTHER];

        return this;
      }
    }.init(),

    LocationCategory: {
      findById: function(id) {
        return this.enums.find(function(element) {
          return element.id === id;
        });
      },
      findByEnumeration: function(enumeration) {
        return this.enums.find(function(element) {
          return element.enumeration === enumeration;
        });
      },

      WALL: { id: 0, enumeration: 'wall', name: 'Parede', color: 'bg-blue' },
      OPENING: { id: 1, enumeration: 'opening', name: 'Abertura', color: 'bg-red-thunderbird' },
      FLOOR: { id: 2, enumeration: 'floor', name: 'Piso', color: 'bg-green-jungle' },

      init: function() {
        this.enums = [this.WALL, this.OPENING, this.FLOOR];

        return this;
      }
    }.init(),

    UserType: {
      findById: function(id) {
        return this.enums.find(function(element) {
          return element.id === id;
        });
      },
      findByEnumeration: function(enumeration) {
        return this.enums.find(function(element) {
          return element.enumeration === enumeration;
        });
      },

      ADMINISTRATOR: { id: "Administrator", name: 'Administrador', color: 'bg-red-thunderbird' },
      MANAGER:       { id: "Manager",       name: 'Gestor',        color: 'bg-green-jungle' },
      SELLER:        { id: "Seller",        name: 'Vendedor',      color: 'bg-yellow' },
      OPERATOR:      { id: "Operator",      name: 'Operador',      color: 'bg-blue' },

      init: function() {
        this.enums = [this.ADMINISTRATOR, this.MANAGER, this.SELLER, this.OPERATOR];
        this.sellers = [this.ADMINISTRATOR, this.MANAGER, this.SELLER];

        return this;
      }
    }.init(),

    WorkplaceType: {
      findById: function(id) {
        return this.enums.find(function(element) {
          return element.id === id;
        });
      },

      FACTORY:     { id: "Factory",     name: 'Fábrica',      color: 'bg-white bg-font-white',  icon: 'fa fa-industry', font: 'font-dark' },
      DISTRIBUTOR: { id: "Distributor", name: 'Distribuidor', color: 'bg-green-jungle bg-font-green-jungle', icon: 'fa fa-building', font: 'font-green-jungle' },
      STORE:       { id: "Store",       name: 'Loja',         color: 'bg-red-thunderbird bg-font-red-thunderbird',  icon: 'icon-handbag',   font: 'font-red-thunderbird' },

      init: function() {
        this.enums = [this.FACTORY, this.DISTRIBUTOR, this.STORE];

        return this;
      }
    }.init(),

    PriceTableType: {
      findById: function(id) {
        return this.enums.find(function(element) {
          return element.id === id;
        });
      },
      findByEnumeration: function(enumeration) {
        return this.enums.find(function(element) {
          return element.enumeration === enumeration;
        });
      },

      FOR_LINES: { id: 0, name: 'Linhas',   color: 'bg-purple-soft' },
      FOR_ITEMS: { id: 1, name: 'Produtos e Serviços', color: 'bg-blue-madison' },

      init: function() {
        this.enums = [this.FOR_LINES, this.FOR_ITEMS];

        return this;
      }
    }.init(),

    ItemType: {
      findById: function(id) {
        return this.enums.find(function(element) {
          return element.id === id;
        });
      },
      findByEnumeration: function(enumeration) {
        return this.enums.find(function(element) {
          return element.enumeration === enumeration;
        });
      },
      PRODUCT: { id: 0, enumeration: 'product', name: 'Produto', color: 'bg-blue' },
      SERVICE: { id: 1, enumeration: 'service', name: 'Serviço', color: 'bg-green' },

      init: function() {
        this.enums = [this.PRODUCT, this.SERVICE];

        return this;
      }
    }.init(),

    OrderStatus: {
      findById: function(id) {
        return this.enums.find(function(element) {
          return element.id === id;
        });
      },
      findByEnumeration: function(enumeration) {
        return this.enums.find(function(element) {
          return element.enumeration === enumeration;
        });
      },

      TO_SEND: { id: 0, enumeration: 'to_send', name: 'Não faturado', color: 'bg-yellow', font: 'bg-font-yellow' },
      SENDING: { id: 1, enumeration: 'sending', name: 'Faturando',    color: 'bg-default', font: 'bg-font-default' },
      SENT:    { id: 2, enumeration: 'sent',    name: 'Faturado',     color: 'bg-green', font: 'bg-font-green' },
      ERROR:   { id: 3, enumeration: 'error',   name: 'Erro no faturamento',     color: 'bg-red', font: 'bg-font-red' },

      init: function() {
        this.enums = [this.TO_SEND, this.SENDING, this.SENT, this.ERROR];

        return this;
      }
    }.init(),

    CST: {
      findById: function(id) {
        return this.enums.find(function(element) {
          return element.id === id;
        });
      },
      findByEnumeration: function(enumeration) {
        return this.enums.find(function(element) {
          return element.enumeration === enumeration;
        });
      },

      NATIONAL:               { id: 0, enumeration: 'national', name: 'Nacional, exceto as indicadas nos códigos 3 a 5' },
      DIRECT_IMPORT:          { id: 1, enumeration: 'direct_import', name: 'Estrangeira - Importação direta, exceto a indicada no código 6' },
      FOREIGN_IM:             { id: 2, enumeration: 'foreign_im', name: 'Estrangeira - Adquirida no mercado interno, exceto a indicada no código 7' },
      NATIONAL_WITH_IC_GT_40: { id: 3, enumeration: 'national_with_ic_gt_40', name: 'Nacional, mercadoria ou bem com Conteúdo de Importação superior a 40% (quarenta por cento)' },
      NATIONAL_OTHER:         { id: 4, enumeration: 'national_other', name: 'Nacional, cuja produção tenha sido feita em conformidade com os processos produtivos básicos de que tratam o Decreto-Lei nº 288/67, e as Leis nº 8.248/91, 8.387/91, 10.176/01 e 11 . 4 8 4 / 0 7' },
      NATIONAL_WITH_IC_LT_40: { id: 5, enumeration: 'national_with_ic_lt_40', name: 'Nacional, mercadoria ou bem com Conteúdo de Importação inferior ou igual a 40% (quarenta por cento)' },
      FOREIGN_DI_CAMEX:       { id: 6, enumeration: 'foreign_di_camex', name: 'Estrangeira - Importação direta, sem similar nacional, constante em lista de Resolução CAMEX' },
      FOREIGN_IM_CAMEX:       { id: 7, enumeration: 'foreign_im_camex', name: 'Estrangeira - Adquirida no mercado interno, sem similar nacional, constante em lista de Resolução CAMEX' },

      init: function() {
        this.enums = [this.NATIONAL, this.DIRECT_IMPORT, this.FOREIGN_IM, this.NATIONAL_WITH_IC_GT_40, this.NATIONAL_OTHER,
          this.NATIONAL_WITH_IC_LT_40, this.FOREIGN_DI_CAMEX, this.FOREIGN_IM_CAMEX];

        return this;
      }
    }.init(),

    EstimateStatus: {
      findById: function(id) {
        return this.enums.find(function(element) {
          return element.id === id;
        });
      },
      findByEnumeration: function(enumeration) {
        return this.enums.find(function(element) {
          return element.enumeration === enumeration;
        });
      },

      FREE:       { id: 0, enumeration: 'free', name: 'Livre', color: 'bg-green', font: 'bg-font-green' },
      ENQUEUED:   { id: 1, enumeration: 'enqueued', name: 'Em espera', color: 'bg-yellow', font: 'bg-font-yellow' },
      PROCESSING: { id: 2, enumeration: 'processing', name: 'Processando', color: 'bg-default', font: 'bg-font-default' },
      ERROR:      { id: 3, enumeration: 'error', name: 'Erro no processamento',     color: 'bg-red', font: 'bg-font-red' },

      init: function() {
        this.enums = [this.FREE, this.ENQUEUED, this.PROCESSING, this.ERROR];

        return this;
      }
    }.init(),

    Weight: {
      findById: function(id) {
        return this.enums.find(function(element) {
          return element.id === id;
        });
      },
      findByEnumeration: function(enumeration) {
        return this.enums.find(function(element) {
          return element.enumeration === enumeration;
        });
      },
      GENERAL: { id: 0, enumeration: 'general', name: 'Geral', color: 'bg-blue' },
      LIGHT:   { id: 1, enumeration: 'light', name: 'Leve', color: 'bg-green' },
      MEDIUM:  { id: 2, enumeration: 'medium', name: 'Médio', color: 'bg-red' },
      HEAVY:   { id: 3, enumeration: 'heavy', name: 'Pesado', color: 'bg-yellow' },

      init: function() {
        this.enums = [this.GENERAL, this.LIGHT, this.MEDIUM, this.HEAVY];

        return this;
      }
    }.init(),

    IuguPaymentStatus: {
      findById: function(id) {
        return this.enums.find(function(element) {
          return element.id === id;
        });
      },
      findByEnumeration: function(enumeration) {
        return this.enums.find(function(element) {
          return element.enumeration === enumeration;
        });
      },
      
      CREATED:           { id:  0,  enumeration: 'created',            name: 'Em aberto',         label_class: 'label bg-dark'},
      APPROVED:          { id:  1,  enumeration: 'approved',           name: 'Aprovado',          label_class: 'label bg-green'},
      CANCELLED:         { id:  2,  enumeration: 'canceled',           name: 'Cancelado',         label_class: 'label bg-red'},

      init: function() {
        this.enums = [this.CREATED, this.APPROVED, this.CANCELLED];

        return this;
      }
    }.init(),

    PaymentMethod: {
      findById: function(id) {
        return this.enums.find(function(element) {
          return element.id === id;
        });
      },
      findByEnumeration: function(enumeration) {
        return this.enums.find(function(element) {
          return element.enumeration === enumeration;
        });
      },
      
      CREDIT_CARD: { id:  0,  enumeration: 'credit_card',  name: 'Cartão de Crédito'},
      BANK_SLIP:   { id:  1,  enumeration: 'bank_slip',    name: 'Boleto Bancário'},
      REVERSAL:    { id:  2,  enumeration: 'reversal',     name: 'Estorno'},

      init: function() {
        this.paymentEnums = [this.CREDIT_CARD, this.BANK_SLIP];
        this.enums = [this.CREDIT_CARD, this.BANK_SLIP, this.REVERSAL];

        return this;
      }
    }.init(),

    QuestionType: {
      findById: function(id) {
        return this.enums.find(function(element) {
          return element.id === id;
        });
      },
      findByEnumeration: function(enumeration) {
        return this.enums.find(function(element) {
          return element.enumeration === enumeration;
        });
      },

      BOOLEAN: { id: 0,  enumeration: 'boolean', name: 'Sim/Não' },
      DROPDOWN: { id: 1, enumeration: 'dropdown', name: 'Seleção' },

      init: function() {
        this.enums = [this.BOOLEAN, this.DROPDOWN];

        return this;
      }
    }.init(),

    Bank: {
      findById: function(id) {
        return this.enums.find(function(element) {
          return element.id === id;
        });
      },
      findByEnumeration: function(enumeration) {
        return this.enums.find(function(element) {
          return element.enumeration === enumeration;
        });
      },

      BRADESCO:  { name: 'Bradesco',                id: 237, image: '/images/bank_logos/bradesco.png'},
      CITIBANK:  { name: 'Citibank',                id: 745, image: '/images/bank_logos/citibank.png'},
      BRASIL:    { name: 'Banco do Brasil',         id:   1, image: '/images/bank_logos/brasil.png'},
      ITAU:      { name: 'Itaú',                    id: 341, image: '/images/bank_logos/itau.png'},
      SAFRA:     { name: 'Safra',                   id: 422, image: '/images/bank_logos/safra.png'},
      BANRISUL:  { name: 'Banrisul',                id:  41, image: '/images/bank_logos/banrisul.png'},
      CAIXA:     { name: 'Caixa Econômica Federal', id: 104, image: '/images/bank_logos/cef.png'},
      HSBC:      { name: 'HSBC',                    id: 399, image: '/images/bank_logos/hsbc.png'},
      SANTANDER: { name: 'Santander',               id:  33, image: '/images/bank_logos/santander.png'},
      SICREDI:   { name: 'Sicredi',                 id: 748, image: '/images/bank_logos/sicredi.png'},
      INTER:     { name: 'Banco Inter',             id:  77, image: '/images/bank_logos/inter.png'},
      SICOOB:    { name: 'Sicoob',                  id: 756, image: '/images/bank_logos/sicoob.png'},
      AGIBANK:   { name: 'Agibank',                 id: 121, image: '/images/bank_logos/agibank.png'},

      init: function() {
        this.enums = [this.BRADESCO, this.CITIBANK, this.BRASIL, this.ITAU, this.SAFRA, this.BANRISUL, this.CAIXA, this.HSBC, this.SANTANDER, this.SICREDI, this.INTER, this.SICOOB, this.AGIBANK];

        return this;
      }
    }.init(),

    OrderProductionStatus: {
      findById: function(id) {
        return this.enums.find(function(element) {
          return element.id === id;
        });
      },
      findByEnumeration: function(enumeration) {
        return this.enums.find(function(element) {
          return element.enumeration === enumeration;
        });
      },

      NOT_STARTED:        { id: 0,  enumeration: 'not_started', name: 'Não iniciada', next: 'send_to_factory', color: 'bg-yellow', font: 'bg-font-yellow' },
      SENT_TO_FACTORY:    { id: 6,  enumeration: 'sent_to_factory', name: 'Enviado para fábrica', next: 'send_to_production', previous: 'cancel_send_to_factory', color: 'bg-yellow', font: 'bg-font-yellow' },
      SENT_TO_PRODUCTION: { id: 1,  enumeration: 'sent_to_production', name: 'Enviado para produção', next: 'produce', previous: 'cancel_send_to_production', color: 'bg-yellow', font: 'bg-font-yellow' },
      IN_PRODUCTION:      { id: 2,  enumeration: 'in_production', name: 'Em produção', next: 'finish', previous: 'cancel_produce', color: 'bg-yellow', font: 'bg-font-yellow' },
      PRODUCED:           { id: 3,  enumeration: 'produced', name: 'Produzido', next: 'dispatch', previous: 'cancel_finish', color: 'bg-blue', font: 'bg-font-blue' },
      DISPATCHED:         { id: 4,  enumeration: 'dispatched', name: 'Despachado', next: 'receive', previous: 'cancel_dispatch', color: 'bg-blue', font: 'bg-font-blue' },
      RECEIVED:           { id: 5,  enumeration: 'received', name: 'Recebido', previous: 'cancel_receive', color: 'bg-green', font: 'bg-font-green' },

      init: function() {
        this.enums = [this.NOT_STARTED, this.SENT_TO_FACTORY, this.SENT_TO_PRODUCTION, this.IN_PRODUCTION, this.PRODUCED, this.DISPATCHED, this.RECEIVED];

        return this;
      }
    }.init(),

    CreditType: {
      findById: function(id) {
        return this.enums.find(function(element) {
          return element.id === id;
        });
      },
      findByEnumeration: function(enumeration) {
        return this.enums.find(function(element) {
          return element.enumeration === enumeration;
        });
      },

      MONEY: { id: 0, enumeration: 'MoneyCredit', name: 'Crédito em conta' },
      PRODUCTION: { id: 1, enumeration: 'ProductionCredit', name: 'Crédito de fabricação' },

      init: function() {
        this.enums = [this.MONEY, this.PRODUCTION];

        return this;
      }
    }.init(),

    CreditSearchType: {
      findById: function(id) {
        return this.enums.find(function(element) {
          return element.id === id;
        });
      },
      findByEnumeration: function(enumeration) {
        return this.enums.find(function(element) {
          return element.enumeration === enumeration;
        });
      },

      CREDIT_EXTRACT: { id: 0, enumeration: 'credit_extract', name: 'Extrato de créditos' },
      CONCEDED_CREDIT: { id: 1, enumeration: 'conceded_credit', name: 'Extrato de créditos concedidos' },

      init: function() {
        this.enums = [this.CREDIT_EXTRACT, this.CONCEDED_CREDIT];

        return this;
      }
    }.init(),

    MovementReason: {
      findById: function(id) {
        return this.enums.find(function(element) {
          return element.id === id;
        });
      },
      findByEnumeration: function(enumeration) {
        return this.enums.find(function(element) {
          return element.enumeration === enumeration;
        });
      },

      CREDIT: { id: 0, enumeration: 'credit', name: 'Crédito avulso' },
      ORDER_PAYMENT: { id: 1, enumeration: 'order_payment', name: 'Pagamento de pedido' },
      SEND_TO_FACTORY: { id: 2, enumeration: 'send_to_factory', name: 'Envio para a fábrica' },
      CANCEL_SEND_TO_FACTORY: { id: 3, enumeration: 'cancel_send_to_factory', name: 'Estorno de envio para a fábrica' },
      ORDER_PRODUCTION: { id: 4, enumeration: 'order_production', name: 'Envio para produção' },
      CANCEL_ORDER_PRODUCTION: { id: 5, enumeration: 'cancel_order_production', name: 'Estorno de envio para produção' },
      ANTECIPATION: { id: 6, enumeration: 'antecipation', name: 'Antecipação de crédito' },
      STORE_CREDIT: { id: 7, enumeration: 'store_credit', name: 'Crédito de loja' },
      STORE_ANTECIPATION: { id: 8, enumeration: 'store_antecipation', name: 'Antecipação de crédito de loja' },

      init: function() {
        this.enums = [this.CREDIT, this.ORDER_PAYMENT, this.SEND_TO_FACTORY, this.CANCEL_SEND_TO_FACTORY, this.ORDER_PRODUCTION, this.CANCEL_ORDER_PRODUCTION, this.ANTECIPATION, this.STORE_CREDIT, this.STORE_ANTECIPATION];

        return this;
      }
    }.init()
  });