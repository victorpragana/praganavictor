'use strict';

angular
  .module("lussApp")
  .factory("CreditModel", function(restmod, $state, RMUtils, lussError, $http) {
    return restmod.model('/api/credits').mix({
      $extend: {
        Model: {
          $antecipate: function(movement_ids) {
            return $http.post(RMUtils.joinUrl(this.$url(), "antecipate"), {
              movement_ids: movement_ids
            });
          }
        }
      },
      $hooks: {
        'after-request-error': function(response) {
          switch(response.status) {
            case 422: {
              lussError.show(response.data.errors);
            } break;
            case 400: {
              lussError.show(response.data.errors);
            } break;
            case 401: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            case 404: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            default: {
              toastr.error("Erro interno do sistema.");
            }
          }
        }
      }
    });
  });
