'use strict';

angular
  .module("lussApp")
  .factory("AttributeModel", function(restmod, $state, RMUtils, lussError) {
    return restmod.model('/api/specifications').mix({
      $extend: {
        Model: {
          forFiltering: function(familyId) {
            return this.$collection({family_id: familyId}, {
              $urlFor: function() {
                return '/api/specifications/for_filtering';
              }
            });
          }
        }
      },
      $hooks: {
        'after-request-error': function(response) {
          switch(response.status) {
            case 422: {
              lussError.show(response.data.errors);
            } break;
            case 400: {
              lussError.show(response.data.errors);
            } break;
            case 401: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            case 404: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            default: {
              toastr.error("Erro interno do sistema.");
            }
          }
        }
      }
    });
  });