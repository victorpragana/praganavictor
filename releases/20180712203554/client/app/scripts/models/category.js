'use strict';

angular
  .module("lussApp")
  .factory("CategoryModel", function(restmod, RMUtils, $http, $state, lussError) {
    return restmod.model('/api/categories').mix({
      $extend: {
        Model: {
          $destroy: function(category_id) {
            return $http.delete(RMUtils.joinUrl(this.$url(), category_id));
          }
        }
      },
      $hooks: {
        'after-request-error': function(response) {
          switch(response.status) {
            case 422: {
              lussError.show(response.data.errors);
            } break;
            case 400: {
              lussError.show(response.data.errors);
            } break;
            case 401: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            case 404: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            default: {
              toastr.error("Erro interno do sistema.");
            }
          }
        }
      }
    });
  });