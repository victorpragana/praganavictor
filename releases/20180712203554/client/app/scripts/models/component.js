'use strict';

angular
  .module("lussApp")
  .factory("ComponentModel", function(restmod, RMUtils, $http, $state, Upload, lussError) {
    return restmod.model('/api/components').mix({
      $extend: {
        Model: {
          $batchDestroy: function(component_ids) {
            return $http.delete(RMUtils.joinUrl(this.$url(), "batch_destroy"), {
              params: {"ids[]": component_ids}
            });
          },
          $export: function() {
            return $http.get(RMUtils.joinUrl(this.$url(), "export"));
          },
          $import: function(file) {
            return Upload.upload({
              url: RMUtils.joinUrl(this.$url(), "upload"),
              data: { file: file }
            });
          }
        },
        Record: {
          $cloneKit: function(componentId) {
            return this.$send({
              method: 'PATCH',
              url: RMUtils.joinUrl(this.$url(), 'clone_kit'),
              data: {"component_id": componentId}
            });
          },
          $batchInsert: function(ruleIds) {
            return this.$send({
              method: 'POST',
              url: RMUtils.joinUrl(this.$url(), 'batch_insert'),
              data: {"rule_ids": ruleIds}
            });
          },
          $reallyDestroy: function() {
            return this.$send({
              method: 'DELETE',
              url: RMUtils.joinUrl(this.$url(), 'really_destroy')
            });
          },
          $activate: function() {
            return this.$send({
              method: 'PATCH',
              url: RMUtils.joinUrl(this.$url(), 'activate')
            });
          }
        }
      },
      $hooks: {
        'after-request-error': function(response) {
          switch(response.status) {
            case 422: {
              lussError.show(response.data.errors);
            } break;
            case 400: {
              lussError.show(response.data.errors);
            } break;
            case 401: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            case 404: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            default: {
              toastr.error("Erro interno do sistema.");
            }
          }
        }
      }
    });
  });