'use strict';

angular
  .module('lussApp')
  .factory('DocumentModel', function(restmod) {
    return restmod.model().mix({
      $config: {
        name: 'document'
      },
      $hooks: {
        'after-request-error': function(response) {
          switch(response.status) {
            case 422: {
              toastr.error(response.data.errors.join(', '));
            } break;
            case 400: {
              toastr.error(response.data.errors.join(', '));
            } break;
            case 401: {
              toastr.i18n_error('views.common.errors.unauthorized');
            } break;
            case 404: {
              toastr.i18n_error('views.common.errors.not_found');
            } break;
            default: {
              toastr.i18n_error('views.common.errors.unknown');
            }
          }
        }
      }
    }).single('/api/document');
  });