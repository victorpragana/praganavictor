'use strict';

angular
  .module("lussApp")
  .factory("QuestionModel", function(restmod, RMUtils, $state, lussError) {
    return restmod.model('/api/questions').mix({
      $extend: {
        Record: {
          $setLines: function(lineIds) {
            return this.$send({
              method: 'POST',
              url: RMUtils.joinUrl(this.$url(), 'set_lines'),
              data: {line_ids: lineIds}
            });
          },
        }
      },
      $hooks: {
        'after-request-error': function(response) {
          switch(response.status) {
            case 422: {
              lussError.show(response.data.errors);
            } break;
            case 400: {
              lussError.show(response.data.errors);
            } break;
            case 401: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            case 404: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            default: {
              toastr.error("Erro interno do sistema.");
            }
          }
        }
      }
    });
  });