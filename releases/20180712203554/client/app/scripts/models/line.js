'use strict';

angular
  .module("lussApp")
  .factory("LineModel", function(restmod, RMUtils, $state, lussError) {
    return restmod.model('/api/lines').mix({
      $setRules: function(rules) {
        return this.$send({
          method: 'POST',
          url: RMUtils.joinUrl(this.$url(), 'set_rules'),
          data: {rules: rules}
        }, function() {
        });
      },
      $extend: {
        Record: {
          $duplicate: function() {
            return this.$send({
              method: 'POST',
              url: RMUtils.joinUrl(this.$url(), "duplicate")
            });
          },
          $rules: function() {
            return this.$send({
              method: 'GET',
              url: RMUtils.joinUrl(this.$url(), "rules")
            });
          },
          $loadCategoryLevels: function() {
            return this.$send({
              method: 'GET',
              url: RMUtils.joinUrl(this.$url(), "category_levels")
            });
          },
          $inactivate: function() {
            return this.$send({
              method: 'PUT',
              url: RMUtils.joinUrl(this.$url(), "inactivate")
            });
          },
          $activate: function() {
            return this.$send({
              method: 'PUT',
              url: RMUtils.joinUrl(this.$url(), "activate")
            });
          },
          $duplicateRule: function(lineRuleId) {
            return this.$send({
              method: 'POST',
              url: RMUtils.joinUrl(this.$url(), "duplicate_rule"),
              data: { line_rule_id: lineRuleId }
            });
          },
          $importRules: function(lineRuleIds) {
            return this.$send({
              method: 'POST',
              url: RMUtils.joinUrl(this.$url(), "import_rules"),
              data: { line_rule_ids: lineRuleIds }
            });
          }
        }
      },
      components: { 
        hasMany: 'ComponentModel'
      },
      $hooks: {
        'after-request-error': function(response) {
          switch(response.status) {
            case 422: {
              lussError.show(response.data.errors);
            } break;
            case 400: {
              lussError.show(response.data.errors);
            } break;
            case 401: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            case 404: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            default: {
              toastr.error("Erro interno do sistema.");
            }
          }
        }
      }
    });
  });