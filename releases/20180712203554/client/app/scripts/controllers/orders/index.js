'use strict';

angular
  .module("lussApp")
  .controller("OrdersIndexCtrl", function(OrderModel, ipCookie, $filter, $timeout, $scope, $compile, $state, $window,  $rootScope, lussError, Enums) {
    var self = this;

    self.$table = null;
    self.orderId = null;
    self.valueLeft = null;

    self.workplaceTypeEnum = Enums.WorkplaceType;
    self.user = $rootScope.user;

    self.init = function() {
      self.$table = $('#table_orders').dataTable({
        language: {
          aria: {
            sortAscending: "clique para ordenar de forma crescente",
            sortDescending: "clique para ordenar de forma decrescente"
          },
          emptyTable: "Não existem dados para serem exibidos",
          info: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
          infoEmpty: "Nenhum registro foi encontrado",
          infoFiltered: "(filtrado de _MAX_ registros no total)",
          lengthMenu: "Mostrar _MENU_ registros",
          search: "Buscar:",
          zeroRecords: "Não foram encontrados resultados",
          processing: "Processando...",
        },

        buttons: [
          { extend: 'excel', className: 'btn yellow btn-outline ' },
          { extend: 'colvis', className: 'btn dark btn-outline', text: "Colunas"}
        ],

        processing: true,
        serverSide: true,
        searchDelay: 800,
        columnDefs: [
          { targets: 0, name: "date", class: "text-center", width: "10%", data: 0, title: "Data"},
          { targets: 1, name: "id", class: "text-center", width: "10%", title: "Pedido", render: function(data, type, row) {
            return "<a href='/orders/" + row[1] + "'>#" + row[1] + "</a>";
          }},
          { targets: 2, data: 2, title: "Cliente" },
          { targets: 3, class: "text-right", width: "15%", title: "Total", searchable: false, orderable: false, render: function(data, type, row) {
            return $filter('currency')(row[3], "", 2);
          }},
          { targets: 4, name: "estimate_id", class: "text-center", width: 20, title: "Orçamento", render: function(data, type, row) {
            if (row[5] === false) {
              return "<a href='/estimates/" + row[4] + "/edit'>#" + row[4] + "</a>";
            } else {
              return "<a href='/estimates/" + row[4] + "/express_edit'>#" + row[4] + "</a>";
            }
          }},
          { targets: 5, class: "text-center", searchable: false, orderable: false, render: function() {
            return "";
          }, createdCell: function(td, cellData, rowData) {
            var scope = $scope.$new();

            scope.orderId = rowData[1];
            scope.valueLeft = rowData[6];
            scope.hasPayments = rowData[7];

            var paidIcon = $compile('<i class="fa fa-credit-card fa-2x font-grey tooltips" data-original-title="Pedido pago"></i>')(scope);
            var paymentButton = $compile('<a href="#" class="tooltips" data-toggle="modal" data-target="#modal_payment" ng-click="ctrl.initPayment(orderId, valueLeft)" data-original-title="Pagar"><i class="fa fa-credit-card fa-2x"></i></a>')(scope);

            angular.element(td).append(scope.hasPayments ? paidIcon : paymentButton);
          }},
        ],

        ajax: {
          url: "/api/orders.json",
          headers: ipCookie("auth_headers"),
          statusCode: {
            401: function() {
              toastr.remove();
              toastr.warning('Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.');
              $state.go('dashboard');
            }
          }
        },

        "order": [
            [1, 'asc']
        ],
        
        "lengthMenu": [
            [5, 10, 15, 20, 300, -1],
            [5, 10, 15, 20, 300, "Todos"] // change per page values here
        ],
        "pageLength": 100,

        "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
      });
    };

    self.initPayment = function(orderId, valueLeft) {
      self.orderId = orderId;
      self.valueLeft = valueLeft;
    };
    self.isFactory = function(){
      return self.user.workplace.type === self.workplaceTypeEnum.FACTORY.id
    }

    self.reloadTable = function() {
      self.$table.api().ajax.reload();
    };

    self.init();
  });