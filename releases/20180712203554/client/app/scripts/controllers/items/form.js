'use strict';

angular
  .module("lussApp")
  .controller("ItemsFormCtrl", function(ItemModel, Enums, ErpModel, $scope, $stateParams, $state) {
    var self = this;

    ItemModel.mix({
      $hooks: {
        'after-create': function(response) {
          if (response.status === 201) {
            toastr.remove();
            toastr.success("Produto/Serviço criado com sucesso!");
            
            $state.go($state.current, {}, {reload: true});

            self.model          = new ItemModel();
            self.selected_state = null;
            self.selected_city  = null;
            self.cities         = [];
          }
        },
        'after-update': function(response) {
          if (response.status === 200) {
            toastr.remove();
            toastr.success("Produto/Serviço atualizado com sucesso!");
            $state.go("items_index");
          }
        },
        'after-destroy': function(response) {
          if (response.status === 204) {
            toastr.remove();
            toastr.success("Produto/Serviço removido com sucesso!");
            $state.go("items_index");
          }
        }
      }
    });

    self.id                = $stateParams.id;
    self.itemTypeEnum      = Enums.ItemType;
    self.categoryLevels    = [];
    self.productCategories = [];
    self.cstEnum           = Enums.CST;

    if (self.id === undefined) {
      self.model = new ItemModel();
    } else {
      self.model = ItemModel.$find(self.id);
    }

    self.init = function() {
      angular.element("input[data-plugin='masked-input-ncm']").inputmask('99.99.99.99', {
        showMaskOnHover: false,
        showMaskOnFocus: false,
        placeholder: ' ',
        rightAlign: false,
        autoUnmask: true
      });
    };

    self.save = function() {
      self.model.$save();
    };

    self.openDestroyModal = function() {
      $("#modal_delete_item").modal();
    };

    self.destroy = function() {
      self.model.$destroy();
    };

    self.productCategoriesByCategoryLevel = function(categoryLevel) {
      return self.productCategories.filter(function(productCategory) {
        return productCategory.level === categoryLevel;
      });
    };

    self.loadModalData = function() {
      if (self.categoryLevels.length === 0) {
        self.categoryLevels = ErpModel.categoryLevels.$search();
      }

      if (self.productCategories.length === 0) {
        self.productCategories = ErpModel.productCategories.$search();
      }
    };

    self.init();
  });
