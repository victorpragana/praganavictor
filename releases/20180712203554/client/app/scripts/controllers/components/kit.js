'use strict';

angular
  .module("lussApp")
  .controller("ComponentKitCtrl", function(ComponentModel, $scope, $stateParams, $state) {
    var self = this;

    self.id = $stateParams.id;
    self.model = null;
    self.components = [];
    self.kitComponents = [];
    self.selected = [];
    self.selectedToRemove = [];
    self.search = "";
    self.addedSearch = "";
    self.kits = [];
    self.cloneKitId = null;

    ComponentModel.mix({
      $hooks: {
        'after-update': function(response) {
          if (response.status === 200) {
            toastr.remove();
            toastr.success("Kit configurado com sucesso!");
            $state.go("components_index");
          }
        }
      }
    });

    self.init = function() {
      ComponentModel.$search({'family_ids[]': self.model.family_ids}).$then(function(data) {
        self.components = data;

        self.initComponents();
      });

      ComponentModel.$search({only_kit: true}).$then(function(data) {
        self.kits = data;
      });
    };

    self.initComponents = function() {
      angular.forEach(self.model.child_ids, function(component_id) {
        var index = self.components.map(function(c) {
          return c.id;
        }).indexOf(component_id);

        if (index !== -1) {
          self.kitComponents.push(angular.copy(self.components[index]));
          self.components.splice(index, 1);
        }
      });
    };

    if (self.id !== undefined) {
      self.model = ComponentModel.$find(self.id).$then(function() {
        if (self.model.child_ids === undefined) {
          self.model.child_ids = [];
        }

        self.init();
      });
    } else {
      $state.go("components_index");
    }

    self.addSelected = function() {
      angular.forEach(self.selected, function(component_id) {
        if (component_id !== null) {
          var index = self.components.map(function(c) {
            return c.id;
          }).indexOf(component_id);

          if (index !== -1) {
            self.model.child_ids.push(component_id);
            self.kitComponents.push(angular.copy(self.components[index]));
            self.components.splice(index, 1);
          }
        }
      });

      self.selected = [];
      self.search = "";
      angular.element("#txtSearch")[0].focus();
    };

    self.removeSelected = function() {
      angular.forEach(self.selectedToRemove, function(component_id) {
        if (component_id !== null) {
          var index = self.kitComponents.map(function(c) {
            return c.id;
          }).indexOf(component_id);

          if (index !== -1) {
            self.components.push(angular.copy(self.kitComponents[index]));
            self.model.child_ids.splice(index, 1);
            self.kitComponents.splice(index, 1);
          }
        }
      });

      self.selectedToRemove = [];
      self.addedSearch = "";
    };

    self.saveKit = function() {
      self.model.$save();
    };

    self.cloneKit = function($item, $model) {
      self.model.$cloneKit($model).$then(function() {
        toastr.remove();
        toastr.success('Kit copiado com sucesso!');

        $state.reload();
      });
    };

    $scope.$watch("model", function() {
      $("input[type=checkbox]").removeAttr('checked');
    });
  });