'use strict';

angular
  .module("lussApp")
  .controller("ComponentFormCtrl", function(ComponentModel, FamilyModel, CutTableModel, $scope, $stateParams, $state) {
    var self = this;

    ComponentModel.mix({
      $hooks: {
        'after-create': function(response) {
          if (response.status === 201) {
            toastr.remove();
            toastr.success("Componente criado com sucesso!");

            $state.go($state.current, {}, {
              reload: true
            });

            self.model = new ComponentModel();
          }
        },
        'after-update': function(response) {
          if (response.status === 200) {
            toastr.remove();
            toastr.success("Componente atualizado com sucesso!");
            $state.go("components_index");
          }
        },
        'after-destroy': function(response) {
          if (response.status === 204) {
            toastr.remove();
            toastr.success("Componente inativado com sucesso!");
            $state.go("components_index");
          }
        }
      }
    });

    self._simulateCost = function() {
      var cost = self.model.cost !== undefined && self.model.cost !== '' && self.model.cost !== null ? self.model.cost : 0.0;
      var exchange = self.model.exchange !== undefined && self.model.exchange !== '' && self.model.exchange !== null ? self.model.exchange : 0.0;
      var loss = self.model.loss !== undefined && self.model.loss !== '' && self.model.loss !== null ? self.model.loss : 0.0;
      var fix_cost = self.model.fix_cost !== undefined && self.model.fix_cost !== '' && self.model.fix_cost !== null ? self.model.fix_cost : 0.0;

      if (exchange === 0) {
        exchange = 1;
      }

      if (loss === 0) {
        loss = 1;
      }

      self.simulated_cost = cost * exchange * loss;
      self.simulated_fix_cost = fix_cost * exchange * loss;
    };

    self.id = $stateParams.id;
    self.showOffsets = false;
    self.showLines = false;
    self.componentName = null;
    self.selectedCategory = new FormData();
    self.cutTables = CutTableModel.$search();
    self.categoryChanged = false;
    self.simulated_cost = 0;
    self.simulated_fix_cost = 0;

    if (self.id === undefined) {
      self.model = new ComponentModel();
    } else {
      self.model = ComponentModel.$find(self.id).$then(function(data) {
        self.componentName = data.name;
        self.previousCategory = self.model.category_id;

        self._simulateCost();
      });

      $scope.$watch(function() {
        return self.model.category_id;
      }, function(newVal) {
        self.categoryChanged = (self.previousCategory !== newVal);
      });
    }

    self.families = FamilyModel.$search();

    self.init = function() {
      angular.element("input[data-plugin='masked-input-decimal']").inputmask("decimal", {
        radixPoint: ',',
        digits: 5,
        rightAlign: false,
        autoUnmask: true,
        unmaskAsNumber: true,
        allowMinus: false
      });

      angular.element("input[data-plugin='masked-input-decimal-negative']").inputmask("decimal", {
        radixPoint: ',',
        digits: 5,
        rightAlign: false,
        autoUnmask: true,
        unmaskAsNumber: true,
        allowMinus: true
      });

      angular.element("input[data-plugin='masked-input']").inputmask("integer", {
        rightAlign: false,
        autoUnmask: true,
        unmaskAsNumber: true,
        allowMinus: false
      });

      angular.element("#component_code").maxlength({
        limitReachedClass: "label label-danger",
      });

      angular.element(".popovers").popover();
    };

    self.saveComponent = function() {
      self.model.$save();
    };

    self.destroy = function(reallyDestroy) {
      if (reallyDestroy === true) {
        self.model.$reallyDestroy().$then(function(response) {
          toastr.remove();
          toastr.success('Componente excluído com sucesso!');

          $state.go("components_index");
        }, function(response) {
          lussError.show(response.$response.data.errors);
        });
      } else {
        self.model.$destroy();
      }
    };

    self.activate = function() {
      self.model.$activate().$then(function(response) {
        toastr.remove();
        toastr.success('Componente ativado com sucesso!');

        $state.go($state.current, {}, {reload: true});
      }, function(response) {
        lussError.show(response.$response.data.errors);
      });
    };

    self.toggleShowOffsets = function() {
      self.showOffsets = !self.showOffsets;
    };

    self.toggleShowLines = function() {
      self.showLines = !self.showLines;
    };

    self.goToKits = function() {
      $state.go("components_kit", {
        id: self.id
      });
    };

    self.init();
  });