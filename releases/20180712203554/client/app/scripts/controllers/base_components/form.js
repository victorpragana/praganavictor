'use strict';

angular
  .module("lussApp")
  .controller("BaseComponentFormCtrl", function(BaseComponentModel, FamilyModel, AttributeModel, CutTableModel, $scope, $stateParams, $state) {
    var self = this;

    self.configuredAttributes = [];
    self.index = 0;
    self.showOffsets = false;
    self.componentName = null;

    BaseComponentModel.mix({
      $hooks: {
        'after-create': function(response) {
          if (response.status === 201) {
            toastr.remove();
            toastr.success("Componente base criado com sucesso!");

            $state.go($state.current, {}, {
              reload: true
            });

            self.model = new BaseComponentModel();
            self.model.values = [];
            self.model.deleted = false;
          }
        },
        'after-update': function(response) {
          if (response.status === 200) {
            toastr.remove();
            toastr.success("Componente base atualizado com sucesso!");
            $state.go("base_components_index");
          }
        },
        'after-destroy': function(response) {
          if (response.status === 204) {
            toastr.remove();
            toastr.success("Componente base inativado com sucesso!");
            $state.go("base_components_index");
          }
        }
      }
    });

    self._simulateCost = function() {
      var cost = self.model.cost !== undefined && self.model.cost !== '' && self.model.cost !== null ? self.model.cost : 0.0;
      var exchange = self.model.exchange !== undefined && self.model.exchange !== '' && self.model.exchange !== null ? self.model.exchange : 0.0;
      var loss = self.model.loss !== undefined && self.model.loss !== '' && self.model.loss !== null ? self.model.loss : 0.0;
      var fix_cost = self.model.fix_cost !== undefined && self.model.fix_cost !== '' && self.model.fix_cost !== null ? self.model.fix_cost : 0.0;

      if (exchange === 0) {
        exchange = 1;
      }

      if (loss === 0) {
        loss = 1;
      }

      self.simulated_cost = cost * exchange * loss;
      self.simulated_fix_cost = fix_cost * exchange * loss;
    };

    self.id = $stateParams.id;
    self.search = null;
    self.selectedCategory = new FormData();
    self.simulated_cost = 0;
    self.simulated_fix_cost = 0;

    self.initNestable = function() {
      $('#value_list').nestable({
        maxDepth: 20
      });
    };

    if (self.id === undefined) {
      self.model = new BaseComponentModel();
      self.model.values = [];
      self.model.deleted = false;

      self.initNestable();
    } else {
      BaseComponentModel.$find(self.id).$then(function(base_component) {
        self.model = base_component;
        self.componentName = base_component.name;

        self._assignIndex(self.model.values);

        self.initNestable();
        self._simulateCost();
      });
    }

    self.families = FamilyModel.$search();
    self.attributes = [];
    self.cutTables = CutTableModel.$search();

    AttributeModel.$search().$then(function(attributes) {
      self.attributes = attributes.map(function(attribute) {
        return {
          id: attribute.id,
          name: attribute.name,
          automatic: attribute.automatic,
          children: attribute.values.map(function(value) {
            return {
              id: value.id,
              value: value.value,
              automatic: attribute.automatic,
              children: []
            };
          })
        };
      });
    });

    self._assignIndex = function(values) {
      angular.forEach(values, function(value) {
        value.index = self.index++;

        self._assignIndex(value.children);
      });
    };

    self.init = function() {
      angular.element("input[data-plugin='masked-input-decimal']").inputmask("decimal", {
        radixPoint: ',',
        digits: 5,
        rightAlign: false,
        autoUnmask: true,
        unmaskAsNumber: true,
        allowMinus: false
      });

      angular.element("input[data-plugin='masked-input-decimal-negative']").inputmask("decimal", {
        radixPoint: ',',
        digits: 5,
        rightAlign: false,
        autoUnmask: true,
        unmaskAsNumber: true,
        allowMinus: true
      });

      angular.element("input[data-plugin='masked-input']").inputmask("integer", {
        rightAlign: false,
        autoUnmask: true,
        unmaskAsNumber: true,
        allowMinus: false
      });

      angular.element("#base_component_code").maxlength({
        limitReachedClass: "label label-danger"
      });

      angular.element(".popovers").popover();
    };

    self.save = function(createChildren) {
      self.model.values = $('#value_list').nestable('serialize');
      self.model.create_children = createChildren;
      self.model.$save();
    };

    self.destroy = function(reallyDestroy) {
      if (reallyDestroy === true) {
        self.model.$reallyDestroy().$then(function(response) {
          toastr.remove();
          toastr.success('Componente base excluído com sucesso!');

          $state.go("base_components_index");
        }, function(response) {
          lussError.show(response.$response.data.errors);
        });
      } else {
        self.model.$destroy();
      }
    };

    self.activate = function(includeChildren) {
      self.model.$activate(includeChildren).$then(function(response) {
        toastr.remove();
        toastr.success('Componente base ativado com sucesso!');

        $state.go($state.current, {}, {reload: true});
      }, function(response) {
        lussError.show(response.$response.data.errors);
      });
    };

    self.addToAttributes = function() {
      angular.forEach(self.configuredAttributes, function(attribute) {
        self.model.values.push({
          id: attribute.id,
          value: attribute.value,
          automatic: attribute.automatic,
          children: [],
          index: self.index++
        });
      });

      self.configuredAttributes = [];
    };

    self.removeValue = function(index) {
      self.model.values = $('#value_list').nestable('serialize');

      var match_object = {
        array: [],
        index: -1
      };

      self._findValue(self.model.values, index, match_object);

      match_object.array.splice(match_object.index, 1);
    };

    self._findValue = function(values, index, match_object) {
      values.forEach(function(value, value_index, array) {
        if (value.index === index) {
          match_object.array = array;
          match_object.index = value_index;
        } else {
          if (value.children.length > 0) {
            self._findValue(value.children, index, match_object);
          }
        }
      });
    };

    self.toggleShowOffsets = function() {
      self.showOffsets = !self.showOffsets;
    };

    self.init();
  });