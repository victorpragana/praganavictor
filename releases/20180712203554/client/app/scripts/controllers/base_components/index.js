'use strict';

angular
  .module("lussApp")
  .controller("BaseComponentIndexCtrl", function(BaseComponentModel, Enums, ipCookie, lussError, $filter, $timeout, $scope, $compile, $location) {
    var self = this;

    self.$table   = null;
    self.errors   = [];
    self.showingDeleted = false;

    self.init = function() {
      self.showingDeleted = $location.search().show_deleted === 'true';

      self.$table = $('#table_base_components').dataTable({
        "language": {
          "aria": {
              "sortAscending": "clique para ordenar de forma crescente",
              "sortDescending": "clique para ordenar de forma decrescente"
          },
          "emptyTable": "Não existem dados para serem exibidos",
          "info": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
          "infoEmpty": "Nenhum registro foi encontrado",
          "infoFiltered": "(filtrado de _MAX_ registros no total)",
          "lengthMenu": "Mostrar _MENU_ registros",
          "search": "Buscar:",
          "zeroRecords": "Não foram encontrados resultados",
          "processing": "Processando...",
        },

        buttons: [
          { extend: 'colvis', className: 'btn dark btn-outline', text: "Colunas"}
        ],

        processing: true,
        serverSide: true,
        searchDelay: 800,
        columnDefs: [
          { targets: 0, name: "id", class: "text-center", width: 20, title: "ID", render: function(data, type, row) {
            return "<a href='/base_components/" + row[0] + "/edit'>" + row[0] + "</a>";
          }},
          { targets: 1, data: 1, width: "10%", title: "Categoria" },
          { targets: 2, data: 2, title: "Nome" },
          { targets: 3, title: "Família", width: "20%", sortable: false, render: function() {
            return "";
          }, createdCell: function(td, cellData, rowData) {
            var scope = $scope.$new();

            scope.families = rowData[3];

            var badge = $compile('<family-list families="families"></family-list>')(scope);

            angular.element(td).append(badge);
          }},
          { targets: 4, class: "text-right", width: 40, title: "Custo", sortable: false, render: function(data, type, row) {
            return $filter('currency')(row[4], "", 4);
          }},
          { targets: 5, class: "text-center", width: 20, title: "UN", sortable: false, render: function(data, type, row) {
            return Enums.Unit.findById(row[5]).unit;
          }}
        ],

        ajax: {
          url: "/api/base_components.json",
          headers: ipCookie("auth_headers"),
          data: {
            show_deleted: self.showingDeleted
          }
        },

        order: [
          [1, 'asc'],
          [2, 'asc']
        ],
        
        lengthMenu: [
          [5, 10, 15, 20, 300, -1],
          [5, 10, 15, 20, 300, "Todos"] // change per page values here
        ],

        pageLength: 100,

        dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
      });
    };

    self.deleteBaseComponent = function(base_component_id) {
      BaseComponentModel.$destroy(base_component_id).error(function(data) {
        lussError.show(data.errors);
      }).success(function() {
        toastr.remove();
        toastr.success("Componente base removido com sucesso!");

        self.$table.api().ajax.reload();
      });
    };

    self.openDeleteModal = function(base_component_id) {
      $("#model_delete_base_component_" + base_component_id).modal();
    };

    self.init();
  });