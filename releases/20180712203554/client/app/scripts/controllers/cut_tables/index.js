'use strict';

angular
  .module("lussApp")
  .controller("CutTableIndexCtrl", function(CutTableModel, ipCookie, $filter) {
    var self = this;

    self.$table   = null;

    self.init = function() {
      self.$table = $('#table_cut_tables').dataTable({
        "language": {
          "aria": {
              "sortAscending": "clique para ordenar de forma crescente",
              "sortDescending": "clique para ordenar de forma decrescente"
          },
          "emptyTable": "Não existem dados para serem exibidos",
          "info": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
          "infoEmpty": "Nenhum registro foi encontrado",
          "infoFiltered": "(filtrado de _MAX_ registros no total)",
          "lengthMenu": "Mostrar _MENU_ registros",
          "search": "Buscar:",
          "zeroRecords": "Não foram encontrados resultados",
          "processing": "Processando...",
        },

        buttons: [
          { extend: 'colvis', className: 'btn dark btn-outline', text: "Colunas"}
        ],

        processing: true,
        serverSide: true,
        searchDelay: 800,
        columnDefs: [
          { targets: 0, name: "id", class: "text-center", width: 20, title: "ID", render: function(data, type, row) {
            return "<a href='/cut_tables/" + row[0] + "/edit'>" + row[0] + "</a>";
          }},
          { targets: 1, data: 1, title: "Nome" },
          { targets: 2, class: "text-right", title: "Tamanho (cm)", render: function(data, type, row) {
            return $filter('currency')(row[2], "", 1);
          }},
        ],

        ajax: {
          url: "/api/cut_tables.json",
          headers: ipCookie("auth_headers"),
          data: {
            show_deleted: self.showingDeleted
          }
        },

        order: [
          [1, 'asc'],
          [2, 'asc']
        ],
        
        lengthMenu: [
            [5, 10, 15, 20, 300, -1],
            [5, 10, 15, 20, 300, "Todos"] // change per page values here
        ],

        pageLength: 100,

        dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
      });
    };

    self.init();
  });