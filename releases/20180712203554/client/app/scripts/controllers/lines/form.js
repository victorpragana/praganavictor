'use strict';

angular
  .module("lussApp")
  .controller("LineFormCtrl", function(LineModel, FamilyModel, CategoryModel, ErpModel, $scope, $stateParams, $state, lussError, Enums, $filter) {
    var self = this;

    LineModel.mix({
      $hooks: {
        'after-create': function(response) {
          if (response.status === 201) {
            toastr.remove();
            toastr.success("Linha criada com sucesso!");
            
            $state.go($state.current, {}, {reload: true});
            
            self.model = new LineModel();
          }
        },
        'after-update': function(response) {
          if (response.status === 200) {
            toastr.remove();
            toastr.success("Linha atualizada com sucesso!");
            
            $state.go($state.current, {}, {reload: true});
          }
        },
        'after-destroy': function(response) {
          if (response.status === 204) {
            toastr.remove();
            toastr.success("Linha excluída com sucesso!");
            $state.go("lines_index");
          }
        }
      }
    });

    self.id = $stateParams.id;
    self.lineName = '';

    if (self.id === undefined) {
      self.model = new LineModel();
    } else {
      self.model = LineModel.$find(self.id).$then(function(data) {
        self.lineName = data.name;
      });
    }

    self.families          = FamilyModel.$search();
    self.search            = null;
    self.categoryLevels    = [];
    self.productCategories = [];
    self.cstEnum           = Enums.CST;
    self.weightEnum        = Enums.Weight;
    self.selectedWeight    = self.weightEnum.GENERAL.id;

    self.init = function() {
      angular.element("input[data-plugin='masked-input-price']").inputmask("decimal", {
        radixPoint: ',',
        digits: 3,
        rightAlign: false,
        autoUnmask: true,
        unmaskAsNumber: true,
        allowMinus: false
      });

      angular.element("input[data-plugin='masked-input-decimal']").inputmask("decimal", {
        radixPoint: ',',
        digits: 2,
        rightAlign: false,
        autoUnmask: true,
        unmaskAsNumber: true,
        allowMinus: false
      });

      angular.element("input[data-plugin='masked-input-ncm']").inputmask('99.99.99.99', {
        showMaskOnHover: false,
        showMaskOnFocus: false,
        placeholder: ' ',
        rightAlign: false,
        autoUnmask: true
      });
    };

    self.saveLine = function() {
      self.model.$save();
    };

    self.calculateCols = function() {
      var dimensionsWeight = $filter('groupBy')(self.model.dimensions, function(dimension) {
        return dimension.weight;
      });

      switch(Object.keys(dimensionsWeight).length) {
        case 0: {
          return 'col-sm-12';
        } break;
        case 1: {
          return 'col-sm-12';
        } break;
        case 2: {
          return 'col-sm-6';
        } break;
        case 3: {
          return 'col-sm-4';
        } break;
        default: {
          return 'col-sm-3';
        } break;
      }
    };

    self.addDimension = function() {
      if (self.selectedWeight == null) {
        toastr.remove();
        toastr.warning('Selecione um peso');
      } else {
        if (self.model.dimensions === undefined) {
          self.model.dimensions = [];
        }

        var dimensionsForWeight = self.model.dimensions.filter(function(dimension) {
          return dimension.weight === self.selectedWeight;
        });

        if (self.model.dimensions === undefined || self.model.dimensions.length === 0) {
          self.model.dimensions = [{
            initial_width:        0,
            final_width:          0,
            initial_height:       0,
            final_height:         0,
            weight:               self.selectedWeight
          }];
        } else {
          if (dimensionsForWeight === undefined || dimensionsForWeight.length === 0) {
            self.model.dimensions.push({
              initial_width:        0,
              final_width:          0,
              initial_height:       0,
              final_height:         0,
              weight:               self.selectedWeight
            });
          } else {
            var last_dimension = dimensionsForWeight.slice(-1)[0];

            self.model.dimensions.push({
              initial_width:        last_dimension.final_width + self.model.horizontal_increment,
              final_width:          last_dimension.final_width + (self.model.horizontal_increment * 2),
              initial_height:       last_dimension.final_height + self.model.vertical_increment,
              final_height:         last_dimension.final_height + (self.model.vertical_increment * 2),
              weight:               self.selectedWeight
            });
          }
        }
      }
    };

    self.deleteLine = function() {
      self.model.$destroy();
    };

    self.openDeleteModal = function() {
      $("#model_delete_line").modal();
    };

    self.duplicate = function() {
      self.model.$duplicate().$then(function(response) {
        toastr.remove();
        toastr.success('Linha duplicada com sucesso!');

        $state.go('lines_edit', {id: response.$response.data.line.id}, {reload: true});
      }, function(response) {
        lussError.show(response.$response.data.errors);
      });
    };

    self.goToRules = function() {
      var hasDimensions = false;

      if (self.model.dimensions !== undefined && self.model.dimensions.length > 0) {
        var index = self.model.dimensions.map(function(dimension) {
          return dimension.id !== undefined && dimension.id !== null;
        }).indexOf(true);

        if (index !== -1) {
          hasDimensions = true;
        }
      }

      if (!hasDimensions) {
        toastr.remove();
        toastr.warning("Ao menos uma dimensão deve ser configurada.");
      } else {
        $state.go("lines_rules", {id: self.id});
      }
    };

    self.productCategoriesByCategoryLevel = function(categoryLevel) {
      return self.productCategories.filter(function(productCategory) {
        return productCategory.level === categoryLevel;
      });
    };

    self.loadModalData = function() {
      if (self.categoryLevels.length === 0) {
        self.categoryLevels = ErpModel.categoryLevels.$search();
      }

      if (self.productCategories.length === 0) {
        self.productCategories = ErpModel.productCategories.$search();
      }
    };

    self.inactivate = function() {
      self.model.$inactivate().$then(function(response) {
        toastr.remove();
        toastr.success('Linha inativada com sucesso!');

        $state.go($state.current, {}, {reload: true});
      }, function(response) {
        lussError.show(response.$response.data.errors);
      });
    };

    self.activate = function() {
      self.model.$activate().$then(function(response) {
        toastr.remove();
        toastr.success('Linha reativada com sucesso!');

        $state.go($state.current, {}, {reload: true});
      }, function(response) {
        lussError.show(response.$response.data.errors);
      });
    };

    self.init();
  });