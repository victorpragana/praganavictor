'use strict';

angular
  .module("lussApp")
  .controller("PriceTableFormCtrl", function(PriceTableModel, LineModel, ItemModel, $scope, $stateParams, $state, Enums) {
    var self = this;

    PriceTableModel.mix({
      $hooks: {
        'after-create': function(response) {
          if (response.status === 201) {
            toastr.remove();
            toastr.success("Tabela de preço criada com sucesso!");

            $state.go($state.current, {}, {reload: true});

            self.model = new PriceTableModel();
          }
        },
        'after-update': function(response) {
          if (response.status === 200) {
            toastr.remove();
            toastr.success("Tabela de preço atualizada com sucesso!");
            $state.go("price_tables_index");
          }
        },
        'after-destroy': function(response) {
          if (response.status === 204) {
            toastr.remove();
            toastr.success("Tabela de preço excluída com sucesso!");
            $state.go("price_tables_index");
          }
        }
      }
    });

    self.id                 = $stateParams.id;
    self.search             = '';
    self.priceToApply       = '';
    self.priceTableTypeEnum = Enums.PriceTableType;
    self.itemTypeEnum       = Enums.ItemType;

    if (self.id === undefined) {
      self.model = new PriceTableModel();
    } else {
      self.model = PriceTableModel.$find(self.id);
    }

    self.init = function() {

    };

    self.save = function() {
      self.model.$save();
    };

    self.openDestroyModal = function() {
      $("#modal_delete_price_table").modal();
    };

    self.destroy = function() {
      self.model.$destroy();
    };

    self.applyToAll = function(type) {
      if (type === self.priceTableTypeEnum.FOR_LINES.id) {
        angular.forEach(self.model.price_table_lines, function(price_table_line) {
          price_table_line.price = self.priceToApply;
        });
      } else if (type === self.priceTableTypeEnum.FOR_ITEMS.id) {
        angular.forEach(self.model.price_table_items, function(price_table_item) {
          price_table_item.price = self.priceToApply;
        });
      }

      self.priceToApply = '';
    };

    self.applyDiscountToAll = function(type) {
      if (type === self.priceTableTypeEnum.FOR_LINES.id) {
        angular.forEach(self.model.price_table_lines, function(price_table_line) {
          price_table_line.maximum_discount = self.discountToApply;
        });
      } else if (type === self.priceTableTypeEnum.FOR_ITEMS.id) {
        angular.forEach(self.model.price_table_items, function(price_table_item) {
          price_table_item.maximum_discount = self.discountToApply;
        });
      }

      self.discountToApply = '';
    };

    self.addPriceVariable = function(variable, pt) {
      if (pt.price === null || pt.price === undefined) {
        pt.price = variable;
      } else {
        pt.price = pt.price + variable;
      }

      var index = self.model.price_table_lines.indexOf(pt);

      angular.element("input[data-index=" + index + "]").focus();
    };

    self.changeSelection = function(selected) {
      self.model.price_table_lines = [];
      self.model.price_table_items = [];

      if (selected === self.priceTableTypeEnum.FOR_LINES.id) {
        LineModel.$search().$then(function(lines) {
          angular.forEach(lines, function(line) {
            self.model.price_table_lines.push({
              line: {id: line.id, name: line.name},
              price: ''
            });
          });
        });
      } else if (selected === self.priceTableTypeEnum.FOR_ITEMS.id) {
        ItemModel.$search().$then(function(items) {
          angular.forEach(items, function(item) {
            self.model.price_table_items.push({
              item: {id: item.id, code: item.code, description: item.description, type: item.type},
              price: ''
            });
          });
        });
      }
    };

    self.init();
  });