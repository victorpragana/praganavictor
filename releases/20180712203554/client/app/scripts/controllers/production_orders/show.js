'use strict';

angular
  .module("lussApp")
  .controller("ProductionOrderShowCtrl", function($scope, $rootScope, $stateParams, $state, OrderModel, Enums, PrinterModel) {
    var self = this;

    self.total = 0;
    self.selected_options = [];
    self.environmentEnum = Enums.EnvironmentCategory;
    self.locationEnum = Enums.LocationCategory;

    self.controlMethodEnum = Enums.ControlMethod;
    self.unitEnum = Enums.Unit;

    self.id = $stateParams.id;
    self.today = new Date();

    self.model = OrderModel.$find(self.id).$then(function(order) {
      self.total = order.total;
    });

    self.init = function() {
      $('.date-picker').datepicker({
        rtl: App.isRTL(),
        orientation: "left",
        format: "dd/mm/yyyy",
        autoclose: true,
        language: 'pt-BR',
        todayBtn: true,
        todayHighlight: true
      }).datepicker('update', new Date());
    };

    self.generateLineLabel = function() {
      PrinterModel.print(function() {
        self.model.$linesLabel();
      });
    };

    self.generateComponentLabel = function() {
      PrinterModel.print(function() {
        self.model.$componentsLabel();
      });
    };

    self.generateCutMap = function() {
      self.model.$cutMap().$then(function(response) {
        window.open("/" + response.$response.data.path);
      });
    };

    self.init();
  });