'use strict';

angular
  .module("lussApp")
  .controller("AppController", function($scope) {
    $scope.$on('$viewContentLoaded', function() {
      App.initComponents(); // init core components
    });
  });