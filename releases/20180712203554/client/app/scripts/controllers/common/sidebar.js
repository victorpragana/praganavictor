'use strict';

angular
  .module("lussApp")
  .controller("SidebarController", function($scope, MenuModel, Enums, $timeout) {
    var self = this;

    self.workplaceEnum = Enums.WorkplaceType;

    self.loadMenu = function() {
      self.menus = MenuModel.$search().$then(function() {
        self.initSidebar();
      });
    };
    
    self.initSidebar = function() {
      $timeout(function() {
        Layout.initSidebar();
        App.initComponents();
      }, 100);
    };
  });