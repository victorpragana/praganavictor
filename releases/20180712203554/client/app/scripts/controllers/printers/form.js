'use strict';

angular
  .module("lussApp")
  .controller("PrinterFormCtrl", function(PrinterModel, $stateParams, $state, $rootScope) {
    var self = this;

    self.printers = [];
    self.currentUser = $rootScope.user;
    self.selectedPrinter = null;

    PrinterModel.mix({
      $hooks: {
        'after-create': function(response) {
          if (response.status === 201) {
            toastr.remove();
            toastr.success("Impressora criada com sucesso!");
            
            $state.go($state.current, {}, {reload: true});

            self.model = new PrinterModel();
          }
        },
        'after-update': function(response) {
          if (response.status === 200) {
            toastr.remove();
            toastr.success("Impressora atualizada com sucesso!");
            $state.go("printers_index");
          }
        },
        'after-destroy': function(response) {
          if (response.status === 204) {
            toastr.remove();
            toastr.success("Impressora removida com sucesso!");
            $state.go("printers_index");
          }
        }
      }
    });

    self.id = $stateParams.id;

    if (self.id === undefined) {
      self.model = new PrinterModel();
    } else {
      self.model = PrinterModel.$find(self.id).$then(function(printer) {
        self.selectedPrinter = {
          real_name: printer.real_name,
          server_name: printer.server_name,
          server_id: printer.server_id
        }
      });
    }

    self.init = function() {
      PrinterModel.subscribe(function(message) {
        const _message = JSON.parse(message);

        if (_message.user_id === self.currentUser.id) {
          angular.forEach(_message.printers, function(printer) {
            self.printers.push({
              real_name: printer,
              server_name: _message.server_name,
              server_id: _message.server_id
            });
          });
        }
      });
      PrinterModel.listRemotePrinters(self.currentUser.id);
    };

    self.save = function() {
      if (self.selectedPrinter === null) {
        toastr.remove();
        toastr.error("Selecione uma impressora");

        return;
      }

      if (self.model.name === null) {
        toastr.remove();
        toastr.error("Informe o nome da impressora");

        return;
      }

      Object.assign(self.model, self.selectedPrinter);

      self.model.$save();
    };

    self.openDestroyModal = function() {
      $("#modal_delete_printer").modal();
    };

    self.destroy = function() {
      self.model.$destroy();
    };

    self.init();
  });
