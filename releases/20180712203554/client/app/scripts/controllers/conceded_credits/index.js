'use strict';

angular
  .module("lussApp")
  .controller("ConcededCreditsIndexCtrl", function(ipCookie, $filter, $scope, $rootScope, $location, Enums, ClientModel) {
    var self = this;

    self.$table = null;
    self.workplaceTypeEnum  = Enums.WorkplaceType;
    self.creditTypeEnum     = Enums.CreditType;
    self.movementReasonEnum = Enums.MovementReason;
    self.user               = $rootScope.user;
    self.clients            = [];
    self.thirdPartyId       = null;
    self.from               = null;
    self.until              = null;
    self.creditType         = self.creditTypeEnum.PRODUCTION.enumeration;
    self.previousBalance    = 0;

    self.init = function() {
      self.thirdPartyId = $location.search().thirdPartyId ? parseInt($location.search().thirdPartyId) : null;
      self.from = $location.search().from ? $location.search().from : null;
      self.until = $location.search().until ? $location.search().until : null;
      self.creditType = $location.search().creditType ? $location.search().creditType : self.creditTypeEnum.PRODUCTION.enumeration;

      self.$table = $('#table_credits').dataTable({
        language: {
          aria: {
            sortAscending: "clique para ordenar de forma crescente",
            sortDescending: "clique para ordenar de forma decrescente"
          },
          emptyTable: "Não existem dados para serem exibidos",
          info: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
          infoEmpty: "Nenhum registro foi encontrado",
          infoFiltered: "(filtrado de _MAX_ registros no total)",
          lengthMenu: "Mostrar _MENU_ registros",
          search: "Buscar:",
          zeroRecords: "Não foram encontrados resultados",
          processing: "Processando...",
        },

        buttons: [
          { extend: 'colvis', className: 'btn dark btn-outline', text: "Colunas"}
        ],

        processing: true,
        serverSide: false,
        searchDelay: 800,
        columnDefs: [
          { targets: 0, data: 'id', title: "", class: "text-center", render: function(data, type, row) {
            if (row.antecipated) {
              var $i = $("<i>").addClass("fa fa-exclamation-circle font-lg text-warning tooltips").attr("data-original-title", "Essa parcela foi antecipada");
              return $i[0].outerHTML;
            } else {
              return '';
            }
          }},
          { targets: 1, data: 'date', title: "Data", class: "text-center" },
          { targets: 2, data: 'owner', title: "Terceiro" },
          { targets: 3, title: "Descrição", render: function(data, type, row) {
            var string = '';

            if (row.payment_method) {
              string = 'Parcela ' + row.payment_method.installment + ' de ' + row.payment_method.installment_number;
              string += '<br><small>' + self.movementReasonEnum.findById(row.reason).name + '</small>';
            } else {
              string = self.movementReasonEnum.findById(row.reason).name;

              if ((row.reason === self.movementReasonEnum.ANTECIPATION.id || row.reason === self.movementReasonEnum.STORE_ANTECIPATION.id) && row.value > 0) {
                string += '<br><small>Estorno para cálculo</small>';
              }
            }

            if (row.order_id) {
              string = string + ' ref. pedido #' + row.order_id;
            }

            return string;
          } },
          { targets: 4, data: 'value', title: "Entrada", class: "text-right", render: function(data, type, row) {
            if (data >= 0) {
              return $filter('currency')(data, "", 2);
            } else {
              return '';
            }
          } },
          { targets: 5, data: 'value', title: "Saída", class: "text-right", render: function(data, type, row) {
            if (data < 0) {
              var span = $('<span>').addClass("text-danger").html($filter('currency')(data, "", 2));
              return span[0].outerHTML;
            } else {
              return '';
            }
          } },
          { targets: 6, data: 'balance', title: "Saldo", class: "text-right", render: function(data, type, row) {
            if (data !== undefined && data !== null) {
              var span = $('<span>').html($filter('currency')(data, "", 2));

              if (data < 0) {
                span.addClass('text-danger');
              }

              return span[0].outerHTML;
            } else {
              return '';
            }
          } }
        ],

        ajax: {
          url: "/api/credits.json",
          headers: ipCookie("auth_headers"),
          data: function(d) {
            d.third_party_id = self.thirdPartyId;
            d.from = self.from;
            d.until = self.until;
            d.credit_type = self.creditType;
            d.type = Enums.CreditSearchType.CONCEDED_CREDIT.enumeration;
          },
          complete: function(data) {
            if (data.responseJSON) {
              self.previousBalance = data.responseJSON.previous_balance;

              $("#previousBalance").html($filter('currency')(self.previousBalance, "", 2));

              if (data.responseJSON.previous_balance < 0) {
                $("#previousBalance").addClass('text-danger');
              } else {
                $("#previousBalance").removeClass('text-danger');
              }

              var lastMovement = data.responseJSON.data.pop();
              var date = new moment().format('DD/MM/YYYY');
              var balance = self.previousBalance;

              if (lastMovement) {
                balance = lastMovement.balance;
                date = lastMovement.date;
              }

              var $tfoot = $('tfoot');
              var $dateCell = $tfoot.find('span').html(self.until || date);

              $("#finalBalance").html($filter('currency')(balance, "", 2));

              $tfoot.find('th[colspan=6]').removeClass('text-center');
            }
          }
        },

        ordering: false,
        
        lengthMenu: [
          [5, 10, 15, 20, 300, -1],
          [5, 10, 15, 20, 300, "Todos"] // change per page values here
        ],

        pageLength: 100,

        dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

        rowCallback: function(row, data, index) {
          if (data.antecipated) {
            $(row).css("background-color", "#FCFDCA");
          }
        },

        drawCallback: function() {
          $("i.tooltips").tooltip();
        }
      });

      self.loadClients();
    };

    self.initDatepicker = function() {
      $('.date-picker').datepicker({
        rtl: App.isRTL(),
        orientation: "left",
        format: "dd/mm/yyyy",
        autoclose: true,
        language: 'pt-BR',
        todayBtn: true,
        todayHighlight: true
      });

      if (self.until) {
        $('#until').datepicker('update', self.until);
      }

      if (self.from) {
        $('#from').datepicker('update', self.from);
      }
    };

    self.loadClients = function() {
      self.clients = ClientModel.$search();
    };

    self.setThirdParty = function($item, $model) {
      self.thirdPartyId = $model;
      
      self.filter();
    };

    self.filter = function() {
      $("form.ng-dirty").removeClass('ng-dirty');

      $location.search('thirdPartyId', self.thirdPartyId);
      $location.search('from', self.from);
      $location.search('until', self.until);
      $location.search('creditType', self.creditType);

      self.reloadTable();
    };

    self.reloadTable = function() {
      self.$table.api().ajax.reload();
    };

    self.setLastDays = function(days) {
      var today = new moment();

      $('#until').datepicker('update', today.toDate());
      $('#from').datepicker('update', today.subtract(days, 'days').toDate());

      self.filter();
    };

    self.init();
  });