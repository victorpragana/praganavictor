Paperclip.interpolates :year do |attachment, style|
  attachment.instance.created_at.year
end

Paperclip.interpolates :month do |attachment, style|
  attachment.instance.created_at.month
end

Paperclip.interpolates :parent_class do |attachment, style|
  if attachment.instance.is_a?(PackingConfiguration)
    if attachment.instance.packable.is_a?(Estimate)
      return Estimate.name.underscore.pluralize
    elsif attachment.instance.packable.is_a?(Order)
      return Order.name.underscore.pluralize
    else
      return attachment.instance.class.name.underscore.pluralize
    end
  else
    attachment.instance.class.name.underscore.pluralize
  end
end

Paperclip::Attachment.default_options.update({
  path: ":rails_root/public:url",
  url: "/system/:parent_class/:year/:month/:filename",
})