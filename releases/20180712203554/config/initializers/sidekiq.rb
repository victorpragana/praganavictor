Sidekiq::Logging.logger.level = Logger::INFO

Sidekiq.configure_client do |config|
  config.redis = { reconnect_attempts: 5 }
end

Sidekiq.configure_server do |config|
  config.redis = { reconnect_attempts: 5 }
end