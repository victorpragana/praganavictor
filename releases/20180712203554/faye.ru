require 'faye'

Faye::WebSocket.load_adapter('thin')

logger = Logger.new('log/faye.log')

use Rack::CommonLogger, logger

bayeux = Faye::RackAdapter.new(mount: '/faye', timeout: 25, ping: 5)

bayeux.on(:publish) do |client_id, channel, data|
  logger.info "Message published! #{client_id}, #{channel}"
end

bayeux.on(:unsubscribe) do |client_id, channel|
  logger.info "Client #{client_id} unsubscribed from channel #{channel}"
end

bayeux.on(:disconnect) do |client_id,|
  logger.info "Client #{client_id} disconnected"
end

run bayeux