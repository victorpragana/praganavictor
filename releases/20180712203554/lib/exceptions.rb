module Exceptions
  class NoPriceTableError < StandardError; end
  class ErpLineCategoriesNotConfiguredError < StandardError; end
  class ErpItemCategoriesNotConfiguredError < StandardError; end
end