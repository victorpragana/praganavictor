namespace :components do
  desc "Regenerate the component names"
  task regenerate_names: :environment do
    errors = []

    Component.user_components.where.not(base_component_id: nil).find_each do |component|
      component_name = nil
      ids = []

      names = component.name.split(' ')

      search_name = "#{names.shift}"

      available_values = names.dup

      last_performed = false

      loop do
        value = component.values.where('lower(value) = ?', search_name.downcase).first

        if !value.nil?
          ids.push(value.id) unless ids.include?(value.id)

          if names.empty?
            last_performed = true
          else
            search_name = "#{names.shift}"
            available_values = names.dup
          end
        else
          if available_values.empty?
            if names.empty?
              last_performed = true
            else
              search_name = "#{names.shift}"
              available_values = names.dup
              next
            end
          else
            search_name = "#{search_name} #{available_values.shift}"
          end
        end

        break if last_performed
      end

      ids.each do |id|
        value = component.values.find(id)

        component_name = "#{component_name} #{value.value}" if !component_name.nil?
        component_name = "#{value.value}"                   if component_name.nil?
      end

      Kernel.puts("#{component.name} ------ #{component_name}")

      component.name = component_name
      if component.save == false
        errors << "Componente #{component.id} (#{component.name}) #{component.errors.full_messages.join(', ')}"
      end
    end

    if !errors.empty?
      Kernel.puts("ERROS: #{errors}")
    end
  end
end
