# == Schema Information
#
# Table name: payments
#
#  id                 :integer          not null, primary key
#  value              :decimal(8, 2)    not null
#  status             :integer          default(0), not null
#  payment_method     :integer          default(0), not null
#  installment_number :integer          default(1), not null
#  bank_slip_url      :string(255)
#  created_at         :datetime
#  updated_at         :datetime
#  iugu_invoice_id    :string
#  tax                :decimal(5, 2)    default(0.0)
#  original_value     :decimal(8, 2)    default(0.0), not null
#

class Payment < ActiveRecord::Base
  validates :payment_method, presence: { allow_blank: false }
  validates :installment_number, presence: { allow_blank: false }, numericality: { only_integer: true, greater_than: 0 }
  validates :value, numericality: { allow_nil: false, greater_than: 0 }
  validates :original_value, numericality: { allow_nil: false, greater_than: 0 }
  validates_numericality_of :tax, greater_than_or_equal_to: 0, less_than_or_equal_to: 100

  has_many :movements

  enum status: {
    created:   0,
    approved:  1,
    cancelled: 2
  }

  enum payment_method: {
    credit_card: 0,
    bank_slip:   1
  }
end
