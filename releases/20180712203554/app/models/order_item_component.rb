# == Schema Information
#
# Table name: order_item_components
#
#  id                        :integer          not null, primary key
#  order_item_id             :integer
#  total_consumption         :decimal(10, 5)   not null
#  unit_value                :decimal(10, 5)   not null
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  component_name            :string(255)      not null
#  unit                      :integer          not null
#  component_characteristics :text
#  category_name             :string(255)      not null
#  width_consumption         :decimal(10, 5)
#  height_consumption        :decimal(10, 5)
#  quantity_consumption      :decimal(10, 5)
#  required                  :boolean          default(FALSE), not null
#  total                     :decimal(10, 2)   default(0.0)
#  component_id              :integer
#

class OrderItemComponent < ActiveRecord::Base
  include HasUnit

  delegate :order, to: :order_item
  
  belongs_to :order_item, required: true
  belongs_to :component, required: true

  validates :width_consumption, numericality: { greater_than_or_equal_to: 0 }, allow_nil: true
  validates :height_consumption, numericality: { greater_than_or_equal_to: 0 }, allow_nil: true
  validates :quantity_consumption, numericality: { greater_than_or_equal_to: 0 }, allow_nil: true
  validates :total_consumption, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :unit_value, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :total, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :component_name, presence: { allow_blank: false }, length: 1..255
  validates :category_name, presence: { allow_blank: false }, length: 1..255
  validates :unit, presence: { allow_blank: false }
  validates :required, inclusion: { in: [true, false] }

  def packing_configuration
    order.packing_configurations.where("#{order_item_id} = ANY(items)").where(component_name: component_name).first
  end
end
