# == Schema Information
#
# Table name: option_components
#
#  id                        :integer          not null, primary key
#  option_id                 :integer
#  total_consumption         :decimal(10, 5)   not null
#  unit_value                :decimal(10, 5)   not null
#  component_name            :string(255)      not null
#  unit                      :integer          not null
#  component_characteristics :text
#  category_name             :string(255)      not null
#  width_consumption         :decimal(10, 5)
#  height_consumption        :decimal(10, 5)
#  quantity_consumption      :decimal(10, 5)
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  required                  :boolean          default(FALSE), not null
#  component_id              :integer
#  rule_id                   :integer
#  total                     :decimal(10, 2)   default(0.0)
#

class OptionComponent < ActiveRecord::Base
  include HasUnit
  
  delegate :estimate, to: :environment
  delegate :environment, to: :location
  delegate :location, to: :option

  belongs_to :option, required: true
  belongs_to :rule
  belongs_to :component
  has_and_belongs_to_many :possible_components, class_name: 'Component'

  validates :width_consumption, numericality: { greater_than_or_equal_to: 0 }, allow_nil: true
  validates :height_consumption, numericality: { greater_than_or_equal_to: 0 }, allow_nil: true
  validates :quantity_consumption, numericality: { greater_than_or_equal_to: 0 }, allow_nil: true
  validates :total_consumption, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :unit_value, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :total, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :component_name, presence: { allow_blank: false }, length: 1..255
  validates :category_name, presence: { allow_blank: false }, length: 1..255
  validates :unit, presence: { allow_blank: false }
  validates :required, inclusion: { in: [true, false] }

  validates :total_consumption, :unit_value, :width_consumption, :height_consumption, :quantity_consumption, :total,
    numericality: { less_than: 100000 }, allow_nil: true

  def packing_configuration
    estimate.packing_configurations.where("#{option_id} = ANY(items)").where(component_name: component_name).first
  end
end
