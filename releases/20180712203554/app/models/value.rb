# == Schema Information
#
# Table name: values
#
#  id               :integer          not null, primary key
#  value            :string           not null
#  specification_id :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  deleted_at       :datetime
#  comparison_value :integer
#

class Value < ActiveRecord::Base
  include HasConversion

  acts_as_paranoid
  
  belongs_to :specification, required: true
  has_and_belongs_to_many :components

  has_many :base_component_values
  has_many :base_components, through: :base_component_values

  validates :value, presence: {allow_blank: false}, uniqueness: {case_sensitive: false, scope: :specification_id}
  validates :comparison_value, presence: {allow_blank: false}, numericality: {only_integer: true, greater_than: 0}, if: :automatic_specification?

  delegate :automatic?, to: :specification

  after_update :regenerate_names, if: Proc.new { |v| v.value_changed? }

  has_conversion_for :comparison_value

  private
  def automatic_specification?
    return !self.specification.blank? && self.specification.automatic?
  end

  def regenerate_names
    self.components.find_each do |component|
      value_ids = component.value_ids

      parent_value = component.base.values.only_parents.find_by(value_id: value_ids)

      component_name = "#{parent_value.value.value}"

      value_ids.delete(parent_value.value.id)

      if parent_value.children.empty?
        update_component_name(component_name, component)
      else
        child_value = parent_value.children.find_by(value_id: value_ids)

        append_component_name(child_value, component_name, component, value_ids)
      end
    end
  end

  def update_component_name(component_name, component)
    component.update(name: component_name)
  end

  def append_component_name(component_value, component_name, component, value_ids)
    name = "#{component_name} #{component_value.value.value}"

    value_ids.delete(component_value.value.id)

    if component_value.children.empty?
      update_component_name(name, component)
    else
      child_value = component_value.children.find_by(value_id: value_ids)
      
      append_component_name(child_value, name, component, value_ids)
    end
  end
end
