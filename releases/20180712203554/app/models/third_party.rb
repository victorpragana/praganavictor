# == Schema Information
#
# Table name: third_parties
#
#  id                     :integer          not null, primary key
#  name                   :string           not null
#  rg                     :string
#  issuing_entity         :string
#  identification         :string
#  zip_code               :string
#  street                 :string
#  number                 :string
#  complement             :string
#  phone                  :string
#  email                  :string
#  corporate_name         :string
#  deleted_at             :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  actable_id             :integer
#  actable_type           :string
#  state_inscription      :string
#  municipal_inscription  :string
#  city_id                :integer
#  neighborhood           :string
#  subdomain              :string
#  logo_file_name         :string
#  logo_content_type      :string
#  logo_file_size         :integer
#  logo_updated_at        :datetime
#  current_account        :string
#  agency                 :string
#  bank                   :integer
#  allow_special_discount :boolean
#  special_discount       :decimal(5, 2)    default(0.0)
#

class ThirdParty < ActiveRecord::Base
  include HasIdentification

  actable
  acts_as_paranoid

  has_attached_file :logo, storage: :fog, fog_directory: "luss-#{Rails.env}", path: ':class/:attachment/:id/:filename', url: ':class/:attachment/:id/:filename',
    default_url: '/images/logo-lg.png',
    fog_credentials: {
      provider: 'Rackspace',
      rackspace_api_key: Rails.application.secrets.rackspace['api_key'],
      rackspace_username: Rails.application.secrets.rackspace['username'],
      rackspace_region: 'IAD'
  }
  validates_attachment_content_type :logo, content_type: %r{\Aimage\/.*\Z}

  enum bank: {
    bradesco: 237,
    citibank: 745,
    brasil:     1,
    itau:     341,
    safra:    422,
    banrisul:  41,
    cef:      104,
    hsbc:     399,
    santander: 33,
    sicredi:  748,
    inter:    77,
    sicoob:   756,
    agibank:  121
  }

  belongs_to :city

  delegate :state, to: :city

  before_validation :remove_unused_information, unless: :is_not_customer?
  before_validation :format_identification, unless: proc { |s| s.identification.blank? }
  before_validation :format_rg, unless: proc { |s| s.rg.blank? }
  before_validation :format_phone, unless: proc { |s| s.phone.blank? }
  before_validation :format_zip_code, unless: proc { |s| s.zip_code.blank? }
  before_validation :remove_special_discount, if: proc { |tp| !tp.allow_special_discount? }

  validates :name, presence: { allow_blank: false }
  validates :identification, presence: { allow_blank: false }, cpf_or_cnpj: true, if: :is_not_customer?
  validates :street, presence: true, allow_blank: false, if: :is_not_customer?
  validates :number, presence: true, allow_blank: false, if: :is_not_customer?
  validates :phone, presence: true, allow_blank: false, if: :is_not_customer?

  validates :subdomain, presence: true, uniqueness: true, if: :is_not_customer?
  validate :validate_subdomain, if: :is_not_customer?

  validates :agency, presence: { allow_blank: false }, if: :banking_information_present?
  validates :current_account, presence: { allow_blank: false }, if: :banking_information_present?
  validates :bank, presence: { allow_blank: false }, if: :banking_information_present?

  validates_numericality_of :special_discount, greater_than_or_equal_to: 0, less_than_or_equal_to: 100, if: proc { |tp| tp.allow_special_discount? }

  def is_not_customer?
    actable_type != 'Customer'
  end

  def validate_subdomain
    errors.add(:subdomain, "não deve conter espaços, caracteres com acento nem pontos. Exemplo: #{subdomain.parameterize}") if subdomain != subdomain&.parameterize
  end

  def has_address?
    !zip_code.blank? && !street.blank? && !number.blank? && !neighborhood.blank?
  end

  private

  def remove_special_discount
    self.special_discount = nil
  end

  def remove_unused_information
    self.agency, self.current_account, self.bank = nil, nil, nil
  end

  def banking_information_present?
    is_not_customer? && (self.agency.present? || self.current_account.present? || self.bank.present?)
  end

  def format_identification
    identification.gsub!(/[^0-9]/, '')
  end

  def format_rg
    rg.gsub!(/[^0-9X]/, '')
  end

  def format_phone
    phone.gsub!(/[^0-9]/, '')
  end

  def format_zip_code
    zip_code.gsub!(/[^0-9]/, '')
  end
end
