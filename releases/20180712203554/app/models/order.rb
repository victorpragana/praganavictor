# == Schema Information
#
# Table name: orders
#
#  id                     :integer          not null, primary key
#  date                   :date             not null
#  observation            :text
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  seller_id              :integer
#  seller_type            :string
#  client_id              :integer
#  client_type            :string
#  discount               :decimal(5, 2)    default(0.0)
#  status                 :integer          default(0), not null
#  erp_id                 :integer
#  processing_messages    :text             default([]), is an Array
#  user_id                :integer
#  billing_client_id      :integer
#  billing_client_type    :string
#  special_discount       :decimal(5, 2)
#  production_status      :integer          default(0), not null
#  sent_to_production_at  :datetime
#  started_production_at  :datetime
#  finished_production_at :datetime
#  dispatched_at          :datetime
#  received_at            :datetime
#  sent_to_factory_at     :datetime
#

class Order < ActiveRecord::Base
  include Orderable
  include Packable
  include AASM

  enum status: {
    to_send:  0,
    sending:  1,
    sent:     2,
    error:    3
   }

  enum production_status: {
    not_started:  0,
    sent_to_production: 1,
    in_production: 2,
    produced: 3,
    dispatched: 4,
    received: 5,
    sent_to_factory: 6
  }

  aasm column: :production_status, enum: true, logger: Rails.logger do
    state :not_started, initial: true
    state :sent_to_production
    state :in_production
    state :produced
    state :dispatched
    state :received
    state :sent_to_factory

    event :send_to_factory do
      transitions from: :not_started, to: :sent_to_factory, after: proc {
        time = Time.zone.now

        update_column(:sent_to_factory_at, time)

        if seller.is_a?(Store)
          distributor = seller.distributor.third_party
          reason = Movement.reasons[:send_to_factory]

          ProductionCredit.create!(owner: seller.third_party, destination: distributor, order: self, value: -1 * store_total_with_discount, direction: :output, status: :approved, date: time, reason: reason)
          ProductionCredit.create!(owner: distributor, destination: Factory.first.third_party, order: self, value: distributor_total_with_discount, direction: :input, status: :approved, date: time, reason: reason)
        end
      }
    end

    event :cancel_send_to_factory do
      transitions from: :sent_to_factory, to: :not_started, after: proc {
        update_column(:sent_to_factory_at, nil)

        if seller.is_a?(Store)
          distributor = seller.distributor.third_party
          time = Time.zone.now
          reason = Movement.reasons[:cancel_send_to_factory]

          ProductionCredit.create!(owner: seller.third_party, destination: distributor, order: self, value: store_total_with_discount, direction: :input, status: :approved, date: time, reason: reason)
          ProductionCredit.create!(owner: distributor, destination: Factory.first.third_party, order: self, value: -1 * distributor_total_with_discount, direction: :output, status: :approved, date: time, reason: reason)
        end
      }
    end
    
    event :send_to_production do
      transitions from: :sent_to_factory, to: :sent_to_production, after: proc {
        time = Time.zone.now

        update_column(:sent_to_production_at, time)

        if seller.is_a?(Distributor) || seller.is_a?(Store)
          reason = Movement.reasons[:order_production]

          third_party = seller.is_a?(Distributor) ? seller.third_party : seller.distributor.third_party

          ProductionCredit.create!(owner: third_party, destination: Factory.first.third_party, order: self, value: -1 * distributor_total_with_discount, direction: :output, status: :approved, date: time, reason: reason)
        end
      }
    end

    event :cancel_send_to_production do
      transitions from: :sent_to_production, to: :sent_to_factory, after: proc {
        update_column(:sent_to_production_at, nil)

        if seller.is_a?(Distributor) || seller.is_a?(Store)
          reason = Movement.reasons[:cancel_order_production]

          third_party = seller.is_a?(Distributor) ? seller.third_party : seller.distributor.third_party

          ProductionCredit.create!(owner: third_party, destination: Factory.first.third_party, order: self, value: distributor_total_with_discount, direction: :input, status: :approved, date: Time.zone.now, reason: reason)
        end
      }
    end

    event :produce do
      transitions from: :sent_to_production, to: :in_production, after: proc { update_column(:started_production_at, Time.zone.now) }
    end

    event :cancel_produce do
      transitions from: :in_production, to: :sent_to_production, after: proc { update_column(:started_production_at, nil) }
    end

    event :finish do
      transitions from: :in_production, to: :produced, after: proc { update_column(:finished_production_at, Time.zone.now) }
    end

    event :cancel_finish do
      transitions from: :produced, to: :in_production, after: proc { update_column(:finished_production_at, nil) }
    end

    event :dispatch do
      transitions from: :produced, to: :dispatched, after: proc { update_column(:dispatched_at, Time.zone.now) }
    end

    event :cancel_dispatch do
      transitions from: :dispatched, to: :produced, after: proc { update_column(:dispatched_at, nil) }
    end
    
    event :receive do
      transitions from: :dispatched, to: :received, after: proc { update_column(:received_at, Time.zone.now) }
    end

    event :cancel_receive do
      transitions from: :received, to: :dispatched, after: proc { update_column(:received_at, nil) }
    end
  end

  has_many :order_items, dependent: :destroy
  has_many :items, class_name: 'OrderLooseItem', dependent: :destroy
  has_many :components, through: :order_items
  has_one :estimate, dependent: :nullify
  has_many :environments, through: :estimate

  has_many :movements, dependent: :destroy

  belongs_to :user, required: true

  belongs_to :billing_client, polymorphic: true, required: true

  validates_numericality_of :discount, greater_than_or_equal_to: 0, less_than_or_equal_to: 100
  validates_presence_of :status, allow_blank: false
  validates_presence_of :erp_id, allow_blank: false, if: proc { |o| o.sent? }
  validate :billing_client_changed?, unless: proc { |o| o.to_send? || o.error? }
  validate :can_change_special_discount?, unless: proc { |o| o.to_send? || o.error? }
  validates_numericality_of :special_discount, greater_than_or_equal_to: 0, less_than_or_equal_to: 100, allow_nil: true
  validate :client_allows_special_discount?, if: proc { |o| !o.billing_client.nil? && !o.special_discount.to_f.zero? }

  before_destroy :sending_or_sent?

  def has_payments?
    !payments.empty?
  end

  def payments
    Payment.where(id: movements.approved.input.order_payment.pluck(:payment_id)).distinct
  end

  def factory_total
    items_total = order_items.sum(:factory_total)

    products_services_total = items.reduce(0) do |total, item|
      total += item.factory_total
    end

    items_total + products_services_total
  end

  def factory_total_discount
    items_total_discount = order_items.sum('order_items.factory_total * (order_items.discount / 100)')

    products_services_total_discount = items.reduce(0) do |total, item|
      total += item.factory_total_discount
    end

    items_total_discount + products_services_total_discount
  end

  def factory_total_with_discount
    (factory_total - factory_total_discount).try(:round, 2)
  end

  def distributor_total
    items_total = order_items.sum(:distributor_total)

    products_services_total = items.reduce(0) do |total, item|
      total += item.distributor_total
    end

    items_total + products_services_total
  end

  def distributor_total_discount
    items_total_discount = order_items.sum('order_items.distributor_total * (order_items.discount / 100)')

    products_services_total_discount = items.reduce(0) do |total, item|
      total += item.distributor_total_discount
    end

    items_total_discount + products_services_total_discount
  end

  def distributor_total_with_discount
    (distributor_total - distributor_total_discount).try(:round, 2)
  end

  def store_total
    items_total = order_items.sum(:store_total)

    products_services_total = items.reduce(0) do |total, item|
      total += item.store_total
    end

    items_total + products_services_total
  end

  def store_total_discount
    items_total_discount = order_items.sum('order_items.store_total * (order_items.discount / 100)')

    products_services_total_discount = items.reduce(0) do |total, item|
      total += item.store_total_discount
    end

    items_total_discount + products_services_total_discount
  end

  def store_total_with_discount
    (store_total - store_total_discount).try(:round, 2)
  end

  def customer_total
    items_total = order_items.sum(:customer_total)

    products_services_total = items.reduce(0) do |total, item|
      total += item.customer_total
    end

    items_total + products_services_total
  end

  def customer_total_discount
    items_total_discount = order_items.sum('order_items.customer_total * (order_items.discount / 100)')

    products_services_total_discount = items.reduce(0) do |total, item|
      total += item.customer_total_discount
    end

    items_total_discount + products_services_total_discount
  end

  def customer_total_with_discount
    (customer_total - customer_total_discount).try(:round, 2)
  end

  def total
    return distributor_total if client.is_a?(Distributor)
    return store_total       if client.is_a?(Store)
    return customer_total    if client.is_a?(Customer)
  end

  def commercial_discount
    return distributor_total_discount if client.is_a?(Distributor)
    return store_total_discount       if client.is_a?(Store)
    return customer_total_discount    if client.is_a?(Customer)
  end

  def partial_total
    return distributor_total_with_discount if client.is_a?(Distributor)
    return store_total_with_discount       if client.is_a?(Store)
    return customer_total_with_discount    if client.is_a?(Customer)
  end

  def total_discount
    return (distributor_total_discount + (partial_total * (discount / 100))).try(:round, 2) if client.is_a?(Distributor)
    return (store_total_discount + (partial_total * (discount / 100))).try(:round, 2)       if client.is_a?(Store)
    return (customer_total_discount + (partial_total * (discount / 100))).try(:round, 2)    if client.is_a?(Customer)
  end

  def total_with_discount
    (partial_total * (1 - (discount / 100))).try(:round, 2)
  end

  def discount_value
    (partial_total * (discount / 100)).try(:round, 2)
  end

  def total_with_special_discount
    (total_with_discount * (1 - (special_discount.to_f / 100))).try(:round, 2)
  end

  def balance_value
    return factory_total_with_discount     if seller.is_a?(Factory)
    return distributor_total_with_discount if seller.is_a?(Distributor)
    return store_total_with_discount       if seller.is_a?(Store)
    return 0
  end

  private

  def billing_client_changed?
    errors.add(:base, I18n.t('activerecord.errors.models.order.cannot_change_billing_client')) if billing_client_id_changed? || billing_client_type_changed?
  end

  def can_change_special_discount?
    errors.add(:base, I18n.t('activerecord.errors.models.order.cannot_change_special_discount')) if special_discount_changed?
  end

  def sending_or_sent?
    if sending? || sent?
      errors.add(:base, I18n.t('activerecord.errors.models.order.cancel_order_first'))

      false
    end
  end

  def client_allows_special_discount?
    errors.add(:base, I18n.t('activerecord.errors.models.order.client_does_not_allow_special_discount')) if !billing_client.allow_special_discount? && !special_discount.to_f.zero?
  end
end
