# == Schema Information
#
# Table name: movements
#
#  id             :integer          not null, primary key
#  owner_id       :integer          not null
#  order_id       :integer
#  payment_id     :integer
#  value          :decimal(8, 2)    not null
#  direction      :integer          not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  status         :integer          default(1), not null
#  date           :datetime         not null
#  type           :string
#  destination_id :integer          not null
#  reason         :integer          default(0), not null
#

class Movement < ActiveRecord::Base
  belongs_to :owner, required: true, class_name: 'ThirdParty'
  belongs_to :destination, required: true, class_name: 'ThirdParty'
  belongs_to :order
  belongs_to :payment

  validates :date, presence: :true
  validates :value, numericality: { allow_nil: false, greater_than: 0 }, if: :input?
  validates :value, numericality: { allow_nil: false, less_than: 0 }, if: :output?
  validates :order, presence: :true, if: proc { |movement| movement.output? && !movement.antecipation? && !movement.store_antecipation? }
  validates :status, :direction, presence: :true
  validates :type, presence: true
  validates :reason, presence: true
  validates :payment, presence: true, if: Proc.new { |movement| movement.credit? || movement.order_payment? }
  validate :owner_is_factory_third_party?, if: Proc.new { |movement| movement.input? && movement.owner_id.present? }
  validate :destination_third_party, if: Proc.new { |movement| movement.input? && movement.owner_id.present? && movement.destination_id.present? }
  validate :owner_not_distributor?, if: Proc.new { |movement| movement.store_credit? || movement.store_antecipation? }
  validate :destination_not_factory?, if: Proc.new { |movement| movement.store_credit? || movement.store_antecipation? }

  scope :approved_or_antecipated, -> { where(status: [Movement.statuses[:approved], Movement.statuses[:antecipated]]) }

  enum direction: {
    input: 0,
    output: 1
  }

  enum status: {
    pending: 0,
    approved: 1,
    antecipated: 2
  }

  enum reason: {
    credit: 0,
    order_payment: 1,
    send_to_factory: 2,
    cancel_send_to_factory: 3,
    order_production: 4,
    cancel_order_production: 5,
    antecipation: 6,
    store_credit: 7,
    store_antecipation: 8
  }

  def installment_number
    return 1 unless payment.present?

    payment.movements.where(reason: reason).order('movements.date ASC').pluck(:id).index(id).to_i + 1
  end

  private

  def owner_is_factory_third_party?
    errors.add(:base, I18n.t('activerecord.errors.models.movement.owner_cannot_be_factory')) if owner.specific.is_a?(Factory)
  end

  def destination_third_party
    errors.add(:base, I18n.t('activerecord.errors.models.movement.destination_is_not_distributor')) if owner.specific.is_a?(Store) && !destination.specific.is_a?(Distributor)
    errors.add(:base, I18n.t('activerecord.errors.models.movement.destination_is_not_factory')) if owner.specific.is_a?(Distributor) && !destination.specific.is_a?(Factory)
  end

  def owner_not_distributor?
    errors.add(:base, I18n.t('activerecord.errors.models.movement.owner_must_be_distributor')) if !owner.specific.is_a?(Distributor)
  end

  def destination_not_factory?
    errors.add(:base, I18n.t('activerecord.errors.models.movement.destination_must_be_factory')) if !destination.specific.is_a?(Factory)
  end
end
