# == Schema Information
#
# Table name: line_categories
#
#  id          :integer          not null, primary key
#  line_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  required    :boolean          default(TRUE), not null
#  category_id :integer
#  order       :integer          not null
#

class LineCategory < ActiveRecord::Base
  include BelongsToCategory
  include BelongsToLine

  default_scope { order('"order"') } 

  validates :category, uniqueness: {scope: :line_id}
  validates_inclusion_of :required, in: [true, false]
  validates :order, presence: true, numericality: {only_integer: true, greater_than_or_equal_to: 0}, allow_blank: false

  after_destroy :remove_associated_rules

  private
    def remove_associated_rules
      Rule.joins(:component, :line_rule).where(line_rules: {line_id: self.line_id}).where(components: {category_id: self.category_id}).destroy_all
    end
end
