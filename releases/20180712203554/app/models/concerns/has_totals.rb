module HasTotals
  extend ActiveSupport::Concern

  included do
    validates_numericality_of :factory_total, greater_than_or_equal_to: 0, allow_blank: true
    validates_numericality_of :distributor_total, greater_than_or_equal_to: 0, allow_blank: true
    validates_numericality_of :store_total, greater_than_or_equal_to: 0, allow_blank: true
    validates_numericality_of :customer_total, greater_than_or_equal_to: 0, allow_blank: true

    def total
      return distributor_total if client.is_a?(Distributor)
      return store_total       if client.is_a?(Store)
      return customer_total    if client.is_a?(Customer)
    end

    def total_discount
      return (total * (discount / 100)).round(2)
    end

    def total_with_discount
      return (total * ((100.0 - discount) / 100)).round(2)
    end
  end
end