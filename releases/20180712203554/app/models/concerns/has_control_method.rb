module HasControlMethod
  extend ActiveSupport::Concern

  included do
    enum control_method: {
      width:      0, 
      height:     1, 
      area:       2, 
      unit:       3,
      total_area: 4
    }
  end
end