module HasCST
  extend ActiveSupport::Concern

  included do
    enum cst: {
      national:               0, 
      direct_import:          1, 
      foreign_im:             2, 
      national_with_ic_gt_40: 3,
      national_other:         4,
      national_with_ic_lt_40: 5,
      foreign_di_camex:       6,
      foreign_im_camex:       7
    }
  end
end