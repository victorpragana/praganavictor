module BelongsToCutTable
  extend ActiveSupport::Concern

  included do
    belongs_to :cut_table

    validates_absence_of :cut_table, if: Proc.new { |object| !object.category.present? || !object.category.use_cut_table? }
    validates_presence_of :cut_table, if: Proc.new { |object| object.category.present? && object.category.use_cut_table? }
  end
end