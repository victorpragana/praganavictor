# == Schema Information
#
# Table name: price_table_lines
#
#  id               :integer          not null, primary key
#  price_table_id   :integer
#  line_id          :integer
#  price            :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  maximum_discount :decimal(5, 2)    default(0.0)
#

class PriceTableLine < ActiveRecord::Base
  include HasFormula

  belongs_to :line, required: true
  belongs_to :price_table, required: true

  has_paper_trail

  validates_presence_of :price, allow_blank: false
  validate :valid_price_formula?, unless: 'price.blank?'
  validate :price_table_for_items?, unless: 'price_table_id.blank?'
  validates_numericality_of :maximum_discount, greater_than_or_equal_to: 0, less_than_or_equal_to: 100

  private

  def valid_price_formula?
    begin
      validate_field_formula(:price, ['_CMV_'])
    rescue Exception => e
      errors.add(:base, I18n.t('activerecord.errors.models.price_table_line.invalid_price_formula'))
    end
  end

  def price_table_for_items?
    errors.add(:base, I18n.t('activerecord.errors.models.price_table_line.price_table_for_items')) if price_table.for_items?
  end
end
