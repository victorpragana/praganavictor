# == Schema Information
#
# Table name: items
#
#  id             :integer          not null, primary key
#  code           :string
#  description    :string           not null
#  unit           :integer          not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  deleted_at     :datetime
#  type           :integer          default(0)
#  erp_categories :json
#  ncm            :string(8)
#  cst            :integer
#

class Item < ActiveRecord::Base
  include HasUnit
  include HasCST

  self.inheritance_column = :_type_disabled

  enum type: {
    product: 0,
    service: 1
  }

  acts_as_paranoid

  validates_presence_of :description, allow_blank: false
  validates_presence_of :unit, allow_blank: false
  validates_presence_of :type, allow_blank: false
  validates :ncm, format: { with: /\A[0-9]{8}\z/, message: "deve ter 8 dígitos númericos" }, allow_blank: true
end
