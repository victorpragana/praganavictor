# == Schema Information
#
# Table name: base_components
#
#  id                        :integer          not null, primary key
#  name                      :string(255)      not null
#  cost                      :decimal(10, 5)   default(0.0), not null
#  category_id               :integer
#  characteristics           :text
#  code                      :string(20)
#  unit                      :integer          default(2), not null
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  deleted_at                :datetime
#  reference_cost            :decimal(10, 5)   default(0.0)
#  offset_left               :integer          default(0)
#  offset_top                :integer          default(0)
#  offset_right              :integer          default(0)
#  offset_bottom             :integer          default(0)
#  allow_rotation            :boolean          default(FALSE)
#  exchange                  :decimal(10, 5)
#  loss                      :decimal(10, 5)
#  cut_table_id              :integer
#  print_on_lines_label      :boolean          default(FALSE)
#  fix_cost                  :decimal(10, 5)   default(0.0)
#  print_on_components_label :boolean          default(FALSE)
#

class BaseComponent < ActiveRecord::Base
  include BelongsToCategory
  include HasUnit
  include HasConversion
  include BelongsToCutTable

  acts_as_paranoid

  has_and_belongs_to_many :families
  has_many :rules
  has_many :components, dependent: :destroy
  has_many :values, dependent: :destroy, class_name: 'BaseComponentValue'

  validates :name, presence: { allow_blank: false }, uniqueness: { case_sensitive: false, scope: :category_id }, length: 1..255
  validates :cost, presence: { allow_blank: false }, numericality: { greater_than_or_equal_to: 0 }
  validates :reference_cost, numericality: { greater_than_or_equal_to: 0 }
  validates :fix_cost, numericality: { greater_than_or_equal_to: 0 }
  validates :code, length: 0..20,  allow_blank: true 
  validates :unit, presence: { allow_blank: false }
  validate :more_than_one_automatic_value?

  validates :offset_left, numericality: { greater_than_or_equal_to: 0, only_integer: true }
  validates :offset_top, numericality: { greater_than_or_equal_to: 0, only_integer: true }
  validates :offset_right, numericality: { greater_than_or_equal_to: 0, only_integer: true }
  validates :offset_bottom, numericality: { greater_than_or_equal_to: 0, only_integer: true }

  has_conversion_for :offset_left
  has_conversion_for :offset_top
  has_conversion_for :offset_right
  has_conversion_for :offset_bottom

  private
  def more_than_one_automatic_value?
    errors.add(:base, I18n.t('activerecord.errors.models.base_component.more_than_one_automatic_value')) if self.values.select { |bvc| bvc.automatic? }.map(&:specification_id).uniq.size > 1
  end
end
