# == Schema Information
#
# Table name: environments
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  category    :integer          not null
#  estimate_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Environment < ActiveRecord::Base
  include TranslateEnum

  belongs_to :estimate, required: true

  delegate :client, to: :estimate
  delegate :seller, to: :estimate

  has_many :locations, dependent: :destroy

  enum category: {
    room:         0,
    kitchen:      1,
    balcony:      2,
    living_room:  3,
    dining_room:  4,
    hall:         5,
    home_theater: 6,
    office:       7,
    lavatory:     8,
    laundry:      9,
    other:       10
  }

  translate_enum :category

  validates_presence_of :category, allow_blank: false
end
