# == Schema Information
#
# Table name: movements
#
#  id             :integer          not null, primary key
#  owner_id       :integer          not null
#  order_id       :integer
#  payment_id     :integer
#  value          :decimal(8, 2)    not null
#  direction      :integer          not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  status         :integer          default(1), not null
#  date           :datetime         not null
#  type           :string
#  destination_id :integer          not null
#  reason         :integer          default(0), not null
#

class MoneyCredit < Movement
  def equivalent_movement
    return nil if owner.specific.is_a?(Distributor) || owner.specific.is_a?(Factory)
    return MoneyCredit.find_by(owner: destination_id, value: value, direction: direction, date: date.beginning_of_day..date.end_of_day, payment_id: payment_id, reason: Movement.reasons[:store_credit])
  end
end
