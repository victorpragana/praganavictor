json.set! :payment_condition do
  json.id                      @payment_condition.id
  json.type                    PaymentCondition.types[@payment_condition.type]
  json.installments            @payment_condition.installments
  json.additional_tax          @payment_condition.additional_tax.to_f
  json.credit_tax              @payment_condition.credit_tax.to_f
  json.antecipation_tax        @payment_condition.antecipation_tax.to_f
  json.credit_antecipation_tax @payment_condition.credit_antecipation_tax.to_f
end