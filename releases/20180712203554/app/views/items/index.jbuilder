json.items @items do |item|
  json.(item, :id, :code, :description)

  json.unit Item.units[item.unit]
  json.type Item.types[item.type]
end