json.set! :question do
  json.(@question, :id, :label, :value)

  json.type Question.types[@question.type]

  json.options @question.options do |option|
    json.label option['label']
    json.value option['value']
  end

  json.line_ids @question.line_ids
end