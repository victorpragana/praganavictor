json.set! :customer do
  json.id                     @customer.id
  json.name                   @customer.name
  json.rg                     @customer.rg
  json.issuing_entity         @customer.issuing_entity
  json.identification         @customer.identification
  json.zip_code               @customer.zip_code
  json.street                 @customer.street
  json.neighborhood           @customer.neighborhood
  json.number                 @customer.number
  json.complement             @customer.complement
  json.phone                  @customer.phone
  json.email                  @customer.email
  json.store_id               @customer.store_id
  json.corporate_name         @customer.corporate_name
  json.state_inscription      @customer.state_inscription
  json.municipal_inscription  @customer.municipal_inscription
  json.allow_special_discount @customer.allow_special_discount?
  json.special_discount       @customer.special_discount.to_f

  unless @customer.city_id.nil?
    json.set! :city do
      json.id           @customer.city.id
      json.name         @customer.city.name
    end

    json.set! :state do
      json.id           @customer.state.id
      json.name         @customer.state.name
    end
  end
end