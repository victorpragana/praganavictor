json.categories @categories do |category|
  json.id            category.id
  json.name          category.name
  json.use_cut_table category.use_cut_table
end