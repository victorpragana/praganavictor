json.set! :store do
  json.id                     @store.id
  json.name                   @store.name
  json.identification         @store.identification
  json.zip_code               @store.zip_code
  json.street                 @store.street
  json.neighborhood           @store.neighborhood
  json.number                 @store.number
  json.complement             @store.complement
  json.phone                  @store.phone
  json.email                  @store.email
  json.corporate_name         @store.corporate_name
  json.state_inscription      @store.state_inscription
  json.municipal_inscription  @store.municipal_inscription
  json.distributor_id         @store.distributor_id
  json.allow_special_discount @store.allow_special_discount?
  json.special_discount       @store.special_discount.to_f
  json.subdomain              @store.subdomain
  json.logo_url               @store.logo.url
  json.logo_file_name         @store.logo_file_name
  json.current_account        @store.current_account
  json.agency                 @store.agency
  json.bank                   ThirdParty.banks[@store.bank]

  unless @store.city_id.nil?
    json.set! :city do
      json.id           @store.city.id
      json.name         @store.city.name
    end

    json.set! :state do
      json.id           @store.state.id
      json.name         @store.state.name
    end
  end
end