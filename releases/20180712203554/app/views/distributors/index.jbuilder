json.distributors @distributors do |distributor|
  json.id                   distributor.id
  json.name                 distributor.name
  json.price_table          distributor.price_table_for(current_administrator.workplace).try(:name)
  json.items_price_table    distributor.items_price_table_for(current_administrator.workplace).try(:name)
end