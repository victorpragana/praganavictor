json.users @users do |user|
  json.id       user.id
  json.name     user.name
  json.username user.username
  json.email    user.email
  json.type     user.type

  json.workplaces user.workplaces do |workplace|
    json.id             workplace.id
    json.workplace_id   workplace.workplace_id
    json.workplace_type workplace.workplace_type
    json.workplace_name workplace.workplace.name
  end
end