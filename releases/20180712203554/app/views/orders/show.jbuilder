json.set! :order do
  json.id                  @order.id
  json.date                @order.date.strftime('%d/%m/%Y')
  json.partial_total       @order.partial_total.to_f
  json.commercial_discount @order.commercial_discount.to_f
  json.discount            @order.discount.to_f
  json.discount_value      @order.discount_value.to_f
  json.total_discount      @order.total_discount.to_f
  json.total_with_discount @order.total_with_discount.to_f
  json.total               @order.total.to_f
  json.special_discount    @order.special_discount.nil? ? nil : @order.special_discount.to_f
  json.observation         @order.observation
  json.status              Order.statuses[@order.status]
  json.production_status   Order.production_statuses[@order.production_status]
  json.processing_messages @order.processing_messages.reverse

  json.total_with_special_discount @order.total_with_special_discount.to_f

  json.has_payments        @order.has_payments?

  json.payments             @order.payments do |payment|
    json.id                 payment.id
    json.installment_number payment.installment_number
    json.payment_method     Payment.payment_methods[payment.payment_method]
    json.value              payment.original_value.to_f
  end

  json.set! :client do
    json.id    @order.client_id
    json.name  @order.client.name
    json.type  @order.client.class.to_s
  end

  json.set! :billing_client do
    json.id                     @order.billing_client_id
    json.name                   @order.billing_client.name
    json.type                   @order.billing_client.class.to_s
    json.allow_special_discount @order.billing_client.allow_special_discount?
    json.special_discount       @order.billing_client.special_discount.to_f
  end

  json.set! :seller do
    json.id    @order.seller_id
    json.name  @order.seller.name
    json.type  @order.seller.class.to_s
  end

  json.estimate_id  @order.estimate.try(:id)
  json.express      @order.estimate.try(:express?)

  json.environments @order.environments.includes(locations: [:options]) do |environment|
    json.id               environment.id
    json.name             environment.name
    json.category         Environment.categories[environment.category]
    json.has_order_items  environment.locations.map { |l| !l.options.includes(:order_item).map(&:order_item).compact.empty? }.flatten.include?(true)

    location_totals = [0]

    json.locations environment.locations do |location|
      location_totals << location.options.includes(order_item: %i[components line]).map(&:order_item).compact.reduce(0) { |total, order_item| total += order_item.total_with_discount }.to_f

      json.name             location.name
      json.category         Location.categories[location.category]
      json.has_order_items  !location.options.map(&:order_item).compact.empty?

      json.order_items location.options.map(&:order_item).compact do |order_item|
        json.id                  order_item.id
        json.width               order_item.width_from_mm_to_cm
        json.height              order_item.height_from_mm_to_cm
        json.discount            order_item.discount
        json.total_with_discount order_item.total_with_discount
        json.total               order_item.total
        json.identification      order_item.identification
        json.specification       order_item.specification

        json.set! :line do
          json.id             order_item.line.id
          json.name           order_item.line.name

          json.set! :family do
            json.id           order_item.line.family.id
            json.name         order_item.line.family.name
            json.color        order_item.line.family.color
          end
        end

        json.components order_item.components do |component|
          json.set! :component do
            json.name             component.component_name
            json.characteristics  component.component_characteristics
            json.unit             OrderItemComponent.units[component.unit]

            json.set! :category do
              json.name component.category_name
            end
          end

          json.total_consumption    component.total_consumption.to_f
          json.width_consumption    component.width_consumption.try(:to_f)
          json.height_consumption   component.height_consumption.try(:to_f)
          json.quantity_consumption component.quantity_consumption.try(:to_f)
          json.unit_value           component.unit_value.to_f
          json.total                component.total.to_f
          json.required             component.required
        end
      end
    end

    json.total location_totals.inject(0, :+).to_f
  end

  json.items @order.items do |item|
    json.id                  item.id
    json.identification      item.identification
    json.item_code           item.item_code
    json.item_description    item.item_description
    json.unit                OrderLooseItem.units[item.unit]
    json.type                OrderLooseItem.types[item.type]
    json.quantity            item.quantity.to_f
    json.value               item.value.to_f
    json.discount            item.discount.to_f
    json.total               item.total.to_f
    json.total_with_discount item.total_with_discount.to_f
  end
end