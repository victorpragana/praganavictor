json.specifications @specifications do |specification|
  json.(specification, :id, :name, :automatic, :used_in_filter)

  values = if params[:family_id].present?
    specification.values.joins(components: [:families]).where("families.id = ?", params[:family_id]).distinct
  else
    specification.values
  end

  json.values values.order(:value) do |value|
    json.id               value.id
    json.value            value.value
    json.comparison_value value.comparison_value_from_mm_to_cm
  end
end