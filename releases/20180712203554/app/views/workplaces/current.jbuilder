json.set! :workplace do
  json.id       @workplace.id
  json.logo_url Rails.env.development? ? '' : @workplace.logo.url
end