json.id                   component.id
json.name                 component.name
json.characteristics      component.characteristics
json.unit                 Component.units[component.unit]
json.rule_required        component.rule_required
json.informed_consumption component.informed_consumption
json.automatic            component.automatic?
json.component_ids        component.component_ids
json.rule_id              component.rule_id
json.child_ids            component.child_ids
json.parent_ids           component.parent_ids

json.set! :category do
  json.id    component.category.id
  json.name  component.category.name
  json.order component.category.order_for_line(@line_id)
end

json.families component.families do |family|
  json.name   family.name
  json.color  family.color
end