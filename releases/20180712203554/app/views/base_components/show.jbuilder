json.set! :base_component do
  json.id               @base_component.id
  json.name             @base_component.name
  json.code             @base_component.code
  json.unit             Component.units[@base_component.unit]
  json.characteristics  @base_component.characteristics
  json.cost             @base_component.cost.to_f
  json.reference_cost   @base_component.reference_cost.to_f
  json.category_id      @base_component.category_id
  json.family_ids       @base_component.family_ids
  json.fix_cost         @base_component.fix_cost.to_f
  json.exchange         @base_component.exchange.nil? ? nil : @base_component.exchange.to_f
  json.loss             @base_component.loss.nil? ? nil : @base_component.loss.to_f
  json.offset_left      @base_component.offset_left_from_mm_to_cm
  json.offset_top       @base_component.offset_top_from_mm_to_cm
  json.offset_right     @base_component.offset_right_from_mm_to_cm
  json.offset_bottom    @base_component.offset_bottom_from_mm_to_cm
  json.allow_rotation   @base_component.allow_rotation?
  json.cut_table_id     @base_component.cut_table_id
  json.deleted          @base_component.deleted?
  json.print_on_components_label   @base_component.print_on_components_label?
  json.print_on_lines_label        @base_component.print_on_lines_label?

  json.values           @base_component.values.only_parents do |parent|
    json.id             parent.value.id
    json.value          parent.value.value
    json.automatic      parent.value.automatic?
    json.partial!       'base_components/children', children: parent.children
  end
end