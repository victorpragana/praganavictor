json.product_categories @product_categories do |product_category|
  json.id    product_category['id']
  json.name  product_category['nome']
  json.level product_category['nivel']
end