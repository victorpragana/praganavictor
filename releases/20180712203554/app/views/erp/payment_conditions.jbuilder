json.payment_conditions @payment_conditions do |payment_condition|
  json.id    payment_condition['id']
  json.name  payment_condition['descricao']
end