class OrderItemService
  attr_reader :success, :errors, :record

  def initialize(parameters)
    @parameters = parameters
  end

  def apply_total(order_item_id)
    order_item = OrderItem.find(order_item_id)
    
    order_item.send("#{order_item.client.class.name.underscore}_total=", @parameters['total'])

    save_record(order_item)
  end



  private
  def save_record(record)
    if record.save
      @success = true
      @record = record.reload
    else
      @success = false
      @errors = record.errors.full_messages
    end
  end
end