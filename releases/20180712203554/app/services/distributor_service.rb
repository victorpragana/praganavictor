class DistributorService
  attr_reader :success, :errors, :record

  def initialize(parameters)
    @parameters = parameters

    @parameters['bank'] = @parameters['bank'].to_i if @parameters['bank'].present?
  end

  def update(distributor_id)
    ActiveRecord::Base.transaction do
      distributor = Distributor.find(distributor_id)

      distributor.assign_attributes(@parameters)

      save_record(distributor)
    end
  end

  def create
    ActiveRecord::Base.transaction do
      distributor = Distributor.with_deleted.find_or_initialize_by(name: @parameters['name']).tap do |s|
        s.assign_attributes(@parameters)
        s.deleted_at = nil
      end

      save_record(distributor)
    end
  end

  private

  def save_record(distributor)
    if distributor.save
      @success = true
      @record = distributor.reload
    else
      @success = false
      @errors = distributor.errors.full_messages
    end
  end
end