class EstimateItemService
  attr_reader :success, :errors, :record

  def initialize(parameters = {})
    @parameters = parameters
  end

  def add_to(estimate_id)
    begin
      ActiveRecord::Base.transaction do
        item = Item.find(@parameters['item_id'])
        estimate = Estimate.find(estimate_id)

        estimate_item = EstimateItem.new

        estimate_item.item = item
        estimate_item.item_code = item.code
        estimate_item.item_description = item.description
        estimate_item.unit = item.unit
        estimate_item.type = item.type
        estimate_item.quantity = @parameters['quantity']
        estimate_item.estimate = estimate

        seller = estimate.seller
        client = estimate.client

        price_table = load_price_table(seller, client, item)

        if client.is_a?(Distributor)
          estimate_item.distributor_value = price_table.price
        elsif client.is_a?(Store)
          estimate_item.store_value = price_table.price
        elsif client.is_a?(Customer)
          estimate_item.customer_value = price_table.price
        end

        save_record(estimate_item)
      end
    rescue ActiveRecord::RecordInvalid => e
      @success = false
      @errors = e.record.errors.full_messages
    rescue => e
      @success = false
      @errors = [e.message]
    end
  end

  def destroy(estimate_item_id)
    estimate_item = EstimateItem.find(estimate_item_id)

    if estimate_item.destroy
      @success = true
      @record = estimate_item
    else
      @success = false
      @errors = estimate_item.errors.full_messages
    end
  end

  def apply_discount(estimate_item_id)
    estimate_item = EstimateItem.find(estimate_item_id)

    if estimate_item.update(discount: @parameters['discount'])
      @success = true
      @record = estimate_item
    else
      @success = false
      @errors = estimate_item.errors.full_messages
    end
  end

  private
  def load_price_table(seller, client, item)
    price_table = client.items_price_table_for(seller, true)

    raise Exceptions::NoPriceTableError.new(I18n.t('services.errors.estimate_service.no_item_price_table', 
      seller: seller.name, client: client.name, item: item.description)) if price_table.blank?

    price_table_for_item = PriceTableItem.find_by(item_id: item.id, price_table_id: price_table.id)

    raise Exceptions::NoPriceTableError.new(I18n.t('services.errors.estimate_service.no_item_price_table', 
      seller: seller.name, client: client.name, item: item.description)) if price_table_for_item.blank?

    return price_table_for_item
  end

  def save_record(estimate_item)
    estimate_item.save!
      
    @success = true
    @record = estimate_item.reload
  end
end