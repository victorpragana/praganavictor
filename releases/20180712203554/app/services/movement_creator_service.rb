class MovementCreatorService
  attr_reader :success, :errors

  def initialize(credit_type, payment, order)
    @credit_type = credit_type
    @payment = payment
    @order = order
  end

  def input(owner, destination)
    begin
      status = @payment.bank_slip? ? :pending : :approved
      reason = @order.present? ? Movement.reasons[:order_payment] : Movement.reasons[:credit]

      date = Time.zone.now + 1.month
      installments = @payment.installment_number

      installment_value = (@payment.original_value / installments.to_f).round(2)

      installments.times do |i|
        value = (i != (installments - 1)) ? installment_value : @payment.original_value - (installment_value * i)

        @credit_type.create!(date: date + i.months, owner: owner, destination: destination, order: @order, payment: @payment, value: value, direction: :input, status: status, reason: reason)

        if @credit_type == MoneyCredit && owner.specific.is_a?(Store)
          @credit_type.create!(date: date + i.months, owner: destination, destination: factory.third_party, order: @order, payment: @payment, value: value, direction: :input,
            status: status, reason: Movement.reasons[:store_credit])
        end
      end

      @success = true
    rescue => e
      @success = false
      @errors = [e.message]
    end
  end

  private

  def factory
    @factory = Factory.first
  end
end