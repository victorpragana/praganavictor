class CommercialClassificationService
  attr_reader :success, :errors, :record

  def initialize(parameters = {})
    @parameters = parameters
  end

  def create
    commercial_classification = CommercialClassification.new(@parameters)

    save_record(commercial_classification)
  end

  def update(commercial_classification_id)
    commercial_classification = CommercialClassification.find(commercial_classification_id)

    commercial_classification.assign_attributes(@parameters)

    save_record(commercial_classification)
  end

  def apply_to_distributors(commercial_classification_id, ids)
    apply_classification(Distributor, commercial_classification_id, ids)
  end

  def apply_to_stores(commercial_classification_id, ids)
    apply_classification(Store, commercial_classification_id, ids)
  end

  private

  def save_record(commercial_classification)
    if commercial_classification.save
      @success = true
      @record = commercial_classification.reload
    else
      @success = false
      @errors = commercial_classification.errors.full_messages
    end
  end

  def apply_classification(klass, commercial_classification_id, ids)
    begin
      klass.where(id: ids).update_all(commercial_classification_id: commercial_classification_id)

      @success = true
    rescue => e
      @success = false
      @errors = [e.message]
    end
  end
end