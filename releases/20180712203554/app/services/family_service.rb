class FamilyService
  attr_reader :success, :errors, :record

  def initialize(parameters)
    @parameters = parameters
  end

  def update(family_id)
    family = Family.find(family_id)

    family.name = @parameters['name']
    family.color = @parameters['color']

    save_record(family)
  end

  def create
    family = Family.new({name: @parameters['name'], color: @parameters['color']})

    save_record(family)
  end

  private
  def save_record(family)
    if family.save
      @success = true
      @record = family.reload
    else
      @success = false
      @errors = family.errors.full_messages
    end
  end
end