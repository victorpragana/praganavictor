class ComponentTotalCostCalculatorService < BaseCalculatorService
  validate :automatic_component?, unless: "component_id.blank?"

  def cost
    return 0 if rule.nil?

    return consumption.to_f * component.final_cost
  end

  def consumption
    if !component.un?
      if rule.width?
        return height
      elsif rule.height?
        return width
      else
        return 0
      end
    else
      if rule.width?
        return (height / unit_dimension).round(2)
      elsif rule.height?
        return (width / unit_dimension).round(2)
      else
        return 0
      end
    end
  end

  private
    def automatic_component?
      errors.add(:base, I18n.t("activemodel.errors.models.component_total_cost_calculator_service.non_automatic_component")) unless component.automatic?
    end
end