class ItemService
  attr_reader :success, :errors, :record

  def initialize(parameters)
    @parameters = parameters

    @parameters['unit'] = @parameters['unit'].to_i if @parameters['unit'].present?
    @parameters['type'] = @parameters['type'].to_i if @parameters['type'].present?
    @parameters['cst']  = @parameters['cst'].to_i if @parameters['cst'].present?
  end

  def create
    item = Item.with_deleted.find_or_initialize_by(description: @parameters['description'], type: @parameters['type']).tap do |_item|
      _item.deleted_at = nil
    end

    item.assign_attributes(@parameters)

    save_record(item)
  end

  def update(item_id)
    item = Item.find(item_id)

    item.assign_attributes(@parameters)

    save_record(item)
  end

  private
  def save_record(item)
    if item.save
      @success = true
      @record = item.reload
    else
      @success = false
      @errors = item.errors.full_messages
    end
  end
end