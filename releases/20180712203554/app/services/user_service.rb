class UserService
  attr_reader :success, :errors, :record

  def initialize(parameters)
    @parameters = parameters
    @workplaces = @parameters.delete('workplaces')
  end

  def update(user_id)
    user = User.find(user_id)

    user.assign_attributes(@parameters)

    save_record(user)
  end

  def create
    user = User.new(@parameters)

    save_record(user)
  end

  private
  def save_record(user)
    assign_workplaces(user)

    if user.save
      @success = true
      @record = User.find(user.id)
    else
      @success = false
      @errors = user.errors.full_messages

      user.workplaces.each do |workplace|
        @errors.concat(workplace.errors.full_messages) unless workplace.valid?
      end
    end
  end

  def assign_workplaces(user)
    workplaces = []
    
    workplaces = @workplaces.map { |workplace| UserWorkplace.find_or_initialize_by(workplace_id: workplace['workplace_id'], workplace_type: workplace['workplace_type'], user_id: user.id) } if @workplaces.present?

    if !user.new_record? && !workplaces.map { |workplace| workplace.id }.include?(user.selected_workplace_id)
      user.update_attribute(:selected_workplace_id, nil)
    end

    user.workplaces = workplaces

    user.selected_workplace = workplaces.first unless user.selected_workplace.present? || workplaces.include?(user.selected_workplace)
  end
end