class LineRuleService
  attr_reader :success, :errors, :record

  def initialize(parameters)
    @parameters = parameters
  end

  def duplicate(line_id)
    line = Line.find(line_id)

    begin
      ActiveRecord::Base.transaction do
        line_rule = line.line_rules.find(@parameters['line_rule_id'])

        new_line_rule = line_rule.deep_clone do |original, clone|
          clone.description = original.description.concat(" (clone)")
        end

        new_line_rule.save!

        line_rule.rules.each do |rule|
          new_rule = rule.deep_clone do |original, clone|
            clone.line_rule = new_line_rule
            clone.description = original.description.concat(" (clone)")
          end

          new_rule.save!
        end

        @success = true
        @record = new_line_rule.reload
      end
    rescue ActiveRecord::RecordInvalid => e
      @success = false
      @errors = e.record.errors.full_messages
    rescue => e
      @success = false
      @errors = [e.message]
    end
  end

  def import(line_id)
    begin
      ActiveRecord::Base.transaction do
        @parameters['line_rule_ids'].each do |line_rule_id|
          line_rule = LineRule.find(line_rule_id)

          new_line_rule = line_rule.deep_clone do |original, clone|
            clone.line_id = line_id
          end

          new_line_rule.save!

          line_rule.rules.each do |rule|
            new_rule = rule.deep_clone do |original, clone|
              clone.line_rule = new_line_rule
            end

            new_rule.save!
          end
        end

        @success = true
      end
    rescue ActiveRecord::RecordInvalid => e
      @success = false
      @errors = e.record.errors.full_messages
    rescue => e
      @success = false
      @errors = [e.message]
    end
  end

  def set_line_rules_for(line_id)
    line = Line.includes(:components).find(line_id)

    begin
      ActiveRecord::Base.transaction do
        if @parameters.blank?
          line.line_rules.destroy_all
        else
          line_rule_ids = @parameters.map{|lrp| lrp["id"]}.compact

          line.line_rules.where.not(id: line_rule_ids).destroy_all

          @parameters.each do |line_rule_parameter|
            lr = LineRule.find(line_rule_parameter["id"]) unless line_rule_parameter["id"].blank?
            lr ||= LineRule.new

            lr.line_id              = line.id
            lr.control_method       = line_rule_parameter["control_method"]["id"].to_i if line_rule_parameter["control_method"].present? && !line_rule_parameter["control_method"]["id"].blank?
            lr.width_consumption    = line_rule_parameter["width_consumption"].try{gsub("\xE2\x80\x8B", "")}
            lr.height_consumption   = line_rule_parameter["height_consumption"].try{gsub("\xE2\x80\x8B", "")}
            lr.total_consumption    = line_rule_parameter["total_consumption"].try{gsub("\xE2\x80\x8B", "")}
            lr.quantity_consumption = line_rule_parameter["quantity_consumption"].try{gsub("\xE2\x80\x8B", "")}
            lr.cost                 = line_rule_parameter["cost"].try{gsub("\xE2\x80\x8B", "")}
            lr.required             = line_rule_parameter["required"]
            lr.automatic            = line_rule_parameter["automatic"]
            lr.description          = line_rule_parameter["description"]
            lr.category_id          = line_rule_parameter["category_id"]
            lr.weight               = line_rule_parameter["weight"].to_i

            case lr[:control_method]
              when LineRule.control_methods[:width]
                lr.width  = line_rule_parameter["initial_width"].to_i..line_rule_parameter["final_width"].to_i if line_rule_parameter["initial_width"].present? && line_rule_parameter["final_width"].present?
              when LineRule.control_methods[:height]
                lr.height = line_rule_parameter["initial_height"].to_i..line_rule_parameter["final_height"].to_i if line_rule_parameter["initial_height"].present? && line_rule_parameter["final_height"].present?
              when LineRule.control_methods[:area]
                lr.width  = line_rule_parameter["initial_width"].to_i..line_rule_parameter["final_width"].to_i if line_rule_parameter["initial_width"].present? && line_rule_parameter["final_width"].present?
                lr.height = line_rule_parameter["initial_height"].to_i..line_rule_parameter["final_height"].to_i if line_rule_parameter["initial_height"].present? && line_rule_parameter["final_height"].present?
              when LineRule.control_methods[:total_area]
                lr.area   = line_rule_parameter["initial_area"].to_i..line_rule_parameter["final_area"].to_i if line_rule_parameter["initial_area"].present? && line_rule_parameter["final_area"].present?
            end

            if line_rule_parameter["component_ids"].present? && !line_rule_parameter["component_ids"].empty?
              lr.rules = line_rule_parameter["component_ids"].map do |component_id|
                lr.rules.find_or_initialize_by(component_id: component_id).tap do |rule|
                  rule.assign_attributes(lr.attributes.except("id", "created_at", "updated_at", "line_id", "category_id"))

                  rule.save! unless rule.new_record?
                end
              end
            else
              lr.rules = []
            end

            lr.save!
          end
        end
      end

      @success = true
      @record = line.reload
    rescue ActiveRecord::RecordInvalid => e
      @success = false
      if e.record.is_a?(LineRule)
        @errors = e.record.errors.full_messages
      else
        @errors = ["#{e.record.component.name}"].concat(e.record.errors.full_messages)
      end
    end
  end
end