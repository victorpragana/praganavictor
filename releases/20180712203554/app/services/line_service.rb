class LineService
  attr_reader :success, :errors, :record

  def initialize(parameters = {})
    @parameters = parameters

    @parameters['cst'] = @parameters['cst'].to_i if @parameters['cst'].present?
    @dimensions = []
  end

  def create
    ActiveRecord::Base.transaction do
      line = Line.new(@parameters.slice('name', 'family_id', 'sale_price', 'horizontal_increment', 'vertical_increment', 'finished'))

      save_record(line)
    end
  end

  def update(line_id)
    begin
      ActiveRecord::Base.transaction do
        line = Line.includes(:line_rules, rules: [component: [values: [:specification]]]).find(line_id)

        line.assign_attributes(@parameters.slice('name', 'family_id', 'sale_price', 'horizontal_increment', 'vertical_increment',
          'erp_categories', 'ncm', 'cst', 'finished'))

        save_record(line)
      end
    rescue ActiveRecord::RecordNotSaved => e
      @success = false

      if !@dimensions.empty?
        @errors ||= []

        @dimensions.each do |dim|
          @errors << dim.errors.full_messages if !dim.errors.empty?
        end

        @errors = @errors.flatten
      end
    end
  end

  def duplicate(line_id)
    line = Line.includes(:line_rules, rules: [component: [values: [:specification]]]).find(line_id)

    begin
      ActiveRecord::Base.transaction do
        new_line = line.deep_clone(include: [:dimensions, :categories]) do |original, clone|
          if clone.is_a?(Line)
            clone.name = original.name.concat(" (clone)")
          end
        end

        new_line.save!

        line.line_rules.each do |line_rule|
          new_line_rule = line_rule.deep_clone do |original, clone|
            clone.line = new_line
          end

          new_line_rule.save!

          line_rule.rules.each do |rule|
            new_rule = rule.deep_clone do |original, clone|
              clone.line_rule = new_line_rule
            end

            new_rule.save!
          end
        end

        @success = true
        @record = new_line.reload
      end
    rescue ActiveRecord::RecordInvalid => e
      @success = false
      @errors = e.record.errors.full_messages
    rescue => e
      @success = false
      @errors = [e.message]
    end
  end

  def inactivate(line_id)
    line = Line.find(line_id)

    line.update_attribute(:active, false)

    @success = true
    @record = line.reload
  end

  def activate(line_id)
    line = Line.find(line_id)

    line.update_attribute(:active, true)

    @success = true
    @record = line.reload
  end

  private
  def set_dimensions(line)
    if @parameters['dimensions'].present?
      @dimensions = @parameters['dimensions'].map do |dimension|
        Dimension.new.tap do |dim|
          dim.width  = dimension['initial_width'].to_i..dimension['final_width'].to_i
          dim.height = dimension['initial_height'].to_i..dimension['final_height'].to_i
          dim.weight = dimension['weight'].to_i
        end
      end

      line.dimensions = @dimensions
    end
  end

  def set_categories(line)
    if @parameters['categories'].present?
      line.categories.where.not(category_id: @parameters['categories'].map{|c| c['category'].to_i}).destroy_all

      line.categories = @parameters['categories'].each_with_index.map do |category, index|
        line.categories.find_or_initialize_by(category_id: category['category'].to_i).tap do |lc|
          lc.required = category['required']
          lc.order = index

          lc.save! unless lc.new_record?
        end
      end
    end
  end

  def save_record(line)
    set_dimensions(line)
    set_categories(line)

    if line.save
      @success = true
      @record = line.reload
    else
      @success = false
      @errors = line.errors.full_messages
    end
  end
end