class AutomaticComponentCalculatorService < BaseCalculatorService
  validate :non_automatic_component?, unless: 'component_id.blank?'
  validate :not_erasable_component?, unless: 'component_id.blank?'

  attr_reader :packing, :option_id

  def initialize(line_id, component_id, width, height, option_id = nil, packing = nil, questions = {})
    @packing = packing
    @option_id = option_id

    super(line_id, component_id, width, height, nil, questions)
  end

  def quantity_consumption
    return 0 if rule.nil?
    return 1 if rule.quantity_consumption.blank?

    return calculator.evaluate(rule.quantity_consumption, user_questions({
      '_LARGURA_' => converted_value(:width),
      '_ALTURA_' => converted_value(:height), 
      '_AREA_' => converted_value(:width) * converted_value(:height)
    }))
  end

  def total_consumption
    return 0 if !valid?

    return unit_dimension if !component.un?

    return (pack_dimension / unit_dimension).round(2)
  end

  def cost(total_cost)
    if !packing.blank?
      return 0 if rule.nil?

      return total_cost * relative_area + (leftover_cost(total_cost) * relative_util_area)
    end
  end

  def price(total_cost)
    if !packing.blank?
      return 0 if rule.nil?

      return cost(total_cost) * line_markup
    end
  end

  def total(total_cost)
    return (price(total_cost) * quantity_consumption).round(2)
  end

  def packing_for_option
    @packing_for_option ||= packing.packings.find do |pack|
      pack.find do |item|
        item.id == option_id
      end
    end
  end

  def pack_dimension
    rule.height? ? packing_for_option.used_width : packing_for_option.used_height
  end

  def component_dimension
    component.automatic_value * component.conversion_factor
  end

  private

  def non_automatic_component?
    errors.add(:base, I18n.t('activemodel.errors.models.automatic_component_calculator_service.non_automatic_component')) unless component.automatic?
  end

  def not_erasable_component?
    errors.add(:base, I18n.t('activemodel.errors.models.automatic_component_calculator_service.not_erasable_component')) unless component.erasable?
  end

  def area
    return width_consumption * height_consumption if !width_consumption.to_f.zero? && !height_consumption.to_f.zero?
    return converted_value(:width) * converted_value(:height)
  end

  def relative_area
    area.to_f / packing_area.to_f
  end

  def relative_leftover_area
    remaining_area.to_f / packing_area.to_f
  end

  def relative_util_area
    area.to_f / (packing_area - remaining_area).to_f
  end

  def leftover_cost(total_cost)
    total_cost * relative_leftover_area
  end

  def packing_area
    @packing_area ||= packing.packings.reduce(0) do |total, pack|
      if rule.height?
        pack_area = (packing.height.y * pack.used_width)

        total = total + pack_area
      elsif rule.width?
        pack_area = (packing.width.x * pack.used_height)

        total = total + pack_area
      end
    end
  end

  def items_area
    @items_area ||= packing.packings.inject(0) do |total_area, pack|
      _area = pack.reduce(0) do |total, item|
        total = total + item.area
      end

      total_area = total_area + _area
    end
  end

  def remaining_area
    @remaining_area ||= packing_area.to_f - items_area
  end
end