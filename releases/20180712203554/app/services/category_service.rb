class CategoryService
  attr_reader :success, :errors, :record

  def initialize(parameters)
    @parameters = parameters
  end

  def update(category_id)
    category = Category.find(category_id)

    category.assign_attributes(@parameters)

    save_record(category)
  end

  def create
    category = Category.new(@parameters)

    save_record(category)
  end

  private
  def save_record(category)
    if category.save
      @success = true
      @record = category.reload
    else
      @success = false
      @errors = category.errors.full_messages
    end
  end
end