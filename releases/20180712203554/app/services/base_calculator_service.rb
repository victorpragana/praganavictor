class BaseCalculatorService
  include ActiveModel::Validations
  extend ActiveModel::Translation

  attr_reader :line_id, :component_id, :width, :height, :rule_id

  validates_presence_of :line_id, :component_id, allow_blank: false
  validates_numericality_of :width, :height, greater_than: 0, allow_blank: false
  validate :component_has_rule_for_line?, unless: "component_id.blank? || line_id.blank?"
  validate :component_category_belongs_to_line?, unless: "component_id.blank? || line_id.blank?"

  def initialize(line_id, component_id, width, height, rule_id = nil, questions = {})
    @line_id, @component_id, @width, @height, @rule_id = line_id, component_id, width, height, rule_id
    @questions = questions
  end

  def width_consumption
    return 0 if rule.nil?

    return calculator.evaluate(rule.width_consumption, user_questions({
      '_LARGURA_' => converted_value(:width),
      '_ALTURA_' => converted_value(:height),
      '_AREA_' => converted_value(:width) * converted_value(:height)
    }))
  end

  def height_consumption
    return 0 if rule.nil?

    return calculator.evaluate(rule.height_consumption, user_questions({
      '_LARGURA_' => converted_value(:width),
      '_ALTURA_' => converted_value(:height),
      '_AREA_' => converted_value(:width) * converted_value(:height)
    }))
  end

  protected

  def unit_dimension
    if rule.height?
      (width_consumption || 0).zero? ? converted_value(:width) : width_consumption
    else
      (height_consumption || 0).zero? ? converted_value(:height) : height_consumption
    end
  end

  def line
    @line ||= Line.find(@line_id)
  end

  def component
    @component ||= Component.find(@component_id)
  end

  def rule
    if @rule_id.blank?
      @rule ||= Rule.joins(:line_rule, :component).where(component_id: component.id, line_rules: {line_id: @line_id}).first
    else
      @rule ||= Rule.joins(:line_rule, :component).where(component_id: component.id, id: @rule_id).first
    end
  end

  def calculator
    Dentaku::Calculator.new
  end

  def component_has_rule_for_line?
    return unless component.erasable?

    errors.add(:base, I18n.t("activemodel.errors.models.component_calculator_service.component_does_not_have_rule")) if rule.blank?
  end

  def component_category_belongs_to_line?
    line_categories = line.categories.pluck(:category_id)

    errors.add(:base, I18n.t("activemodel.errors.models.component_calculator_service.component_category_does_not_belong_to_line")) unless line_categories.include?(component.category_id)
  end

  def converted_value(value)
    send(value) * component.conversion_factor
  end

  def line_markup
    (1 + (line.sale_price / 100))
  end

  def questions
    return nil if @questions.nil? || @questions.empty?

    @questions.each_with_object({}) do |elem, hash|
      question = line.questions.find_by(value: elem[0])

      hash[question.as_variable] = elem[1] if question
    end
  end

  def user_questions(hash)
    return hash if questions.nil?

    hash.merge(questions)
  end
end