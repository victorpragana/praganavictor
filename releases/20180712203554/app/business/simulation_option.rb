class SimulationOption
  include ActiveModel::Validations
  extend ActiveModel::Translation

  attr_reader :line_id, :width, :height, :questions

  validates_presence_of :line_id, allow_blank: false
  validates_numericality_of :width, :height, only_integer: true, greater_than: 0, allow_blank: false

  def initialize(line_id, width, height, questions = {})
    @line_id, @width, @height = line_id, width.to_i, height.to_i

    @questions = questions
  end

  def line
    @line ||= Line.find(line_id)
  end

  def id
    @id ||= SecureRandom.uuid
  end
end