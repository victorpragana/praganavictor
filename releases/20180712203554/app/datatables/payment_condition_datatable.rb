class PaymentConditionDatatable < AjaxDatatablesRails::Base
  def sortable_columns
    @sortable_columns ||= %w{PaymentCondition.created_at PaymentCondition.type PaymentCondition.installments PaymentCondition.additional_tax PaymentCondition.credit_tax PaymentCondition.antecipation_tax PaymentCondition.credit_antecipation_tax }
  end

  def searchable_columns
    @searchable_columns ||= %w{PaymentCondition.created_at PaymentCondition.type PaymentCondition.installments PaymentCondition.additional_tax PaymentCondition.credit_tax PaymentCondition.antecipation_tax PaymentCondition.credit_antecipation_tax }
  end

  private

  def data
    records.map do |record|
      [
        record.created_at.strftime('%d/%m/%Y'),
        PaymentCondition.types[record.type],
        record.installments,
        record.additional_tax,
        record.credit_tax,
        record.antecipation_tax,
        record.credit_antecipation_tax,
        record.id
      ]
    end
  end

  def get_raw_records
    PaymentCondition.all
  end
end
