class ComponentDatatable < AjaxDatatablesRails::Base
  def sortable_columns
    @sortable_columns ||= %w{Component.id Category.name Component.name Family.name Component.cost Component.unit}
  end

  def searchable_columns
    @searchable_columns ||= %w{Component.id Category.name Component.name Family.name}
  end

  private

  def data
    records.map do |record|
      [
        "",
        record.id,
        record.category.name,
        record.name,
        record.families.map do |family|
          { name: family.name, color: family.color }
        end,
        record.cost.to_f,
        Component.units[record.unit]
      ]
    end
  end

  def get_raw_records
    scope = Component.select('components.*, categories.name as category_name').joins("LEFT JOIN components_families ON components_families.component_id = components.id 
      LEFT JOIN families ON families.id = components_families.family_id LEFT JOIN categories ON categories.id = components.category_id")
      .includes(:families, :category).user_components.distinct
    

    scope = scope.only_deleted if show_deleted?

    scope
  end

  def show_deleted?
    [true, 'true'].include?(params[:show_deleted])
  end
end
