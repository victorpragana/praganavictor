class DistributorDatatable < AjaxDatatablesRails::Base
  def_delegator :@view, :current_seller

  def sortable_columns
    @sortable_columns ||= %w{Distributor.id ThirdParty.name ThirdParty.identification ThirdParty.phone ThirdParty.subdomain CommercialClassification.name}
  end

  def searchable_columns
    @searchable_columns ||= %w{Distributor.id ThirdParty.name ThirdParty.identification ThirdParty.phone ThirdParty.subdomain CommercialClassification.name}
  end

  private

  def data
    records.map do |record|
      [
        "",
        record.id,
        record.name,
        record.formated_identification,
        record.phone,
        record.subdomain,
        record.price_table_for(current_seller.workplace).try(:name),
        record.price_table_for(current_seller.workplace).try(:id),
        record.items_price_table_for(current_seller.workplace).try(:name),
        record.items_price_table_for(current_seller.workplace).try(:id),
        record.commercial_classification.try(:name),
        record.commercial_classification.try(:id)
      ]
    end
  end

  def get_raw_records
    Distributor.joins(:third_party).joins('LEFT JOIN commercial_classifications ON commercial_classifications.id = distributors.commercial_classification_id').all
  end
end
