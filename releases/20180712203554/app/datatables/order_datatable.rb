class OrderDatatable < AjaxDatatablesRails::Base
  def_delegator :@view, :current_worker

  def sortable_columns
    @sortable_columns ||= %w{Order.date Order.id ThirdParty.name Estimate.id}
  end

  def searchable_columns
    @searchable_columns ||= %w{Order.date Order.id ThirdParty.name Estimate.id}
  end

  private

  def data
    records.map do |record|
      [
        record.date.strftime('%d/%m/%Y'),
        record.id,
        record.client.name,
        record.total.to_f,
        record.estimate.try(:id),
        record.estimate.try(:express?),
        record.total_with_special_discount.to_f,
        record.has_payments?
      ]
    end
  end

  def get_raw_records
    query = current_worker.workplace.orders.includes(:estimate).joins(:estimate)
      .joins("LEFT JOIN distributors ON distributors.id = estimates.client_id AND estimates.client_type = 'Distributor'")
      .joins("LEFT JOIN stores ON stores.id = estimates.client_id AND estimates.client_type = 'Store'")
      .joins("LEFT JOIN customers ON customers.id = estimates.client_id AND estimates.client_type = 'Customer'")
      .joins(
        "INNER JOIN third_parties ON third_parties.actable_id = distributors.id AND third_parties.actable_type = 'Distributor' OR \
        third_parties.actable_id = stores.id AND third_parties.actable_type = 'Store' OR \
        third_parties.actable_id = customers.id AND third_parties.actable_type = 'Customer'"
      )
    
    query = query.where(user_id: current_worker.id) if current_worker.is_a? Seller
    
    query
  end
end