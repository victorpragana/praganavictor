class QuestionDatatable < AjaxDatatablesRails::Base
  def sortable_columns
    @sortable_columns ||= %w{Question.id Question.label Question.value Question.type}
  end

  def searchable_columns
    @searchable_columns ||= %w{Question.id Question.label Question.value Question.type}
  end

  private

  def data
    records.map do |record|
      [
        record.id,
        record.label,
        record.value,
        Question.types[record.type]
      ]
    end
  end

  def get_raw_records
    Question.all
  end
end
