class EstimateDatatable < AjaxDatatablesRails::Base
  def_delegator :@view, :current_seller

  def sortable_columns
    @sortable_columns ||= %w{Estimate.date Estimate.id Seller.name Client.name Estimate.order_id}
  end

  def searchable_columns
    @searchable_columns ||= %w{Estimate.date Estimate.id Seller.name Client.name Estimate.order_id}
  end

  private

  def data
    records.map do |record|
      [
        record.date.strftime("%d/%m/%Y"),
        record.id,
        record.seller.name,
        record.client.name,
        record.total_with_discount.to_f,
        record.order_id
      ]
    end
  end

  def new_search_condition(column, value)
    model, column = column.split('.')

    if model == 'Seller'
      casted_column = ::Arel::Nodes::NamedFunction.new('CAST', [ThirdParty.arel_table.alias('sellers')[column.to_sym].as(typecast)])
      casted_column.matches("%#{value}%")
    elsif model == 'Client'
      casted_column = ::Arel::Nodes::NamedFunction.new('CAST', [ThirdParty.arel_table.alias('clients')[column.to_sym].as(typecast)])
      casted_column.matches("%#{value}%")
    else
      model = model.constantize

      casted_column = ::Arel::Nodes::NamedFunction.new('CAST', [model.arel_table[column.to_sym].as(typecast)])
      casted_column.matches("%#{value}%")
    end
  end

  def get_raw_records
    current_seller.workplace.estimates
      .joins('INNER JOIN third_parties clients ON estimates.client_id = clients.actable_id')
      .joins('INNER JOIN third_parties sellers ON estimates.seller_id = sellers.actable_id')
      .distinct
  end
end
