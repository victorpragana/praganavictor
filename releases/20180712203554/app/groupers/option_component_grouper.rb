class OptionComponentGrouper
  include ActiveModel::Validations
  extend ActiveModel::Translation

  attr_reader :component_id, :options, :components, :possible_options

  validates_presence_of :component_id, allow_blank: false
  validates_presence_of :possible_options, allow_blank: false
  validate :component_not_deleted, if: Proc.new { |grouper| grouper.component_id.present? }
  validate :components_with_same_control_method?, unless: 'possible_options.blank? || component_id.blank? || !component.automatic?'

  def initialize(component_id, possible_options, optimize)
    @component_id, @possible_options, @optimize = component_id, possible_options, optimize
  end

  def group
    return false unless valid?

    return group_non_automatic_component unless component.automatic?
    return group_automatic_components
  end

  private

  def component_not_deleted
    errors.add(:base, I18n.t('activemodel.errors.models.option_component_grouper.deleted_component', name: component.name)) if Component.only_deleted.exists?(id: component_id)
  end

  def components_with_same_control_method?
    component_ids = Component.select(:id).joins(:components_option_components)
      .where(components_option_components: { option_component_id: possible_components.map(&:id) }).distinct.pluck(:id)

    errors.add(:base, I18n.t('activemodel.errors.models.option_component_grouper.components_with_different_control_method')) if Rule.select(:control_method).joins(:line_rule, :component).where(component_id: component_ids, line_rules: {line_id: possible_options.select(:line_id).pluck(:line_id).uniq}).pluck(:control_method).uniq.size > 1
  end

  def component
    @component ||= Component.with_deleted.find(component_id)
  end

  def group_non_automatic_component
    cache_key = Digest::MD5.hexdigest("grouping_for_#{component_id}_#{possible_option_ids}")

    cached_grouping = Rails.cache.fetch(cache_key)

    if cached_grouping.nil?
      @components = [component_id]
      @options = possible_options.joins(:components).where(option_components: {component_id: component_id})

      Rails.cache.write(cache_key, { components: @components, options: @options }, expires_in: 1.minute) unless Rails.env.test?
    else
      @components = cached_grouping[:components]
      @options = cached_grouping[:options]
    end

    return true
  end

  def group_automatic_components
    components = possible_components

    @components = if @optimize
      OptionComponent.joins(:possible_components).where(id: components.pluck(:id)).pluck("DISTINCT components_option_components.component_id")
    else
      option_components_count = components.count

      OptionComponent.joins(:possible_components).where(id: components.pluck(:id)).pluck("DISTINCT components_option_components.component_id").map do |_component_id|
        _component_id if OptionComponent.joins(:possible_components).where(id: components.pluck(:id)).where(components_option_components: { component_id: _component_id }).count == option_components_count
      end.compact
    end

    @options = possible_options.joins(components: [:possible_components]).where(components_option_components: { component_id: @components }).select("DISTINCT ON (options.id) options.*")

    return true
  end

  def possible_components
    option_component_ids = OptionComponent.joins(:component).includes(:component).where(option_id: possible_option_ids, components: { category_id: component.category_id }).pluck(:id)
    component_ids = OptionComponent.where(id: option_component_ids).pluck(:component_id)
    automatic_component_ids = Component.joins(values: :specification).where(components_values: { component_id: component_ids}).where(specifications: { automatic: true }).pluck(:id)

    ids = OptionComponent.joins(:possible_components).where(components_option_components: { component_id: automatic_component_ids }).where(id: option_component_ids).pluck(:id)

    OptionComponent.joins(:possible_components).where(id: ids).where(components_option_components: { component_id: component_id})
  end

  def possible_option_ids
    @possible_option_ids ||= possible_options.pluck(:id)
  end
end