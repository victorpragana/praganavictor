class ClientsController < ApplicationController
  include VerifyWorkplace

  before_action :authenticate_seller!
  before_action {|c| c.ensure_workplace_is(Factory, Distributor, Store)}

  def index
    @clients = current_seller.workplace.clients
  end
end
