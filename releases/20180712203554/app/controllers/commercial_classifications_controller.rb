# == Schema Information
#
# Table name: commercial_classifications
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class CommercialClassificationsController < ApplicationController
  before_action :authenticate_administrator!
  before_action :load_commercial_classification, only: [:destroy, :show]

  def index
    if params['columns'].present?
      render json: CommercialClassificationDatatable.new(view_context)
    else
      @commercial_classifications = CommercialClassification.all.order('name ASC')
    end
  end

  def create
    service = CommercialClassificationService.new(commercial_classification_params)

    service.create

    if service.success
      @commercial_classification = service.record

      head :created
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def show
  end

  def update
    service = CommercialClassificationService.new(commercial_classification_params)

    service.update(params[:id])

    if service.success
      @commercial_classification = service.record

      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @commercial_classification.destroy
      head :no_content
    else
      render json: { errors: @commercial_classification.errors.full_messages }, status: :bad_request
    end
  end

  private

  def commercial_classification_params
    params.permit(:name, :line_ids, line_ids: [])
  end

  def load_commercial_classification
    @commercial_classification = CommercialClassification.find(params[:id])
  end
end
