class ErpController < ApplicationController
  before_action :authenticate_administrator!

  def sellers
    @sellers = []

    erp_response = erp_client.sellers.list

    if erp_response.status == 200
      @sellers = erp_response.payload.reject{|s| s['ativo'] != true}.sort_by{|s| s['nome']}
    end
  end

  def category_levels
    @category_levels = []

    erp_response = erp_client.category_levels.list

    if erp_response.status == 200
      @category_levels = erp_response.payload.reject{|s| s['ativo'] != true}.sort_by{|s| s['nivel']}
    end
  end

  def product_categories
    @product_categories = []

    erp_response = erp_client.product_categories.list

    if erp_response.status == 200
      @product_categories = erp_response.payload.reject{|s| s['ativo'] != true}.sort_by{|s| s['nivel']}
    end
  end

  def payment_conditions
    @payment_conditions = []

    erp_response = erp_client.payment_conditions.list

    if erp_response.status == 200
      @payment_conditions = erp_response.payload.reject{|s| s['ativo'] != true}.sort_by{|s| s['descricao']}
    end
  end

  private
  def erp_client
    Varejonline.new(Rails.application.secrets.varejonline["access_token"])
  end
end
