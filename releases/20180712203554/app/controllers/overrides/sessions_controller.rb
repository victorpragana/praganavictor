module Overrides
  class SessionsController < DeviseTokenAuth::SessionsController
    include ParseSubdomain

    def create
      # Check
      field = (resource_params.keys.map(&:to_sym) & resource_class.authentication_keys).first

      @resource = nil
      if field
        q_value = resource_params[field]

        if resource_class.case_insensitive_keys.include?(field)
          q_value.downcase!
        end

        q = "#{field.to_s} = ? AND provider='email'"

        if ActiveRecord::Base.connection.adapter_name.downcase.starts_with? 'mysql'
          q = "BINARY " + q
        end

        @resource = resource_class.where(q, q_value).first
      end

      if @resource and valid_params?(field, q_value) and @resource.valid_password?(resource_params[:password]) and (!@resource.respond_to?(:active_for_authentication?) or @resource.active_for_authentication?)
        @user_workplace_by_subdomain = @resource.user_workplace_by_subdomain(subdomain)

        if  !@user_workplace_by_subdomain
          return render_create_error_invalid_subdomain
        end

        
        
        @resource.update(selected_workplace_id: @user_workplace_by_subdomain.id)
        @resource.reload
        # create client id
        @client_id = SecureRandom.urlsafe_base64(nil, false)
        @token     = SecureRandom.urlsafe_base64(nil, false)

        @resource.tokens[@client_id] = {
          token: BCrypt::Password.create(@token),
          expiry: (Time.now + DeviseTokenAuth.token_lifespan).to_i
        }
        @resource.save

        sign_in(:user, @resource, store: false, bypass: false)

        yield if block_given?

        render_create_success
      elsif @resource and not (!@resource.respond_to?(:active_for_authentication?) or @resource.active_for_authentication?)
        render_create_error_not_confirmed
      else
        render_create_error_bad_credentials
      end
    end
    protected
      def render_create_error_invalid_subdomain
        render json: {
          errors: ["Você não tem permissão para acessar este subdomínio"]
        }, status: 401
      end
    
      def render_create_success
        render json: {
          data: @resource.token_validation_response.merge({
            client_id: Rails.application.secrets.varejonline['client_id'],
            redirect_uri: Rails.application.secrets.varejonline['redirect_uri'],
            type: @resource.type,
            workplace: { type: @resource.workplace.actable_type, name: @resource.workplace.name },
            workplaces: @resource.workplaces.map { |w| {id: w.id, type: w.workplace_type, name: w.workplace.name} }
          })
        }
      end
  end
end