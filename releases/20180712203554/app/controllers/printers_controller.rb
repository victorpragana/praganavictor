# == Schema Information
#
# Table name: printers
#
#  id          :integer          not null, primary key
#  name        :string           not null
#  real_name   :string           not null
#  server_name :string           not null
#  server_id   :string           not null
#  default     :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class PrintersController < ApplicationController
  include VerifyWorkplace

  before_action :authenticate_administrator!
  before_action { |c| c.ensure_workplace_is(Factory) }
  before_filter :load_printer, only: [:show, :destroy]

  def index
    render json: PrinterDatatable.new(view_context)
  end

  def create
    service = PrinterService.new(printer_params)

    service.create

    if service.success
      head :created
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def update
    service = PrinterService.new(printer_params)

    service.update(params[:id])

    if service.success
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def show
  end

  def destroy
    if @printer.destroy
      head :no_content
    else
      render json: { errors: @printer.errors.full_messages }, status: :bad_request
    end
  end

  private

  def load_printer
    @printer = Printer.find(params[:id])
  end

  def printer_params
    params.permit(:name, :real_name, :server_name, :server_id, :default)
  end
end
