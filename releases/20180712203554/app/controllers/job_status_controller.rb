class JobStatusController < ApplicationController
  include VerifyWorkplace

  before_action :authenticate_seller!
  before_action {|c| c.ensure_workplace_is(Factory, Distributor, Store)}

  def show
    begin
      container = SidekiqStatus::Container.load(params[:id])

      if SidekiqStatus::Container::FINISHED_STATUS_NAMES.include?(container.status)
        if container.killed?
          estimate = Estimate.find_by_id(container.args.first)

          estimate.free! if estimate
        end

        render json: { job_status: { in_progress: false, message: container.payload } }, status: :ok
      else
        render json: { job_status: { in_progress: true, at: container.at, total: container.total, pct_complete: container.pct_complete, message: container.message, cancelling: container.kill_requested?} }, status: :ok
      end
    rescue SidekiqStatus::Container::StatusNotFound => e
      render json: { job_status: { in_progress: false } }, status: :not_found
    rescue => e
      render json: { job_status: { in_progress: false, errors: [e.message] } }, status: :bad_request
    end
  end

  def destroy
    begin
      container = SidekiqStatus::Container.load(params[:id])

      container.request_kill if container.killable?

      head :no_content
    rescue SidekiqStatus::Container::StatusNotFound => e
      head :no_content
    rescue => e
      render json: { job_status: { in_progress: false, errors: [e.message] } }, status: :bad_request
    end
  end
end
