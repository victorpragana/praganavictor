class ApplicationController < ActionController::API
  include ActionController::Helpers
  include ActionController::Caching
  include DeviseTokenAuth::Concerns::SetUserByToken
  include ParseSubdomain
  
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_paper_trail_whodunnit
  before_action :change_workplace_by_subdomain

  devise_token_auth_group :worker, contains: [:administrator, :manager, :seller, :operator]
  devise_token_auth_group :seller, contains: [:administrator, :manager, :seller]

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << [:name, :username]
    devise_parameter_sanitizer.for(:sign_in) << [:username]
  end

  def change_workplace_by_subdomain
    if current_user && subdomain.present?
      @user_workplace_by_subdomain = current_user.user_workplace_by_subdomain(subdomain)

      if !@user_workplace_by_subdomain
        return render_create_error_invalid_subdomain
      else
        current_user.update(selected_workplace_id: @user_workplace_by_subdomain.id)
      end
    end
  end

  def render_create_error_invalid_subdomain
    render json: {
      errors: ["Você não tem permissão para acessar este subdomínio"]
    }, status: 401
  end
end
