# == Schema Information
#
# Table name: items
#
#  id             :integer          not null, primary key
#  code           :string
#  description    :string           not null
#  unit           :integer          not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  deleted_at     :datetime
#  type           :integer          default(0)
#  erp_categories :json
#  ncm            :string(8)
#  cst            :integer
#

class ItemsController < ApplicationController
  before_action :authenticate_administrator!, except: [:index, :show]
  before_action :authenticate_seller!, only: [:index, :show]
  before_filter :load_item, only: [:destroy, :update, :show]

  def index
    if params['columns'].present?
      render json: ItemDatatable.new(view_context)
    else
      @items = Item.all.order(:description)
    end
  end

  def create
    service = ItemService.new(item_params)

    service.create

    if service.success
      @item = service.record

      head :created
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def show
  end

  def update
    service = ItemService.new(item_params)

    service.update(params[:id])

    if service.success
      @item = service.record

      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @item.destroy
      head :no_content
    else
      render json: { errors: @item.errors.full_messages }, status: :bad_request
    end
  end

  def batch_destroy
    begin
      ActiveRecord::Base.transaction do
        params[:ids].each do |id|
          item = Item.find(id)

          item.destroy!
        end
      end

      head :no_content
    rescue => e
      render json: { errors: [e.message] }, status: :bad_request
    end
  end

  private

  def item_params
    params.permit(:code, :description, :unit, :type).tap do |parameters|
      parameters.merge!(erp_categories: params[:erp_categories]) if !params[:erp_categories].nil?
      parameters.merge!(ncm: params[:ncm])                       if !params[:ncm].nil?
      parameters.merge!(cst: params[:cst])                       if !params[:cst].nil?
    end
  end

  def load_item
    @item = Item.find(params[:id])
  end
end
