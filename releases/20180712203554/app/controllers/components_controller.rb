# == Schema Information
#
# Table name: components
#
#  id                        :integer          not null, primary key
#  name                      :string(255)      not null
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  cost                      :decimal(10, 5)   default(0.0), not null
#  erasable                  :boolean          default(TRUE)
#  category_id               :integer
#  characteristics           :text
#  code                      :string(20)
#  unit                      :integer          default(2), not null
#  deleted_at                :datetime
#  base_component_id         :integer
#  reference_cost            :decimal(10, 5)   default(0.0)
#  offset_left               :integer          default(0)
#  offset_top                :integer          default(0)
#  offset_right              :integer          default(0)
#  offset_bottom             :integer          default(0)
#  allow_rotation            :boolean          default(FALSE)
#  exchange                  :decimal(10, 5)
#  loss                      :decimal(10, 5)
#  cut_table_id              :integer
#  print_on_lines_label      :boolean          default(FALSE)
#  fix_cost                  :decimal(10, 5)   default(0.0)
#  print_on_components_label :boolean          default(FALSE)
#

class ComponentsController < ApplicationController
  include HasConversionToMm

  before_action :authenticate_administrator!
  before_filter :load_component, only: [:destroy, :really_destroy]

  def index
    if params['columns'].present?
      render json: ComponentDatatable.new(view_context)
    else
      scope = Component.select('components.*, categories.name as category_name').user_components.distinct
      
      scope = scope.joins("INNER JOIN components_components ON components_components.parent_id = components.id") if params['only_kit'] == 'true'
      if params['family_ids'].present?
        scope = scope.joins(:families, :category).where(families: {id: params['family_ids']})
      else
        scope = scope.joins("LEFT JOIN components_families ON components_families.component_id = components.id 
        LEFT JOIN families ON families.id = components_families.family_id LEFT JOIN categories ON categories.id = components.category_id")
      end

      @components = scope.order('categories.name ASC, components.name ASC')
    end
  end

  def create
    service = ComponentService.new(component_params)

    service.create

    if service.success
      @component = service.record

      render template: 'components/show', status: :created
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def show
    @component = Component.with_deleted.find(params[:id])
  end

  def update
    service = ComponentService.new(update_component_params)

    service.update(params[:id])

    if service.success
      @component = service.record

      render template: 'components/show', status: :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @component.destroy
      head :no_content
    else
      render json: { errors: @component.errors.full_messages }, status: :bad_request
    end
  end

  def really_destroy
    begin
      if @component.really_destroy!
        head :no_content
      else
        render json: { errors: @component.errors.full_messages }, status: :bad_request
      end
    rescue ActiveRecord::RecordNotDestroyed => e
      render json: { errors: e.record.errors.full_messages }, status: :bad_request
    end
  end

  def batch_destroy
    begin
      ActiveRecord::Base.transaction do
        params[:ids].each do |id|
          component = Component.find(id)

          component.destroy!
        end
      end

      head :no_content
    rescue ActiveRecord::RecordNotDestroyed => e
      render json: { errors: e.record.errors.full_messages }, status: :bad_request
    rescue => e
      render json: { errors: [e.message] }, status: :bad_request
    end
  end

  def select
    width = price_params['width'].to_i
    height = price_params['height'].to_i
    questions = price_params['questions']

    option = SimulationOption.new(params[:line_id], width, height, questions)

    selector = ComponentSelectionService.new(params[:component_ids], [option], false)

    if selector.perform
      @component = selector.component

      if @component.automatic?
        calculator = AutomaticComponentCalculatorService.new(params[:line_id], @component.id, width, height, option.id, selector.packing, option.questions)
        total_cost = selector.total_cost

        @component_select = ComponentSelect.new({
          total_consumption: calculator.total_consumption,
          width_consumption: calculator.width_consumption,
          height_consumption: calculator.height_consumption,
          quantity_consumption: calculator.quantity_consumption,
          cost: calculator.cost(total_cost),
          price: calculator.price(total_cost),
          total: calculator.total(total_cost),
          component: @component.name
        })
      else
        calculator = ComponentCalculatorService.new(params[:line_id], @component.id, width, height, params[:rule_id], option.questions)

        @component_select = ComponentSelect.new({
          total_consumption: calculator.total_consumption,
          width_consumption: calculator.width_consumption,
          height_consumption: calculator.height_consumption,
          quantity_consumption: calculator.quantity_consumption,
          cost: calculator.cost,
          price: calculator.price,
          total: calculator.total,
          component: @component.name
        })
      end

      unless @component_select.valid?
        render json: { errors: @component_select.errors.full_messages }, status: :unprocessable_entity
      end
    else
      render json: { errors: selector.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def export
    exporter = ComponentExporter.new

    response = exporter.export

    if response
      render json: { path: response }, status: :ok
    else
      render nothing: true, status: :bad_request
    end
  end

  def upload
    importer = ComponentImporter.new(params[:file])

    importer.import

    if importer.success
      render json: { success: importer.success, errors: importer.errors }, status: :ok
    else
      render json: { success: importer.success, errors: importer.errors }, status: :unprocessable_entity
    end
  end

  def clone_kit
    service = ComponentService.new(clone_kit_params)

    service.clone_kit(params[:id])

    if service.success
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def batch_insert
    service = ComponentService.new(batch_insert_params)

    service.create_rules(params[:id])

    if service.success
      render json: { errors: service.errors }, status: :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def activate
    service = ComponentService.new

    service.activate(params[:id])

    if service.success
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  private

  def price_params
    params.permit(:width, :height, :questions).tap do |parameters|
      convert_parameter_to_mm(parameters, :width)
      convert_parameter_to_mm(parameters, :height)

      parameters['questions'] = JSON.parse(parameters['questions']) if parameters['questions'].present?
    end
  end

  def component_params
    params.permit(:name, :control_method, :category_id, :unit, :cost, :reference_cost, :fix_cost, :code, :exchange, :loss,
      :offset_left, :offset_right, :offset_top, :offset_bottom, :characteristics, :allow_rotation, :print_on_lines_label, :print_on_components_label, :cut_table_id, family_ids: []).tap do |parameters|
      convert_parameter_to_mm(parameters, :offset_left)
      convert_parameter_to_mm(parameters, :offset_right)
      convert_parameter_to_mm(parameters, :offset_top)
      convert_parameter_to_mm(parameters, :offset_bottom)
    end
  end

  def update_component_params
    component_params.merge(params.permit(:child_ids, child_ids: []))
  end

  def clone_kit_params
    params.permit(:component_id)
  end

  def batch_insert_params
    params.permit(rule_ids: [])
  end

  def load_component
    @component = Component.with_deleted.find(params[:id])
  end
end
