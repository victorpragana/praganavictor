# == Schema Information
#
# Table name: families
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  color      :string(7)        not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class FamiliesController < ApplicationController
  before_action :authenticate_administrator!, except: [:index, :show]
  before_action :authenticate_seller!, only: [:index, :show]
  before_filter :load_families, only: [:index, :create, :update]

  def index
  end

  def show
    @family = Family.find(params[:id])
  end

  def create
    service = FamilyService.new(family_params)

    service.create

    if service.success
      @family = service.record

      render :template => "families/show", :status => :created
    else
      render :json => {:errors => service.errors}, :status => :unprocessable_entity
    end
  end

  def update
    service = FamilyService.new(family_params)

    service.update(params[:id])

    if service.success
      @family = service.record

      render :template => "families/show", :status => :ok
    else
      render :json => {:errors => service.errors}, :status => :unprocessable_entity
    end
  end

  private
  def family_params
    params.permit(:name, :color)
  end

  def load_families
    @families = Family.all.order(:name)
  end
end
