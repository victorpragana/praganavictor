# == Schema Information
#
# Table name: customers
#
#  id         :integer          not null, primary key
#  deleted_at :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  store_id   :integer
#

class CustomersController < ApplicationController
  include VerifyWorkplace

  before_action :authenticate_seller!
  before_action {|c| c.ensure_workplace_is(Factory, Store)}
  before_action :load_customer, only: [:destroy, :show]

  def index
    if params['columns'].present?
      render json: CustomerDatatable.new(view_context)
    else
      @customers = Customer.all
    end
  end

  def create
    service = CustomerService.new(customer_params)

    service.create

    if service.success
      head :created
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def show
  end

  def update
    service = CustomerService.new(customer_params)

    service.update(params[:id])

    if service.success
      @customer = service.record

      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @customer.destroy
      head :no_content
    else
      render json: { errors: @customer.errors.full_messages }, status: :bad_request
    end
  end

  def batch_destroy
    begin
      ActiveRecord::Base.transaction do
        params[:ids].each do |id|
          customer = current_seller.workplace.customers.find(id)

          customer.destroy!
        end
      end

      head :no_content
    rescue StandardError => e
      render json: { errors: [e.message] }, status: :bad_request
    end
  end

  def apply_price_table
    service = PriceTableService.new({'owner' => current_seller.workplace})

    service.apply_to_customers(params[:price_table_id], params[:ids])

    if service.success
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  private

  def load_customer
    @customer = current_seller.workplace.customers.find(params[:id])
  end

  def customer_params
    params.permit(:name, :identification, :zip_code, :street, :neighborhood, :number, :complement, :phone, :email, 
      :city_id, :corporate_name, :state_inscription, :municipal_inscription, :rg, :issuing_entity, :allow_special_discount, :special_discount).tap do |whitelisted|
      whitelisted.merge!({'store_id' => params[:store_id]})           if current_seller.workplace.is_a?(Factory)
      whitelisted.merge!({'store_id' => current_seller.workplace.id}) if current_seller.workplace.is_a?(Store)
    end
  end
end
