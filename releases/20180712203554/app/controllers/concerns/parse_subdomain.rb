module ParseSubdomain
  extend ActiveSupport::Concern

  included do
    protected

    def subdomain
      request.subdomain.split('.').try(:first)
    end
  end
end