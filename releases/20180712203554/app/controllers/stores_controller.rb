# == Schema Information
#
# Table name: stores
#
#  id                           :integer          not null, primary key
#  deleted_at                   :datetime
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  distributor_id               :integer
#  commercial_classification_id :integer
#

class StoresController < ApplicationController
  include VerifyWorkplace

  before_action :authenticate_seller!
  before_action {|c| c.ensure_workplace_is(Factory, Distributor)}
  before_action :load_store, only: [:destroy, :show]
  before_action :authenticate_administrator!, only: [:upload_logo]

  def index
    if params['columns'].present?
      render json: StoreDatatable.new(view_context)
    else
      @stores = Store.all
    end
  end

  def create
    service = StoreService.new(store_params)

    service.create

    if service.success
      render json: { store: {id: service.record.id} }, status: :created
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def show
  end

  def update
    service = StoreService.new(store_params)

    service.update(params[:id])

    if service.success
      @store = service.record

      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @store.destroy
      head :no_content
    else
      render json: { errors: @store.errors.full_messages }, status: :bad_request
    end
  end

  def batch_destroy
    begin
      ActiveRecord::Base.transaction do
        params[:ids].each do |id|
          store = current_seller.workplace.stores.find(id)

          store.destroy!
        end
      end

      head :no_content
    rescue => e
      render json: { errors: [e.message] }, status: :bad_request
    end
  end

  def apply_price_table
    service = PriceTableService.new({'owner' => current_seller.workplace})

    service.apply_to_stores(params[:price_table_id], params[:ids])

    if service.success
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def apply_commercial_classification
    service = CommercialClassificationService.new

    service.apply_to_stores(params[:commercial_classification_id], params[:ids])

    if service.success
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def upload_logo
    service = StoreService.new(logo: params[:file])

    service.update(params[:id])

    if service.success
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  private
  def load_store
    @store = current_seller.workplace.stores.find(params[:id])
  end

  def store_params
    params.permit(:name, :identification, :zip_code, :street, :neighborhood, :number, :complement, :phone, :email, :corporate_name, 
      :state_inscription, :municipal_inscription, :city_id, :allow_special_discount, :special_discount, :subdomain, :current_account, :agency, :bank).tap do |whitelisted|
      whitelisted.merge!({'distributor_id' => params[:distributor_id]})     if current_seller.workplace.is_a?(Factory)
      whitelisted.merge!({'distributor_id' => current_seller.workplace.id}) if current_seller.workplace.is_a?(Distributor)
    end
  end
end
