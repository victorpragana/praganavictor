# == Schema Information
#
# Table name: orders
#
#  id                     :integer          not null, primary key
#  date                   :date             not null
#  observation            :text
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  seller_id              :integer
#  seller_type            :string
#  client_id              :integer
#  client_type            :string
#  discount               :decimal(5, 2)    default(0.0)
#  status                 :integer          default(0), not null
#  erp_id                 :integer
#  processing_messages    :text             default([]), is an Array
#  user_id                :integer
#  billing_client_id      :integer
#  billing_client_type    :string
#  special_discount       :decimal(5, 2)
#  production_status      :integer          default(0), not null
#  sent_to_production_at  :datetime
#  started_production_at  :datetime
#  finished_production_at :datetime
#  dispatched_at          :datetime
#  received_at            :datetime
#  sent_to_factory_at     :datetime
#

class OrdersController < ApplicationController
  include VerifyWorkplace

  before_action :authenticate_seller!
  before_action { |c| c.ensure_workplace_is(Factory, Distributor, Store) }

  before_filter :load_order, only: %i[show send_to_erp cancel]

  def index
    render json: OrderDatatable.new(view_context)
  end

  def show; end

  def destroy
    service = OrderService.new

    service.destroy(params[:id])

    if service.success
      head :no_content
    else
      render json: { errors: service.errors }, status: :bad_request
    end
  end

  def generate
    service = GenerateOrderService.new(generate_order_params)

    service.perform

    if service.success
      @order = service.record

      render template: 'orders/show', status: :created
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def send_to_erp
    if @order.to_send? || @order.error?
      job_id = ErpWorker.perform_async(@order.id, params[:payment_condition_id])

      render json: { order: { job_id: job_id } }, status: :ok
    else
      error = if @order.sending?
        I18n.t('activerecord.errors.models.order.sending')
      else
        I18n.t('activerecord.errors.models.order.already_sent')
      end

      render json: { errors: [error] }, status: :bad_request
    end
  end

  def cancel
    if @order.sent?
      client = Varejonline.new(Rails.application.secrets.varejonline['access_token'])

      response = client.orders.cancel(@order.erp_id)

      if response.status == 200
        @order.to_send!

        head :no_content
      else
        render json: { errors: [response.payload] }, status: :bad_request
      end
    else
      render json: { errors: [I18n.t('activerecord.errors.models.order.only_sent')] }, status: :bad_request
    end
  end

  def change_billing_client
    service = OrderService.new(change_billing_client_params)

    service.change_billing_client(params[:id])

    if service.success
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def update_special_discount
    service = OrderService.new(update_special_discount_params)

    service.update_special_discount(params[:id])

    if service.success
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def lines_label
    service = OrderLabelService.new(params[:id], current_seller.id)

    service.lines_label

    if service.success
      head :ok
    else
      render nothing: true, status: :bad_request
    end
  end

  def components_label
    service = OrderLabelService.new(params[:id], current_seller.id)

    service.components_label

    if service.success
      head :ok
    else
      render nothing: true, status: :bad_request
    end
  end

  def cut_map
    service = OrderCutMapService.new(params[:id])

    response = service.generate

    if response
      render json: { path: response }, status: :ok
    else
      render nothing: true, status: :bad_request
    end
  end

  def production_status
    service = OrderService.new(update_production_status_params)

    service.update_production_status(params[:id])

    if service.success
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  private

  def generate_order_params
    params.permit(:estimate_id).merge('user_id' => current_seller.id)
  end

  def load_order
    @order = Order.find(params[:id])
  end

  def change_billing_client_params
    params.permit(:billing_client_id, :billing_client_type)
  end

  def update_special_discount_params
    params.permit(:special_discount)
  end

  def update_production_status_params
    params.permit(:transition)
  end
end
