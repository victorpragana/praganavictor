class ProductionOrdersController < ApplicationController
  include VerifyWorkplace

  before_action :authenticate_administrator!
  before_action {|c| c.ensure_workplace_is(Factory, Distributor, Store)}
  
  def index
    render json: ProductionOrderDatatable.new(view_context)
  end

  def purchase_planning
    service = PurchasePlanningService.new(params[:order_ids])

    response = service.generate

    if response
      render json: { path: response }, status: :ok
    else
      render nothing: true, status: :bad_request
    end
  end
end
