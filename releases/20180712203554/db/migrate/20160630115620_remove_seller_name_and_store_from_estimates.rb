class RemoveSellerNameAndStoreFromEstimates < ActiveRecord::Migration
  def change
    remove_column :estimates, :seller_name, :string
    remove_reference :estimates, :store, index: true, foreign_key: true
  end
end
