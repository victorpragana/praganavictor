class AddComponentNameAndUnitToOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :component_name, :string, :null => false, :limit => 255
    add_column :order_items, :unit, :integer, :null => false
  end
end
