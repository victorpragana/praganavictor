class RemoveVpsaTokenFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :vpsa_token, :string
  end
end
