class AddClientIdAndClientTypeToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :client_id, :integer
    add_index :orders, :client_id
    add_column :orders, :client_type, :string
    add_index :orders, :client_type
  end
end
