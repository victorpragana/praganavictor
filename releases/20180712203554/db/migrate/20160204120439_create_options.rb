class CreateOptions < ActiveRecord::Migration
  def change
    create_table :options do |t|
      t.belongs_to :line, index: true, foreign_key: true
      t.belongs_to :location, index: true, foreign_key: true
      t.integer :width, :null => false
      t.integer :height, :null => false

      t.timestamps null: false
    end
  end
end
