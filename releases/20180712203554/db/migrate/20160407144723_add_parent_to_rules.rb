class AddParentToRules < ActiveRecord::Migration
  def change
    add_column :rules, :parent_id, :integer 
    add_index :rules, :parent_id
    add_foreign_key :rules, :rules, column: :parent_id
  end
end
