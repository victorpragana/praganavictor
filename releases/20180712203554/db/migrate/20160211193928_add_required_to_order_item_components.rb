class AddRequiredToOrderItemComponents < ActiveRecord::Migration
  def change
    add_column :order_item_components, :required, :boolean, :default => false, :null => false
  end
end
