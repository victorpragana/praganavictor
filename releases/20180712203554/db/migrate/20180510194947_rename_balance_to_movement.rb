class RenameBalanceToMovement < ActiveRecord::Migration
  def change
    rename_table :balances, :movements
  end
end
