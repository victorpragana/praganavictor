class AddItemTypeToEstimateItems < ActiveRecord::Migration
  def change
    add_column :estimate_items, :type, :integer, default: 0
  end
end
