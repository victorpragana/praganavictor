class RemoveNameNullConstraintFromEnvironments < ActiveRecord::Migration
  def change
    change_column_null :environments, :name, true
  end
end
