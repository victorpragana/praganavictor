class AddCurrentAccountAgencyAndBankToThirdParties < ActiveRecord::Migration
  def change
    add_column :third_parties, :current_account, :string
    add_column :third_parties, :agency, :string
    add_column :third_parties, :bank, :integer, index: true
  end
end
