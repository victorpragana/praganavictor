class CreateRules < ActiveRecord::Migration
  def change
    create_table :rules do |t|
      t.belongs_to :line, index: true, foreign_key: true
      t.belongs_to :component, index: true, foreign_key: true
      t.numrange :width
      t.numrange :height
      t.integer :control_method, :null => false

      t.timestamps null: false
    end
  end
end
