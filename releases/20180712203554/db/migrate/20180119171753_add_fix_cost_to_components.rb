class AddFixCostToComponents < ActiveRecord::Migration
  def change
    add_column :components, :fix_cost, :decimal, precision: 10, scale: 5, default: 0
  end
end
