class RemovePaymentStatusFromOrders < ActiveRecord::Migration
  def change
    remove_column :orders, :payment_status, :integer
  end
end
