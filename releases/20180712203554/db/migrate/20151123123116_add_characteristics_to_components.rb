class AddCharacteristicsToComponents < ActiveRecord::Migration
  def change
    add_column :components, :characteristics, :text
  end
end
