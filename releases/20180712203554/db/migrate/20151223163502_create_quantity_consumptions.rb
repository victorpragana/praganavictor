class CreateQuantityConsumptions < ActiveRecord::Migration
  def change
    create_table :quantity_consumptions do |t|
      t.belongs_to :rule, index: true, foreign_key: true
      t.int4range :height, null: false
      t.int4range :width, null: false
      t.integer :quantity, null: false

      t.timestamps null: false
    end
  end
end
