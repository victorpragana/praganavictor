class AddWeightToRules < ActiveRecord::Migration
  def change
    add_column :rules, :weight, :integer, null: false, default: 0
    add_index :rules, :weight
  end
end
