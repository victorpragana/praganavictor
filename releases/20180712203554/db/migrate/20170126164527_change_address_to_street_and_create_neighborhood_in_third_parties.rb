class ChangeAddressToStreetAndCreateNeighborhoodInThirdParties < ActiveRecord::Migration
  def change
    rename_column :third_parties, :address, :street
    add_column :third_parties, :neighborhood, :string
  end
end
