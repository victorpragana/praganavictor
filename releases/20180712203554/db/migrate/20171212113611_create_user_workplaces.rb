class CreateUserWorkplaces < ActiveRecord::Migration
  def change
    create_table :user_workplaces do |t|
      t.references :user, index: true, foreign_key: true
      t.references :workplace, polymorphic: true, index: true

      t.timestamps null: false
    end

    UserWorkplace.reset_column_information

    User.find_each do |user|
      say "Adding workplace #{user.workplace_type} ##{user.workplace_id} to user #{user.username}"

      UserWorkplace.create!(user: user, workplace_id: user.workplace_id, workplace_type: user.workplace_type)
    end
  end
end
