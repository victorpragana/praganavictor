class ChangeStores < ActiveRecord::Migration
  def up
    change_table :stores do |t|
      t.remove :name
      t.remove :sale_price
    end
  end

  def down
    change_table :stores do |t|
      t.string :name, null: false
      t.decimal :sale_price, precision: 8, scale: 3, null: false, default: 0
    end
  end
end
