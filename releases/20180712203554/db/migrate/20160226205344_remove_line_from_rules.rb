class RemoveLineFromRules < ActiveRecord::Migration
  def change
    remove_column :rules, :line_id, :integer
  end
end
