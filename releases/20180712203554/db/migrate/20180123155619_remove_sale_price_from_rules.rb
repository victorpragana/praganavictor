class RemoveSalePriceFromRules < ActiveRecord::Migration
  def change
    remove_column :rules, :sale_price, :string, null: false
  end
end
