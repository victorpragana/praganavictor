class AddItemReferenceToEstimateItems < ActiveRecord::Migration
  def change
    add_reference :estimate_items, :item, index: true, foreign_key: true

    EstimateItem.reset_column_information

    EstimateItem.find_each do |ei|
      ei.item_id = Item.with_deleted.find_by_code(ei.item_code).id

      ei.save!
    end
  end
end
