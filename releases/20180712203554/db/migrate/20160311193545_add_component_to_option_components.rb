class AddComponentToOptionComponents < ActiveRecord::Migration
  def change
    add_reference :option_components, :component, index: true, foreign_key: true
  end
end
