class RemoveComponentFromOrderItems < ActiveRecord::Migration
  def change
    remove_reference :order_items, :component, index: true
  end
end
