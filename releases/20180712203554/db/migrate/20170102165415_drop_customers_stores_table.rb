class DropCustomersStoresTable < ActiveRecord::Migration
  def change
    drop_table :customers_stores, id: false do |t|
      t.references :customer, :store
    end
  end
end
