class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :name, null: false
      t.string :rg
      t.string :issuing_entity
      t.string :identification, null: false
      t.string :zip_code
      t.string :address, null: false
      t.string :number, null: false
      t.string :complement
      t.string :phone, null: false
      t.string :email
      t.string :corporate_name
      t.datetime :deleted_at, :index => true

      t.timestamps null: false
    end
  end
end
