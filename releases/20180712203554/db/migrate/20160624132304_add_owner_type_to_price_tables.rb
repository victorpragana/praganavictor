class AddOwnerTypeToPriceTables < ActiveRecord::Migration
  def change
    add_column :price_tables, :owner_type, :string
    add_index :price_tables, :owner_type
  end
end
