class RemoveTotalFromEnvironment < ActiveRecord::Migration
  def change
    remove_column :environments, :total, :decimal
  end
end
