class AddUnitToComponents < ActiveRecord::Migration
  def change
    add_column :components, :unit, :integer, null: false, default: Component.units[:un]
  end
end
