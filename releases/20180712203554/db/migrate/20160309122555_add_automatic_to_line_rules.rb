class AddAutomaticToLineRules < ActiveRecord::Migration
  def change
    add_column :line_rules, :automatic, :boolean, :default => false
  end
end
