class AddTotalsToEstimateItems < ActiveRecord::Migration
  def change
    add_column :estimate_items, :factory_value, :decimal, precision: 10, scale: 2, default: 0
    add_column :estimate_items, :distributor_value, :decimal, precision: 10, scale: 2, default: 0
    add_column :estimate_items, :store_value, :decimal, precision: 10, scale: 2, default: 0
    add_column :estimate_items, :customer_value, :decimal, precision: 10, scale: 2, default: 0
    remove_column :estimate_items, :value, :decimal, precision: 10, scale: 2
  end
end
