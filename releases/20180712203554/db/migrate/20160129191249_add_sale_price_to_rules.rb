class AddSalePriceToRules < ActiveRecord::Migration
  def change
    add_column :rules, :sale_price, :string, :default => "_PRECO_VENDA_", :null => false
  end
end