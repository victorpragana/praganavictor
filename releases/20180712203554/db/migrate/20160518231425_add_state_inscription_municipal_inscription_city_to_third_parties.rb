class AddStateInscriptionMunicipalInscriptionCityToThirdParties < ActiveRecord::Migration
  def change
    add_column :third_parties, :state_inscription, :string
    add_column :third_parties, :municipal_inscription, :string
    add_reference :third_parties, :city, index: true, foreign_key: true
  end
end
