class AddWeightToLineRules < ActiveRecord::Migration
  def change
    add_column :line_rules, :weight, :integer, null: false, default: 0
    add_index :line_rules, :weight
  end
end
