class CreateBalances < ActiveRecord::Migration
  def change
    create_table :balances do |t|
      t.references :third_party, index: true, null: false
      t.references :order, index: true
      t.references :payment, index: true
      t.decimal :value, :precision => 8, :scale => 2, null: false
      t.integer :flow_direction, null: false
      t.timestamps null: false
    end
  end
end
