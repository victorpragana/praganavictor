class AddFixCostToBaseComponents < ActiveRecord::Migration
  def change
    add_column :base_components, :fix_cost, :decimal, precision: 10, scale: 5, default: 0
  end
end
