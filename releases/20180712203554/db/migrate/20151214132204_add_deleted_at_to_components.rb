class AddDeletedAtToComponents < ActiveRecord::Migration
  def change
    add_column :components, :deleted_at, :datetime
    add_index :components, :deleted_at
  end
end
