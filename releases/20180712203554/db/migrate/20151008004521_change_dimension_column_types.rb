class ChangeDimensionColumnTypes < ActiveRecord::Migration
  def up
    change_column :dimensions, :width, "int4range USING null"
    change_column :dimensions, :height, "int4range USING null"
    change_column :dimensions, :vertical_increment, :integer
    change_column :dimensions, :horizontal_increment, :integer
  end

  def down
    change_column :dimensions, :width, "numrange USING null"
    change_column :dimensions, :height, "numrange USING null"
    change_column :dimensions, :vertical_increment, :decimal, :precision => 5, :scale => 2
    change_column :dimensions, :horizontal_increment, :decimal, :precision => 5, :scale => 2
  end
end
