class AddJobIdToEstimates < ActiveRecord::Migration
  def change
    add_column :estimates, :job_id, :string
  end
end
