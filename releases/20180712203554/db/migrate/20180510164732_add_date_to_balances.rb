class AddDateToBalances < ActiveRecord::Migration
  def change
    add_column :balances, :date, :date

    execute "UPDATE balances SET date = created_at;"

    change_column :balances, :date, :date, null: false
  end
end
