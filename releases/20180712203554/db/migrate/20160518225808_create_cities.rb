class CreateCities < ActiveRecord::Migration
  def change
    create_table :cities do |t|
      t.string :name, null: false
      t.belongs_to :state, index: true, foreign_key: true
      t.integer :ibge_code, null: false

      t.timestamps null: false
    end
  end
end
