class AddDescriptionToLineRules < ActiveRecord::Migration
  def change
    add_column :line_rules, :description, :string
  end
end
