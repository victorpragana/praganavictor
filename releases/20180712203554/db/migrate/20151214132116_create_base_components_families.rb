class CreateBaseComponentsFamilies < ActiveRecord::Migration
  def change
    create_table :base_components_families, :id => false do |t|
      t.references :base_component, :family
    end

    add_index :base_components_families, [:base_component_id, :family_id],
      name: "base_components_families_index",
      unique: true
  end
end
