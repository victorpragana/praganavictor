class CreateLineRules < ActiveRecord::Migration
  def change
    create_table :line_rules do |t|
      t.belongs_to :line, index: true, foreign_key: true
      t.int4range :width
      t.int4range :height
      t.integer :control_method, :null => false
      t.boolean :required, :null => false, :default => true
      t.string :cost, :null => false
      t.string :sale_price, :null => false
      t.string :height_consumption
      t.string :width_consumption
      t.string :total_consumption, :null => false

      t.timestamps null: false
    end
  end
end
