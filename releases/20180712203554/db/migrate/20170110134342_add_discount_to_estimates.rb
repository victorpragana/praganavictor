class AddDiscountToEstimates < ActiveRecord::Migration
  def change
    add_column :estimates, :discount, :decimal, precision: 5, scale: 2, default: 0
  end
end
