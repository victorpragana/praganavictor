class AddPrintOnLabelToComponents < ActiveRecord::Migration
  def change
    add_column :components, :print_on_label, :boolean, default: false
  end
end
