class RenamePrintOnLabelAndAddPrintOnLinesLabel < ActiveRecord::Migration
  def change
    rename_column :base_components, :print_on_label, :print_on_components_label
    rename_column :components, :print_on_label, :print_on_components_label
    add_column :base_components, :print_on_lines_label, :boolean, default: false
    add_column :components, :print_on_lines_label, :boolean, default: false
  end
end
