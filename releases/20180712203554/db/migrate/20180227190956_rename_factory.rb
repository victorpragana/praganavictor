class RenameFactory < ActiveRecord::Migration
  def up
    tp = ThirdParty.find_by(name: 'Fábrica Suzano', actable_type: 'Factory')

    if tp
      tp.update_column(:name, 'Fábrica')
      tp.update_column(:subdomain, 'fabrica')
    end
  end

  def down
    tp = ThirdParty.find_by(name: 'Fábrica', actable_type: 'Factory')

    if tp
      tp.update_column(:name, 'Fábrica Suzano')
      tp.update_column(:subdomain, 'fabrica-suzano')
    end
  end
end
