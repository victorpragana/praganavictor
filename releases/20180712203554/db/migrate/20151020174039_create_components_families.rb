class CreateComponentsFamilies < ActiveRecord::Migration
  def change
    create_table :components_families, :id => false do |t|
      t.references :component, :family
    end

    add_index :components_families, [:component_id, :family_id],
      name: "components_families_index",
      unique: true
  end
end
