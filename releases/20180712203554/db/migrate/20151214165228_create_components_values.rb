class CreateComponentsValues < ActiveRecord::Migration
  def change
    create_table :components_values, :id => false do |t|
      t.references :component, :value
    end

    add_index :components_values, [:component_id, :value_id],
      name: "components_values_index",
      unique: true
  end
end
