class AddTotalsToOrderLooseItems < ActiveRecord::Migration
  def change
    add_column :order_loose_items, :factory_value, :decimal, precision: 10, scale: 2, default: 0
    add_column :order_loose_items, :distributor_value, :decimal, precision: 10, scale: 2, default: 0
    add_column :order_loose_items, :store_value, :decimal, precision: 10, scale: 2, default: 0
    add_column :order_loose_items, :customer_value, :decimal, precision: 10, scale: 2, default: 0
    remove_column :order_loose_items, :value, :decimal, precision: 10, scale: 2
  end
end
