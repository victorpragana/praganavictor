class CreateLines < ActiveRecord::Migration
  def change
    create_table :lines do |t|
      t.string :name, :null => false, :limit => 255

      t.timestamps null: false
    end
  end
end
