class ChangeEstimateProductsToEstimateItems < ActiveRecord::Migration
  def up
    rename_table :estimate_products, :estimate_items
  end

  def down
    rename_table :estimate_items, :estimate_products
  end
end
