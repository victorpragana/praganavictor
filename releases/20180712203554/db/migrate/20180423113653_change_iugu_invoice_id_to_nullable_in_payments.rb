class ChangeIuguInvoiceIdToNullableInPayments < ActiveRecord::Migration
  def up
    change_column :payments, :iugu_invoice_id, :string, null: true
  end

  def down
    change_column :payments, :iugu_invoice_id, :string, null: false
  end
end
