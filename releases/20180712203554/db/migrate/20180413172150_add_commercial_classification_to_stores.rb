class AddCommercialClassificationToStores < ActiveRecord::Migration
  def change
    add_reference :stores, :commercial_classification, index: true, foreign_key: true
  end
end
