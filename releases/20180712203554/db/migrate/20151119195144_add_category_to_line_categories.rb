class AddCategoryToLineCategories < ActiveRecord::Migration
  def change
    remove_column :line_categories, :category
    
    add_reference :line_categories, :category, index: true, foreign_key: true
  end
end
