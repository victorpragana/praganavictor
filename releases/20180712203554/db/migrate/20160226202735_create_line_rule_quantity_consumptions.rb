class CreateLineRuleQuantityConsumptions < ActiveRecord::Migration
  def change
    create_table :line_rule_quantity_consumptions do |t|
      t.belongs_to :line_rule, index: true, foreign_key: true
      t.int4range :width, null: false
      t.int4range :height, null: false
      t.integer :quantity, null: false

      t.timestamps null: false
    end
  end
end
