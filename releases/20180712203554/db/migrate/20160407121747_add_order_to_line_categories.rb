class AddOrderToLineCategories < ActiveRecord::Migration
  def change
    add_column :line_categories, :order, :integer

    LineCategory.reset_column_information

    Line.find_each do |line|
      line.categories.each_with_index do |category, index|
        category.update_attribute(:order, index)
      end
    end

    change_column :line_categories, :order, :integer, :null => false
  end
end
