class CreateColorsComponents < ActiveRecord::Migration
  def change
    create_table :colors_components, :id => false do |t|
      t.references :color, :component
    end

    add_index :colors_components, [:color_id, :component_id],
      name: "colors_components_index",
      unique: true
  end
end
