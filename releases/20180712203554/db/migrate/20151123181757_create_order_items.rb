class CreateOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.belongs_to :component, index: true, foreign_key: true
      t.belongs_to :order, index: true, foreign_key: true
      t.decimal :consumption, :precision => 10, :scale => 3, :null => false
      t.decimal :unit_value, :precision => 10, :scale => 2, :null => false

      t.timestamps null: false
    end
  end
end
