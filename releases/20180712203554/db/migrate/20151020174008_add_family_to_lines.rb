class AddFamilyToLines < ActiveRecord::Migration
  def change
    add_reference :lines, :family, index: true, foreign_key: true
  end
end
