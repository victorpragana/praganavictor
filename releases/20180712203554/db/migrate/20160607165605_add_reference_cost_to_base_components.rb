class AddReferenceCostToBaseComponents < ActiveRecord::Migration
  def change
    add_column :base_components, :reference_cost, :decimal, precision: 8, scale: 3, default: 0
  end
end
