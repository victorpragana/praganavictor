class CreateStates < ActiveRecord::Migration
  def change
    create_table :states do |t|
      t.string :name, null: false
      t.string :acronym, null: false
      t.integer :ibge_code, null: false

      t.timestamps null: false
    end
  end
end
