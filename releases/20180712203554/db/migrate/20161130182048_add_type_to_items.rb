class AddTypeToItems < ActiveRecord::Migration
  def change
    add_column :items, :type, :integer, default: 0
  end
end
