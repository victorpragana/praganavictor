class RemoveSalePriceFromBaseComponents < ActiveRecord::Migration
  def change
    remove_column :base_components, :sale_price, :decimal, precision: 8, scale: 3, null: false, default: 0
  end
end
