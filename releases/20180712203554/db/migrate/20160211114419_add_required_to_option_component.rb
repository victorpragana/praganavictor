class AddRequiredToOptionComponent < ActiveRecord::Migration
  def change
    add_column :option_components, :required, :boolean, :default => false, :null => false
  end
end
