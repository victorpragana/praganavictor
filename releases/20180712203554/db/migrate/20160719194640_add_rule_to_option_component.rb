class AddRuleToOptionComponent < ActiveRecord::Migration
  def change
    add_reference :option_components, :rule, index: true, foreign_key: true
  end
end
