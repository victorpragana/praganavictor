class CreateCommercialClassificationsLines < ActiveRecord::Migration
  def change
    create_table :commercial_classifications_lines, :id => false do |t|
      t.references :commercial_classification, :line
    end

    add_index :commercial_classifications_lines, [:commercial_classification_id, :line_id],
      name: "commercial_classifications_lines_index",
      unique: true
  end
end
