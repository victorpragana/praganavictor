class RenameCustomersToThirdParties < ActiveRecord::Migration
  def change
    rename_table :customers, :third_parties
  end
end
