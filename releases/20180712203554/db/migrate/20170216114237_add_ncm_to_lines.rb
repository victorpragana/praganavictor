class AddNcmToLines < ActiveRecord::Migration
  def change
    add_column :lines, :ncm, :string, limit: 8
  end
end
