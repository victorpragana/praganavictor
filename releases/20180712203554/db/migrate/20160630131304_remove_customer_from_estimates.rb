class RemoveCustomerFromEstimates < ActiveRecord::Migration
  def change
    remove_reference :estimates, :customer, index: true, foreign_key: true
  end
end
