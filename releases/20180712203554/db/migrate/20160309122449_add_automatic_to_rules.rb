class AddAutomaticToRules < ActiveRecord::Migration
  def change
    add_column :rules, :automatic, :boolean, :default => false
  end
end
