class AddFinishedToLines < ActiveRecord::Migration
  def change
    add_column :lines, :finished, :boolean, default: false
  end
end
