class RenameFlowDirectionInMovements < ActiveRecord::Migration
  def change
    rename_column :movements, :flow_direction, :direction
  end
end
