class AddOptimizeToEstimates < ActiveRecord::Migration
  def change
    add_column :estimates, :optimize, :boolean, default: false
  end
end
