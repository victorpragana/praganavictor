class AddDeletedAtToValues < ActiveRecord::Migration
  def change
    add_column :values, :deleted_at, :datetime
    add_index :values, :deleted_at
  end
end
