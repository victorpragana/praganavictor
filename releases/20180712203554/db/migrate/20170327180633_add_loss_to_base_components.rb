class AddLossToBaseComponents < ActiveRecord::Migration
  def change
    add_column :base_components, :loss, :decimal, precision: 10, scale: 5
  end
end
