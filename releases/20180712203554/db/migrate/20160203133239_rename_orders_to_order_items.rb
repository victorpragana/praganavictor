class RenameOrdersToOrderItems < ActiveRecord::Migration
  def change
    rename_table :orders, :order_items
  end
end
