class CreatePriceTableProducts < ActiveRecord::Migration
  def change
    create_table :price_table_products do |t|
      t.belongs_to :price_table, index: true, foreign_key: true
      t.belongs_to :product, index: true, foreign_key: true
      t.decimal :price, null: false

      t.timestamps null: false
    end
  end
end
