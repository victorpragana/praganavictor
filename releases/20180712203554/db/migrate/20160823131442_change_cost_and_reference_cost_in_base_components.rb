class ChangeCostAndReferenceCostInBaseComponents < ActiveRecord::Migration
  def change
    change_column :base_components, :cost, :decimal, precision: 10, scale: 5, default: 0
    change_column :base_components, :reference_cost, :decimal, precision: 10, scale: 5, default: 0
  end
end
