class AddDeletedAtToBaseComponents < ActiveRecord::Migration
  def change
    add_column :base_components, :deleted_at, :datetime
    add_index :base_components, :deleted_at
  end
end
