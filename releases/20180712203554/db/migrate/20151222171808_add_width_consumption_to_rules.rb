class AddWidthConsumptionToRules < ActiveRecord::Migration
  def change
    add_column :rules, :width_consumption, :string
  end
end
