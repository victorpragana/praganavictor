class ChangeOrderProductsToOrderLooseItems < ActiveRecord::Migration
  def up
    rename_table :order_products, :order_loose_items
  end

  def down
    rename_table :order_loose_items, :order_products
  end
end
