class AddCutTableToBaseComponents < ActiveRecord::Migration
  def change
    add_reference :base_components, :cut_table, index: true, foreign_key: true
  end
end
