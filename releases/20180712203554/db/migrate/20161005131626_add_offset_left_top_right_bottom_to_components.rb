class AddOffsetLeftTopRightBottomToComponents < ActiveRecord::Migration
  def change
    add_column :components, :offset_left, :integer, default: 0
    add_column :components, :offset_top, :integer, default: 0
    add_column :components, :offset_right, :integer, default: 0
    add_column :components, :offset_bottom, :integer, default: 0
  end
end
