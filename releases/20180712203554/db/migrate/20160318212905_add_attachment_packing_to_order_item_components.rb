class AddAttachmentPackingToOrderItemComponents < ActiveRecord::Migration
  def self.up
    change_table :order_item_components do |t|
      t.attachment :packing
    end
  end

  def self.down
    remove_attachment :order_item_components, :packing
  end
end
