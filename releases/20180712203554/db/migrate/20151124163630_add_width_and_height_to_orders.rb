class AddWidthAndHeightToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :width, :integer, null: false
    add_column :orders, :height, :integer, null: false
  end
end
