class AddProcessingMessageToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :processing_messages, :text, array: true, default: []
  end
end
