class AddOrderToEstimate < ActiveRecord::Migration
  def change
    add_reference :estimates, :order, index: true, foreign_key: true
  end
end
