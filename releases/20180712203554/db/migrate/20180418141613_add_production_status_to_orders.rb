class AddProductionStatusToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :production_status, :integer, null: false, default: 0, index: true
  end
end
