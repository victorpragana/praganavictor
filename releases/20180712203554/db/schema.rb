# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180514173320) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "attributes", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "base_component_values", force: :cascade do |t|
    t.integer  "base_component_id"
    t.integer  "value_id"
    t.integer  "base_component_value_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.datetime "deleted_at"
  end

  add_index "base_component_values", ["base_component_id"], name: "index_base_component_values_on_base_component_id", using: :btree
  add_index "base_component_values", ["deleted_at"], name: "index_base_component_values_on_deleted_at", using: :btree
  add_index "base_component_values", ["value_id"], name: "index_base_component_values_on_value_id", using: :btree

  create_table "base_components", force: :cascade do |t|
    t.string   "name",                      limit: 255,                                          null: false
    t.decimal  "cost",                                  precision: 10, scale: 5, default: 0.0,   null: false
    t.integer  "category_id"
    t.text     "characteristics"
    t.string   "code",                      limit: 20
    t.integer  "unit",                                                           default: 2,     null: false
    t.datetime "created_at",                                                                     null: false
    t.datetime "updated_at",                                                                     null: false
    t.datetime "deleted_at"
    t.decimal  "reference_cost",                        precision: 10, scale: 5, default: 0.0
    t.integer  "offset_left",                                                    default: 0
    t.integer  "offset_top",                                                     default: 0
    t.integer  "offset_right",                                                   default: 0
    t.integer  "offset_bottom",                                                  default: 0
    t.boolean  "allow_rotation",                                                 default: false
    t.decimal  "exchange",                              precision: 10, scale: 5
    t.decimal  "loss",                                  precision: 10, scale: 5
    t.integer  "cut_table_id"
    t.boolean  "print_on_components_label",                                      default: false
    t.decimal  "fix_cost",                              precision: 10, scale: 5, default: 0.0
    t.boolean  "print_on_lines_label",                                           default: false
  end

  add_index "base_components", ["category_id"], name: "index_base_components_on_category_id", using: :btree
  add_index "base_components", ["cut_table_id"], name: "index_base_components_on_cut_table_id", using: :btree
  add_index "base_components", ["deleted_at"], name: "index_base_components_on_deleted_at", using: :btree

  create_table "base_components_families", id: false, force: :cascade do |t|
    t.integer "base_component_id"
    t.integer "family_id"
  end

  add_index "base_components_families", ["base_component_id", "family_id"], name: "base_components_families_index", unique: true, using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "name",          limit: 255,                 null: false
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.boolean  "use_cut_table",             default: false
  end

  create_table "cities", force: :cascade do |t|
    t.string   "name",       null: false
    t.integer  "state_id"
    t.integer  "ibge_code",  null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "cities", ["state_id"], name: "index_cities_on_state_id", using: :btree

  create_table "commercial_classifications", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "commercial_classifications", ["name"], name: "index_commercial_classifications_on_name", using: :btree

  create_table "commercial_classifications_lines", id: false, force: :cascade do |t|
    t.integer "commercial_classification_id"
    t.integer "line_id"
  end

  add_index "commercial_classifications_lines", ["commercial_classification_id", "line_id"], name: "commercial_classifications_lines_index", unique: true, using: :btree

  create_table "components", force: :cascade do |t|
    t.string   "name",                      limit: 255,                                          null: false
    t.datetime "created_at",                                                                     null: false
    t.datetime "updated_at",                                                                     null: false
    t.decimal  "cost",                                  precision: 10, scale: 5, default: 0.0,   null: false
    t.boolean  "erasable",                                                       default: true
    t.integer  "category_id"
    t.text     "characteristics"
    t.string   "code",                      limit: 20
    t.integer  "unit",                                                           default: 2,     null: false
    t.datetime "deleted_at"
    t.integer  "base_component_id"
    t.decimal  "reference_cost",                        precision: 10, scale: 5, default: 0.0
    t.integer  "offset_left",                                                    default: 0
    t.integer  "offset_top",                                                     default: 0
    t.integer  "offset_right",                                                   default: 0
    t.integer  "offset_bottom",                                                  default: 0
    t.boolean  "allow_rotation",                                                 default: false
    t.decimal  "exchange",                              precision: 10, scale: 5
    t.decimal  "loss",                                  precision: 10, scale: 5
    t.integer  "cut_table_id"
    t.boolean  "print_on_components_label",                                      default: false
    t.decimal  "fix_cost",                              precision: 10, scale: 5, default: 0.0
    t.boolean  "print_on_lines_label",                                           default: false
  end

  add_index "components", ["base_component_id"], name: "index_components_on_base_component_id", using: :btree
  add_index "components", ["category_id"], name: "index_components_on_category_id", using: :btree
  add_index "components", ["cut_table_id"], name: "index_components_on_cut_table_id", using: :btree
  add_index "components", ["deleted_at"], name: "index_components_on_deleted_at", using: :btree

  create_table "components_components", id: false, force: :cascade do |t|
    t.integer "parent_id"
    t.integer "child_id"
  end

  add_index "components_components", ["parent_id", "child_id"], name: "components_components_index", unique: true, using: :btree

  create_table "components_families", id: false, force: :cascade do |t|
    t.integer "component_id"
    t.integer "family_id"
  end

  add_index "components_families", ["component_id", "family_id"], name: "components_families_index", unique: true, using: :btree

  create_table "components_option_components", id: false, force: :cascade do |t|
    t.integer "component_id"
    t.integer "option_component_id"
  end

  add_index "components_option_components", ["component_id", "option_component_id"], name: "components_option_components_index", unique: true, using: :btree

  create_table "components_values", id: false, force: :cascade do |t|
    t.integer "component_id"
    t.integer "value_id"
  end

  add_index "components_values", ["component_id", "value_id"], name: "components_values_index", unique: true, using: :btree

  create_table "customers", force: :cascade do |t|
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "store_id"
  end

  add_index "customers", ["deleted_at"], name: "index_customers_on_deleted_at", using: :btree
  add_index "customers", ["store_id"], name: "index_customers_on_store_id", using: :btree

  create_table "cut_tables", force: :cascade do |t|
    t.string   "name",       null: false
    t.integer  "size",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "dimensions", force: :cascade do |t|
    t.int4range "width",                  null: false
    t.int4range "height",                 null: false
    t.integer   "line_id"
    t.datetime  "created_at",             null: false
    t.datetime  "updated_at",             null: false
    t.integer   "weight",     default: 0, null: false
  end

  add_index "dimensions", ["line_id"], name: "index_dimensions_on_line_id", using: :btree
  add_index "dimensions", ["weight"], name: "index_dimensions_on_weight", using: :btree

  create_table "distributors", force: :cascade do |t|
    t.datetime "deleted_at"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "commercial_classification_id"
  end

  add_index "distributors", ["commercial_classification_id"], name: "index_distributors_on_commercial_classification_id", using: :btree
  add_index "distributors", ["deleted_at"], name: "index_distributors_on_deleted_at", using: :btree

  create_table "documents", force: :cascade do |t|
    t.string   "name"
    t.text     "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "environments", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.integer  "category",                null: false
    t.integer  "estimate_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "environments", ["estimate_id"], name: "index_environments_on_estimate_id", using: :btree

  create_table "estimate_items", force: :cascade do |t|
    t.integer  "estimate_id"
    t.string   "item_code"
    t.string   "item_description",                                         null: false
    t.integer  "unit",                                                     null: false
    t.decimal  "quantity",          precision: 8,  scale: 2,               null: false
    t.datetime "created_at",                                               null: false
    t.datetime "updated_at",                                               null: false
    t.integer  "type",                                       default: 0
    t.decimal  "factory_value",     precision: 10, scale: 2, default: 0.0
    t.decimal  "distributor_value", precision: 10, scale: 2, default: 0.0
    t.decimal  "store_value",       precision: 10, scale: 2, default: 0.0
    t.decimal  "customer_value",    precision: 10, scale: 2, default: 0.0
    t.decimal  "discount",          precision: 5,  scale: 2, default: 0.0
    t.integer  "item_id"
  end

  add_index "estimate_items", ["estimate_id"], name: "index_estimate_items_on_estimate_id", using: :btree
  add_index "estimate_items", ["item_id"], name: "index_estimate_items_on_item_id", using: :btree

  create_table "estimates", force: :cascade do |t|
    t.date     "date",                                                       null: false
    t.text     "observation"
    t.datetime "created_at",                                                 null: false
    t.datetime "updated_at",                                                 null: false
    t.integer  "order_id"
    t.integer  "seller_id"
    t.string   "seller_type"
    t.integer  "client_id"
    t.string   "client_type"
    t.boolean  "optimize",                                   default: false
    t.decimal  "discount",           precision: 5, scale: 2, default: 0.0
    t.integer  "status",                                     default: 0,     null: false
    t.string   "job_id"
    t.text     "processing_message",                         default: [],                 array: true
  end

  add_index "estimates", ["client_id"], name: "index_estimates_on_client_id", using: :btree
  add_index "estimates", ["client_type"], name: "index_estimates_on_client_type", using: :btree
  add_index "estimates", ["order_id"], name: "index_estimates_on_order_id", using: :btree
  add_index "estimates", ["seller_id"], name: "index_estimates_on_seller_id", using: :btree
  add_index "estimates", ["seller_type"], name: "index_estimates_on_seller_type", using: :btree

  create_table "factories", force: :cascade do |t|
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "factories", ["deleted_at"], name: "index_factories_on_deleted_at", using: :btree

  create_table "families", force: :cascade do |t|
    t.string   "name",                 null: false
    t.string   "color",      limit: 7, null: false
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "items", force: :cascade do |t|
    t.string   "code"
    t.string   "description",                          null: false
    t.integer  "unit",                                 null: false
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.datetime "deleted_at"
    t.integer  "type",                     default: 0
    t.json     "erp_categories"
    t.string   "ncm",            limit: 8
    t.integer  "cst"
  end

  add_index "items", ["deleted_at"], name: "index_items_on_deleted_at", using: :btree

  create_table "line_categories", force: :cascade do |t|
    t.integer  "line_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "required",    default: true, null: false
    t.integer  "category_id"
    t.integer  "order",                      null: false
  end

  add_index "line_categories", ["category_id"], name: "index_line_categories_on_category_id", using: :btree
  add_index "line_categories", ["line_id"], name: "index_line_categories_on_line_id", using: :btree

  create_table "line_rules", force: :cascade do |t|
    t.integer   "line_id"
    t.int4range "width"
    t.int4range "height"
    t.integer   "control_method",                       null: false
    t.boolean   "required",             default: true,  null: false
    t.string    "cost",                                 null: false
    t.string    "height_consumption"
    t.string    "width_consumption"
    t.string    "total_consumption",                    null: false
    t.datetime  "created_at",                           null: false
    t.datetime  "updated_at",                           null: false
    t.boolean   "automatic",            default: false
    t.string    "quantity_consumption"
    t.string    "description"
    t.integer   "category_id"
    t.integer   "weight",               default: 0,     null: false
    t.int4range "area"
  end

  add_index "line_rules", ["category_id"], name: "index_line_rules_on_category_id", using: :btree
  add_index "line_rules", ["line_id"], name: "index_line_rules_on_line_id", using: :btree
  add_index "line_rules", ["weight"], name: "index_line_rules_on_weight", using: :btree

  create_table "lines", force: :cascade do |t|
    t.string   "name",                                                                   null: false
    t.datetime "created_at",                                                             null: false
    t.datetime "updated_at",                                                             null: false
    t.integer  "family_id"
    t.decimal  "sale_price",                     precision: 8, scale: 3, default: 0.0,   null: false
    t.integer  "horizontal_increment",                                   default: 0,     null: false
    t.integer  "vertical_increment",                                     default: 0,     null: false
    t.json     "erp_categories"
    t.string   "ncm",                  limit: 8
    t.integer  "cst"
    t.boolean  "active",                                                 default: true
    t.boolean  "finished",                                               default: false
  end

  add_index "lines", ["active"], name: "index_lines_on_active", using: :btree
  add_index "lines", ["family_id"], name: "index_lines_on_family_id", using: :btree

  create_table "lines_questions", id: false, force: :cascade do |t|
    t.integer "line_id"
    t.integer "question_id"
  end

  add_index "lines_questions", ["line_id", "question_id"], name: "lines_questions_index", unique: true, using: :btree

  create_table "locations", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.integer  "category",                   null: false
    t.integer  "environment_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "width"
    t.integer  "height"
    t.integer  "quantity"
  end

  add_index "locations", ["environment_id"], name: "index_locations_on_environment_id", using: :btree

  create_table "movements", force: :cascade do |t|
    t.integer  "owner_id",                                           null: false
    t.integer  "order_id"
    t.integer  "payment_id"
    t.decimal  "value",          precision: 8, scale: 2,             null: false
    t.integer  "direction",                                          null: false
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.integer  "status",                                 default: 1, null: false
    t.datetime "date",                                               null: false
    t.string   "type"
    t.integer  "destination_id",                                     null: false
    t.integer  "reason",                                 default: 0, null: false
  end

  add_index "movements", ["destination_id"], name: "index_movements_on_destination_id", using: :btree
  add_index "movements", ["order_id"], name: "index_movements_on_order_id", using: :btree
  add_index "movements", ["owner_id"], name: "index_movements_on_owner_id", using: :btree
  add_index "movements", ["payment_id"], name: "index_movements_on_payment_id", using: :btree

  create_table "option_components", force: :cascade do |t|
    t.integer  "option_id"
    t.decimal  "total_consumption",                     precision: 10, scale: 5,                 null: false
    t.decimal  "unit_value",                            precision: 10, scale: 5,                 null: false
    t.string   "component_name",            limit: 255,                                          null: false
    t.integer  "unit",                                                                           null: false
    t.text     "component_characteristics"
    t.string   "category_name",             limit: 255,                                          null: false
    t.decimal  "width_consumption",                     precision: 10, scale: 5
    t.decimal  "height_consumption",                    precision: 10, scale: 5
    t.decimal  "quantity_consumption",                  precision: 10, scale: 5
    t.datetime "created_at",                                                                     null: false
    t.datetime "updated_at",                                                                     null: false
    t.boolean  "required",                                                       default: false, null: false
    t.integer  "component_id"
    t.integer  "rule_id"
    t.decimal  "total",                                 precision: 10, scale: 2, default: 0.0
  end

  add_index "option_components", ["component_id"], name: "index_option_components_on_component_id", using: :btree
  add_index "option_components", ["option_id"], name: "index_option_components_on_option_id", using: :btree
  add_index "option_components", ["rule_id"], name: "index_option_components_on_rule_id", using: :btree

  create_table "options", force: :cascade do |t|
    t.integer  "line_id"
    t.integer  "location_id"
    t.integer  "width",                                                      null: false
    t.integer  "height",                                                     null: false
    t.datetime "created_at",                                                 null: false
    t.datetime "updated_at",                                                 null: false
    t.boolean  "selected",                                   default: false, null: false
    t.decimal  "factory_total",     precision: 10, scale: 2, default: 0.0
    t.decimal  "distributor_total", precision: 10, scale: 2, default: 0.0
    t.decimal  "store_total",       precision: 10, scale: 2, default: 0.0
    t.decimal  "customer_total",    precision: 10, scale: 2, default: 0.0
    t.decimal  "discount",          precision: 5,  scale: 2, default: 0.0
    t.json     "questions"
  end

  add_index "options", ["line_id"], name: "index_options_on_line_id", using: :btree
  add_index "options", ["location_id"], name: "index_options_on_location_id", using: :btree

  create_table "order_item_components", force: :cascade do |t|
    t.integer  "order_item_id"
    t.decimal  "total_consumption",                     precision: 10, scale: 5,                 null: false
    t.decimal  "unit_value",                            precision: 10, scale: 5,                 null: false
    t.datetime "created_at",                                                                     null: false
    t.datetime "updated_at",                                                                     null: false
    t.string   "component_name",            limit: 255,                                          null: false
    t.integer  "unit",                                                                           null: false
    t.text     "component_characteristics"
    t.string   "category_name",             limit: 255,                                          null: false
    t.decimal  "width_consumption",                     precision: 10, scale: 5
    t.decimal  "height_consumption",                    precision: 10, scale: 5
    t.decimal  "quantity_consumption",                  precision: 10, scale: 5
    t.boolean  "required",                                                       default: false, null: false
    t.decimal  "total",                                 precision: 10, scale: 2, default: 0.0
    t.integer  "component_id"
  end

  add_index "order_item_components", ["component_id"], name: "index_order_item_components_on_component_id", using: :btree
  add_index "order_item_components", ["order_item_id"], name: "index_order_item_components_on_order_item_id", using: :btree

  create_table "order_items", force: :cascade do |t|
    t.integer  "line_id"
    t.datetime "created_at",                                               null: false
    t.datetime "updated_at",                                               null: false
    t.integer  "width",                                                    null: false
    t.integer  "height",                                                   null: false
    t.integer  "order_id"
    t.integer  "option_id"
    t.decimal  "factory_total",     precision: 10, scale: 2, default: 0.0
    t.decimal  "distributor_total", precision: 10, scale: 2, default: 0.0
    t.decimal  "store_total",       precision: 10, scale: 2, default: 0.0
    t.decimal  "customer_total",    precision: 10, scale: 2, default: 0.0
    t.decimal  "discount",          precision: 5,  scale: 2, default: 0.0
  end

  add_index "order_items", ["line_id"], name: "index_order_items_on_line_id", using: :btree
  add_index "order_items", ["option_id"], name: "index_order_items_on_option_id", using: :btree
  add_index "order_items", ["order_id"], name: "index_order_items_on_order_id", using: :btree

  create_table "order_loose_items", force: :cascade do |t|
    t.integer  "order_id"
    t.string   "item_code"
    t.string   "item_description",                                         null: false
    t.integer  "unit",                                                     null: false
    t.decimal  "quantity",          precision: 8,  scale: 2,               null: false
    t.datetime "created_at",                                               null: false
    t.datetime "updated_at",                                               null: false
    t.integer  "type",                                       default: 0
    t.decimal  "factory_value",     precision: 10, scale: 2, default: 0.0
    t.decimal  "distributor_value", precision: 10, scale: 2, default: 0.0
    t.decimal  "store_value",       precision: 10, scale: 2, default: 0.0
    t.decimal  "customer_value",    precision: 10, scale: 2, default: 0.0
    t.decimal  "discount",          precision: 5,  scale: 2, default: 0.0
    t.integer  "item_id"
  end

  add_index "order_loose_items", ["item_id"], name: "index_order_loose_items_on_item_id", using: :btree
  add_index "order_loose_items", ["order_id"], name: "index_order_loose_items_on_order_id", using: :btree

  create_table "orders", force: :cascade do |t|
    t.date     "date",                                                         null: false
    t.text     "observation"
    t.datetime "created_at",                                                   null: false
    t.datetime "updated_at",                                                   null: false
    t.integer  "seller_id"
    t.string   "seller_type"
    t.integer  "client_id"
    t.string   "client_type"
    t.decimal  "discount",               precision: 5, scale: 2, default: 0.0
    t.integer  "status",                                         default: 0,   null: false
    t.integer  "erp_id"
    t.text     "processing_messages",                            default: [],               array: true
    t.integer  "user_id"
    t.integer  "billing_client_id"
    t.string   "billing_client_type"
    t.decimal  "special_discount",       precision: 5, scale: 2
    t.integer  "production_status",                              default: 0,   null: false
    t.datetime "sent_to_production_at"
    t.datetime "started_production_at"
    t.datetime "finished_production_at"
    t.datetime "dispatched_at"
    t.datetime "received_at"
    t.datetime "sent_to_factory_at"
  end

  add_index "orders", ["client_id"], name: "index_orders_on_client_id", using: :btree
  add_index "orders", ["client_type"], name: "index_orders_on_client_type", using: :btree
  add_index "orders", ["seller_id"], name: "index_orders_on_seller_id", using: :btree
  add_index "orders", ["seller_type"], name: "index_orders_on_seller_type", using: :btree
  add_index "orders", ["user_id"], name: "index_orders_on_user_id", using: :btree

  create_table "packing_configurations", force: :cascade do |t|
    t.integer  "packable_id"
    t.string   "packable_type"
    t.integer  "items",                                                      array: true
    t.decimal  "automatic_dimension",  precision: 10, scale: 5, null: false
    t.decimal  "component_dimension",  precision: 10, scale: 5, null: false
    t.string   "component_name",                                null: false
    t.integer  "unit",                                          null: false
    t.string   "packing_file_name"
    t.string   "packing_content_type"
    t.integer  "packing_file_size"
    t.datetime "packing_updated_at"
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
  end

  add_index "packing_configurations", ["items"], name: "index_packing_configurations_on_items", using: :btree
  add_index "packing_configurations", ["packable_type", "packable_id"], name: "index_packing_configurations_on_packable_type_and_packable_id", using: :btree

  create_table "payment_conditions", force: :cascade do |t|
    t.integer  "installments",                                                  null: false
    t.decimal  "additional_tax",          precision: 5, scale: 2, default: 0.0
    t.decimal  "antecipation_tax",        precision: 5, scale: 2, default: 0.0
    t.decimal  "credit_antecipation_tax", precision: 5, scale: 2, default: 0.0
    t.datetime "created_at",                                                    null: false
    t.datetime "updated_at",                                                    null: false
    t.integer  "type",                                            default: 0,   null: false
    t.decimal  "credit_tax",              precision: 5, scale: 2, default: 0.0
  end

  create_table "payments", force: :cascade do |t|
    t.decimal  "value",                          precision: 8, scale: 2,               null: false
    t.integer  "status",                                                 default: 0,   null: false
    t.integer  "payment_method",                                         default: 0,   null: false
    t.integer  "installment_number",                                     default: 1,   null: false
    t.string   "bank_slip_url",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "iugu_invoice_id"
    t.decimal  "tax",                            precision: 5, scale: 2, default: 0.0
    t.decimal  "original_value",                 precision: 8, scale: 2, default: 0.0, null: false
  end

  create_table "price_table_clients", force: :cascade do |t|
    t.integer  "price_table_id"
    t.integer  "client_id"
    t.string   "client_type"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "price_table_clients", ["client_id"], name: "index_price_table_clients_on_client_id", using: :btree
  add_index "price_table_clients", ["client_type"], name: "index_price_table_clients_on_client_type", using: :btree
  add_index "price_table_clients", ["price_table_id"], name: "index_price_table_clients_on_price_table_id", using: :btree

  create_table "price_table_items", force: :cascade do |t|
    t.integer  "price_table_id"
    t.integer  "item_id"
    t.decimal  "price",            precision: 8, scale: 2
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.decimal  "maximum_discount", precision: 5, scale: 2, default: 0.0
  end

  add_index "price_table_items", ["item_id"], name: "index_price_table_items_on_item_id", using: :btree
  add_index "price_table_items", ["price_table_id"], name: "index_price_table_items_on_price_table_id", using: :btree

  create_table "price_table_lines", force: :cascade do |t|
    t.integer  "price_table_id"
    t.integer  "line_id"
    t.string   "price"
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.decimal  "maximum_discount", precision: 5, scale: 2, default: 0.0
  end

  add_index "price_table_lines", ["line_id"], name: "index_price_table_lines_on_line_id", using: :btree
  add_index "price_table_lines", ["price_table_id"], name: "index_price_table_lines_on_price_table_id", using: :btree

  create_table "price_tables", force: :cascade do |t|
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "owner_id"
    t.string   "owner_type"
    t.string   "name",       default: "",    null: false
    t.integer  "type",       default: 0,     null: false
    t.boolean  "default",    default: false
  end

  add_index "price_tables", ["owner_id"], name: "index_price_tables_on_owner_id", using: :btree
  add_index "price_tables", ["owner_type"], name: "index_price_tables_on_owner_type", using: :btree

  create_table "printers", force: :cascade do |t|
    t.string   "name",        null: false
    t.string   "real_name",   null: false
    t.string   "server_name", null: false
    t.string   "server_id",   null: false
    t.boolean  "default"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "questions", force: :cascade do |t|
    t.string   "label",      null: false
    t.string   "value",      null: false
    t.integer  "type",       null: false
    t.json     "options"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rules", force: :cascade do |t|
    t.integer   "component_id"
    t.int4range "width"
    t.int4range "height"
    t.integer   "control_method",                       null: false
    t.datetime  "created_at",                           null: false
    t.datetime  "updated_at",                           null: false
    t.string    "total_consumption",    default: "",    null: false
    t.string    "cost",                 default: "",    null: false
    t.boolean   "required",             default: true,  null: false
    t.string    "height_consumption"
    t.string    "width_consumption"
    t.integer   "line_rule_id"
    t.boolean   "automatic",            default: false
    t.string    "quantity_consumption"
    t.string    "description"
    t.integer   "weight",               default: 0,     null: false
    t.int4range "area"
  end

  add_index "rules", ["component_id"], name: "index_rules_on_component_id", using: :btree
  add_index "rules", ["line_rule_id"], name: "index_rules_on_line_rule_id", using: :btree
  add_index "rules", ["weight"], name: "index_rules_on_weight", using: :btree

  create_table "specifications", force: :cascade do |t|
    t.string   "name",                           null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.datetime "deleted_at"
    t.boolean  "automatic",      default: false
    t.boolean  "used_in_filter", default: false
  end

  add_index "specifications", ["deleted_at"], name: "index_specifications_on_deleted_at", using: :btree

  create_table "states", force: :cascade do |t|
    t.string   "name",       null: false
    t.string   "acronym",    null: false
    t.integer  "ibge_code",  null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "stores", force: :cascade do |t|
    t.datetime "deleted_at"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "distributor_id"
    t.integer  "commercial_classification_id"
  end

  add_index "stores", ["commercial_classification_id"], name: "index_stores_on_commercial_classification_id", using: :btree
  add_index "stores", ["deleted_at"], name: "index_stores_on_deleted_at", using: :btree
  add_index "stores", ["distributor_id"], name: "index_stores_on_distributor_id", using: :btree

  create_table "third_parties", force: :cascade do |t|
    t.string   "name",                                                         null: false
    t.string   "rg"
    t.string   "issuing_entity"
    t.string   "identification"
    t.string   "zip_code"
    t.string   "street"
    t.string   "number"
    t.string   "complement"
    t.string   "phone"
    t.string   "email"
    t.string   "corporate_name"
    t.datetime "deleted_at"
    t.datetime "created_at",                                                   null: false
    t.datetime "updated_at",                                                   null: false
    t.integer  "actable_id"
    t.string   "actable_type"
    t.string   "state_inscription"
    t.string   "municipal_inscription"
    t.integer  "city_id"
    t.string   "neighborhood"
    t.string   "subdomain"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.string   "current_account"
    t.string   "agency"
    t.integer  "bank"
    t.boolean  "allow_special_discount"
    t.decimal  "special_discount",       precision: 5, scale: 2, default: 0.0
  end

  add_index "third_parties", ["city_id"], name: "index_third_parties_on_city_id", using: :btree
  add_index "third_parties", ["deleted_at"], name: "index_third_parties_on_deleted_at", using: :btree

  create_table "user_workplaces", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "workplace_id"
    t.string   "workplace_type"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "user_workplaces", ["user_id"], name: "index_user_workplaces_on_user_id", using: :btree
  add_index "user_workplaces", ["workplace_type", "workplace_id"], name: "index_user_workplaces_on_workplace_type_and_workplace_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "provider",                                       default: "email", null: false
    t.string   "uid",                                            default: "",      null: false
    t.string   "email",                                          default: "",      null: false
    t.string   "username",                                       default: "",      null: false
    t.string   "encrypted_password",                             default: "",      null: false
    t.string   "name",                                           default: "",      null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                                  default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.json     "tokens"
    t.datetime "created_at",                                                       null: false
    t.datetime "updated_at",                                                       null: false
    t.string   "type"
    t.decimal  "maximum_discount",       precision: 5, scale: 2, default: 0.0
    t.integer  "erp_id"
    t.integer  "selected_workplace_id"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["selected_workplace_id"], name: "index_users_on_selected_workplace_id", using: :btree
  add_index "users", ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true, using: :btree
  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

  create_table "values", force: :cascade do |t|
    t.string   "value",            null: false
    t.integer  "specification_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.datetime "deleted_at"
    t.integer  "comparison_value"
  end

  add_index "values", ["deleted_at"], name: "index_values_on_deleted_at", using: :btree
  add_index "values", ["specification_id"], name: "index_values_on_specification_id", using: :btree

  create_table "version_associations", force: :cascade do |t|
    t.integer "version_id"
    t.string  "foreign_key_name", null: false
    t.integer "foreign_key_id"
  end

  add_index "version_associations", ["foreign_key_name", "foreign_key_id"], name: "index_version_associations_on_foreign_key", using: :btree
  add_index "version_associations", ["version_id"], name: "index_version_associations_on_version_id", using: :btree

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",      null: false
    t.integer  "item_id",        null: false
    t.string   "event",          null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
    t.integer  "transaction_id"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree
  add_index "versions", ["transaction_id"], name: "index_versions_on_transaction_id", using: :btree

  add_foreign_key "base_component_values", "\"values\"", column: "value_id"
  add_foreign_key "base_component_values", "base_components"
  add_foreign_key "base_components", "categories"
  add_foreign_key "base_components", "cut_tables"
  add_foreign_key "cities", "states"
  add_foreign_key "components", "base_components"
  add_foreign_key "components", "categories"
  add_foreign_key "components", "cut_tables"
  add_foreign_key "customers", "stores"
  add_foreign_key "dimensions", "lines"
  add_foreign_key "distributors", "commercial_classifications"
  add_foreign_key "environments", "estimates"
  add_foreign_key "estimate_items", "estimates"
  add_foreign_key "estimate_items", "items"
  add_foreign_key "estimates", "orders"
  add_foreign_key "line_categories", "categories"
  add_foreign_key "line_categories", "lines"
  add_foreign_key "line_rules", "categories"
  add_foreign_key "line_rules", "lines"
  add_foreign_key "lines", "families"
  add_foreign_key "locations", "environments"
  add_foreign_key "option_components", "components"
  add_foreign_key "option_components", "options"
  add_foreign_key "option_components", "rules"
  add_foreign_key "options", "lines"
  add_foreign_key "options", "locations"
  add_foreign_key "order_item_components", "components"
  add_foreign_key "order_item_components", "order_items"
  add_foreign_key "order_items", "lines"
  add_foreign_key "order_items", "options"
  add_foreign_key "order_items", "orders"
  add_foreign_key "order_loose_items", "items"
  add_foreign_key "order_loose_items", "orders"
  add_foreign_key "orders", "users"
  add_foreign_key "price_table_clients", "price_tables"
  add_foreign_key "price_table_items", "items"
  add_foreign_key "price_table_items", "price_tables"
  add_foreign_key "price_table_lines", "lines"
  add_foreign_key "price_table_lines", "price_tables"
  add_foreign_key "rules", "components"
  add_foreign_key "rules", "line_rules"
  add_foreign_key "stores", "commercial_classifications"
  add_foreign_key "stores", "distributors"
  add_foreign_key "third_parties", "cities"
  add_foreign_key "user_workplaces", "users"
  add_foreign_key "users", "user_workplaces", column: "selected_workplace_id"
  add_foreign_key "values", "specifications"
end
