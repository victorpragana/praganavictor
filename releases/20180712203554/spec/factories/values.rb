# == Schema Information
#
# Table name: values
#
#  id               :integer          not null, primary key
#  value            :string           not null
#  specification_id :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  deleted_at       :datetime
#  comparison_value :integer
#

FactoryGirl.define do
  factory :value do
    value         {Faker::Name.name}
    specification nil
  end
end
