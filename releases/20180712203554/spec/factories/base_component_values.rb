# == Schema Information
#
# Table name: base_component_values
#
#  id                      :integer          not null, primary key
#  base_component_id       :integer
#  value_id                :integer
#  base_component_value_id :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  deleted_at              :datetime
#

FactoryGirl.define do
  factory :base_component_value do
    base_component nil
    value nil
    base_component_value_id nil
  end
end
