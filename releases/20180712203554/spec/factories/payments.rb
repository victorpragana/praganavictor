# == Schema Information
#
# Table name: payments
#
#  id                 :integer          not null, primary key
#  value              :decimal(8, 2)    not null
#  status             :integer          default(0), not null
#  payment_method     :integer          default(0), not null
#  installment_number :integer          default(1), not null
#  bank_slip_url      :string(255)
#  created_at         :datetime
#  updated_at         :datetime
#  iugu_invoice_id    :string
#  tax                :decimal(5, 2)    default(0.0)
#  original_value     :decimal(8, 2)    default(0.0), not null
#

FactoryGirl.define do
  factory :payment do
    value           { Faker::Number.decimal(2) }
    original_value  { Faker::Number.decimal(2) }
    iugu_invoice_id { Faker::Internet.password(10) }
  end
end
