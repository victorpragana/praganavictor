# == Schema Information
#
# Table name: states
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  acronym    :string           not null
#  ibge_code  :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :state do
    name          {Faker::Address.state}
    acronym       {Faker::Address.state_abbr}
    ibge_code     {Faker::Number.number(7)}
  end
end
