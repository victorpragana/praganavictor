# == Schema Information
#
# Table name: orders
#
#  id                     :integer          not null, primary key
#  date                   :date             not null
#  observation            :text
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  seller_id              :integer
#  seller_type            :string
#  client_id              :integer
#  client_type            :string
#  discount               :decimal(5, 2)    default(0.0)
#  status                 :integer          default(0), not null
#  erp_id                 :integer
#  processing_messages    :text             default([]), is an Array
#  user_id                :integer
#  billing_client_id      :integer
#  billing_client_type    :string
#  special_discount       :decimal(5, 2)
#  production_status      :integer          default(0), not null
#  sent_to_production_at  :datetime
#  started_production_at  :datetime
#  finished_production_at :datetime
#  dispatched_at          :datetime
#  received_at            :datetime
#  sent_to_factory_at     :datetime
#

FactoryGirl.define do
  factory :order do
    date        { Date.today }
    observation { Faker::Lorem.paragraph }
  end
end
