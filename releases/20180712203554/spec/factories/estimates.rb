# == Schema Information
#
# Table name: estimates
#
#  id                 :integer          not null, primary key
#  date               :date             not null
#  observation        :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  order_id           :integer
#  seller_id          :integer
#  seller_type        :string
#  client_id          :integer
#  client_type        :string
#  optimize           :boolean          default(FALSE)
#  discount           :decimal(5, 2)    default(0.0)
#  status             :integer          default(0), not null
#  job_id             :string
#  processing_message :text             default([]), is an Array
#

FactoryGirl.define do
  factory :estimate do
    date        { Date.today }
    observation { Faker::Lorem.paragraph }
  end
end
