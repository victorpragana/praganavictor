# == Schema Information
#
# Table name: line_rules
#
#  id                   :integer          not null, primary key
#  line_id              :integer
#  width                :int4range
#  height               :int4range
#  control_method       :integer          not null
#  required             :boolean          default(TRUE), not null
#  cost                 :string           not null
#  height_consumption   :string
#  width_consumption    :string
#  total_consumption    :string           not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  automatic            :boolean          default(FALSE)
#  quantity_consumption :string
#  description          :string
#  category_id          :integer
#  weight               :integer          default(0), not null
#  area                 :int4range
#

FactoryGirl.define do
  factory :line_rule do
    line                 nil
    width                {Faker::Number.between(1, 10)..Faker::Number.between(10, 20)}
    height               {Faker::Number.between(1, 10)..Faker::Number.between(10, 20)}
    control_method       {Faker::Number.between(0, 3)}
    required             {[false, true].sample}
    automatic            {false}
    cost                 {"1+1"}
    quantity_consumption {"1+1"}
    height_consumption   {"1+1"}
    width_consumption    {"1+1"}
    total_consumption    {"1+1"}
    description          {Faker::Lorem.sentence}
    weight               {:general}
  end
end
