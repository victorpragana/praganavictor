# == Schema Information
#
# Table name: packing_configurations
#
#  id                   :integer          not null, primary key
#  packable_id          :integer
#  packable_type        :string
#  items                :integer          is an Array
#  automatic_dimension  :decimal(10, 5)   not null
#  component_dimension  :decimal(10, 5)   not null
#  component_name       :string           not null
#  unit                 :integer          not null
#  packing_file_name    :string
#  packing_content_type :string
#  packing_file_size    :integer
#  packing_updated_at   :datetime
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

FactoryGirl.define do
  factory :packing_configuration do
    packable            nil
    items               { [1, 2, 3] }
    automatic_dimension { Faker::Number.between(100, 500) }
    component_dimension { Faker::Number.between(100, 500) }
    component_name      { Faker::Name.name }
    unit                { Faker::Number.between(0, 2) }
  end
end
