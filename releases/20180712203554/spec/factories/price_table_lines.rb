# == Schema Information
#
# Table name: price_table_lines
#
#  id               :integer          not null, primary key
#  price_table_id   :integer
#  line_id          :integer
#  price            :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  maximum_discount :decimal(5, 2)    default(0.0)
#

FactoryGirl.define do
  factory :price_table_line do
    line        nil
    price_table nil
    price       { '1+1' }
  end
end
