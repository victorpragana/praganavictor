# == Schema Information
#
# Table name: third_parties
#
#  id                     :integer          not null, primary key
#  name                   :string           not null
#  rg                     :string
#  issuing_entity         :string
#  identification         :string
#  zip_code               :string
#  street                 :string
#  number                 :string
#  complement             :string
#  phone                  :string
#  email                  :string
#  corporate_name         :string
#  deleted_at             :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  actable_id             :integer
#  actable_type           :string
#  state_inscription      :string
#  municipal_inscription  :string
#  city_id                :integer
#  neighborhood           :string
#  subdomain              :string
#  logo_file_name         :string
#  logo_content_type      :string
#  logo_file_size         :integer
#  logo_updated_at        :datetime
#  current_account        :string
#  agency                 :string
#  bank                   :integer
#  allow_special_discount :boolean
#  special_discount       :decimal(5, 2)    default(0.0)
#

FactoryGirl.define do
  factory :third_party do
    name             { Faker::Name.name }
    rg               { "345899869" }
    issuing_entity   { "SSP-SP" }
    identification   { Faker::CPF.numeric }
    zip_code         { Faker::Address.zip_code }
    street           { Faker::Address.street_name }
    neighborhood     { Faker::Address.street_name }
    number           { Faker::Address.building_number }
    complement       { Faker::Address.secondary_address }
    phone            { Faker::Number.number(9) }
    email            { Faker::Internet.email }
    corporate_name   { Faker::Company.name }
    allow_special_discount { false }
    subdomain        {"#{name&.parameterize}"}
  end
end
