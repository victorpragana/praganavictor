# == Schema Information
#
# Table name: options
#
#  id                :integer          not null, primary key
#  line_id           :integer
#  location_id       :integer
#  width             :integer          not null
#  height            :integer          not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  selected          :boolean          default(FALSE), not null
#  factory_total     :decimal(10, 2)   default(0.0)
#  distributor_total :decimal(10, 2)   default(0.0)
#  store_total       :decimal(10, 2)   default(0.0)
#  customer_total    :decimal(10, 2)   default(0.0)
#  discount          :decimal(5, 2)    default(0.0)
#  questions         :json
#

FactoryGirl.define do
  factory :option do
    line      nil
    location  nil
    width     { Faker::Number.between(1, 2000) }
    height    { Faker::Number.between(1, 2000) }
    selected  { false }
  end
end
