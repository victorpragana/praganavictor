# == Schema Information
#
# Table name: items
#
#  id             :integer          not null, primary key
#  code           :string
#  description    :string           not null
#  unit           :integer          not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  deleted_at     :datetime
#  type           :integer          default(0)
#  erp_categories :json
#  ncm            :string(8)
#  cst            :integer
#

FactoryGirl.define do
  factory :item do
    code        { Faker::Code.ean }
    description { Faker::Commerce.product_name }
    unit        { Item.units.to_a.sample[1] }
    type        { Item.types.to_a.sample[1] }
  end
end
