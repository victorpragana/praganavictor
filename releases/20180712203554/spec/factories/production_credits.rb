# == Schema Information
#
# Table name: movements
#
#  id             :integer          not null, primary key
#  third_party_id :integer          not null
#  order_id       :integer
#  payment_id     :integer
#  value          :decimal(8, 2)    not null
#  direction      :integer          not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  status         :integer          default(1), not null
#  date           :date             not null
#  type           :string
#

FactoryGirl.define do
  factory :production_credit do
    value  { Faker::Number.decimal(2) }
    date   { Date.today }
    status { :approved }
    type   { "ProductionCredit" }
  end
end
