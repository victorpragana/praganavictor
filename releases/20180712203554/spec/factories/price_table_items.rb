# == Schema Information
#
# Table name: price_table_items
#
#  id               :integer          not null, primary key
#  price_table_id   :integer
#  item_id          :integer
#  price            :decimal(8, 2)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  maximum_discount :decimal(5, 2)    default(0.0)
#

FactoryGirl.define do
  factory :price_table_item do
    price_table nil
    item        nil
    price       { Faker::Number.decimal(2) }
  end
end
