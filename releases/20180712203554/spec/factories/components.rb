# == Schema Information
#
# Table name: components
#
#  id                        :integer          not null, primary key
#  name                      :string(255)      not null
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  cost                      :decimal(10, 5)   default(0.0), not null
#  erasable                  :boolean          default(TRUE)
#  category_id               :integer
#  characteristics           :text
#  code                      :string(20)
#  unit                      :integer          default(2), not null
#  deleted_at                :datetime
#  base_component_id         :integer
#  reference_cost            :decimal(10, 5)   default(0.0)
#  offset_left               :integer          default(0)
#  offset_top                :integer          default(0)
#  offset_right              :integer          default(0)
#  offset_bottom             :integer          default(0)
#  allow_rotation            :boolean          default(FALSE)
#  exchange                  :decimal(10, 5)
#  loss                      :decimal(10, 5)
#  cut_table_id              :integer
#  print_on_lines_label      :boolean          default(FALSE)
#  fix_cost                  :decimal(10, 5)   default(0.0)
#  print_on_components_label :boolean          default(FALSE)
#

FactoryGirl.define do
  factory :component do
    name            { Faker::Name.name }
    cost            { Faker::Number.between(0, 5) }
    reference_cost  { Faker::Number.between(0, 5) }
    erasable        { true }
    code            { Faker::Lorem.characters(20) }
    characteristics { Faker::Lorem.paragraph }
    unit            { Faker::Number.between(0, 2) }
  end
end
