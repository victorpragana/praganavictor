RSpec.shared_examples "a base calculator" do |packing|
  it "fails if the line is not present" do
    expect(described_class.new(nil, @component_1.id, width, height, packing)).to be_invalid
  end

  it "fails if the line is blank" do
    expect(described_class.new("", @component_1.id, width, height, packing)).to be_invalid
  end

  it "fails if the component is not present" do
    expect(described_class.new(@line.id, nil, width, height, packing)).to be_invalid
  end

  it "fails if the component is blank" do
    expect(described_class.new(@line.id, "", width, height, packing)).to be_invalid
  end

  it "fails if the width is not present" do
    expect(described_class.new(@line.id, @component_1.id, nil, height, packing)).to be_invalid
  end

  it "fails if the width is blank" do
    expect(described_class.new(@line.id, @component_1.id, "", height, packing)).to be_invalid
  end

  it "fails if the width is zero" do
    expect(described_class.new(@line.id, @component_1.id, 0, height, packing)).to be_invalid
  end

  it "fails if the width is negative" do
    expect(described_class.new(@line.id, @component_1.id, -1, height, packing)).to be_invalid
  end

  it "fails if the height is not present" do
    expect(described_class.new(@line.id, @component_1.id, width, nil, packing)).to be_invalid
  end

  it "fails if the height is blank" do
    expect(described_class.new(@line.id, @component_1.id, width, "", packing)).to be_invalid
  end

  it "fails if the height is zero" do
    expect(described_class.new(@line.id, @component_1.id, width, 0, packing)).to be_invalid
  end

  it "fails if the height is negative" do
    expect(described_class.new(@line.id, @component_1.id, width, -1, packing)).to be_invalid
  end

  it "fails if the component does not have a rule for the given line" do
    expect(described_class.new(@line.id, @component_2.id, width, height, packing)).to be_invalid
  end
end