RSpec.shared_examples 'a movement' do
  describe 'associations' do
    it { is_expected.to belong_to(:owner).class_name('ThirdParty') }
    it { is_expected.to belong_to(:destination).class_name('ThirdParty') }
    it { is_expected.to belong_to(:order) }
    it { is_expected.to belong_to(:payment) }
  end

  describe 'enums' do
    it do
      should define_enum_for(:direction).with(
        input:  0,
        output: 1
      )
    end

    it do
      should define_enum_for(:status).with(
        pending: 0,
        approved: 1,
        antecipated: 2
      )
    end

    it do
      should define_enum_for(:reason).with(
        credit: 0,
        order_payment: 1,
        send_to_factory: 2,
        cancel_send_to_factory: 3,
        order_production: 4,
        cancel_order_production: 5,
        antecipation: 6,
        store_credit: 7,
        store_antecipation: 8
      )
    end
  end

  context 'validation' do
    before do
      @order = build(:order)
      @payment = build(:payment)

      @factory = create(:factory, name: 'F1', subdomain: 'factory-f1')
      @distributor = create(:distributor, name: 'D1', subdomain: 'd1')
      @store = create(:store, distributor: @distributor)
    end

    it { is_expected.to validate_presence_of(:status) }
    it { is_expected.to validate_presence_of(:direction) }

    it 'fails if type is not given' do
      expect(build(described_class, value: 100, payment: @payment, direction: :input, owner: @store.third_party, destination: @distributor.third_party, type: nil)).to be_invalid
    end

    it 'fails if reason is not given' do
      expect(build(described_class, value: 100, payment: @payment, direction: :input, owner: @store.third_party, destination: @distributor.third_party, reason: nil)).to be_invalid
    end

    it 'fails if owner is not given' do
      expect(build(described_class, value: 100, payment: @payment, destination: @distributor.third_party, direction: :input)).to be_invalid
    end

    it 'fails if owner is nil' do
      expect(build(described_class, owner: nil, payment: @payment, value: 100, destination: @distributor.third_party, direction: :input)).to be_invalid
    end

    it 'fails if destination is not given' do
      expect(build(described_class, value: 100, payment: @payment, owner: @distributor.third_party, direction: :input)).to be_invalid
    end

    it 'fails if destination is nil' do
      expect(build(described_class, destination: nil, payment: @payment, value: 100, owner: @distributor.third_party, direction: :input)).to be_invalid
    end

    it 'fails if value is nil' do
      expect(build(described_class, owner: @store.third_party, payment: @payment, value: nil, direction: :input, destination: @distributor.third_party)).to be_invalid
    end

    it 'fails if value is blank' do
      expect(build(described_class, owner: @store.third_party, payment: @payment, value: '', direction: :input, destination: @distributor.third_party)).to be_invalid
    end

    it 'fails if date is not present' do
      expect(build(described_class, date: nil, owner: @store.third_party, payment: @payment, direction: :input, destination: @distributor.third_party)).to be_invalid
    end

    it 'fails if date is blank' do
      expect(build(described_class, date: '', owner: @store.third_party, payment: @payment, direction: :input, destination: @distributor.third_party)).to be_invalid
    end

    it 'fails if the reason is store credit and the owner is not a distributor' do
      expect(build(described_class, owner: @store.third_party, value: 10, direction: :input, destination: @distributor.third_party, reason: :store_credit, payment: nil)).to be_invalid
    end

    it 'fails if the reason is store credit and the destination is not the factory' do
      expect(build(described_class, owner: @distributor.third_party, value: 10, direction: :input, destination: @store.third_party, reason: :store_credit, payment: nil)).to be_invalid
    end

    it 'fails if the reason is store antecipation and the owner is not a distributor' do
      expect(build(described_class, owner: @store.third_party, value: 10, direction: :input, destination: @distributor.third_party, reason: :store_antecipation, payment: nil)).to be_invalid
    end

    it 'fails if the reason is store antecipation and the destination is not the factory' do
      expect(build(described_class, owner: @distributor.third_party, value: 10, direction: :input, destination: @store.third_party, reason: :store_antecipation, payment: nil)).to be_invalid
    end

    context 'for input movements' do
      it 'fails if the owner is the factory' do
        expect(build(described_class, owner: @factory.third_party, payment: @payment, value: 100, destination: @distributor.third_party, direction: :input)).to be_invalid
      end

      context 'for store owner' do
        it 'fails if the destination is not a distributor' do
          expect(build(described_class, owner: @store.third_party, payment: @payment, destination: @factory.third_party, direction: :input)).to be_invalid
        end
      end

      context 'for distributor owner' do
        it 'fails if the destination is not the factory' do
          expect(build(described_class, owner: @distributor.third_party, payment: @payment, destination: @store.third_party, direction: :input)).to be_invalid
        end
      end

      it 'fails if value is negative' do
        expect(build(described_class, owner: @store.third_party, payment: @payment, value: -1, direction: :input, destination: @distributor.third_party)).to be_invalid
      end

      it 'fails if the reason is credit and the payment is not present' do
        expect(build(described_class, owner: @store.third_party, value: 10, direction: :input, destination: @distributor.third_party, reason: :credit, payment: nil)).to be_invalid
      end

      it 'fails if the reason is order_payment and the payment is not present' do
        expect(build(described_class, owner: @store.third_party, value: 10, direction: :input, destination: @distributor.third_party, reason: :order_payment, payment: nil)).to be_invalid
      end

      it 'passes with order' do
        expect(build(described_class, owner: @store.third_party, payment: @payment, order: @order, direction: :input, destination: @distributor.third_party)).to be_valid
      end

      it 'passes without order' do
        expect(build(described_class, owner: @store.third_party, payment: @payment, direction: :input, destination: @distributor.third_party)).to be_valid
      end
    end

    context 'for output movements' do
      it 'fails if value is positive' do
        expect(build(described_class, owner: @store.third_party, order: @order, value: 1, direction: :output, destination: @distributor.third_party, reason: :order_production)).to be_invalid
      end

      [:send_to_factory, :cancel_send_to_factory, :order_production, :cancel_order_production].each do |reason|
        context "for #{reason} reason" do
          it 'fails if order is not given' do
            expect(build(described_class, owner: @store.third_party, value: -1, direction: :output, destination: @distributor.third_party, reason: reason)).to be_invalid
          end

          it 'fails if order is nil' do
            expect(build(described_class, owner: @store.third_party, value: -1, order: nil, direction: :output, destination: @distributor.third_party, reason: reason)).to be_invalid
          end
        end
      end

      context "for antecipation reason" do
        it 'passes even if order is not given' do
          expect(build(described_class, owner: @store.third_party, value: -1, direction: :output, destination: @distributor.third_party, reason: :antecipation)).to be_valid
        end

        it 'passes even if order is nil' do
          expect(build(described_class, owner: @store.third_party, value: -1, order: nil, direction: :output, destination: @distributor.third_party, reason: :antecipation)).to be_valid
        end
      end

      context "for store antecipation reason" do
        it 'passes even if order is not given' do
          expect(build(described_class, owner: @distributor.third_party, value: -1, direction: :output, destination: @factory.third_party, reason: :store_antecipation)).to be_valid
        end

        it 'passes even if order is nil' do
          expect(build(described_class, owner: @distributor.third_party, value: -1, order: nil, direction: :output, destination: @factory.third_party, reason: :store_antecipation)).to be_valid
        end
      end

      it 'passes' do
        expect(build(described_class, owner: @store.third_party, order: @order, value: -1, direction: :output, destination: @distributor.third_party, reason: :order_production)).to be_valid
      end
    end
  end

  describe '#installment_number' do
    before do
      @factory = create(:factory, name: 'F1', subdomain: 'factory-f1')
      @distributor = create(:distributor, name: 'D1', subdomain: 'd1')
      @store = create(:store, distributor: @distributor)
    end

    context 'when a payment is present' do
      before do
        @payment = create(:payment, installment_number: 3)

        @movement1 = create(described_class, owner: @store.third_party, direction: :input, destination: @distributor.third_party, payment: @payment, date: 15.hours.ago)
        @movement2 = create(described_class, owner: @store.third_party, direction: :input, destination: @distributor.third_party, payment: @payment, date: 15.hours.ago + 1.month)
        @movement3 = create(described_class, owner: @store.third_party, direction: :input, destination: @distributor.third_party, payment: @payment, date: 15.hours.ago + 2.months)
      end

      it 'returns the movement position based on the movements dates originated by that payment' do
        expect(@movement1.installment_number).to eq(1)
        expect(@movement2.installment_number).to eq(2)
        expect(@movement3.installment_number).to eq(3)
      end
    end

    context 'when a payment is not present' do
      it 'returns 1' do
        expect(build(described_class, owner: @store.third_party, direction: :input, destination: @distributor.third_party, payment: nil).installment_number).to eq(1)
      end
    end
  end
end