require 'rails_helper'

RSpec.describe CreditSearcher, type: :searcher do
  subject { described_class.new parameters }

  describe '#search' do
    before do
      @factory = create(:factory, name: 'F1', subdomain: 'faoo')
      @distributor = create(:distributor, name: 'D1')
      @store = create(:store, name: 'S1', distributor: @distributor)

      @distributor2 = create(:distributor, name: 'D2')

      @user = create(:administrator).tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: @store, user: administrator))
      end

      @order = create(:order, seller: @store, client: @distributor, billing_client: @distributor, user: @user)
    end

    context 'for money credit search' do
      let(:parameters) { { 'credit_type' => 'MoneyCredit' } }

      before do
        @payment = create(:payment)
        @movement1 = create(:money_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 20.days.ago, value: 200, payment: @payment)
        @movement2 = create(:money_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 25.days.ago, value: 300, order: @order, payment: @payment)
        @movement3 = create(:money_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :output, date: 2.days.ago, value: -400, order: @order, reason: :send_to_factory)
        
        create(:production_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 25.days.ago, value: 800, order: @order, payment: @payment)

        @movement4 = create(:money_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :input, date: 17.days.ago, value: 200, reason: :send_to_factory)
        @movement5 = create(:money_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :output, date: 15.days.ago, value: -50, order: @order, reason: :order_production)

        @movement6 = create(:money_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 20.days.ago, value: 333, status: :pending, payment: @payment)
        @movement7 = create(:money_credit, owner: @distributor2.third_party, destination: @factory.third_party, direction: :input, date: 21.days.ago, value: 333, status: :approved, payment: @payment)

        @movement8 = create(:money_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 20.days.ago, value: 200, payment: @payment, status: :antecipated)
      end

      context 'for store' do
        before do
          @administrator = create(:administrator, name: 'admin', username: 'admin').tap do |administrator|
            administrator.update!(selected_workplace: create(:user_workplace, workplace: @store, user: administrator))
          end
        end

        context 'without search parameter' do
          it 'lists all the store\'s approved and antecipated money credits ordered by date' do
            results = subject.search(@store)

            expect(results.size).to eq(4)
            expect(results).to eq([@movement2, @movement1, @movement8, @movement3])
          end
        end
      end

      context 'for distributor' do
        before do
          @administrator = create(:administrator, name: 'admin', username: 'admin').tap do |administrator|
            administrator.update!(selected_workplace: create(:user_workplace, workplace: @distributor, user: administrator))
          end
        end

        context 'when the type is credit_extract' do
          let(:parameters) { { 'credit_type' => 'MoneyCredit', 'type' => 'credit_extract' } }

          context 'without search parameter' do
            it 'lists all the distributor\'s approved money credits ordered by date' do
              results = subject.search(@distributor)

              expect(results.size).to eq(2)
              expect(results).to eq([@movement4, @movement5])
            end
          end
        end

        context 'when the type is conceded_credit' do
          let(:parameters) { { 'credit_type' => 'MoneyCredit', 'type' => 'conceded_credit' } }

          context 'without search parameter' do
            it 'lists all the approved and antecipated money credits to the distributor ordered by date' do
              results = subject.search(@distributor)

              expect(results.size).to eq(4)
              expect(results).to eq([@movement2, @movement1, @movement8, @movement3])
            end
          end

          context 'with search parameter' do
            context 'for third party' do
              let(:parameters) { { 'credit_type' => 'MoneyCredit', 'type' => 'conceded_credit', 'third_party_id' => "#{@store2.third_party.id}" } }

              before do
                @store2 = create(:store, name: 'S2', distributor: @distributor)
                @movement8 = create(:money_credit, owner: @store2.third_party, destination: @distributor.third_party, direction: :input, date: 20.days.ago, value: 200, payment: @payment)
              end

              it 'lists the money credits for the third party from the distributor point of view ordered by date' do
                results = subject.search(@distributor)

                expect(results.size).to eq(1)
                expect(results).to eq([@movement8])
              end
            end
          end
        end
      end

      context 'for factory' do
        before do
          @administrator = create(:administrator, name: 'admin', username: 'admin').tap do |administrator|
            administrator.update!(selected_workplace: create(:user_workplace, workplace: @factory, user: administrator))
          end
        end

        context 'without search parameter' do
          it 'lists all money credits from the factory point of view ordered by date' do
            results = subject.search(@factory)

            expect(results.size).to eq(3)
            expect(results).to eq([@movement7, @movement4, @movement5])
          end
        end

        context 'with search parameter' do
          context 'for third party' do
            let(:parameters) { { 'third_party_id' => "#{@distributor2.third_party.id}", 'credit_type' => 'MoneyCredit' } }

            it 'lists the money credits for the third party from the factory point of view ordered by date' do
              results = subject.search(@factory)

              expect(results.size).to eq(1)
              expect(results).to eq([@movement7])
            end
          end

          context 'for from date' do
            let(:parameters) { { 'from' => "#{20.days.ago.strftime("%d/%m/%Y")}", 'credit_type' => 'MoneyCredit' } }

            it 'lists the money credits starting from the given date from the factory point of view ordered by date' do
              results = subject.search(@factory)

              expect(results.size).to eq(2)
            expect(results).to eq([@movement4, @movement5])
            end
          end

          context 'for until date' do
            let(:parameters) { { 'until' => "#{20.days.ago.strftime("%d/%m/%Y")}", 'credit_type' => 'MoneyCredit' } }

            it 'lists the money credits ending at the given date from the factory point of view ordered by date' do
              results = subject.search(@factory)

              expect(results.size).to eq(1)
              expect(results).to eq([@movement7])
            end
          end
        end
      end
    end

    context 'for production credit search' do
      let(:parameters) { { 'credit_type' => 'ProductionCredit' } }

      before do
        @payment = create(:payment)
        @movement1 = create(:production_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 20.days.ago, value: 200, payment: @payment)
        @movement2 = create(:production_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 25.days.ago, value: 300, order: @order, payment: @payment)
        @movement3 = create(:production_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :output, date: 2.days.ago, value: -400, order: @order, reason: :send_to_factory)
        
        create(:money_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 25.days.ago, value: 800, order: @order, payment: @payment)

        @movement4 = create(:production_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :input, date: 17.days.ago, value: 200, reason: :send_to_factory)
        @movement5 = create(:production_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :output, date: 15.days.ago, value: -50, order: @order, reason: :order_production)

        @movement6 = create(:production_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 20.days.ago, value: 333, status: :pending, payment: @payment)
        @movement7 = create(:production_credit, owner: @distributor2.third_party, destination: @factory.third_party, direction: :input, date: 21.days.ago, value: 333, status: :approved, payment: @payment)
        @movement8 = create(:production_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 25.days.ago, value: 300, order: @order, payment: @payment, status: :antecipated)
      end

      context 'for store' do
        before do
          @administrator = create(:administrator, name: 'admin', username: 'admin').tap do |administrator|
            administrator.update!(selected_workplace: create(:user_workplace, workplace: @store, user: administrator))
          end
        end

        context 'without search parameter' do
          it 'lists all the store\'s approved production credits ordered by date' do
            results = subject.search(@store)

            expect(results.size).to eq(4)
            expect(results).to eq([@movement2, @movement8, @movement1, @movement3])
          end
        end
      end

      context 'for distributor' do
        before do
          @administrator = create(:administrator, name: 'admin', username: 'admin').tap do |administrator|
            administrator.update!(selected_workplace: create(:user_workplace, workplace: @distributor, user: administrator))
          end
        end

        context 'when the type is credit_extract' do
          let(:parameters) { { 'credit_type' => 'ProductionCredit', 'type' => 'credit_extract' } }

          context 'without search parameter' do
            it 'lists all the distributor\'s approved money credits ordered by date' do
              results = subject.search(@distributor)

              expect(results.size).to eq(2)
              expect(results).to eq([@movement4, @movement5])
            end
          end
        end

        context 'when the type is conceded_credit' do
          let(:parameters) { { 'credit_type' => 'ProductionCredit', 'type' => 'conceded_credit' } }

          context 'without search parameter' do
            it 'lists all the approved money credits to the distributor ordered by date' do
              results = subject.search(@distributor)

              expect(results.size).to eq(4)
              expect(results).to eq([@movement2, @movement8, @movement1, @movement3])
            end
          end

          context 'with search parameter' do
            context 'for third party' do
              let(:parameters) { { 'credit_type' => 'ProductionCredit', 'type' => 'conceded_credit', 'third_party_id' => "#{@store2.third_party.id}" } }

              before do
                @store2 = create(:store, name: 'S2', distributor: @distributor)
                @movement8 = create(:production_credit, owner: @store2.third_party, destination: @distributor.third_party, direction: :input, date: 20.days.ago, value: 200, payment: @payment)
              end

              it 'lists the money credits for the third party from the distributor point of view ordered by date' do
                results = subject.search(@distributor)

                expect(results.size).to eq(1)
                expect(results).to eq([@movement8])
              end
            end
          end
        end
      end

      context 'for factory' do
        before do
          @administrator = create(:administrator, name: 'admin', username: 'admin').tap do |administrator|
            administrator.update!(selected_workplace: create(:user_workplace, workplace: @factory, user: administrator))
          end
        end

        context 'without search parameter' do
          it 'lists all production credits from the factory point of view ordered by date' do
            results = subject.search(@factory)

            expect(results.size).to eq(3)
            expect(results).to eq([@movement7, @movement4, @movement5])
          end
        end

        context 'with search parameter' do
          context 'for third party' do
            let(:parameters) { { 'third_party_id' => "#{@distributor2.third_party.id}", 'credit_type' => 'ProductionCredit' } }

            it 'lists the production credits for the third party from the factory point of view ordered by date' do
              results = subject.search(@factory)

              expect(results.size).to eq(1)
              expect(results).to eq([@movement7])
            end
          end

          context 'for from date' do
            let(:parameters) { { 'from' => "#{20.days.ago.strftime("%d/%m/%Y")}", 'credit_type' => 'ProductionCredit' } }

            it 'lists the production credits starting from the given date from the factory point of view ordered by date' do
              results = subject.search(@factory)

              expect(results.size).to eq(2)
            expect(results).to eq([@movement4, @movement5])
            end
          end

          context 'for until date' do
            let(:parameters) { { 'until' => "#{20.days.ago.strftime("%d/%m/%Y")}", 'credit_type' => 'ProductionCredit' } }

            it 'lists the production credits ending at the given date from the factory point of view ordered by date' do
              results = subject.search(@factory)

              expect(results.size).to eq(1)
              expect(results).to eq([@movement7])
            end
          end
        end
      end
    end
  end

  describe '#previous_balance' do
    let(:parameters) { { 'credit_type' => 'MoneyCredit' } }

    before do
      @factory = create(:factory, name: 'F1')
      @distributor = create(:distributor, name: 'D1')
      @store = create(:store, name: 'S1', distributor: @distributor)

      @user = create(:administrator).tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: @store, user: administrator))
      end

      @order = create(:order, seller: @store, client: @distributor, billing_client: @distributor, user: @user)
    end

    context 'for money credit' do
      before do
        @payment = create(:payment)
        @movement1 = create(:money_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 20.days.ago, value: 200, payment: @payment)
        @movement2 = create(:money_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 25.days.ago, value: 300, order: @order, payment: @payment)
        @movement3 = create(:money_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :output, date: 2.days.ago, value: -400, order: @order, reason: :send_to_factory)
        
        @movement4 = create(:money_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :input, date: 17.days.ago, value: 200, payment: @payment)
        @movement5 = create(:money_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :output, date: 15.days.ago, value: -50, order: @order, reason: :order_production)

        @movement6 = create(:money_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 25.days.ago, value: 300, status: :pending, order: @order, payment: @payment)
      end

      context 'for factory' do
        before do
          @administrator = create(:administrator, name: 'admin', username: 'admin').tap do |administrator|
            administrator.update!(selected_workplace: create(:user_workplace, workplace: @factory, user: administrator))
          end
        end

        context 'without search parameter' do
          it 'returns zero' do
            expect(subject.previous_balance(@factory)).to eq(0)
          end
        end

        context 'with search parameter' do
          context 'for from date' do
            let(:parameters) { { 'from' => "#{10.days.ago.strftime("%d/%m/%Y")}", 'credit_type' => 'MoneyCredit' } }

            it 'sums the credits before the given date from the factory point of view' do
              expect(subject.previous_balance(@factory)).to eq(-150)
            end
          end
        end
      end

      context 'for distributor' do
        before do
          @administrator = create(:administrator, name: 'admin', username: 'admin').tap do |administrator|
            administrator.update!(selected_workplace: create(:user_workplace, workplace: @distributor, user: administrator))
          end
        end

        context 'without search parameter' do
          it 'returns zero' do
            expect(subject.previous_balance(@distributor)).to eq(0)
          end
        end

        context 'with search parameter' do
          context 'for from date' do
            context 'when the type is credit_extract' do
              let(:parameters) { { 'from' => "#{10.days.ago.strftime("%d/%m/%Y")}", 'credit_type' => 'MoneyCredit', 'type' => 'credit_extract' } }

              it 'sums the credits before the given date' do
                expect(subject.previous_balance(@distributor)).to eq(150)
              end
            end

            context 'when the type is conceded_credit' do
              let(:parameters) { { 'from' => "#{10.days.ago.strftime("%d/%m/%Y")}", 'credit_type' => 'MoneyCredit', 'type' => 'conceded_credit' } }

              it 'sums the credits before the given date from the distributor point of view' do
                expect(subject.previous_balance(@distributor)).to eq(-500)
              end
            end
          end
        end
      end

      context 'for store' do
        before do
          @administrator = create(:administrator, name: 'admin', username: 'admin').tap do |administrator|
            administrator.update!(selected_workplace: create(:user_workplace, workplace: @store, user: administrator))
          end
        end

        context 'without search parameter' do
          it 'returns zero' do
            expect(subject.previous_balance(@store)).to eq(0)
          end
        end

        context 'with search parameter' do
          context 'for from date' do
            let(:parameters) { { 'from' => "#{10.days.ago.strftime("%d/%m/%Y")}", 'credit_type' => 'MoneyCredit' } }

            it 'sums the credits before the given date' do
              expect(subject.previous_balance(@store)).to eq(500)
            end
          end
        end
      end
    end

    context 'for production credit' do
      before do
        @payment = create(:payment)
        @movement1 = create(:production_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 20.days.ago, value: 200, payment: @payment)
        @movement2 = create(:production_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 25.days.ago, value: 300, order: @order, payment: @payment)
        @movement3 = create(:production_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :output, date: 2.days.ago, value: -400, order: @order, reason: :send_to_factory)
        
        @movement4 = create(:production_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :input, date: 17.days.ago, value: 200, payment: @payment)
        @movement5 = create(:production_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :output, date: 15.days.ago, value: -50, order: @order, reason: :order_production)

        @movement6 = create(:production_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 25.days.ago, value: 300, status: :pending, order: @order, payment: @payment)
      end

      context 'for factory' do
        before do
          @administrator = create(:administrator, name: 'admin', username: 'admin').tap do |administrator|
            administrator.update!(selected_workplace: create(:user_workplace, workplace: @factory, user: administrator))
          end
        end

        context 'without search parameter' do
          it 'returns zero' do
            expect(subject.previous_balance(@factory)).to eq(0)
          end
        end

        context 'with search parameter' do
          context 'for from date' do
            let(:parameters) { { 'from' => "#{10.days.ago.strftime("%d/%m/%Y")}", 'credit_type' => 'ProductionCredit' } }

            it 'sums the credits before the given date from the factory point of view' do
              expect(subject.previous_balance(@factory)).to eq(-150)
            end
          end
        end
      end

      context 'for distributor' do
        before do
          @administrator = create(:administrator, name: 'admin', username: 'admin').tap do |administrator|
            administrator.update!(selected_workplace: create(:user_workplace, workplace: @distributor, user: administrator))
          end
        end

        context 'without search parameter' do
          it 'returns zero' do
            expect(subject.previous_balance(@distributor)).to eq(0)
          end
        end

        context 'with search parameter' do
          context 'for from date' do
            context 'when the type is credit_extract' do
              let(:parameters) { { 'from' => "#{10.days.ago.strftime("%d/%m/%Y")}", 'credit_type' => 'ProductionCredit', 'type' => 'credit_extract' } }

              it 'sums the credits before the given date' do
                expect(subject.previous_balance(@distributor)).to eq(150)
              end
            end

            context 'when the type is conceded_credit' do
              let(:parameters) { { 'from' => "#{10.days.ago.strftime("%d/%m/%Y")}", 'credit_type' => 'ProductionCredit', 'type' => 'conceded_credit' } }

              it 'sums the credits before the given date from the distributor point of view' do
                expect(subject.previous_balance(@distributor)).to eq(-500)
              end
            end
          end
        end
      end

      context 'for store' do
        before do
          @administrator = create(:administrator, name: 'admin', username: 'admin').tap do |administrator|
            administrator.update!(selected_workplace: create(:user_workplace, workplace: @store, user: administrator))
          end
        end

        context 'without search parameter' do
          it 'returns zero' do
            expect(subject.previous_balance(@store)).to eq(0)
          end
        end

        context 'with search parameter' do
          context 'for from date' do
            let(:parameters) { { 'from' => "#{10.days.ago.strftime("%d/%m/%Y")}", 'credit_type' => 'ProductionCredit' } }

            it 'sums the credits before the given date' do
              expect(subject.previous_balance(@store)).to eq(500)
            end
          end
        end
      end
    end
  end
end