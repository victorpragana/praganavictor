require 'rails_helper'

RSpec.describe CategoryService, type: :service do
  describe "create" do
    let(:service) {described_class.new(parameters)}

    context "with invalid parameters" do
      let(:parameters) {{"name" => ""}}

      it "does not create a new Category" do
        expect {
          service.create
        }.to change(Category, :count).by(0)
      end

      it "should return false" do
        service.create
        
        expect(service.success).to eq(false)
      end

      it "should return the errors" do
        service.create
        
        expect(service.errors).to_not be_empty
      end
    end

    context "with valid parameters" do
      let(:parameters) {{"name" => "Acionamento", 'use_cut_table' => 'true'}}

      it "should create a new Category" do
        expect {
          service.create
        }.to change(Category, :count).by(1)
      end

      it "should return the created category" do
        service.create

        expect(service.record).to eq(Category.last)
      end

      it "should return true" do
        service.create
        
        expect(service.success).to eq(true)
      end

      it "populates the data" do
        service.create

        category = service.record

        expect(category.name).to eq("Acionamento")
        expect(category.use_cut_table?).to be_truthy
      end
    end
  end

  describe "update" do
    let(:service) {described_class.new(parameters)}

    before(:each) do
      @category = FactoryGirl.create(:category, name: "ab")
    end

    context "with invalid parameters" do
      let(:parameters) {{"name" => ""}}

      it "should return false" do
        service.update(@category.id)
        
        expect(service.success).to eq(false)
      end

      it "should return the errors" do
        service.update(@category.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context "with valid parameters" do
      let(:parameters) {{"name" => "abc", 'use_cut_table' => 'true'}}

      it "does not create a new Category" do
        expect {
          service.update(@category.id)
        }.to change(Category, :count).by(0)
      end

      it "should return the category" do
        service.update(@category.id)

        expect(service.record).to eq(Category.last)
      end

      it "should return true" do
        service.update(@category.id)
        
        expect(service.success).to eq(true)
      end

      it "populates the data" do
        service.update(@category.id)

        category = service.record

        expect(category.name).to eq("abc")
        expect(category.use_cut_table?).to be_truthy
      end
    end
  end
end