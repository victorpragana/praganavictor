require 'rails_helper'

RSpec.describe PaymentService, type: :service do
 describe '#create' do
    let(:service) { described_class.new(parameters) }
    let(:charge)  { double(:charge, success: true, url: 'http://foo.com.br/bank_slip', invoice_id: 'FOOBAR1234', id: 123) }
    let(:value)   { 100 }
    let(:installments) { 3 }
    let(:movement_creator) { double(:movement_creator, input: true, success: true) }

    before do
      @factory = create(:factory, name: 'F1', subdomain: 'factory-f1')
      @distributor = create(:distributor)
      @store = create(:store, distributor: @distributor)
      @third_party = create(:third_party, subdomain: 'tp')
      @state = create(:state)
      @city = create(:city, state: @state)
      @customer = create(:customer, third_party: @third_party, store: @store, city: @city)

      @user = create(:administrator).tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: @store, user: administrator))
      end

      @family = create(:family)
      @line = create(:line, family: @family, sale_price: BigDecimal.new('12.50'))

      @order = create(:order, seller: @store, client: @customer, billing_client: @customer, user: @user)

      create(:order_item, factory_total: BigDecimal.new('100.22'), distributor_total: 120, line: @line, order: @order,
                          store_total: 140, customer_total: 160)
      create(:order_item, factory_total: 80, distributor_total: 100, line: @line, order: @order,
                          store_total: 160, customer_total: 200, discount: BigDecimal.new('32.50'))

      @payment_condition = create(:payment_condition, additional_tax: BigDecimal.new('5.25'), credit_tax: BigDecimal.new('3.25'), installments: installments, type: :credit_card)

      allow(Iugu::Charge).to receive(:create).and_return(charge)
      allow(MovementCreatorService).to receive(:new).and_return(movement_creator)
    end

    context 'with valid parameters' do
      describe 'paying with credit_card' do
        let(:credit_type) { 'ProductionCredit' }
        let(:parameters) { { 'value' => "#{value}", 'credit_card_iugu_token' => '2222', 'payment_method' => 'credit_card', 'payment_condition_id' => "#{@payment_condition.id}", 'credit_type' => credit_type }.merge('third_party_id' => @user.workplace.third_party.id) }

        it 'creates a new payment' do
          expect do
            service.create(nil)
          end.to change(Payment, :count).by(1)
        end

        context 'when the order id is given' do
          context 'for production credit' do
            let(:credit_type) { 'ProductionCredit' }

            it 'creates an iugu charge with the order data description applying the payment condition credit tax and number of installments' do
              expect(Iugu::Charge).to receive(:create).with(
                'token' => '2222',
                'email' => PaymentService::DESTINATION_EMAIL,
                'cc_emails' => @order.seller.third_party.email,
                'months' => "#{installments}",
                'items' =>
                  [
                    {
                      'description' => "#{@order.billing_client.third_party.name} - Nº DO PEDIDO: #{@order.id.to_s}",
                      'quantity' => '1',
                      'price_cents' => '10325'
                    }
                  ]
              )
              service.create(@order.id)
            end
          end

          context 'for money credit' do
            let(:credit_type) { 'MoneyCredit' }

            it 'creates an iugu charge with the order data description applying the payment condition additional tax and number of installments' do
              expect(Iugu::Charge).to receive(:create).with(
                'token' => '2222',
                'email' => PaymentService::DESTINATION_EMAIL,
                'cc_emails' => @order.seller.third_party.email,
                'months' => "#{installments}",
                'items' =>
                  [
                    {
                      'description' => "#{@order.billing_client.third_party.name} - Nº DO PEDIDO: #{@order.id.to_s}",
                      'quantity' => '1',
                      'price_cents' => '10525'
                    }
                  ]
              )
              service.create(@order.id)
            end
          end

          context 'for distributor seller' do
            before do
              @order.update(seller: @distributor)
            end

            it 'creates the movements using the movement creator passing the distributor as owner and the factory as destination' do
              expect(MovementCreatorService).to receive(:new).with(credit_type.constantize, instance_of(Payment), @order)
              expect(movement_creator).to receive(:input).with(@order.seller.third_party, @factory.third_party)

              service.create(@order.id)
            end
          end

          context 'for store seller' do
            before do
              @order.update(seller: @store)
            end

            it 'creates the movements using the movement creator passing the store as owner and the distributor as destination' do
              expect(MovementCreatorService).to receive(:new).with(credit_type.constantize, instance_of(Payment), @order)
              expect(movement_creator).to receive(:input).with(@order.seller.third_party, @store.distributor.third_party)

              service.create(@order.id)
            end
          end
        end

        context 'when the order id is not given' do
          context 'for production credit' do
            let(:credit_type) { 'ProductionCredit' }

            it 'creates an iugu charge with the credit description applying the payment condition credit tax and number of installments' do
              expect(Iugu::Charge).to receive(:create).with(
                'token' => '2222',
                'email' => PaymentService::DESTINATION_EMAIL,
                'cc_emails' => @order.seller.third_party.email,
                'months' => "#{installments}",
                'items' =>
                  [
                    {
                      'description' => "Crédito para: #{@user.workplace.third_party.name}",
                      'quantity' => '1',
                      'price_cents' => '10325'
                    }
                  ]
              )
              service.create(nil)
            end
          end

          context 'for money credit' do
            let(:credit_type) { 'MoneyCredit' }

            it 'creates an iugu charge with the credit description applying the payment condition additional tax and number of installments' do
              expect(Iugu::Charge).to receive(:create).with(
                'token' => '2222',
                'email' => PaymentService::DESTINATION_EMAIL,
                'cc_emails' => @order.seller.third_party.email,
                'months' => "#{installments}",
                'items' =>
                  [
                    {
                      'description' => "Crédito para: #{@user.workplace.third_party.name}",
                      'quantity' => '1',
                      'price_cents' => '10525'
                    }
                  ]
              )
              service.create(nil)
            end
          end

          context 'for distributor workplace' do
            before do
              @user.update!(selected_workplace: create(:user_workplace, workplace: @distributor, user: @user))
            end

            it 'creates the movements using the movement creator passing the distributor as owner and the factory as destination' do
              expect(MovementCreatorService).to receive(:new).with(credit_type.constantize, instance_of(Payment), nil)
              expect(movement_creator).to receive(:input).with(@user.workplace.third_party, @factory.third_party)

              service.create(nil)
            end
          end

          context 'for store seller' do
            before do
              @user.update!(selected_workplace: create(:user_workplace, workplace: @store, user: @user))
            end

            it 'creates the movements using the movement creator passing the store as owner and the distributor as destination' do
              expect(MovementCreatorService).to receive(:new).with(credit_type.constantize, instance_of(Payment), nil)
              expect(movement_creator).to receive(:input).with(@user.workplace.third_party, @user.workplace.distributor.third_party)

              service.create(nil)
            end
          end
        end

        it 'populates the payment data' do
          service.create(nil)

          payment = service.record

          expect(payment.original_value).to eq(BigDecimal.new('100.00'))
          expect(payment.tax).to eq(BigDecimal.new('3.25'))
          expect(payment.value).to eq(BigDecimal.new('103.25'))
          expect(payment.approved?).to eq(true)
          expect(payment.credit_card?).to eq(true)
          expect(payment.installment_number).to eq(installments)
          expect(payment.bank_slip_url).to eq(nil)
          expect(payment.iugu_invoice_id).to eq('FOOBAR1234')
        end
      end

      describe 'paying with bank slip' do
        let(:credit_type) { 'ProductionCredit' }
        let(:parameters) { { 'value' => '100', 'payment_method' => 'bank_slip', 'payment_condition_id' => "#{@payment_condition.id}", 'credit_type' => credit_type }.merge('third_party_id' => @user.workplace.third_party.id) }

        before do
          @payment_condition = create(:payment_condition, additional_tax: BigDecimal.new('5.55'), credit_tax: BigDecimal.new('2.55'), installments: 1, type: :bank_slip)
        end

        it 'creates a new payment' do
          expect do
            service.create(nil)
          end.to change(Payment, :count).by(1)
        end

        context 'when the order id is given' do
          context 'for production credit' do
            let(:credit_type) { 'ProductionCredit' }

            it 'creates an iugu charge with the order data description applying the payment condition credit tax and number of installments' do
              expect(Iugu::Charge).to receive(:create).with(
                'method' => 'bank_slip',
                'email' => PaymentService::DESTINATION_EMAIL,
                'cc_emails' => @order.seller.third_party.email,
                'items' =>
                  [
                    {
                      'description' => "#{@order.billing_client.third_party.name} - Nº DO PEDIDO: #{@order.id.to_s}",
                      'quantity' => '1',
                      'price_cents' => '10255'
                    }
                  ],
                'payer' =>
                  {
                    'cpf_cnpj' => @order.seller.third_party.identification,
                    'name' => @order.seller.third_party.name,
                    'email' => @order.seller.third_party.email,
                    'address' => {
                      'street' => @order.seller.third_party.street,
                      'number' => @order.seller.third_party.number,
                      'city' => @order.seller.third_party.city.try(:name),
                      'state' => @order.seller.third_party.city.try(:state).try(:name),
                      'zip_code' => @order.seller.third_party.zip_code
                    }
                  }
              )
              service.create(@order.id)
            end
          end

          context 'for money credit' do
            let(:credit_type) { 'MoneyCredit' }

            it 'creates an iugu charge with the order data description applying the payment condition additional tax and number of installments' do
              expect(Iugu::Charge).to receive(:create).with(
                'method' => 'bank_slip',
                'email' => PaymentService::DESTINATION_EMAIL,
                'cc_emails' => @order.seller.third_party.email,
                'items' =>
                  [
                    {
                      'description' => "#{@order.billing_client.third_party.name} - Nº DO PEDIDO: #{@order.id.to_s}",
                      'quantity' => '1',
                      'price_cents' => '10555'
                    }
                  ],
                'payer' =>
                  {
                    'cpf_cnpj' => @order.seller.third_party.identification,
                    'name' => @order.seller.third_party.name,
                    'email' => @order.seller.third_party.email,
                    'address' => {
                      'street' => @order.seller.third_party.street,
                      'number' => @order.seller.third_party.number,
                      'city' => @order.seller.third_party.city.try(:name),
                      'state' => @order.seller.third_party.city.try(:state).try(:name),
                      'zip_code' => @order.seller.third_party.zip_code
                    }
                  }
              )
              service.create(@order.id)
            end
          end

          context 'for distributor seller' do
            before do
              @order.update(seller: @distributor)
            end

            it 'creates the movements using the movement creator passing the distributor as owner and the factory as destination' do
              expect(MovementCreatorService).to receive(:new).with(credit_type.constantize, instance_of(Payment), @order)
              expect(movement_creator).to receive(:input).with(@order.seller.third_party, @factory.third_party)

              service.create(@order.id)
            end
          end

          context 'for store seller' do
            before do
              @order.update(seller: @store)
            end

            it 'creates the movements using the movement creator passing the store as owner and the distributor as destination' do
              expect(MovementCreatorService).to receive(:new).with(credit_type.constantize, instance_of(Payment), @order)
              expect(movement_creator).to receive(:input).with(@order.seller.third_party, @store.distributor.third_party)

              service.create(@order.id)
            end
          end
        end

        context 'when the order id is not given' do
          context 'for production credit' do
            let(:credit_type) { 'ProductionCredit' }

            it 'creates an iugu charge with the credit description applying the payment condition credit tax and number of installments' do
              expect(Iugu::Charge).to receive(:create).with(
                'method' => 'bank_slip',
                'email' => PaymentService::DESTINATION_EMAIL,
                'cc_emails' => @order.seller.third_party.email,
                'items' =>
                  [
                    {
                      'description' => "Crédito para: #{@user.workplace.third_party.name}",
                      'quantity' => '1',
                      'price_cents' => '10255'
                    }
                  ],
                'payer' =>
                  {
                    'cpf_cnpj' => @user.workplace.third_party.identification,
                    'name' => @user.workplace.third_party.name,
                    'email' => @user.workplace.third_party.email,
                    'address' => {
                      'street' => @user.workplace.third_party.street,
                      'number' => @user.workplace.third_party.number,
                      'city' => @user.workplace.third_party.city.try(:name),
                      'state' => @user.workplace.third_party.city.try(:state).try(:name),
                      'zip_code' => @user.workplace.third_party.zip_code
                    }
                  }
              )
              service.create(nil)
            end
          end

          context 'for money credit' do
            let(:credit_type) { 'MoneyCredit' }

            it 'creates an iugu charge with the credit description applying the payment condition additional tax and number of installments' do
              expect(Iugu::Charge).to receive(:create).with(
                'method' => 'bank_slip',
                'email' => PaymentService::DESTINATION_EMAIL,
                'cc_emails' => @order.seller.third_party.email,
                'items' =>
                  [
                    {
                      'description' => "Crédito para: #{@user.workplace.third_party.name}",
                      'quantity' => '1',
                      'price_cents' => '10555'
                    }
                  ],
                'payer' =>
                  {
                    'cpf_cnpj' => @user.workplace.third_party.identification,
                    'name' => @user.workplace.third_party.name,
                    'email' => @user.workplace.third_party.email,
                    'address' => {
                      'street' => @user.workplace.third_party.street,
                      'number' => @user.workplace.third_party.number,
                      'city' => @user.workplace.third_party.city.try(:name),
                      'state' => @user.workplace.third_party.city.try(:state).try(:name),
                      'zip_code' => @user.workplace.third_party.zip_code
                    }
                  }
              )
              service.create(nil)
            end
          end

          context 'for distributor workplace' do
            before do
              @user.update!(selected_workplace: create(:user_workplace, workplace: @distributor, user: @user))
            end

            it 'creates the movements using the movement creator passing the distributor as owner and the factory as destination' do
              expect(MovementCreatorService).to receive(:new).with(credit_type.constantize, instance_of(Payment), nil)
              expect(movement_creator).to receive(:input).with(@user.workplace.third_party, @factory.third_party)

              service.create(nil)
            end
          end

          context 'for store seller' do
            before do
              @user.update!(selected_workplace: create(:user_workplace, workplace: @store, user: @user))
            end

            it 'creates the movements using the movement creator passing the store as owner and the distributor as destination' do
              expect(MovementCreatorService).to receive(:new).with(credit_type.constantize, instance_of(Payment), nil)
              expect(movement_creator).to receive(:input).with(@user.workplace.third_party, @user.workplace.distributor.third_party)

              service.create(nil)
            end
          end
        end

        it 'populates the data' do
          service.create(nil)

          payment = service.record

          expect(payment.original_value).to eq(BigDecimal.new('100.00'))
          expect(payment.tax).to eq(BigDecimal.new('2.55'))
          expect(payment.value).to eq(BigDecimal.new('102.55'))
          expect(payment.created?).to eq(true)
          expect(payment.bank_slip?).to eq(true)
          expect(payment.installment_number).to eq(1)
          expect(payment.bank_slip_url).to eq('http://foo.com.br/bank_slip')
          expect(payment.iugu_invoice_id).to eq('FOOBAR1234')
        end
      end
    end
    

    context 'with invalid parameters' do
      let(:parameters) { { 'value' => '10000', 'credit_card_iugu_token' => '2222', 'payment_method' => 'bank_slip', 'payment_condition_id' => "#{@payment_condition.id}", 'credit_type' => 'ProductionCredit' }.merge('third_party_id' => @third_party.id) }

      context 'when customer has no address' do 
        before do
          @third_party.update_attribute(:zip_code, nil)
        end

        it 'does not create a new payment' do
          expect do
            service.create(nil)
          end.to_not change(Payment, :count)
        end

        it 'does not create a new movement' do
          expect do
            service.create(nil)
          end.to_not change(Movement, :count)
        end

        it 'returns false' do
          service.create(nil)

          expect(service.success).to eq(false)
        end

        it 'returns the errors' do
          service.create(nil)

          expect(service.errors).to eq(['Cliente sem endereço cadastrado'])
        end

        it 'does not create an iugu charge' do
          expect(Iugu::Charge).to_not receive(:create)
          service.create(nil)
        end
      end
      
      context 'when customer has no identification' do 
        before do
          @store.update_attribute(:identification, nil)
        end

        it 'does not create a new payment' do
          expect do
            service.create(@order.id)
          end.to_not change(Payment, :count)
        end

        it 'does not create a new movement' do
          expect do
            service.create(@order.id)
          end.to_not change(Movement, :count)
        end

        it 'returns false' do 
          service.create(@order.id)

          expect(service.success).to eq(false)
        end

        it 'returns errors' do 
          service.create(@order.id)

          expect(service.errors).to eq(['Cliente sem CPF/CNPJ cadastrado'])
        end
      end

      context 'when the third party is the factory' do
        context 'for orders' do
          before do
            @order.update(seller: @factory)
          end

          it 'does not create a new payment' do
            expect do
              service.create(@order.id)
            end.to_not change(Payment, :count)
          end

          it 'does not create a new movement' do
            expect do
              service.create(@order.id)
            end.to_not change(Movement, :count)
          end

          it 'returns false' do 
            service.create(@order.id)

            expect(service.success).to eq(false)
          end

          it 'returns errors' do 
            service.create(@order.id)

            expect(service.errors).to eq(['A fábrica não pode realizar pagamentos'])
          end
        end

        context 'without orders' do
          before do
            @third_party = @factory.third_party
          end

          it 'does not create a new payment' do
            expect do
              service.create(nil)
            end.to_not change(Payment, :count)
          end

          it 'does not create a new movement' do
            expect do
              service.create(nil)
            end.to_not change(Movement, :count)
          end

          it 'returns false' do 
            service.create(nil)

            expect(service.success).to eq(false)
          end

          it 'returns errors' do 
            service.create(nil)

            expect(service.errors).to eq(['A fábrica não pode realizar pagamentos'])
          end
        end
      end
    end

    context 'with failed iugu charge' do
      let(:charge)     { double(:charge, success: false, errors: 'foo error', invoice_id: 'FOOBAR1234', id: nil) }
      let(:parameters) { { 'value' => '100', 'credit_card_iugu_token' => '2222', 'payment_method' => 'credit_card', 'payment_condition_id' => "#{@payment_condition.id}", 'credit_type' => 'ProductionCredit' }.merge('third_party_id' => @third_party.id) }

      before do
        allow(Iugu::Charge).to receive(:create).and_return(charge)
      end

      it 'does not create a new payment' do
        expect do
          service.create(nil)
        end.to_not change(Payment, :count)
      end

      it 'does not create a movement' do
        expect do
          service.create(nil)
        end.to_not change(Movement, :count)
      end

      it 'returns false' do
        service.create(nil)

        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.create(nil)

        expect(service.errors).to match_array(['foo error'])
      end

      context 'with error array' do
        let(:charge) { double(:charge, success: false, errors: { 'foo' => 'bar', 'baz' => 'batz' }, invoice_id: 'FOOBAR1234', id: nil) }

        it 'returns the errors' do
          service.create(nil)

          expect(service.errors).to match_array(['baz, batz', 'foo, bar'])
        end
      end
    end
  end

  describe 'status_changed' do
    let(:service) { described_class.new(parameters) }

    before do
      @factory = create(:factory, name: 'F1')
      @distributor = create(:distributor, name: 'D1')
      @store = create(:store, name: 'S1', distributor: @distributor)

      @user = create(:administrator).tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: @store, user: administrator))
      end

      @order = create(:order, seller: @distributor, client: @store, billing_client: @store, user: @user)

      @payment1 = create(:payment, iugu_invoice_id: '1757E1D7FD5E410A9C563024250015BF', status: :created, payment_method: :bank_slip)
      
      @movimentation1 = create(:movement, owner: @distributor.third_party, destination: @factory.third_party, payment: @payment1, direction: :input, date: 20.days.ago, value: 200, status: :pending)
      @movimentation2 = create(:movement, owner: @distributor.third_party, destination: @factory.third_party, order: @order, payment: @payment1, direction: :input, date: 25.days.ago, value: 300, status: :pending)
    end

    context 'with valid parameters' do
      let(:parameters) { { 'id' => @payment1.iugu_invoice_id.to_s, 'status' => 'paid', 'account_id' => '70CA234077134ED0BF2E0E46B0EDC36F', 'subscription_id' => 'F4115E5E28AE4CCA941FCCCCCABE9A0A' } }

      it 'returns true' do
        service.status_changed

        expect(service.success).to eq(true)
      end

      it 'updates the payment status to approved' do
        service.status_changed

        expect(Payment.find(@payment1.id).approved?).to eq(true)
      end

      it 'updates all orders to approved' do 
        service.status_changed

        expect(Movement.find(@movimentation1.id).approved?).to eq(true)
        expect(Movement.find(@movimentation2.id).approved?).to eq(true)
      end
    end

    context 'with invalid parameters' do
      let(:parameters) { { 'id' => '', 'status' => 'paid', 'account_id' => '', 'subscription_id' => '' } }

      it 'returns false' do
        service.status_changed

        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.status_changed

        expect(service.errors).to_not be_empty
      end

      it 'does not change the payment status to approved' do
        service.status_changed
        expect(Payment.find(@payment1.id).approved?).to eq(false)
      end
    end
  end
end
