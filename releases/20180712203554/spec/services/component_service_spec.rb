require 'rails_helper'

RSpec.describe ComponentService, type: :service do
  before(:each) do
    @family    = create(:family)
    @category  = create(:category, use_cut_table: true)
    @cut_table = create(:cut_table)
  end

  describe 'create' do
    let(:service) { described_class.new(parameters) }

    context 'with invalid parameters' do
      let(:parameters) { {'name' => ''} }

      it 'does not create a new Component' do
        expect {
          service.create
        }.to change(Component, :count).by(0)
      end

      it 'returns false' do
        service.create
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.create
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) {{'name' => 'Manual Mola', 'unit'=>"#{Component.units[:un]}", 'category_id' => "#{@category.id}", 
        'cost' => '2.54', 'offset_left' => '5', 'offset_right' => '7', 'offset_top' => '15', 'offset_bottom' => '17',
        'allow_rotation' => 'true', 'exchange' => '2.69', 'loss' => '1.53', 'cut_table_id' => "#{@cut_table.id}",
        'fix_cost' => '5.54', 'code' => 'A12', 'reference_cost' => '1.54', 'print_on_components_label' => 'true', 'print_on_lines_label' => 'true',
        'family_ids' => ["#{@family.id}"]}}

      it 'creates a new Component' do
        expect {
          service.create
        }.to change(Component, :count).by(1)
      end

      it 'returns the created Component' do
        service.create

        expect(service.record).to eq(Component.last)
      end

      it 'returns true' do
        service.create
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.create

        component = service.record

        expect(component.name).to eq('Manual Mola')
        expect(component.cost).to eq(BigDecimal.new('2.54'))
        expect(component.reference_cost).to eq(BigDecimal.new('1.54'))
        expect(component.exchange).to eq(BigDecimal.new('2.69'))
        expect(component.loss).to eq(BigDecimal.new('1.53'))
        expect(component.code).to eq('A12')
        expect(Component.units[component.unit]).to eq(Component.units[:un])
        expect(component.category_id).to eq(@category.id)
        expect(component.family_ids).to match_array([@family.id])
        expect(component.offset_left).to eq(5)
        expect(component.offset_top).to eq(15)
        expect(component.offset_right).to eq(7)
        expect(component.offset_bottom).to eq(17)
        expect(component.allow_rotation?).to eq(true)
        expect(component.cut_table).to eq(@cut_table)
        expect(component.print_on_components_label?).to be_truthy
        expect(component.print_on_lines_label?).to be_truthy
      end
    end
  end

  describe 'update' do
    let(:service) {described_class.new(parameters)}

    before(:each) do
      @category_2 = create(:category)
      @component = create(:component, category: @category_2)
    end

    context 'with invalid parameters' do
      let(:parameters) {{'name' => ''}}

      it 'returns false' do
        service.update(@component.id)
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.update(@component.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) {{'name' => 'Manual Mola', 'unit'=>"#{Component.units[:un]}", 'category_id' => "#{@category.id}", 
        'cost' => '2.54', 'offset_left' => '5', 'offset_right' => '7', 'offset_top' => '15', 'offset_bottom' => '17',
        'allow_rotation' => 'true', 'exchange' => '2.69', 'loss' => '1.53', 'cut_table_id' => "#{@cut_table.id}",
        'fix_cost' => '5.54', 'code' => 'A12', 'reference_cost' => '1.54', 'print_on_components_label' => 'true', 'print_on_lines_label' => 'true',
        'family_ids' => ["#{@family.id}"]}}

      it 'does not create a new Component' do
        expect {
          service.update(@component.id)
        }.to change(Component, :count).by(0)
      end

      it 'returns the component' do
        service.update(@component.id)

        expect(service.record).to eq(Component.last)
      end

      it 'returns true' do
        service.update(@component.id)
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.update(@component.id)

        component = service.record

        expect(component.name).to eq('Manual Mola')
        expect(component.cost).to eq(BigDecimal.new('2.54'))
        expect(component.reference_cost).to eq(BigDecimal.new('1.54'))
        expect(component.exchange).to eq(BigDecimal.new('2.69'))
        expect(component.loss).to eq(BigDecimal.new('1.53'))
        expect(component.code).to eq('A12')
        expect(Component.units[component.unit]).to eq(Component.units[:un])
        expect(component.category_id).to eq(@category.id)
        expect(component.family_ids).to match_array([@family.id])
        expect(component.offset_left).to eq(5)
        expect(component.offset_top).to eq(15)
        expect(component.offset_right).to eq(7)
        expect(component.offset_bottom).to eq(17)
        expect(component.allow_rotation?).to eq(true)
        expect(component.cut_table).to eq(@cut_table)
        expect(component.print_on_components_label).to be_truthy
        expect(component.print_on_lines_label?).to be_truthy
      end
    end
  end

  describe 'clone_kit' do
    let(:service) { described_class.new(parameters) }

    before(:each) do
      @category_1 = create(:category, name: 'C1')
      @category_2 = create(:category, name: 'C2')
      @category_3 = create(:category, name: 'C3')

      @child_1 = create(:component, unit: :cm, name: 'C1', category: @category_1, families: [@family])
      @child_2 = create(:component, unit: :cm, name: 'C2', category: @category_2)
      @child_3 = create(:component, unit: :cm, name: 'C3', category: @category_3)
      @child_4 = create(:component, unit: :cm, name: 'C4', category: @category_3, erasable: false)

      @component   = create(:component, unit: :cm, category: @category_2, children: [@child_1, @child_4])
      @component_2 = create(:component, unit: :cm, category: @category_2, children: [])
    end

    context 'with invalid parameters' do
      let(:parameters) {{'component_id' => ''}}

      it 'returns false' do
        service.clone_kit(@component_2.id)
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.clone_kit(@component_2.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) {{'component_id' => "#{@component.id}"}}

      it 'returns the component' do
        service.clone_kit(@component_2.id)

        expect(service.record).to eq(@component_2)
      end

      it 'returns true' do
        service.clone_kit(@component_2.id)
        
        expect(service.success).to eq(true)
      end

      it 'should clone the kit configuration' do
        service.clone_kit(@component_2.id)

        component = service.record

        expect(@component.children).to match_array([@child_1, @child_4])
        expect(component.children).to match_array(@component.children)
      end
    end
  end

  describe 'create_rules' do
    let(:service) { described_class.new(parameters) }

    before(:each) do
      @category = create(:category, name: 'C1')
      @component = create(:component, unit: :cm, name: 'C1', category: @category, families: [@family])

      @line_1 = create(:line, family: @family, name: "L1", active: true)
      @line_rule_1 = create(:line_rule, line: @line_1, control_method: LineRule.control_methods[:width],
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_',
        width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', automatic: true, category_id: @category.id)

      @line_2 = create(:line, family: @family, name: "L2", active: true)
      @line_rule_2 = create(:line_rule, line: @line_2, control_method: LineRule.control_methods[:width],
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_',
        width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', automatic: true, category_id: @category.id)
    end

    context 'with invalid parameters' do
      let(:parameters) {{'rule_ids' => ''}}

      it 'returns false' do
        service.create_rules(@component.id)
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.create_rules(@component.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) {{'rule_ids' => ["#{@line_rule_1.id}", "#{@line_rule_2.id}"]}}

      it 'returns true' do
        service.create_rules(@component.id)
        
        expect(service.success).to eq(true)
      end

      it 'creates 2 rules' do
        expect do
          service.create_rules(@component.id)
        end.to change(Rule, :count).by(2)
      end

      it 'adds the rules to the corresponding line rules' do
        service.create_rules(@component.id)

        expect(@line_rule_1.reload.rules.find_by(component_id: @component.id)).to_not be_nil
        expect(@line_rule_2.reload.rules.find_by(component_id: @component.id)).to_not be_nil
      end
    end
  end

  describe '#activate' do
    let(:service) { described_class.new }

    before do
      @category = create(:category, name: 'C1')
      @component = create(:component, category: @category)
      @component.destroy
      @component.reload
    end

    it 'activates the component' do
      service.activate(@component.id)

      expect(service.record.deleted?).to eq(false)
    end
  end
end
