require 'rails_helper'

RSpec.describe PriceTableService, type: :service do
  before(:each) do
    @family = FactoryGirl.create(:family, name: 'F1')
    @distributor = FactoryGirl.create(:distributor, name: 'D1')

    @owner = [FactoryGirl.build(:factory, name: 'F1'), @distributor, FactoryGirl.create(:store, distributor: @distributor)].sample

    @line   = FactoryGirl.create(:line, family: @family, name: 'L1')
    @line_2 = FactoryGirl.create(:line, family: @family, name: 'L2')

    @item   = FactoryGirl.create(:item)
    @item_2 = FactoryGirl.create(:item)
  end

  describe 'create' do
    let(:service) {described_class.new(parameters)}

    context 'with invalid parameters' do
      context 'for lines only table' do
        let(:parameters) { {'name' => '', 'type' => '0', 'default' => 'true', 'price_table_lines' => 
          [
            {'price' => '', 'line' => {'id' => @line.id}},
            {'price' => '', 'line' => {'id' => @line_2.id}}
          ]
        } }

        it 'does not create a new Price Table' do
          expect {
            service.create
          }.to change(PriceTable, :count).by(0)
        end

        it 'does not create any new Price Table Line' do
          expect {
            service.create
          }.to change(PriceTableLine, :count).by(0)
        end

        it 'returns false' do
          service.create
          
          expect(service.success).to eq(false)
        end

        it 'returns the errors' do
          service.create
          
          expect(service.errors).to_not be_empty
        end
      end

      context 'for items only table' do
        let(:parameters) { {'name' => '', 'type' => '1', 'default' => 'true', 'price_table_items' => 
          [
            {'price' => '', 'item' => {'id' => @item.id}},
            {'price' => '', 'item' => {'id' => @item_2.id}}
          ]
        } }

        it 'does not create a new Price Table' do
          expect {
            service.create
          }.to change(PriceTable, :count).by(0)
        end

        it 'does not create any new Price Table Item' do
          expect {
            service.create
          }.to change(PriceTableItem, :count).by(0)
        end

        it 'returns false' do
          service.create
          
          expect(service.success).to eq(false)
        end

        it 'returns the errors' do
          service.create
          
          expect(service.errors).to_not be_empty
        end
      end
    end

    context 'with valid parameters' do
      context 'for lines only table' do
        let(:parameters) { {'name' => 'Foo', 'type' => '0', 'default' => 'true', 'price_table_lines' => 
          [
            {'price' => '_CMV_', 'maximum_discount' => '12.50', 'line' => {'id' => @line.id}},
            {'price' => '_CMV_ * 2', 'maximum_discount' => '14.20','line' => {'id' => @line_2.id}}
          ], 'owner' => @owner
        } }

        it 'creates a new price table' do
          expect {
            service.create
          }.to change(PriceTable, :count).by(1)
        end

        it 'creates two new price table line' do
          expect {
            service.create
          }.to change(PriceTableLine, :count).by(2)
        end

        it 'returns the created price table' do
          service.create

          expect(service.record).to eq(PriceTable.last)
        end

        it 'returns true' do
          service.create
          
          expect(service.success).to eq(true)
        end

        it 'populates the data' do
          service.create

          price_table = service.record

          expect(price_table.name).to eq('Foo')
          expect(price_table.default?).to eq(true)
          expect(price_table.owner).to eq(@owner)

          lines = price_table.lines.order(:id)

          expect(lines[0].price).to eq('_CMV_')
          expect(lines[0].line_id).to eq(@line.id)
          expect(lines[0].maximum_discount).to eq(BigDecimal.new('12.50'))
          expect(lines[1].price).to eq('_CMV_*2')
          expect(lines[1].line_id).to eq(@line_2.id)
          expect(lines[1].maximum_discount).to eq(BigDecimal.new('14.20'))
        end
      end

      context 'for items only table' do
        let(:parameters) { {'name' => 'Foo', 'type' => '1', 'default' => 'true', 'price_table_items' => 
          [
            {'price' => '15.22', 'maximum_discount' => '10', 'item' => {'id' => @item.id}},
            {'price' => '13.44', 'maximum_discount' => '9.80', 'item' => {'id' => @item_2.id}}
          ], 'owner' => @owner
        } }

        it 'creates a new price table' do
          expect {
            service.create
          }.to change(PriceTable, :count).by(1)
        end

        it 'creates two new price table item' do
          expect {
            service.create
          }.to change(PriceTableItem, :count).by(2)
        end

        it 'returns the created price table' do
          service.create

          expect(service.record).to eq(PriceTable.last)
        end

        it 'returns true' do
          service.create
          
          expect(service.success).to eq(true)
        end

        it 'populates the data' do
          service.create

          price_table = service.record

          expect(price_table.name).to eq('Foo')
          expect(price_table.default?).to eq(true)
          expect(price_table.owner).to eq(@owner)

          items = price_table.items.order(:id)

          expect(items[0].price).to eq(BigDecimal.new('15.22'))
          expect(items[0].maximum_discount).to eq(BigDecimal.new('10'))
          expect(items[0].item_id).to eq(@item.id)
          expect(items[1].price).to eq(BigDecimal.new('13.44'))
          expect(items[1].maximum_discount).to eq(BigDecimal.new('9.8'))
          expect(items[1].item_id).to eq(@item_2.id)
        end
      end
    end
  end

  describe 'update' do
    let(:service) {described_class.new(parameters)}

    context 'with invalid parameters' do
      context 'for lines only table' do
        before(:each) do
          @price_table = FactoryGirl.create(:price_table, name: 'ab', owner: @owner)

          FactoryGirl.create(:price_table_line, line_id: @line.id, price: '2+2', price_table: @price_table)
        end

        let(:parameters) { {'name' => '', 'owner' => @owner} }

        it 'does not create a new Price Table' do
          expect {
            service.update(@price_table.id)
          }.to change(PriceTable, :count).by(0)
        end

        it 'does not create any new Price Table Line' do
          expect {
            service.update(@price_table.id)
          }.to change(PriceTableLine, :count).by(0)
        end

        it 'returns false' do
          service.update(@price_table.id)
          
          expect(service.success).to eq(false)
        end

        it 'returns the errors' do
          service.update(@price_table.id)
          
          expect(service.errors).to_not be_empty
        end
      end

      context 'for items only table' do
        before(:each) do
          @price_table = FactoryGirl.create(:price_table, name: 'ab', owner: @owner, type: :for_items)

          FactoryGirl.create(:price_table_item, item_id: @item.id, price: '1.22', price_table: @price_table)
        end

        let(:parameters) { {'name' => '', 'owner' => @owner} }

        it 'does not create a new Price Table' do
          expect {
            service.update(@price_table.id)
          }.to change(PriceTable, :count).by(0)
        end

        it 'does not create any new Price Table Item' do
          expect {
            service.update(@price_table.id)
          }.to change(PriceTableItem, :count).by(0)
        end

        it 'returns false' do
          service.update(@price_table.id)
          
          expect(service.success).to eq(false)
        end

        it 'returns the errors' do
          service.update(@price_table.id)
          
          expect(service.errors).to_not be_empty
        end
      end
    end

    context 'with valid parameters' do
      context 'for lines only table' do
        before(:each) do
          @price_table = FactoryGirl.create(:price_table, name: 'ab', owner: @owner)

          FactoryGirl.create(:price_table_line, line_id: @line.id, price: '2+2', price_table: @price_table)
        end

        let(:parameters) { {'name' => 'Foo', 'default' => 'true', 'price_table_lines' => 
          [
            {'price' => '_CMV_', 'maximum_discount' => '12', 'line' => {'id' => @line.id}},
            {'price' => '_CMV_ * 2', 'maximum_discount' => '13', 'line' => {'id' => @line_2.id}}
          ], 'owner' => @owner
        } }

        it 'does not create a new price table' do
          expect {
            service.update(@price_table.id)
          }.to change(PriceTable, :count).by(0)
        end

        it 'creates a new price table line' do
          expect {
            service.update(@price_table.id)
          }.to change(PriceTableLine, :count).by(1)
        end

        it 'returns the price table' do
          service.update(@price_table.id)

          expect(service.record).to eq(PriceTable.last)
        end

        it 'returns true' do
          service.update(@price_table.id)
          
          expect(service.success).to eq(true)
        end

        it 'populates the data' do
          service.update(@price_table.id)

          price_table = service.record

          expect(price_table.name).to eq('Foo')
          expect(price_table.default?).to eq(true)
          expect(price_table.owner).to eq(@owner)

          lines = price_table.lines.order(:id)

          expect(lines[0].price).to eq('_CMV_')
          expect(lines[0].maximum_discount).to eq(BigDecimal.new('12'))
          expect(lines[0].line_id).to eq(@line.id)
          expect(lines[1].price).to eq('_CMV_*2')
          expect(lines[1].maximum_discount).to eq(BigDecimal.new('13'))
          expect(lines[1].line_id).to eq(@line_2.id)
        end
      end

      context 'for items only table' do
        before(:each) do
          @price_table = FactoryGirl.create(:price_table, name: 'ab', owner: @owner, type: :for_items)

          FactoryGirl.create(:price_table_item, item_id: @item.id, price: '16.98', price_table: @price_table)
        end

        let(:parameters) { {'name' => 'Foo', 'default' => 'true', 'price_table_items' => 
          [
            {'price' => '22.55', 'maximum_discount' => '17', 'item' => {'id' => @item.id}},
            {'price' => '88.99', 'maximum_discount' => '22', 'item' => {'id' => @item_2.id}}
          ], 'owner' => @owner
        } }

        it 'does not create a new price table' do
          expect {
            service.update(@price_table.id)
          }.to change(PriceTable, :count).by(0)
        end

        it 'creates a new price table item' do
          expect {
            service.update(@price_table.id)
          }.to change(PriceTableItem, :count).by(1)
        end

        it 'returns the price table' do
          service.update(@price_table.id)

          expect(service.record).to eq(PriceTable.last)
        end

        it 'returns true' do
          service.update(@price_table.id)
          
          expect(service.success).to eq(true)
        end

        it 'populates the data' do
          service.update(@price_table.id)

          price_table = service.record

          expect(price_table.name).to eq('Foo')
          expect(price_table.default?).to eq(true)
          expect(price_table.owner).to eq(@owner)

          items = price_table.items.order(:id)

          expect(items[0].price).to eq(BigDecimal.new('22.55'))
          expect(items[0].maximum_discount).to eq(BigDecimal.new('17'))
          expect(items[0].item_id).to eq(@item.id)
          expect(items[1].price).to eq(BigDecimal.new('88.99'))
          expect(items[1].maximum_discount).to eq(BigDecimal.new('22'))
          expect(items[1].item_id).to eq(@item_2.id)
        end
      end
    end
  end

  describe 'apply_to_distributors' do
    let(:service) {described_class.new({'owner' => @owner})}

    before(:each) do
      @price_table = FactoryGirl.create(:price_table, name: 'ab', owner: @owner)

      @distributor_1 = FactoryGirl.create(:distributor, name: 'DA')
      @distributor_2 = FactoryGirl.create(:distributor, name: 'DB')
      @distributor_3 = FactoryGirl.create(:distributor, name: 'DC')

      @store = FactoryGirl.create(:store, name: 'SA', distributor: @distributor_1)

      @price_table.clients << PriceTableClient.new(client: @store)
    end

    context 'with invalid parameters' do
      before(:each) do
        allow_any_instance_of(PriceTableClient).to receive(:save!).and_raise(StandardError)
      end

      it 'does not create any new Price Table Client' do
        expect {
          service.apply_to_distributors(@price_table.id, [''])
        }.to change(PriceTableClient, :count).by(0)
      end

      it 'returns false' do
        service.apply_to_distributors(@price_table.id, [''])
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.apply_to_distributors(@price_table.id, [''])
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) {["#{@distributor_1.id}", "#{@distributor_2.id}", "#{@distributor_3.id}"]}

      it 'creates three new price table clients' do
        expect {
          service.apply_to_distributors(@price_table.id, parameters)
        }.to change(PriceTableClient, :count).by(3)
      end

      it 'returns the price table' do
        service.apply_to_distributors(@price_table.id, parameters)

        expect(service.record).to eq(PriceTable.last)
      end

      it 'returns true' do
        service.apply_to_distributors(@price_table.id, parameters)
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.apply_to_distributors(@price_table.id, parameters)

        price_table = service.record

        expect(price_table.clients.count).to eq(4)
        expect(price_table.clients[0].client).to eq(@store)
        expect(price_table.clients[1].client).to eq(@distributor_1)
        expect(price_table.clients[2].client).to eq(@distributor_2)
        expect(price_table.clients[3].client).to eq(@distributor_3)
      end
    end
  end

  describe 'apply_to_stores' do
    let(:service) {described_class.new({'owner' => @owner})}

    before(:each) do
      @price_table = FactoryGirl.create(:price_table, name: 'ab', owner: @owner)

      @store_1 = FactoryGirl.create(:store, name: 'SA', distributor: @distributor)
      @store_2 = FactoryGirl.create(:store, name: 'SB', distributor: @distributor)
      @store_3 = FactoryGirl.create(:store, name: 'SC', distributor: @distributor)

      @price_table.clients << PriceTableClient.new(client: @distributor)
    end

    context 'with invalid parameters' do
      before(:each) do
        allow_any_instance_of(PriceTableClient).to receive(:save!).and_raise(ActiveRecord::RecordInvalid)
      end

      it 'does not create any new Price Table Client' do
        expect {
          service.apply_to_stores(@price_table.id, [''])
        }.to change(PriceTableClient, :count).by(0)
      end

      it 'returns false' do
        service.apply_to_stores(@price_table.id, [''])
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.apply_to_stores(@price_table.id, [''])
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) {["#{@store_1.id}", "#{@store_2.id}", "#{@store_3.id}"]}

      it 'creates three new price table clients' do
        expect {
          service.apply_to_stores(@price_table.id, parameters)
        }.to change(PriceTableClient, :count).by(3)
      end

      it 'returns the price table' do
        service.apply_to_stores(@price_table.id, parameters)

        expect(service.record).to eq(PriceTable.last)
      end

      it 'returns true' do
        service.apply_to_stores(@price_table.id, parameters)
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.apply_to_stores(@price_table.id, parameters)

        price_table = service.record

        expect(price_table.clients.count).to eq(4)
        expect(price_table.clients[0].client).to eq(@distributor)
        expect(price_table.clients[1].client).to eq(@store_1)
        expect(price_table.clients[2].client).to eq(@store_2)
        expect(price_table.clients[3].client).to eq(@store_3)
      end
    end
  end

  describe 'apply_to_customers' do
    let(:service) {described_class.new({'owner' => @owner})}

    before(:each) do
      @price_table = FactoryGirl.create(:price_table, name: 'ab', owner: @owner)

      @store = FactoryGirl.create(:store, name: 'SA', distributor: @distributor)

      @customer_1 = FactoryGirl.create(:customer, name: 'CA', store_id: @store.id)
      @customer_2 = FactoryGirl.create(:customer, name: 'CB', store_id: @store.id)
      @customer_3 = FactoryGirl.create(:customer, name: 'CC', store_id: @store.id)

      @price_table.clients << PriceTableClient.new(client: @distributor)
      @price_table.clients << PriceTableClient.new(client: @store)
    end

    context 'with invalid parameters' do
      before(:each) do
        allow_any_instance_of(PriceTableClient).to receive(:save!).and_raise(ActiveRecord::RecordInvalid)
      end

      it 'does not create any new Price Table Client' do
        expect {
          service.apply_to_customers(@price_table.id, [''])
        }.to change(PriceTableClient, :count).by(0)
      end

      it 'returns false' do
        service.apply_to_customers(@price_table.id, [''])
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.apply_to_customers(@price_table.id, [''])
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) {["#{@customer_1.id}", "#{@customer_2.id}", "#{@customer_3.id}"]}

      it 'creates three new price table clients' do
        expect {
          service.apply_to_customers(@price_table.id, parameters)
        }.to change(PriceTableClient, :count).by(3)
      end

      it 'returns the price table' do
        service.apply_to_customers(@price_table.id, parameters)

        expect(service.record).to eq(PriceTable.last)
      end

      it 'returns true' do
        service.apply_to_customers(@price_table.id, parameters)
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.apply_to_customers(@price_table.id, parameters)

        price_table = service.record

        expect(price_table.clients.count).to eq(5)
        expect(price_table.clients[0].client).to eq(@distributor)
        expect(price_table.clients[1].client).to eq(@store)
        expect(price_table.clients[2].client).to eq(@customer_1)
        expect(price_table.clients[3].client).to eq(@customer_2)
        expect(price_table.clients[4].client).to eq(@customer_3)
      end
    end
  end
end
