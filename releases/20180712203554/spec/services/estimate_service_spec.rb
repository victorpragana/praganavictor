require 'rails_helper'

RSpec.describe EstimateService, type: :service do
  before do
    Rails.cache.clear

    @factory     = create(:factory, name: 'F1')
    @distributor = create(:distributor, name: 'D1')
    @store       = create(:store, distributor: @distributor, name: 'S1')
    @customer    = create(:customer, store_id: @store.id, name: 'C1')
  end

  describe 'update_prices' do
    let(:service)   { described_class.new }
    let(:container) { double(:container, kill_requested?: false) }

    before do
      @distributor = create(:distributor, name: 'D3')
      @store       = create(:store, distributor: @distributor, name: 'S3')
      @estimate    = create(:estimate, seller: @store, client: @customer)

      @environment = create(:environment, estimate: @estimate)
      @location = create(:location, environment: @environment)

      @family = create(:family)
      @line = create(:line, family: @family, sale_price: BigDecimal.new('12.50'))

      @category = create(:category, name: 'C1', use_cut_table: true)
      @category_2 = create(:category, name: 'C2')

      @line.categories << build(:line_category, category: @category, required: true)
      @line.categories << build(:line_category, category: @category_2, required: false)

      @line.save!

      @value_1 = build(:value, value: 'V1', comparison_value: 100)
      @value_2 = build(:value, value: 'V2', comparison_value: 150)
      @value_3 = build(:value, value: 'V3', comparison_value: 200)

      @specification = build(:specification, automatic: true)
      @specification.values = [@value_1, @value_2, @value_3]
      @specification.save!

      @cut_table = create(:cut_table, size: 5000)

      @component_1 = create(:component, name: 'C1', unit: :cm, cut_table: @cut_table, category: @category, erasable: true, cost: BigDecimal.new('11'), values: [@value_1])
      @component_2 = create(:component, name: 'C2', unit: :cm, cut_table: @cut_table, category: @category, erasable: true, cost: BigDecimal.new('13'), values: [@value_2])
      @component_3 = create(:component, name: 'C3', unit: :cm, cut_table: @cut_table, category: @category, erasable: true, cost: BigDecimal.new('16'), values: [@value_3])
      @component_4 = create(:component, name: 'C4', unit: :cm, category: @category_2, erasable: true, cost: BigDecimal.new('16'))
      @component_5 = create(:component, name: 'C5', unit: :cm, category: @category_2, erasable: true, cost: BigDecimal.new('12'))

      @line_rule = create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], 
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_', 
        width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', automatic: true)
      create(:rule, line_rule: @line_rule, component: @component_1, control_method: Rule.control_methods[:width], 
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_', 
        width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', automatic: true)
      create(:rule, line_rule: @line_rule, component: @component_2, control_method: Rule.control_methods[:width], 
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_', 
        width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', automatic: true)
      create(:rule, line_rule: @line_rule, component: @component_3, control_method: Rule.control_methods[:width], 
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_', 
        width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', automatic: true)

      @line_rule = create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], total_consumption: "_INFORMADO_", cost: "_CUSTO_BASE_", width_consumption: "_LARGURA_", height_consumption: "_ALTURA_", automatic: false)
      create(:rule, line_rule: @line_rule, component: @component_4, control_method: Rule.control_methods[:width], total_consumption: "_INFORMADO_", cost: "_CUSTO_BASE_", width_consumption: "_LARGURA_", height_consumption: "_ALTURA_", automatic: false)

      @line_rule = create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], total_consumption: "_LARGURA_*2", cost: "_CUSTO_BASE_", width_consumption: "_LARGURA_", height_consumption: "_ALTURA_", automatic: false)
      create(:rule, line_rule: @line_rule, component: @component_5, control_method: Rule.control_methods[:width], total_consumption: "_LARGURA_*2", cost: "_CUSTO_BASE_", width_consumption: "_LARGURA_", height_consumption: "_ALTURA_", automatic: false)

      item_component_1_1 = build(:option_component, component_id: @component_1.id, possible_component_ids: [@component_1.id, @component_2.id, @component_3.id], 
        component_name: @component_1.name, component_characteristics: @component_1.characteristics, category_name: @component_1.category.name, 
        unit: @component_1.unit, total_consumption: 100, unit_value: 250, width_consumption: 20, height_consumption: 30, quantity_consumption: 100, 
        required: true, total: 25000)
      item_component_1_2 = build(:option_component, component_id: @component_5.id, possible_component_ids: [@component_5.id], 
        component_name: @component_5.name, component_characteristics: @component_5.characteristics, category_name: @component_5.category.name, 
        unit: @component_5.unit, total_consumption: 20, unit_value: 65, required: false, total: 1300)

      item_component_2_1 = build(:option_component, component_id: @component_1.id, possible_component_ids: [@component_1.id, @component_2.id, @component_3.id], 
        component_name: @component_1.name, component_characteristics: @component_1.characteristics, category_name: @component_1.category.name, 
        unit: @component_1.unit, total_consumption: 100, unit_value: 250, width_consumption: 20, height_consumption: 30, quantity_consumption: 100, 
        required: true, total: 25000)
      item_component_2_2 = build(:option_component, component_id: @component_5.id, possible_component_ids: [@component_5.id], 
        component_name: @component_5.name, component_characteristics: @component_5.characteristics, category_name: @component_5.category.name, 
        unit: @component_5.unit, total_consumption: 20, unit_value: 65, required: false, total: 1300)

      item_component_3_1 = build(:option_component, component_id: @component_3.id, possible_component_ids: [@component_1.id, @component_2.id, @component_3.id], 
        component_name: @component_1.name, component_characteristics: @component_1.characteristics, category_name: @component_1.category.name, 
        unit: @component_1.unit, total_consumption: 100, unit_value: 250, width_consumption: 20, height_consumption: 30, quantity_consumption: 100, 
        required: true, total: 25000)
      item_component_3_2 = build(:option_component, component_id: @component_5.id, possible_component_ids: [@component_5.id], 
        component_name: @component_5.name, component_characteristics: @component_5.characteristics, category_name: @component_5.category.name, 
        unit: @component_5.unit, total_consumption: 20, unit_value: 65, required: false, total: 1300)

      item_component_4_1 = build(:option_component, component_id: @component_4.id, possible_component_ids: [@component_4.id], 
        component_name: @component_4.name, component_characteristics: @component_4.characteristics, category_name: @component_4.category.name, 
        unit: @component_1.unit, total_consumption: 38, unit_value: 250, width_consumption: 20, height_consumption: 30, quantity_consumption: 100, 
        required: true, total: 25000)

      @option_1 = create(:option, width: 100, height: 300, line: @line, location: @location, selected: true, components: [item_component_1_1, item_component_1_2])
      @option_2 = create(:option, width:  70, height: 100, line: @line, location: @location, selected: true, components: [item_component_2_1, item_component_2_2])
      @option_3 = create(:option, width:  70, height: 200, line: @line, location: @location, selected: false, components: [item_component_3_1, item_component_3_2])
      @option_4 = create(:option, width:  70, height: 120, line: @line, location: @location, selected: true, components: [item_component_4_1])

      @price_table = create(:price_table, owner: @distributor, type: :for_lines, name: 'PT1')
      create(:price_table_line, line: @line, price: '_CMV_*2', price_table: @price_table)

      @price_table_factory_distributor = create(:price_table, owner: @factory, type: :for_lines, name: 'PT2')
      create(:price_table_line, line: @line, price: '_CMV_*1.1', price_table: @price_table_factory_distributor)
      create(:price_table_client, price_table: @price_table_factory_distributor, client: @distributor)

      @price_table_distributor_store = create(:price_table, owner: @distributor, type: :for_lines, name: 'PT3')
      create(:price_table_line, line: @line, price: '_CMV_*1.5', price_table: @price_table_distributor_store)
      create(:price_table_client, price_table: @price_table_distributor_store, client: @store)

      @price_table_store = create(:price_table, owner: @store, type: :for_lines, name: 'PT4')
      create(:price_table_line, line: @line, price: '_CMV_*2', price_table: @price_table_store)
      create(:price_table_client, price_table: @price_table_store, client: @customer)

      allow(SidekiqStatus::Container).to receive(:load).and_return(container)
    end

    it 'initializes the option components grouper for each selected option component' do
      expect(OptionComponentGrouper).to receive(:new).exactly(5).times.and_call_original

      service.update_prices(@estimate.id)
    end

    context 'with valid data' do
      it 'updates the component selection for all selected options' do
        service.update_prices(@estimate.id)

        expect(service.success).to eq(true)

        @option_1.reload
        @option_2.reload
        @option_3.reload
        @option_4.reload

        components = @option_1.components.order(id: :asc)
        expect(components[1].component).to eq(@component_5)
        expect(components[0].component).to eq(@component_1)

        components = @option_2.components.order(id: :asc)
        expect(components[0].component).to eq(@component_1)
        expect(components[1].component).to eq(@component_5)

        components = @option_3.components.order(id: :asc)
        expect(components[0].component).to eq(@component_3)
        expect(components[1].component).to eq(@component_5)

        expect(@option_4.components[0].component).to eq(@component_4)
      end

      it 'maintains the given total consumption for the _INFORMADO_ rule' do
        service.update_prices(@estimate.id)

        expect(service.success).to eq(true)

        @option_4.reload

        expect(@option_4.components[0].total_consumption).to eq(38)
      end

      it 'returns true' do
        service.update_prices(@estimate.id)

        expect(service.success).to eq(true)
      end

      it 'returns the record' do
        service.update_prices(@estimate.id)

        expect(service.record).to eq(@estimate)
      end
    end

    context 'with invalid data' do
      let(:grouper) {double(:grouper, group: nil, errors: double(:errors, full_messages: ['foo']))}

      before do
        allow(OptionComponentGrouper).to receive(:new).and_return(grouper)
      end

      it 'returns false' do
        service.update_prices(@estimate.id)

        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.update_prices(@estimate.id)

        expect(service.errors).to_not be_empty
      end
    end
  end

  describe 'create' do
    let(:service) { described_class.new(parameters) }

    context 'with invalid parameters' do
      let(:parameters) { {'date' => '', 'observation' => 'Baz', 'seller' => @store, 
        'client_id' => "#{@customer.id}", 'client_type' => 'Customer'} }

      it 'does not create a new Estimate' do
        expect {
          service.create
        }.to change(Estimate, :count).by(0)
      end

      it 'returns false' do
        service.create
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.create
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { {'date' => '12/02/2016', 'observation' => 'Baz', 'seller' => @store, 
        'client_id' => "#{@customer.id}", 'client_type' => 'Customer'} }

      it 'creates a new Estimate' do
        expect {
          service.create
        }.to change(Estimate, :count).by(1)
      end

      it 'returns the created estimate' do
        service.create

        expect(service.record).to eq(Estimate.last)
      end

      it 'returns true' do
        service.create
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.create

        estimate = service.record

        expect(estimate.observation).to eq('Baz')
        expect(estimate.date).to eq(Date.parse('12/02/2016'))
        expect(estimate.seller).to eq(@store)
        expect(estimate.client).to eq(@customer)
      end
    end
  end

  describe 'update' do
    let(:service) {described_class.new(parameters)}

    before do
      @estimate = create(:estimate, seller: @store, client: @customer)

      @environment = create(:environment, estimate: @estimate)
      @location = create(:location, environment: @environment)

      @family = create(:family)
      @line = create(:line, family: @family, sale_price: BigDecimal.new('12.50'))

      @category = create(:category, name: 'C1')
      @category_2 = create(:category, name: 'C2')
      @category_3 = create(:category, name: 'C3')

      @component_1 = create(:component, name: 'C1', category: @category, erasable: true, cost: BigDecimal.new('10'))
      @component_2 = create(:component, name: 'C2', category: @category_3, erasable: true, cost: BigDecimal.new('12'))

      item_component_1 = build(:option_component, component_name: @component_1.name, component_characteristics: @component_1.characteristics, category_name: @component_1.category.name, unit: @component_1.unit, total_consumption: 100, unit_value: 250, width_consumption: 20, height_consumption: 30, quantity_consumption: 100, required: true)
      item_component_2 = build(:option_component, component_name: @component_2.name, component_characteristics: @component_2.characteristics, category_name: @component_2.category.name, unit: @component_2.unit, total_consumption: 20, unit_value: 65, required: false)

      @option_1 = create(:option, line: @line, location: @location, selected: false, components: [item_component_1])
      @option_2 = create(:option, line: @line, location: @location, selected: false, components: [item_component_2])

      @item = create(:item)

      @item_price_table = create(:price_table, owner: @store, type: :for_items)
      create(:price_table_item, item: @item, price: '58.41', price_table: @item_price_table)
      create(:price_table_client, price_table: @item_price_table, client: @customer)
      create(:estimate_item, estimate: @estimate, item: @item, quantity: 2, factory_value: 30, distributor_value: 40, store_value: 50, customer_value: 60)

      @price_table_factory_distributor = create(:price_table, owner: @factory, type: :for_lines, name: 'PTFD10')
      create(:price_table_line, line: @line, price: '_CMV_*1.1', price_table: @price_table_factory_distributor)
      create(:price_table_client, price_table: @price_table_factory_distributor, client: @distributor)

      @price_table_distributor_store = create(:price_table, owner: @distributor, type: :for_lines, name: 'PTDS10')
      create(:price_table_line, line: @line, price: '_CMV_*1.5', price_table: @price_table_distributor_store)
      create(:price_table_client, price_table: @price_table_distributor_store, client: @store)

      @price_table_store = create(:price_table, owner: @store, type: :for_lines, name: 'PTS10')
      create(:price_table_line, line: @line, price: '_CMV_*2', price_table: @price_table_store)
      create(:price_table_client, price_table: @price_table_store, client: @customer)
    end

    context 'with invalid parameters' do
      let(:parameters) { {'date' => '', 'seller' => @store, 'client_id' => "#{@customer.id}", 'client_type' => 'Customer',
        'observation' => 'Baz', 'option_ids' => ["#{@option_1.id}"]
      } }

      it 'does not create a new Estimate' do
        expect {
          service.update(@estimate.id)
        }.to change(Estimate, :count).by(0)
      end

      it 'returns false' do
        service.update(@estimate.id)
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.update(@estimate.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { {'date' => '12/02/2016', 'seller' => @store, 'client_id' => "#{@customer.id}", 'client_type' => 'Customer',
        'observation' => 'Baz', 'option_ids' => ["#{@option_1.id}"], 'optimize' => 'true'
      } }

      it 'does not create a new Estimate' do
        expect {
          service.update(@estimate.id)
        }.to change(Estimate, :count).by(0)
      end

      it 'returns the estimate' do
        service.update(@estimate.id)

        expect(service.record).to eq(Estimate.last)
      end

      it 'should mark the option 1 as selected' do
        service.update(@estimate.id)

        expect(@option_1.reload.selected).to eq(true)
        expect(@option_2.reload.selected).to eq(false)
      end

      it 'returns true' do
        service.update(@estimate.id)
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.update(@estimate.id)

        estimate = service.record

        expect(estimate.observation).to eq('Baz')
        expect(estimate.date).to eq(Date.parse('12/02/2016'))
        expect(estimate.seller).to eq(@store)
        expect(estimate.client).to eq(@customer)
        expect(estimate.optimize?).to eq(true)
      end
    end
  end

  describe 'duplicate' do
    let(:service) { described_class.new }

    before do
      @estimate = create(:estimate, seller: @store, client: @customer, status: :enqueued)

      @environment = create(:environment, estimate: @estimate)
      @location = create(:location, environment: @environment)

      @family = create(:family)
      @line = create(:line, family: @family, sale_price: BigDecimal.new('12.50'))

      @category = create(:category, name: 'C1')
      @category_2 = create(:category, name: 'C2')
      @category_3 = create(:category, name: 'C3')

      @component_1 = create(:component, name: 'C1', category: @category, erasable: true, cost: BigDecimal.new('10'))
      @component_2 = create(:component, name: 'C2', category: @category_3, erasable: true, cost: BigDecimal.new('12'))

      item_component_1 = build(:option_component, component_name: @component_1.name, component_characteristics: @component_1.characteristics, category_name: @component_1.category.name, unit: @component_1.unit, total_consumption: 100, unit_value: 250, width_consumption: 20, height_consumption: 30, quantity_consumption: 100, required: true)
      item_component_2 = build(:option_component, component_name: @component_2.name, component_characteristics: @component_2.characteristics, category_name: @component_2.category.name, unit: @component_2.unit, total_consumption: 20, unit_value: 65, required: false)

      @option_1 = create(:option, line: @line, location: @location, selected: false, components: [item_component_1])
      @option_2 = create(:option, line: @line, location: @location, selected: false, components: [item_component_2])

      @item = create(:item)

      @item_price_table = create(:price_table, owner: @store, type: :for_items)
      create(:price_table_item, item: @item, price: '58.41', price_table: @item_price_table)
      create(:price_table_client, price_table: @item_price_table, client: @customer)
      create(:estimate_item, estimate: @estimate, item: @item, quantity: 2, factory_value: 30, distributor_value: 40, store_value: 50, customer_value: 60)

      @price_table_store = create(:price_table, owner: @store, type: :for_lines)
      create(:price_table_line, line: @line, price: '_CMV_*2', price_table: @price_table_store)
      create(:price_table_client, price_table: @price_table_store, client: @customer)
    end

    context 'with invalid parameters' do
      let(:estimate) { mock_model(Estimate, errors: double(:errors, full_messages: ['foo'])).as_null_object }

      before do
        allow_any_instance_of(Estimate).to receive(:deep_clone).and_return(estimate)
        allow(estimate).to receive(:save!).and_raise(ActiveRecord::RecordInvalid)
      end

      it 'does not create a new Estimate' do
        expect {
          service.duplicate(@estimate.id)
        }.to_not change(Estimate, :count)
      end

      it 'returns false' do
        service.duplicate(@estimate.id)

        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.duplicate(@estimate.id)

        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      it 'creates a new Estimate' do
        expect {
          service.duplicate(@estimate.id)
        }.to change(Estimate, :count).by(1)
      end

      it 'returns the estimate' do
        service.duplicate(@estimate.id)

        expect(service.record).to eq(Estimate.last)
      end

      it 'returns true' do
        service.duplicate(@estimate.id)
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.duplicate(@estimate.id)

        estimate = service.record

        expect(estimate.observation).to eq(@estimate.observation)
        expect(estimate.date).to eq(@estimate.date)
        expect(estimate.seller).to eq(@estimate.seller)
        expect(estimate.client).to eq(@estimate.client)
        expect(estimate.optimize?).to eq(@estimate.optimize?)
        expect(estimate.free?).to be_truthy

        expect(estimate.environments[0].name).to eq(@estimate.environments[0].name)
        expect(estimate.environments[0].category).to eq(@estimate.environments[0].category)
        expect(estimate.locations[0].name).to eq(@estimate.locations[0].name)
        expect(estimate.locations[0].category).to eq(@estimate.locations[0].category)
        expect(estimate.locations[0].width).to eq(@estimate.locations[0].width)
        expect(estimate.locations[0].height).to eq(@estimate.locations[0].height)
        expect(estimate.locations[0].quantity).to eq(@estimate.locations[0].quantity)
        expect(estimate.options).to be_empty
      end
    end
  end

  describe '#recalculate_totals' do
    let(:service) { described_class.new }

    before do
      @family = create(:family)
      @line = create(:line, family: @family, sale_price: BigDecimal.new('12.50'))

      @category = create(:category, name: 'C1')

      @line.categories << build(:line_category, category: @category, required: true)

      @line.save!

      @component_1 = create(:component, name: 'C1', unit: :cm, category: @category, erasable: true, cost: BigDecimal.new('10'))      

      @line_rule = create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], 
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_', 
        width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', automatic: false)
      create(:rule, line_rule: @line_rule, component: @component_1, control_method: Rule.control_methods[:width], 
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_', 
        width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', automatic: false)

      @price_table_factory_distributor = create(:price_table, owner: @factory, type: :for_lines, name: 'PT1')
      create(:price_table_line, line: @line, price: '_CMV_*1.1', price_table: @price_table_factory_distributor)
      create(:price_table_client, price_table: @price_table_factory_distributor, client: @distributor)

      @price_table_factory_store = create(:price_table, owner: @factory, type: :for_lines, name: 'PT2')
      create(:price_table_line, line: @line, price: '_CMV_*1.2', price_table: @price_table_factory_store)
      create(:price_table_client, price_table: @price_table_factory_store, client: @store)

      @price_table_factory_customer = create(:price_table, owner: @factory, type: :for_lines, name: 'PT3')
      create(:price_table_line, line: @line, price: '_CMV_*1.3', price_table: @price_table_factory_customer)
      create(:price_table_client, price_table: @price_table_factory_customer, client: @customer)

      @price_table_distributor_store = create(:price_table, owner: @distributor, type: :for_lines, name: 'PT4')
      create(:price_table_line, line: @line, price: '_CMV_*1.5', price_table: @price_table_distributor_store)
      create(:price_table_client, price_table: @price_table_distributor_store, client: @store)

      @price_table_distributor_customer = create(:price_table, owner: @distributor, type: :for_lines, name: 'PT5')
      create(:price_table_line, line: @line, price: '_CMV_*1.7', price_table: @price_table_distributor_customer)
      create(:price_table_client, price_table: @price_table_distributor_customer, client: @customer)

      @price_table_store = create(:price_table, owner: @store, type: :for_lines, name: 'PT6')
      create(:price_table_line, line: @line, price: '_CMV_*2', price_table: @price_table_store)
      create(:price_table_client, price_table: @price_table_store, client: @customer)
    end

    context 'for factory seller' do
      context 'and distributor client' do
        before do
          @estimate    = create(:estimate, seller: @factory, client: @distributor)
          @environment = create(:environment, estimate: @estimate)
          @location    = create(:location, environment: @environment)

          item_component_1_1 = build(:option_component, component_id: @component_1.id, possible_component_ids: [@component_1.id], 
            component_name: @component_1.name, component_characteristics: @component_1.characteristics, category_name: @component_1.category.name, 
            unit: @component_1.unit, total_consumption: 200, unit_value: 44.40, width_consumption: 100, height_consumption: 300, required: true, 
            total: 8880)
      
          item_component_2_1 = build(:option_component, component_id: @component_1.id, possible_component_ids: [@component_1.id], 
            component_name: @component_1.name, component_characteristics: @component_1.characteristics, category_name: @component_1.category.name, 
            unit: @component_1.unit, total_consumption: 140, unit_value: 44.40, width_consumption: 70, height_consumption: 100, required: true, 
            total: 6216)
          
          @option_1 = create(:option, width: 1000, height: 3000, line: @line, location: @location, selected: true, components: [item_component_1_1])
          @option_2 = create(:option, width:  700, height: 1000, line: @line, location: @location, selected: true, components: [item_component_2_1])
        end

        it 'calculates the factory totals for the options' do
          service.recalculate_totals(@estimate.id)

          @option_1.reload
          @option_2.reload

          expect(@option_1.factory_total).to eq(BigDecimal.new('9990.00'))
          expect(@option_2.factory_total).to eq(BigDecimal.new('6993.00'))
        end

        it 'calculates the distributor totals applying the factory-distributor price table' do
          service.recalculate_totals(@estimate.id)

          @option_1.reload
          @option_2.reload

          expect(@option_1.distributor_total).to eq(BigDecimal.new('10989.00'))
          expect(@option_2.distributor_total).to eq(BigDecimal.new('7692.30'))
        end
      end

      context 'and store client' do
        before do
          @estimate    = create(:estimate, seller: @factory, client: @store)
          @environment = create(:environment, estimate: @estimate)
          @location    = create(:location, environment: @environment)

          item_component_1_1 = build(:option_component, component_id: @component_1.id, possible_component_ids: [@component_1.id], 
            component_name: @component_1.name, component_characteristics: @component_1.characteristics, category_name: @component_1.category.name, 
            unit: @component_1.unit, total_consumption: 200, unit_value: 44.40, width_consumption: 100, height_consumption: 300, required: true, 
            total: 8880)
      
          item_component_2_1 = build(:option_component, component_id: @component_1.id, possible_component_ids: [@component_1.id], 
            component_name: @component_1.name, component_characteristics: @component_1.characteristics, category_name: @component_1.category.name, 
            unit: @component_1.unit, total_consumption: 140, unit_value: 44.40, width_consumption: 70, height_consumption: 100, required: true, 
            total: 6216)
          
          @option_1 = create(:option, width: 1000, height: 3000, line: @line, location: @location, selected: true, components: [item_component_1_1])
          @option_2 = create(:option, width:  700, height: 1000, line: @line, location: @location, selected: true, components: [item_component_2_1])
        end

        it 'calculates the factory totals for the options' do
          service.recalculate_totals(@estimate.id)

          @option_1.reload
          @option_2.reload

          expect(@option_1.factory_total).to eq(BigDecimal.new('9990.00'))
          expect(@option_2.factory_total).to eq(BigDecimal.new('6993.00'))
        end

        it 'calculates the store totals applying the factory-store price table' do
          service.recalculate_totals(@estimate.id)

          @option_1.reload
          @option_2.reload

          expect(@option_1.store_total).to eq(BigDecimal.new('11988.00'))
          expect(@option_2.store_total).to eq(BigDecimal.new('8391.60'))
        end
      end

      context 'and customer client' do
        before do
          @estimate    = create(:estimate, seller: @factory, client: @customer)
          @environment = create(:environment, estimate: @estimate)
          @location    = create(:location, environment: @environment)

          item_component_1_1 = build(:option_component, component_id: @component_1.id, possible_component_ids: [@component_1.id], 
            component_name: @component_1.name, component_characteristics: @component_1.characteristics, category_name: @component_1.category.name, 
            unit: @component_1.unit, total_consumption: 200, unit_value: 44.40, width_consumption: 100, height_consumption: 300, required: true, 
            total: 8880)
      
          item_component_2_1 = build(:option_component, component_id: @component_1.id, possible_component_ids: [@component_1.id], 
            component_name: @component_1.name, component_characteristics: @component_1.characteristics, category_name: @component_1.category.name, 
            unit: @component_1.unit, total_consumption: 140, unit_value: 44.40, width_consumption: 70, height_consumption: 100, required: true, 
            total: 6216)
          
          @option_1 = create(:option, width: 1000, height: 3000, line: @line, location: @location, selected: true, components: [item_component_1_1])
          @option_2 = create(:option, width:  700, height: 1000, line: @line, location: @location, selected: true, components: [item_component_2_1])
        end

        it 'calculates the factory totals for the options' do
          service.recalculate_totals(@estimate.id)

          @option_1.reload
          @option_2.reload

          expect(@option_1.factory_total).to eq(BigDecimal.new('9990.00'))
          expect(@option_2.factory_total).to eq(BigDecimal.new('6993.00'))
        end

        it 'calculates the customer totals applying the factory-customer price table' do
          service.recalculate_totals(@estimate.id)

          @option_1.reload
          @option_2.reload

          expect(@option_1.customer_total).to eq(BigDecimal.new('12987.00'))
          expect(@option_2.customer_total).to eq(BigDecimal.new('9090.90'))
        end
      end
    end

    context 'for distributor seller' do
      context 'and store client' do
        before do
          @estimate    = create(:estimate, seller: @distributor, client: @store)
          @environment = create(:environment, estimate: @estimate)
          @location    = create(:location, environment: @environment)

          item_component_1_1 = build(:option_component, component_id: @component_1.id, possible_component_ids: [@component_1.id], 
            component_name: @component_1.name, component_characteristics: @component_1.characteristics, category_name: @component_1.category.name, 
            unit: @component_1.unit, total_consumption: 200, unit_value: 44.40, width_consumption: 100, height_consumption: 300, required: true, 
            total: 8880)
      
          item_component_2_1 = build(:option_component, component_id: @component_1.id, possible_component_ids: [@component_1.id], 
            component_name: @component_1.name, component_characteristics: @component_1.characteristics, category_name: @component_1.category.name, 
            unit: @component_1.unit, total_consumption: 140, unit_value: 44.40, width_consumption: 70, height_consumption: 100, required: true, 
            total: 6216)
          
          @option_1 = create(:option, width: 1000, height: 3000, line: @line, location: @location, selected: true, components: [item_component_1_1])
          @option_2 = create(:option, width:  700, height: 1000, line: @line, location: @location, selected: true, components: [item_component_2_1])
        end

        it 'calculates the factory totals for the options' do
          service.recalculate_totals(@estimate.id)

          @option_1.reload
          @option_2.reload

          expect(@option_1.factory_total).to eq(BigDecimal.new('9990.00'))
          expect(@option_2.factory_total).to eq(BigDecimal.new('6993.00'))
        end

        it 'calculates the distributor totals applying the factory-distributor price table' do
          service.recalculate_totals(@estimate.id)

          @option_1.reload
          @option_2.reload

          expect(@option_1.distributor_total).to eq(BigDecimal.new('10989.00'))
          expect(@option_2.distributor_total).to eq(BigDecimal.new('7692.30'))
        end

        it 'calculates the store totals applying the distributor-store price table' do
          service.recalculate_totals(@estimate.id)

          @option_1.reload
          @option_2.reload

          expect(@option_1.store_total).to eq(BigDecimal.new('16483.50'))
          expect(@option_2.store_total).to eq(BigDecimal.new('11538.45'))
        end
      end

      context 'and customer client' do
        before do
          @estimate    = create(:estimate, seller: @distributor, client: @customer)
          @environment = create(:environment, estimate: @estimate)
          @location    = create(:location, environment: @environment)

          item_component_1_1 = build(:option_component, component_id: @component_1.id, possible_component_ids: [@component_1.id], 
            component_name: @component_1.name, component_characteristics: @component_1.characteristics, category_name: @component_1.category.name, 
            unit: @component_1.unit, total_consumption: 200, unit_value: 44.40, width_consumption: 100, height_consumption: 300, required: true, 
            total: 8880)
      
          item_component_2_1 = build(:option_component, component_id: @component_1.id, possible_component_ids: [@component_1.id], 
            component_name: @component_1.name, component_characteristics: @component_1.characteristics, category_name: @component_1.category.name, 
            unit: @component_1.unit, total_consumption: 140, unit_value: 44.40, width_consumption: 70, height_consumption: 100, required: true, 
            total: 6216)
          
          @option_1 = create(:option, width: 1000, height: 3000, line: @line, location: @location, selected: true, components: [item_component_1_1])
          @option_2 = create(:option, width:  700, height: 1000, line: @line, location: @location, selected: true, components: [item_component_2_1])
        end

        it 'calculates the factory totals for the options' do
          service.recalculate_totals(@estimate.id)

          @option_1.reload
          @option_2.reload

          expect(@option_1.factory_total).to eq(BigDecimal.new('9990.00'))
          expect(@option_2.factory_total).to eq(BigDecimal.new('6993.00'))
        end

        it 'calculates the distributor totals applying the factory-distributor price table' do
          service.recalculate_totals(@estimate.id)

          @option_1.reload
          @option_2.reload

          expect(@option_1.distributor_total).to eq(BigDecimal.new('10989.00'))
          expect(@option_2.distributor_total).to eq(BigDecimal.new('7692.30'))
        end

        it 'calculates the customer totals applying the distributor-customer price table' do
          service.recalculate_totals(@estimate.id)

          @option_1.reload
          @option_2.reload

          expect(@option_1.customer_total).to eq(BigDecimal.new('18681.30'))
          expect(@option_2.customer_total).to eq(BigDecimal.new('13076.91'))
        end
      end
    end

    context 'for store seller' do
      context 'and customer client' do
        before do
          @estimate    = create(:estimate, seller: @store, client: @customer)
          @environment = create(:environment, estimate: @estimate)
          @location    = create(:location, environment: @environment)

          item_component_1_1 = build(:option_component, component_id: @component_1.id, possible_component_ids: [@component_1.id], 
            component_name: @component_1.name, component_characteristics: @component_1.characteristics, category_name: @component_1.category.name, 
            unit: @component_1.unit, total_consumption: 200, unit_value: 44.40, width_consumption: 100, height_consumption: 300, required: true, 
            total: 8880)
      
          item_component_2_1 = build(:option_component, component_id: @component_1.id, possible_component_ids: [@component_1.id], 
            component_name: @component_1.name, component_characteristics: @component_1.characteristics, category_name: @component_1.category.name, 
            unit: @component_1.unit, total_consumption: 140, unit_value: 44.40, width_consumption: 70, height_consumption: 100, required: true, 
            total: 6216)
          
          @option_1 = create(:option, width: 1000, height: 3000, line: @line, location: @location, selected: true, components: [item_component_1_1])
          @option_2 = create(:option, width:  700, height: 1000, line: @line, location: @location, selected: true, components: [item_component_2_1])
        end

        it 'calculates the factory totals for the options' do
          service.recalculate_totals(@estimate.id)

          @option_1.reload
          @option_2.reload

          expect(@option_1.factory_total).to eq(BigDecimal.new('9990.00'))
          expect(@option_2.factory_total).to eq(BigDecimal.new('6993.00'))
        end

        it 'calculates the distributor totals applying the factory-distributor price table' do
          service.recalculate_totals(@estimate.id)

          @option_1.reload
          @option_2.reload

          expect(@option_1.distributor_total).to eq(BigDecimal.new('10989.00'))
          expect(@option_2.distributor_total).to eq(BigDecimal.new('7692.30'))
        end

        it 'calculates the store totals applying the distributor-store price table' do
          service.recalculate_totals(@estimate.id)

          @option_1.reload
          @option_2.reload

          expect(@option_1.store_total).to eq(BigDecimal.new('16483.50'))
          expect(@option_2.store_total).to eq(BigDecimal.new('11538.45'))
        end

        it 'calculates the customer totals applying the store-customer price table' do
          service.recalculate_totals(@estimate.id)

          @option_1.reload
          @option_2.reload

          expect(@option_1.customer_total).to eq(BigDecimal.new('32967.00'))
          expect(@option_2.customer_total).to eq(BigDecimal.new('23076.90'))
        end
      end
    end
  end

  describe '#apply_discount' do
    let(:service) {described_class.new(parameters)}

    before do
      @family = create(:family)
      @line = create(:line, family: @family)
      @estimate    = create(:estimate, seller: @store, client: @customer)
      @environment = create(:environment, estimate: @estimate)
      @location    = create(:location, environment: @environment)

      @price_table = create(:price_table, owner: @store, default: true, type: :for_lines)
      create(:price_table_line, line: @line, price: '_CMV_*2', price_table: @price_table, maximum_discount: BigDecimal.new('50.1'))

      @option_1 = create(:option, location: @location, line: @line)
      @option_2 = create(:option, location: @location, line: @line, selected: true)
    end

    context 'with invalid parameters' do
      let(:parameters) { {'discount' => '-1'} }

      it 'does not update the estimate discount' do
        expect {
          service.apply_discount(@estimate.id)
          @estimate.reload
        }.to_not change(@estimate, :discount)
      end

      it 'returns false' do
        service.apply_discount(@estimate.id)
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.apply_discount(@estimate.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { {'discount' => '10'} }

      it 'returns the estimate' do
        service.apply_discount(@estimate.id)

        expect(service.record).to eq(Estimate.last)
      end

      it 'returns true' do
        service.apply_discount(@estimate.id)
        
        expect(service.success).to eq(true)
      end

      it 'updates the estimate discount' do
        service.apply_discount(@estimate.id)

        estimate = service.record

        expect(estimate.discount).to eq(BigDecimal.new('10'))
      end
    end
  end
end