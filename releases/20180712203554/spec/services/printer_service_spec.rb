require 'rails_helper'

RSpec.describe PrinterService, type: :service do
  describe '#create' do
    let(:service) { described_class.new(parameters) }

    context "with invalid parameters" do
      let(:parameters) { { "name" => "", "real_name" => "", 'server_name' => '', 'server_id' => '' } }

      it "does not create a new Printer" do
        expect {
          service.create
        }.to_not change(Printer, :count)
      end

      it "returns false" do
        service.create
        
        expect(service.success).to eq(false)
      end

      it "returns the errors" do
        service.create
        
        expect(service.errors).to_not be_empty
      end
    end

    context "with valid parameters" do
      let(:parameters) { { "name" => "Foo", "real_name" => "Bar", 'server_name' => 'Bartz', 'server_id' => 'Pats', 'default' => 'true' } }

      it "creates a new printer" do
        expect {
          service.create
        }.to change(Printer, :count).by(1)
      end

      it "returns the created printer" do
        service.create

        expect(service.record).to eq(Printer.last)
      end

      it "returns true" do
        service.create
        
        expect(service.success).to eq(true)
      end

      it "populates the data" do
        service.create

        printer = service.record

        expect(printer.name).to eq('Foo')
        expect(printer.real_name).to eq('Bar')
        expect(printer.server_name).to eq('Bartz')
        expect(printer.server_id).to eq('Pats')
        expect(printer.default?).to be_truthy
      end
    end
  end

  describe '#update' do
    let(:service) { described_class.new(parameters) }

    before(:each) do
      @printer = FactoryGirl.create(:printer, name: 'b', default: true)
    end

    context "with invalid parameters" do
      let(:parameters) { { "name" => "", "real_name" => "", 'server_name' => '', 'server_id' => '' } }

      it "returns false" do
        service.update(@printer.id)
        
        expect(service.success).to eq(false)
      end

      it "returns the errors" do
        service.update(@printer.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context "with valid parameters" do
      let(:parameters) { { "name" => "Foo", "real_name" => "Bar", 'server_name' => 'Bartz', 'server_id' => 'Pats', 'default' => 'false' } }

      it "does not create a new printer" do
        expect {
          service.update(@printer.id)
        }.to change(Printer, :count).by(0)
      end

      it "returns the printer" do
        service.update(@printer.id)

        expect(service.record).to eq(Printer.last)
      end

      it "returns true" do
        service.update(@printer.id)
        
        expect(service.success).to eq(true)
      end

      it "populates the data" do
        service.update(@printer.id)

        printer = service.record

        expect(printer.name).to eq('Foo')
        expect(printer.real_name).to eq('Bar')
        expect(printer.server_name).to eq('Bartz')
        expect(printer.server_id).to eq('Pats')
        expect(printer.default?).to be_falsy
      end
    end
  end

  describe '#default_printer' do
    let(:service) { described_class.new }

    before do
      @printer1 = FactoryGirl.create(:printer, name: 'a', default: false)
      @printer2 = FactoryGirl.create(:printer, name: 'b', default: true)
      @printer3 = FactoryGirl.create(:printer, name: 'c', default: false)
    end

    it 'returns the printer marked as default' do
      expect(service.default_printer).to eq(@printer2)
    end
  end
end
