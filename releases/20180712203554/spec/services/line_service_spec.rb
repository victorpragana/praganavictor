require 'rails_helper'

RSpec.describe LineService, type: :service do
  before(:each) do
    @family = FactoryGirl.create(:family)
    @category = FactoryGirl.create(:category, name: 'C1')
    @category2 = FactoryGirl.create(:category, name: 'C2')
    @category3 = FactoryGirl.create(:category, name: 'C3')
  end

  describe 'create' do
    let(:service) { described_class.new(parameters) }

    context 'with invalid parameters' do
      let(:parameters) { {'name' => ''} }

      it 'does not create a new Line' do
        expect {
          service.create
        }.to change(Line, :count).by(0)
      end

      it 'returns false' do
        service.create
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.create
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { {'family_id' => "#{@family.id}", 'name' => 'Rolô', 'sale_price' => '5.54', 'finished' => true, 'horizontal_increment' => '10', 'vertical_increment' => '11', 'dimensions' => [
          {'initial_width' => '10', 'final_width' => '30', 'initial_height' => '10', 'final_height' => '30', 'weight' => '0'},
          {'initial_width' => '40', 'final_width' => '50', 'initial_height' => '40', 'final_height' => '60', 'weight' => '1'}
        ], 'categories' => [
          {'category' => "#{@category.id}", 'required' => true},
          {'category' => "#{@category2.id}", 'required' => true},
          {'category' => "#{@category3.id}", 'required' => false}
        ]
      } }

      it 'creates a new Line' do
        expect {
          service.create
        }.to change(Line, :count).by(1)
      end

      it 'creates two Dimensions' do
        expect {
          service.create
        }.to change(Dimension, :count).by(2)
      end

      it 'creates three Line categories' do
        expect {
          service.create
        }.to change(LineCategory, :count).by(3)
      end

      it 'returns the created line' do
        service.create

        expect(service.record).to eq(Line.last)
      end

      it 'returns true' do
        service.create
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.create

        line = service.record

        expect(line.family).to eq(@family)
        expect(line.name).to eq('Rolô')
        expect(line.sale_price).to eq(BigDecimal.new('5.54'))
        expect(line.horizontal_increment).to eq(10)
        expect(line.vertical_increment).to eq(11)
        expect(line.dimensions).to_not be_empty
        expect(line.dimensions.count).to eq(2)
        expect(line.categories).to_not be_empty
        expect(line.categories.count).to eq(3)
        expect(line.finished).to eq(true)
        
        expect(line.dimensions[0].width).to eq(10...31)
        expect(line.dimensions[0].height).to eq(10...31)
        expect(line.dimensions[0].general?).to be_truthy

        expect(line.dimensions[1].width).to eq(40...51)
        expect(line.dimensions[1].height).to eq(40...61)
        expect(line.dimensions[1].light?).to be_truthy

        expect(line.categories[0].category_id).to eq(@category.id)
        expect(line.categories[0].required).to eq(true)
        expect(line.categories[0].order).to eq(0)
        expect(line.categories[1].category_id).to eq(@category2.id)
        expect(line.categories[1].required).to eq(true)
        expect(line.categories[1].order).to eq(1)
        expect(line.categories[2].category_id).to eq(@category3.id)
        expect(line.categories[2].required).to eq(false)
        expect(line.categories[2].order).to eq(2)
      end
    end
  end

  describe 'update' do
    let(:service) { described_class.new(parameters) }

    before(:each) do
      @line = FactoryGirl.create(:line, family: @family, horizontal_increment: 10, vertical_increment: 11, finished: false)

      @line.dimensions << FactoryGirl.build(:dimension, height: 10..50, width: 10..20)
      @line.dimensions << FactoryGirl.build(:dimension, height: 70..90, width: 30..40)
      @line.dimensions << FactoryGirl.build(:dimension, height: 60..60, width: 10..40)

      @line.categories << FactoryGirl.build(:line_category, category: @category, order: 0)
      @line.categories << FactoryGirl.build(:line_category, category: @category3, order: 1)

      @line.save!
    end

    context 'with invalid parameters' do
      let(:parameters) { {'name' => ''} }

      it 'does not create a new Line' do
        expect {
          service.update(@line.id)
        }.to change(Line, :count).by(0)
      end

      it 'returns false' do
        service.update(@line.id)
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.update(@line.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { {'family_id' => "#{@family.id}", 'name' => 'Rolô', 'sale_price' => '5.54', 'finished' => false, 'horizontal_increment' => '10', 'vertical_increment' => '11', 'dimensions' => [
          {'initial_width' => '10', 'final_width' => '30', 'initial_height' => '10', 'final_height' => '30', 'weight' => '1'},
          {'initial_width' => '40', 'final_width' => '50', 'initial_height' => '40', 'final_height' => '60', 'weight' => '0'}
        ], 'categories' => [
          {'category' => "#{@category.id}", 'required' => true},
          {'category' => "#{@category2.id}", 'required' => true},
          {'category' => "#{@category3.id}", 'required' => false}
        ], 'erp_categories' => {"LINHA DE PRODUTO"=>"PRODUTO ACABADO", "GRUPO"=>"ADAPTADOR", "SUBGRUPO"=>"ROMANA", "TRIBUTAÇÃO"=>"TRIBUTADO ST"},
        'ncm' => '25897858', 'cst' => '5'
      } }

      it 'does not create a new Line' do
        expect {
          service.update(@line.id)
        }.to change(Line, :count).by(0)
      end

      it 'destroys the three Dimensions and create two' do
        expect {
          service.update(@line.id)
        }.to change(Dimension, :count).by(-1)
      end

      it 'destroys the two Line categories and create three' do
        expect {
          service.update(@line.id)
        }.to change(LineCategory, :count).by(1)
      end

      it 'returns the line' do
        service.update(@line.id)

        expect(service.record).to eq(Line.last)
      end

      it 'returns true' do
        service.update(@line.id)
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.update(@line.id)

        line = service.record

        expect(line.family).to eq(@family)
        expect(line.name).to eq('Rolô')
        expect(line.sale_price).to eq(BigDecimal.new('5.54'))
        expect(line.horizontal_increment).to eq(10)
        expect(line.vertical_increment).to eq(11)
        expect(line.dimensions).to_not be_empty
        expect(line.dimensions.count).to eq(2)
        expect(line.categories).to_not be_empty
        expect(line.categories.count).to eq(3)
        expect(line.finished).to eq(false)
        
        expect(line.dimensions[0].width).to eq(10...31)
        expect(line.dimensions[0].height).to eq(10...31)
        expect(line.dimensions[0].light?).to be_truthy

        expect(line.dimensions[1].width).to eq(40...51)
        expect(line.dimensions[1].height).to eq(40...61)
        expect(line.dimensions[1].general?).to be_truthy

        expect(line.categories[0].category_id).to eq(@category.id)
        expect(line.categories[0].required).to eq(true)
        expect(line.categories[0].order).to eq(0)
        expect(line.categories[1].category_id).to eq(@category2.id)
        expect(line.categories[1].required).to eq(true)
        expect(line.categories[1].order).to eq(1)
        expect(line.categories[2].category_id).to eq(@category3.id)
        expect(line.categories[2].required).to eq(false)
        expect(line.categories[2].order).to eq(2)

        expect(line.erp_categories).to eq({"LINHA DE PRODUTO"=>"PRODUTO ACABADO", "GRUPO"=>"ADAPTADOR", "SUBGRUPO"=>"ROMANA", "TRIBUTAÇÃO"=>"TRIBUTADO ST"})
        expect(line.ncm).to eq('25897858')
        expect(Line.csts[line.cst]).to eq(5)
      end
    end
  end

  describe 'duplicate' do
    let(:service) { described_class.new }

    before(:each) do
      @line = FactoryGirl.create(:line, family: @family, horizontal_increment: 10, vertical_increment: 11, finished: true)

      @line.dimensions << FactoryGirl.build(:dimension, height: 10..50, width: 10..20)
      @line.dimensions << FactoryGirl.build(:dimension, height: 70..90, width: 30..40)
      @line.dimensions << FactoryGirl.build(:dimension, height: 60..60, width: 10..40)

      @line.categories << FactoryGirl.build(:line_category, category: @category, order: 0)
      @line.categories << FactoryGirl.build(:line_category, category: @category3, order: 1)

      @line.save!
    end

    context 'with invalid parameters' do
      let(:line) { mock_model(Line, deep_clone: true) }

      before(:each) do
        allow(Line).to receive_message_chain(:includes, :find).and_return(line)
        allow(line).to receive(:save!).and_raise(ActiveRecord::RecordInvalid, line)
      end

      it 'does not create a new Line' do
        expect {
          service.duplicate(@line.id)
        }.to change(Line, :count).by(0)
      end

      it 'returns false' do
        service.duplicate(@line.id)
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.duplicate(@line.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      before(:each) do
        allow(Line).to receive_message_chain(:includes, :find).and_return(@line)
      end

      it 'should duplicate the Line' do
        expect(@line).to receive(:deep_clone).with({include: [:dimensions, :categories]})

        service.duplicate(@line.id)
      end

      it 'returns the line' do
        service.duplicate(@line.id)

        expect(service.record).to eq(Line.last)
      end

      it 'returns true' do
        service.duplicate(@line.id)
        
        expect(service.success).to eq(true)
      end
    end
  end

  describe '#inactivate' do
    let(:service) { described_class.new }

    before do
      @line = FactoryGirl.create(:line, family: @family, horizontal_increment: 10, vertical_increment: 11, active: true)
    end

    it 'inactivates the line' do
      service.inactivate(@line.id)

      expect(service.record.reload.active?).to be_falsy
    end
  end

  describe '#activate' do
    let(:service) { described_class.new }

    before do
      @line = FactoryGirl.create(:line, family: @family, horizontal_increment: 10, vertical_increment: 11, active: false)
    end

    it 'activates the line' do
      service.activate(@line.id)

      expect(service.record.reload.active?).to be_truthy
    end
  end
end
