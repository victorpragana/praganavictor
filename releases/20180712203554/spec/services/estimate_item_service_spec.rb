require 'rails_helper'

RSpec.describe EstimateItemService, type: :service do
  before do
    @item_1 = FactoryGirl.create(:item)
    @item_2 = FactoryGirl.create(:item)
    @item_3 = FactoryGirl.create(:item)

    @factory     = FactoryGirl.create(:factory, name: 'F1')
    @distributor = FactoryGirl.create(:distributor, name: 'D1')
    @store       = FactoryGirl.create(:store, distributor: @distributor, name: 'S1')
    @customer    = FactoryGirl.create(:customer, store_id: @store.id, name: 'C1')

    @price_table_factory_distributor = FactoryGirl.create(:price_table, owner: @factory, type: :for_items, name: 'PTI1')
    FactoryGirl.create(:price_table_item, item_id: @item_1.id, price: '50', price_table: @price_table_factory_distributor)
    FactoryGirl.create(:price_table_client, price_table: @price_table_factory_distributor, client: @distributor)

    @price_table_factory_store = FactoryGirl.create(:price_table, owner: @factory, type: :for_items, name: 'PTI2')
    FactoryGirl.create(:price_table_item, item_id: @item_1.id, price: '60', price_table: @price_table_factory_store)
    FactoryGirl.create(:price_table_client, price_table: @price_table_factory_store, client: @store)

    @price_table_factory_customer = FactoryGirl.create(:price_table, owner: @factory, type: :for_items, name: 'PTI3')
    FactoryGirl.create(:price_table_item, item_id: @item_1.id, price: '70', price_table: @price_table_factory_customer)
    FactoryGirl.create(:price_table_client, price_table: @price_table_factory_customer, client: @customer)

    @price_table_distributor_store = FactoryGirl.create(:price_table, owner: @distributor, type: :for_items, name: 'PTI4')
    FactoryGirl.create(:price_table_item, item_id: @item_1.id, price: '80', price_table: @price_table_distributor_store)
    FactoryGirl.create(:price_table_client, price_table: @price_table_distributor_store, client: @store)

    @price_table_distributor_customer = FactoryGirl.create(:price_table, owner: @distributor, type: :for_items, name: 'PTI5')
    FactoryGirl.create(:price_table_item, item_id: @item_1.id, price: '90', price_table: @price_table_distributor_customer)
    FactoryGirl.create(:price_table_client, price_table: @price_table_distributor_customer, client: @customer)

    @price_table_store = FactoryGirl.create(:price_table, owner: @store, type: :for_items, name: 'PTI6')
    FactoryGirl.create(:price_table_item, item_id: @item_1.id, price: '100', price_table: @price_table_store, maximum_discount: BigDecimal.new('50.1'))
    FactoryGirl.create(:price_table_client, price_table: @price_table_store, client: @customer)
  end

  describe 'add_to' do
    let(:service) {described_class.new(parameters)}

    before do
      @estimate = FactoryGirl.create(:estimate, seller: @store, client: @customer)
    end

    context 'with invalid parameters' do
      let(:parameters) { {'item_id' => "#{@item_1.id}", 'quantity' => '' } }

      it 'does not create a new Estimate Item' do
        expect {
          service.add_to(@estimate.id)
        }.to_not change(EstimateItem, :count)
      end

      it 'returns false' do
        service.add_to(@estimate.id)
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.add_to(@estimate.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { {'item_id' => "#{@item_1.id}", 'quantity' => '2.3' } }

      it 'creates a new Estimate Item' do
        expect {
          service.add_to(@estimate.id)
        }.to change(EstimateItem, :count).by(1)
      end

      it 'returns the created Estimate Item' do
        service.add_to(@estimate.id)

        expect(service.record).to eq(EstimateItem.last)
      end


      it 'returns true' do
        service.add_to(@estimate.id)
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.add_to(@estimate.id)

        estimate_item = service.record

        expect(estimate_item.item).to eq(@item_1)
        expect(estimate_item.item_code).to eq(@item_1.code)
        expect(estimate_item.item_description).to eq(@item_1.description)
        expect(estimate_item.unit).to eq(@item_1.unit)
        expect(estimate_item.type).to eq(@item_1.type)
        expect(estimate_item.quantity).to eq(BigDecimal.new('2.3'))
        expect(estimate_item.estimate).to eq(@estimate)
      end

      context 'for factory seller' do
        context 'and distributor client' do
          before do
            @estimate = FactoryGirl.create(:estimate, seller: @factory, client: @distributor)
          end

          it 'calculates the distributor value applying the factory-distributor price table' do
            service.add_to(@estimate.id)

            estimate_item = service.record

            expect(estimate_item.distributor_value).to eq(BigDecimal.new('50'))
          end
        end

        context 'and store client' do
          before do
            @estimate = FactoryGirl.create(:estimate, seller: @factory, client: @store)
          end

          it 'calculates the distributor value applying the factory-store price table' do
            service.add_to(@estimate.id)

            estimate_item = service.record

            expect(estimate_item.store_value).to eq(BigDecimal.new('60'))
          end
        end

        context 'and customer client' do
          before do
            @estimate = FactoryGirl.create(:estimate, seller: @factory, client: @customer)
          end

          it 'calculates the distributor value applying the factory-customer price table' do
            service.add_to(@estimate.id)

            estimate_item = service.record

            expect(estimate_item.customer_value).to eq(BigDecimal.new('70'))
          end
        end
      end

      context 'for distributor seller' do
        context 'and store client' do
          before do
            @estimate = FactoryGirl.create(:estimate, seller: @distributor, client: @store)
          end

          it 'calculates the store value applying the distributor-store price table' do
            service.add_to(@estimate.id)

            estimate_item = service.record

            expect(estimate_item.store_value).to eq(BigDecimal.new('80'))
          end
        end

        context 'and customer client' do
          before do
            @estimate = FactoryGirl.create(:estimate, seller: @distributor, client: @customer)
          end

          it 'calculates the customer value applying the distributor-customer price table' do
            service.add_to(@estimate.id)

            estimate_item = service.record

            expect(estimate_item.customer_value).to eq(BigDecimal.new('90'))
          end
        end
      end

      context 'for store seller' do
        context 'and customer client' do
          before do
            @estimate = FactoryGirl.create(:estimate, seller: @store, client: @customer)
          end

          it 'calculates the customer value applying the store-customer price table' do
            service.add_to(@estimate.id)

            estimate_item = service.record

            expect(estimate_item.customer_value).to eq(BigDecimal.new('100'))
          end
        end
      end
    end
  end

  describe 'destroy' do
    let(:service) { described_class.new }

    before(:each) do
      @estimate = FactoryGirl.create(:estimate, seller: @store, client: @customer)
      @estimate_item = FactoryGirl.create(:estimate_item, estimate: @estimate, item: @item_1)
    end

    context 'with invalid parameters' do
      let(:record)  { mock_model(EstimateItem, destroy: false, errors: double(:errors, full_messages: ['foo'])) }

      before(:each) do
        allow(EstimateItem).to receive(:find).and_return(record)
      end

      it 'does not destroy the Estimate Item' do
        expect {
          service.destroy(@estimate_item.id)
        }.to_not change(EstimateItem, :count)
      end

      it 'returns false' do
        service.destroy(@estimate_item.id)
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.destroy(@estimate_item.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      it 'destroys the Estimate Item' do
        expect {
          service.destroy(@estimate_item.id)
        }.to change(EstimateItem, :count).by(-1)
      end

      it 'returns the created Option' do
        service.destroy(@estimate_item.id)

        expect(service.record).to eq(@estimate_item)
      end


      it 'returns true' do
        service.destroy(@estimate_item.id)
        
        expect(service.success).to eq(true)
      end
    end
  end

  describe 'apply_discount' do
    let(:service) {described_class.new(parameters)}

    before do
      @estimate = FactoryGirl.create(:estimate, seller: @store, client: @customer)
      @estimate_item = FactoryGirl.create(:estimate_item, estimate: @estimate, item: @item_1)
    end

    context 'with invalid parameters' do
      let(:parameters) { {'discount' => '-1'} }

      it 'does not update the estimate item discount' do
        expect {
          service.apply_discount(@estimate_item.id)
          @estimate_item.reload
        }.to_not change(@estimate_item, :discount)
      end

      it 'returns false' do
        service.apply_discount(@estimate_item.id)
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.apply_discount(@estimate_item.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { {'discount' => '10'} }

      it 'returns the estimate item' do
        service.apply_discount(@estimate_item.id)

        expect(service.record).to eq(EstimateItem.last)
      end

      it 'returns true' do
        service.apply_discount(@estimate_item.id)
        
        expect(service.success).to eq(true)
      end

      it 'updates the options discounts' do
        service.apply_discount(@estimate_item.id)

        estimate_item = service.record

        expect(estimate_item.discount).to eq(BigDecimal.new('10'))
      end
    end
  end
end