require 'rails_helper'

RSpec.describe OrderLooseItemService, type: :service do
  describe "#apply_value" do
    before(:each) do
      @distributor      = FactoryGirl.create(:distributor, name: 'D1')
      @store            = FactoryGirl.create(:store, distributor: @distributor, name: 'S1')
      @customer         = FactoryGirl.create(:customer, store_id: @store.id, name: 'C1')
      @user             = FactoryGirl.create(:administrator, username: 'F1').tap do |administrator|
        administrator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @store, user: administrator))
      end
      @order            = FactoryGirl.create(:order, environments: [], seller: @store, client: client, billing_client: client, user: @user)
      @item             = FactoryGirl.create(:item)
      @order_loose_item = FactoryGirl.create(:order_loose_item, distributor_value: 0, store_value: 0, customer_value: 0, order: @order, item: @item)
    end

    context 'to distributor' do
      let(:parameters) {{'value' => '50.0'}}
      let(:service) {described_class.new(parameters)}
      let(:client) {@distributor}

      it 'should apply value to distributor_value' do
        service.apply_value(@order_loose_item.id)
        @order_loose_item.reload
        expect(@order_loose_item.distributor_value).to eq(50)
      end

      it 'does not apply value to store_value' do
        service.apply_value(@order_loose_item.id)
        @order_loose_item.reload
        expect(@order_loose_item.store_value).to eq(0)
      end

      it 'does not apply value to customer_value' do
        service.apply_value(@order_loose_item.id)
        @order_loose_item.reload
        expect(@order_loose_item.customer_value).to eq(0)
      end
    end

    context 'to store' do
      let(:parameters) {{'value' => '50.0'}}
      let(:service) {described_class.new(parameters)}
      let(:client) {@store}

      it 'should apply value to store_value' do
        service.apply_value(@order_loose_item.id)
        @order_loose_item.reload
        expect(@order_loose_item.store_value).to eq(50)
      end

      it 'does not apply value to distributor_value' do
        service.apply_value(@order_loose_item.id)
        @order_loose_item.reload
        expect(@order_loose_item.distributor_value).to eq(0)
      end

      it 'does not apply value to customer_value' do
        service.apply_value(@order_loose_item.id)
        @order_loose_item.reload
        expect(@order_loose_item.customer_value).to eq(0)
      end
    end

    context 'to customer' do
      let(:parameters) {{'value' => '60.0'}}
      let(:service) {described_class.new(parameters)}
      let(:client) {@customer}

      it 'should apply value to customer_value' do
        service.apply_value(@order_loose_item.id)
        @order_loose_item.reload
        expect(@order_loose_item.customer_value).to eq(60)
      end

      it 'does not apply value to distributor_value' do
        service.apply_value(@order_loose_item.id)
        @order_loose_item.reload
        expect(@order_loose_item.distributor_value).to eq(0)
      end

      it 'does not apply value to store_value' do
        service.apply_value(@order_loose_item.id)
        @order_loose_item.reload
        expect(@order_loose_item.store_value).to eq(0)
      end
    end
  end
end