require 'rails_helper'

RSpec.describe BaseComponentService, type: :service do
  before(:each) do
    @family   = FactoryGirl.create(:family, name: 'F1')
    @family2  = FactoryGirl.create(:family, name: 'F2')
    @category = FactoryGirl.create(:category, use_cut_table: true)
  end

  describe "create" do
    let(:service) {described_class.new(parameters)}

    before(:each) do
      @specification_1  = FactoryGirl.create(:specification, name: 'A1')
      @specification_2  = FactoryGirl.create(:specification, name: 'A2')
      @specification_3  = FactoryGirl.create(:specification, name: 'A3')

      @value_1_1        = FactoryGirl.create(:value, value: 'V11', specification: @specification_1)
      @value_1_2        = FactoryGirl.create(:value, value: 'V12', specification: @specification_1)
      @value_1_3        = FactoryGirl.create(:value, value: 'V13', specification: @specification_1)

      @value_2_1        = FactoryGirl.create(:value, value: 'V21', specification: @specification_2)
      @value_2_2        = FactoryGirl.create(:value, value: 'V22', specification: @specification_2)
      @value_2_3        = FactoryGirl.create(:value, value: 'V23', specification: @specification_2)

      @value_3_1        = FactoryGirl.create(:value, value: 'V31', specification: @specification_3)
      @value_3_2        = FactoryGirl.create(:value, value: 'V32', specification: @specification_3)
      @value_3_3        = FactoryGirl.create(:value, value: 'V33', specification: @specification_3)

      @cut_table        = FactoryGirl.create(:cut_table)
    end

    context 'with invalid parameters' do
      let(:parameters) {{'name' => ''}}

      it 'does not create a new BaseComponent' do
        expect {
          service.create
        }.to change(BaseComponent, :count).by(0)
      end

      it 'returns false' do
        service.create
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.create
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) {{'name' => 'Manual Mola', 'unit'=>"#{BaseComponent.units[:un]}", 'category_id' => "#{@category.id}", 'cost' => '2.54', 'fix_cost' => '5.54',
        'family_ids' => ["#{@family.id}"], 'code' => 'A12', 'reference_cost' => '1.54', 'exchange' => '2.59', 'create_children' => 'true',
        'offset_left' => '5', 'offset_right' => '8', 'offset_top' => '7', 'offset_bottom' => '9', 'allow_rotation' => 'true',
        'values' => [
          {'id' => "#{@value_1_1.id}", 'children' => [{'id' => "#{@value_2_1.id}"}, {'id' => "#{@value_2_2.id}"}]},
          {'id' => "#{@value_1_2.id}", 'children' => []},
          {'id' => "#{@value_1_3.id}", 'children' => [{'id' => "#{@value_2_1.id}"}, {'id' => "#{@value_2_3.id}", 'children' => [{'id' => "#{@value_3_1.id}"}, {'id' => "#{@value_3_2.id}"}, {'id' => "#{@value_3_3.id}"}]}]}
        ], 'loss' => '1.22', 'cut_table_id' => "#{@cut_table.id}", 'print_on_components_label' => 'true', 'print_on_lines_label' => 'true', 
      }}

      it 'creates a new BaseComponent' do
        expect {
          service.create
        }.to change(BaseComponent.with_deleted, :count).by(1)
      end

      it 'creates 10 new BaseComponentValue' do
        expect {
          service.create
        }.to change(BaseComponentValue.with_deleted, :count).by(10)
      end

      context 'creating children' do
        it 'creates 7 new components' do
          expect {
            service.create
          }.to change(Component.with_deleted, :count).by(7)
        end
      end

      context 'without creating children' do
        let(:parameters) {{'name' => 'Manual Mola', 'unit'=>"#{BaseComponent.units[:un]}", 'category_id' => "#{@category.id}", 'cost' => '2.54', 'fix_cost' => '5.54',
          'family_ids' => ["#{@family.id}"], 'code' => 'A12', 'reference_cost' => '1.54', 'exchange' => '2.59', 'create_children' => 'false',
          'offset_left' => '5', 'offset_right' => '8', 'offset_top' => '7', 'offset_bottom' => '9', 'allow_rotation' => 'true',
          'values' => [
            {'id' => "#{@value_1_1.id}", 'children' => [{'id' => "#{@value_2_1.id}"}, {'id' => "#{@value_2_2.id}"}]},
            {'id' => "#{@value_1_2.id}", 'children' => []},
            {'id' => "#{@value_1_3.id}", 'children' => [{'id' => "#{@value_2_1.id}"}, {'id' => "#{@value_2_3.id}", 'children' => [{'id' => "#{@value_3_1.id}"}, {'id' => "#{@value_3_2.id}"}, {'id' => "#{@value_3_3.id}"}]}]}
          ], 'cut_table_id' => "#{@cut_table.id}"
        }}

        it 'does not create any children' do
          expect {
            service.create
          }.to_not change(Component.with_deleted, :count)
        end
      end

      it 'returns the created BaseComponent' do
        service.create

        expect(service.record).to eq(BaseComponent.last)
      end

      it 'returns true' do
        service.create
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.create

        base_component = service.record

        expect(base_component.name).to eq('Manual Mola')
        expect(base_component.cost).to eq(BigDecimal.new('2.54'))
        expect(base_component.reference_cost).to eq(BigDecimal.new('1.54'))
        expect(base_component.exchange).to eq(BigDecimal.new('2.59'))
        expect(base_component.loss).to eq(BigDecimal.new('1.22'))
        expect(base_component.fix_cost).to eq(BigDecimal.new('5.54'))
        expect(BaseComponent.units[base_component.unit]).to eq(BaseComponent.units[:un])
        expect(base_component.category_id).to eq(@category.id)
        expect(base_component.family_ids).to match_array([@family.id])
        expect(base_component.offset_left).to eq(5)
        expect(base_component.offset_top).to eq(7)
        expect(base_component.offset_right).to eq(8)
        expect(base_component.offset_bottom).to eq(9)
        expect(base_component.allow_rotation?).to eq(true)

        expect(base_component.values).to_not be_nil
        expect(base_component.values.count).to eq(10)

        expect(base_component.components).to_not be_nil
        expect(base_component.components.count).to eq(7)

        expect(base_component.cut_table).to eq(@cut_table)

        expect(base_component.print_on_components_label?).to be_truthy
        expect(base_component.print_on_lines_label?).to be_truthy

        expect(BaseComponentValue.find_by_value_id(@value_1_1.id).children.count).to eq(2)
        expect(BaseComponentValue.find_by_value_id(@value_1_1.id).children[0].value_id).to eq(@value_2_1.id)
        expect(BaseComponentValue.find_by_value_id(@value_1_1.id).children[1].value_id).to eq(@value_2_2.id)

        expect(BaseComponentValue.find_by_value_id(@value_1_2.id).children.count).to eq(0)

        expect(BaseComponentValue.find_by_value_id(@value_1_3.id).children.count).to eq(2)
        expect(BaseComponentValue.find_by_value_id(@value_1_3.id).children[0].value_id).to eq(@value_2_1.id)
        expect(BaseComponentValue.find_by_value_id(@value_1_3.id).children[1].value_id).to eq(@value_2_3.id)

        expect(BaseComponentValue.find_by_value_id(@value_2_3.id).children.count).to eq(3)
        expect(BaseComponentValue.find_by_value_id(@value_2_3.id).children[0].value_id).to eq(@value_3_1.id)
        expect(BaseComponentValue.find_by_value_id(@value_2_3.id).children[1].value_id).to eq(@value_3_2.id)
        expect(BaseComponentValue.find_by_value_id(@value_2_3.id).children[2].value_id).to eq(@value_3_3.id)
      end

      it 'creates the child components correctly' do
        service.create

        base_component = service.record

        components = Component.where(base_component_id: base_component.id).order(updated_at: :asc)

        expect(components[0].name).to eq("#{@value_1_1.value} #{@value_2_1.value}")
        expect(components[0].code).to eq("0-0")
        expect(components[0].value_ids).to match_array([@value_1_1.id, @value_2_1.id])
        expect(components[0].base_component_id).to eq(base_component.id)
        expect(components[0].offset_left).to eq(5)
        expect(components[0].offset_top).to eq(7)
        expect(components[0].offset_right).to eq(8)
        expect(components[0].offset_bottom).to eq(9)
        expect(components[0].allow_rotation?).to eq(true)
        expect(components[0].exchange).to eq(BigDecimal.new('2.59'))
        expect(components[0].loss).to eq(BigDecimal.new('1.22'))
        expect(components[0].cut_table).to eq(@cut_table)
        expect(components[0].print_on_components_label?).to be_truthy
        expect(components[0].print_on_lines_label?).to be_truthy

        expect(components[1].name).to eq("#{@value_1_1.value} #{@value_2_2.value}")
        expect(components[1].code).to eq("0-1")
        expect(components[1].value_ids).to match_array([@value_1_1.id, @value_2_2.id])
        expect(components[1].base_component_id).to eq(base_component.id)
        expect(components[1].offset_left).to eq(5)
        expect(components[1].offset_top).to eq(7)
        expect(components[1].offset_right).to eq(8)
        expect(components[1].offset_bottom).to eq(9)
        expect(components[1].allow_rotation?).to eq(true)
        expect(components[1].exchange).to eq(BigDecimal.new('2.59'))
        expect(components[1].loss).to eq(BigDecimal.new('1.22'))
        expect(components[1].cut_table).to eq(@cut_table)
        expect(components[1].print_on_components_label?).to be_truthy
        expect(components[1].print_on_lines_label?).to be_truthy

        expect(components[2].name).to eq("#{@value_1_2.value}")
        expect(components[2].code).to eq("1")
        expect(components[2].value_ids).to match_array([@value_1_2.id])
        expect(components[2].base_component_id).to eq(base_component.id)
        expect(components[2].offset_left).to eq(5)
        expect(components[2].offset_top).to eq(7)
        expect(components[2].offset_right).to eq(8)
        expect(components[2].offset_bottom).to eq(9)
        expect(components[2].allow_rotation?).to eq(true)
        expect(components[2].exchange).to eq(BigDecimal.new('2.59'))
        expect(components[2].loss).to eq(BigDecimal.new('1.22'))
        expect(components[2].cut_table).to eq(@cut_table)
        expect(components[2].print_on_components_label?).to be_truthy
        expect(components[2].print_on_lines_label?).to be_truthy

        expect(components[3].name).to eq("#{@value_1_3.value} #{@value_2_1.value}")
        expect(components[3].code).to eq("2-0")
        expect(components[3].value_ids).to match_array([@value_1_3.id, @value_2_1.id])
        expect(components[3].base_component_id).to eq(base_component.id)
        expect(components[3].offset_left).to eq(5)
        expect(components[3].offset_top).to eq(7)
        expect(components[3].offset_right).to eq(8)
        expect(components[3].offset_bottom).to eq(9)
        expect(components[3].allow_rotation?).to eq(true)
        expect(components[3].exchange).to eq(BigDecimal.new('2.59'))
        expect(components[3].loss).to eq(BigDecimal.new('1.22'))
        expect(components[3].cut_table).to eq(@cut_table)
        expect(components[3].print_on_components_label?).to be_truthy
        expect(components[3].print_on_lines_label?).to be_truthy

        expect(components[4].name).to eq("#{@value_1_3.value} #{@value_2_3.value} #{@value_3_1.value}")
        expect(components[4].code).to eq("2-1-0")
        expect(components[4].value_ids).to match_array([@value_1_3.id, @value_2_3.id, @value_3_1.id])
        expect(components[4].base_component_id).to eq(base_component.id)
        expect(components[4].offset_left).to eq(5)
        expect(components[4].offset_top).to eq(7)
        expect(components[4].offset_right).to eq(8)
        expect(components[4].offset_bottom).to eq(9)
        expect(components[4].allow_rotation?).to eq(true)
        expect(components[4].exchange).to eq(BigDecimal.new('2.59'))
        expect(components[4].loss).to eq(BigDecimal.new('1.22'))
        expect(components[4].cut_table).to eq(@cut_table)
        expect(components[4].print_on_components_label?).to be_truthy
        expect(components[4].print_on_lines_label?).to be_truthy

        expect(components[5].name).to eq("#{@value_1_3.value} #{@value_2_3.value} #{@value_3_2.value}")
        expect(components[5].code).to eq("2-1-1")
        expect(components[5].value_ids).to match_array([@value_1_3.id, @value_2_3.id, @value_3_2.id])
        expect(components[5].base_component_id).to eq(base_component.id)
        expect(components[5].offset_left).to eq(5)
        expect(components[5].offset_top).to eq(7)
        expect(components[5].offset_right).to eq(8)
        expect(components[5].offset_bottom).to eq(9)
        expect(components[5].allow_rotation?).to eq(true)
        expect(components[5].exchange).to eq(BigDecimal.new('2.59'))
        expect(components[5].loss).to eq(BigDecimal.new('1.22'))
        expect(components[5].cut_table).to eq(@cut_table)
        expect(components[5].print_on_components_label?).to be_truthy
        expect(components[5].print_on_lines_label?).to be_truthy

        expect(components[6].name).to eq("#{@value_1_3.value} #{@value_2_3.value} #{@value_3_3.value}")
        expect(components[6].code).to eq("2-1-2")
        expect(components[6].value_ids).to match_array([@value_1_3.id, @value_2_3.id, @value_3_3.id])
        expect(components[6].base_component_id).to eq(base_component.id)
        expect(components[6].offset_left).to eq(5)
        expect(components[6].offset_top).to eq(7)
        expect(components[6].offset_right).to eq(8)
        expect(components[6].offset_bottom).to eq(9)
        expect(components[6].allow_rotation?).to eq(true)
        expect(components[6].exchange).to eq(BigDecimal.new('2.59'))
        expect(components[6].loss).to eq(BigDecimal.new('1.22'))
        expect(components[6].cut_table).to eq(@cut_table)
        expect(components[6].print_on_components_label?).to be_truthy
        expect(components[6].print_on_lines_label?).to be_truthy
      end
    end
  end

  describe 'update' do
    let(:service) {described_class.new(parameters)}

    before(:each) do
      @specification_1  = FactoryGirl.create(:specification, name: 'A1')
      @specification_2  = FactoryGirl.create(:specification, name: 'A2')
      @specification_3  = FactoryGirl.create(:specification, name: 'A3')

      @value_1_1        = FactoryGirl.create(:value, value: 'V11', specification: @specification_1)
      @value_1_2        = FactoryGirl.create(:value, value: 'V12', specification: @specification_1)
      @value_1_3        = FactoryGirl.create(:value, value: 'V13', specification: @specification_1)

      @value_2_1        = FactoryGirl.create(:value, value: 'V21', specification: @specification_2)
      @value_2_2        = FactoryGirl.create(:value, value: 'V22', specification: @specification_2)
      @value_2_3        = FactoryGirl.create(:value, value: 'V23', specification: @specification_2)

      @value_3_1        = FactoryGirl.create(:value, value: 'V31', specification: @specification_3)
      @value_3_2        = FactoryGirl.create(:value, value: 'V32', specification: @specification_3)
      @value_3_3        = FactoryGirl.create(:value, value: 'V33', specification: @specification_3)

      @cut_table        = FactoryGirl.create(:cut_table)

      service = described_class.new({'name' => 'Manual Mola', 'unit'=>"#{BaseComponent.units[:un]}", 'category_id' => "#{@category.id}", 'cost' => '2.54', 'fix_cost' => '5.54',
        'family_ids' => ["#{@family.id}"], 'code' => 'A12', 'reference_cost' => '1.54', 'exchange' => '2.59', 'create_children' => 'true',
        'values' => [
          {'id' => "#{@value_1_3.id}", 'children' => [{'id' => "#{@value_2_1.id}"}, {'id' => "#{@value_2_3.id}", 'children' => [{'id' => "#{@value_3_1.id}"}, {'id' => "#{@value_3_2.id}"}, {'id' => "#{@value_3_3.id}"}]}]}
        ], 'loss' => '1.22', 'cut_table_id' => "#{@cut_table.id}", 'print_on_components_label' => 'false',  'print_on_lines_label' => 'true'})

      service.create

      @base_component = service.record
    end

    context 'with invalid parameters' do
      let(:parameters) {{'name' => ''}}

      it 'returns false' do
        service.update(@base_component.id)
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.update(@base_component.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) {{'unit'=>"#{BaseComponent.units[:un]}", 'category_id' => "#{@category.id}", 'cost' => '2.54', 
        'family_ids' => ["#{@family.id}"], 'characteristics' => 'Under the Roof', 'fix_cost' => '5.54', 'reference_cost' => '1.54', 'exchange' => '2.68',
        'create_children' => 'true', 'offset_left' => '5', 'offset_right' => '8', 'offset_top' => '7', 'offset_bottom' => '9', 'allow_rotation' => 'true',
        'print_on_components_label' => 'true', 'print_on_lines_label' => 'true', 
        'values' => [
          {'id' => "#{@value_1_1.id}", 'children' => [{'id' => "#{@value_2_1.id}"}, {'id' => "#{@value_2_2.id}"}]},
          {'id' => "#{@value_1_2.id}", 'children' => []},
          {'id' => "#{@value_1_3.id}", 'children' => [{'id' => "#{@value_2_1.id}"}, {'id' => "#{@value_2_3.id}", 'children' => [{'id' => "#{@value_3_1.id}"}, {'id' => "#{@value_3_2.id}"}, {'id' => "#{@value_3_3.id}"}]}]}
        ], 'loss' => '1.27', 'cut_table_id' => "#{@cut_table.id}", "edit" => {
            'category_id' => true, 'unit' => true, 'cut_table_id' => true, 'allow_rotation' => true, 'name' => true, 'cost' => true, 'reference_cost' => true,
            'exchange' => true, 'loss' => true, 'fix_cost' => true, 'family_ids' => true, 'characteristics' => true, 'offset_top' => true, 'offset_left' => true,
            'offset_right' => true, 'offset_bottom' => true, 'print_on_components_label' => true,  'print_on_lines_label' => true
          }
      }}

      it 'does not create a new Base Component' do
        expect {
          service.update(@base_component.id)
        }.to change(BaseComponent.unscoped, :count).by(0)
      end

      context 'creating children' do
        it 'creates 3 new components' do
          expect {
            service.update(@base_component.id)
          }.to change(Component, :count).by(3)
        end
      end

      context 'without creating children' do
        let(:parameters) {{'unit'=>"#{BaseComponent.units[:un]}", 'category_id' => "#{@category.id}", 'cost' => '2.54', 
          'family_ids' => ["#{@family.id}"], 'characteristics' => 'Under the Roof', 'fix_cost' => '5.54', 'reference_cost' => '1.54', 'exchange' => '2.68',
          'create_children' => 'false', 'offset_left' => '5', 'offset_right' => '8', 'offset_top' => '7', 'offset_bottom' => '9', 'allow_rotation' => 'true',
          'values' => [
            {'id' => "#{@value_1_1.id}", 'children' => [{'id' => "#{@value_2_1.id}"}, {'id' => "#{@value_2_2.id}"}]},
            {'id' => "#{@value_1_2.id}", 'children' => []},
            {'id' => "#{@value_1_3.id}", 'children' => [{'id' => "#{@value_2_1.id}"}, {'id' => "#{@value_2_3.id}", 'children' => [{'id' => "#{@value_3_1.id}"}, {'id' => "#{@value_3_2.id}"}, {'id' => "#{@value_3_3.id}"}]}]}
          ], 'cut_table_id' => "#{@cut_table.id}", "edit" => {
            'category_id' => true, 'unit' => true, 'cut_table_id' => true, 'allow_rotation' => true, 'name' => true, 'cost' => true, 'reference_cost' => true,
            'exchange' => true, 'loss' => true, 'fix_cost' => true, 'family_ids' => true, 'characteristics' => true, 'offset_top' => true, 'offset_left' => true,
            'offset_right' => true, 'offset_bottom' => true
          }
        }}

        it 'does not create any new component' do
          expect {
            service.update(@base_component.id)
          }.to_not change(Component, :count)
        end
      end

      it 'returns the component' do
        service.update(@base_component.id)

        expect(service.record).to eq(BaseComponent.last)
      end

      it 'returns true' do
        service.update(@base_component.id)
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.update(@base_component.id)

        base_component = service.record

        components = base_component.components.order(created_at: :desc)

        expect(components[0].cost).to eq(BigDecimal.new('2.54'))
        expect(components[0].reference_cost).to eq(BigDecimal.new('1.54'))
        expect(components[0].exchange).to eq(BigDecimal.new('2.68'))
        expect(components[0].loss).to eq(BigDecimal.new('1.27'))
        expect(components[0].fix_cost).to eq(BigDecimal.new('5.54'))
        expect(components[1].cost).to eq(BigDecimal.new('2.54'))
        expect(components[1].reference_cost).to eq(BigDecimal.new('1.54'))
        expect(components[1].exchange).to eq(BigDecimal.new('2.68'))
        expect(components[1].loss).to eq(BigDecimal.new('1.27'))
        expect(components[1].fix_cost).to eq(BigDecimal.new('5.54'))
        expect(Component.units[components[0].unit]).to eq(Component.units[:un])
        expect(Component.units[components[1].unit]).to eq(Component.units[:un])
        expect(components[0].category_id).to eq(@category.id)
        expect(components[1].category_id).to eq(@category.id)
        expect(components[0].family_ids).to match_array([@family.id])
        expect(components[1].family_ids).to match_array([@family.id])
        expect(components[0].characteristics).to eq('Under the Roof')
        expect(components[1].characteristics).to eq('Under the Roof')
        expect(base_component.cost).to eq(BigDecimal.new('2.54'))
        expect(base_component.reference_cost).to eq(BigDecimal.new('1.54'))
        expect(base_component.fix_cost).to eq(BigDecimal.new('5.54'))
        expect(base_component.exchange).to eq(BigDecimal.new('2.68'))
        expect(base_component.loss).to eq(BigDecimal.new('1.27'))
        expect(base_component.offset_left).to eq(5)
        expect(base_component.offset_top).to eq(7)
        expect(base_component.offset_right).to eq(8)
        expect(base_component.offset_bottom).to eq(9)
        expect(base_component.allow_rotation?).to eq(true)
        expect(Component.units[base_component.unit]).to eq(Component.units[:un])
        expect(base_component.category_id).to eq(@category.id)
        expect(base_component.family_ids).to match_array([@family.id])

        expect(base_component.values).to_not be_nil
        expect(base_component.values.count).to eq(10)

        expect(base_component.components).to_not be_nil
        expect(base_component.components.count).to eq(7)

        expect(base_component.cut_table).to eq(@cut_table)

        expect(base_component.print_on_components_label?).to be_truthy
        expect(base_component.print_on_lines_label?).to be_truthy

        expect(BaseComponentValue.find_by_value_id(@value_1_1.id).children.count).to eq(2)
        expect(BaseComponentValue.find_by_value_id(@value_1_1.id).children[0].value_id).to eq(@value_2_1.id)
        expect(BaseComponentValue.find_by_value_id(@value_1_1.id).children[1].value_id).to eq(@value_2_2.id)

        expect(BaseComponentValue.find_by_value_id(@value_1_2.id).children.count).to eq(0)

        expect(BaseComponentValue.find_by_value_id(@value_1_3.id).children.count).to eq(2)
        expect(BaseComponentValue.find_by_value_id(@value_1_3.id).children[0].value_id).to eq(@value_2_1.id)
        expect(BaseComponentValue.find_by_value_id(@value_1_3.id).children[1].value_id).to eq(@value_2_3.id)

        expect(BaseComponentValue.find_by_value_id(@value_2_3.id).children.count).to eq(3)
        expect(BaseComponentValue.find_by_value_id(@value_2_3.id).children[0].value_id).to eq(@value_3_1.id)
        expect(BaseComponentValue.find_by_value_id(@value_2_3.id).children[1].value_id).to eq(@value_3_2.id)
        expect(BaseComponentValue.find_by_value_id(@value_2_3.id).children[2].value_id).to eq(@value_3_3.id)
      end

      it 'updates the child components' do
        @base_component.components.first.destroy

        service.update(@base_component.id)

        base_component = service.record
        components = base_component.components.order(created_at: :asc)

        expect(components[0].name).to eq("#{@value_1_3.value} #{@value_2_1.value}")
        expect(components[0].code).to eq("2-0")
        expect(components[0].value_ids).to match_array([@value_1_3.id, @value_2_1.id])
        expect(components[0].base_component_id).to eq(base_component.id)
        expect(components[0].allow_rotation?).to eq(true)
        expect(components[0].cut_table).to eq(@cut_table)
        expect(components[0].print_on_components_label?).to be_truthy
        expect(components[0].print_on_lines_label?).to be_truthy

        expect(components[1].name).to eq("#{@value_1_3.value} #{@value_2_3.value} #{@value_3_1.value}")
        expect(components[1].code).to eq("2-1-0")
        expect(components[1].value_ids).to match_array([@value_1_3.id, @value_2_3.id, @value_3_1.id])
        expect(components[1].base_component_id).to eq(base_component.id)
        expect(components[1].allow_rotation?).to eq(true)
        expect(components[1].cut_table).to eq(@cut_table)
        expect(components[1].print_on_components_label?).to be_truthy
        expect(components[1].print_on_lines_label?).to be_truthy

        expect(components[2].name).to eq("#{@value_1_3.value} #{@value_2_3.value} #{@value_3_2.value}")
        expect(components[2].code).to eq("2-1-1")
        expect(components[2].value_ids).to match_array([@value_1_3.id, @value_2_3.id, @value_3_2.id])
        expect(components[2].base_component_id).to eq(base_component.id)
        expect(components[2].allow_rotation?).to eq(true)
        expect(components[2].cut_table).to eq(@cut_table)
        expect(components[2].print_on_components_label?).to be_truthy
        expect(components[2].print_on_lines_label?).to be_truthy

        expect(components[3].name).to eq("#{@value_1_3.value} #{@value_2_3.value} #{@value_3_3.value}")
        expect(components[3].code).to eq("2-1-2")
        expect(components[3].value_ids).to match_array([@value_1_3.id, @value_2_3.id, @value_3_3.id])
        expect(components[3].base_component_id).to eq(base_component.id)
        expect(components[3].allow_rotation?).to eq(true)
        expect(components[3].cut_table).to eq(@cut_table)
        expect(components[3].print_on_components_label?).to be_truthy
        expect(components[3].print_on_lines_label?).to be_truthy

        expect(components[4].name).to eq("#{@value_1_1.value} #{@value_2_1.value}")
        expect(components[4].code).to eq("0-0")
        expect(components[4].value_ids).to match_array([@value_1_1.id, @value_2_1.id])
        expect(components[4].base_component_id).to eq(base_component.id)
        expect(components[4].allow_rotation?).to eq(true)
        expect(components[4].cut_table).to eq(@cut_table)
        expect(components[4].print_on_components_label?).to be_truthy
        expect(components[4].print_on_lines_label?).to be_truthy

        expect(components[5].name).to eq("#{@value_1_1.value} #{@value_2_2.value}")
        expect(components[5].code).to eq("0-1")
        expect(components[5].value_ids).to match_array([@value_1_1.id, @value_2_2.id])
        expect(components[5].base_component_id).to eq(base_component.id)
        expect(components[5].allow_rotation?).to eq(true)
        expect(components[5].cut_table).to eq(@cut_table)
        expect(components[5].print_on_components_label?).to be_truthy
        expect(components[5].print_on_lines_label?).to be_truthy

        expect(components[6].name).to eq("#{@value_1_2.value}")
        expect(components[6].code).to eq("1")
        expect(components[6].value_ids).to match_array([@value_1_2.id])
        expect(components[6].base_component_id).to eq(base_component.id)
        expect(components[6].allow_rotation?).to eq(true)
        expect(components[6].cut_table).to eq(@cut_table)
        expect(components[6].print_on_components_label?).to be_truthy
        expect(components[6].print_on_lines_label?).to be_truthy
      end

      context 'updating child specific field' do
        UPDATE_FIELDS = ['category_id', 'unit', 'cut_table_id', 'allow_rotation', 'cost' , 'reference_cost', 'exchange', 'loss', 'fix_cost', 'family_ids',
          'characteristics', 'offset_top', 'offset_left', 'offset_right', 'offset_bottom', 'print_on_components_label', 'print_on_lines_label']

        before do
          @category_2  = FactoryGirl.create(:category, use_cut_table: true)
          @cut_table_2 = FactoryGirl.create(:cut_table)
        end

        let(:parameters) {{'unit'=>"#{BaseComponent.units[:pleat]}", 'category_id' => "#{@category_2.id}", 'cost' => '1.11', 
          'family_ids' => ["#{@family2.id}"], 'characteristics' => 'Foob', 'fix_cost' => '2.22', 'reference_cost' => '3.33', 'exchange' => '4.44',
          'create_children' => 'false', 'offset_left' => '1', 'offset_right' => '2', 'offset_top' => '3', 'offset_bottom' => '4', 'allow_rotation' => 'false',
          'print_on_components_label' => 'true',  'print_on_lines_label' => 'true', 
          'cut_table_id' => "#{@cut_table_2.id}", 'loss' => '88.88', "edit" => {
            'category_id' => false, 'unit' => false, 'cut_table_id' => false, 'allow_rotation' => false, 'name' => false, 'cost' => false, 'reference_cost' => false,
            'exchange' => false, 'loss' => false, 'fix_cost' => false, 'family_ids' => false, 'characteristics' => false, 'offset_top' => false, 'offset_left' => false,
            'offset_right' => false, 'offset_bottom' => false
          }
        }}

        UPDATE_FIELDS.each do |field|
          it 'does not update the children fields that are not marked to update' do
            parameters['edit'][field] = true
            (UPDATE_FIELDS - [field]).each {|f| parameters['edit'][f] = false}

            @base_component.components.each do |component|
              (UPDATE_FIELDS - [field]).each do |not_updated_field|
                expect {
                  service.update(@base_component.id)
                }.to_not change(component.reload, not_updated_field.to_sym)
              end
            end
          end

          it "updates only the children #{field} field" do
            parameters['edit'][field] = true
            (UPDATE_FIELDS - [field]).each {|f| parameters['edit'][f] = false}

            service.update(@base_component.id)

            @base_component.reload.components.each do |component|
              component.reload

              if field == 'unit'
                expect(Component.units[component.send(field.to_sym)]).to eq(Component.units[:pleat])
              else
                case component.send(field.to_sym)
                when Fixnum
                  expect(component.send(field.to_sym)).to eq(parameters[field].to_i)
                when BigDecimal
                  expect(component.send(field.to_sym)).to eq(parameters[field].to_d)
                when Array
                  expect(component.send(field.to_sym).map(&:to_i)).to match_array(parameters[field].map(&:to_i))
                when FalseClass
                  expect(component.send(field.to_sym)).to be_falsy
                when TrueClass
                  expect(component.send(field.to_sym)).to be_truthy
                when NilClass
                  expect(component.send(field.to_sym)).to be_nil
                else
                  expect(component.send(field.to_sym)).to eq(parameters[field])
                end
              end
            end
          end
        end
      end
    end
  end

  describe '#activate' do
    let(:service) { described_class.new }

    before do
      @specification_1  = FactoryGirl.create(:specification, name: 'A1')
      @specification_2  = FactoryGirl.create(:specification, name: 'A2')
      @specification_3  = FactoryGirl.create(:specification, name: 'A3')

      @value_1_1        = FactoryGirl.create(:value, value: 'V11', specification: @specification_1)
      @value_1_2        = FactoryGirl.create(:value, value: 'V12', specification: @specification_1)
      @value_1_3        = FactoryGirl.create(:value, value: 'V13', specification: @specification_1)

      @value_2_1        = FactoryGirl.create(:value, value: 'V21', specification: @specification_2)
      @value_2_2        = FactoryGirl.create(:value, value: 'V22', specification: @specification_2)
      @value_2_3        = FactoryGirl.create(:value, value: 'V23', specification: @specification_2)

      @value_3_1        = FactoryGirl.create(:value, value: 'V31', specification: @specification_3)
      @value_3_2        = FactoryGirl.create(:value, value: 'V32', specification: @specification_3)
      @value_3_3        = FactoryGirl.create(:value, value: 'V33', specification: @specification_3)

      @cut_table        = FactoryGirl.create(:cut_table)

      service = described_class.new({'name' => 'Manual Mola', 'unit'=>"#{BaseComponent.units[:un]}", 'category_id' => "#{@category.id}", 'cost' => '2.54', 'fix_cost' => '5.54',
        'family_ids' => ["#{@family.id}"], 'code' => 'A12', 'reference_cost' => '1.54', 'exchange' => '2.59', 'create_children' => 'true',
        'values' => [
          {'id' => "#{@value_1_3.id}", 'children' => [{'id' => "#{@value_2_1.id}"}, {'id' => "#{@value_2_3.id}", 'children' => [{'id' => "#{@value_3_1.id}"}, {'id' => "#{@value_3_2.id}"}, {'id' => "#{@value_3_3.id}"}]}]}
        ], 'loss' => '1.22', 'cut_table_id' => "#{@cut_table.id}"})

      service.create

      @base_component = service.record

      @base_component.destroy

      @base_component.reload
    end

    it 'activates the base component' do
      service.activate(@base_component.id, false)

      expect(service.record.deleted?).to eq(false)
    end

    context 'without including children' do
      it 'does not activate the children' do
        service.activate(@base_component.id, false)

        expect(service.record.components.with_deleted.map {|object| object.deleted?}).to_not include(false)
      end
    end

    context 'including children' do
      it 'activates the children' do
        service.activate(@base_component.id, true)

        expect(service.record.components.with_deleted.map {|object| object.deleted?}).to_not include(true)
      end
    end
  end
end
