require 'rails_helper'

RSpec.describe LocationService, type: :service do
  before(:each) do
    @distributor = FactoryGirl.create(:distributor, name: 'D1')
    @store       = FactoryGirl.create(:store, distributor: @distributor, name: 'S1')
    @customer    = FactoryGirl.create(:customer, store_id: @store.id, name: 'C1')
    @estimate    = FactoryGirl.create(:estimate, seller: @store, client: @customer)
    @environment = FactoryGirl.create(:environment, estimate: @estimate)
  end

  describe 'create' do
    let(:service) {described_class.new(parameters)}

    context 'with invalid parameters' do
      let(:parameters) { {'category' => '', 'name' => 'Foo', 'environment_id' => "#{@environment.id}"} }

      it 'does not create a new Location' do
        expect {
          service.create
        }.to change(Location, :count).by(0)
      end

      it 'returns false' do
        service.create
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.create
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { {'category' => '0', 'name' => 'Foo', 'width' => '1000', 'height' => '1200', 'quantity' => '10',
        'environment_id' => "#{@environment.id}"} }

      it 'creates a new Location' do
        expect {
          service.create
        }.to change(Location, :count).by(1)
      end

      it 'returns the created location' do
        service.create

        expect(service.record).to eq(Location.last)
      end

      it 'returns true' do
        service.create
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.create

        location = service.record

        expect(Location.categories[location.category]).to eq(0)
        expect(location.name).to eq('Foo')
        expect(location.environment).to eq(@environment)
        expect(location.width).to eq(1000)
        expect(location.height).to eq(1200)
        expect(location.quantity).to eq(10)
      end
    end
  end

  describe 'update' do
    let(:service) {described_class.new(parameters)}

    before(:each) do
      @location = FactoryGirl.create(:location, environment: @environment)
    end

    context 'with invalid parameters' do
      let(:parameters) { {'category' => '', 'name' => 'Foo', 'environment_id' => "#{@environment.id}"} }

      it 'does not create a new Location' do
        expect {
          service.update(@location.id)
        }.to change(Location, :count).by(0)
      end

      it 'returns false' do
        service.update(@location.id)
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.update(@location.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { {'category' => '0', 'name' => 'Foo', 'width' => '1000', 'height' => '1200', 'quantity' => '10',
        'environment_id' => "#{@environment.id}"} }

      it 'does not create a new Location' do
        expect {
          service.update(@location.id)
        }.to change(Location, :count).by(0)
      end

      it 'returns the location' do
        service.update(@location.id)

        expect(service.record).to eq(Location.last)
      end

      it 'returns true' do
        service.update(@location.id)
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.update(@location.id)

        location = service.record

        expect(Location.categories[location.category]).to eq(0)
        expect(location.name).to eq('Foo')
        expect(location.width).to eq(1000)
        expect(location.height).to eq(1200)
        expect(location.quantity).to eq(10)
        expect(location.environment).to eq(@environment)
      end
    end
  end

  describe 'apply_discount' do
    let(:service) {described_class.new(parameters)}

    before(:each) do
      @family = FactoryGirl.create(:family)
      @line = FactoryGirl.create(:line, family: @family)
      @location = FactoryGirl.create(:location, environment: @environment)

      @price_table = FactoryGirl.create(:price_table, owner: @store, default: true, type: :for_lines)
      FactoryGirl.create(:price_table_line, line: @line, price: '_CMV_*2', price_table: @price_table, maximum_discount: BigDecimal.new('50.1'))

      @option_1 = FactoryGirl.create(:option, location: @location, line: @line)
      @option_2 = FactoryGirl.create(:option, location: @location, line: @line, selected: true)
    end

    context 'with invalid parameters' do
      let(:parameters) { {'discount' => '-1'} }

      it 'does not update the location\'s options discounts' do
        expect {
          service.apply_discount(@location.id)
          @option_1.reload
        }.to_not change(@option_1, :discount)
      end

      it 'does not update the location\'s options discounts' do
        expect {
          service.apply_discount(@location.id)
          @option_2.reload
        }.to_not change(@option_2, :discount)
      end

      it 'returns false' do
        service.apply_discount(@location.id)
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.apply_discount(@location.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { {'discount' => '10'} }

      it 'returns the location' do
        service.apply_discount(@location.id)

        expect(service.record).to eq(Location.last)
      end

      it 'returns true' do
        service.apply_discount(@location.id)
        
        expect(service.success).to eq(true)
      end

      it 'updates the options discounts' do
        service.apply_discount(@location.id)

        location = service.record

        expect(location.options.first.discount).to eq(BigDecimal.new('10'))
        expect(location.options.last.discount).to eq(BigDecimal.new('10'))
      end
    end
  end
end
