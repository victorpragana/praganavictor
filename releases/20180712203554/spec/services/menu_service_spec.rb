require 'rails_helper'

RSpec.describe MenuService, type: :service do
  before(:each) do
    @factory     = FactoryGirl.create(:factory, name: 'F1')
    @distributor = FactoryGirl.create(:distributor, name: 'D1')
    @store       = FactoryGirl.create(:store, name: 'S1', distributor: @distributor)
  end

  describe 'menu loading' do
    context 'when user is Administrator' do
      before(:each) do
        @administrator = FactoryGirl.create(:administrator)
      end

      context 'for factory workplace' do
        before(:each) do
          @administrator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @factory, user: @administrator))
        end

        it 'loads the entire menu' do 
          expect(
            MenuService.new.load_for(@administrator.type, @administrator.workplace.class.to_s)
          ).to match_array([
              MenuService::DASHBOARD, 
              MenuService::ENTITIES.merge({ submenu: [MenuService::DISTRIBUTORS, MenuService::STORES, MenuService::CUSTOMERS] }),
              MenuService::SHUTTER.merge({ submenu: [MenuService::ATTRIBUTES, MenuService::BASE_COMPONENTS, MenuService::COMPONENTS, MenuService::LINES] }),
              MenuService::ITEMS, 
              MenuService::COMERCIAL.merge({ submenu: [MenuService::ESTIMATES, MenuService::ORDERS, MenuService::PRODUCTION_ORDERS, MenuService::CONCEDED_CREDITS] }),
              MenuService::ADJUSTMENTS.merge({ submenu: [MenuService::USERS, MenuService::FAMILIES, MenuService::PRICE_TABLES, MenuService::CUT_TABLES, MenuService::PRINTERS, MenuService::QUESTIONS, MenuService::CLASSIFICATION, MenuService::PAYMENT_CONDITIONS] }),
              MenuService::SIMULATION,
              MenuService::DOCUMENT
            ])
        end
      end


      context 'for distributor workplace' do
        before(:each) do
          @administrator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @distributor, user: @administrator))
        end

        it 'loads the distributor admin menu' do 
          expect(
            MenuService.new.load_for(@administrator.type, @administrator.workplace.class.to_s)
          ).to match_array([
            MenuService::DASHBOARD, 
            MenuService::ENTITIES.merge({ submenu: [MenuService::STORES] }),
            MenuService::ITEMS, 
            MenuService::COMERCIAL.merge({ submenu: [MenuService::ESTIMATES, MenuService::ORDERS, MenuService::CREDITS, MenuService::CONCEDED_CREDITS] }),
            MenuService::ADJUSTMENTS.merge({ submenu:  [MenuService::USERS, MenuService::PRICE_TABLES] })
          ])
        end
      end

      context 'for store workplace' do
        before(:each) do
          @administrator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @store, user: @administrator))
        end

        it 'loads the store admin menu' do 
          expect(
            MenuService.new.load_for(@administrator.type, @administrator.workplace.class.to_s)
          ).to match_array([
            MenuService::DASHBOARD,
            MenuService::ENTITIES.merge({ submenu: [MenuService::CUSTOMERS] }),
            MenuService::ITEMS,
            MenuService::COMERCIAL.merge({ submenu: [MenuService::ESTIMATES, MenuService::ORDERS, MenuService::CREDITS] }),
            MenuService::ADJUSTMENTS.merge({ submenu:  [MenuService::USERS, MenuService::PRICE_TABLES] })
          ])
        end
      end
    end

    context 'when user is Manager' do
      before(:each) do
        @manager = FactoryGirl.create(:manager)
      end

      context 'for factory workplace' do
        before(:each) do
          @manager.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @factory, user: @manager))
        end

        it 'loads the factory manager menu' do 
          expect(
            MenuService.new.load_for(@manager.type, @manager.workplace.class.to_s)
          ).to match_array([
              MenuService::DASHBOARD, 
              MenuService::ENTITIES.merge({ submenu: [MenuService::DISTRIBUTORS, MenuService::STORES, MenuService::CUSTOMERS] }),
              MenuService::COMERCIAL.merge({ submenu: [MenuService::ESTIMATES, MenuService::ORDERS] })
            ])
        end
      end


      context 'for distributor workplace' do
        before(:each) do
          @manager.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @distributor, user: @manager))
        end

        it 'loads the distributor admin menu' do 
          expect(
            MenuService.new.load_for(@manager.type, @manager.workplace.class.to_s)
          ).to match_array([
            MenuService::DASHBOARD, 
            MenuService::ENTITIES.merge({ submenu: [MenuService::STORES] }),
            MenuService::COMERCIAL.merge({ submenu: [MenuService::ESTIMATES, MenuService::ORDERS] })
          ])
        end
      end

      context 'for store workplace' do
        before(:each) do
          @manager.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @store, user: @manager))
        end

        it 'loads the store admin menu' do 
          expect(
            MenuService.new.load_for(@manager.type, @manager.workplace.class.to_s)
          ).to match_array([
            MenuService::DASHBOARD,
            MenuService::ENTITIES.merge({ submenu: [MenuService::CUSTOMERS] }),
            MenuService::COMERCIAL.merge({ submenu: [MenuService::ESTIMATES, MenuService::ORDERS] })
          ])
        end
      end
    end

    context 'when user is Seller' do
      before(:each) do
        @seller = FactoryGirl.create(:seller)
      end

      context 'for factory workplace' do
        before(:each) do
          @seller.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @factory, user: @seller))
        end

        it 'loads the factory seller menu' do 
          expect(
            MenuService.new.load_for(@seller.type, @seller.workplace.class.to_s)
          ).to match_array([
              MenuService::DASHBOARD, 
              MenuService::ENTITIES.merge({ submenu: [MenuService::DISTRIBUTORS, MenuService::STORES, MenuService::CUSTOMERS] }),
              MenuService::COMERCIAL.merge({ submenu: [MenuService::ESTIMATES, MenuService::ORDERS] })
            ])
        end
      end


      context 'for distributor workplace' do
        before(:each) do
          @seller.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @distributor, user: @seller))
        end

        it 'loads the distributor admin menu' do 
          expect(
            MenuService.new.load_for(@seller.type, @seller.workplace.class.to_s)
          ).to match_array([
            MenuService::DASHBOARD, 
            MenuService::ENTITIES.merge({ submenu: [MenuService::STORES] }),
            MenuService::COMERCIAL.merge({ submenu: [MenuService::ESTIMATES, MenuService::ORDERS] })
          ])
        end
      end

      context 'for store workplace' do
        before(:each) do
          @seller.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @store, user: @seller))
        end

        it 'loads the store admin menu' do 
          expect(
            MenuService.new.load_for(@seller.type, @seller.workplace.class.to_s)
          ).to match_array([
            MenuService::DASHBOARD,
            MenuService::ENTITIES.merge({ submenu: [MenuService::CUSTOMERS] }),
            MenuService::COMERCIAL.merge({ submenu: [MenuService::ESTIMATES, MenuService::ORDERS] })
          ])
        end
      end
    end

    context 'when user is Operator' do
      before(:each) do
        @operator = FactoryGirl.create(:operator)
      end

      context 'for factory workplace' do
        before(:each) do
          @operator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @factory, user: @operator))
        end

        it 'loads the factory operator menu' do 
          expect(
            MenuService.new.load_for(@operator.type, @operator.workplace.class.to_s)
          ).to match_array([
              MenuService::DASHBOARD, 
              MenuService::COMERCIAL.merge({ submenu: [MenuService::PRODUCTION_ORDERS] })
          ])
        end
      end
    end
  end
end
