require 'rails_helper'

RSpec.describe MovementCreatorService, type: :service do
  describe '#input' do
    let(:service) { described_class.new(credit_type, @payment, order) }
    let(:value)   { 100 }
    let(:installments) { 3 }

    before do
      @factory = create(:factory, name: 'F1', subdomain: 'factory-f1')
      @distributor = create(:distributor)
      @store = create(:store, distributor: @distributor)
      
      @third_party = create(:third_party, subdomain: 'tp')

      @state = create(:state)
      @city = create(:city, state: @state)
      @customer = create(:customer, third_party: @third_party, store: @store, city: @city)

      @user = create(:administrator).tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: @store, user: administrator))
      end

      @payment = create(:payment, original_value: value, tax: BigDecimal.new('3.85'), value: BigDecimal.new('103.85'), installment_number: installments)
    end

    context 'with valid parameters' do
      context 'for production credit' do
        let(:order) { nil }
        let(:credit_type) { ProductionCredit }

        it 'creates a new production credit' do
          expect do
            service.input(@user.workplace.third_party, @distributor.third_party)
          end.to change(ProductionCredit, :count).by(installments)
        end

        context 'when the order is given' do
          context 'and the seller is a distributor' do
            let(:order) { create(:order, seller: @distributor, client: @customer, billing_client: @customer, user: @user) }

            it "creates 'installment_number' approved 'order_payment' movements for each month the proportional value, the distributor as owner and the factory as destination" do
              service.input(order.seller.third_party, @factory.third_party)

              movements = ProductionCredit.last(installments)

              movements.each do |movement|
                expect(movement.owner).to eq(@distributor.third_party)
                expect(movement.destination).to eq(@factory.third_party)
                expect(movement.order).to eq(order)
                expect(movement.payment).to eq(@payment)
                expect(movement.input?).to be_truthy
                expect(movement.approved?).to be_truthy
                expect(movement.order_payment?).to be_truthy
              end

              expect(movements[0].date).to be_within(1.second).of(Time.zone.now + 1.month)
              expect(movements[1].date).to be_within(1.second).of(Time.zone.now + 2.months)
              expect(movements[2].date).to be_within(1.second).of(Time.zone.now + 3.months)

              expect(movements[0].value).to eq(BigDecimal.new('33.33'))
              expect(movements[1].value).to eq(BigDecimal.new('33.33'))
              expect(movements[2].value).to eq(BigDecimal.new('33.34'))
            end
          end

          context 'and the seller is a store' do
            let(:order) { create(:order, seller: @store, client: @customer, billing_client: @customer, user: @user) }

            it "creates 'installment_number' approved 'order_payment' movements for each month the proportional value, the store as owner and the distributor as destination" do
              service.input(order.seller.third_party, @distributor.third_party)

              movements = ProductionCredit.last(installments)

              movements.each do |movement|
                expect(movement.owner).to eq(@store.third_party)
                expect(movement.destination).to eq(@distributor.third_party)
                expect(movement.order).to eq(order)
                expect(movement.payment).to eq(@payment)
                expect(movement.input?).to be_truthy
                expect(movement.approved?).to be_truthy
                expect(movement.order_payment?).to be_truthy
              end

              expect(movements[0].date).to be_within(1.second).of(Time.zone.now + 1.month)
              expect(movements[1].date).to be_within(1.second).of(Time.zone.now + 2.months)
              expect(movements[2].date).to be_within(1.second).of(Time.zone.now + 3.months)

              expect(movements[0].value).to eq(BigDecimal.new('33.33'))
              expect(movements[1].value).to eq(BigDecimal.new('33.33'))
              expect(movements[2].value).to eq(BigDecimal.new('33.34'))
            end
          end
        end

        context 'when the order is not given' do
          context 'and the workplace is a distributor' do
            before do
              @user.update!(selected_workplace: create(:user_workplace, workplace: @distributor, user: @user))
            end

            it "creates 'installment_number' approved 'credit' movements for each month the proportional value, the distributor as owner and the factory as destination" do
              service.input(@user.workplace.third_party, @factory.third_party)

              movements = ProductionCredit.last(installments)

              movements.each do |movement|
                expect(movement.owner).to eq(@distributor.third_party)
                expect(movement.destination).to eq(@factory.third_party)
                expect(movement.order).to eq(order)
                expect(movement.payment).to eq(@payment)
                expect(movement.input?).to be_truthy
                expect(movement.approved?).to be_truthy
                expect(movement.credit?).to be_truthy
              end

              expect(movements[0].date).to be_within(1.second).of(Time.zone.now + 1.month)
              expect(movements[1].date).to be_within(1.second).of(Time.zone.now + 2.months)
              expect(movements[2].date).to be_within(1.second).of(Time.zone.now + 3.months)

              expect(movements[0].value).to eq(BigDecimal.new('33.33'))
              expect(movements[1].value).to eq(BigDecimal.new('33.33'))
              expect(movements[2].value).to eq(BigDecimal.new('33.34'))
            end
          end

          context 'and the workplace is a store' do
            before do
              @user.update!(selected_workplace: create(:user_workplace, workplace: @store, user: @user))
            end

            it "creates 'installment_number' approved 'credit' movements for each month the proportional value, the store as owner and the distributor as destination" do
              service.input(@user.workplace.third_party, @distributor.third_party)

              movements = ProductionCredit.last(installments)

              movements.each do |movement|
                expect(movement.owner).to eq(@store.third_party)
                expect(movement.destination).to eq(@distributor.third_party)
                expect(movement.order).to eq(order)
                expect(movement.payment).to eq(@payment)
                expect(movement.input?).to be_truthy
                expect(movement.approved?).to be_truthy
                expect(movement.credit?).to be_truthy
              end

              expect(movements[0].date).to be_within(1.second).of(Time.zone.now + 1.month)
              expect(movements[1].date).to be_within(1.second).of(Time.zone.now + 2.months)
              expect(movements[2].date).to be_within(1.second).of(Time.zone.now + 3.months)

              expect(movements[0].value).to eq(BigDecimal.new('33.33'))
              expect(movements[1].value).to eq(BigDecimal.new('33.33'))
              expect(movements[2].value).to eq(BigDecimal.new('33.34'))
            end
          end
        end
      end

      context 'for money credit' do
        let(:credit_type) { MoneyCredit }
        let(:order) { nil }

        context 'when the order is given' do
          context 'and the seller is a distributor' do
            let(:order) { create(:order, seller: @distributor, client: @customer, billing_client: @customer, user: @user) }

            it "creates new money credits" do
              expect do
                service.input(order.seller.third_party, @factory.third_party)
              end.to change(MoneyCredit, :count).by(installments)
            end

            it "creates 'installment_number' approved 'order_payment' movements for each month the proportional value, the distributor as owner and the factory as destination" do
              service.input(order.seller.third_party, @factory.third_party)

              movements = MoneyCredit.last(installments)

              movements.each do |movement|
                expect(movement.owner).to eq(@distributor.third_party)
                expect(movement.destination).to eq(@factory.third_party)
                expect(movement.order).to eq(order)
                expect(movement.payment).to eq(@payment)
                expect(movement.input?).to be_truthy
                expect(movement.approved?).to be_truthy
                expect(movement.order_payment?).to be_truthy
              end

              expect(movements[0].date).to be_within(1.second).of(Time.zone.now + 1.month)
              expect(movements[1].date).to be_within(1.second).of(Time.zone.now + 2.months)
              expect(movements[2].date).to be_within(1.second).of(Time.zone.now + 3.months)

              expect(movements[0].value).to eq(BigDecimal.new('33.33'))
              expect(movements[1].value).to eq(BigDecimal.new('33.33'))
              expect(movements[2].value).to eq(BigDecimal.new('33.34'))
            end
          end

          context 'and the seller is a store' do
            let(:order) { create(:order, seller: @store, client: @customer, billing_client: @customer, user: @user) }

            it "creates new money credits" do
              expect do
                service.input(order.seller.third_party, @distributor.third_party)
              end.to change(MoneyCredit, :count).by(installments * 2)
            end

            it "creates 'installment_number' approved 'order_payment' movements for each month the proportional value, the store as owner and the distributor as destination" do
              service.input(order.seller.third_party, @distributor.third_party)

              movements = MoneyCredit.where(owner_id: order.seller.third_party.id).last(installments)

              movements.each do |movement|
                expect(movement.owner).to eq(@store.third_party)
                expect(movement.destination).to eq(@distributor.third_party)
                expect(movement.order).to eq(order)
                expect(movement.payment).to eq(@payment)
                expect(movement.input?).to be_truthy
                expect(movement.approved?).to be_truthy
                expect(movement.order_payment?).to be_truthy
              end

              expect(movements[0].date).to be_within(1.second).of(Time.zone.now + 1.month)
              expect(movements[1].date).to be_within(1.second).of(Time.zone.now + 2.months)
              expect(movements[2].date).to be_within(1.second).of(Time.zone.now + 3.months)

              expect(movements[0].value).to eq(BigDecimal.new('33.33'))
              expect(movements[1].value).to eq(BigDecimal.new('33.33'))
              expect(movements[2].value).to eq(BigDecimal.new('33.34'))
            end

            it "creates 'installment_number' approved 'store_credit' movements for each month the proportional value, the distributor as owner and the factory as destination" do
              service.input(order.seller.third_party, @distributor.third_party)

              movements = MoneyCredit.where(owner_id: @distributor.third_party.id).last(installments)

              movements.each do |movement|
                expect(movement.owner).to eq(@distributor.third_party)
                expect(movement.destination).to eq(@factory.third_party)
                expect(movement.order).to eq(order)
                expect(movement.payment).to eq(@payment)
                expect(movement.input?).to be_truthy
                expect(movement.approved?).to be_truthy
                expect(movement.store_credit?).to be_truthy
              end

              expect(movements[0].date).to be_within(1.second).of(Time.zone.now + 1.month)
              expect(movements[1].date).to be_within(1.second).of(Time.zone.now + 2.months)
              expect(movements[2].date).to be_within(1.second).of(Time.zone.now + 3.months)

              expect(movements[0].value).to eq(BigDecimal.new('33.33'))
              expect(movements[1].value).to eq(BigDecimal.new('33.33'))
              expect(movements[2].value).to eq(BigDecimal.new('33.34'))
            end
          end
        end

        context 'when the order is not given' do
          context 'and the workplace is a distributor' do
            before do
              @user.update!(selected_workplace: create(:user_workplace, workplace: @distributor, user: @user))
            end

            it "creates new money credits" do
              expect do
                service.input(@user.workplace.third_party, @factory.third_party)
              end.to change(MoneyCredit, :count).by(installments)
            end

            it "creates 'installment_number' approved 'credit' movements for each month the proportional value, the distributor as owner and the factory as destination" do
              service.input(@user.workplace.third_party, @factory.third_party)

              movements = MoneyCredit.last(installments)

              movements.each do |movement|
                expect(movement.owner).to eq(@distributor.third_party)
                expect(movement.destination).to eq(@factory.third_party)
                expect(movement.order).to eq(order)
                expect(movement.payment).to eq(@payment)
                expect(movement.input?).to be_truthy
                expect(movement.approved?).to be_truthy
                expect(movement.credit?).to be_truthy
              end

              expect(movements[0].date).to be_within(1.second).of(Time.zone.now + 1.month)
              expect(movements[1].date).to be_within(1.second).of(Time.zone.now + 2.months)
              expect(movements[2].date).to be_within(1.second).of(Time.zone.now + 3.months)

              expect(movements[0].value).to eq(BigDecimal.new('33.33'))
              expect(movements[1].value).to eq(BigDecimal.new('33.33'))
              expect(movements[2].value).to eq(BigDecimal.new('33.34'))
            end
          end

          context 'and the workplace is a store' do
            before do
              @user.update!(selected_workplace: create(:user_workplace, workplace: @store, user: @user))
            end

            it "creates new money credits" do
              expect do
                service.input(@user.workplace.third_party, @distributor.third_party)
              end.to change(MoneyCredit, :count).by(installments * 2)
            end

            it "creates 'installment_number' approved 'credit' movements for each month the proportional value, the store as owner and the distributor as destination" do
              service.input(@user.workplace.third_party, @distributor.third_party)

              movements = MoneyCredit.where(owner_id: @user.workplace.third_party.id).last(installments)

              movements.each do |movement|
                expect(movement.owner).to eq(@store.third_party)
                expect(movement.destination).to eq(@distributor.third_party)
                expect(movement.order).to eq(order)
                expect(movement.payment).to eq(@payment)
                expect(movement.input?).to be_truthy
                expect(movement.approved?).to be_truthy
                expect(movement.credit?).to be_truthy
              end

              expect(movements[0].date).to be_within(1.second).of(Time.zone.now + 1.month)
              expect(movements[1].date).to be_within(1.second).of(Time.zone.now + 2.months)
              expect(movements[2].date).to be_within(1.second).of(Time.zone.now + 3.months)

              expect(movements[0].value).to eq(BigDecimal.new('33.33'))
              expect(movements[1].value).to eq(BigDecimal.new('33.33'))
              expect(movements[2].value).to eq(BigDecimal.new('33.34'))
            end

            it "creates 'installment_number' approved 'store_credit' movements for each month the proportional value, the distributor as owner and the factory as destination" do
              service.input(@user.workplace.third_party, @distributor.third_party)

              movements = MoneyCredit.where(owner_id: @distributor.third_party.id).last(installments)

              movements.each do |movement|
                expect(movement.owner).to eq(@distributor.third_party)
                expect(movement.destination).to eq(@factory.third_party)
                expect(movement.order).to eq(order)
                expect(movement.payment).to eq(@payment)
                expect(movement.input?).to be_truthy
                expect(movement.approved?).to be_truthy
                expect(movement.store_credit?).to be_truthy
              end

              expect(movements[0].date).to be_within(1.second).of(Time.zone.now + 1.month)
              expect(movements[1].date).to be_within(1.second).of(Time.zone.now + 2.months)
              expect(movements[2].date).to be_within(1.second).of(Time.zone.now + 3.months)

              expect(movements[0].value).to eq(BigDecimal.new('33.33'))
              expect(movements[1].value).to eq(BigDecimal.new('33.33'))
              expect(movements[2].value).to eq(BigDecimal.new('33.34'))
            end
          end
        end
      end
    end
    
    context 'with invalid parameters' do
      let(:order) { nil }
      let(:credit_type) { [ProductionCredit, MoneyCredit].sample }

      it 'does not create any new movement' do
        expect do
          service.input(@factory.third_party, @distributor.third_party)
        end.to_not change(Movement, :count)
      end

      it 'returns false' do
        service.input(@factory.third_party, @distributor.third_party)

        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.input(@factory.third_party, @distributor.third_party)

        expect(service.errors).to_not be_empty
      end
    end
  end
end
