require 'rails_helper'

RSpec.describe PaymentConditionService, type: :service do
  describe 'create' do
    let(:service) { described_class.new(parameters) }

    context 'with invalid parameters' do
      let(:parameters) { { 'installments' => '' } }

      it 'does not create a new payment condition' do
        expect {
          service.create
        }.to change(PaymentCondition, :count).by(0)
      end

      it 'returns false' do
        service.create
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.create
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { { 'installments' => '5', 'additional_tax' => '12.22', 'credit_tax' => '1.22', 'antecipation_tax' => '9.88', 'credit_antecipation_tax' => '25.69', 'type' => '1' } }

      it 'creates a new payment condition' do
        expect {
          service.create
        }.to change(PaymentCondition, :count).by(1)
      end

      it 'returns the created payment condition' do
        service.create

        expect(service.record).to eq(PaymentCondition.last)
      end

      it 'returns true' do
        service.create
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.create

        payment_condition = service.record

        expect(payment_condition.installments).to eq(5)
        expect(payment_condition.bank_slip?).to be_truthy
        expect(payment_condition.additional_tax).to eq(BigDecimal.new('12.22'))
        expect(payment_condition.credit_tax).to eq(BigDecimal.new('1.22'))
        expect(payment_condition.antecipation_tax).to eq(BigDecimal.new('9.88'))
        expect(payment_condition.credit_antecipation_tax).to eq(BigDecimal.new('25.69'))
      end
    end
  end

  describe 'update' do
    let(:service) { described_class.new(parameters) }

    before do
      @payment_condition = create(:payment_condition)
    end

    context 'with invalid parameters' do
      let(:parameters) { { 'installments' => '' } }

      it 'returns false' do
        service.update(@payment_condition.id)
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.update(@payment_condition.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { { 'installments' => '5', 'additional_tax' => '12.22', 'credit_tax' => '1.22', 'antecipation_tax' => '9.88', 'credit_antecipation_tax' => '25.69', 'type' => '1' } }

      it 'does not create a new payment condition' do
        expect {
          service.update(@payment_condition.id)
        }.to change(PaymentCondition, :count).by(0)
      end

      it 'returns the payment condition' do
        service.update(@payment_condition.id)

        expect(service.record).to eq(PaymentCondition.last)
      end

      it 'returns true' do
        service.update(@payment_condition.id)
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.update(@payment_condition.id)

        payment_condition = service.record

        expect(payment_condition.installments).to eq(5)
        expect(payment_condition.bank_slip?).to be_truthy
        expect(payment_condition.additional_tax).to eq(BigDecimal.new('12.22'))
        expect(payment_condition.credit_tax).to eq(BigDecimal.new('1.22'))
        expect(payment_condition.antecipation_tax).to eq(BigDecimal.new('9.88'))
        expect(payment_condition.credit_antecipation_tax).to eq(BigDecimal.new('25.69'))
      end
    end
  end
end