require 'rails_helper'

RSpec.describe ComponentTotalCostCalculatorService, type: :service do
  let(:width)  {250}
  let(:height) {500}

  before(:each) do
    @family = FactoryGirl.create(:family)

    @category = FactoryGirl.create(:category, name: 'C1')
    @category_2 = FactoryGirl.create(:category, name: 'C2')
    @category_3 = FactoryGirl.create(:category, name: 'C3')

    @line = FactoryGirl.create(:line, family: @family)

    @line.categories << FactoryGirl.build(:line_category, category: @category, required: true)
    @line.categories << FactoryGirl.build(:line_category, category: @category_3, required: false)

    @line.save!

    @specification = FactoryGirl.build(:specification, automatic: true)
    @specification.values << FactoryGirl.build(:value, value: 'V1', comparison_value: 1000)
    @specification.values << FactoryGirl.build(:value, value: 'V2', comparison_value: 1500)
    @specification.values << FactoryGirl.build(:value, value: 'V3', comparison_value: 2000)
    @specification.save!

    @component_1 = FactoryGirl.create(:component, name: 'C1', unit: :cm, values: [@specification.values[0]], category: @category, erasable: true, cost: BigDecimal.new('10'))
    @component_2 = FactoryGirl.create(:component, name: 'C2', unit: :cm, values: [@specification.values[1]], category: @category_3, erasable: true, cost: BigDecimal.new('12'))
    @component_3 = FactoryGirl.create(:component, name: 'C3', unit: :cm, values: [@specification.values[2]], category: @category_3, erasable: true, cost: BigDecimal.new('15'))
    @component_4 = FactoryGirl.create(:component, name: 'C4', unit: :cm, values: [@specification.values[2]], category: @category_2, erasable: true, cost: BigDecimal.new('15'))

    @line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], 
      total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*2', width_consumption: '_ALTURA_*3', 
      height_consumption: '_LARGURA_ + 3', automatic: true)
    FactoryGirl.create(:rule, line_rule: @line_rule, component: @component_1, control_method: Rule.control_methods[:width], 
      total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*2', width_consumption: '_ALTURA_*3', 
      height_consumption: '_LARGURA_ + 3', automatic: true)
    FactoryGirl.create(:rule, line_rule: @line_rule, component: @component_3, control_method: Rule.control_methods[:width], 
      total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*2', width_consumption: '_ALTURA_*3', 
      height_consumption: '_LARGURA_ + 3', automatic: true)
  end

  describe 'validation' do
    it_should_behave_like 'a base calculator'

    it 'fails if the component category does not belong to line' do
      expect(described_class.new(@line.id, @component_4.id, width, height)).to be_invalid
    end

    it 'fails if the component is not automatic' do
      @component_1.values = []
      @component_1.save!

      expect(described_class.new(@line.id, @component_1.id, width, height)).to be_invalid
    end
  end

  describe 'component consumption' do
    before(:each) do
      LineRule.destroy_all
    end

    context 'for width control rule' do
      before(:each) do
        @line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], 
          total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*2', width_consumption: '_ALTURA_*3', 
          height_consumption: '_LARGURA_ + 3', automatic: true)
        FactoryGirl.create(:rule, line_rule: @line_rule, component: @component_1, control_method: Rule.control_methods[:width], 
          total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*2', width_consumption: '_ALTURA_*3', 
          height_consumption: '_LARGURA_ + 3', automatic: true)
        FactoryGirl.create(:rule, line_rule: @line_rule, component: @component_2, control_method: Rule.control_methods[:width], 
          total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*2', width_consumption: '_ALTURA_*3', 
          height_consumption: '_LARGURA_ + 3', automatic: true)
        FactoryGirl.create(:rule, line_rule: @line_rule, component: @component_3, control_method: Rule.control_methods[:width], 
          total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*2', width_consumption: '_ALTURA_*3', 
          height_consumption: '_LARGURA_ + 3', automatic: true)
      end

      it 'returns the height information as consumption' do
        service = described_class.new(@line.id, @component_1.id, 100, 600)

        expect(service.consumption).to eq(600)
      end

      it 'returns the height information as consumption' do
        service = described_class.new(@line.id, @component_2.id, 150, 500)

        expect(service.consumption).to eq(500)
      end

      it 'returns the height information as consumption' do
        service = described_class.new(@line.id, @component_3.id, 200, 300)

        expect(service.consumption).to eq(300)
      end
    end

    context 'for height control rule' do
      before(:each) do
        @line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:height], 
          total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*2', width_consumption: '_ALTURA_*3', 
          height_consumption: '_LARGURA_ + 3', automatic: true)
        FactoryGirl.create(:rule, line_rule: @line_rule, component: @component_1, control_method: Rule.control_methods[:height], 
          total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*2', width_consumption: '_ALTURA_*3', 
          height_consumption: '_LARGURA_ + 3', automatic: true)
        FactoryGirl.create(:rule, line_rule: @line_rule, component: @component_2, control_method: Rule.control_methods[:height], 
          total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*2', width_consumption: '_ALTURA_*3', 
          height_consumption: '_LARGURA_ + 3', automatic: true)
        FactoryGirl.create(:rule, line_rule: @line_rule, component: @component_3, control_method: Rule.control_methods[:height], 
          total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*2', width_consumption: '_ALTURA_*3', 
          height_consumption: '_LARGURA_ + 3', automatic: true)
      end

      it 'returns the width information as consumption' do
        service = described_class.new(@line.id, @component_1.id, 100, 600)

        expect(service.consumption).to eq(100)
      end

      it 'returns the width information as consumption' do
        service = described_class.new(@line.id, @component_2.id, 150, 500)

        expect(service.consumption).to eq(150)
      end

      it 'returns the width information as consumption' do
        service = described_class.new(@line.id, @component_3.id, 200, 300)

        expect(service.consumption).to eq(200)
      end
    end
  end

  describe 'component cost' do
    before(:each) do
      @line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], 
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*2', width_consumption: '_ALTURA_*3', 
        height_consumption: '_LARGURA_ + 3', automatic: true)
      FactoryGirl.create(:rule, line_rule: @line_rule, component: @component_1, control_method: Rule.control_methods[:width], 
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*2', width_consumption: '_ALTURA_*3', 
        height_consumption: '_LARGURA_ + 3', automatic: true)
      FactoryGirl.create(:rule, line_rule: @line_rule, component: @component_2, control_method: Rule.control_methods[:width], 
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*2', width_consumption: '_ALTURA_*3', 
        height_consumption: '_LARGURA_ + 3', automatic: true)
      FactoryGirl.create(:rule, line_rule: @line_rule, component: @component_3, control_method: Rule.control_methods[:width], 
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*2', width_consumption: '_ALTURA_*3', 
        height_consumption: '_LARGURA_ + 3', automatic: true)
    end

    it 'should multiply the consumption by the component cost ignoring the rule cost' do
      service = described_class.new(@line.id, @component_1.id, 100, 600)

      expect(service.cost).to eq(BigDecimal.new('6000'))
    end

    it 'should multiply the consumption by the component cost ignoring the rule cost' do
      service = described_class.new(@line.id, @component_2.id, 150, 500)

      expect(service.cost).to eq(BigDecimal.new('6000'))
    end

    it 'should multiply the consumption by the component cost ignoring the rule cost' do
      service = described_class.new(@line.id, @component_3.id, 200, 300)

      expect(service.cost).to eq(BigDecimal.new('4500'))
    end
  end
end