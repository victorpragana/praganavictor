# == Schema Information
#
# Table name: categories
#
#  id            :integer          not null, primary key
#  name          :string(255)      not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  use_cut_table :boolean          default(FALSE)
#

require 'rails_helper'

RSpec.describe CategoriesController, type: :controller do
  before do
    sign_in :administrator, create(:administrator)
  end
  
  describe "GET index" do
    render_views

    before do
      @category_1 = create(:category, name: "aa")
      @category_2 = create(:category, name: "bb", use_cut_table: true)
      @category_3 = create(:category, name: "cc")
    end

    it "returns OK" do
      xhr :get, :index

      expect(response).to have_http_status(:ok)
    end

    it "lists the categories" do
      xhr :get, :index

      expect(json["categories"].size).to eq(3)

      expect(json["categories"][0]["id"]).to eq(@category_1.id)
      expect(json["categories"][0]["name"]).to eq(@category_1.name)
      expect(json["categories"][0]["use_cut_table"]).to eq(@category_1.use_cut_table)

      expect(json["categories"][1]["id"]).to eq(@category_2.id)
      expect(json["categories"][1]["name"]).to eq(@category_2.name)
      expect(json["categories"][1]["use_cut_table"]).to eq(@category_2.use_cut_table)

      expect(json["categories"][2]["id"]).to eq(@category_3.id)
      expect(json["categories"][2]["name"]).to eq(@category_3.name)
      expect(json["categories"][2]["use_cut_table"]).to eq(@category_3.use_cut_table)
    end
  end

  describe "POST create" do
    render_views

    context "with invalid params" do
      let(:parameters) {{"name" => ""}}

      it "does not create a new Category" do
        expect {
          xhr :post, :create, parameters
        }.to change(Category, :count).by(0)
      end

      it "returns unprocessable entity status" do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "returns the errors" do
        xhr :post, :create, parameters
        
        expect(json["errors"]).to_not be_empty
      end
    end

    context "with valid params" do
      let(:parameters) {{"name" => "Category", 'use_cut_table' => 'true'}}

      it "should initialize the category service" do
        expect(CategoryService).to receive(:new).with(parameters).and_call_original

        xhr :post, :create, parameters
      end

      it "should create the category" do
        expect_any_instance_of(CategoryService).to receive(:create).and_call_original
        
        xhr :post, :create, parameters
      end

      it "returns created status" do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:created)
      end
    end
  end

  describe "GET show" do
    render_views

    before do
      @category = create(:category, name: "bb")
    end

    it "should ok status" do
      xhr :get, :show, id: @category.id

      expect(response).to have_http_status(:ok)
    end

    it "returns the category data" do
      xhr :get, :show, id: @category.id

      expect(json["category"]["id"]).to eq(@category.id)
      expect(json["category"]["name"]).to eq(@category.name)
      expect(json["category"]["use_cut_table"]).to eq(@category.use_cut_table)
    end
  end

  describe "PUT update" do
    render_views

    before do
      @category = create(:category, name: "bb")
    end

    context "with invalid params" do
      let(:parameters) {{"name" => ""}.merge({"id" => "#{@category.id}"})}

      it "does not create a new Category" do
        expect {
          xhr :put, :update, parameters
        }.to change(Category, :count).by(0)
      end

      it "returns unprocessable entity status" do
        xhr :put, :update, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "returns the errors" do
        xhr :put, :update, parameters
        
        expect(json["errors"]).to_not be_empty
      end
    end

    context "with valid params" do
      let(:parameters) {{"name" => "bb", 'use_cut_table' => 'true'}}

      it "should initialize the category service" do
        expect(CategoryService).to receive(:new).with(parameters).and_call_original

        xhr :put, :update, parameters.merge({"id" => "#{@category.id}"})
      end

      it "should update the category" do
        expect_any_instance_of(CategoryService).to receive(:update).with(@category.id.to_s).and_call_original
        
        xhr :put, :update, parameters.merge({"id" => "#{@category.id}"})
      end

      it "returns ok status" do
        xhr :put, :update, parameters.merge({"id" => "#{@category.id}"})
        
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe "DELETE destroy" do
    before do
      @category = create(:category, name: "bb")
    end

    context "with success" do
      it "should destroy the category" do
        expect {
          xhr :delete, :destroy, {id: @category.id}
        }.to change(Category, :count).by(-1)
      end

      it "returns no content status" do
        xhr :delete, :destroy, {id: @category.id}

        expect(response).to have_http_status(:no_content)
      end
    end

    context "with error" do
      before do
        create(:component, unit: :cm, category: @category, erasable: true)
      end

      it "does not destroy the category" do
        expect {
          xhr :delete, :destroy, {id: @category.id}
        }.to_not change(Category, :count)
      end

      it "returns the bad request status" do
        xhr :delete, :destroy, {id: @category.id}

        expect(response).to have_http_status(:bad_request)
      end

      it "returns the errors" do
        xhr :delete, :destroy, {id: @category.id}
        
        expect(json["errors"]).to_not be_empty
      end
    end
  end
end
