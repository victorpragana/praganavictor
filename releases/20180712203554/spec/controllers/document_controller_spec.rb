require 'rails_helper'

RSpec.describe DocumentController, type: :controller do
  before(:each) do
    @document       = FactoryGirl.create(:document, name: 'Documento', text: 'Foo Bar')
    @factory = FactoryGirl.create(:factory, name: 'F1')
    FactoryGirl.create(:administrator, username: 'F1').tap do |administrator|
      administrator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @factory, user: administrator))
    end
    sign_in :administrator, @factory.users.first
  end

  describe 'GET index' do
    render_views

    it 'returns ok status' do
      xhr :get, :index

      expect(response).to have_http_status(:ok)

    end
    it 'should get the document' do
      xhr :get, :index

      expect(assigns(:document)).to eq(Document.first)
    end
  end

  describe 'PUT update' do
    render_views

    context 'with valid params' do
      let(:parameters) { {'text' => 'Foo Bar' } }

      it 'updates the document' do
        expect_any_instance_of(DocumentService).to receive(:update).and_call_original
        
        xhr :put, :update
      end

      it 'returns ok status' do
        xhr :put, :update
        
        expect(response).to have_http_status(:ok)
      end
    end
  end
end
