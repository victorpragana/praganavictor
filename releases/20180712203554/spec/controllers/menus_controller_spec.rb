require 'rails_helper'

RSpec.describe MenusController, type: :controller do
  describe 'GET index' do
    render_views

    before(:each) do
      @factory = FactoryGirl.create(:factory, name: 'F1')
      @user = [
        FactoryGirl.create(:administrator, username: 'admin').tap { |administrator| administrator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @factory, user: administrator)) },
        FactoryGirl.create(:manager, username: 'manager').tap { |manager| manager.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @factory, user: manager)) },
        FactoryGirl.create(:seller, username: 'seller').tap { |seller| seller.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @factory, user: seller)) },
        FactoryGirl.create(:operator, username: 'operator').tap { |operator| operator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @factory, user: operator)) }
      ].sample
    end

    def sign_user(user)
      sign_in user.class.to_s.underscore.to_sym, user
    end

    it 'returns ok status' do
      sign_user(@user)

      xhr :get, :index

      expect(response).to have_http_status(:ok)
    end

    it 'initializes the menu service' do
      sign_user(@user)

      expect(MenuService).to receive(:new).and_call_original

      xhr :get, :index
    end

    it 'should load the menu for user type' do
      sign_user(@user)

      expect_any_instance_of(MenuService).to receive(:load_for).with(@user.type, @user.workplace.class.to_s)

      xhr :get, :index
    end
  end
end
