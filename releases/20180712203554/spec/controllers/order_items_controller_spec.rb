# == Schema Information
#
# Table name: order_items
#
#  id                :integer          not null, primary key
#  line_id           :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  width             :integer          not null
#  height            :integer          not null
#  order_id          :integer
#  option_id         :integer
#  factory_total     :decimal(10, 2)   default(0.0)
#  distributor_total :decimal(10, 2)   default(0.0)
#  store_total       :decimal(10, 2)   default(0.0)
#  customer_total    :decimal(10, 2)   default(0.0)
#  discount          :decimal(5, 2)    default(0.0)
#

require 'rails_helper'

RSpec.describe OrderItemsController, type: :controller do
  before(:each) do
    @family      = create(:family)
    @line        = create(:line, family: @family, sale_price: BigDecimal.new('15.56'))
    @distributor = create(:distributor, name: 'D1')
    @store       = create(:store, distributor: @distributor, name: 'S1')
    @customer    = create(:customer, store_id: @store.id, name: 'C1')
    @user        = create(:administrator, username: 'F1').tap do |administrator|
      administrator.update!(selected_workplace: create(:user_workplace, workplace: @store, user: administrator))
    end
    @order       = create(:order, seller: @store, client: @customer, billing_client: @customer, user: @user)

    sign_in :administrator, @user
  end

 
  describe 'PUT apply_total' do
    render_views

    before(:each) do
     @order_item  = create(:order_item, line: @line, order: @order)
    end

    context 'with invalid data' do
      context 'for unallowed values' do
        let(:parameters) { {'total' => '-1'}.merge('id' => "#{@order_item.id}") }

        it 'does not update the order_item\'s options totals' do
          expect {
            xhr :put, :apply_total, parameters
            @order_item.reload
          }.to_not change(@order_item, :total)
        end

        it 'does not update the order_item\'s options totals' do
          expect {
            xhr :put, :apply_total, parameters
            @order_item.reload
          }.to_not change(@order_item, :total)
        end

        it 'returns unprocessable entity status' do
          xhr :put, :apply_total, parameters
          
          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'returns the errors' do
          xhr :put, :apply_total, parameters
          
          expect(json['errors']).to_not be_empty
        end
      end
    end

    context 'with valid params' do
      let(:parameters) { {'total' => '10.58'} }


      it 'initializes the order_item service' do
        expect(OrderItemService).to receive(:new).with(parameters).and_call_original

        xhr :put, :apply_total, parameters.merge('id' => "#{@order_item.id}")
      end

      it 'should apply the total to the order_item' do
        expect_any_instance_of(OrderItemService).to receive(:apply_total).with(@order_item.id.to_s).and_call_original
        
        xhr :put, :apply_total, parameters.merge('id' => "#{@order_item.id}")
      end

      it 'returns ok status' do
        xhr :put, :apply_total, parameters.merge('id' => "#{@order_item.id}")
        
        expect(response).to have_http_status(:ok)
      end
    end
  end
end
