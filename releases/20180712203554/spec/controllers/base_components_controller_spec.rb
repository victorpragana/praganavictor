# == Schema Information
#
# Table name: base_components
#
#  id                        :integer          not null, primary key
#  name                      :string(255)      not null
#  cost                      :decimal(10, 5)   default(0.0), not null
#  category_id               :integer
#  characteristics           :text
#  code                      :string(20)
#  unit                      :integer          default(2), not null
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  deleted_at                :datetime
#  reference_cost            :decimal(10, 5)   default(0.0)
#  offset_left               :integer          default(0)
#  offset_top                :integer          default(0)
#  offset_right              :integer          default(0)
#  offset_bottom             :integer          default(0)
#  allow_rotation            :boolean          default(FALSE)
#  exchange                  :decimal(10, 5)
#  loss                      :decimal(10, 5)
#  cut_table_id              :integer
#  print_on_lines_label      :boolean          default(FALSE)
#  fix_cost                  :decimal(10, 5)   default(0.0)
#  print_on_components_label :boolean          default(FALSE)
#

require 'rails_helper'

RSpec.describe BaseComponentsController, type: :controller do
  before do
    sign_in :administrator, create(:administrator)
    
    @family = create(:family)
    @category_1 = create(:category, name: 'CA1')
    @category_2 = create(:category, name: 'CA2')
    @category_3 = create(:category, name: 'CA3')
    @cut_table  = create(:cut_table)
  end

  describe 'GET index' do
    render_views

    before do
      @base_component_1 = create(:base_component, name: 'C1', category: @category_1, families: [@family])
      @base_component_2 = create(:base_component, name: 'C2', category: @category_2)
      @base_component_3 = create(:base_component, name: 'C3', category: @category_3)
    end

    it 'returns OK' do
      xhr :get, :index

      expect(response).to have_http_status(:ok)
    end
  end

  describe 'POST create' do
    render_views

    context 'with invalid params' do
      let(:parameters) {{'name' => ''}}

      it 'creates a new BaseComponent' do
        expect {
          xhr :post, :create, parameters
        }.to change(BaseComponent, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :create, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:offset_left)             {5}
      let(:offset_top)              {6}
      let(:offset_right)            {20}
      let(:offset_bottom)           {11}
      let(:converted_offset_left)   {offset_left  * 10}
      let(:converted_offset_top)    {offset_top  * 10}
      let(:converted_offset_right)  {offset_right * 10}
      let(:converted_offset_bottom) {offset_bottom * 10}

      let(:parameters) {{'name' => 'Manual Mola', 'unit'=>"#{BaseComponent.units[:un]}", 'category_id' => "#{@category_1.id}", 'cost' => '2.54', 
        'offset_left' => "#{offset_left}", 'offset_right' => "#{offset_right}", 'offset_top' => "#{offset_top}", 'offset_bottom' => "#{offset_bottom}",
        'allow_rotation' => 'true', 'exchange' => '2.59', 'create_children' => false, 'print_on_components_label' => 'true', 'print_on_lines_label' => 'true', 
        'reference_cost' => '1.54', 'fix_cost' => '22.5', 'code' => 'ABC123', 'characteristics' => 'Big text', 'family_ids' => ["#{@family.id}"],
        'loss' => '2.19', 'cut_table_id' => "#{@cut_table.id}"
      }}

      before do
        @category_1.update!(use_cut_table: true)
      end

      it 'initializes the base component service' do
        expect(BaseComponentService).to receive(:new).with(
          parameters.dup.tap do |p|
            p['offset_left']   = converted_offset_left
            p['offset_top']    = converted_offset_top
            p['offset_right']  = converted_offset_right
            p['offset_bottom'] = converted_offset_bottom
          end
        ).and_call_original

        xhr :post, :create, parameters
      end

      it 'creates the BaseComponent' do
        expect_any_instance_of(BaseComponentService).to receive(:create).and_call_original
        
        xhr :post, :create, parameters
      end

      it 'returns created status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:created)
      end

      it 'returns the base component data' do
        xhr :post, :create, parameters

        base_component = BaseComponent.last

        expect(json['base_component']['id']).to eq(base_component.id)
        expect(json['base_component']['name']).to eq(base_component.name)
        expect(json['base_component']['cost']).to eq(base_component.cost)
        expect(json['base_component']['reference_cost']).to eq(base_component.reference_cost)
        expect(json['base_component']['exchange']).to eq(base_component.exchange)
        expect(json['base_component']['loss']).to eq(base_component.loss)
        expect(json['base_component']['code']).to eq(base_component.code)
        expect(json['base_component']['unit']).to eq(Component.units[base_component.unit])
        expect(json['base_component']['characteristics']).to eq(base_component.characteristics)
        expect(json['base_component']['fix_cost']).to eq(base_component.fix_cost)
        expect(json['base_component']['category_id']).to eq(@category_1.id)
        expect(json['base_component']['family_ids']).to eq([base_component.families.first.id])
        expect(json['base_component']['offset_left']).to eq(base_component.offset_left_from_mm_to_cm)
        expect(json['base_component']['offset_top']).to eq(base_component.offset_top_from_mm_to_cm)
        expect(json['base_component']['offset_right']).to eq(base_component.offset_right_from_mm_to_cm)
        expect(json['base_component']['offset_bottom']).to eq(base_component.offset_bottom_from_mm_to_cm)
        expect(json['base_component']['allow_rotation']).to eq(base_component.allow_rotation?)
        expect(json['base_component']['cut_table_id']).to eq(base_component.cut_table_id)
        expect(json['base_component']['print_on_components_label']).to eq(base_component.print_on_components_label?)
        expect(json['base_component']['print_on_lines_label']).to eq(base_component.print_on_lines_label?)
      end
    end
  end

  describe 'PUT update' do
    render_views

    before do
      @base_component = create(:base_component, name: 'C1', category: @category_1, families: [@family])
    end

    context 'with valid params' do
      let(:offset_left)             {5}
      let(:offset_top)              {6}
      let(:offset_right)            {20}
      let(:offset_bottom)           {11}
      let(:converted_offset_left)   {offset_left  * 10}
      let(:converted_offset_top)    {offset_top  * 10}
      let(:converted_offset_right)  {offset_right * 10}
      let(:converted_offset_bottom) {offset_bottom * 10}

      let(:parameters) {{'category_id' => "#{@category_1.id}", 'unit'=>"#{BaseComponent.units[:un]}", 'cost' => '2.54',
        'offset_left' => "#{offset_left}", 'offset_right' => "#{offset_right}", 'offset_top' => "#{offset_top}", 'offset_bottom' => "#{offset_bottom}",
        'allow_rotation' => 'true', 'exchange' => '2.59', 'create_children' => true, 'print_on_components_label' => 'true', 'print_on_lines_label' => 'true',
        'reference_cost' => '1.54', 'fix_cost' => '22.5', 'characteristics' => 'Big text', 'family_ids' => ["#{@family.id}"],
        'loss' => '2.19', 'cut_table_id' => "#{@cut_table.id}", "edit" => {
          'category_id' => true, 'unit' => true, 'cut_table_id' => true, 'allow_rotation' => true, 'cost' => true, 'reference_cost' => true,
          'exchange' => true, 'loss' => true, 'fix_cost' => true, 'family_ids' => true, 'characteristics' => true, 'offset_top' => true, 'offset_left' => true,
          'offset_right' => true, 'offset_bottom' => true, 'print_on_components_label' => true, 'print_on_lines_label' => true
        }
      }}

      before do
        @category_1.update!(use_cut_table: true)
      end

      it 'initializes the base component service' do
        expect(BaseComponentService).to receive(:new).with(
          parameters.dup.tap do |p|
            p['offset_left']   = converted_offset_left
            p['offset_top']    = converted_offset_top
            p['offset_right']  = converted_offset_right
            p['offset_bottom'] = converted_offset_bottom
          end
        ).and_call_original

        xhr :put, :update, parameters.merge({'id' => "#{@base_component.id}"})
      end

      it 'updates the base component' do
        expect_any_instance_of(BaseComponentService).to receive(:update).with(@base_component.id.to_s).and_call_original
        
        xhr :put, :update, parameters.merge({'id' => "#{@base_component.id}"})
      end

      it 'returns ok status' do
        xhr :put, :update, parameters.merge({'id' => "#{@base_component.id}"})
        
        expect(response).to have_http_status(:ok)
      end

      it 'returns the base component data' do
        xhr :put, :update, parameters.merge({'id' => "#{@base_component.id}"})

        base_component = BaseComponent.last

        expect(json['base_component']['id']).to eq(base_component.id)
        expect(json['base_component']['cost']).to eq(base_component.cost)
        expect(json['base_component']['reference_cost']).to eq(base_component.reference_cost)
        expect(json['base_component']['exchange']).to eq(base_component.exchange)
        expect(json['base_component']['loss']).to eq(base_component.loss)
        expect(json['base_component']['unit']).to eq(Component.units[base_component.unit])
        expect(json['base_component']['characteristics']).to eq(base_component.characteristics)
        expect(json['base_component']['fix_cost']).to eq(base_component.fix_cost)
        expect(json['base_component']['category_id']).to eq(@category_1.id)
        expect(json['base_component']['family_ids']).to eq([base_component.families.first.id])
        expect(json['base_component']['offset_left']).to eq(base_component.offset_left_from_mm_to_cm)
        expect(json['base_component']['offset_top']).to eq(base_component.offset_top_from_mm_to_cm)
        expect(json['base_component']['offset_right']).to eq(base_component.offset_right_from_mm_to_cm)
        expect(json['base_component']['offset_bottom']).to eq(base_component.offset_bottom_from_mm_to_cm)
        expect(json['base_component']['allow_rotation']).to eq(base_component.allow_rotation?)
        expect(json['base_component']['cut_table_id']).to eq(base_component.cut_table_id)
        expect(json['base_component']['print_on_components_label']).to eq(base_component.print_on_components_label?)
        expect(json['base_component']['print_on_lines_label']).to eq(base_component.print_on_lines_label?)
      end
    end
  end

  describe 'GET show' do
    render_views

    before do
      @category_1.update!(use_cut_table: true)
      @base_component = create(:base_component, name: 'C1', category: @category_1, cut_table: @cut_table, families: [@family])
    end

    it 'should ok status' do
      xhr :get, :show, id: @base_component.id

      expect(response).to have_http_status(:ok)
    end

    it 'returns the component data' do
      xhr :get, :show, id: @base_component.id

      expect(json['base_component']['id']).to eq(@base_component.id)
      expect(json['base_component']['name']).to eq(@base_component.name)
      expect(json['base_component']['cost']).to eq(@base_component.cost)
      expect(json['base_component']['exchange']).to eq(@base_component.exchange)
      expect(json['base_component']['loss']).to eq(@base_component.loss)
      expect(json['base_component']['reference_cost']).to eq(@base_component.reference_cost)
      expect(json['base_component']['code']).to eq(@base_component.code)
      expect(json['base_component']['unit']).to eq(Component.units[@base_component.unit])
      expect(json['base_component']['characteristics']).to eq(@base_component.characteristics)
      expect(json['base_component']['category_id']).to eq(@category_1.id)
      expect(json['base_component']['family_ids']).to eq([@base_component.families.first.id])
      expect(json['base_component']['offset_left']).to eq(@base_component.offset_left_from_mm_to_cm)
      expect(json['base_component']['offset_top']).to eq(@base_component.offset_top_from_mm_to_cm)
      expect(json['base_component']['offset_right']).to eq(@base_component.offset_right_from_mm_to_cm)
      expect(json['base_component']['offset_bottom']).to eq(@base_component.offset_bottom_from_mm_to_cm)
      expect(json['base_component']['allow_rotation']).to eq(@base_component.allow_rotation?)
      expect(json['base_component']['cut_table_id']).to eq(@base_component.cut_table_id)
      expect(json['base_component']['deleted']).to eq(@base_component.deleted?)
      expect(json['base_component']['print_on_components_label']).to eq(@base_component.print_on_components_label?)
      expect(json['base_component']['print_on_lines_label']).to eq(@base_component.print_on_lines_label?)
    end
  end

  describe 'DELETE destroy' do
    before do
      @base_component = create(:base_component, name: 'C2', category: @category_2)
    end

    context 'with success' do
      it 'deactivates the base component' do
        xhr :delete, :destroy, { id: @base_component.id }

        expect(@base_component.reload.deleted?).to be_truthy
      end

      it 'does not remove the base component' do
        expect {
          xhr :delete, :destroy, { id: @base_component.id }
        }.to_not change(BaseComponent.with_deleted, :count)
      end

      it 'returns no content status' do
        xhr :delete, :destroy, { id: @base_component.id }

        expect(response).to have_http_status(:no_content)
      end
    end

    context 'with error' do
      let(:base_component) {mock_model(BaseComponent, destroy: false, errors: double(:errors, full_messages: ['foo']))}

      before do
        allow(BaseComponent).to receive_message_chain(:with_deleted, :includes, :find).and_return(base_component)
      end

      it 'does not destroy the base component' do
        expect {
          xhr :delete, :destroy, { id: base_component.id }
        }.to_not change(BaseComponent, :count)
      end

      it 'returns the bad request status' do
        xhr :delete, :destroy, { id: base_component.id }

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the errors' do
        xhr :delete, :destroy, { id: base_component.id }
        
        expect(json['errors']).to_not be_empty
      end
    end
  end

  describe 'DELETE really_destroy' do
    before do
      @base_component = create(:base_component, name: 'C2', category: @category_2)
    end

    context 'with success' do
      it 'removes the base component' do
        expect {
          xhr :delete, :really_destroy, { id: @base_component.id }
        }.to change(BaseComponent.with_deleted, :count).by(-1)
      end

      it 'returns no content status' do
        xhr :delete, :really_destroy, { id: @base_component.id }

        expect(response).to have_http_status(:no_content)
      end
    end

    context 'with error' do
      let(:base_component) {mock_model(BaseComponent, really_destroy!: false, errors: double(:errors, full_messages: ['foo']))}

      before do
        allow(BaseComponent).to receive_message_chain(:with_deleted, :includes, :find).and_return(base_component)
      end

      it 'does not destroy the base component' do
        expect {
          xhr :delete, :really_destroy, { id: @base_component.id }
        }.to_not change(BaseComponent, :count)
      end

      it 'returns the bad request status' do
        xhr :delete, :really_destroy, { id: @base_component.id }

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the errors' do
        xhr :delete, :really_destroy, { id: @base_component.id }
        
        expect(json['errors']).to_not be_empty
      end
    end
  end

  describe 'PATCH activate' do
    before do
      @base_component = create(:base_component, name: 'C2', category: @category_2, deleted_at: Time.zone.now)
    end

    context 'with invalid params' do
      let(:service) { double(:service, activate: false, success: false, errors: ['foo']) }

      before do
        allow(BaseComponentService).to receive(:new).and_return(service)
      end

      it 'does not change the base component status' do
        expect {
          xhr :patch, :activate, {'id' => "#{@base_component.id}", "include_children" => true}
        }.to_not change(@base_component, :deleted?)
      end

      it 'returns unprocessable entity status' do
        xhr :patch, :activate, {'id' => "#{@base_component.id}", "include_children" => true}
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :patch, :activate, {'id' => "#{@base_component.id}", "include_children" => true}
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      it 'initializes the base component service' do
        expect(BaseComponentService).to receive(:new).and_call_original

        xhr :patch, :activate, {'id' => "#{@base_component.id}", "include_children" => true}
      end

      it 'activates the base component' do
        expect_any_instance_of(BaseComponentService).to receive(:activate).with(@base_component.id.to_s, true).and_call_original
        
        xhr :patch, :activate, {'id' => "#{@base_component.id}", "include_children" => true}
      end

      it 'returns ok status' do
        xhr :patch, :activate, {'id' => "#{@base_component.id}", "include_children" => true}
        
        expect(response).to have_http_status(:ok)
      end

      it 'renders nothing' do
        xhr :patch, :activate, {'id' => "#{@base_component.id}", "include_children" => true}

        expect(response.body).to be_blank
      end
    end
  end
end
