require 'rails_helper'

RSpec.describe ErpController, type: :controller do
  before(:each) do
    factory = FactoryGirl.create(:factory, name: 'F1')
    distributor = FactoryGirl.create(:distributor, name: 'D1')
    store = FactoryGirl.create(:store, name: 'S1', distributor: distributor)

    @workplace = [factory, distributor, store].sample

    @administrator = FactoryGirl.create(:administrator, name: 'admin', username: 'admin').tap do |administrator|
      administrator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @workplace, user: administrator))
    end
    
    sign_in :administrator, @administrator
  end

  describe 'GET sellers' do
    render_views

    let(:client)        { double(:service, sellers: sellers) }
    let(:sellers)       { double(:sellers, list: erp_response) }
    let(:erp_response)  { double(:erp_response, status: 200, payload: [
      { 'id' => 1, 'ativo' => true, 'nome' => 'RICARDO HAMADA' },
      { 'id' => 2, 'ativo' => false, 'nome' => 'Stanley' },
      { 'id' => 3, 'ativo' => true, 'nome' => 'Stanley' },
      { 'id' => 4, 'ativo' => true, 'nome' => 'STANLEY WANG' },
      { 'id' => 5, 'ativo' => true, 'nome' => 'ELAINE FERREIRA COSTA' }]
    )}

    before(:each) do
      allow(Varejonline).to receive(:new).and_return(client)
    end

    it 'loads the sellers from ERP' do
      expect(sellers).to receive(:list)

      xhr :get, :sellers
    end

    it 'returns ok status' do
      xhr :get, :sellers

      expect(response).to have_http_status(:ok)
    end

    it 'returns the sellers data' do
      xhr :get, :sellers

      expect(json['sellers'][0]['id']).to eq(erp_response.payload[4]['id'])
      expect(json['sellers'][0]['name']).to eq(erp_response.payload[4]['nome'])
      expect(json['sellers'][1]['id']).to eq(erp_response.payload[0]['id'])
      expect(json['sellers'][1]['name']).to eq(erp_response.payload[0]['nome'])
      expect(json['sellers'][2]['id']).to eq(erp_response.payload[3]['id'])
      expect(json['sellers'][2]['name']).to eq(erp_response.payload[3]['nome'])
      expect(json['sellers'][3]['id']).to eq(erp_response.payload[2]['id'])
      expect(json['sellers'][3]['name']).to eq(erp_response.payload[2]['nome'])
    end
  end

  describe 'GET category_levels' do
    render_views

    let(:client)          { double(:service, category_levels: category_levels) }
    let(:category_levels) { double(:category_levels, list: erp_response) }
    let(:erp_response)    { double(:erp_response, status: 200, payload: [
      {'id' => 1,'nome' => 'LINHA DE PRODUTO','compoeReferenciaBase' => true,'nivel' => 1,'ativo' => true,'dataAlteracao' => '04-11-2015 10:16:02'},
      {'id' => 2,'nome' => 'GRUPO','idNivelCategoriaPai' => 1,'compoeReferenciaBase' => true,'nivel' => 2,'ativo' => true,'dataAlteracao' => '04-11-2015 10:16:45'},
      {'id' => 3,'nome' => 'SUBGRUPO','idNivelCategoriaPai' => 2,'compoeReferenciaBase' => true,'nivel' => 3,'ativo' => true,'dataAlteracao' => '04-11-2015 10:17:26'},
      {'id' => 21,'nome' => 'TRIBUTAÇÃO','idNivelCategoriaPai' => 3,'compoeReferenciaBase' => false,'nivel' => 4,'ativo' => true,'dataAlteracao' => '19-10-2016 16:06:33'},
      {'id' => 99,'nome' => 'INATIVO','idNivelCategoriaPai' => 21,'compoeReferenciaBase' => false,'nivel' => 5,'ativo' => false,'dataAlteracao' => '19-10-2016 16:06:33'}]
    )}

    before(:each) do
      allow(Varejonline).to receive(:new).and_return(client)
    end

    it 'loads the category levels from ERP' do
      expect(category_levels).to receive(:list)

      xhr :get, :category_levels
    end

    it 'returns ok status' do
      xhr :get, :category_levels

      expect(response).to have_http_status(:ok)
    end

    it 'returns the active category levels data ordered by level' do
      xhr :get, :category_levels

      expect(json['category_levels'].count).to eq(4)

      expect(json['category_levels'][0]['id']).to eq(erp_response.payload[0]['id'])
      expect(json['category_levels'][0]['name']).to eq(erp_response.payload[0]['nome'])
      expect(json['category_levels'][0]['level']).to eq(erp_response.payload[0]['nivel'])
      expect(json['category_levels'][1]['id']).to eq(erp_response.payload[1]['id'])
      expect(json['category_levels'][1]['name']).to eq(erp_response.payload[1]['nome'])
      expect(json['category_levels'][1]['level']).to eq(erp_response.payload[1]['nivel'])
      expect(json['category_levels'][2]['id']).to eq(erp_response.payload[2]['id'])
      expect(json['category_levels'][2]['name']).to eq(erp_response.payload[2]['nome'])
      expect(json['category_levels'][2]['level']).to eq(erp_response.payload[2]['nivel'])
      expect(json['category_levels'][3]['id']).to eq(erp_response.payload[3]['id'])
      expect(json['category_levels'][3]['name']).to eq(erp_response.payload[3]['nome'])
      expect(json['category_levels'][3]['level']).to eq(erp_response.payload[3]['nivel'])
    end
  end

  describe 'GET product_categories' do
    render_views

    let(:client)             { double(:service, product_categories: product_categories) }
    let(:product_categories) { double(:product_categories, list: erp_response) }
    let(:erp_response)       { double(:erp_response, status: 200, payload: [
      {'id' => 1,'nome' => 'PRODUTO ACABADO','unidade' => 'PC','nivel' => 1,'ativo' => true,'origem' => 0,'dataAlteracao' => '19-10-2016 15:12:24','classificacao' => 'REVENDA','metodoControle' => 'ESTOCAVEL'},
      {'id' => 2,'nome' => 'TAPETE','unidade' => 'PC','nivel' => 1,'ativo' => false,'origem' => 0,'dataAlteracao' => '19-10-2016 15:12:29','classificacao' => 'REVENDA','metodoControle' => 'ESTOCAVEL'},
      {'id' => 3,'nome' => 'PAPEL DE PAREDE','unidade' => 'PC','nivel' => 1,'ativo' => false,'origem' => 0,'dataAlteracao' => '19-10-2016 15:09:12','classificacao' => 'REVENDA','metodoControle' => 'ESTOCAVEL'},
      {'id' => 4,'nome' => 'ROLO','unidade' => 'PC','nivel' => 2,'ativo' => true,'origem' => 0,'dataAlteracao' => '04-11-2015 10:25:00','classificacao' => 'REVENDA','metodoControle' => 'ESTOCAVEL'},
      {'id' => 5,'nome' => 'ROMANA','unidade' => 'PC','nivel' => 2,'ativo' => true,'origem' => 0,'dataAlteracao' => '04-11-2015 10:25:26','classificacao' => 'REVENDA','metodoControle' => 'ESTOCAVEL'},
      {'id' => 6,'nome' => 'FACTORY','unidade' => 'PC','nivel' => 2,'ativo' => false,'origem' => 0,'dataAlteracao' => '19-10-2016 15:01:23','classificacao' => 'REVENDA','metodoControle' => 'ESTOCAVEL'}]
    )}

    before(:each) do
      allow(Varejonline).to receive(:new).and_return(client)
    end

    it 'loads the product categories from ERP' do
      expect(product_categories).to receive(:list)

      xhr :get, :product_categories
    end

    it 'returns ok status' do
      xhr :get, :product_categories

      expect(response).to have_http_status(:ok)
    end

    it 'returns the active product categories data ordered by level' do
      xhr :get, :product_categories

      expect(json['product_categories'].count).to eq(3)

      expect(json['product_categories'][0]['id']).to eq(erp_response.payload[0]['id'])
      expect(json['product_categories'][0]['name']).to eq(erp_response.payload[0]['nome'])
      expect(json['product_categories'][0]['level']).to eq(erp_response.payload[0]['nivel'])
      expect(json['product_categories'][1]['id']).to eq(erp_response.payload[3]['id'])
      expect(json['product_categories'][1]['name']).to eq(erp_response.payload[3]['nome'])
      expect(json['product_categories'][1]['level']).to eq(erp_response.payload[3]['nivel'])
      expect(json['product_categories'][2]['id']).to eq(erp_response.payload[4]['id'])
      expect(json['product_categories'][2]['name']).to eq(erp_response.payload[4]['nome'])
      expect(json['product_categories'][2]['level']).to eq(erp_response.payload[4]['nivel'])
    end
  end

  describe 'GET payment_conditions' do
    render_views

    let(:client)             { double(:service, payment_conditions: payment_conditions) }
    let(:payment_conditions) { double(:payment_conditions, list: erp_response) }
    let(:erp_response)       { double(:erp_response, status: 200, payload: [
      {"id" => 1,"descricao" => "A VISTA","juros" => 0,"acrescimo" => 0,"ativo" => true,"multa" => 0,"dataAlteracao" => "21-03-2012 08:55:28"},
      {"id" => 2,"descricao" => "AVULSO","juros" => 0,"acrescimo" => 0,"ativo" => true,"multa" => 0,"dataAlteracao" => "21-03-2012 08:55:28"},
      {"id" => 100,"descricao" => "CHEQUE 30 DIAS","juros" => 0,"acrescimo" => 0,"ativo" => true,"multa" => 0,"dataAlteracao" => "03-01-2017 11:38:53"},
      {"id" => 101,"descricao" => "CHEQUE 30/60","juros" => 0,"acrescimo" => 0,"ativo" => true,"multa" => 0,"dataAlteracao" => "04-11-2015 11:50:35"},
      {"id" => 102,"descricao" => "BOLETO 30DD","juros" => 0,"acrescimo" => 0,"ativo" => false,"multa" => 0,"dataAlteracao" => "03-01-2017 11:36:51"},
      {"id" => 103,"descricao" => "BOLETO 30/60DD","juros" => 0,"acrescimo" => 0,"ativo" => true,"multa" => 0,"dataAlteracao" => "03-01-2017 11:38:24"}]
    )}

    before(:each) do
      allow(Varejonline).to receive(:new).and_return(client)
    end

    it 'loads the payment plans from ERP' do
      expect(payment_conditions).to receive(:list)

      xhr :get, :payment_conditions
    end

    it 'returns ok status' do
      xhr :get, :payment_conditions

      expect(response).to have_http_status(:ok)
    end

    it 'returns the active payment plans data ordered by name' do
      xhr :get, :payment_conditions

      expect(json['payment_conditions'].count).to eq(5)

      expect(json['payment_conditions'][0]['id']).to eq(erp_response.payload[0]['id'])
      expect(json['payment_conditions'][0]['name']).to eq(erp_response.payload[0]['descricao'])
      expect(json['payment_conditions'][1]['id']).to eq(erp_response.payload[1]['id'])
      expect(json['payment_conditions'][1]['name']).to eq(erp_response.payload[1]['descricao'])
      expect(json['payment_conditions'][2]['id']).to eq(erp_response.payload[5]['id'])
      expect(json['payment_conditions'][2]['name']).to eq(erp_response.payload[5]['descricao'])
      expect(json['payment_conditions'][3]['id']).to eq(erp_response.payload[2]['id'])
      expect(json['payment_conditions'][3]['name']).to eq(erp_response.payload[2]['descricao'])
      expect(json['payment_conditions'][4]['id']).to eq(erp_response.payload[3]['id'])
      expect(json['payment_conditions'][4]['name']).to eq(erp_response.payload[3]['descricao'])
    end
  end
end
