# == Schema Information
#
# Table name: families
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  color      :string(7)        not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe FamiliesController, type: :controller do
  before do
    sign_in :administrator, create(:administrator)
  end
  
  describe "GET index" do
    render_views

    before do
      
      @family_1 = create(:family, :name => "aa")
      @family_2 = create(:family, :name => "bb")
      @family_3 = create(:family, :name => "cc")
    end

    it "should return OK" do
      xhr :get, :index

      expect(response).to have_http_status(:ok)
    end

    it "lists the families" do
      xhr :get, :index

      expect(json["families"].size).to eq(3)

      expect(json["families"][0]["id"]).to eq(@family_1.id)
      expect(json["families"][0]["name"]).to eq(@family_1.name)
      expect(json["families"][0]["color"]).to eq(@family_1.color)

      expect(json["families"][1]["id"]).to eq(@family_2.id)
      expect(json["families"][1]["name"]).to eq(@family_2.name)
      expect(json["families"][1]["color"]).to eq(@family_2.color)

      expect(json["families"][2]["id"]).to eq(@family_3.id)
      expect(json["families"][2]["name"]).to eq(@family_3.name)
      expect(json["families"][2]["color"]).to eq(@family_3.color)
    end
  end

  describe "POST create" do
    render_views

    context "with invalid params" do
      let(:parameters) {{"name" => ""}}

      it "does not create a new Family" do
        expect {
          xhr :post, :create, parameters
        }.to change(Family, :count).by(0)
      end

      it "should return unprocessable entity status" do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "should return the errors" do
        xhr :post, :create, parameters
        
        expect(json["errors"]).to_not be_empty
      end
    end

    context "with valid params" do
      let(:parameters) {{"name" => "Family", "color" => "#123222"}}

      it "should initialize the family service" do
        expect(FamilyService).to receive(:new).with(parameters).and_call_original

        xhr :post, :create, parameters
      end

      it "should create the family" do
        expect_any_instance_of(FamilyService).to receive(:create).and_call_original
        
        xhr :post, :create, parameters
      end

      it "should return created status" do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:created)
      end

      it "should return the families" do
        xhr :post, :create, parameters

        family = Family.last

        expect(json["family"]["id"]).to eq(family.id)
        expect(json["family"]["name"]).to eq(family.name)
        expect(json["family"]["color"]).to eq(family.color)
      end
    end
  end

  describe "PUT update" do
    render_views

    before do
      @family = create(:family)
    end

    context "with invalid params" do
      let(:parameters) {{"name" => ""}.merge({"id" => "#{@family.id}"})}

      it "does not create a new Family" do
        expect {
          xhr :put, :update, parameters
        }.to change(Family, :count).by(0)
      end

      it "should return unprocessable entity status" do
        xhr :put, :update, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "should return the errors" do
        xhr :put, :update, parameters
        
        expect(json["errors"]).to_not be_empty
      end
    end

    context "with valid params" do
      let(:parameters) {{"name" => "Rolô", "color" => "#212111"}}

      it "should initialize the family service" do
        expect(FamilyService).to receive(:new).with(parameters).and_call_original

        xhr :put, :update, parameters.merge({"id" => "#{@family.id}"})
      end

      it "should update the family" do
        expect_any_instance_of(FamilyService).to receive(:update).with(@family.id.to_s).and_call_original
        
        xhr :put, :update, parameters.merge({"id" => "#{@family.id}"})
      end

      it "should return ok status" do
        xhr :put, :update, parameters.merge({"id" => "#{@family.id}"})
        
        expect(response).to have_http_status(:ok)
      end

      it "should return the family data" do
        xhr :put, :update, parameters.merge({"id" => "#{@family.id}"})

        family = Family.last

        expect(json["family"]["id"]).to eq(family.id)
        expect(json["family"]["name"]).to eq(family.name)
        expect(json["family"]["color"]).to eq(family.color)
      end
    end
  end

  describe "GET show" do
    render_views

    before do
      @family = create(:family)
    end

    it "should ok status" do
      xhr :get, :show, :id => @family.id

      expect(response).to have_http_status(:ok)
    end

    it "should return the family data" do
      xhr :get, :show, :id => @family.id

      expect(json["family"]["id"]).to eq(@family.id)
      expect(json["family"]["name"]).to eq(@family.name)
      expect(json["family"]["color"]).to eq(@family.color)
    end
  end
end
