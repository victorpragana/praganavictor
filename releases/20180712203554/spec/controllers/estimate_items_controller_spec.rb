# == Schema Information
#
# Table name: estimate_items
#
#  id                :integer          not null, primary key
#  estimate_id       :integer
#  item_code         :string
#  item_description  :string           not null
#  unit              :integer          not null
#  quantity          :decimal(8, 2)    not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  type              :integer          default(0)
#  factory_value     :decimal(10, 2)   default(0.0)
#  distributor_value :decimal(10, 2)   default(0.0)
#  store_value       :decimal(10, 2)   default(0.0)
#  customer_value    :decimal(10, 2)   default(0.0)
#  discount          :decimal(5, 2)    default(0.0)
#  item_id           :integer
#

require 'rails_helper'

RSpec.describe EstimateItemsController, type: :controller do
  before(:each) do
    @factory = create(:factory, name: 'F1')
    create(:administrator, username: 'F1').tap do |administrator|
      administrator.update!(selected_workplace: create(:user_workplace, workplace: @factory, user: administrator))
    end

    sign_in :administrator, @factory.users.first

    @item_1 = create(:item)
    @item_2 = create(:item)
    @item_3 = create(:item)

    @distributor = create(:distributor, name: 'D1')
    @store       = create(:store, distributor: @distributor, name: 'S1')
    @customer    = create(:customer, store_id: @store.id, name: 'C1')
    @estimate    = create(:estimate, seller: @store, client: @customer)

    @price_table = create(:price_table, name: 'ab', owner: @store, type: :for_items)

    create(:price_table_item, item_id: @item_1.id, price: '58.22', price_table: @price_table, maximum_discount: 70)
    create(:price_table_item, item_id: @item_2.id, price: '65',    price_table: @price_table, maximum_discount: 50)
    create(:price_table_item, item_id: @item_3.id, price: '100',   price_table: @price_table, maximum_discount: 10)

    create(:price_table_client, price_table: @price_table, client: @customer)
  end

  describe 'POST create' do
    render_views

    context 'with invalid data' do
      let(:parameters) { {'estimate_id' => "#{@estimate.id}", 'item_id' => "#{@item_1.id}", 'quantity' => '' } }

      it 'does not create a new Estimate item' do
        expect {
          xhr :post, :create, parameters
        }.to_not change(EstimateItem, :count)
      end

      it 'returns unprocessable entity status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :create, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) { {'estimate_id' => "#{@estimate.id}", 'item_id' => "#{@item_1.id}", 'quantity' => '2.3' } }

      it 'initializes the estimate item service' do
        expect(EstimateItemService).to receive(:new).with(parameters.except('estimate_id')).and_call_original

        xhr :post, :create, parameters
      end

      it 'creates the estimate item' do
        expect_any_instance_of(EstimateItemService).to receive(:add_to).with(parameters['estimate_id']).and_call_original
        
        xhr :post, :create, parameters
      end

      it 'initializes the estimate service' do
        expect(EstimateService).to receive(:new).and_call_original

        xhr :post, :create, parameters
      end

      it 'updates the estimate' do
        expect_any_instance_of(EstimateService).to receive(:recalculate_totals).with(@estimate.id).and_call_original

        xhr :post, :create, parameters
      end

      it 'returns created status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:created)
      end
    end
  end

  describe 'DELETE destroy' do
    before(:each) do
      @estimate_item = create(:estimate_item, estimate: @estimate, item: @item_1)
    end

    context 'with success' do
      it 'destroys the estimate item' do
        expect {
          xhr :delete, :destroy, { id: @estimate_item.id }
        }.to change(EstimateItem, :count).by(-1)
      end

      it 'initializes the estimate service' do
        expect(EstimateService).to receive(:new).and_call_original

        xhr :delete, :destroy, { id: @estimate_item.id }
      end

      it 'updates the estimate' do
        expect_any_instance_of(EstimateService).to receive(:recalculate_totals).with(@estimate.id).and_call_original

        xhr :delete, :destroy, { id: @estimate_item.id }
      end

      it 'returns no content status' do
        xhr :delete, :destroy, { id: @estimate_item.id }

        expect(response).to have_http_status(:no_content)
      end
    end

    context 'with error' do
      let(:service) { double(:service, destroy: false, success: false, errors: ['foo']) }

      before(:each) do
        allow(EstimateItemService).to receive(:new).and_return(service)
      end

      it 'does not destroy the estimate item' do
        expect {
          xhr :delete, :destroy, { id: @estimate_item.id }
        }.to_not change(EstimateItem, :count)
      end

      it 'returns the bad request status' do
        xhr :delete, :destroy, { id: @estimate_item.id }

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the errors' do
        xhr :delete, :destroy, { id: @estimate_item.id }
        
        expect(json['errors']).to_not be_empty
      end
    end
  end

  describe 'PUT apply_discount' do
    render_views

    before(:each) do
      create(:administrator, username: 'A1', maximum_discount: BigDecimal.new('12.50')).tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: @store, user: administrator))
      end

      sign_in :administrator, @store.users.first
    end

    context 'with invalid data' do
      context 'for unallowed values' do
        let(:parameters) { {'discount' => '-1'}.merge('id' => "#{@estimate_item.id}") }

        before do
          @estimate_item = create(:estimate_item, estimate: @estimate, item: @item_3)
        end

        it 'does not update the estimate_item discount' do
          expect {
            xhr :put, :apply_discount, parameters
            @estimate_item.reload
          }.to_not change(@estimate_item, :discount)
        end

        it 'returns unprocessable entity status' do
          xhr :put, :apply_discount, parameters
          
          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'returns the errors' do
          xhr :put, :apply_discount, parameters
          
          expect(json['errors']).to_not be_empty
        end
      end

      context 'for discount greater than the allowed for the user' do
        let(:parameters) { {'discount' => '12.51'}.merge('id' => "#{@estimate_item.id}") }

        before do 
          @estimate_item = create(:estimate_item, estimate: @estimate, item: @item_1)
        end

        it 'does not update the estimate_item discount' do
          expect {
            xhr :put, :apply_discount, parameters
            @estimate_item.reload
          }.to_not change(@estimate_item, :discount)
        end

        it 'returns unprocessable entity status' do
          xhr :put, :apply_discount, parameters
          
          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'returns the errors' do
          xhr :put, :apply_discount, parameters
          
          expect(json['errors']).to_not be_empty
        end
      end
    end

    context 'with valid params' do
      let(:parameters) { {'discount' => '10.58'} }

      before do
        @estimate_item = create(:estimate_item, estimate: @estimate, item: @item_2)
      end

      it 'initializes the estimate item service' do
        expect(EstimateItemService).to receive(:new).with(parameters).and_call_original

        xhr :put, :apply_discount, parameters.merge('id' => "#{@estimate_item.id}")
      end

      it 'should apply the discount to the estimate_item' do
        expect_any_instance_of(EstimateItemService).to receive(:apply_discount).with(@estimate_item.id.to_s).and_call_original
        
        xhr :put, :apply_discount, parameters.merge('id' => "#{@estimate_item.id}")
      end

      it 'returns no content status' do
        xhr :put, :apply_discount, parameters.merge('id' => "#{@estimate_item.id}")
        
        expect(response).to have_http_status(:no_content)
      end

      it 'renders nothing' do
        xhr :put, :apply_discount, parameters.merge('id' => "#{@estimate_item.id}")

        expect(response.body).to be_blank
      end
    end
  end
end
