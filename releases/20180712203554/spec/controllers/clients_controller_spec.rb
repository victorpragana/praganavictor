require 'rails_helper'

RSpec.describe ClientsController, type: :controller do
  describe 'GET index' do
    render_views

    before do
      @factory = create(:factory, name: 'F1')
      create(:administrator, username: 'F1').tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: @factory, user: administrator))
      end

      @distributor = create(:distributor, name: 'D1')
      create(:administrator, username: 'D1').tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: @distributor, user: administrator))
      end

      @store_1 = create(:store, name: 'S1', distributor: @distributor)
      create(:administrator, username: 'S1').tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: @store_1, user: administrator))
      end

      @store_2 = create(:store, name: 'S2', distributor: @distributor)
      create(:administrator, username: 'S2').tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: @store_2, user: administrator))
      end

      @customer = create(:customer, name: 'C1', store_id: @store_1.id)
    end

    context 'with factory, distributor or store logged' do
      it 'returns the ok status' do
        @user = [@factory.users.first, @distributor.users.first, @store_1.users.first, @store_2.users.first].sample

        sign_in @user.class.to_s.underscore.to_sym, @user

        xhr :get, :index

        expect(response).to have_http_status(:ok)
      end

      context 'with logged factory' do
        before do
          sign_in :administrator, @factory.users.first
        end

        it 'returns the distributors, stores and customers' do
          xhr :get, :index

          expect(json['clients'].size).to eq(4)
          expect(json['clients'][0]['id']).to eq(@distributor.id)
          expect(json['clients'][0]['type']).to eq(@distributor.class.to_s)
          expect(json['clients'][0]['name']).to eq(@distributor.name)
          expect(json['clients'][0]['allow_special_discount']).to eq(@distributor.allow_special_discount?)
          expect(json['clients'][0]['special_discount']).to eq(@distributor.special_discount.to_f)
          expect(json['clients'][0]['third_party_id']).to eq(@distributor.third_party.id)
          expect(json['clients'][0]['owner']['id']).to eq(@distributor.owner.id)
          expect(json['clients'][0]['owner']['name']).to eq(@distributor.owner.name)
          expect(json['clients'][0]['owner']['type']).to eq(@distributor.owner.class.to_s)
          expect(json['clients'][1]['id']).to eq(@store_1.id)
          expect(json['clients'][1]['type']).to eq(@store_1.class.to_s)
          expect(json['clients'][1]['name']).to eq(@store_1.name)
          expect(json['clients'][1]['allow_special_discount']).to eq(@store_1.allow_special_discount?)
          expect(json['clients'][1]['special_discount']).to eq(@store_1.special_discount.to_f)
          expect(json['clients'][1]['third_party_id']).to eq(@store_1.third_party.id)
          expect(json['clients'][1]['owner']['id']).to eq(@store_1.owner.id)
          expect(json['clients'][1]['owner']['name']).to eq(@store_1.owner.name)
          expect(json['clients'][1]['owner']['type']).to eq(@store_1.owner.class.to_s)
          expect(json['clients'][2]['id']).to eq(@store_2.id)
          expect(json['clients'][2]['type']).to eq(@store_2.class.to_s)
          expect(json['clients'][2]['name']).to eq(@store_2.name)
          expect(json['clients'][2]['allow_special_discount']).to eq(@store_2.allow_special_discount?)
          expect(json['clients'][2]['special_discount']).to eq(@store_2.special_discount.to_f)
          expect(json['clients'][2]['third_party_id']).to eq(@store_2.third_party.id)
          expect(json['clients'][2]['owner']['id']).to eq(@store_2.owner.id)
          expect(json['clients'][2]['owner']['name']).to eq(@store_2.owner.name)
          expect(json['clients'][2]['owner']['type']).to eq(@store_2.owner.class.to_s)
          expect(json['clients'][3]['id']).to eq(@customer.id)
          expect(json['clients'][3]['type']).to eq(@customer.class.to_s)
          expect(json['clients'][3]['name']).to eq(@customer.name)
          expect(json['clients'][3]['allow_special_discount']).to eq(@customer.allow_special_discount?)
          expect(json['clients'][3]['special_discount']).to eq(@customer.special_discount.to_f)
          expect(json['clients'][3]['third_party_id']).to eq(@customer.third_party.id)
          expect(json['clients'][3]['owner']['id']).to eq(@customer.owner.id)
          expect(json['clients'][3]['owner']['name']).to eq(@customer.owner.name)
          expect(json['clients'][3]['owner']['type']).to eq(@customer.owner.class.to_s)
        end
      end

      context 'with logged distributor' do
        before do
          sign_in :administrator, @distributor.users.first
        end

        it 'returns the stores and customers' do
          xhr :get, :index

          expect(json['clients'].size).to eq(3)
          expect(json['clients'][0]['id']).to eq(@store_1.id)
          expect(json['clients'][0]['type']).to eq(@store_1.class.to_s)
          expect(json['clients'][0]['name']).to eq(@store_1.name)
          expect(json['clients'][0]['allow_special_discount']).to eq(@store_1.allow_special_discount?)
          expect(json['clients'][0]['special_discount']).to eq(@store_1.special_discount.to_f)
          expect(json['clients'][0]['third_party_id']).to eq(@store_1.third_party.id)
          expect(json['clients'][0]['owner']['id']).to eq(@store_1.owner.id)
          expect(json['clients'][0]['owner']['name']).to eq(@store_1.owner.name)
          expect(json['clients'][0]['owner']['type']).to eq(@store_1.owner.class.to_s)
          expect(json['clients'][1]['id']).to eq(@store_2.id)
          expect(json['clients'][1]['type']).to eq(@store_2.class.to_s)
          expect(json['clients'][1]['name']).to eq(@store_2.name)
          expect(json['clients'][1]['allow_special_discount']).to eq(@store_2.allow_special_discount?)
          expect(json['clients'][1]['special_discount']).to eq(@store_2.special_discount.to_f)
          expect(json['clients'][1]['third_party_id']).to eq(@store_2.third_party.id)
          expect(json['clients'][1]['owner']['id']).to eq(@store_2.owner.id)
          expect(json['clients'][1]['owner']['name']).to eq(@store_2.owner.name)
          expect(json['clients'][1]['owner']['type']).to eq(@store_2.owner.class.to_s)
          expect(json['clients'][2]['id']).to eq(@customer.id)
          expect(json['clients'][2]['type']).to eq(@customer.class.to_s)
          expect(json['clients'][2]['name']).to eq(@customer.name)
          expect(json['clients'][2]['allow_special_discount']).to eq(@customer.allow_special_discount?)
          expect(json['clients'][2]['special_discount']).to eq(@customer.special_discount.to_f)
          expect(json['clients'][2]['third_party_id']).to eq(@customer.third_party.id)
          expect(json['clients'][2]['owner']['id']).to eq(@customer.owner.id)
          expect(json['clients'][2]['owner']['name']).to eq(@customer.owner.name)
          expect(json['clients'][2]['owner']['type']).to eq(@customer.owner.class.to_s)
        end
      end

      context 'with logged store' do
        before do
          sign_in :administrator, @store_1.users.first
        end

        it 'returns the customers' do
          xhr :get, :index

          expect(json['clients'].size).to eq(1)
          expect(json['clients'][0]['id']).to eq(@customer.id)
          expect(json['clients'][0]['type']).to eq(@customer.class.to_s)
          expect(json['clients'][0]['name']).to eq(@customer.name)
          expect(json['clients'][0]['allow_special_discount']).to eq(@customer.allow_special_discount?)
          expect(json['clients'][0]['special_discount']).to eq(@customer.special_discount.to_f)
          expect(json['clients'][0]['third_party_id']).to eq(@customer.third_party.id)
          expect(json['clients'][0]['owner']['id']).to eq(@customer.owner.id)
          expect(json['clients'][0]['owner']['name']).to eq(@customer.owner.name)
          expect(json['clients'][0]['owner']['type']).to eq(@customer.owner.class.to_s)
        end
      end
    end
  end
end
