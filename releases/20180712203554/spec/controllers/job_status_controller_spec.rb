require 'rails_helper'

RSpec.describe JobStatusController, type: :controller do
  before do
    @factory = FactoryGirl.create(:factory, name: 'F1')
    FactoryGirl.create(:administrator, username: 'F1').tap do |administrator|
      administrator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @factory, user: administrator))
    end

    sign_in :administrator, @factory.users.first
  end

  describe 'GET show' do
    let(:job_id) { 'ABC2323232' }
    let(:job)    { double(:job, at: 10, total: 20, pct_complete: 50, message: 'foo', payload: 'foo', status: 'working', kill_requested?: false, killed?: false) }

    before do
      allow(SidekiqStatus::Container).to receive(:load).and_return(job)
    end

    it 'loads the sidekiq job' do
      expect(SidekiqStatus::Container).to receive(:load).with(job_id)

      xhr :get, :show, { id: job_id }
    end

    context 'when job exists' do
      context 'for \'waiting\' status' do
        let(:job) { double(:job, at: 0, total: 0, pct_complete: 0, message: '', status: 'waiting', kill_requested?: false, killed?: false) }

        it 'returns the in progress json' do
          xhr :get, :show, { id: job_id }

          expect(json['job_status']['in_progress']).to eq(true)
          expect(json['job_status']['at']).to eq(0)
          expect(json['job_status']['total']).to eq(0)
          expect(json['job_status']['pct_complete']).to eq(0)
          expect(json['job_status']['message']).to eq('')
          expect(json['job_status']['cancelling']).to be_falsy
        end
      end

      context 'for \'working\' status' do
        let(:job) { double(:job, at: 10, total: 100, pct_complete: 10, message: 'foo', status: 'working', kill_requested?: false, killed?: false) }

        it 'returns the in progress json' do
          xhr :get, :show, { id: job_id }

          expect(json['job_status']['in_progress']).to eq(true)
          expect(json['job_status']['at']).to eq(10)
          expect(json['job_status']['total']).to eq(100)
          expect(json['job_status']['pct_complete']).to eq(10)
          expect(json['job_status']['message']).to eq('foo')
          expect(json['job_status']['cancelling']).to be_falsy
        end
      end

      context 'for \'complete\' status' do
        let(:job) { double(:job, at: 100, total: 100, pct_complete: 100, payload: 'fobo', status: 'complete', kill_requested?: false, killed?: false) }

        it 'returns the finished json' do
          xhr :get, :show, { id: job_id }

          expect(json['job_status']['in_progress']).to eq(false)
          expect(json['job_status']['cancelling']).to be_falsy
        end
      end

      context 'for \'failed\' status' do
        let(:job) { double(:job, at: 10, total: 100, pct_complete: 10, payload: 'foco', status: 'failed', kill_requested?: false, killed?: false) }

        it 'returns the finished json' do
          xhr :get, :show, { id: job_id }

          expect(json['job_status']['in_progress']).to eq(false)
          expect(json['job_status']['message']).to eq('foco')
          expect(json['job_status']['cancelling']).to be_falsy
        end
      end

      context 'for \'killed\' status' do
        let(:job)      { double(:job, at: 10, total: 100, pct_complete: 10, payload: 'foco', status: 'killed', kill_requested?: false, killed?: true, args: [100]) }
        let(:estimate) { instance_double(Estimate, id: 100, status: :processing, free!: true) }

        before do
          allow(Estimate).to receive(:find_by_id).and_return(estimate)
        end

        it 'frees the estimate' do
          expect(estimate).to receive(:free!)

          xhr :get, :show, { id: job_id }
        end

        it 'returns the finished json' do
          xhr :get, :show, { id: job_id }

          expect(json['job_status']['in_progress']).to eq(false)
          expect(json['job_status']['message']).to eq('foco')
          expect(json['job_status']['cancelling']).to be_falsy
        end
      end
    end

    context 'when job is not found' do
      before do
        allow(SidekiqStatus::Container).to receive(:load).and_call_original
      end

      it 'returns finished json' do
        xhr :get, :show, { id: job_id }

        expect(json['job_status']['in_progress']).to eq(false)
      end

      it 'returns not found status' do
        xhr :get, :show, { id: job_id }

        expect(response).to have_http_status(:not_found)
      end
    end
  end

  describe 'DELETE destroy' do
    let(:job_id) { 'ABC2323232' }
    let(:job)    { double(:job, status: 'working', killable?: false) }

    before do
      allow(SidekiqStatus::Container).to receive(:load).and_return(job)
    end

    it 'loads the sidekiq job' do
      expect(SidekiqStatus::Container).to receive(:load).with(job_id)

      xhr :delete, :destroy, { id: job_id }
    end

    context 'when job exists' do
      it 'returns no content status' do
        xhr :delete, :destroy, { id: job_id }

        expect(response).to have_http_status(:no_content)
      end

      context 'for \'waiting\' status' do
        context 'and the job is killable' do
          let(:job) { double(:job, status: 'waiting', killable?: true) }

          it 'deletes the job' do
            expect(job).to receive(:request_kill)

            xhr :delete, :destroy, { id: job_id }
          end
        end

        context 'and the job is not killable' do
          let(:job) { double(:job, status: 'waiting', killable?: false) }

          it 'does not delete the job' do
            expect(job).to_not receive(:request_kill)

            xhr :delete, :destroy, { id: job_id }
          end
        end
      end

      context 'for \'working\' status' do
        context 'and the job is killable' do
          let(:job) { double(:job, status: 'working', killable?: true) }

          it 'deletes the job' do
            expect(job).to receive(:request_kill)

            xhr :delete, :destroy, { id: job_id }
          end
        end

        context 'and the job is not killable' do
          let(:job) { double(:job, status: 'working', killable?: false) }

          it 'does not delete the job' do
            expect(job).to_not receive(:request_kill)

            xhr :delete, :destroy, { id: job_id }
          end
        end
      end

      context 'for \'complete\' status' do
        context 'and the job is killable' do
          let(:job) { double(:job, status: 'complete', killable?: true) }

          it 'deletes the job' do
            expect(job).to receive(:request_kill)

            xhr :delete, :destroy, { id: job_id }
          end
        end

        context 'and the job is not killable' do
          let(:job) { double(:job, status: 'complete', killable?: false) }

          it 'does not delete the job' do
            expect(job).to_not receive(:request_kill)

            xhr :delete, :destroy, { id: job_id }
          end
        end
      end

      context 'for \'failed\' status' do
        context 'and the job is killable' do
          let(:job) { double(:job, status: 'failed', killable?: true) }

          it 'deletes the job' do
            expect(job).to receive(:request_kill)

            xhr :delete, :destroy, { id: job_id }
          end
        end

        context 'and the job is not killable' do
          let(:job) { double(:job, status: 'failed', killable?: false) }

          it 'does not delete the job' do
            expect(job).to_not receive(:request_kill)

            xhr :delete, :destroy, { id: job_id }
          end
        end
      end
    end
  end
end
