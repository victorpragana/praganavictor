# == Schema Information
#
# Table name: items
#
#  id             :integer          not null, primary key
#  code           :string
#  description    :string           not null
#  unit           :integer          not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  deleted_at     :datetime
#  type           :integer          default(0)
#  erp_categories :json
#  ncm            :string(8)
#  cst            :integer
#

require 'rails_helper'

RSpec.describe ItemsController, type: :controller do
  before do
    sign_in :administrator, create(:administrator)
  end
  
  describe 'GET index' do
    render_views

    before do
      
      @item_1 = create(:item, code: 'aa', description: 'aa')
      @item_2 = create(:item, code: 'bb', description: 'bb')
      @item_3 = create(:item, code: 'cc', description: 'cc')
    end

    it 'returns OK' do
      xhr :get, :index

      expect(response).to have_http_status(:ok)
    end

    it 'lists the items' do
      xhr :get, :index

      expect(json['items'].size).to eq(3)

      expect(json['items'][0]['id']).to eq(@item_1.id)
      expect(json['items'][0]['code']).to eq(@item_1.code)
      expect(json['items'][0]['description']).to eq(@item_1.description)
      expect(json['items'][0]['unit']).to eq(Item.units[@item_1.unit])
      expect(json['items'][0]['type']).to eq(Item.types[@item_1.type])

      expect(json['items'][1]['id']).to eq(@item_2.id)
      expect(json['items'][1]['code']).to eq(@item_2.code)
      expect(json['items'][1]['description']).to eq(@item_2.description)
      expect(json['items'][1]['unit']).to eq(Item.units[@item_2.unit])
      expect(json['items'][1]['type']).to eq(Item.types[@item_2.type])

      expect(json['items'][2]['id']).to eq(@item_3.id)
      expect(json['items'][2]['code']).to eq(@item_3.code)
      expect(json['items'][2]['description']).to eq(@item_3.description)
      expect(json['items'][2]['unit']).to eq(Item.units[@item_3.unit])
      expect(json['items'][2]['type']).to eq(Item.types[@item_3.type])
    end
  end

  describe 'POST create' do
    render_views

    context 'with invalid params' do
      let(:parameters) { {'code' => '123-abc', 'description' => '', 'unit' => '0', 'type' => '1'} }

      it 'does not create a new Item' do
        expect {
          xhr :post, :create, parameters
        }.to change(Item, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :create, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) { {'code' => '123-abc', 'description' => 'Foo', 'unit' => '0', 'type' => '1'} }

      it 'initializes the item service' do
        expect(ItemService).to receive(:new).with(parameters).and_call_original

        xhr :post, :create, parameters
      end

      it 'creates the item' do
        expect_any_instance_of(ItemService).to receive(:create).and_call_original
        
        xhr :post, :create, parameters
      end

      it 'returns created status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:created)
      end
    end
  end

  describe 'PUT update' do
    render_views

    before do
      @item = create(:item)
    end

    context 'with invalid params' do
      let(:parameters) { {'code' => '123-abc', 'description' => '', 'unit' => '0', 'type' => '1'}.merge({'id' => "#{@item.id}"})}

      it 'does not create a new Item' do
        expect {
          xhr :put, :update, parameters
        }.to change(Item, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :put, :update, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :put, :update, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) { {'code' => '123-abc', 'description' => 'Foo', 'unit' => '0', 'type' => '1',
        'erp_categories' => {"LINHA DE PRODUTO"=>"PRODUTO ACABADO", "GRUPO"=>"ADAPTADOR", "SUBGRUPO"=>"ROMANA", "TRIBUTAÇÃO"=>"TRIBUTADO ST"},
        'ncm' => '58975841', 'cst' => '3'
      } }

      it 'initializes the item service' do
        expect(ItemService).to receive(:new).with(parameters).and_call_original

        xhr :put, :update, parameters.merge({'id' => "#{@item.id}"})
      end

      it 'updates the item' do
        expect_any_instance_of(ItemService).to receive(:update).with(@item.id.to_s).and_call_original
        
        xhr :put, :update, parameters.merge({'id' => "#{@item.id}"})
      end

      it 'returns ok status' do
        xhr :put, :update, parameters.merge({'id' => "#{@item.id}"})
        
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'GET show' do
    render_views

    before do
      @item = create(:item, erp_categories: {
        "LINHA DE PRODUTO"=>"PRODUTO ACABADO", "GRUPO"=>"ADAPTADOR", "SUBGRUPO"=>"ROMANA", "TRIBUTAÇÃO"=>"TRIBUTADO ST"
      }, ncm: '58975841', cst: 3)
    end

    it 'should ok status' do
      xhr :get, :show, id: @item.id

      expect(response).to have_http_status(:ok)
    end

    it 'returns the item data' do
      xhr :get, :show, id: @item.id

      expect(json['item']['id']).to eq(@item.id)
      expect(json['item']['code']).to eq(@item.code)
      expect(json['item']['description']).to eq(@item.description)
      expect(json['item']['unit']).to eq(Item.units[@item.unit])
      expect(json['item']['type']).to eq(Item.types[@item.type])
      expect(json['item']['type']).to eq(Item.types[@item.type])
      expect(json['item']['erp_categories']['LINHA DE PRODUTO']).to eq('PRODUTO ACABADO')
      expect(json['item']['erp_categories']['GRUPO']).to eq('ADAPTADOR')
      expect(json['item']['erp_categories']['SUBGRUPO']).to eq('ROMANA')
      expect(json['item']['erp_categories']['TRIBUTAÇÃO']).to eq('TRIBUTADO ST')
      expect(json['item']['ncm']).to eq('58975841')
      expect(json['item']['cst']).to eq(3)
    end
  end

  describe 'DELETE destroy' do
    before do
      @item = create(:item)
    end

    context 'with success' do
      it 'destroys the item' do
        expect {
          xhr :delete, :destroy, { id: @item.id }
        }.to change(Item, :count).by(-1)
      end

      it 'returns no content status' do
        xhr :delete, :destroy, { id: @item.id }

        expect(response).to have_http_status(:no_content)
      end
    end

    context 'with error' do
      let(:item) {mock_model(Item, destroy: false, errors: double(:errors, full_messages: ['foo']))}

      before do
        allow(Item).to receive(:find).and_return(item)
      end

      it 'does not destroy the item' do
        expect {
          xhr :delete, :destroy, { id: item.id }
        }.to_not change(Item, :count)
      end

      it 'returns the bad request status' do
        xhr :delete, :destroy, { id: item.id }

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the errors' do
        xhr :delete, :destroy, { id: item.id }
        
        expect(json['errors']).to_not be_empty
      end
    end
  end

  describe 'DELETE batch_destroy' do
    render_views

    before do
      @item_1 = create(:item)
      @item_2 = create(:item)
    end

    context 'with success' do
      it 'destroys the items' do
        expect {
          xhr :delete, :batch_destroy, { ids: [@item_1.id, @item_2.id] }
        }.to change(Item, :count).by(-2)
      end

      it 'returns no content status' do
        xhr :delete, :batch_destroy, { ids: [@item_1.id, @item_2.id] }

        expect(response).to have_http_status(:no_content)
      end
    end

    context 'with error' do
      before do
        allow(@item_1).to receive(:destroy!).and_raise(StandardError)
        allow(Item).to receive_message_chain(:find).and_return(@item_1)
      end

      it 'does not destroy the items' do
        expect {
          xhr :delete, :batch_destroy, { ids: [@item_1.id] }
        }.to_not change(Item, :count)
      end

      it 'returns the bad request status' do
        xhr :delete, :batch_destroy, { ids: [@item_1.id] }

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the errors' do
        xhr :delete, :batch_destroy, { ids: [@item_1.id] }
        
        expect(json['errors']).to_not be_empty
      end
    end
  end
end
