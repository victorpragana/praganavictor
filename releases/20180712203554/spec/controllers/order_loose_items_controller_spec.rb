# == Schema Information
#
# Table name: order_loose_items
#
#  id                :integer          not null, primary key
#  order_id          :integer
#  item_code         :string
#  item_description  :string           not null
#  unit              :integer          not null
#  quantity          :decimal(8, 2)    not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  type              :integer          default(0)
#  factory_value     :decimal(10, 2)   default(0.0)
#  distributor_value :decimal(10, 2)   default(0.0)
#  store_value       :decimal(10, 2)   default(0.0)
#  customer_value    :decimal(10, 2)   default(0.0)
#  discount          :decimal(5, 2)    default(0.0)
#  item_id           :integer
#

require 'rails_helper'

RSpec.describe OrderLooseItemsController, type: :controller do
  before(:each) do
    @family      = create(:family)
    @line        = create(:line, family: @family, sale_price: BigDecimal.new('15.56'))
    @distributor = create(:distributor, name: 'D1')
    @store       = create(:store, distributor: @distributor, name: 'S1')
    @customer    = create(:customer, store_id: @store.id, name: 'C1')
    @user        = create(:administrator, username: 'F1').tap do |administrator|
      administrator.update!(selected_workplace: create(:user_workplace, workplace: @store, user: administrator))
    end
    @order       = create(:order, seller: @store, client: @customer, billing_client: @customer, user: @user)
    @item        = create(:item)

    sign_in :administrator, @user
  end

 
  describe 'PUT apply_value' do
    render_views

    before(:each) do
      @order_loose_item = create(:order_loose_item, distributor_value: 0, store_value: 0, customer_value: 0, order: @order, item: @item)
    end

    context 'with invalid data' do
      context 'for unallowed values' do
        let(:parameters) { {'value' => '-1'}.merge('id' => "#{@order_loose_item.id}") }

        it 'does not update the order_loose_item\'s options values' do
          expect {
            xhr :put, :apply_value, parameters
            @order_loose_item.reload
          }.to_not change(@order_loose_item, :value)
        end

        it 'does not update the order_loose_item\'s options values' do
          expect {
            xhr :put, :apply_value, parameters
            @order_loose_item.reload
          }.to_not change(@order_loose_item, :value)
        end

        it 'returns unprocessable entity status' do
          xhr :put, :apply_value, parameters
          
          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'returns the errors' do
          xhr :put, :apply_value, parameters
          
          expect(json['errors']).to_not be_empty
        end
      end
    end

    context 'with valid params' do
      let(:parameters) { {'value' => '10.58'} }


      it 'initializes the order_loose_item service' do
        expect(OrderLooseItemService).to receive(:new).with(parameters).and_call_original

        xhr :put, :apply_value, parameters.merge('id' => "#{@order_loose_item.id}")
      end

      it 'should apply the value to the order_loose_item' do
        expect_any_instance_of(OrderLooseItemService).to receive(:apply_value).with(@order_loose_item.id.to_s).and_call_original
        
        xhr :put, :apply_value, parameters.merge('id' => "#{@order_loose_item.id}")
      end

      it 'returns ok status' do
        xhr :put, :apply_value, parameters.merge('id' => "#{@order_loose_item.id}")
        
        expect(response).to have_http_status(:ok)
      end
    end
  end
end
