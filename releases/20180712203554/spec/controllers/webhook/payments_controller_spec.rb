require 'rails_helper'

RSpec.describe Webhooks::PaymentsController, type: :controller do
  describe 'POST status_changed' do
    before(:each) do
      @factory = create(:factory, name: 'F1')
      @distributor = create(:distributor, name: 'D1')
      @store = create(:store, name: 'S1', distributor: @distributor)

      @user = create(:administrator).tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: @store, user: administrator))
      end

      @order = create(:order, seller: @store, client: @distributor, billing_client: @distributor, user: @user)

      @payment = create(:payment, iugu_invoice_id: '1757E1D7FD5E410A9C563024250015BF', status: :created, payment_method: :bank_slip)

      @movement1 = create(:production_credit, owner: @store.third_party, destination: @distributor.third_party, payment: @payment, direction: :input, date: 20.days.ago, value: 200, status: :pending)
      @movement2 = create(:money_credit, owner: @store.third_party, destination: @distributor.third_party, order: @order, payment: @payment, direction: :input, date: 25.days.ago, value: 300, status: :pending)
    end

    context 'with invalid parameters' do
      let(:parameters) { { 'event' => '', 'data' => { 'id' => 'FOOBAR1234', 'status' => 'paid', 'account_id' => '', 'subscription_id' => '' } } }

      it 'returns the bad request status' do
        xhr :post, :status_changed, parameters

        expect(response).to have_http_status(:ok)
      end
    end

    context 'with valid parameters' do
      let(:parameters) { { 'event' => 'invoice.status_changed', 'data' => { 'id' => @payment.iugu_invoice_id.to_s, 'status' => 'paid', 'account_id' => '70CA234077134ED0BF2E0E46B0EDC36F', 'subscription_id' => 'F4115E5E28AE4CCA941FCCCCCABE9A0A' } } }

      it 'initializes the payment service' do
        expect(PaymentService).to receive(:new).with(ActionController::Parameters.new(parameters).require(:data).permit(:id, :status, :account_id, :subscription_id)).and_call_original

        xhr :post, :status_changed, parameters
      end

      it 'updates the payment' do
        expect_any_instance_of(PaymentService).to receive(:status_changed).and_call_original

        xhr :post, :status_changed, parameters
      end

      it 'returns the ok status' do
        xhr :post, :status_changed, parameters

        expect(response).to have_http_status(:ok)
      end
    end
  end
end
