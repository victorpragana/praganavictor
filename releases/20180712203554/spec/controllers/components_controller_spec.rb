# == Schema Information
#
# Table name: components
#
#  id                        :integer          not null, primary key
#  name                      :string(255)      not null
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  cost                      :decimal(10, 5)   default(0.0), not null
#  erasable                  :boolean          default(TRUE)
#  category_id               :integer
#  characteristics           :text
#  code                      :string(20)
#  unit                      :integer          default(2), not null
#  deleted_at                :datetime
#  base_component_id         :integer
#  reference_cost            :decimal(10, 5)   default(0.0)
#  offset_left               :integer          default(0)
#  offset_top                :integer          default(0)
#  offset_right              :integer          default(0)
#  offset_bottom             :integer          default(0)
#  allow_rotation            :boolean          default(FALSE)
#  exchange                  :decimal(10, 5)
#  loss                      :decimal(10, 5)
#  cut_table_id              :integer
#  print_on_lines_label      :boolean          default(FALSE)
#  fix_cost                  :decimal(10, 5)   default(0.0)
#  print_on_components_label :boolean          default(FALSE)
#

require 'rails_helper'

RSpec.describe ComponentsController, type: :controller do
  before do
    sign_in :administrator, create(:administrator)
    
    @family = create(:family)
    @category_1 = create(:category, name: 'C1')
    @category_2 = create(:category, name: 'C2')
    @category_3 = create(:category, name: 'C3')
    @cut_table  = create(:cut_table)
  end

  describe 'GET index' do
    render_views

    before do
      @component_1 = create(:component, unit: :cm, name: 'C1', category: @category_1, families: [@family])
      @component_2 = create(:component, unit: :cm, name: 'C2', category: @category_2)
      @component_3 = create(:component, unit: :cm, name: 'C3', category: @category_3)
      @component_4 = create(:component, unit: :cm, name: 'C4', category: @category_3, erasable: false)
    end

    it 'returns OK' do
      xhr :get, :index

      expect(response).to have_http_status(:ok)
    end
  end

  describe 'POST create' do
    render_views

    context 'with invalid params' do
      let(:parameters) {{'name' => ''}}

      it 'does not create a new Component' do
        expect {
          xhr :post, :create, parameters
        }.to change(Component, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :create, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:offset_left)             {5}
      let(:offset_top)              {6}
      let(:offset_right)            {20}
      let(:offset_bottom)           {11}
      let(:converted_offset_left)   {offset_left  * 10}
      let(:converted_offset_top)    {offset_top  * 10}
      let(:converted_offset_right)  {offset_right * 10}
      let(:converted_offset_bottom) {offset_bottom * 10}

      let(:parameters) {{'name' => 'Manual Mola', 'unit'=>"#{Component.units[:un]}", 'category_id' => "#{@category_1.id}", 'cost' => '2.54', 
        'offset_left' => "#{offset_left}", 'offset_right' => "#{offset_right}", 'offset_top' => "#{offset_top}", 'offset_bottom' => "#{offset_bottom}",
        'allow_rotation' => 'true', 'exchange' => '2.69', 'loss' => '1.36', 'cut_table_id' => "#{@cut_table.id}",
        'reference_cost' => '1.54', 'fix_cost' => '5.54', 'code' => 'ABC123', 'characteristics' => 'Big text', 'print_on_components_label' => 'true', 'print_on_lines_label' => 'true', 
        'family_ids' => ["#{@family.id}"]
      }}

      before do
        @category_1.update!(use_cut_table: true)
      end

      it 'initializes the component service' do
        expect(ComponentService).to receive(:new).with(
          parameters.dup.tap do |p|
            p['offset_left']   = converted_offset_left
            p['offset_top']    = converted_offset_top
            p['offset_right']  = converted_offset_right
            p['offset_bottom'] = converted_offset_bottom
          end
        ).and_call_original

        xhr :post, :create, parameters
      end

      it 'creates the component' do
        expect_any_instance_of(ComponentService).to receive(:create).and_call_original
        
        xhr :post, :create, parameters
      end

      it 'returns created status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:created)
      end

      it 'returns the component data' do
        xhr :post, :create, parameters

        component = Component.last

        expect(json['component']['id']).to eq(component.id)
        expect(json['component']['name']).to eq(component.name)
        expect(json['component']['cost']).to eq(component.cost)
        expect(json['component']['reference_cost']).to eq(component.reference_cost)
        expect(json['component']['exchange']).to eq(component.exchange)
        expect(json['component']['loss']).to eq(component.loss)
        expect(json['component']['fix_cost']).to eq(component.fix_cost)
        expect(json['component']['code']).to eq(component.code)
        expect(json['component']['unit']).to eq(Component.units[component.unit])
        expect(json['component']['characteristics']).to eq(component.characteristics)
        expect(json['component']['category_id']).to eq(@category_1.id)
        expect(json['component']['family_ids']).to eq([component.families.first.id])
        expect(json['component']['automatic']).to eq(component.automatic?)
        expect(json['component']['offset_left']).to eq(component.offset_left_from_mm_to_cm)
        expect(json['component']['offset_top']).to eq(component.offset_top_from_mm_to_cm)
        expect(json['component']['offset_right']).to eq(component.offset_right_from_mm_to_cm)
        expect(json['component']['offset_bottom']).to eq(component.offset_bottom_from_mm_to_cm)
        expect(json['component']['allow_rotation']).to eq(component.allow_rotation?)
        expect(json['component']['cut_table_id']).to eq(component.cut_table_id)
        expect(json['component']['print_on_components_label']).to eq(component.print_on_components_label?)
        expect(json['component']['print_on_lines_label']).to eq(component.print_on_lines_label?)
      end
    end
  end

  describe 'PUT update' do
    render_views

    before do
      @child_1 = create(:component, unit: :cm, name: 'C1', category: @category_1, families: [@family])
      @child_2 = create(:component, unit: :cm, name: 'C2', category: @category_2)
      @child_3 = create(:component, unit: :cm, name: 'C3', category: @category_3)
      @child_4 = create(:component, unit: :cm, name: 'C4', category: @category_3, erasable: false)

      @component = create(:component, unit: :cm, category: @category_2, children: [@child_1, @child_4])
    end

    context 'with invalid params' do
      let(:parameters) {{'name' => ''}.merge({'id' => "#{@component.id}"})}

      it 'does not create a new Component' do
        expect {
          xhr :put, :update, parameters
        }.to change(Component, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :put, :update, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :put, :update, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:offset_left)             {5}
      let(:offset_top)              {6}
      let(:offset_right)            {20}
      let(:offset_bottom)           {11}
      let(:converted_offset_left)   {offset_left  * 10}
      let(:converted_offset_top)    {offset_top  * 10}
      let(:converted_offset_right)  {offset_right * 10}
      let(:converted_offset_bottom) {offset_bottom * 10}

      let(:parameters) {{'name' => 'Manual Mola', 'unit'=>"#{Component.units[:un]}", 'category_id' => "#{@category_1.id}", 'cost' => '2.54', 
        'offset_left' => "#{offset_left}", 'offset_right' => "#{offset_right}", 'offset_top' => "#{offset_top}", 'offset_bottom' => "#{offset_bottom}",
        'allow_rotation' => 'true', 'exchange' => '2.69', 'loss' => '1.36', 'cut_table_id' => "#{@cut_table.id}",
        'reference_cost' => '1.54', 'fix_cost' => '5.54', 'code' => 'ABC123', 'characteristics' => 'Big text', 'print_on_components_label' => 'true', 'print_on_lines_label' => 'true', 
        'family_ids' => ["#{@family.id}"], 'child_ids' => ["#{@child_2.id}", "#{@child_3.id}"]
      }}

      before do
        @category_1.update!(use_cut_table: true)
      end

      it 'initializes the component service' do
        expect(ComponentService).to receive(:new).with(
          parameters.dup.tap do |p|
            p['offset_left']   = converted_offset_left
            p['offset_top']    = converted_offset_top
            p['offset_right']  = converted_offset_right
            p['offset_bottom'] = converted_offset_bottom
          end
        ).and_call_original

        xhr :put, :update, parameters.merge({'id' => "#{@component.id}"})
      end

      it 'updates the component' do
        expect_any_instance_of(ComponentService).to receive(:update).with(@component.id.to_s).and_call_original
        
        xhr :put, :update, parameters.merge({'id' => "#{@component.id}"})
      end

      it 'returns ok status' do
        xhr :put, :update, parameters.merge({'id' => "#{@component.id}"})
        
        expect(response).to have_http_status(:ok)
      end

      it 'returns the component data' do
        xhr :put, :update, parameters.merge({'id' => "#{@component.id}"})

        component = Component.last

        expect(json['component']['id']).to eq(component.id)
        expect(json['component']['name']).to eq(component.name)
        expect(json['component']['cost']).to eq(component.cost)
        expect(json['component']['reference_cost']).to eq(component.reference_cost)
        expect(json['component']['exchange']).to eq(component.exchange)
        expect(json['component']['loss']).to eq(component.loss)
        expect(json['component']['fix_cost']).to eq(component.fix_cost)
        expect(json['component']['code']).to eq(component.code)
        expect(json['component']['unit']).to eq(Component.units[component.unit])
        expect(json['component']['characteristics']).to eq(component.characteristics)
        expect(json['component']['category_id']).to eq(@category_1.id)
        expect(json['component']['family_ids']).to eq([component.families.first.id])
        expect(json['component']['automatic']).to eq(component.automatic?)
        expect(json['component']['offset_left']).to eq(component.offset_left_from_mm_to_cm)
        expect(json['component']['offset_top']).to eq(component.offset_top_from_mm_to_cm)
        expect(json['component']['offset_right']).to eq(component.offset_right_from_mm_to_cm)
        expect(json['component']['offset_bottom']).to eq(component.offset_bottom_from_mm_to_cm)
        expect(json['component']['allow_rotation']).to eq(component.allow_rotation?)
        expect(json['component']['child_ids']).to match_array([@child_2.id, @child_3.id])
        expect(json['component']['cut_table_id']).to eq(component.cut_table_id)
        expect(json['component']['print_on_components_label']).to eq(component.print_on_components_label?)
        expect(json['component']['print_on_lines_label']).to eq(component.print_on_lines_label?)
      end
    end
  end

  describe 'GET show' do
    render_views

    before do
      @category_3.update!(use_cut_table: true)
      @component = create(:component, unit: :cm, category: @category_3, cut_table: @cut_table, families: [@family])
    end

    it 'returns ok status' do
      xhr :get, :show, id: @component.id

      expect(response).to have_http_status(:ok)
    end

    it 'returns the component data' do
      xhr :get, :show, id: @component.id

      expect(json['component']['id']).to eq(@component.id)
      expect(json['component']['name']).to eq(@component.name)
      expect(json['component']['cost']).to eq(@component.cost)
      expect(json['component']['reference_cost']).to eq(@component.reference_cost)
      expect(json['component']['exchange']).to eq(@component.exchange)
      expect(json['component']['loss']).to eq(@component.loss)
      expect(json['component']['fix_cost']).to eq(@component.fix_cost)
      expect(json['component']['code']).to eq(@component.code)
      expect(json['component']['unit']).to eq(Component.units[@component.unit])
      expect(json['component']['characteristics']).to eq(@component.characteristics)
      expect(json['component']['category_id']).to eq(@category_3.id)
      expect(json['component']['family_ids']).to eq([@component.families.first.id])
      expect(json['component']['automatic']).to eq(@component.automatic?)
      expect(json['component']['offset_left']).to eq(@component.offset_left_from_mm_to_cm)
      expect(json['component']['offset_top']).to eq(@component.offset_top_from_mm_to_cm)
      expect(json['component']['offset_right']).to eq(@component.offset_right_from_mm_to_cm)
      expect(json['component']['offset_bottom']).to eq(@component.offset_bottom_from_mm_to_cm)
      expect(json['component']['allow_rotation']).to eq(@component.allow_rotation?)
      expect(json['component']['deleted']).to eq(@component.deleted?)
      expect(json['component']['cut_table_id']).to eq(@component.cut_table_id)
      expect(json['component']['print_on_components_label']).to eq(@component.print_on_components_label?)
      expect(json['component']['print_on_lines_label']).to eq(@component.print_on_lines_label?)
    end
  end

  describe 'DELETE destroy' do
    render_views

    before do
      @component = create(:component, unit: :cm, name: 'C1', category: @category_1, families: [@family])
    end

    context 'with success' do
      it 'destroys the component' do
        expect {
          xhr :delete, :destroy, { id: @component.id }
        }.to change(Component, :count).by(-1)
      end

      it 'returns no content status' do
        xhr :delete, :destroy, { id: @component.id }

        expect(response).to have_http_status(:no_content)
      end
    end

    context 'with error' do
      let(:component) {mock_model(Component, destroy: false, errors: double(:errors, full_messages: ['foo']))}

      before do
        allow(Component).to receive_message_chain(:with_deleted, :find).and_return(component)
      end

      it 'does not destroy the component' do
        expect {
          xhr :delete, :destroy, { id: component.id }
        }.to_not change(Component, :count)
      end

      it 'returns the bad request status' do
        xhr :delete, :destroy, { id: component.id }

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the errors' do
        xhr :delete, :destroy, { id: component.id }
        
        expect(json['errors']).to_not be_empty
      end
    end
  end

  describe 'DELETE really_destroy' do
    before do
      @component = create(:component, unit: :cm, name: 'C1', category: @category_1, families: [@family])
    end

    context 'with success' do
      it 'removes the component' do
        expect {
          xhr :delete, :really_destroy, { id: @component.id }
        }.to change(Component.with_deleted, :count).by(-1)
      end

      it 'returns no content status' do
        xhr :delete, :really_destroy, { id: @component.id }

        expect(response).to have_http_status(:no_content)
      end
    end

    context 'with error' do
      let(:component) {mock_model(Component, really_destroy!: false, errors: double(:errors, full_messages: ['foo']))}

      before do
        allow(Component).to receive_message_chain(:with_deleted, :find).and_return(component)
      end

      it 'does not destroy the component' do
        expect {
          xhr :delete, :really_destroy, { id: @component.id }
        }.to_not change(Component, :count)
      end

      it 'returns the bad request status' do
        xhr :delete, :really_destroy, { id: @component.id }

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the errors' do
        xhr :delete, :really_destroy, { id: @component.id }
        
        expect(json['errors']).to_not be_empty
      end
    end
  end

  describe 'DELETE batch_destroy' do
    render_views

    before do
      @component_1 = create(:component, unit: :cm, name: 'C1', category: @category_1, families: [@family])
      @component_2 = create(:component, unit: :cm, name: 'C2', category: @category_1, families: [@family])
    end

    context 'with success' do
      it 'destroys the components' do
        expect {
          xhr :delete, :batch_destroy, {ids: [@component_1.id, @component_2.id]}
        }.to change(Component, :count).by(-2)
      end

      it 'returns no content status' do
        xhr :delete, :batch_destroy, {ids: [@component_1.id, @component_2.id]}

        expect(response).to have_http_status(:no_content)
      end
    end

    context 'with error' do
      let(:component) {mock_model(Component, errors: double(:errors, full_messages: ['foo']))}

      before do
        allow(component).to receive(:destroy!).and_raise(StandardError)
        allow(Component).to receive(:find).and_return(component)
      end

      it 'does not destroy the components' do
        expect {
          xhr :delete, :batch_destroy, { ids: [component.id] }
        }.to_not change(Component, :count)
      end

      it 'returns the bad request status' do
        xhr :delete, :batch_destroy, { ids: [component.id] }

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the errors' do
        xhr :delete, :batch_destroy, { ids: [component.id] }
        
        expect(json['errors']).to_not be_empty
      end
    end
  end

  describe 'GET select' do
    render_views

    before do
      @line = create(:line, family: @family, sale_price: 12.5)

      @line.categories << build(:line_category, category: @category_1, required: true)
      @line.categories << build(:line_category, category: @category_3, required: false)

      @line.save!
    end

    context 'with invalid data' do
      let(:parameters) { { 'line_id' => @line.id, 'component_ids' => '', 'width' => '', 'height' => '' } }

      it 'returns unprocessable entity status' do
        xhr :get, :select, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :get, :select, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid data' do
      let(:width)            { 50 }
      let(:height)           { 20 }
      let(:converted_width)  { width  * 10 }
      let(:converted_height) { height * 10 }

      before do
        @question1 = create(:question, value: 'LOCAL_INSTALACAO', type: :dropdown, options: [{ 'label' => 'Banheiro', 'value' => 'banheiro' }, { 'label' => 'Cozinha', 'value' => 'cozinha' }])
        @question2 = create(:question, value: 'MAO_DE_OBRA', type: :boolean)
        @question3 = create(:question, value: 'FRETE', type: :boolean)
      end

      context 'for not automatic components' do
        let(:questions_string) { "{\"LOCAL_INSTALACAO\":\"banheiro\",\"MAO_DE_OBRA\":true,\"FRETE\":false}" }
        let(:parameters) { {
          'line_id' => @line.id, 'component_ids' => [@component_1.id], 'width' => "#{width}", 'height' => "#{height}",
          "questions" => questions_string
        } }

        before do
          @component_1 = create(:component, category: @category_1, erasable: true, cost: BigDecimal.new('32.28'), 
            fix_cost: BigDecimal.new('18'), unit: :cm)

          @line_rule = create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: 50..600, 
            total_consumption: '_LARGURA_*2', width_consumption: '_LARGURA_', height_consumption: '_ALTURA_',
            cost: '_CUSTO_BASE_+2', quantity_consumption: '_ALTURA_ * 2')
          @rule = create(:rule, :line_rule => @line_rule, :component => @component_1, control_method: Rule.control_methods[:width], width: 50..600, 
            total_consumption: '_LARGURA_*2', width_consumption: '_LARGURA_', height_consumption: '_ALTURA_',
            cost: '_CUSTO_BASE_+2', quantity_consumption: '_ALTURA_ * 2')
        end

        it 'initializes a new simulation option with the line id, converted width and converted height' do
          expect(SimulationOption).to receive(:new).with(@line.id.to_s, converted_width, converted_height, JSON.parse(questions_string)).and_call_original

          xhr :get, :select, parameters
        end

        it 'returns ok status' do
          xhr :get, :select, parameters
          
          expect(response).to have_http_status(:ok)
        end

        it 'returns the component prices and total_consumption' do
          xhr :get, :select, parameters
          
          expect(json['component']['id']).to eq(@component_1.id)
          expect(json['component']['name']).to eq(@component_1.name)
          expect(json['component']['price']['total_consumption']).to eq(100)
          expect(json['component']['price']['width_consumption']).to eq(50)
          expect(json['component']['price']['height_consumption']).to eq(20)
          expect(json['component']['price']['quantity_consumption']).to eq(40)
          expect(json['component']['price']['cost']).to eq(BigDecimal.new('34.28'))
          expect(json['component']['price']['price']).to eq(BigDecimal.new('38.565'))
          expect(json['component']['price']['total']).to eq(BigDecimal.new('3856.50'))
        end
      end

      context 'for automatic components' do
        before do
          @specification_1 = build(:specification, automatic: false, name: 'S1')
          @specification_1.values << build(:value, value: 'V1')
          @specification_1.values << build(:value, value: 'V2')
          @specification_1.values << build(:value, value: 'V3')
          @specification_1.save!

          @specification_2 = build(:specification, automatic: true, name: 'S2')
          @specification_2.values << build(:value, value: 'V4', comparison_value: 510)
          @specification_2.save!

          @category_1.update!(use_cut_table: true)
          @category_2.update!(use_cut_table: true)
          @category_3.update!(use_cut_table: true)

          @cut_table = create(:cut_table, size: 5000)

          @component_1 = create(:component, unit: :cm, cut_table: @cut_table, values: [@specification_1.values.first, @specification_1.values.last, @specification_2.values.first], category: @category_1, erasable: true, cost: BigDecimal.new('32.28'), fix_cost: BigDecimal.new('15'))
          @component_2 = create(:component, unit: :cm, cut_table: @cut_table, values: [@specification_1.values.first, @specification_1.values.last, @specification_2.values.first], category: @category_3, erasable: true)
          @component_3 = create(:component, unit: :cm, cut_table: @cut_table, values: [@specification_1.values.first, @specification_1.values.last, @specification_2.values.first], category: @category_2, erasable: true)

          @line_rule = create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], 
            total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*2',
            width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', quantity_consumption: '20', automatic: true)
          @rule = create(:rule, :line_rule => @line_rule, :component => @component_1, 
            control_method: Rule.control_methods[:width], width: 50..300, total_consumption: '_LARGURA_*2', 
            cost: '_CUSTO_BASE_*2', width_consumption: '_LARGURA_', 
            height_consumption: '_ALTURA_', quantity_consumption: '20', automatic: true)
        end
        
        let(:questions_string) { "{\"LOCAL_INSTALACAO\":\"banheiro\",\"MAO_DE_OBRA\":true,\"FRETE\":false}" }
        let(:parameters) { {
          'line_id' => @line.id, 'component_ids' => [@component_1.id], 'width' => "#{width}", 'height' => "#{height}", "questions" => questions_string
        } }

        it 'initializes a new simulation option with the line id, converted width and converted height' do
          expect(SimulationOption).to receive(:new).with(@line.id.to_s, converted_width, converted_height, JSON.parse(questions_string)).and_call_original

          xhr :get, :select, parameters
        end

        it 'returns ok status' do
          xhr :get, :select, parameters
          
          expect(response).to have_http_status(:ok)
        end

        it 'returns the component prices and total_consumption' do
          xhr :get, :select, parameters
          
          expect(json['component']['id']).to eq(@component_1.id)
          expect(json['component']['name']).to eq(@component_1.name)
          expect(json['component']['price']['total_consumption']).to eq(BigDecimal.new('20'))
          expect(json['component']['price']['width_consumption']).to eq(50)
          expect(json['component']['price']['height_consumption']).to eq(20)
          expect(json['component']['price']['quantity_consumption']).to eq(20)
          expect(json['component']['price']['cost'].round(2)).to eq(BigDecimal.new('645.6'))
          expect(json['component']['price']['price']).to eq(BigDecimal.new('726.30'))
          expect(json['component']['price']['total']).to eq(BigDecimal.new('14526'))
        end
      end
    end
  end

  describe 'GET export' do
    render_views

    context 'with success' do
      let(:exporter) { double(:exporter, export: 'foo/bar') }

      before do
        allow(ComponentExporter).to receive(:new).and_return(exporter)
      end

      it 'initializes the exporter' do
        expect(ComponentExporter).to receive(:new).and_return(exporter)

        xhr :get, :export
      end

      it 'exports the components' do
        expect(exporter).to receive(:export).and_return(true)

        xhr :get, :export
      end

      it 'has ok status' do
        xhr :get, :export

        expect(response).to have_http_status(:ok)
      end

      it 'renders the filepath' do
        xhr :get, :export

        expect(json["path"]).to eq('foo/bar')
      end
    end

    context 'with errors' do
      let(:exporter) { double(:exporter, export: nil) }

      before do
        allow(ComponentExporter).to receive(:new).and_return(exporter)
      end

      it 'has bad request status' do
        xhr :get, :export

        expect(response).to have_http_status(:bad_request)
      end

      it 'render nothing' do
        xhr :get, :export

        expect(response.body).to be_blank
      end
    end
  end

  describe 'POST import' do
    context 'with success' do
      let(:importer) { double(:importer, import: true, success: true, errors: []) }

      before do
        allow(ComponentImporter).to receive(:new).and_return(importer)
      end

      it 'initializes the importer with the file' do
        expect(ComponentImporter).to receive(:new).with('foo').and_return(importer)

        xhr :post, :upload, file: 'foo'
      end

      it 'imports the components' do
        expect(importer).to receive(:import)

        xhr :post, :upload, file: 'foo'
      end

      it 'has ok status' do
        xhr :post, :upload, file: 'foo'

        expect(response).to have_http_status(:ok)
      end

      context 'with no errors' do
        it 'returns success' do
          xhr :post, :upload, file: 'foo'

          expect(json["success"]).to eq(true)
          expect(json["errors"]).to be_empty
        end
      end

      context 'with errors' do
        let(:importer) { double(:importer, import: true, success: true, errors: ['foo', 'bar']) }

        it 'returns the success with the errors' do
          xhr :post, :upload, file: 'foo'

          expect(json["success"]).to eq(true)
          expect(json["errors"]).to match_array(['foo', 'bar'])
        end
      end
    end

    context 'with errors' do
      let(:importer) { double(:importer, import: true, success: false, errors: ['foo', 'bar']) }

      before do
        allow(ComponentImporter).to receive(:new).and_return(importer)
      end

      it 'has unprocessable entity status' do
        xhr :post, :upload, file: 'foo'

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :upload, file: 'foo'

        expect(json["success"]).to eq(false)
        expect(json["errors"]).to match_array(['foo', 'bar'])
      end
    end
  end

  describe 'PATCH clone_kit' do
    render_views

    before do
      @child_1 = create(:component, unit: :cm, name: 'C1', category: @category_1, families: [@family])
      @child_2 = create(:component, unit: :cm, name: 'C2', category: @category_2)
      @child_3 = create(:component, unit: :cm, name: 'C3', category: @category_3)
      @child_4 = create(:component, unit: :cm, name: 'C4', category: @category_3, erasable: false)

      @component   = create(:component, unit: :cm, category: @category_2, children: [@child_1, @child_4])
      @component_2 = create(:component, unit: :cm, category: @category_2, children: [])
    end

    context 'with invalid params' do
      let(:parameters) {{'component_id' => ''}.merge({'id' => "#{@component_2.id}"})}

      it 'returns unprocessable entity status' do
        xhr :patch, :clone_kit, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :patch, :clone_kit, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) {{'component_id' => "#{@component.id}"}}

      it 'initializes the component service' do
        expect(ComponentService).to receive(:new).with(parameters).and_call_original

        xhr :patch, :clone_kit, parameters.merge({'id' => "#{@component_2.id}"})
      end

      it 'clones the kit configuration' do
        expect_any_instance_of(ComponentService).to receive(:clone_kit).with(@component_2.id.to_s).and_call_original
        
        xhr :patch, :clone_kit, parameters.merge({'id' => "#{@component_2.id}"})
      end

      it 'returns ok status' do
        xhr :patch, :clone_kit, parameters.merge({'id' => "#{@component_2.id}"})
        
        expect(response).to have_http_status(:ok)
      end

      it 'returns nothing' do
        xhr :patch, :clone_kit, parameters.merge({'id' => "#{@component_2.id}"})

        expect(response.body).to be_blank
      end
    end
  end

  describe 'POST batch_insert' do
    render_views

    before do
      @component = create(:component, unit: :cm, name: 'C1', category: @category_1, families: [@family])
    end

    context 'with invalid params' do
      let(:parameters) {{'rule_ids' => ''}.merge({'id' => "#{@component.id}"})}

      it 'returns unprocessable entity status' do
        xhr :post, :batch_insert, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :batch_insert, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) {{'rule_ids' => ["#{@line_rule_1.id}", "#{@line_rule_2.id}"]}}

      before do
        @line_1 = create(:line, family: @family, name: "L1", active: true)
        @line_rule_1 = create(:line_rule, line: @line_1, control_method: LineRule.control_methods[:width],
          total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_', 
          width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', automatic: true, category_id: @category_1.id)

        @line_2 = create(:line, family: @family, name: "L2", active: true)
        @line_rule_2 = create(:line_rule, line: @line_2, control_method: LineRule.control_methods[:width],
          total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_',
          width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', automatic: true, category_id: @category_1.id)

        allow_any_instance_of(ComponentService).to receive(:errors).and_return(['Erro ao criar regra 1'])
      end

      it 'initializes the component service' do
        expect(ComponentService).to receive(:new).with(parameters).and_call_original

        xhr :post, :batch_insert, parameters.merge({'id' => "#{@component.id}"})
      end

      it 'creates rules for the component' do
        expect_any_instance_of(ComponentService).to receive(:create_rules).with(@component.id.to_s).and_call_original
        
        xhr :post, :batch_insert, parameters.merge({'id' => "#{@component.id}"})
      end

      it 'returns ok status' do
        xhr :post, :batch_insert, parameters.merge({'id' => "#{@component.id}"})
        
        expect(response).to have_http_status(:ok)
      end

      it 'returns the possible create errors' do
        xhr :post, :batch_insert, parameters.merge({'id' => "#{@component.id}"})

        expect(json['errors'][0]).to eq('Erro ao criar regra 1')
      end
    end
  end

  describe 'PATCH activate' do
    before do
      @component = create(:component, unit: :cm, name: 'C1', category: @category_1, families: [@family])
    end

    context 'with invalid params' do
      let(:service) { double(:service, activate: false, success: false, errors: ['foo']) }

      before do
        allow(ComponentService).to receive(:new).and_return(service)
      end

      it 'does not change the component status' do
        expect {
          xhr :patch, :activate, { 'id' => "#{@component.id}" }
        }.to_not change(@component, :deleted?)
      end

      it 'returns unprocessable entity status' do
        xhr :patch, :activate, { 'id' => "#{@component.id}" }
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :patch, :activate, { 'id' => "#{@component.id}" }
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      it 'initializes the component service' do
        expect(ComponentService).to receive(:new).and_call_original

        xhr :patch, :activate, { 'id' => "#{@component.id}" }
      end

      it 'activates the component' do
        expect_any_instance_of(ComponentService).to receive(:activate).with(@component.id.to_s).and_call_original
        
        xhr :patch, :activate, { 'id' => "#{@component.id}" }
      end

      it 'returns ok status' do
        xhr :patch, :activate, { 'id' => "#{@component.id}" }
        
        expect(response).to have_http_status(:ok)
      end

      it 'renders nothing' do
        xhr :patch, :activate, { 'id' => "#{@component.id}" }

        expect(response.body).to be_blank
      end
    end
  end
end
