# == Schema Information
#
# Table name: states
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  acronym    :string           not null
#  ibge_code  :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe State, type: :model do
  describe "associations" do
    it { should have_many(:cities).dependent(:destroy) }
  end

  context "validation" do
    it "should invalidate if name is not present" do
      expect(FactoryGirl.build(:state, :name => nil)).to be_invalid
    end

    it "should invalidate if name is blank" do
      expect(FactoryGirl.build(:state, :name => "")).to be_invalid
    end

    it "should invalidate if acronym is not present" do
      expect(FactoryGirl.build(:state, :acronym => nil)).to be_invalid
    end

    it "should invalidate if acronym is blank" do
      expect(FactoryGirl.build(:state, :acronym => "",)).to be_invalid
    end

    it "should invalidate if ibge_code is not present" do
      expect(FactoryGirl.build(:state, :ibge_code => nil)).to be_invalid
    end

    it "should invalidate if ibge_code is blank" do
      expect(FactoryGirl.build(:state, :ibge_code => "")).to be_invalid
    end
  end
end
