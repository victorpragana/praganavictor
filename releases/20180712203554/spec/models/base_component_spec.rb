# == Schema Information
#
# Table name: base_components
#
#  id                        :integer          not null, primary key
#  name                      :string(255)      not null
#  cost                      :decimal(10, 5)   default(0.0), not null
#  category_id               :integer
#  characteristics           :text
#  code                      :string(20)
#  unit                      :integer          default(2), not null
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  deleted_at                :datetime
#  reference_cost            :decimal(10, 5)   default(0.0)
#  offset_left               :integer          default(0)
#  offset_top                :integer          default(0)
#  offset_right              :integer          default(0)
#  offset_bottom             :integer          default(0)
#  allow_rotation            :boolean          default(FALSE)
#  exchange                  :decimal(10, 5)
#  loss                      :decimal(10, 5)
#  cut_table_id              :integer
#  print_on_lines_label      :boolean          default(FALSE)
#  fix_cost                  :decimal(10, 5)   default(0.0)
#  print_on_components_label :boolean          default(FALSE)
#

require 'rails_helper'

RSpec.describe BaseComponent, type: :model do
  before(:each) do
    @category = FactoryGirl.create(:category, use_cut_table: false)
  end

  context 'associations' do
    it { should have_and_belong_to_many(:families) }
    it { should have_many(:components).dependent(:destroy) }
    it { should have_many(:values).class_name('BaseComponentValue').dependent(:destroy) }
    it { should belong_to(:category) }
    it { should belong_to(:cut_table) }
  end

  describe 'conversions' do
    it { expect(described_class.new).to respond_to(:offset_left_from_mm_to_cm) }
    it { expect(described_class.new).to respond_to(:offset_top_from_mm_to_cm) }
    it { expect(described_class.new).to respond_to(:offset_right_from_mm_to_cm) }
    it { expect(described_class.new).to respond_to(:offset_bottom_from_mm_to_cm) }
  end

  context 'validation' do
    it 'fails if the name is not present' do
      expect(FactoryGirl.build(:base_component, name: nil, category: @category)).to be_invalid
    end

    it 'fails if the name is blank' do
      expect(FactoryGirl.build(:base_component, name: '', category: @category)).to be_invalid
    end

    it 'fails if the name length is greater than 255 characters' do
      expect(FactoryGirl.build(:base_component, name: Faker::Lorem.characters(256), category: @category)).to be_invalid
    end

    it 'fails if the name is not unique for the same category' do
      FactoryGirl.create(:base_component, name: 'abc', category: @category)

      expect(FactoryGirl.build(:base_component, name: 'AbC', category: @category)).to be_invalid
    end

    it 'fails if category is not present' do
      expect(FactoryGirl.build(:base_component, category: nil)).to be_invalid
    end

    it 'fails if the cost is not present' do
      expect(FactoryGirl.build(:base_component, cost: nil, category: @category)).to be_invalid
    end

    it 'fails if the cost is blank' do
      expect(FactoryGirl.build(:base_component, cost: '', category: @category)).to be_invalid
    end

    it 'fails if the cost is negative' do
      expect(FactoryGirl.build(:base_component, cost: BigDecimal.new('-0.01'), category: @category)).to be_invalid
    end

    it 'fails if the code length is greater than 20 characters' do
      expect(FactoryGirl.build(:base_component, code: Faker::Lorem.characters(21), category: @category)).to be_invalid
    end

    it 'fails if the unit is not present' do
      expect(FactoryGirl.build(:base_component, unit: nil, category: @category)).to be_invalid
    end

    it 'fails if the unit is blank' do
      expect(FactoryGirl.build(:base_component, unit: '', category: @category)).to be_invalid
    end

    it 'fails if the reference cost is negative' do
      expect(FactoryGirl.build(:base_component, reference_cost: BigDecimal.new('-0.01'), category: @category)).to be_invalid
    end

    it 'fails if the fix cost is negative' do
      expect(FactoryGirl.build(:base_component, fix_cost: BigDecimal.new('-0.01'), category: @category)).to be_invalid
    end

    it 'fails if the offset_left is negative' do
      expect(FactoryGirl.build(:base_component, offset_left: -1, category: @category)).to be_invalid
    end

    it 'fails if the offset_left is not an integer' do
      expect(FactoryGirl.build(:base_component, offset_left: BigDecimal.new('0.1'), category: @category)).to be_invalid
    end

    it 'fails if the offset_top is negative' do
      expect(FactoryGirl.build(:base_component, offset_top: -1, category: @category)).to be_invalid
    end

    it 'fails if the offset_top is not an integer' do
      expect(FactoryGirl.build(:base_component, offset_top: BigDecimal.new('0.1'), category: @category)).to be_invalid
    end

    it 'fails if the offset_right is negative' do
      expect(FactoryGirl.build(:base_component, offset_right: -1, category: @category)).to be_invalid
    end

    it 'fails if the offset_right is not an integer' do
      expect(FactoryGirl.build(:base_component, offset_right: BigDecimal.new('0.1'), category: @category)).to be_invalid
    end

    it 'fails if the offset_bottom is negative' do
      expect(FactoryGirl.build(:base_component, offset_bottom: -1, category: @category)).to be_invalid
    end

    it 'fails if the offset_bottom is not an integer' do
      expect(FactoryGirl.build(:base_component, offset_bottom: BigDecimal.new('0.1'), category: @category)).to be_invalid
    end

    context 'with cut table' do
      before do
        @cut_table = FactoryGirl.create(:cut_table)
      end

      it 'fails if it has cut table but the category does not allow the usage' do
        expect(FactoryGirl.build(:base_component, category: @category, cut_table: @cut_table)).to be_invalid
      end

      it 'fails if it the category uses a cut table but it is not present' do
        @category.update!(use_cut_table: true)

        expect(FactoryGirl.build(:base_component, category: @category, cut_table: nil)).to be_invalid
      end
    end

    context 'with automatic values' do
      before(:each) do
        @specification_1 = FactoryGirl.build(:specification, automatic: true)
        @specification_1.values << FactoryGirl.build(:value, value: 'V1', comparison_value: 1)
        @specification_1.values << FactoryGirl.build(:value, value: 'V2', comparison_value: 2)
        @specification_1.save!

        @specification_2 = FactoryGirl.build(:specification, automatic: true)
        @specification_2.values << FactoryGirl.build(:value, value: 'V3', comparison_value: 3)
        @specification_2.save!

        @specification_3 = FactoryGirl.build(:specification, automatic: false)
        @specification_3.values << FactoryGirl.build(:value, value: 'V4')
        @specification_3.save!
      end

      it 'fails if it has automatic values for different specifications' do
        expect(FactoryGirl.build(:base_component, category: @category, values: [BaseComponentValue.new(value_id: @specification_3.values[0].id), BaseComponentValue.new(value_id: @specification_1.values.first.id), BaseComponentValue.new(value_id: @specification_2.values.first.id)])).to be_invalid
      end

      it 'passes if it has automatic values for the same specification' do
        expect(FactoryGirl.build(:base_component, category: @category, values: [BaseComponentValue.new(value_id: @specification_3.values[0].id), BaseComponentValue.new(value_id: @specification_1.values[0].id), BaseComponentValue.new(value_id: @specification_1.values[1].id)])).to be_valid
      end
    end

    it 'shoud pass' do
      expect(FactoryGirl.build(:base_component, category: @category)).to be_valid
    end

    it 'passes if the name is not unique for the different category' do
      FactoryGirl.create(:base_component, name: 'abc', category: @category)

      expect(FactoryGirl.build(:base_component, name: 'AbC', category: FactoryGirl.create(:category))).to be_valid
    end

    it 'passes if the code length is 0' do
      expect(FactoryGirl.build(:base_component, code: nil, category: @category)).to be_valid
    end
  end
end
