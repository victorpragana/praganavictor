# == Schema Information
#
# Table name: third_parties
#
#  id                     :integer          not null, primary key
#  name                   :string           not null
#  rg                     :string
#  issuing_entity         :string
#  identification         :string
#  zip_code               :string
#  street                 :string
#  number                 :string
#  complement             :string
#  phone                  :string
#  email                  :string
#  corporate_name         :string
#  deleted_at             :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  actable_id             :integer
#  actable_type           :string
#  state_inscription      :string
#  municipal_inscription  :string
#  city_id                :integer
#  neighborhood           :string
#  subdomain              :string
#  logo_file_name         :string
#  logo_content_type      :string
#  logo_file_size         :integer
#  logo_updated_at        :datetime
#  current_account        :string
#  agency                 :string
#  bank                   :integer
#  allow_special_discount :boolean
#  special_discount       :decimal(5, 2)    default(0.0)
#

require 'rails_helper'

RSpec.describe ThirdParty, type: :model do
  describe 'associations' do
    it { should belong_to(:city) }
    it { should delegate_method(:state).to(:city) }
    it { expect(described_class).to be_actable }
  end

  describe 'enums' do
    it { should define_enum_for(:bank)
      .with({
        bradesco: 237,
        citibank: 745,
        brasil: 1,
        itau: 341,
        safra: 422,
        banrisul: 41,
        cef: 104,
        hsbc: 399,
        santander: 33,
        sicredi:  748,
        inter:  77,
        sicoob: 756,
        agibank: 121
      })
    }
  end

  describe 'logo' do
    it { should have_attached_file(:logo) }
    it {
      should validate_attachment_content_type(:logo)
        .allowing('image/png', 'image/gif')
        .rejecting('text/plain', 'text/xml')
    }
    it 'has default url value equals to "/images/logo-lg.png"' do
      expect(build(:third_party).logo.url).to eq('/images/logo-lg.png')
    end
  end

  describe 'pre validation' do
    context 'when the special discount is not allowed' do
      before do
        @third_party = build(:third_party, allow_special_discount: false, special_discount: 10)

        @third_party.valid?
      end

      it 'sets the special discount to nil' do
        expect(@third_party.special_discount).to be_nil
      end
    end

    context 'when the third party is a customer' do
      before do
        @third_party = build(:third_party, actable_type: 'Customer', agency: '1234', current_account: '12345-5', bank: 237)

        @third_party.valid?
      end

      it 'removes agency information' do
        expect(@third_party.agency).to be_nil
      end

      it 'removes current account information' do
        expect(@third_party.current_account).to be_nil
      end

      it 'removes bank information' do
        expect(@third_party.bank).to be_nil
      end
    end

    context 'when the third party is not a customer' do
      before do
        @third_party = build(:third_party, actable_type: ['Distributor', 'Store'], agency: '1234', current_account: '12345-5', bank: 237)

        @third_party.valid?
      end

      it 'maintains agency information' do
        expect(@third_party.agency).to eq('1234')
      end

      it 'maintains current account information' do
        expect(@third_party.current_account).to eq('12345-5')
      end

      it 'maintains bank information' do
        expect(@third_party.bank).to eq("bradesco")
      end
    end
  end

  context 'validation' do
    it 'fails if the name is not present' do
      expect(build(:third_party, name: nil)).to be_invalid
    end

    it 'fails if the name is blank' do
      expect(build(:third_party, name: '')).to be_invalid
    end

    it 'fails if the identification is not present' do
      expect(build(:third_party, identification: nil)).to be_invalid
    end

    it 'fails if the identification is blank' do
      expect(build(:third_party, identification: '')).to be_invalid
    end

    it 'fails if the identification is not a valid CNPJ' do
      expect(build(:third_party, identification: '33828441000132')).to be_invalid
    end

    it 'fails if the identification is not a valid CPF' do
      expect(build(:third_party, identification: '66203533808')).to be_invalid
    end

    it 'fails if the street is not present' do
      expect(build(:third_party, street: nil)).to be_invalid
    end

    it 'fails if the street is blank' do
      expect(build(:third_party, street: '')).to be_invalid
    end

    it 'fails if the number is not present' do
      expect(build(:third_party, number: nil)).to be_invalid
    end

    it 'fails if the number is blank' do
      expect(build(:third_party, number: '')).to be_invalid
    end

    it 'fails if the phone is not present' do
      expect(build(:third_party, phone: nil)).to be_invalid
    end

    it 'fails if the phone is blank' do
      expect(build(:third_party, phone: '')).to be_invalid
    end

    it 'fails if the subdomain is not present and actable_type Store' do
      expect(build(:third_party, subdomain: nil)).to be_invalid
    end

    it 'fails if the subdomain is blank and actable_type Store' do
      expect(build(:third_party, subdomain: '')).to be_invalid
    end

    it 'fails if subdomain already exists and actable_type Store' do
      create(:third_party, subdomain: 'store-d1')
      expect(build(:third_party, subdomain: 'store-d1')).to be_invalid
    end

    it 'fails if subdomain has spaces' do
      expect(build(:third_party, subdomain: 'Fabrica Suzano')).to be_invalid
    end

    it 'fails if subdomain has invalid characters' do
      expect(build(:third_party, subdomain: 'fábrica-suzano')).to be_invalid
    end

    context 'for Customer' do 
      subject { build(:third_party, actable_type: 'Customer') }
      it 'is valid if identification is not present' do 
        subject.identification = nil;
        expect(subject).to be_valid
      end
      it 'is valid if street is not present' do 
        subject.street = nil;
        expect(subject).to be_valid
      end
      it 'is valid if street is blank' do 
        subject.street = '';
        expect(subject).to be_valid
      end
      it 'is valid if number is not present' do 
        subject.number = nil;
        expect(subject).to be_valid
      end
      it 'is valid if number is blank' do 
        subject.number = '';
        expect(subject).to be_valid
      end
      it 'is valid if phone is not present' do 
        subject.phone = nil;
        expect(subject).to be_valid
      end
      it 'is valid if phone is blank' do 
        subject.phone = '';
        expect(subject).to be_valid
      end
      it 'is valid if subdomain is not present' do 
        subject.subdomain = nil;
        expect(subject).to be_valid
      end
      it 'is valid if subdomain is blank' do 
        subject.subdomain = '';
        expect(subject).to be_valid
      end
    end

    context 'for distributor' do
      context 'when the agency information is present' do
        it 'fails if the current account is not present' do
          expect(build(:third_party, actable_type: 'Distributor', agency: '1234', current_account: nil, bank: :brasil)).to be_invalid
        end

        it 'fails if the bank is not present' do
          expect(build(:third_party, actable_type: 'Distributor', agency: '1234', current_account: '1234-5', bank: nil)).to be_invalid
        end
      end

      context 'when the current account information is present' do
        it 'fails if the agency is not present' do
          expect(build(:third_party, actable_type: 'Distributor', agency: nil, current_account: '1234-5', bank: :brasil)).to be_invalid
        end

        it 'fails if the bank is not present' do
          expect(build(:third_party, actable_type: 'Distributor', agency: '1234', current_account: '1234-5', bank: nil)).to be_invalid
        end
      end

      context 'when the bank information is present' do
        it 'fails if the current account is not present' do
          expect(build(:third_party, actable_type: 'Distributor', agency: '1234', current_account: nil, bank: :brasil)).to be_invalid
        end

        it 'fails if the agency is not present' do
          expect(build(:third_party, actable_type: 'Distributor', agency: nil, current_account: '1234-5', bank: :brasil)).to be_invalid
        end
      end
    end

    context 'for store' do
      it 'fails if subdomain has spaces' do
        expect(build(:third_party, subdomain: 'Fabrica Suzano', actable_type: 'Store')).to be_invalid
      end

      it 'fails if subdomain has invalid characters' do
        expect(build(:third_party, subdomain: 'fábrica-suzano', actable_type: 'Store')).to be_invalid
      end
      context 'when the agency information is present' do
        it 'fails if the current account is not present' do
          expect(build(:third_party, actable_type: 'Store', agency: '1234', current_account: nil, bank: :brasil)).to be_invalid
        end

        it 'fails if the bank is not present' do
          expect(build(:third_party, actable_type: 'Store', agency: '1234', current_account: '1234-5', bank: nil)).to be_invalid
        end
      end

      context 'when the current account information is present' do
        it 'fails if the agency is not present' do
          expect(build(:third_party, actable_type: 'Store', agency: nil, current_account: '1234-5', bank: :brasil)).to be_invalid
        end

        it 'fails if the bank is not present' do
          expect(build(:third_party, actable_type: 'Store', agency: '1234', current_account: '1234-5', bank: nil)).to be_invalid
        end
      end

      context 'when the bank information is present' do
        it 'fails if the current account is not present' do
          expect(build(:third_party, actable_type: 'Store', agency: '1234', current_account: nil, bank: :brasil)).to be_invalid
        end

        it 'fails if the agency is not present' do
          expect(build(:third_party, actable_type: 'Store', agency: nil, current_account: '1234-5', bank: :brasil)).to be_invalid
        end
      end
    end

    context 'when special discount is allowed' do
      it 'fails if the special discount is negative' do
        expect(build(:third_party, allow_special_discount: true, special_discount: BigDecimal.new('-0.01'))).to be_invalid
      end

      it 'fails if the special discount is greater than 100' do
        expect(build(:third_party, allow_special_discount: true, special_discount: BigDecimal.new('100.01'))).to be_invalid
      end
    end

    it { expect(build(:third_party)).to be_valid }
  end

  context '.formated_identification' do
    context 'without identification' do
      before(:each) do
        @third_party = build(:third_party, identification: nil)
      end

      it 'should return a blank string' do
        expect(@third_party.formated_identification).to eq('')
      end
    end

    context 'with cpf identification' do
      before(:each) do
        @third_party = build(:third_party, identification: '97078202686')
      end

      it 'should return the formated cpf' do
        expect(@third_party.formated_identification).to eq('970.782.026-86')
      end
    end

    context 'with cnpj identification' do
      before(:each) do
        @third_party = build(:third_party, identification: '46734147000106')
      end

      it 'should return the formated cnpj' do
        expect(@third_party.formated_identification).to eq('46.734.147/0001-06')
      end
    end
  end

  context '.has_address?' do
    it 'return false when there is not zip_code' do
      expect(build(:third_party, zip_code: nil).has_address?).to eq(false)
    end

    it 'return false when there is not street' do
      expect(build(:third_party, street: nil).has_address?).to eq(false)
    end

    it 'return false when there is not number' do
      expect(build(:third_party, number: nil).has_address?).to eq(false)
    end

    it 'return false when there is not neighborhood' do
      expect(build(:third_party, neighborhood: nil).has_address?).to eq(false)
    end

    it 'return true when there is zip_code, street and number' do
      expect(build(:third_party).has_address?).to eq(true)
    end
  end
end
