# == Schema Information
#
# Table name: orders
#
#  id                     :integer          not null, primary key
#  date                   :date             not null
#  observation            :text
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  seller_id              :integer
#  seller_type            :string
#  client_id              :integer
#  client_type            :string
#  discount               :decimal(5, 2)    default(0.0)
#  status                 :integer          default(0), not null
#  erp_id                 :integer
#  processing_messages    :text             default([]), is an Array
#  user_id                :integer
#  billing_client_id      :integer
#  billing_client_type    :string
#  special_discount       :decimal(5, 2)
#  production_status      :integer          default(0), not null
#  sent_to_production_at  :datetime
#  started_production_at  :datetime
#  finished_production_at :datetime
#  dispatched_at          :datetime
#  received_at            :datetime
#  sent_to_factory_at     :datetime
#

require 'rails_helper'

RSpec.describe Order, type: :model do
  describe 'associations' do
    it { is_expected.to have_many(:order_items).dependent(:destroy) }
    it { is_expected.to have_many(:items).class_name(OrderLooseItem).dependent(:destroy) }
    it { is_expected.to have_many(:components).through(:order_items) }
    it { is_expected.to have_many(:packing_configurations) }
    it { is_expected.to have_many(:movements) }
    it { is_expected.to have_one(:estimate).dependent(:nullify) }
    it { is_expected.to belong_to(:seller) }
    it { is_expected.to belong_to(:client) }
    it { is_expected.to belong_to(:billing_client) }
    it { is_expected.to belong_to(:user) }
  end

  describe 'enums' do
    it do
      is_expected.to define_enum_for(:status).with(
        to_send:  0,
        sending:  1,
        sent:     2,
        error:    3
      )
    end

    it do
      is_expected.to define_enum_for(:production_status).with(
        not_started:  0,
        sent_to_production: 1,
        in_production: 2,
        produced: 3,
        dispatched: 4,
        received: 5,
        sent_to_factory: 6
      )
    end
  end

  describe 'transitions' do
    before do
      @factory     = create(:factory, name: 'F1')
      @distributor = create(:distributor, name: 'D1', allow_special_discount: true, special_discount: 100)
      @store       = create(:store, distributor: @distributor, name: 'S1')
      @customer    = create(:customer, store_id: @store.id, name: 'C1')

      @user        = create(:administrator, username: 'F1').tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: @store, user: administrator))
      end

      now = Time.zone.now

      allow(Time.zone).to receive(:now).and_return(now)
    end

    context 'for a \'not_started\' order' do
      subject { create(:order, seller: @store, client: @customer, billing_client: @customer, user: @user, production_status: :not_started) }

      before do
        allow(subject).to receive(:factory_total_with_discount).and_return(1500)
        allow(subject).to receive(:distributor_total_with_discount).and_return(1800)
        allow(subject).to receive(:store_total_with_discount).and_return(2000)
      end
    
      it { expect(subject).to transition_from(:not_started).to(:sent_to_factory).on_event(:send_to_factory) }

      it 'updates the sent_to_factory_at data' do
        expect { subject.send_to_factory! }.to change(subject, :sent_to_factory_at).from(nil).to(Time.zone.now)
      end

      context 'when the seller is the factory' do
        before do
          subject.update!(seller: @factory)
        end

        it 'does not create any movement' do
          expect {
            subject.send_to_factory!
          }.to_not change(Movement, :count)
        end
      end

      context 'when the seller is a distributor' do
        before do
          subject.update!(seller: @distributor)
        end

        it 'does not create any movement' do
          expect {
            subject.send_to_factory!
          }.to_not change(Movement, :count)
        end
      end

      context 'when the seller is a store' do
        before do
          subject.update!(seller: @store)
        end

        it 'creates an output production credit' do
          expect {
            subject.send_to_factory!
          }.to change(ProductionCredit.output, :count).by(1)
        end

        it 'creates an output \'send_to_factory\' movement from the order\'s third party to the store\'s distributor with the store total with discount' do
          subject.send_to_factory!
          
          output = ProductionCredit.output.last

          expect(output.date).to be_within(1.second).of(Time.zone.now)
          expect(output.owner).to eq(subject.seller.third_party)
          expect(output.destination).to eq(subject.seller.distributor.third_party)
          expect(output.order).to eq(subject)
          expect(output.value).to eq(-1 * subject.store_total_with_discount)
          expect(output.send_to_factory?).to be_truthy
        end

        it 'creates an input production credit' do
          expect {
            subject.send_to_factory!
          }.to change(ProductionCredit.input, :count).by(1)
        end

        it 'creates an input \'send_to_factory\' movement from the store\'s distributor the factory with the distributor total with discount' do
          subject.send_to_factory!
          
          input = ProductionCredit.input.last

          expect(input.date).to be_within(1.second).of(Time.zone.now)
          expect(input.owner).to eq(subject.seller.distributor.third_party)
          expect(input.destination).to eq(@factory.third_party)
          expect(input.order).to eq(subject)
          expect(input.value).to eq(subject.distributor_total_with_discount)
          expect(input.send_to_factory?).to be_truthy
        end
      end
    end

    context 'for a \'sent_to_factory\' order' do
      subject { create(:order, seller: @store, client: @customer, billing_client: @customer, user: @user, production_status: :sent_to_factory, sent_to_factory_at: Time.zone.now) }

      before do
        allow(subject).to receive(:factory_total_with_discount).and_return(1500)
        allow(subject).to receive(:distributor_total_with_discount).and_return(1800)
        allow(subject).to receive(:store_total_with_discount).and_return(2000)
      end
    
      it { expect(subject).to transition_from(:sent_to_factory).to(:sent_to_production).on_event(:send_to_production) }
      
      it 'updates the sent_to_production_at data' do
        expect { subject.send_to_production! }.to change(subject, :sent_to_production_at).from(nil).to(Time.zone.now)
      end

      context 'when the seller is the factory' do
        before do
          subject.update!(seller: @factory)
        end

        it 'does not create any movement' do
          expect {
            subject.send_to_production!
          }.to_not change(Movement, :count)
        end
      end

      context 'when the seller is a distributor' do
        before do
          subject.update!(seller: @distributor)
        end

        it 'creates an output production credit' do
          expect {
            subject.send_to_production!
          }.to change(ProductionCredit.output, :count).by(1)
        end

        it 'creates an output \'order_production\' movement from the order\'s third party to the factory with the distributor total with discount' do
          subject.send_to_production!
          
          output = ProductionCredit.output.last

          expect(output.date).to be_within(1.second).of(Time.zone.now)
          expect(output.owner).to eq(subject.seller.third_party)
          expect(output.destination).to eq(@factory.third_party)
          expect(output.order).to eq(subject)
          expect(output.value).to eq(-1 * subject.distributor_total_with_discount)
          expect(output.order_production?).to be_truthy
        end
      end

      context 'when the seller is a store' do
        before do
          subject.update!(seller: @store)
        end

        it 'creates an output production credit' do
          expect {
            subject.send_to_production!
          }.to change(ProductionCredit.output, :count).by(1)
        end

        it 'creates an output \'order_production\' movement from the store\'s distributor to the factory with the distributor total with discount' do
          subject.send_to_production!
          
          output = ProductionCredit.output.last

          expect(output.date).to be_within(1.second).of(Time.zone.now)
          expect(output.owner).to eq(subject.seller.distributor.third_party)
          expect(output.destination).to eq(@factory.third_party)
          expect(output.order).to eq(subject)
          expect(output.value).to eq(-1 * subject.distributor_total_with_discount)
          expect(output.order_production?).to be_truthy
        end
      end

      describe 'cancel sending to factory' do
        before do
          allow(subject).to receive(:factory_total_with_discount).and_return(1500)
          allow(subject).to receive(:distributor_total_with_discount).and_return(1800)
          allow(subject).to receive(:store_total_with_discount).and_return(2000)
        end
        
        it { expect(subject).to transition_from(:sent_to_factory).to(:not_started).on_event(:cancel_send_to_factory) }

        it 'updates the sent_to_factory_at data to nil' do
          expect { subject.cancel_send_to_factory! }.to change(subject, :sent_to_factory_at).from(subject.sent_to_factory_at).to(nil)
        end

        context 'when the seller is the factory' do
          before do
            subject.update!(seller: @factory)
          end

          it 'does not create any movement' do
            expect {
              subject.cancel_send_to_factory!
            }.to_not change(Movement, :count)
          end
        end

        context 'when the seller is a distributor' do
          before do
            subject.update!(seller: @distributor)
          end

          it 'does not create any movement' do
            expect {
              subject.cancel_send_to_factory!
            }.to_not change(Movement, :count)
          end
        end

        context 'when the seller is a store' do
          before do
            subject.update!(seller: @store)
          end

          it 'creates an input production credit' do
            expect {
              subject.cancel_send_to_factory!
            }.to change(ProductionCredit.input, :count).by(1)
          end

          it 'creates an input \'cancel_send_to_factory\' movement from the order\'s third party to the store\'s distributor with the store total with discount' do
            subject.cancel_send_to_factory!
            
            input = ProductionCredit.input.last

            expect(input.date).to be_within(1.second).of(Time.zone.now)
            expect(input.owner).to eq(subject.seller.third_party)
            expect(input.destination).to eq(subject.seller.distributor.third_party)
            expect(input.order).to eq(subject)
            expect(input.value).to eq(subject.store_total_with_discount)
            expect(input.cancel_send_to_factory?).to be_truthy
          end

          it 'creates an output production credit' do
            expect {
              subject.cancel_send_to_factory!
            }.to change(ProductionCredit.output, :count).by(1)
          end

          it 'creates an output \'cancel_send_to_factory\' movement from the store\'s distributor the factory with the distributor total with discount' do
            subject.cancel_send_to_factory!
            
            output = ProductionCredit.output.last

            expect(output.date).to be_within(1.second).of(Time.zone.now)
            expect(output.owner).to eq(subject.seller.distributor.third_party)
            expect(output.destination).to eq(@factory.third_party)
            expect(output.order).to eq(subject)
            expect(output.value).to eq(-1 * subject.distributor_total_with_discount)
            expect(output.cancel_send_to_factory?).to be_truthy
          end
        end
      end
    end

    context 'for a \'sent_to_production\' order' do
      subject { create(:order, seller: @store, client: @customer, billing_client: @customer, user: @user, production_status: :sent_to_production, sent_to_production_at: Time.zone.now) }
    
      it { expect(subject).to transition_from(:sent_to_production).to(:in_production).on_event(:produce) }
      
      it 'updates the started_production_at data' do
        expect { subject.produce! }.to change(subject, :started_production_at).from(nil).to(Time.zone.now)
      end

      describe 'cancel sending to production' do
        before do
          allow(subject).to receive(:factory_total_with_discount).and_return(1500)
          allow(subject).to receive(:distributor_total_with_discount).and_return(1800)
          allow(subject).to receive(:store_total_with_discount).and_return(2000)
        end
        
        it { expect(subject).to transition_from(:sent_to_production).to(:sent_to_factory).on_event(:cancel_send_to_production) }

        it 'updates the sent_to_production_at data to nil' do
          expect { subject.cancel_send_to_production! }.to change(subject, :sent_to_production_at).from(subject.sent_to_production_at).to(nil)
        end

        context 'when the seller is the factory' do
          before do
            subject.update!(seller: @factory)
          end

          it 'does not create any movement' do
            expect {
              subject.cancel_send_to_production!
            }.to_not change(Movement, :count)
          end
        end

        context 'when the seller is a distributor' do
          before do
            subject.update!(seller: @distributor)
          end

          it 'creates an input production credit' do
            expect {
              subject.cancel_send_to_production!
            }.to change(ProductionCredit.input, :count).by(1)
          end

          it 'creates an input \'cancel_order_production\' movement from the order\'s third party to the factory with the distributor total with discount' do
            subject.cancel_send_to_production!
            
            input = ProductionCredit.input.last

            expect(input.date).to be_within(1.second).of(Time.zone.now)
            expect(input.owner).to eq(subject.seller.third_party)
            expect(input.destination).to eq(@factory.third_party)
            expect(input.order).to eq(subject)
            expect(input.value).to eq(subject.distributor_total_with_discount)
            expect(input.cancel_order_production?).to be_truthy
          end
        end

        context 'when the seller is a store' do
          before do
            subject.update!(seller: @store)
          end

          it 'creates an input production credit' do
            expect {
              subject.cancel_send_to_production!
            }.to change(ProductionCredit.input, :count).by(1)
          end

          it 'creates an input \'cancel_order_production\' movement from the store\'s distributor to the factory with the distributor total with discount' do
            subject.cancel_send_to_production!
            
            input = ProductionCredit.input.last

            expect(input.date).to be_within(1.second).of(Time.zone.now)
            expect(input.owner).to eq(subject.seller.distributor.third_party)
            expect(input.destination).to eq(@factory.third_party)
            expect(input.order).to eq(subject)
            expect(input.value).to eq(subject.distributor_total_with_discount)
            expect(input.cancel_order_production?).to be_truthy
          end
        end
      end
    end

    context 'for a \'in_production\' order' do
      subject { create(:order, seller: @store, client: @customer, billing_client: @customer, user: @user, production_status: :in_production, started_production_at: Time.zone.now) }
    
      it { expect(subject).to transition_from(:in_production).to(:produced).on_event(:finish) }

      it 'updates the finished_production_at data' do
        expect { subject.finish! }.to change(subject, :finished_production_at).from(nil).to(Time.zone.now)
      end

      it { expect(subject).to transition_from(:in_production).to(:sent_to_production).on_event(:cancel_produce) }

      it 'updates the started_production_at data to nil' do
        expect { subject.cancel_produce! }.to change(subject, :started_production_at).from(subject.started_production_at).to(nil)
      end
    end

    context 'for a \'produced\' order' do
      subject { create(:order, seller: @store, client: @customer, billing_client: @customer, user: @user, production_status: :produced, finished_production_at: Time.zone.now) }
    
      it { expect(subject).to transition_from(:produced).to(:dispatched).on_event(:dispatch) }

      it 'updates the dispatched_at data' do
        expect { subject.dispatch! }.to change(subject, :dispatched_at).from(nil).to(Time.zone.now)
      end

      it { expect(subject).to transition_from(:produced).to(:in_production).on_event(:cancel_finish) }

      it 'updates the finished_production_at data to nil' do
        expect { subject.cancel_finish! }.to change(subject, :finished_production_at).from(subject.finished_production_at).to(nil)
      end
    end

    context 'for a \'dispatched\' order' do
      subject { create(:order, seller: @store, client: @customer, billing_client: @customer, user: @user, production_status: :dispatched, dispatched_at: Time.zone.now) }
    
      it { expect(subject).to transition_from(:dispatched).to(:received).on_event(:receive) }

      it 'updates the received_at data' do
        expect { subject.receive! }.to change(subject, :received_at).from(nil).to(Time.zone.now)
      end

      it { expect(subject).to transition_from(:dispatched).to(:produced).on_event(:cancel_dispatch) }

      it 'updates the dispatched_at data to nil' do
        expect { subject.cancel_dispatch! }.to change(subject, :dispatched_at).from(subject.dispatched_at).to(nil)
      end
    end

    context 'for a \'received\' order' do
      subject { create(:order, seller: @store, client: @customer, billing_client: @customer, user: @user, production_status: :received, received_at: Time.zone.now) }

      it { expect(subject).to transition_from(:received).to(:dispatched).on_event(:cancel_receive) }

      it 'updates the received_at data to nil' do
        expect { subject.cancel_receive! }.to change(subject, :received_at).from(subject.received_at).to(nil)
      end
    end
  end

  context 'validation' do
    before do
      @distributor = create(:distributor, name: 'D1', allow_special_discount: true, special_discount: 100)
      @store       = create(:store, distributor: @distributor, name: 'S1')
      @customer    = create(:customer, store_id: @store.id, name: 'C1')

      @user        = create(:administrator, username: 'F1').tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: @store, user: administrator))
      end
    end

    it 'fails if the seller is not present' do
      expect(build(:order, seller: nil, client: @customer, billing_client: @customer, user: @user)).to be_invalid
    end

    it 'fails if the seller is blank' do
      expect(build(:order, seller_id: '', client: @customer, billing_client: @customer, user: @user)).to be_invalid
    end

    it 'fails if the user is not present' do
      expect(build(:order, user: nil, billing_client: @customer, client: @customer)).to be_invalid
    end

    it 'fails if the user is blank' do
      expect(build(:order, user_id: '', billing_client: @customer, client: @customer)).to be_invalid
    end

    it 'fails if the client is not present' do
      expect(build(:order, seller: @store, client: nil, billing_client: @customer, user: @user)).to be_invalid
    end

    it 'fails if the billing client is not present' do
      expect(build(:order, seller: @store, client: @customer, billing_client: nil, user: @user)).to be_invalid
    end

    it 'fails if the date is not present' do
      expect(build(:order, seller: @store, date: nil, client: @customer, billing_client: @customer, user: @user)).to be_invalid
    end

    it 'fails if the date is blank' do
      expect(build(:order, seller: @store, date: '', client: @customer, billing_client: @customer, user: @user)).to be_invalid
    end

    it 'fails if the discount is negative' do
      expect(build(:order, seller: @store, client: @customer, discount: -1, billing_client: @customer, user: @user)).to be_invalid
    end

    it 'fails if the discount is greater than 100' do
      expect(build(:order, seller: @store, client: @customer, discount: BigDecimal.new('100.1'), billing_client: @customer, user: @user)).to be_invalid
    end

    it 'fails if the billing client cannot have special discount and it is configured' do
      @distributor.update(allow_special_discount: false)

      expect(build(:order, seller: @store, client: @customer, billing_client: @distributor, user: @user, special_discount: BigDecimal.new('20'))).to be_invalid
    end

    it 'fails if the status is not \'to_send\' or \'error\' and the special discount changed' do
      order = create(:order, seller: @store, client: @customer, status: :to_send, billing_client: @distributor, user: @user)

      order.update(status: :sent, erp_id: 10)

      order.special_discount = 20

      expect(order).to be_invalid
    end

    it 'fails if the special discount is negative' do
      expect(build(:order, seller: @store, client: @customer, billing_client: @distributor, user: @user, special_discount: -1)).to be_invalid
    end

    it 'fails if the special discount is greater than 100' do
      expect(build(:order, seller: @store, client: @customer, billing_client: @distributor, user: @user, special_discount: BigDecimal.new('100.1'))).to be_invalid
    end

    it 'fails if the status is not present' do
      expect(build(:order, seller: @store, client: @customer, status: nil, billing_client: @customer, user: @user)).to be_invalid
    end

    it 'fails if the status is \'sent\' but the erp id is not present' do
      expect(build(:order, seller: @store, client: @customer, status: :sent, erp_id: nil, billing_client: @customer, user: @user)).to be_invalid
    end

    it 'fails if the status is not \'to_send\' or \'error\' and the billing client changed' do
      expect(build(:order, seller: @store, client: @customer, status: :sent, erp_id: 10, billing_client: @customer, user: @user)).to be_invalid
    end

    it 'allows a blank special discount' do
      expect(build(:order, seller: @store, client: @customer, billing_client: @distributor, user: @user, special_discount: nil)).to be_valid
    end

    it 'passes' do
      expect(build(:order, seller: @store, client: @customer, billing_client: @customer, user: @user)).to be_valid
    end
  end

  describe 'destroying' do
    before do
      @distributor = create(:distributor, name: 'D1')
      @store       = create(:store, distributor: @distributor, name: 'S1')
      @customer    = create(:customer, store_id: @store.id, name: 'C1')

      @user        = create(:administrator, username: 'F1').tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: @store, user: administrator))
      end
      @order = create(:order, seller: @store, client: @customer, billing_client: @customer, user: @user)
    end

    it 'fails if the status is \'sending\'' do
      @order.update(status: :sending)

      expect(@order.destroy).to eq(false)
      expect(@order.errors.full_messages).to_not be_empty
    end

    it 'fails if the status is \'sent\'' do
      @order.update(status: :sent)

      expect(@order.destroy).to eq(false)
      expect(@order.errors.full_messages).to_not be_empty
    end
  end

  describe '#has_payments?' do
    before do
      @factory     = create(:factory, name: 'F1')
      @distributor = create(:distributor, name: 'D1')
      @store       = create(:store, distributor: @distributor, name: 'S1')
      @customer    = create(:customer, store_id: @store.id, name: 'C1')
      @user        = create(:administrator, username: 'F1')

      @payment1 = create(:payment)
      @payment2 = create(:payment)
      @payment3 = create(:payment)
      @payment4 = create(:payment)

      @order = create(:order, seller: @distributor, client: @store, billing_client: @customer, user: @user)
    end

    context 'when the order does not have any associated payment' do
      it 'returns false' do
        expect(@order.has_payments?).to be_falsy
      end
    end

    context 'when the order has associated payments' do
      before do
        create(:production_credit, owner: @distributor.third_party, destination: @factory.third_party, payment: @payment1, order: @order, reason: :order_payment, direction: :input)
        create(:production_credit, owner: @distributor.third_party, destination: @factory.third_party, order: @order, reason: :order_production, direction: :output, value: -100)
        create(:production_credit, owner: @distributor.third_party, destination: @factory.third_party, payment: @payment2, reason: :credit, direction: :input)
        create(:production_credit, owner: @distributor.third_party, destination: @factory.third_party, payment: @payment3, order: @order, reason: :order_payment, direction: :input)
        create(:production_credit, owner: @distributor.third_party, destination: @factory.third_party, payment: @payment4, order: @order, reason: :order_payment, direction: :input, status: :pending)
      end

      it 'returns true' do
        expect(@order.has_payments?).to be_truthy
      end
    end
  end

  describe '#payments' do
    before do
      @factory     = create(:factory, name: 'F1')
      @distributor = create(:distributor, name: 'D1')
      @store       = create(:store, distributor: @distributor, name: 'S1')
      @customer    = create(:customer, store_id: @store.id, name: 'C1')
      @user        = create(:administrator, username: 'F1')

      @payment1 = create(:payment)
      @payment2 = create(:payment)
      @payment3 = create(:payment)
      @payment4 = create(:payment)

      @order = create(:order, seller: @distributor, client: @store, billing_client: @customer, user: @user)

      create(:production_credit, owner: @distributor.third_party, destination: @factory.third_party, payment: @payment1, order: @order, reason: :order_payment, direction: :input)
      create(:production_credit, owner: @distributor.third_party, destination: @factory.third_party, order: @order, reason: :order_production, direction: :output, value: -100)
      create(:production_credit, owner: @distributor.third_party, destination: @factory.third_party, payment: @payment2, reason: :credit, direction: :input)
      create(:production_credit, owner: @distributor.third_party, destination: @factory.third_party, payment: @payment3, order: @order, reason: :order_payment, direction: :input)
      create(:production_credit, owner: @distributor.third_party, destination: @factory.third_party, payment: @payment4, order: @order, reason: :order_payment, direction: :input, status: :pending)
    end

    it 'returns all approved payments related to the order' do
      expect(@order.payments).to match_array([@payment1, @payment3])
    end
  end

  context 'totals' do
    before do
      @factory     = create(:factory, name: 'F1')
      @distributor = create(:distributor, name: 'D1')
      @store       = create(:store, distributor: @distributor, name: 'S1')
      @customer    = create(:customer, store_id: @store.id, name: 'C1')
    
      @user        = create(:administrator, username: 'F1').tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: @factory, user: administrator))
      end

      @order = create(:order, seller: @factory, client: @customer, billing_client: @customer, user: @user)

      @family = create(:family)
      @line = create(:line, family: @family, sale_price: BigDecimal.new('12.50'))

      create(:order_item, factory_total: BigDecimal.new('100.22'), distributor_total: 120, line: @line, order: @order,
        store_total: 140, customer_total: 160)
      create(:order_item, factory_total: 80, distributor_total: 100, line: @line, order: @order,
        store_total: 160, customer_total: 200, discount: BigDecimal.new('32.50'))

      @item = create(:item)

      create(:order_loose_item, order: @order, item: @item, quantity: 3, factory_value: BigDecimal.new('52.22'), distributor_value: BigDecimal.new('60.36'),
        store_value: BigDecimal.new('68.55'), customer_value: BigDecimal.new('72.69'), discount: BigDecimal.new('25.30'))
    end

    describe '#factory_total' do
      it 'sums the factory totals of the items and products/services without discounts' do
        expect(@order.factory_total).to eq(BigDecimal.new('336.88'))
      end
    end

    describe '#factory_total_with_discount' do
      it 'sums the factory totals of the items and products/services with discounts' do
        expect(@order.factory_total_with_discount).to eq(BigDecimal.new('271.25'))
      end
    end

    describe '#factory_total_discount' do
      it 'sums the total discount for the items and products/services' do
        expect(@order.factory_total_discount).to eq(BigDecimal.new('65.63'))
      end
    end

    describe '#distributor_total' do
      it 'sums the distributor totals of the items and products/services without discounts' do
        expect(@order.distributor_total).to eq(BigDecimal.new('401.08'))
      end
    end

    describe '#distributor_total_with_discount' do
      it 'sums the distributor totals of the items and products/services with discounts' do
        expect(@order.distributor_total_with_discount).to eq(BigDecimal.new('322.77'))
      end
    end

    describe '#distributor_total_discount' do
      it 'sums the total discount for the items and products/services' do
        expect(@order.distributor_total_discount).to eq(BigDecimal.new('78.31'))
      end
    end

    describe '#store_total' do
      it 'sums the store totals of the items and products/services with discounts' do
        expect(@order.store_total).to eq(BigDecimal.new('505.65'))
      end
    end

    describe '#store_total_with_discount' do
      it 'sums the store totals of the items and products/services without discounts' do
        expect(@order.store_total_with_discount).to eq(BigDecimal.new('401.62'))
      end
    end

    describe '#store_total_discount' do
      it 'sums the total discount for the items and products/services' do
        expect(@order.store_total_discount).to eq(BigDecimal.new('104.03'))
      end
    end

    describe '#customer_total' do
      it 'sums the customer totals of the items and products/services without discounts' do
        expect(@order.customer_total).to eq(BigDecimal.new('578.07'))
      end
    end

    describe '#customer_total_with_discount' do
      it 'sums the customer totals of the items and products/services with discounts' do
        expect(@order.customer_total_with_discount).to eq(BigDecimal.new('457.90'))
      end
    end

    describe '#customer_total_discount' do
      it 'sums the total discount for the items and products/services' do
        expect(@order.customer_total_discount).to eq(BigDecimal.new('120.17'))
      end
    end

    describe '#total' do
      context 'for a distributor client' do
        before do
          @order.update(client: @distributor)
        end

        it 'returns the distributor total' do
          expect(@order.total).to eq(BigDecimal.new('401.08'))
        end
      end

      context 'for a store client' do
        before do
          @order.update(client: @store)
        end

        it 'returns the store total' do
          expect(@order.total).to eq(BigDecimal.new('505.65'))
        end
      end

      context 'for a customer client' do
        before do
          @order.update(client: @customer)
        end

        it 'returns the customer total' do
          expect(@order.total).to eq(BigDecimal.new('578.07'))
        end
      end
    end

    describe '#commercial_discount' do
      context 'for a distributor client' do
        before do
          @order.update(client: @distributor)
        end

        it 'sums the distributor total discount with the order discount' do
          expect(@order.commercial_discount).to eq(BigDecimal.new('78.31'))
        end
      end

      context 'for a store client' do
        before do
          @order.update(client: @store)
        end

        it 'sums the store total discount with the order discount' do
          expect(@order.commercial_discount).to eq(BigDecimal.new('104.03'))
        end
      end

      context 'for a customer client' do
        before do
          @order.update(client: @customer)
        end

        it 'sums the customer total discount with the order discount' do
          expect(@order.commercial_discount).to eq(BigDecimal.new('120.17'))
        end
      end
    end

    describe '#partial_total' do
      context 'for a distributor client' do
        before do
          @order.update(client: @distributor)
        end

        it 'returns the distributor total' do
          expect(@order.partial_total).to eq(BigDecimal.new('322.77'))
        end
      end

      context 'for a store client' do
        before do
          @order.update(client: @store)
        end

        it 'returns the store total' do
          expect(@order.partial_total).to eq(BigDecimal.new('401.62'))
        end
      end

      context 'for a customer client' do
        before do
          @order.update(client: @customer)
        end

        it 'returns the customer total' do
          expect(@order.partial_total).to eq(BigDecimal.new('457.90'))
        end
      end
    end

    describe '#total_with_discount' do
      context 'for a distributor client' do
        before do
          @order.update(client: @distributor, discount: BigDecimal.new('25'))
        end

        it 'returns the distributor total with the total discount' do
          expect(@order.total_with_discount).to eq(BigDecimal.new('242.08'))
        end
      end

      context 'for a store client' do
        before do
          @order.update(client: @store, discount: BigDecimal.new('26'))
        end

        it 'returns the store total with the total discount' do
          expect(@order.total_with_discount).to eq(BigDecimal.new('297.20'))
        end
      end

      context 'for a customer client' do
        before do
          @order.update(client: @customer, discount: BigDecimal.new('27'))
        end

        it 'returns the customer total with the total discount' do
          expect(@order.total_with_discount).to eq(BigDecimal.new('334.27'))
        end
      end
    end

    describe '#total_discount' do
      context 'for a distributor client' do
        before do
          @order.update(client: @distributor, discount: BigDecimal.new('25'))
        end

        it 'sums the distributor total discount with the order discount' do
          expect(@order.total_discount).to eq(BigDecimal.new('159.00'))
        end
      end

      context 'for a store client' do
        before do
          @order.update(client: @store, discount: BigDecimal.new('26'))
        end

        it 'sums the store total discount with the order discount' do
          expect(@order.total_discount).to eq(BigDecimal.new('208.45'))
        end
      end

      context 'for a customer client' do
        before do
          @order.update(client: @customer, discount: BigDecimal.new('27'))
        end

        it 'sums the customer total discount with the order discount' do
          expect(@order.total_discount).to eq(BigDecimal.new('243.80'))
        end
      end
    end

    describe '#total_with_special_discount' do
      before do
        @customer.update(allow_special_discount: true, special_discount: 90)
      end

      context 'for a distributor client' do
        before do
          @order.update!(client: @distributor, discount: BigDecimal.new('25'), special_discount: BigDecimal.new('10'))
        end

        it 'returns the distributor total with the total discount and total special discount' do
          expect(@order.total_with_special_discount).to eq(BigDecimal.new('217.87'))
        end
      end

      context 'for a store client' do
        before do
          @order.update!(client: @store, discount: BigDecimal.new('26'), special_discount: BigDecimal.new('12'))
        end

        it 'returns the store total with the total discount and total special discount' do
          expect(@order.total_with_special_discount).to eq(BigDecimal.new('261.54'))
        end
      end

      context 'for a customer client' do
        before do
          @order.update!(client: @customer, discount: BigDecimal.new('27'), special_discount: BigDecimal.new('14'))
        end

        it 'returns the customer total with the total discount and total special discount' do
          expect(@order.total_with_special_discount).to eq(BigDecimal.new('287.47'))
        end
      end
    end
  end
end
