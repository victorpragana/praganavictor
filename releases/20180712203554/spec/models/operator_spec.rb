# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  provider               :string           default("email"), not null
#  uid                    :string           default(""), not null
#  email                  :string           default(""), not null
#  username               :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  name                   :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  tokens                 :json
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  type                   :string
#  maximum_discount       :decimal(5, 2)    default(0.0)
#  erp_id                 :integer
#  selected_workplace_id  :integer
#

require 'rails_helper'

RSpec.describe Operator, type: :model do
  it_behaves_like "an user"

  before(:each) do
    @factory     = FactoryGirl.create(:factory, name: 'F1')
    @distributor = FactoryGirl.create(:distributor, name: 'D1')
    @store       = FactoryGirl.create(:store, name: 'S1', distributor: @distributor)
    @operator = FactoryGirl.create(:operator)
  end

  context 'saving' do
    it 'removes the maximum discount information' do
      operator = FactoryGirl.create(:operator, name: 'O1', maximum_discount: BigDecimal.new('0.01'))

      expect(operator.maximum_discount).to eq(0)
    end

    it 'removes the erp id information' do
      operator = FactoryGirl.create(:operator, name: 'O1', erp_id: 20)

      expect(operator.erp_id).to be_nil
    end
  end

  context 'permissions' do
    context 'for factory workplace' do
      before(:each) do
        @operator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @factory, user: @operator))
      end

      it 'contains all operator permissions' do
        expect(@operator.permissions).to contain_exactly(
          ["dashboard", %w{index}],
          ["production_orders", %w{index new edit}]
        )
      end
    end

    context 'for others' do
      it 'contains no permission' do
        expect(@operator.permissions).to be_empty
      end
    end
  end
end
