# == Schema Information
#
# Table name: factories
#
#  id         :integer          not null, primary key
#  deleted_at :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Factory, type: :model do
  before(:each) do
    @factory = FactoryGirl.create(:factory)
  end

  describe 'associations' do
    it { is_expected.to act_as(:third_party) }

    it { is_expected.to have_many(:permitted_administrators) }
    it { is_expected.to have_many(:permitted_managers) }
    it { is_expected.to have_many(:permitted_sellers) }
    it { is_expected.to have_many(:permitted_operators) }
    it { is_expected.to have_many(:permitted_users) }

    it { is_expected.to have_many(:administrators).through(:permitted_administrators).class_name(Administrator) }
    it { is_expected.to have_many(:managers).through(:permitted_managers).class_name(Manager) }
    it { is_expected.to have_many(:sellers).through(:permitted_sellers).class_name(Seller) }
    it { is_expected.to have_many(:operators).through(:permitted_operators).class_name(Operator) }
    it { is_expected.to have_many(:users).through(:permitted_users).class_name(User) }

    it { is_expected.to have_many(:price_tables) }
    it { is_expected.to have_many(:selling_estimates).class_name(Estimate) }
    it { is_expected.to have_many(:selling_orders).class_name(Order) }
  end

  describe '#distributors' do
    before(:each) do
      @distributor_1 = FactoryGirl.create(:distributor, name: 'd1')
      @distributor_2 = FactoryGirl.create(:distributor, name: 'd2')
    end

    it 'returns all distributors' do
      expect(@factory.distributors).to match_array([@distributor_1, @distributor_2])
    end
  end

  describe '#stores' do
    before(:each) do
      @distributor = FactoryGirl.create(:distributor, name: 'DS')
      @store_1     = FactoryGirl.create(:store, name: 'aa', distributor: @distributor)
      @store_2     = FactoryGirl.create(:store, name: 'bb', distributor: @distributor)
      @store_3     = FactoryGirl.create(:store, name: 'cc', distributor: @distributor)
    end

    it 'returns all stores' do
      expect(@factory.stores).to match_array([@store_1, @store_2, @store_3])
    end
  end

  describe '#customers' do
    before(:each) do
      @distributor = FactoryGirl.create(:distributor, name: 'DCU')
      @store_1     = FactoryGirl.create(:store, name: 's1', distributor: @distributor)
      @customer_1     = FactoryGirl.create(:customer, name: 'aa', store: @store_1)
      @customer_2     = FactoryGirl.create(:customer, name: 'bb', store: @store_1)
      @customer_3     = FactoryGirl.create(:customer, name: 'cc', store: @store_1)
    end

    it 'returns all customers' do
      expect(@factory.customers).to match_array([@customer_1, @customer_2, @customer_3])
    end
  end

  describe '#clients' do
    before(:each) do
      @distributor = FactoryGirl.create(:distributor, name: 'DC')
      @store_1     = FactoryGirl.create(:store, name: 's1', distributor: @distributor)
      @store_2     = FactoryGirl.create(:store, name: 's2', distributor: @distributor)
      @store_3     = FactoryGirl.create(:store, name: 's3', distributor: @distributor)
      @customer_1     = FactoryGirl.create(:customer, name: 'c1', store_id: @store_1.id)
      @customer_2     = FactoryGirl.create(:customer, name: 'c2', store_id: @store_2.id)
      @customer_3     = FactoryGirl.create(:customer, name: 'c3', store_id: @store_3.id)
    end

    it 'returns all distributors, stores and customers' do
      expect(@factory.clients).to match_array([@distributor, @store_1, @store_2, @store_3, @customer_1, @customer_2, @customer_3])
    end
  end

  describe '#workplaces' do
    before(:each) do
      @distributor = FactoryGirl.create(:distributor, name: 'DW')
      @store_1     = FactoryGirl.create(:store, name: 's1', distributor: @distributor)
      @store_2     = FactoryGirl.create(:store, name: 's2', distributor: @distributor)
      @store_3     = FactoryGirl.create(:store, name: 's3', distributor: @distributor)
      @customer_1     = FactoryGirl.create(:customer, name: 'c1', store_id: @store_1.id)
      @customer_2     = FactoryGirl.create(:customer, name: 'c2', store_id: @store_2.id)
      @customer_3     = FactoryGirl.create(:customer, name: 'c3', store_id: @store_3.id)
    end

    it 'returns all factories, distributors and stores' do
      expect(@factory.workplaces).to match_array([@factory, @distributor, @store_1, @store_2, @store_3])
    end
  end
end
