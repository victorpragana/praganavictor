# == Schema Information
#
# Table name: price_tables
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  owner_id   :integer
#  owner_type :string
#  name       :string           default(""), not null
#  type       :integer          default(0), not null
#  default    :boolean          default(FALSE)
#

require 'rails_helper'

RSpec.describe PriceTable, type: :model do
  describe 'associations' do
    it { should belong_to(:owner) }
    it { should have_many(:lines).class_name(PriceTableLine).dependent(:destroy) }
    it { should have_many(:items).class_name(PriceTableItem).dependent(:destroy) }
    it { should have_many(:clients).class_name(PriceTableClient).dependent(:destroy) }
  end

  describe 'validation' do
    before do
      @distributor = FactoryGirl.create(:distributor)
    end

    it 'fails if the name is not present' do
      expect(FactoryGirl.build(:price_table, owner: @distributor, name: nil)).to be_invalid
    end

    it 'fails if the name is blank' do
      expect(FactoryGirl.build(:price_table, owner: @distributor, name: '')).to be_invalid
    end

    it 'fails if the owner is not present' do
      expect(FactoryGirl.build(:price_table, owner: nil)).to be_invalid
    end

    it 'fails if already exists a price table with the same name' do
      FactoryGirl.create(:price_table, owner: @distributor, name: 'FoO')

      expect(FactoryGirl.build(:price_table, owner: @distributor, name: 'Foo')).to be_invalid
    end

    it 'fails if the type is not present' do
      expect(FactoryGirl.build(:price_table, owner: @distributor, type: nil)).to be_invalid
    end

    it 'passes' do
      expect(FactoryGirl.build(:price_table, owner: @distributor)).to be_valid
    end
  end

  describe 'after saving' do
    before do
      @distributor   = FactoryGirl.create(:distributor, name: 'D1')
      @distributor_2 = FactoryGirl.create(:distributor, name: 'D2')
      @price_table_1 = FactoryGirl.create(:price_table, owner: @distributor, default: false, type: :for_lines)
      @price_table_2 = FactoryGirl.create(:price_table, owner: @distributor, default: true, type: :for_lines)
      @price_table_3 = FactoryGirl.create(:price_table, owner: @distributor, default: false, type: :for_lines)
      @price_table_4 = FactoryGirl.create(:price_table, owner: @distributor, default: true, type: :for_items)
      @price_table_5 = FactoryGirl.create(:price_table, owner: @distributor_2, default: true, type: :for_lines)
    end

    it 'marks the others owner\'s price tables of the same type as not default if the saved is the new default table' do
      new_price_table = FactoryGirl.create(:price_table, owner: @distributor, default: true, type: :for_lines)

      expect(@price_table_1.reload.default?).to eq(false)
      expect(@price_table_2.reload.default?).to eq(false)
      expect(@price_table_3.reload.default?).to eq(false)
      expect(@price_table_4.reload.default?).to eq(true)
      expect(@price_table_5.reload.default?).to eq(true)
    end
  end
end
