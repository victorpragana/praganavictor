# == Schema Information
#
# Table name: option_components
#
#  id                        :integer          not null, primary key
#  option_id                 :integer
#  total_consumption         :decimal(10, 5)   not null
#  unit_value                :decimal(10, 5)   not null
#  component_name            :string(255)      not null
#  unit                      :integer          not null
#  component_characteristics :text
#  category_name             :string(255)      not null
#  width_consumption         :decimal(10, 5)
#  height_consumption        :decimal(10, 5)
#  quantity_consumption      :decimal(10, 5)
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  required                  :boolean          default(FALSE), not null
#  component_id              :integer
#  rule_id                   :integer
#  total                     :decimal(10, 2)   default(0.0)
#

require 'rails_helper'

RSpec.describe OptionComponent, type: :model do
  describe 'associations' do
    it { is_expected.to belong_to(:option) }
    it { is_expected.to belong_to(:rule) }
    it { is_expected.to belong_to(:component) }
    it { is_expected.to have_and_belong_to_many(:possible_components).class_name(Component) }
  end

  describe 'enums' do
    it { is_expected.to define_enum_for(:unit).with({
      m:     0, 
      mm:    1, 
      un:    2, 
      m2:    3,
      cm:    4,
      km:    5,
      pleat: 6
    }) }
  end

  before(:each) do
    @category    = FactoryGirl.create(:category)
    @family      = FactoryGirl.create(:family)
    @line        = FactoryGirl.create(:line, family: @family)
    @component   = FactoryGirl.create(:component, unit: :cm, category: @category)
    @distributor = FactoryGirl.create(:distributor, name: 'D1')
    @store       = FactoryGirl.create(:store, distributor: @distributor, name: 'S1')
    @customer    = FactoryGirl.create(:customer, store_id: @store.id, name: 'C1')
    @estimate    = FactoryGirl.create(:estimate, seller: @store, client: @customer)
    @environment = FactoryGirl.create(:environment, estimate: @estimate)
    @location    = FactoryGirl.create(:location, environment: @environment)
    @option      = FactoryGirl.create(:option, line: @line, location: @location)
  end

  describe 'validation' do
    it 'fails if the order item is not present' do
      expect(FactoryGirl.build(:option_component, option: nil)).to be_invalid
    end

    it 'fails if the total_consumption is not present' do
      expect(FactoryGirl.build(:option_component, option: @option, total_consumption: nil)).to be_invalid
    end

    it 'fails if the total_consumption is blank' do
      expect(FactoryGirl.build(:option_component, option: @option, total_consumption: '')).to be_invalid
    end

    it 'fails if the total_consumption is negative' do
      expect(FactoryGirl.build(:option_component, option: @option, total_consumption: '-1')).to be_invalid
    end

    it 'fails if the total_consumption is greater than 100000' do
      expect(FactoryGirl.build(:option_component, option: @option, total_consumption: 100000)).to be_invalid
    end

    it 'fails if the width_consumption is negative' do
      expect(FactoryGirl.build(:option_component, option: @option, width_consumption: '-1')).to be_invalid
    end

    it 'fails if the width_consumption is greater than 100000' do
      expect(FactoryGirl.build(:option_component, option: @option, width_consumption: 100000)).to be_invalid
    end

    it 'fails if the height_consumption is negative' do
      expect(FactoryGirl.build(:option_component, option: @option, height_consumption: '-1')).to be_invalid
    end

    it 'fails if the height_consumption is greater than 100000' do
      expect(FactoryGirl.build(:option_component, option: @option, height_consumption: 100000)).to be_invalid
    end

    it 'fails if the quantity_consumption is negative' do
      expect(FactoryGirl.build(:option_component, option: @option, quantity_consumption: '-1')).to be_invalid
    end

    it 'fails if the quantity_consumption is greater than 100000' do
      expect(FactoryGirl.build(:option_component, option: @option, quantity_consumption: 100000)).to be_invalid
    end

    it 'fails if the unit value is not present' do
      expect(FactoryGirl.build(:option_component, option: @option, unit_value: nil)).to be_invalid
    end

    it 'fails if the unit value is blank' do
      expect(FactoryGirl.build(:option_component, option: @option, unit_value: '')).to be_invalid
    end

    it 'fails if the unit value is negative' do
      expect(FactoryGirl.build(:option_component, option: @option, unit_value: '-1')).to be_invalid
    end

    it 'fails if the unit value is greater than 100000' do
      expect(FactoryGirl.build(:option_component, option: @option, unit_value: 100000)).to be_invalid
    end

    it 'fails if the component name is not present' do
      expect(FactoryGirl.build(:option_component, component_name: nil, option: @option)).to be_invalid
    end

    it 'fails if the component name is blank' do
      expect(FactoryGirl.build(:option_component, component_name: '', option: @option)).to be_invalid
    end

    it 'fails if the unit is not present' do
      expect(FactoryGirl.build(:option_component, unit: nil, option: @option)).to be_invalid
    end

    it 'fails if the unit is blank' do
      expect(FactoryGirl.build(:option_component, unit: '', option: @option)).to be_invalid
    end

    it 'fails if the category name is not present' do
      expect(FactoryGirl.build(:option_component, category_name: nil, option: @option)).to be_invalid
    end

    it 'fails if the category name is blank' do
      expect(FactoryGirl.build(:option_component, category_name: '', option: @option)).to be_invalid
    end

    it 'fails if required is not present' do
      expect(FactoryGirl.build(:option_component, required: nil, option: @option)).to be_invalid
    end

    it 'fails if required is blank' do
      expect(FactoryGirl.build(:option_component, required: '', option: @option)).to be_invalid
    end

    it 'fails if the total is not present' do
      expect(FactoryGirl.build(:option_component, option: @option, total: nil)).to be_invalid
    end

    it 'fails if the total is blank' do
      expect(FactoryGirl.build(:option_component, option: @option, total: '')).to be_invalid
    end

    it 'fails if the total is negative' do
      expect(FactoryGirl.build(:option_component, option: @option, total: '-1')).to be_invalid
    end

    it 'fails if the total is greater than 100000' do
      expect(FactoryGirl.build(:option_component, option: @option, total: 100000)).to be_invalid
    end

    it 'passes' do
      expect(FactoryGirl.build(:option_component, option: @option)).to be_valid
    end
  end

  describe '#packing_configuration' do
    before do
      @option_component = FactoryGirl.create(:option_component, option: @option)

      @package1 = FactoryGirl.create(:packing_configuration, packable: @estimate, items: [@option.id + 1, @option.id + 2], component_name: @option_component.component_name)
      @package2 = FactoryGirl.create(:packing_configuration, packable: @estimate, items: [@option.id + 4, @option.id], component_name: 'Foo bar')
      @package3 = FactoryGirl.create(:packing_configuration, packable: @estimate, items: [@option.id + 3, @option.id], component_name: @option_component.component_name)
    end

    it 'returns the package configuration that contains the option' do
      expect(@option_component.packing_configuration).to eq(@package3)
    end
  end
end
