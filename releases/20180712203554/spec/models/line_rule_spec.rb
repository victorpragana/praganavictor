# == Schema Information
#
# Table name: line_rules
#
#  id                   :integer          not null, primary key
#  line_id              :integer
#  width                :int4range
#  height               :int4range
#  control_method       :integer          not null
#  required             :boolean          default(TRUE), not null
#  cost                 :string           not null
#  height_consumption   :string
#  width_consumption    :string
#  total_consumption    :string           not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  automatic            :boolean          default(FALSE)
#  quantity_consumption :string
#  description          :string
#  category_id          :integer
#  weight               :integer          default(0), not null
#  area                 :int4range
#

require 'rails_helper'

RSpec.describe LineRule, type: :model do
  before(:each) do
    @family = create(:family)
    @category = create(:category)
    @line = create(:line, family: @family)
  end

  describe "associations" do
    it { is_expected.to belong_to(:line) }
    it { is_expected.to belong_to(:category) }
    it { is_expected.to have_many(:rules).dependent(:destroy) }
    it { is_expected.to have_many(:components).through(:rules) }
    it { is_expected.to have_many(:questions).through(:line) }
  end

  describe "conversions" do
    it { expect(described_class.new).to respond_to(:width_from_mm_to_cm) }
    it { expect(described_class.new).to respond_to(:height_from_mm_to_cm) }
    it { expect(described_class.new).to respond_to(:area_from_mm2_to_cm2) }
  end

  describe 'enums' do
    it { is_expected.to define_enum_for(:weight).with({
      general: 0,
      light:   1,
      medium:  2,
      heavy:   3
    }) }

    it { is_expected.to define_enum_for(:control_method).with({
      width:      0, 
      height:     1, 
      area:       2, 
      unit:       3,
      total_area: 4
    }) }
  end

  describe "before validation" do
    context "for unit control" do
      it "resets the width information" do
        line_rule = build(:line_rule, line: @line, control_method: LineRule.control_methods[:unit], height: 1..2, width: 2..3)

        line_rule.valid?

        expect(line_rule.width).to be_nil
      end

      it "resets the height information" do
        line_rule = build(:line_rule, line: @line, control_method: LineRule.control_methods[:unit], height: 1..2, width: 2..3)

        line_rule.valid?

        expect(line_rule.height).to be_nil
      end
    end
  end

  describe "validation" do
    it "fails if the line is not present" do
      expect(build(:line_rule, line: nil)).to be_invalid
    end

    it "fails if the control method is not present" do
      expect(build(:line_rule, line: @line, control_method: nil)).to be_invalid
    end

    it "fails if the control method is blank" do
      expect(build(:line_rule, line: @line, control_method: "")).to be_invalid
    end

    it "fails if the total_consumption is not present" do
      expect(build(:line_rule, line: @line, total_consumption: nil)).to be_invalid
    end

    it "fails if the total_consumption is blank" do
      expect(build(:line_rule, line: @line, total_consumption: "")).to be_invalid
    end

    it "fails if the total_consumption formula is invalid" do
      expect(build(:line_rule, line: @line, total_consumption: "2/0")).to be_invalid
    end

    it "fails if the total_consumption formula uses an invalid word" do
      expect(build(:line_rule, line: @line, total_consumption: "2/_UNIT_")).to be_invalid
    end

    it "fails if the width consumption formula uses an invalid word" do
      expect(build(:line_rule, line: @line, width_consumption: "2/_UNIT_")).to be_invalid
    end

    it "fails if the height consumption formula uses an invalid word" do
      expect(build(:line_rule, line: @line, height_consumption: "2/_UNIT_")).to be_invalid
    end

    it "fails if the quantity consumption formula uses an invalid word" do
      expect(build(:line_rule, line: @line, quantity_consumption: "2/_UNIT_")).to be_invalid
    end

    it "fails if the cost is not present" do
      expect(build(:line_rule, line: @line, cost: nil)).to be_invalid
    end

    it "fails if the cost is blank" do
      expect(build(:line_rule, line: @line, cost: "")).to be_invalid
    end

    it "fails if the cost formula is invalid" do
      expect(build(:line_rule, line: @line, cost: "2/0")).to be_invalid
    end

    it "fails if the cost formula uses an invalid word" do
      expect(build(:line_rule, line: @line, cost: "2/_UNIT_")).to be_invalid
    end

    it "fails if the required is not present" do
      expect(build(:line_rule, line: @line, required: nil)).to be_invalid
    end

    it "fails if the automatic is not present" do
      expect(build(:line_rule, line: @line, automatic: nil)).to be_invalid
    end

    context "for width control" do
      context "for non automatic rule" do
        it "fails if the width is not present" do
          expect(build(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: nil, automatic: false)).to be_invalid
        end
      end

      context "for automatic rule" do
        it "passes even if the width is not present" do
          expect(build(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: nil, automatic: true)).to be_valid
        end
      end
    end

    context "for height control" do
      context "for non automatic rule" do
        it "fails if the height is not present" do
          expect(build(:line_rule, line: @line, control_method: LineRule.control_methods[:height], height: nil, automatic: false)).to be_invalid
        end
      end

      context "for automatic rule" do
        it "passes even if the height is not present" do
          expect(build(:line_rule, line: @line, control_method: LineRule.control_methods[:height], height: nil, automatic: true)).to be_valid
        end
      end
    end

    context "for area control" do
      context "for non automatic rule" do
        it "fails if the width is not present" do
          expect(build(:line_rule, line: @line, control_method: LineRule.control_methods[:area], width: nil, automatic: false)).to be_invalid
        end

        it "fails if the height is not present" do
          expect(build(:line_rule, line: @line, control_method: LineRule.control_methods[:area], height: nil, automatic: false)).to be_invalid
        end
      end

      context "for automatic rule" do
        it "fails" do
          expect(build(:line_rule, line: @line, control_method: LineRule.control_methods[:area], width: nil, automatic: true)).to be_invalid
        end
      end
    end

    context "for unit control" do
      it "ignores the width and height" do
        expect(build(:line_rule, line: @line, control_method: LineRule.control_methods[:unit], height: nil, width: nil)).to be_valid
      end

      context "for automatic rule" do
        it "fails" do
          expect(build(:line_rule, line: @line, control_method: LineRule.control_methods[:unit], width: nil, automatic: true)).to be_invalid
        end
      end
    end

    context "for total area control" do
      context "for non automatic rule" do
        it "fails if the area is not present" do
          expect(build(:line_rule, line: @line, control_method: LineRule.control_methods[:total_area], area: nil, automatic: false)).to be_invalid
        end
      end

      context "for automatic rule" do
        it "fails" do
          expect(build(:line_rule, line: @line, control_method: LineRule.control_methods[:total_area], area: 12..13, automatic: true)).to be_invalid
        end
      end
    end

    it 'fails if the weight is not present' do
      expect(build(:line_rule, line: @line, weight: nil)).to be_invalid
    end

    it "passes" do
      expect(build(:line_rule, line: @line)).to be_valid
    end

    it "accepts the word _LARGURA_ for total_consumption" do
      expect(build(:line_rule, line: @line, total_consumption: "_LARGURA_*3")).to be_valid
    end

    it "accepts the word _ALTURA_ for total_consumption" do
      expect(build(:line_rule, line: @line, total_consumption: "_ALTURA_*3")).to be_valid
    end

    it "accepts the word _AREA_ for total_consumption" do
      expect(build(:line_rule, line: @line, total_consumption: "_AREA_*3")).to be_valid
    end

    it "accepts the word _CONSUMO_LARGURA_ for total_consumption" do
      expect(build(:line_rule, line: @line, total_consumption: "_CONSUMO_LARGURA_*3")).to be_valid
    end

    it "accepts the word _CONSUMO_ALTURA_ for total_consumption" do
      expect(build(:line_rule, line: @line, total_consumption: "_CONSUMO_ALTURA_*3")).to be_valid
    end

    it "accepts the word _CONSUMO_QUANTIDADE_ for total_consumption" do
      expect(build(:line_rule, line: @line, total_consumption: "_CONSUMO_QUANTIDADE_*3")).to be_valid
    end

    it "accepts the word _INFORMADO_ for total_consumption" do
      expect(build(:line_rule, line: @line, total_consumption: "_INFORMADO_*3")).to be_valid
    end

    it "accepts the word _LARGURA_ for cost" do
      expect(build(:line_rule, line: @line, cost: "_LARGURA_*3")).to be_valid
    end

    it "accepts the word _ALTURA_ for cost" do
      expect(build(:line_rule, line: @line, cost: "_ALTURA_*3")).to be_valid
    end

    it "accepts the word _AREA_ for cost" do
      expect(build(:line_rule, line: @line, cost: "_AREA_*3")).to be_valid
    end

    it "accepts the word _CUSTO_BASE_ for cost" do
      expect(build(:line_rule, line: @line, cost: "_CUSTO_BASE_*3")).to be_valid
    end

    it "accepts the word _CUSTO_FIXO_ for cost" do
      expect(build(:line_rule, line: @line, cost: "_CUSTO_FIXO_*3")).to be_valid
    end

    it "accepts the word _CONSUMO_QUANTIDADE_ for cost" do
      expect(build(:line_rule, line: @line, cost: "_CONSUMO_QUANTIDADE_*3")).to be_valid
    end

    it "accepts the word _LARGURA_ for width consumption" do
      expect(build(:line_rule, line: @line, width_consumption: "_LARGURA_*3")).to be_valid
    end

    it "accepts the word _ALTURA_ for width consumption" do
      expect(build(:line_rule, line: @line, width_consumption: "_ALTURA_*3")).to be_valid
    end

    it "accepts the word _AREA_ for width consumption" do
      expect(build(:line_rule, line: @line, width_consumption: "_AREA_*3")).to be_valid
    end

    it "accepts the word _LARGURA_ for height consumption" do
      expect(build(:line_rule, line: @line, height_consumption: "_LARGURA_*3")).to be_valid
    end

    it "accepts the word _ALTURA_ for height consumption" do
      expect(build(:line_rule, line: @line, height_consumption: "_ALTURA_*3")).to be_valid
    end

    it "accepts the word _AREA_ for height consumption" do
      expect(build(:line_rule, line: @line, height_consumption: "_AREA_*3")).to be_valid
    end

    it "accepts the word _LARGURA_ for quantity consumption" do
      expect(build(:line_rule, line: @line, quantity_consumption: "_LARGURA_*3")).to be_valid
    end

    it "accepts the word _ALTURA_ for quantity consumption" do
      expect(build(:line_rule, line: @line, quantity_consumption: "_ALTURA_*3")).to be_valid
    end

    it "accepts the word _AREA_ for quantity consumption" do
      expect(build(:line_rule, line: @line, quantity_consumption: "_AREA_*3")).to be_valid
    end

    ['total_consumption', 'cost', 'width_consumption', 'height_consumption', 'quantity_consumption'].each do |field|
      before do
        @question1 = create(:question, type: :dropdown, label: 'Local instalação', value: 'LOCAL_INSTALACAO', options: [{ 'label' => 'Foo', 'value' => 'foo' }, { 'label' => 'Bar', 'value' => 'bar' }])
        @question2 = create(:question, type: :boolean, label: 'Drywall', value: 'DRYWALL')

        @line.question_ids = [@question1.id, @question2.id]
        @line.save!
      end

      it "accepts the registered questions values for #{field}" do
        line_rule = build(:line_rule, line: @line)

        line_rule.send("#{field}=".to_sym, "#{@question1.as_variable}*#{@question2.as_variable}")

        expect(line_rule).to be_valid
      end
    end
  end

  context ".informed_consumption?" do
    it "returns true if the total consumption has the word _INFORMADO_" do
      expect(build(:line_rule, line: @line, total_consumption: "_INFORMADO_*3").informed_consumption?).to eq(true)
    end

    it "returns false if the total consumption does not have the word _INFORMADO_" do
      expect(build(:line_rule, line: @line, total_consumption: "_LARGURA*3").informed_consumption?).to eq(false)
    end
  end
end
