# == Schema Information
#
# Table name: specifications
#
#  id             :integer          not null, primary key
#  name           :string           not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  deleted_at     :datetime
#  automatic      :boolean          default(FALSE)
#  used_in_filter :boolean          default(FALSE)
#

require 'rails_helper'

RSpec.describe Specification, type: :model do
  describe "associations" do
    it { should have_many(:values).dependent(:destroy) }
  end

  context "validation" do
    it "fails if the name is not present" do
      expect(FactoryGirl.build(:specification, name: nil)).to be_invalid
    end

    it "fails if the name is blank" do
      expect(FactoryGirl.build(:specification, name: "")).to be_invalid
    end

    it "fails if already exists a specification with the same name" do
      FactoryGirl.create(:specification, name: "AbC")

      expect(FactoryGirl.build(:specification, name: "abC")).to be_invalid
    end

    it "should pass" do
      expect(FactoryGirl.build(:specification)).to be_valid
    end
  end
end
