# == Schema Information
#
# Table name: order_items
#
#  id                :integer          not null, primary key
#  line_id           :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  width             :integer          not null
#  height            :integer          not null
#  order_id          :integer
#  option_id         :integer
#  factory_total     :decimal(10, 2)   default(0.0)
#  distributor_total :decimal(10, 2)   default(0.0)
#  store_total       :decimal(10, 2)   default(0.0)
#  customer_total    :decimal(10, 2)   default(0.0)
#  discount          :decimal(5, 2)    default(0.0)
#

require 'rails_helper'

RSpec.describe OrderItem, type: :model do
  before(:each) do
    @family      = FactoryGirl.create(:family)
    @line        = FactoryGirl.create(:line, family: @family, sale_price: BigDecimal.new('15.56'))
    @factory     = FactoryGirl.create(:factory, name: 'F1')
    @distributor = FactoryGirl.create(:distributor, name: 'D1')
    @store       = FactoryGirl.create(:store, distributor: @distributor, name: 'S1')
    @customer    = FactoryGirl.create(:customer, store_id: @store.id, name: 'C1')
    @user        = FactoryGirl.create(:administrator, username: 'F1').tap do |administrator|
      administrator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @store, user: administrator))
    end
    @order       = FactoryGirl.create(:order, seller: @store, client: @customer, billing_client: @customer, user: @user)
  end

  describe 'associations' do
    it { should belong_to(:line) }
    it { should belong_to(:order) }
    it { should have_many(:components).class_name('OrderItemComponent').dependent(:destroy) }
  end

  describe 'conversions' do
    it { expect(described_class.new).to respond_to(:width_from_mm_to_cm) }
    it { expect(described_class.new).to respond_to(:height_from_mm_to_cm) }
  end

  describe 'validation' do
    it 'fails if the line is not present' do
      expect(FactoryGirl.build(:order_item, line: nil, order: @order)).to be_invalid
    end

    it 'fails if the line is not active' do
      @line.update!(active: false)

      expect(FactoryGirl.build(:order_item, line: @line, order: @order)).to be_invalid
    end

    it 'fails if the order is not present' do
      expect(FactoryGirl.build(:order_item, order: nil, line: @line)).to be_invalid
    end

    it 'fails if the width is not present' do
      expect(FactoryGirl.build(:order_item, line: @line, order: @order, width: nil)).to be_invalid
    end

    it 'fails if the width is blank' do
      expect(FactoryGirl.build(:order_item, line: @line, order: @order, width: '')).to be_invalid
    end

    it 'fails if the width zero' do
      expect(FactoryGirl.build(:order_item, line: @line, order: @order, width: 0)).to be_invalid
    end

    it 'fails if the width negative' do
      expect(FactoryGirl.build(:order_item, line: @line, order: @order, width: -1)).to be_invalid
    end

    it 'fails if the width is not an integer' do
      expect(FactoryGirl.build(:order_item, line: @line, order: @order, width: BigDecimal.new('0.01'))).to be_invalid
    end

    it 'fails if the height is not present' do
      expect(FactoryGirl.build(:order_item, line: @line, order: @order, height: nil)).to be_invalid
    end

    it 'fails if the height is blank' do
      expect(FactoryGirl.build(:order_item, line: @line, order: @order, height: '')).to be_invalid
    end

    it 'fails if the height zero' do
      expect(FactoryGirl.build(:order_item, line: @line, order: @order, height: 0)).to be_invalid
    end

    it 'fails if the height negative' do
      expect(FactoryGirl.build(:order_item, line: @line, order: @order, height: -1)).to be_invalid
    end

    it 'fails if the height is not an integer' do
      expect(FactoryGirl.build(:order_item, line: @line, order: @order, height: BigDecimal.new('0.01'))).to be_invalid
    end

    it 'fails if the factory total is negative' do
      expect(FactoryGirl.build(:order_item, line: @line, order: @order, factory_total: -1)).to be_invalid
    end

    it 'fails if the distributor total is negative' do
      expect(FactoryGirl.build(:order_item, line: @line, order: @order, distributor_total: -1)).to be_invalid
    end

    it 'fails if the store total is negative' do
      expect(FactoryGirl.build(:order_item, line: @line, order: @order, store_total: -1)).to be_invalid
    end

    it 'fails if the customer total is negative' do
      expect(FactoryGirl.build(:order_item, line: @line, order: @order, customer_total: -1)).to be_invalid
    end

    it 'fails if the discount is negative' do
      expect(FactoryGirl.build(:order_item, line: @line, order: @order, discount: -1)).to be_invalid
    end

    it 'fails if the discount is greater than 100' do
      expect(FactoryGirl.build(:order_item, line: @line, order: @order, discount: BigDecimal.new('100.1'))).to be_invalid
    end

    it 'passes' do
      expect(FactoryGirl.build(:order_item, line: @line, order: @order)).to be_valid
    end
  end

  describe '#identification' do
    before(:each) do
      @order_item = FactoryGirl.create(:order_item, line: @line, order: @order)
    end

    it 'returns the order id with order item id' do
      expect(@order_item.identification).to eq(@order.id.to_s.rjust(5, '0') + '-' + @order_item.id.to_s.rjust(6, '0'))
    end
  end

  describe '#name_for_erp' do
    before(:each) do
      @order_item = FactoryGirl.create(:order_item, line: @line, order: @order)
    end

    it 'returns the order id with order item id and the line name with dimensions' do
      expect(@order_item.name_for_erp).to eq("#{@order_item.identification} #{@order_item.line.name} #{@order_item.width_from_mm_to_cm}cm x #{@order_item.height_from_mm_to_cm}cm")
    end
  end

  describe '#specification' do
    before(:each) do
      @category = FactoryGirl.create(:category, name: 'C1')
      @category_2 = FactoryGirl.create(:category, name: 'C2')
      @category_3 = FactoryGirl.create(:category, name: 'C3')

      @line.categories << FactoryGirl.build(:line_category, category: @category, required: true)
      @line.categories << FactoryGirl.build(:line_category, category: @category_2, required: false)
      @line.categories << FactoryGirl.build(:line_category, category: @category_3, required: false)

      @line.save!

      @component_1 = FactoryGirl.create(:component, unit: :cm, category: @category, erasable: true, cost: BigDecimal.new('32.28'), name: 'CC1')
      @component_2 = FactoryGirl.create(:component, unit: :cm, category: @category_2, erasable: true, name: 'CC2')
      @component_3 = FactoryGirl.create(:component, unit: :cm, category: @category_3, erasable: true, name: 'CC3')

      @item_component_1 = FactoryGirl.build(:order_item_component, component_id: @component_1.id, component_name: @component_1.name, component_characteristics: @component_1.characteristics, 
        category_name: @component_1.category.name, unit: @component_1.unit, total_consumption: 100, unit_value: 250, width_consumption: 20, 
        height_consumption: 30, quantity_consumption: 100, required: true, total: 25000)
      @item_component_2 = FactoryGirl.build(:order_item_component, component_id: @component_2.id, component_name: @component_2.name, component_characteristics: @component_2.characteristics, 
        category_name: @component_2.category.name, unit: @component_2.unit, total_consumption: 20, unit_value: 65, required: false, total: 1300)
      @item_component_3 = FactoryGirl.build(:order_item_component, component_id: @component_3.id, component_name: @component_3.name, component_characteristics: @component_3.characteristics, 
        category_name: @component_3.category.name, unit: @component_3.unit, total_consumption: 20, unit_value: 65, required: false, total: 1300)

      @order_item = FactoryGirl.create(:order_item, line: @line, order: @order, components: [@item_component_1, @item_component_2, @item_component_3])
    end

    it 'returns the order not required componentes names' do
      expect(@order_item.specification).to eq("#{@item_component_2.component_name}, #{@item_component_3.component_name}")
    end
  end

  describe '#total' do
    before do
      @line = FactoryGirl.create(:line, family: @family, sale_price: BigDecimal.new('12.50'))

      @order_item = FactoryGirl.create(:order_item, line: @line, order: @order, factory_total: BigDecimal.new('100.22'), distributor_total: 120,
        store_total: 140, customer_total: 160)
    end

    context 'for a distributor client' do
      before do
        @order.update(client: @distributor)
      end

      it 'returns the distributor total' do
        expect(@order_item.total).to eq(120)
      end
    end

    context 'for a store client' do
      before do
        @order.update(client: @store)
      end

      it 'returns the store total' do
        expect(@order_item.total).to eq(140)
      end
    end

    context 'for a customer client' do
      before do
        @order.update(client: @customer)
      end

      it 'returns the customer total' do
        expect(@order_item.total).to eq(160)
      end
    end
  end

  describe '#total_discount' do
    before do
      @line = FactoryGirl.create(:line, family: @family, sale_price: BigDecimal.new('12.50'))

      @order_item = FactoryGirl.create(:order_item, line: @line, order: @order, factory_total: BigDecimal.new('100.22'), distributor_total: 120,
        store_total: 140, customer_total: 160, discount: 20)
    end

    context 'for a distributor client' do
      before do
        @order.update(client: @distributor)
      end

      it 'returns the distributor total discount' do
        expect(@order_item.total_discount).to eq(24)
      end
    end

    context 'for a store client' do
      before do
        @order.update(client: @store)
      end

      it 'returns the store total discount' do
        expect(@order_item.total_discount).to eq(28)
      end
    end

    context 'for a customer client' do
      before do
        @order.update(client: @customer)
      end

      it 'returns the customer total discount' do
        expect(@order_item.total_discount).to eq(32)
      end
    end
  end

  describe '#total_cost' do
    before do
      @line = FactoryGirl.create(:line, family: @family, sale_price: BigDecimal.new('12.50'))

      @order_item = FactoryGirl.create(:order_item, line: @line, order: @order, factory_total: BigDecimal.new('100.22'), distributor_total: 120,
        store_total: 140, customer_total: 160, discount: 20)
    end

    context 'for a factory seller' do
      before do
        @order.update(seller: @factory)
      end

      it 'returns the factory total' do
        expect(@order_item.total_cost).to eq(BigDecimal.new('100.22'))
      end
    end

    context 'for a distributor seller' do
      before do
        @order.update(seller: @distributor)
      end

      it 'returns the distributor total' do
        expect(@order_item.total_cost).to eq(120)
      end
    end

    context 'for a store seller' do
      before do
        @order.update(seller: @store)
      end

      it 'returns the store total' do
        expect(@order_item.total_cost).to eq(140)
      end
    end
  end
end
