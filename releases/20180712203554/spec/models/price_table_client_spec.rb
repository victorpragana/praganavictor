# == Schema Information
#
# Table name: price_table_clients
#
#  id             :integer          not null, primary key
#  price_table_id :integer
#  client_id      :integer
#  client_type    :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

require 'rails_helper'

RSpec.describe PriceTableClient, type: :model do
  describe 'associations' do
    it { should belong_to(:client) }
    it { should belong_to(:price_table) }
  end

  describe 'validation' do
    before do
      @family = create(:family)
      @line = create(:line, family: @family)
      @distributor = create(:distributor, name: 'D1')
      @store = create(:store, name: 'S1', distributor: @distributor)
      @customer = create(:customer, name: 'C1', store: @store)
      @price_table = create(:price_table, owner: @distributor, type: :for_lines)
    end

    it 'fails if the client is not present' do
      expect(build(:price_table_client, price_table: @price_table, client: nil)).to be_invalid
    end

    it 'fails if the price table is not present' do
      expect(build(:price_table_client, price_table: nil, client: @customer)).to be_invalid
    end

    it 'fails if the client is already present on a price table for the same owner and same type' do
      create(:price_table_client, price_table: @price_table, client: @customer)

      @price_table_2 = create(:price_table, owner: @distributor, name: 'P2', type: :for_lines)

      expect(build(:price_table_client, price_table: @price_table_2, client: @customer)).to be_invalid
    end

    it 'fails if the client is already present on the same price table' do
      create(:price_table_client, price_table: @price_table, client: @customer)

      expect(build(:price_table_client, price_table: @price_table, client: @customer)).to be_invalid
    end

    it 'passes' do
      expect(build(:price_table_client, price_table: @price_table, client: @customer)).to be_valid
    end

    it 'passes if the client is already present on a price table for the same owner and other type' do
      create(:price_table_client, price_table: @price_table, client: @customer)

      @price_table_2 = create(:price_table, owner: @distributor, name: 'P2', type: :for_items)

      expect(build(:price_table_client, price_table: @price_table_2, client: @customer)).to be_valid
    end
  end
end
