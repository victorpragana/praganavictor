require 'rails_helper'

Sidekiq::Testing.inline!

RSpec.describe EstimatePriceCalculatorWorker, type: :worker do
  it { is_expected.to be_processed_in :estimate }
  it { is_expected.to save_backtrace }
  it { is_expected.to be_retryable 0 }

  describe '#perform' do
    subject { described_class }

    before do
      @factory = create(:factory, name: 'F1')
      create(:administrator, username: 'F1').tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: @factory, user: administrator))
      end

      @distributor = create(:distributor, name: 'D1')
      @store       = create(:store, name: 'S1', distributor: @distributor)
      @customer    = create(:customer, name: 'C1', store_id: @store.id)

      @estimate = create(:estimate, seller: @factory, client: @customer, job_id: 'abc123')

      allow(Estimate).to receive(:find).and_return(@estimate)
    end

    context 'when the estimate is \'enqueued\'' do
      let(:service) { double(:service, update_prices: true, success: true)}

      before do
        @estimate.enqueued!

        allow(EstimateService).to receive(:new).and_return(service)
        allow(@estimate).to receive(:update_attribute).and_call_original
      end

      it 'clears the processing message' do
        expect(@estimate).to receive(:update_attribute).with(:processing_message, [])

        subject.perform_async(@estimate.id)
      end

      it 'marks the estimate as \'processing\'' do
        expect_any_instance_of(Estimate).to receive(:processing!)

        subject.perform_async(@estimate.id)
      end

      it 'initializes the estimate service' do
        expect(EstimateService).to receive(:new).and_return(service)

        subject.perform_async(@estimate.id)
      end

      it 'updates the estimate prices' do
        expect(service).to receive(:update_prices).with(@estimate.id)

        subject.perform_async(@estimate.id)
      end

      context 'with success' do
        it 'sets the estimate as free' do
          subject.perform_async(@estimate.id)

          expect(Estimate.statuses[@estimate.reload.status]).to eq(Estimate.statuses[:free])
        end

        it 'clears the job id' do
          subject.perform_async(@estimate.id)

          expect(@estimate.reload.job_id).to be_nil
        end
      end

      context 'with error' do
        let(:service) { double(:service, update_prices: false, success: false, errors: ['foo'])}

        it 'sets the estimate as error' do
          subject.perform_async(@estimate.id)

          expect(Estimate.statuses[@estimate.reload.status]).to eq(Estimate.statuses[:error])
        end

        it 'clears the job id' do
          subject.perform_async(@estimate.id)

          expect(@estimate.reload.job_id).to be_nil
        end
      end
    end

    context 'when the estimate is \'free\'' do
      before do
        @estimate.update(status: Estimate.statuses[:free])
      end

      it 'does nothing' do
        subject.perform_async(@estimate.id)
      end
    end

    context 'when the estimate is \'processing\'' do
      before do
        @estimate.update(status: Estimate.statuses[:processing])
      end

      it 'does nothing' do
        subject.perform_async(@estimate.id)
      end
    end

    context 'when the estimate is \'error\'' do
      before do
        @estimate.update(status: Estimate.statuses[:error])
      end

      it 'does nothing' do
        subject.perform_async(@estimate.id)
      end
    end
  end
end
