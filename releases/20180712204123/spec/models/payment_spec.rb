# == Schema Information
#
# Table name: payments
#
#  id                 :integer          not null, primary key
#  value              :decimal(8, 2)    not null
#  status             :integer          default(0), not null
#  payment_method     :integer          default(0), not null
#  installment_number :integer          default(1), not null
#  bank_slip_url      :string(255)
#  created_at         :datetime
#  updated_at         :datetime
#  iugu_invoice_id    :string
#  tax                :decimal(5, 2)    default(0.0)
#  original_value     :decimal(8, 2)    default(0.0), not null
#

require 'rails_helper'

RSpec.describe Payment, type: :model do
  describe 'enums' do
    it {
      should define_enum_for(:status).with(
        created:   0,
        approved:  1,
        cancelled: 2
      )
    }

    it {
      should define_enum_for(:payment_method).with(
        credit_card: 0,
        bank_slip:   1
      )
    }
  end

  describe 'associations' do
    it { is_expected.to have_many(:movements) }
  end

  context 'validation' do
    it 'fails if payment_method is blank' do
      expect(build(:payment, payment_method: '')).to be_invalid
    end

    it 'fails if payment_method is null' do
      expect(build(:payment, payment_method: nil)).to be_invalid
    end

    it 'fails if installment_number is blank' do
      expect(build(:payment, installment_number: '')).to be_invalid
    end

    it 'fails if installment_number is null' do
      expect(build(:payment, installment_number: nil)).to be_invalid
    end

    it 'fails if installment_number is zero' do
      expect(build(:payment, installment_number: 0)).to be_invalid
    end

    it 'fails if installment_number is float' do
      expect(build(:payment, installment_number: 1.2)).to be_invalid
    end

    it 'fails if original_value is negative' do
      expect(build(:payment, original_value: BigDecimal.new('-0.01'))).to be_invalid
    end

    it 'fails if original_value is 0' do
      expect(build(:payment, original_value: BigDecimal.new('0'))).to be_invalid
    end

    it 'fails if value is negative' do
      expect(build(:payment, value: BigDecimal.new('-0.01'))).to be_invalid
    end

    it 'fails if value is 0' do
      expect(build(:payment, value: BigDecimal.new('0'))).to be_invalid
    end

    it 'fails if the tax is negative' do
      expect(build(:payment, tax: -1)).to be_invalid
    end

    it 'fails if the tax is greater than 100' do
      expect(build(:payment, tax: BigDecimal.new('100.01'))).to be_invalid
    end

    it 'passes' do
      expect(build(:payment)).to be_valid
    end
  end
end
