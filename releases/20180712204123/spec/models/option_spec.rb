# == Schema Information
#
# Table name: options
#
#  id                :integer          not null, primary key
#  line_id           :integer
#  location_id       :integer
#  width             :integer          not null
#  height            :integer          not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  selected          :boolean          default(FALSE), not null
#  factory_total     :decimal(10, 2)   default(0.0)
#  distributor_total :decimal(10, 2)   default(0.0)
#  store_total       :decimal(10, 2)   default(0.0)
#  customer_total    :decimal(10, 2)   default(0.0)
#  discount          :decimal(5, 2)    default(0.0)
#  questions         :json
#

require 'rails_helper'

RSpec.describe Option, type: :model do
  before do
    @family      = create(:family)
    @line        = create(:line, family: @family, sale_price: BigDecimal.new('15.56'))
    @distributor = create(:distributor, name: 'D1')
    @store       = create(:store, distributor: @distributor, name: 'S1')
    @customer    = create(:customer, store_id: @store.id, name: 'C1')
    @estimate    = create(:estimate, seller: @store, client: @customer)
    @environment = create(:environment, estimate: @estimate)
    @location    = create(:location, environment: @environment)
  end

  describe 'associations' do
    it { is_expected.to belong_to(:line) }
    it { is_expected.to belong_to(:location) }
    it { is_expected.to have_many(:components).class_name('OptionComponent').dependent(:destroy) }
  end

  describe 'conversions' do
    it { expect(described_class.new).to respond_to(:width_from_mm_to_cm) }
    it { expect(described_class.new).to respond_to(:height_from_mm_to_cm) }
  end

  describe 'validation' do
    it 'fails if the line is not present' do
      expect(build(:option, line: nil, location: @location)).to be_invalid
    end

    it 'fails if the location is not present' do
      expect(build(:option, location: nil, line: @line)).to be_invalid
    end

    it 'fails if the width is not present' do
      expect(build(:option, line: @line, location: @location, width: nil)).to be_invalid
    end

    it 'fails if the width is blank' do
      expect(build(:option, line: @line, location: @location, width: '')).to be_invalid
    end

    it 'fails if the width zero' do
      expect(build(:option, line: @line, location: @location, width: 0)).to be_invalid
    end

    it 'fails if the width negative' do
      expect(build(:option, line: @line, location: @location, width: -1)).to be_invalid
    end

    it 'fails if the width is not an integer' do
      expect(build(:option, line: @line, location: @location, width: BigDecimal.new('0.01'))).to be_invalid
    end

    it 'fails if the height is not present' do
      expect(build(:option, line: @line, location: @location, height: nil)).to be_invalid
    end

    it 'fails if the height is blank' do
      expect(build(:option, line: @line, location: @location, height: '')).to be_invalid
    end

    it 'fails if the height zero' do
      expect(build(:option, line: @line, location: @location, height: 0)).to be_invalid
    end

    it 'fails if the height negative' do
      expect(build(:option, line: @line, location: @location, height: -1)).to be_invalid
    end

    it 'fails if the height is not an integer' do
      expect(build(:option, line: @line, location: @location, height: BigDecimal.new('0.01'))).to be_invalid
    end

    it 'fails if the selected is not present' do
      expect(build(:option, line: @line, location: @location, selected: nil)).to be_invalid
    end

    it 'fails if the selected is blank' do
      expect(build(:option, line: @line, location: @location, selected: '')).to be_invalid
    end

    it 'fails if the factory total is negative' do
      expect(build(:option, line: @line, location: @location, factory_total: -1)).to be_invalid
    end

    it 'fails if the distributor total is negative' do
      expect(build(:option, line: @line, location: @location, distributor_total: -1)).to be_invalid
    end

    it 'fails if the store total is negative' do
      expect(build(:option, line: @line, location: @location, store_total: -1)).to be_invalid
    end

    it 'fails if the customer total is negative' do
      expect(build(:option, line: @line, location: @location, customer_total: -1)).to be_invalid
    end

    it 'fails if the discount is negative' do
      expect(build(:option, line: @line, location: @location, discount: -1)).to be_invalid
    end

    it 'fails if the discount is greater than 100' do
      expect(build(:option, line: @line, location: @location, discount: BigDecimal.new('100.1'))).to be_invalid
    end

    it 'fails if the discount is greater than the allowed for the line for the price table' do
      @price_table = create(:price_table, owner: @store, default: true, type: :for_lines)
      create(:price_table_line, line: @line, price: '_CMV_*2', price_table: @price_table, maximum_discount: BigDecimal.new('50.1'))
      create(:price_table_client, price_table: @price_table, client: @customer)

      expect(build(:option, line: @line, location: @location, discount: BigDecimal.new('50.2'))).to be_invalid
    end

    it 'passes' do
      expect(build(:option, line: @line, location: @location)).to be_valid
    end
  end

  describe '#total' do
    before do
      @environment = create(:environment, estimate: @estimate)
      @location = create(:location, environment: @environment)

      @family = create(:family)
      @line = create(:line, family: @family, sale_price: BigDecimal.new('12.50'))

      @option = create(:option, line: @line, location: @location, selected: true, factory_total: BigDecimal.new('100.22'), distributor_total: 120,
        store_total: 140, customer_total: 160)
    end

    context 'for a distributor client' do
      before do
        @estimate.update(client: @distributor)
      end

      it 'returns the distributor total' do
        expect(@option.total).to eq(120)
      end
    end

    context 'for a store client' do
      before do
        @estimate.update(client: @store)
      end

      it 'returns the store total' do
        expect(@option.total).to eq(140)
      end
    end

    context 'for a customer client' do
      before do
        @estimate.update(client: @customer)
      end

      it 'returns the customer total' do
        expect(@option.total).to eq(160)
      end
    end
  end

  describe '#total_discount' do
    before do
      @environment = create(:environment, estimate: @estimate)
      @location = create(:location, environment: @environment)

      @family = create(:family)
      @line = create(:line, family: @family, sale_price: BigDecimal.new('12.50'))

      @price_table = create(:price_table, owner: @store, default: true, type: :for_lines)
      create(:price_table_line, line: @line, price: '_CMV_*2', price_table: @price_table, maximum_discount: BigDecimal.new('50.1'))
      create(:price_table_client, price_table: @price_table, client: @customer)

      @option = create(:option, line: @line, location: @location, selected: true, factory_total: BigDecimal.new('100.22'), distributor_total: 120,
        store_total: 140, customer_total: 160, discount: 20)
    end

    context 'for a distributor client' do
      before do
        @estimate.update(client: @distributor)
      end

      it 'returns the distributor total discount' do
        expect(@option.total_discount).to eq(24)
      end
    end

    context 'for a store client' do
      before do
        @estimate.update(client: @store)
      end

      it 'returns the store total discount' do
        expect(@option.total_discount).to eq(28)
      end
    end

    context 'for a customer client' do
      before do
        @estimate.update(client: @customer)
      end

      it 'returns the customer total discount' do
        expect(@option.total_discount).to eq(32)
      end
    end
  end

  describe '#parsed_questions' do
    let(:questions) { { 'FOO' => true, 'BAR' => 'bar' } }

    before do
      @environment = create(:environment, estimate: @estimate)
      @location = create(:location, environment: @environment)

      @family = create(:family)
      @line = create(:line, family: @family, sale_price: BigDecimal.new('12.50'))

      @price_table = create(:price_table, owner: @store, default: true, type: :for_lines)
      create(:price_table_line, line: @line, price: '_CMV_*2', price_table: @price_table, maximum_discount: BigDecimal.new('50.1'))
      create(:price_table_client, price_table: @price_table, client: @customer)

      @question1 = create(:question, value: 'FOO', type: :boolean)
      @question2 = create(:question, value: 'BAR', type: :dropdown, options: [{ 'label' => 'Bar', 'value' => 'bar' }])

      @option = create(:option, line: @line, location: @location, selected: true, factory_total: BigDecimal.new('100.22'), distributor_total: 120,
        store_total: 140, customer_total: 160, discount: 20, questions: questions)
    end

    it 'returns the questions label and selected values' do
      expect(@option.parsed_questions[0]['label']).to eq(@question1.label)
      expect(@option.parsed_questions[0]['value']).to eq(true)

      expect(@option.parsed_questions[1]['label']).to eq(@question2.label)
      expect(@option.parsed_questions[1]['value']).to eq(@question2.options[0]['value'])
    end
  end
end
