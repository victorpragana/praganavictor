# == Schema Information
#
# Table name: customers
#
#  id         :integer          not null, primary key
#  deleted_at :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  store_id   :integer
#

require 'rails_helper'

RSpec.describe Customer, type: :model do
  describe 'associations' do
    it { is_expected.to act_as(:third_party) }

    it { should belong_to(:store) }

    it { should have_many(:price_table_clients).dependent(:destroy) }

    it { should have_many(:purchasing_estimates).class_name(Estimate) }
    it { should have_many(:purchasing_orders).class_name(Order) }
  end

  context 'validation' do
    before(:each) do
      @distributor = FactoryGirl.create(:distributor)
      @store       = FactoryGirl.create(:store, distributor: @distributor)
    end

    it 'fails if the store is not present' do
      expect(FactoryGirl.build(:customer, store: nil)).to be_invalid
    end

    it 'passes for a customer with the same identification but with different stores' do
      store_2 = FactoryGirl.create(:store, distributor: @distributor, name: 'S2')
      FactoryGirl.create(:customer, store: store_2, identification: '510.761.464-73')
      expect(FactoryGirl.build(:customer, store: @store, identification: '510.761.464-73')).to be_valid
    end

    it 'passes' do
      expect(FactoryGirl.build(:customer, store: @store)).to be_valid
    end
  end

  describe '#owner' do
    before(:each) do
      @distributor = FactoryGirl.create(:distributor, name: 'D1')
      @store       = FactoryGirl.create(:store, distributor: @distributor, name: 'S1')
      @customer    = FactoryGirl.create(:customer, store_id: @store.id, name: 'C1')
    end

    it 'returns the store that owns the customer' do
      expect(@customer.owner).to eq(@customer.store)
    end
  end

  describe '#distributor' do
    before(:each) do
      @distributor = FactoryGirl.create(:distributor, name: 'D1')
      @store       = FactoryGirl.create(:store, distributor: @distributor, name: 'S1')
      @customer    = FactoryGirl.create(:customer, store_id: @store.id, name: 'C1')
    end

    it 'returns the store\'s distributor that owns the customer' do
      expect(@customer.distributor).to eq(@distributor)
    end
  end

  describe '#distributor_id' do
    before(:each) do
      @distributor = FactoryGirl.create(:distributor)
      @store       = FactoryGirl.create(:store, distributor: @distributor)
      @customer    = FactoryGirl.create(:customer, store_id: @store.id)
    end

    it 'returns the store\'s distributor id that owns the customer' do
      expect(@customer.distributor_id).to eq(@distributor.id)
    end
  end

  describe '#price_table_for' do
    before(:each) do
      @owner_1 = FactoryGirl.create(:factory, name: 'admin1')
      @owner_2 = FactoryGirl.create(:factory, name: 'admin2')
      @distributor = FactoryGirl.create(:distributor, name: 'D1')

      @store = FactoryGirl.create(:store, name: 'S1', distributor: @distributor)
      @customer = FactoryGirl.create(:customer, name: 'C1', store_id: @store.id)

      @price_table_1 = FactoryGirl.create(:price_table, name: 'P1', owner: @owner_1)
      FactoryGirl.create(:price_table_client, price_table: @price_table_1, client: @distributor)
      FactoryGirl.create(:price_table_client, price_table: @price_table_1, client: @store)
      FactoryGirl.create(:price_table_client, price_table: @price_table_1, client: @customer)

      @price_table_2 = FactoryGirl.create(:price_table, name: 'P2', owner: @owner_2)
    end

    it 'returns the price table por the given owner' do
      expect(@customer.price_table_for(@owner_1)).to eq(@price_table_1)
    end

    context 'with a default price table' do
      before do
        @price_table_2.update(default: true)
      end

      it 'returns the default price table if the customer does not have a price table configured' do
        expect(@customer.price_table_for(@owner_2)).to eq(@price_table_2)
      end
    end

    context 'without a default price table' do
      it 'returns nil if the customer does not have a price table configured' do
        expect(@customer.price_table_for(@owner_2)).to be_nil
      end
    end
  end

  describe '#items_price_table_for' do
    before(:each) do
      @owner_1 = FactoryGirl.create(:factory, name: 'admin1')
      @owner_2 = FactoryGirl.create(:factory, name: 'admin2')
      @distributor = FactoryGirl.create(:distributor, name: 'D1')

      @store = FactoryGirl.create(:store, name: 'S1', distributor: @distributor)
      @customer = FactoryGirl.create(:customer, name: 'C1', store_id: @store.id)

      @price_table_1 = FactoryGirl.create(:price_table, name: 'P1', owner: @owner_1, type: :for_items)
      FactoryGirl.create(:price_table_client, price_table: @price_table_1, client: @distributor)
      FactoryGirl.create(:price_table_client, price_table: @price_table_1, client: @store)
      FactoryGirl.create(:price_table_client, price_table: @price_table_1, client: @customer)

      @price_table_2 = FactoryGirl.create(:price_table, name: 'P2', owner: @owner_2, type: :for_items)
    end

    it 'returns the price table por the given owner' do
      expect(@customer.items_price_table_for(@owner_1)).to eq(@price_table_1)
    end

    context 'with a default price table' do
      before do
        @price_table_2.update(default: true)
      end

      it 'returns the default price table if the customer does not have a price table configured' do
        expect(@customer.items_price_table_for(@owner_2)).to eq(@price_table_2)
      end
    end

    context 'without a default price table' do
      it 'returns nil if the customer does not have a price table configured' do
        expect(@customer.items_price_table_for(@owner_2)).to be_nil
      end
    end
  end
end
