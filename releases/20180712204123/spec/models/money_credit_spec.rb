# == Schema Information
#
# Table name: movements
#
#  id             :integer          not null, primary key
#  owner_id       :integer          not null
#  order_id       :integer
#  payment_id     :integer
#  value          :decimal(8, 2)    not null
#  direction      :integer          not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  status         :integer          default(1), not null
#  date           :datetime         not null
#  type           :string
#  destination_id :integer          not null
#  reason         :integer          default(0), not null
#

require 'rails_helper'

RSpec.describe MoneyCredit, type: :model do
  it_behaves_like 'a movement'

  describe '#equivalent_movement' do
    before do
      @factory = create(:factory, name: 'F1')
      @distributor = create(:distributor, name: 'D1')
      @store = create(:store, name: 'S1', distributor: @distributor)
      @payment = create(:payment, installment_number: 3)
    end

    context 'when the owner is a distributor' do
      before do
        service = MovementCreatorService.new(described_class, @payment, nil)

        service.input(@distributor.third_party, @factory.third_party)

        @movement = described_class.last
      end

      it 'returns nil' do
        expect(@movement.equivalent_movement).to be_nil
      end
    end

    context 'when the owner is a store' do
      before do
        service = MovementCreatorService.new(described_class, @payment, nil)

        service.input(@store.third_party, @distributor.third_party)

        @movement = described_class.where(owner: @store.third_party).last
        @equivalent = described_class.where(owner: @distributor.third_party).last
      end

      it 'returns the equivalent movement for distributor->factory' do
        expect(@movement.equivalent_movement).to eq(@equivalent)
      end
    end
  end
end
