# == Schema Information
#
# Table name: lines
#
#  id                   :integer          not null, primary key
#  name                 :string           not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  family_id            :integer
#  sale_price           :decimal(8, 3)    default(0.0), not null
#  horizontal_increment :integer          default(0), not null
#  vertical_increment   :integer          default(0), not null
#  erp_categories       :json
#  ncm                  :string(8)
#  cst                  :integer
#  active               :boolean          default(TRUE)
#  finished             :boolean          default(FALSE)
#

require 'rails_helper'

RSpec.describe Line, type: :model do
  before(:each) do
    @family = create(:family)
  end

  describe 'associations' do
    it { is_expected.to belong_to(:family) }
    it { is_expected.to have_many(:dimensions).dependent(:destroy) }
    it { is_expected.to have_many(:line_rules).dependent(:destroy) }
    it { is_expected.to have_many(:rules).through(:line_rules) }
    it { is_expected.to have_many(:components).through(:rules) }
    it { is_expected.to have_many(:categories).class_name('LineCategory').dependent(:destroy) }
    it { is_expected.to have_many(:price_table_lines).dependent(:destroy) }
    it { is_expected.to have_many(:price_tables).through(:price_table_lines) }
    it { is_expected.to have_many(:options).dependent(:destroy) }
    it { is_expected.to have_and_belong_to_many(:questions) }
    it { is_expected.to have_and_belong_to_many(:commercial_classifications) }
  end

  it { is_expected.to define_enum_for(:cst).with({
    national:               0, 
    direct_import:          1, 
    foreign_im:             2, 
    national_with_ic_gt_40: 3,
    national_other:         4,
    national_with_ic_lt_40: 5,
    foreign_di_camex:       6,
    foreign_im_camex:       7
  }) }

  describe 'conversions' do
    it { expect(described_class.new).to respond_to(:horizontal_increment_from_mm_to_cm) }
    it { expect(described_class.new).to respond_to(:vertical_increment_from_mm_to_cm) }
  end

  describe 'validation' do
    it 'fails if the family is not present' do
      expect(build(:line, family: nil)).to be_invalid
    end

    it 'fails if the name is not present' do
      expect(build(:line, name: nil, family: @family)).to be_invalid
    end

    it 'fails if the name is blank' do
      expect(build(:line, name: '', family: @family)).to be_invalid
    end

    it 'fails if the name length is greater than 255 characters' do
      expect(build(:line, name: Faker::Lorem.characters(256), family: @family)).to be_invalid
    end

    it 'fails if the name is not unique' do
      create(:line, name: 'abc', family: @family)

      expect(build(:line, name: 'AbC', family: @family)).to be_invalid
    end

    it 'fails if the sale price is not present' do
      expect(build(:line, sale_price: nil, family: @family)).to be_invalid
    end

    it 'fails if the sale price is blank' do
      expect(build(:line, sale_price: '', family: @family)).to be_invalid
    end

    it 'fails if the sale price is negative' do
      expect(build(:line, sale_price: BigDecimal.new('-0.01'), family: @family)).to be_invalid
    end

    it 'fails if the vertical increment is not present' do
      expect(build(:line, vertical_increment: nil, family: @family)).to be_invalid
    end

    it 'fails if the vertical increment is negative' do
      expect(build(:line, vertical_increment: BigDecimal.new('-0.01'), family: @family)).to be_invalid
    end

    it 'fails if the vertical increment is not an integer' do
      expect(build(:line, vertical_increment: BigDecimal.new('1.01'), family: @family)).to be_invalid
    end

    it 'fails if the horizontal increment is not present' do
      expect(build(:line, horizontal_increment: nil, family: @family)).to be_invalid
    end

    it 'fails if the horizontal increment is negative' do
      expect(build(:line, horizontal_increment: BigDecimal.new('-0.01'), family: @family)).to be_invalid
    end

    it 'fails if the horizontal increment is not an integer' do
      expect(build(:line, horizontal_increment: BigDecimal.new('1.01'), family: @family)).to be_invalid
    end

    it 'fails if the dimensions overlaps in height for same weight dimension' do
      line = build(:line, family: @family)

      line.dimensions << build(:dimension, width: 1..5, height: 1..2, weight: :general)
      line.dimensions << build(:dimension, width: 7..9, height: 3..4, weight: :general)
      line.dimensions << build(:dimension, width: 6..7, height: 1..4, weight: :general)

      expect(line).to be_invalid
    end

    it 'fails if the dimensions overlaps in width for same weight dimension' do
      line = build(:line, family: @family)

      line.dimensions << build(:dimension, height: 1..5, width: 1..2, weight: :general)
      line.dimensions << build(:dimension, height: 7..9, width: 3..4, weight: :general)
      line.dimensions << build(:dimension, height: 6..7, width: 1..4, weight: :general)

      expect(line).to be_invalid
    end

    it 'fails if the ncm does not have only numbers' do
      expect(build(:line, ncm: '2222222A', family: @family)).to be_invalid
    end

    it 'fails if the ncm has less than 8 digits' do
      expect(build(:line, ncm: '2222222', family: @family)).to be_invalid
    end

    it 'fails if the ncm has more than 8 digits' do
      expect(build(:line, ncm: '888569899', family: @family)).to be_invalid
    end

    it 'passes' do
      line = build(:line, family: @family)

      line.dimensions << build(:dimension, height: 10..50, width: 10..20)
      line.dimensions << build(:dimension, height: 70..90, width: 30..41)
      line.dimensions << build(:dimension, height: 60..60, width: 10..40)

      expect(line).to be_valid
    end

    it 'passes' do
      line = build(:line, family: @family)

      line.dimensions << build(:dimension, height: 10..50, width: 10..20)
      line.dimensions << build(:dimension, height: 70..90, width: 30..41)
      line.dimensions << build(:dimension, height: 70..90, width: 10..20)

      expect(line).to be_valid
    end

    it 'passes' do
      line = build(:line, family: @family, ncm: '58964788')
    end

    it 'passes even if the dimensions overlaps in height for different weight dimension' do
      line = build(:line, family: @family)

      line.dimensions << build(:dimension, width: 1..5, height: 1..2, weight: :general)
      line.dimensions << build(:dimension, width: 7..9, height: 3..4, weight: :general)
      line.dimensions << build(:dimension, width: 6..7, height: 1..4, weight: :light)

      expect(line).to be_valid
    end

    it 'passes even if the dimensions overlaps in width for different weight dimension' do
      line = build(:line, family: @family)

      line.dimensions << build(:dimension, height: 1..5, width: 1..2, weight: :general)
      line.dimensions << build(:dimension, height: 7..9, width: 3..4, weight: :general)
      line.dimensions << build(:dimension, height: 6..7, width: 1..4, weight: :light)

      expect(line).to be_valid
    end
  end

  describe 'before saving' do
    before(:each) do
      @family_2 = create(:family, name: 'F2')

      @category  = create(:category, name: 'C1')

      @component_1 = create(:component, category: @category, families: [@family])
      @component_2 = create(:component, category: @category, families: [@family_2])

      @line = create(:line, family: @family, dimensions: [
        build(:dimension, height: 10..50, width: 10..20),
        build(:dimension, height: 70..90, width: 30..41),
        build(:dimension, height: 70..90, width: 10..20)
      ], categories: [
        build(:line_category, category: @category, order: 0)
      ])

      @line_rule = create(:line_rule, line: @line, control_method: LineRule.control_methods[:unit], height: 10..20, width: 11..15,
        rules: [
          build(:rule, line_rule: @line_rule, component: @component_1)
        ]
      )
    end

    it 'removes the rules that use components which family does not match the line family' do
      @line.family = @family_2

      @line.save!

      @line_rule.reload

      expect(@line_rule.rules[0]).to be_nil
    end
  end
end
