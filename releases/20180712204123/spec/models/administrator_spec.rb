# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  provider               :string           default("email"), not null
#  uid                    :string           default(""), not null
#  email                  :string           default(""), not null
#  username               :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  name                   :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  tokens                 :json
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  type                   :string
#  maximum_discount       :decimal(5, 2)    default(0.0)
#  erp_id                 :integer
#  selected_workplace_id  :integer
#

require 'rails_helper'

RSpec.describe Administrator, type: :model do
  it_behaves_like 'an user'

  describe 'associations' do
    it { is_expected.to have_many(:orders).dependent(:restrict_with_error).with_foreign_key(:user_id) }
  end

  before do
    @factory     = create(:factory, name: 'F1')
    @distributor = create(:distributor, name: 'D1')
    @store       = create(:store, name: 'S1', distributor: @distributor)
    @administrator = create(:administrator)
  end

  context 'validation' do
    it 'fails if the maximum discount is negative' do
      expect(build(:administrator, maximum_discount: -1)).to be_invalid
    end

    it 'fails if the maximum discount is greater than 100' do
      expect(build(:administrator, maximum_discount: BigDecimal.new('100.1'))).to be_invalid
    end
  end

  context 'permissions' do
    context 'for factory workplace' do
      before do
        @administrator.update!(selected_workplace: create(:user_workplace, workplace: @factory, user: @administrator))
      end

      it 'contains all permissions' do
        expect(@administrator.permissions).to contain_exactly(
          ['dashboard', %w{index}],
          ['families', %w{index}],
          ['attributes', %w{index new edit}],
          ['base_components', %w{index new edit}],
          ['components', %w{index new edit}],
          ['lines', %w{index new edit rules}],
          ['estimates', %w{index new edit}],
          ['orders', %w{index new edit}],
          ['production_orders', %w{index new edit}],
          ['users', %w{index new edit}],
          ['stores', %w{index new edit}],
          ['distributors', %w{index new edit}],
          ['customers', %w{index new edit}],
          ['price_tables', %w{index new edit}],
          ['items', %w{index new edit}],
          ['cut_tables', %w{index new edit}],
          ['document', %w{index new edit}],
          ['printers', %w{index new edit}],
          ['questions', %w{index new edit}],
          ['classifications', %w{index new edit}],
          ['conceded_credits', %w{index}],
          ['payment_conditions', %w{index}]
        )
      end
    end

    context 'for distributor workplace' do
      before do
        @administrator.update!(selected_workplace: create(:user_workplace, workplace: @distributor, user: @administrator))
      end

      it 'contains admin distributor permissions' do
        expect(@administrator.permissions).to contain_exactly(
          ['dashboard', %w{index}],
          ['estimates', %w{index new edit}],
          ['orders', %w{index edit}],
          ['users', %w{index new edit}],
          ['stores', %w{index new edit}],
          ['price_tables', %w{index new edit}],
          ['items', %w{index new edit}],
          ['credits', %w{index}],
          ['conceded_credits', %w{index}]
        )
      end
    end

    context 'for store workplace' do
      before do
        @administrator.update!(selected_workplace: create(:user_workplace, workplace: @store, user: @administrator))
      end

      it 'contains admin store permissions' do
        expect(@administrator.permissions).to contain_exactly(
          ['dashboard', %w{index}],
          ['customers', %w{index new edit}],
          ['estimates', %w{index new edit}],
          ['users', %w{index new edit}],
          ['orders', %w{index edit}],
          ['price_tables', %w{index new edit}],
          ['items', %w{index new edit}],
          ['credits', %w{index}]
        )
      end
    end

    context 'for others' do
      it 'contains no permission' do
        expect(@administrator.permissions).to be_empty
      end
    end
  end
end
