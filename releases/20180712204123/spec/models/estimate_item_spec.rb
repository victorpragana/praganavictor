# == Schema Information
#
# Table name: estimate_items
#
#  id                :integer          not null, primary key
#  estimate_id       :integer
#  item_code         :string
#  item_description  :string           not null
#  unit              :integer          not null
#  quantity          :decimal(8, 2)    not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  type              :integer          default(0)
#  factory_value     :decimal(10, 2)   default(0.0)
#  distributor_value :decimal(10, 2)   default(0.0)
#  store_value       :decimal(10, 2)   default(0.0)
#  customer_value    :decimal(10, 2)   default(0.0)
#  discount          :decimal(5, 2)    default(0.0)
#  item_id           :integer
#

require 'rails_helper'

RSpec.describe EstimateItem, type: :model do
  before do
    @distributor = FactoryGirl.create(:distributor, name: 'D1')
    @store       = FactoryGirl.create(:store, distributor: @distributor, name: 'S1')
    @customer    = FactoryGirl.create(:customer, store_id: @store.id, name: 'C1')

    @estimate    = FactoryGirl.create(:estimate, seller: @store, client: @customer)
    @item        = FactoryGirl.create(:item)
  end

  describe 'associations' do
    it { should belong_to(:estimate) }
    it { should belong_to(:item) }
  end

  describe 'enums' do
    it { should define_enum_for(:type).with({
      product: 0, 
      service: 1
    }) }
  end

  describe 'validations' do
    it 'fails if the estimate is not present' do
      expect(FactoryGirl.build(:estimate_item, estimate: nil)).to be_invalid
    end

    it 'fails if the item is not present' do
      expect(FactoryGirl.build(:estimate_item, estimate: @estimate, item: nil)).to be_invalid
    end

    it 'fails if the item description is not present' do
      expect(FactoryGirl.build(:estimate_item, estimate: @estimate, item: @item, item_description: nil)).to be_invalid
    end

    it 'fails if the unit is not present' do
      expect(FactoryGirl.build(:estimate_item, estimate: @estimate, item: @item, unit: nil)).to be_invalid
    end

    it 'fails if the quantity is not present' do
      expect(FactoryGirl.build(:estimate_item, estimate: @estimate, item: @item, quantity: nil)).to be_invalid
    end

    it 'fails if the quantity is negative' do
      expect(FactoryGirl.build(:estimate_item, estimate: @estimate, item: @item, quantity: BigDecimal.new('-0.01'))).to be_invalid
    end

    it 'fails if the quantity is zero' do
      expect(FactoryGirl.build(:estimate_item, estimate: @estimate, item: @item, quantity: 0)).to be_invalid
    end

    it 'fails if the factory value is negative' do
      expect(FactoryGirl.build(:estimate_item, estimate: @estimate, item: @item, factory_value: BigDecimal.new('-0.01'))).to be_invalid
    end

    it 'fails if the distributor value is negative' do
      expect(FactoryGirl.build(:estimate_item, estimate: @estimate, item: @item, distributor_value: BigDecimal.new('-0.01'))).to be_invalid
    end

    it 'fails if the store value is negative' do
      expect(FactoryGirl.build(:estimate_item, estimate: @estimate, item: @item, store_value: BigDecimal.new('-0.01'))).to be_invalid
    end

    it 'fails if the customer value is negative' do
      expect(FactoryGirl.build(:estimate_item, estimate: @estimate, item: @item, customer_value: BigDecimal.new('-0.01'))).to be_invalid
    end

    it 'fails if the type is not present' do
      expect(FactoryGirl.build(:estimate_item, estimate: @estimate, item: @item, type: nil)).to be_invalid
    end

    it 'fails if the discount is negative' do
      expect(FactoryGirl.build(:estimate_item, estimate: @estimate, item: @item, discount: -1)).to be_invalid
    end

    it 'fails if the discount is greater than 100' do
      expect(FactoryGirl.build(:estimate_item, estimate: @estimate, item: @item, discount: BigDecimal.new('100.1'))).to be_invalid
    end

    it 'fails if the discount is greater than the allowed for the price table' do
      @price_table = FactoryGirl.create(:price_table, owner: @store, default: true, type: :for_items)
      FactoryGirl.create(:price_table_item, item: @item, price: '200', price_table: @price_table, maximum_discount: BigDecimal.new('50.1'))

      expect(FactoryGirl.build(:estimate_item, estimate: @estimate, item: @item, discount: BigDecimal.new('50.2'))).to be_invalid
    end

    it 'passes' do
      expect(FactoryGirl.build(:estimate_item, estimate: @estimate, item: @item)).to be_valid
    end
  end

  describe '#value' do
    before do
      @estimate_item = FactoryGirl.create(:estimate_item, estimate: @estimate, item: @item, quantity: BigDecimal.new('3'), 
        factory_value: 10, distributor_value: 20, store_value: 30, customer_value: 40)
    end

    context 'for a distributor client' do
      before do
        @estimate.update(client: @distributor)
      end

      it 'returns the distributor value' do
        expect(@estimate_item.value).to eq(20)
      end
    end

    context 'for a store client' do
      before do
        @estimate.update(client: @store)
      end

      it 'returns the store value' do
        expect(@estimate_item.value).to eq(30)
      end
    end

    context 'for a customer client' do
      before do
        @estimate.update(client: @customer)
      end

      it 'returns the customer value' do
        expect(@estimate_item.value).to eq(40)
      end
    end
  end

  describe '#factory_total' do
    before do
      @price_table_item = FactoryGirl.create(:price_table, owner: @store, default: true, type: :for_items)
      FactoryGirl.create(:price_table_item, item: @item, price: '200', price_table: @price_table_item, maximum_discount: BigDecimal.new('25.8'))
    end

    it 'should multiply the quantity by the factory value' do
      expect(FactoryGirl.build(:estimate_item, estimate: @estimate, item: @item, quantity: BigDecimal.new('2.3'), factory_value: BigDecimal.new('58.22'), 
        discount: BigDecimal.new('25.8')).factory_total).to eq(BigDecimal.new('133.91'))
    end
  end

  describe '#factory_total_discount' do
    before do
      @price_table_item = FactoryGirl.create(:price_table, owner: @store, default: true, type: :for_items)
      FactoryGirl.create(:price_table_item, item: @item, price: '200', price_table: @price_table_item, maximum_discount: BigDecimal.new('25.8'))
    end

    it 'should multiply the quantity by the factory value and by the discount' do
      expect(FactoryGirl.build(:estimate_item, estimate: @estimate, item: @item, quantity: BigDecimal.new('2.3'), factory_value: BigDecimal.new('58.22'), 
        discount: BigDecimal.new('25.8')).factory_total_discount).to eq(BigDecimal.new('34.55'))
    end
  end

  describe '#factory_total_with_discount' do
    before do
      @price_table_item = FactoryGirl.create(:price_table, owner: @store, default: true, type: :for_items)
      FactoryGirl.create(:price_table_item, item: @item, price: '200', price_table: @price_table_item, maximum_discount: BigDecimal.new('25.8'))
    end

    it 'should subtract the factory total with the factory total discount' do
      expect(FactoryGirl.build(:estimate_item, estimate: @estimate, item: @item, quantity: BigDecimal.new('2.3'), factory_value: BigDecimal.new('58.22'), 
        discount: BigDecimal.new('25.8')).factory_total_with_discount).to eq(BigDecimal.new('99.36'))
    end
  end

  describe '#distributor_total' do
    it 'should multiply the quantity by the distributor value' do
      expect(FactoryGirl.build(:estimate_item, estimate: @estimate, item: @item, quantity: BigDecimal.new('2.4'), distributor_value: BigDecimal.new('58.22'), 
        discount: BigDecimal.new('25.8')).distributor_total).to eq(BigDecimal.new('139.73'))
    end
  end

  describe '#distributor_total_discount' do
    it 'should multiply the quantity by the distributor value and by the discount' do
      expect(FactoryGirl.build(:estimate_item, estimate: @estimate, item: @item, quantity: BigDecimal.new('2.4'), distributor_value: BigDecimal.new('58.22'), 
        discount: BigDecimal.new('25.8')).distributor_total_discount).to eq(BigDecimal.new('36.05'))
    end
  end

  describe '#distributor_total_with_discount' do
    it 'should subtract the distributor total with the distributor total discount' do
      expect(FactoryGirl.build(:estimate_item, estimate: @estimate, item: @item, quantity: BigDecimal.new('2.4'), distributor_value: BigDecimal.new('58.22'), 
        discount: BigDecimal.new('25.8')).distributor_total_with_discount).to eq(BigDecimal.new('103.68'))
    end
  end

  describe '#store_total' do
    it 'should multiply the quantity by the store value' do
      expect(FactoryGirl.build(:estimate_item, estimate: @estimate, item: @item, quantity: BigDecimal.new('2'), store_value: BigDecimal.new('58.22'), 
        discount: BigDecimal.new('25.8')).store_total).to eq(BigDecimal.new('116.44'))
    end
  end

  describe '#store_total_discount' do
    it 'should multiply the quantity by the store value and by the discount' do
      expect(FactoryGirl.build(:estimate_item, estimate: @estimate, item: @item, quantity: BigDecimal.new('2'), store_value: BigDecimal.new('58.22'), 
        discount: BigDecimal.new('25.8')).store_total_discount).to eq(BigDecimal.new('30.04'))
    end
  end

  describe '#store_total_with_discount' do
    it 'should subtract the store total with the store total discount' do
      expect(FactoryGirl.build(:estimate_item, estimate: @estimate, item: @item, quantity: BigDecimal.new('2'), store_value: BigDecimal.new('58.22'), 
        discount: BigDecimal.new('25.8')).store_total_with_discount).to eq(BigDecimal.new('86.40'))
    end
  end

  describe '#customer_total' do
    it 'should multiply the quantity by the customer value' do
      expect(FactoryGirl.build(:estimate_item, estimate: @estimate, item: @item, quantity: BigDecimal.new('3'), customer_value: BigDecimal.new('58.22'), 
        discount: BigDecimal.new('25.8')).customer_total).to eq(BigDecimal.new('174.66'))
    end
  end

  describe '#customer_total_discount' do
    it 'should multiply the quantity by the customer value and by the discount' do
      expect(FactoryGirl.build(:estimate_item, estimate: @estimate, item: @item, quantity: BigDecimal.new('3'), customer_value: BigDecimal.new('58.22'), 
        discount: BigDecimal.new('25.8')).customer_total_discount).to eq(BigDecimal.new('45.06'))
    end
  end

  describe '#customer_total_with_discount' do
    it 'should subtract the customer total with the customer total discount' do
      expect(FactoryGirl.build(:estimate_item, estimate: @estimate, item: @item, quantity: BigDecimal.new('3'), customer_value: BigDecimal.new('58.22'), 
        discount: BigDecimal.new('25.8')).customer_total_with_discount).to eq(BigDecimal.new('129.60'))
    end
  end

  describe '#total' do
    before do
      @price_table_item = FactoryGirl.create(:price_table, owner: @store, default: true, type: :for_items)
      FactoryGirl.create(:price_table_item, item: @item, price: '200', price_table: @price_table_item, maximum_discount: BigDecimal.new('25.8'))

      @estimate_item = FactoryGirl.create(:estimate_item, estimate: @estimate, item: @item, quantity: BigDecimal.new('3'), factory_value: 10, 
        distributor_value: 20, store_value: 30, customer_value: 40, discount: BigDecimal.new('25.8'))
    end

    context 'for a distributor client' do
      before do
        @estimate.update(client: @distributor)
      end

      it 'returns the distributor total' do
        expect(@estimate_item.total).to eq(BigDecimal.new('60'))
      end
    end

    context 'for a store client' do
      before do
        @estimate.update(client: @store)
      end

      it 'returns the store total' do
        expect(@estimate_item.total).to eq(BigDecimal.new('90'))
      end
    end

    context 'for a customer client' do
      before do
        @estimate.update(client: @customer)
      end

      it 'returns the customer total' do
        expect(@estimate_item.total).to eq(BigDecimal.new('120'))
      end
    end
  end

  describe '#total_discount' do
    before do
      @price_table_item = FactoryGirl.create(:price_table, owner: @store, default: true, type: :for_items)
      FactoryGirl.create(:price_table_item, item: @item, price: '200', price_table: @price_table_item, maximum_discount: BigDecimal.new('25.8'))

      @estimate_item = FactoryGirl.create(:estimate_item, estimate: @estimate, item: @item, quantity: BigDecimal.new('3'), factory_value: 10, 
        distributor_value: 20, store_value: 30, customer_value: 40, discount: BigDecimal.new('25.8'))
    end

    context 'for a distributor client' do
      before do
        @estimate.update(client: @distributor)
      end

      it 'returns the distributor total discount' do
        expect(@estimate_item.total_discount).to eq(BigDecimal.new('15.48'))
      end
    end

    context 'for a store client' do
      before do
        @estimate.update(client: @store)
      end

      it 'returns the store total discount' do
        expect(@estimate_item.total_discount).to eq(BigDecimal.new('23.22'))
      end
    end

    context 'for a customer client' do
      before do
        @estimate.update(client: @customer)
      end

      it 'returns the customer total discount' do
        expect(@estimate_item.total_discount).to eq(BigDecimal.new('30.96'))
      end
    end
  end

  describe '#total_with_discount' do
    before do
      @price_table_item = FactoryGirl.create(:price_table, owner: @store, default: true, type: :for_items)
      FactoryGirl.create(:price_table_item, item: @item, price: '200', price_table: @price_table_item, maximum_discount: BigDecimal.new('25.8'))

      @estimate_item = FactoryGirl.create(:estimate_item, estimate: @estimate, item: @item, quantity: BigDecimal.new('3'), factory_value: 10, 
        distributor_value: 20, store_value: 30, customer_value: 40, discount: BigDecimal.new('25.8'))
    end

    context 'for a distributor client' do
      before do
        @estimate.update(client: @distributor)
      end

      it 'returns the distributor total' do
        expect(@estimate_item.total_with_discount).to eq(BigDecimal.new('44.52'))
      end
    end

    context 'for a store client' do
      before do
        @estimate.update(client: @store)
      end

      it 'returns the store total' do
        expect(@estimate_item.total_with_discount).to eq(BigDecimal.new('66.78'))
      end
    end

    context 'for a customer client' do
      before do
        @estimate.update(client: @customer)
      end

      it 'returns the customer total' do
        expect(@estimate_item.total_with_discount).to eq(BigDecimal.new('89.04'))
      end
    end
  end
end
