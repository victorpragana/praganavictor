# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  provider               :string           default("email"), not null
#  uid                    :string           default(""), not null
#  email                  :string           default(""), not null
#  username               :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  name                   :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  tokens                 :json
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  type                   :string
#  maximum_discount       :decimal(5, 2)    default(0.0)
#  erp_id                 :integer
#  selected_workplace_id  :integer
#

require 'rails_helper'

RSpec.describe Seller, type: :model do
  it_behaves_like "an user"

  describe 'associations' do
    it { is_expected.to have_many(:orders).dependent(:restrict_with_error).with_foreign_key(:user_id) }
  end

  before(:each) do
    @factory = FactoryGirl.create(:factory, name: 'F1')
    @distributor = FactoryGirl.create(:distributor, name: 'D1')
    @store = FactoryGirl.create(:store, name: 'S1', distributor: @distributor)
    @seller = FactoryGirl.create(:seller)
  end

  context 'validation' do
    it 'fails if the maximum discount is negative' do
      expect(FactoryGirl.build(:seller, maximum_discount: -1)).to be_invalid
    end

    it 'fails if the maximum discount is greater than 100' do
      expect(FactoryGirl.build(:seller, maximum_discount: BigDecimal.new('100.1'))).to be_invalid
    end
  end

  context 'permissions' do
    context 'for factory workplace' do
      before(:each) do
        @seller.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @factory, user: @seller))
      end

      it 'contains all seller permissions' do
        expect(@seller.permissions).to contain_exactly(
          ['dashboard', %w{index}],
          ['estimates', %w{index new edit}],
          ['orders', %w{index new edit}],
          ['stores', %w{index new edit}],
          ['distributors', %w{index new edit}],
          ['customers', %w{index new edit}]
        )
      end
    end

    context 'for distributor workplace' do
      before(:each) do
        @seller.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @distributor, user: @seller))
      end

      it 'contains seller distributor permissions' do
        expect(@seller.permissions).to contain_exactly(
          ['dashboard', %w{index}],
          ['estimates', %w{index new edit}],
          ['orders', %w{index edit}],
          ['stores', %w{index new edit}]
        )
      end
    end

    context 'for store workplace' do
      before(:each) do
        @seller.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @store, user: @seller))
      end

      it 'contains seller store permissions' do
        expect(@seller.permissions).to contain_exactly(
          ['dashboard', %w{index}],
          ['customers', %w{index new edit}],
          ['estimates', %w{index new edit}],
          ['orders', %w{index edit}]
        )
      end
    end

    context 'for others' do
      it 'contains no permission' do
        expect(@seller.permissions).to be_empty
      end
    end
  end
end
