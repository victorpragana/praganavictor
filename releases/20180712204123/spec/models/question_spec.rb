# == Schema Information
#
# Table name: questions
#
#  id         :integer          not null, primary key
#  label      :string           not null
#  value      :string           not null
#  type       :integer          not null
#  options    :json
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Question, type: :model do
  describe 'enums' do
    it do
      is_expected.to define_enum_for(:type).with(
        boolean:  0,
        dropdown: 1
      )
    end
  end

  describe 'associations' do
    it { is_expected.to have_and_belong_to_many(:lines) }
  end

  describe 'validations' do
    it 'fails if the label is not present' do
      expect(build(:question, label: nil)).to be_invalid
    end

    it 'fails if the label is not present' do
      expect(build(:question, label: '')).to be_invalid
    end

    it 'fails if the value is not present' do
      expect(build(:question, value: nil)).to be_invalid
    end

    it 'fails if the value is not present' do
      expect(build(:question, value: '')).to be_invalid
    end

    it 'fails if the value contains characters other than [a-zA-Z] and _' do
      expect(build(:question, value: 'campo-inválido')).to be_invalid
    end

    it 'fails if the type is not present' do
      expect(build(:question, type: nil)).to be_invalid
    end

    it 'fails if the type is not present' do
      expect(build(:question, type: '')).to be_invalid
    end

    context 'for dropdown type' do
      it 'fails if it does not have any option' do
        expect(build(:question, type: :dropdown, options: nil)).to be_invalid
      end

      it 'fails if the option does not have label' do
        expect(build(:question, type: :dropdown, options: [{ value: 'foo' }])).to be_invalid
      end

      it 'fails if the option does not have value' do
        expect(build(:question, type: :dropdown, options: [{ label: 'foo' }])).to be_invalid
      end

      it 'fails if the option contains an invalid field' do
        expect(build(:question, type: :dropdown, options: [{ label: 'foo', foo: 'foo' }])).to be_invalid
      end

      it 'fails if the option value contains characters other than [a-zA-Z_-]' do
        expect(build(:question, type: :dropdown, options: [{ label: 'foo', value: 'opção/inválida!' }])).to be_invalid
      end

      it 'passes' do
        expect(build(:question, type: :dropdown, options: [{ label: 'foo', value: 'bar' }])).to be_valid
      end
    end

    it 'passes' do
      expect(build(:question)).to be_valid
    end
  end

  describe 'before validation' do
    it 'removes the starting and end \'_\' from the value and upcases it' do
      question = build(:question, value: '_foo_bar_').tap(&:valid?)

      expect(question.value).to eq('FOO_BAR')
    end

    context 'when the question is not a dropdown' do
      it 'removes any options given' do
        question = build(:question, type: Question.types.except(:dropdown).to_a.sample[0], options: [{ label: 'foo', value: 'bar' }]).tap(&:valid?)

        expect(question.options).to be_blank
      end
    end
  end

  describe '#as_variable' do
    it 'returns the question value with trailing underscores' do
      expect(build(:question, value: 'FOO_BAR_BAZ').as_variable).to eq('_FOO_BAR_BAZ_')
    end
  end

  describe '#options' do
    context 'for non dropdown question' do
      it 'returns nil' do
        expect(create(:question, type: Question.types.except(:dropdown).to_a.sample[0]).options).to be_nil
      end
    end

    context 'for a dropdown question' do
      let(:option1) { { 'label' => 'bar', 'value' => 'foo' } }
      let(:option2) { { 'label' => 'foo', 'value' => 'bar' } }
      let(:option3) { { 'label' => 'art', 'value' => 'art' } }

      let(:options) { [option1, option2, option3] }

      it 'returns the options ordered by label' do
        expect(create(:question, type: :dropdown, options: options).options).to eq([option3, option1, option2])
      end
    end
  end
end
