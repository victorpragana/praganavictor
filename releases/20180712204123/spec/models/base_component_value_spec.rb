# == Schema Information
#
# Table name: base_component_values
#
#  id                      :integer          not null, primary key
#  base_component_id       :integer
#  value_id                :integer
#  base_component_value_id :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  deleted_at              :datetime
#

require 'rails_helper'

RSpec.describe BaseComponentValue, type: :model do
  describe "associations" do
    it { should belong_to(:base_component) }
    it { should belong_to(:value) }
    it { should belong_to(:parent).class_name("BaseComponentValue") }
    it { should have_many(:children).class_name("BaseComponentValue") }
    it { should delegate_method(:specification_id).to(:value) }
    it { should delegate_method(:automatic?).to(:value) }
  end

  context "validation" do
    before(:each) do
      @category = FactoryGirl.create(:category)
      @base_component = FactoryGirl.create(:base_component, :category => @category)

      @specification = FactoryGirl.create(:specification)
      @value = FactoryGirl.create(:value, :specification => @specification)
    end

    it "fails if the base component is not present" do
      expect(FactoryGirl.build(:base_component_value, :base_component => nil, :value => @value)).to be_invalid
    end

    it "fails if the value is not present" do
      expect(FactoryGirl.build(:base_component_value, :base_component => @base_component, :value => nil)).to be_invalid
    end

    it "should pass" do
      expect(FactoryGirl.build(:base_component_value, :base_component => @base_component, :value => @value)).to be_valid
    end
  end
end
