# == Schema Information
#
# Table name: dimensions
#
#  id         :integer          not null, primary key
#  width      :int4range        not null
#  height     :int4range        not null
#  line_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  weight     :integer          default(0), not null
#

require 'rails_helper'

RSpec.describe Dimension, type: :model do
  before(:each) do
    @family = FactoryGirl.create(:family)
    @line = FactoryGirl.create(:line, family: @family)
  end

  describe "associations" do
    it { should belong_to(:line) }
  end

  describe 'enums' do
    it { should define_enum_for(:weight).with({
      general: 0,
      light:   1,
      medium:  2,
      heavy:   3
    }) }
  end

  describe "conversions" do
    it { expect(described_class.new).to respond_to(:width_from_mm_to_cm) }
    it { expect(described_class.new).to respond_to(:height_from_mm_to_cm) }
  end

  describe "validation" do
    it "fails if the line is not present" do
      expect(FactoryGirl.build(:dimension, line: nil)).to be_invalid
    end

    it "fails if the width is not present" do
      expect(FactoryGirl.build(:dimension, width: nil, line: @line)).to be_invalid
    end

    it "fails if the height is not present" do
      expect(FactoryGirl.build(:dimension, height: nil, line: @line)).to be_invalid
    end

    it "fails if there is already a dimension that overlaps with another for the same weight" do
      FactoryGirl.create(:dimension, line: @line, width: 1..5, height: 1..2, weight: :general)
      FactoryGirl.create(:dimension, line: @line, width: 7..9, height: 3..4, weight: :general)

      expect(FactoryGirl.build(:dimension, line: @line, width: 6..7, height: 1..3, weight: :general)).to be_invalid
    end

    it "fails if there is already a dimension that overlaps with another for the same weight" do
      FactoryGirl.create(:dimension, line: @line, height: 1..5, width: 1..2, weight: :general)
      FactoryGirl.create(:dimension, line: @line, height: 7..9, width: 3..4, weight: :general)

      expect(FactoryGirl.build(:dimension, line: @line, height: 6..7, width: 1..3, weight: :general)).to be_invalid
    end

    it 'fails if the weight is not present' do
      expect(FactoryGirl.build(:dimension, line: @line, height: 6..7, width: 1..3, weight: nil)).to be_invalid
    end

    it "passes" do
      FactoryGirl.create(:dimension, line: @line, width: 1..5, height: 1..2)
      FactoryGirl.create(:dimension, line: @line, width: 7..9, height: 3..4)

      expect(FactoryGirl.build(:dimension, line: @line, width: 6..6, height: 1..4)).to be_valid
    end

    it "passes" do
      FactoryGirl.create(:dimension, line: @line, width: 1..5, height: 1..2)
      FactoryGirl.create(:dimension, line: @line, width: 7..9, height: 3..4)

      expect(FactoryGirl.build(:dimension, line: @line, width: 7..9, height: 1..2)).to be_valid
    end

    it "passes even if there is already a dimension that overlaps with another for the different weight" do
      FactoryGirl.create(:dimension, line: @line, width: 1..5, height: 1..2, weight: :general)
      FactoryGirl.create(:dimension, line: @line, width: 7..9, height: 3..4, weight: :general)

      expect(FactoryGirl.build(:dimension, line: @line, width: 6..7, height: 1..3, weight: :light)).to be_valid
    end

    it "passes even if there is already a dimension that overlaps with another for the different weight" do
      FactoryGirl.create(:dimension, line: @line, height: 1..5, width: 1..2, weight: :general)
      FactoryGirl.create(:dimension, line: @line, height: 7..9, width: 3..4, weight: :general)

      expect(FactoryGirl.build(:dimension, line: @line, height: 6..7, width: 1..3, weight: :light)).to be_valid
    end
  end
end
