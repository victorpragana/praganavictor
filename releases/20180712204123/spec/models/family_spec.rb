# == Schema Information
#
# Table name: families
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  color      :string(7)        not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Family, type: :model do
  describe "associations" do
    it { should have_many(:lines) }
    it { should have_and_belong_to_many(:components) }
  end

  describe "validation" do
    it "fails if the name is not present" do
      expect(FactoryGirl.build(:family, :name => nil)).to be_invalid
    end

    it "fails if the name is blank" do
      expect(FactoryGirl.build(:family, :name => "")).to be_invalid
    end

    it "fails if the name already exists" do
      FactoryGirl.create(:family, :name => "foo")

      expect(FactoryGirl.build(:family, :name => "FoO")).to be_invalid
    end

    it "fails if the color is not present" do
      expect(FactoryGirl.build(:family, :color => nil)).to be_invalid
    end

    it "fails if the color is blank" do
      expect(FactoryGirl.build(:family, :color => nil)).to be_invalid
    end

    it "fails if the color already exists" do
      FactoryGirl.create(:family, :color => "#AAAAAA")

      expect(FactoryGirl.build(:family, :color => "#aaAAaa")).to be_invalid
    end

    it "fails if the color is invalid" do
      expect(FactoryGirl.build(:family, :color => "322")).to be_invalid
    end

    it "should pass" do
      expect(FactoryGirl.build(:family)).to be_valid
    end
  end
end
