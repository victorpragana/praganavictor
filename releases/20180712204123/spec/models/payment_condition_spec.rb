# == Schema Information
#
# Table name: payment_conditions
#
#  id                      :integer          not null, primary key
#  installments            :integer          not null
#  additional_tax          :decimal(5, 2)    default(0.0)
#  antecipation_tax        :decimal(5, 2)    default(0.0)
#  credit_antecipation_tax :decimal(5, 2)    default(0.0)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  type                    :integer          default(0), not null
#  credit_tax              :decimal(5, 2)    default(0.0)
#

require 'rails_helper'

RSpec.describe PaymentCondition, type: :model do
  describe 'enums' do
    it do
      is_expected.to define_enum_for(:type).with(
        credit_card: 0,
        bank_slip:   1
      )
    end
  end

  describe 'validations' do
    it 'fails if the type is not present' do
      expect(build(:payment_condition, type: nil)).to be_invalid
    end

    it 'fails if the type is not present' do
      expect(build(:payment_condition, type: '')).to be_invalid
    end

    it 'fails if the installments is not present' do
      expect(build(:payment_condition, installments: nil)).to be_invalid
    end

    it 'fails if the installments is not present' do
      expect(build(:payment_condition, installments: '')).to be_invalid
    end

    it 'fails if the installments are zero' do
      expect(build(:payment_condition, installments: 0)).to be_invalid
    end

    it 'fails if the installments are negative' do
      expect(build(:payment_condition, installments: -1)).to be_invalid
    end

    it 'fails if the installments are not an integer' do
      expect(build(:payment_condition, installments: BigDecimal.new('1.01'))).to be_invalid
    end

    it 'fails if already exists a payment condition for the same type and installments number' do
      create(:payment_condition, installments: 5, type: :credit_card)
      expect(build(:payment_condition, installments: 5, type: :credit_card)).to be_invalid
    end

    it 'fails if the additional_tax is negative' do
      expect(build(:payment_condition, additional_tax: -1)).to be_invalid
    end

    it 'fails if the additional_tax is greater than 100' do
      expect(build(:payment_condition, additional_tax: BigDecimal.new('100.01'))).to be_invalid
    end

    it 'fails if the credit_tax is negative' do
      expect(build(:payment_condition, credit_tax: -1)).to be_invalid
    end

    it 'fails if the credit_tax is greater than 100' do
      expect(build(:payment_condition, credit_tax: BigDecimal.new('100.01'))).to be_invalid
    end

    it 'fails if the antecipation_tax is negative' do
      expect(build(:payment_condition, antecipation_tax: -1)).to be_invalid
    end

    it 'fails if the antecipation_tax is greater than 100' do
      expect(build(:payment_condition, antecipation_tax: BigDecimal.new('100.01'))).to be_invalid
    end

    it 'fails if the credit_antecipation_tax is negative' do
      expect(build(:payment_condition, credit_antecipation_tax: -1)).to be_invalid
    end

    it 'fails if the credit_antecipation_tax is greater than 100' do
      expect(build(:payment_condition, credit_antecipation_tax: BigDecimal.new('100.01'))).to be_invalid
    end

    it 'fails if already exists a bank slip payment condition' do
      create(:payment_condition, type: :bank_slip)

      expect(build(:payment_condition, type: :bank_slip)).to be_invalid
    end

    it 'passes' do
      expect(build(:payment_condition)).to be_valid
    end
  end

  describe '#apply_additional_tax' do
    it 'calculates the new total based on the additional tax' do
      expect(build(:payment_condition, additional_tax: BigDecimal.new('56.98')).apply_additional_tax(1500)).to eq(BigDecimal.new('2354.70'))
    end
  end

  describe '#apply_credit_tax' do
    it 'calculates the new total based on the credit tax' do
      expect(build(:payment_condition, credit_tax: BigDecimal.new('27.98')).apply_credit_tax(1352)).to eq(BigDecimal.new('1730.2896'))
    end
  end

  describe '#apply_antecipation_tax' do
    it 'calculates the new total based on the antecipation tax' do
      expect(build(:payment_condition, antecipation_tax: BigDecimal.new('26.77')).apply_antecipation_tax(1350)).to eq(BigDecimal.new('988.61'))
    end
  end

  describe '#apply_credit_antecipation_tax' do
    it 'calculates the new total based on the credit antecipation tax' do
      expect(build(:payment_condition, credit_antecipation_tax: BigDecimal.new('23.33')).apply_credit_antecipation_tax(BigDecimal.new('1785.89'))).to eq(BigDecimal.new('1369.24'))
    end
  end
end
