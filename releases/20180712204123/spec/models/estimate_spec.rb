# == Schema Information
#
# Table name: estimates
#
#  id                 :integer          not null, primary key
#  date               :date             not null
#  observation        :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  order_id           :integer
#  seller_id          :integer
#  seller_type        :string
#  client_id          :integer
#  client_type        :string
#  optimize           :boolean          default(FALSE)
#  discount           :decimal(5, 2)    default(0.0)
#  status             :integer          default(0), not null
#  job_id             :string
#  processing_message :text             default([]), is an Array
#

require 'rails_helper'

RSpec.describe Estimate, type: :model do
  describe 'associations' do
    it { is_expected.to have_many(:environments).dependent(:destroy) }
    it { is_expected.to have_many(:locations).through(:environments) }
    it { is_expected.to have_many(:options).through(:locations) }
    it { is_expected.to have_many(:components).through(:options) }
    it { is_expected.to have_many(:items).class_name(EstimateItem).dependent(:destroy) }
    it { is_expected.to have_many(:packing_configurations) }
    it { is_expected.to belong_to(:order) }
    it { is_expected.to belong_to(:seller) }
    it { is_expected.to belong_to(:client) }
  end

  describe 'enums' do
    it { is_expected.to define_enum_for(:status).with({
      free:           0, 
      enqueued:       1, 
      processing:     2,
      error:          3
    }) }
  end

  context 'validation' do
    before do
      @distributor = create(:distributor, name: 'D1')
      @store       = create(:store, distributor: @distributor, name: 'S1')
      @customer    = create(:customer, store_id: @store.id, special_discount: true)
    end

    it 'fails if the seller is not present' do
      expect(build(:estimate, seller: nil, client: @customer)).to be_invalid
    end

    it 'fails if the client is not present' do
      expect(build(:estimate, seller: @store, client: nil)).to be_invalid
    end

    it 'fails if the date is not present' do
      expect(build(:estimate, seller: @store, date: nil, client: @customer)).to be_invalid
    end

    it 'fails if the date is blank' do
      expect(build(:estimate, seller: @store, date: '', client: @customer)).to be_invalid
    end

    it 'fails if the discount is negative' do
      expect(build(:estimate, seller: @store, client: @customer, discount: -1)).to be_invalid
    end

    it 'fails if the discount is greater than 100' do
      expect(build(:estimate, seller: @store, client: @customer, discount: BigDecimal.new('100.1'))).to be_invalid
    end

    it 'passes' do
      expect(build(:estimate, seller: @store, client: @customer)).to be_valid
    end
  end

  context 'totals' do
    before do
      @factory     = create(:factory, name: 'F1')
      @distributor = create(:distributor, name: 'D1')
      @store       = create(:store, distributor: @distributor, name: 'S1')
      @customer    = create(:customer, store_id: @store.id)

      @estimate    = create(:estimate, seller: @factory, client: @customer)

      @environment = create(:environment, estimate: @estimate)
      @location = create(:location, environment: @environment)

      @family = create(:family)
      @line = create(:line, family: @family, sale_price: BigDecimal.new('12.50'))

      @price_table = create(:price_table, owner: @factory, default: true, type: :for_lines)
      create(:price_table_line, line: @line, price: '_CMV_*2', price_table: @price_table, maximum_discount: BigDecimal.new('50.1'))

      @item = create(:item)

      @price_table_item = create(:price_table, owner: @factory, default: true, type: :for_items, name: 'PTI1')
      create(:price_table_item, item: @item, price: '200', price_table: @price_table_item, maximum_discount: BigDecimal.new('25.8'))

      create(:option, line: @line, location: @location, selected: true, factory_total: BigDecimal.new('100.22'), distributor_total: 120,
        store_total: 140, customer_total: 160)
      create(:option, line: @line, location: @location, selected: true, factory_total: 80, distributor_total: 100,
        store_total: 160, customer_total: 200, discount: BigDecimal.new('32.50'))
      create(:option, line: @line, location: @location, selected: false, factory_total: 100, distributor_total: 120,
        store_total: 140, customer_total: 160)

      create(:estimate_item, estimate: @estimate, item: @item, quantity: 3, factory_value: BigDecimal.new('52.22'), distributor_value: BigDecimal.new('60.36'),
        store_value: BigDecimal.new('68.55'), customer_value: BigDecimal.new('72.69'), discount: BigDecimal.new('25.30'))
    end

    describe '#factory_total' do
      it 'sums the factory totals of the selected options and items without discounts' do
        expect(@estimate.factory_total).to eq(BigDecimal.new('336.88'))
      end
    end

    describe '#factory_total_with_discount' do
      it 'sums the factory totals of the selected options and items with discounts' do
        expect(@estimate.factory_total_with_discount).to eq(BigDecimal.new('271.25'))
      end
    end

    describe '#factory_total_discount' do
      it 'sums the total discount for the selected options and items' do
        expect(@estimate.factory_total_discount).to eq(BigDecimal.new('65.63'))
      end
    end

    describe '#distributor_total' do
      it 'sums the distributor totals of the selected options and items without discounts' do
        expect(@estimate.distributor_total).to eq(BigDecimal.new('401.08'))
      end
    end

    describe '#distributor_total_with_discount' do
      it 'sums the distributor totals of the selected options and items with discounts' do
        expect(@estimate.distributor_total_with_discount).to eq(BigDecimal.new('322.77'))
      end
    end

    describe '#distributor_total_discount' do
      it 'sums the total discount for the selected options and items' do
        expect(@estimate.distributor_total_discount).to eq(BigDecimal.new('78.31'))
      end
    end

    describe '#store_total' do
      it 'sums the store totals of the selected options and items with discounts' do
        expect(@estimate.store_total).to eq(BigDecimal.new('505.65'))
      end
    end

    describe '#store_total_with_discount' do
      it 'sums the store totals of the selected options and items without discounts' do
        expect(@estimate.store_total_with_discount).to eq(BigDecimal.new('401.62'))
      end
    end

    describe '#store_total_discount' do
      it 'sums the total discount for the selected options and items' do
        expect(@estimate.store_total_discount).to eq(BigDecimal.new('104.03'))
      end
    end

    describe '#customer_total' do
      it 'sums the customer totals of the selected options and items without discounts' do
        expect(@estimate.customer_total).to eq(BigDecimal.new('578.07'))
      end
    end

    describe '#customer_total_with_discount' do
      it 'sums the customer totals of the selected options and items with discounts' do
        expect(@estimate.customer_total_with_discount).to eq(BigDecimal.new('457.90'))
      end
    end

    describe '#customer_total_discount' do
      it 'sums the total discount for the selected options and items' do
        expect(@estimate.customer_total_discount).to eq(BigDecimal.new('120.17'))
      end
    end

    describe '#total' do
      context 'for a distributor client' do
        before do
          @estimate.update(client: @distributor)
        end

        it 'returns the distributor total' do
          expect(@estimate.total).to eq(BigDecimal.new('401.08'))
        end
      end

      context 'for a store client' do
        before do
          @estimate.update(client: @store)
        end

        it 'returns the store total' do
          expect(@estimate.total).to eq(BigDecimal.new('505.65'))
        end
      end

      context 'for a customer client' do
        before do
          @estimate.update(client: @customer)
        end

        it 'returns the customer total' do
          expect(@estimate.total).to eq(BigDecimal.new('578.07'))
        end
      end
    end

    describe '#partial_total' do
      context 'for a distributor client' do
        before do
          @estimate.update(client: @distributor)
        end

        it 'returns the distributor total' do
          expect(@estimate.partial_total).to eq(BigDecimal.new('322.77'))
        end
      end

      context 'for a store client' do
        before do
          @estimate.update(client: @store)
        end

        it 'returns the store total' do
          expect(@estimate.partial_total).to eq(BigDecimal.new('401.62'))
        end
      end

      context 'for a customer client' do
        before do
          @estimate.update(client: @customer)
        end

        it 'returns the customer total' do
          expect(@estimate.partial_total).to eq(BigDecimal.new('457.90'))
        end
      end
    end

    describe '#total_with_discount' do
      context 'for a distributor client' do
        before do
          @estimate.update(client: @distributor, discount: BigDecimal.new('25'))
        end

        it 'returns the distributor total with the total discount' do
          expect(@estimate.total_with_discount).to eq(BigDecimal.new('242.08'))
        end
      end

      context 'for a store client' do
        before do
          @estimate.update(client: @store, discount: BigDecimal.new('26'))
        end

        it 'returns the store total' do
          expect(@estimate.total_with_discount).to eq(BigDecimal.new('297.20'))
        end
      end

      context 'for a customer client' do
        before do
          @estimate.update(client: @customer, discount: BigDecimal.new('27'))
        end

        it 'returns the customer total' do
          expect(@estimate.total_with_discount).to eq(BigDecimal.new('334.27'))
        end
      end
    end

    describe '#total_discount' do
      context 'for a distributor client' do
        before do
          @estimate.update(client: @distributor, discount: BigDecimal.new('25'))
        end

        it 'sums the distributor total discount with the estimate discount' do
          expect(@estimate.total_discount).to eq(BigDecimal.new('159.00'))
        end
      end

      context 'for a store client' do
        before do
          @estimate.update(client: @store, discount: BigDecimal.new('26'))
        end

        it 'sums the store total discount with the estimate discount' do
          expect(@estimate.total_discount).to eq(BigDecimal.new('208.45'))
        end
      end

      context 'for a customer client' do
        before do
          @estimate.update(client: @customer, discount: BigDecimal.new('27'))
        end

        it 'sums the customer total discount with the estimate discount' do
          expect(@estimate.total_discount).to eq(BigDecimal.new('243.80'))
        end
      end
    end
  end
end
