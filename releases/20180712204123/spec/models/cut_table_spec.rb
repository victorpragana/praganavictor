# == Schema Information
#
# Table name: cut_tables
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  size       :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe CutTable, type: :model do
  describe 'conversions' do
    it { expect(described_class.new).to respond_to(:size_from_mm_to_cm) }
  end

  context 'validation' do
    it 'fails if the name is not present' do
      expect(FactoryGirl.build(:cut_table, name: nil)).to be_invalid
    end

    it 'fails if the name is blank' do
      expect(FactoryGirl.build(:cut_table, name: '')).to be_invalid
    end

    it 'fails if the size is not present' do
      expect(FactoryGirl.build(:cut_table, size: nil)).to be_invalid
    end

    it 'fails if the size is negative' do
      expect(FactoryGirl.build(:cut_table, size: -1)).to be_invalid
    end

    it 'fails if the size is zero' do
      expect(FactoryGirl.build(:cut_table, size: 0)).to be_invalid
    end

    it 'passes' do
      expect(FactoryGirl.build(:cut_table)).to be_valid
    end
  end
end
