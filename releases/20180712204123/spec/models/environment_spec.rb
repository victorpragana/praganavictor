# == Schema Information
#
# Table name: environments
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  category    :integer          not null
#  estimate_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe Environment, type: :model do
  before(:each) do
    @distributor = FactoryGirl.create(:distributor)
    @store       = FactoryGirl.create(:store, distributor: @distributor)
    @customer    = FactoryGirl.create(:customer, store_id: @store.id)
    @estimate    = FactoryGirl.create(:estimate, seller: @store, client: @customer)
  end

  describe "associations" do
    it { should belong_to(:estimate) }
    it { should have_many(:locations).dependent(:destroy) }
  end

  context "validation" do
    it "fails if the estimate is not present" do
      expect(FactoryGirl.build(:environment, estimate: nil)).to be_invalid
    end

    it "fails if the category is not present" do
      expect(FactoryGirl.build(:environment, estimate: @estimate, category: nil)).to be_invalid
    end

    it "fails if the category is blank" do
      expect(FactoryGirl.build(:environment, estimate: @estimate, category: "")).to be_invalid
    end

    it "should pass" do
      expect(FactoryGirl.build(:environment, estimate: @estimate)).to be_valid
    end
  end
end
