require 'rails_helper'

RSpec.describe SpecificationService, type: :service do
  describe "create" do
    let(:service) {described_class.new(parameters)}

    context "with invalid parameters" do
      let(:parameters) {{"name" => "", "automatic" => "false"}}

      it "does not create a new Specification" do
        expect {
          service.create
        }.to change(Specification, :count).by(0)
      end

      it "should return false" do
        service.create
        
        expect(service.success).to eq(false)
      end

      it "should return the errors" do
        service.create
        
        expect(service.errors).to_not be_empty
      end
    end

    context "with valid parameters" do
      let(:parameters) {{"name" => "Cor", "automatic" => "true", "used_in_filter" => "true", "values" => [
        {"value" => "Verde", "comparison_value" => "1"}, {"value" => "Amarelo", "comparison_value" => "2"}, {"value" => "Vermelho", "comparison_value" => "3"}
      ]}}

      context "for a new specification" do
        it "should create a new Specification" do
          expect {
            service.create
          }.to change(Specification, :count).by(1)
        end
      end

      context "for a deleted specification" do
        before(:each) do
          @spec = FactoryGirl.create(:specification, name: "Cor", deleted_at: Time.now)
        end

        it "does not create a new Specification" do
          expect {
            service.create
          }.to change(Specification.with_deleted, :count).by(0)
        end

        it "should restore the specification" do
          service.create

          @spec.reload

          expect(@spec.deleted_at).to be_nil
        end
      end

      context "with three new values" do
        it "should create the three new values" do
          expect {
            service.create
          }.to change(Value, :count).by(3)
        end
      end

      it "should return the created Specification" do
        service.create

        expect(service.record).to eq(Specification.last)
      end

      it "should return true" do
        service.create
        
        expect(service.success).to eq(true)
      end

      it "populates the data" do
        service.create

        specification = service.record

        expect(specification.name).to eq("Cor")
        expect(specification.automatic?).to eq(true)
        expect(specification.used_in_filter?).to eq(true)

        expect(specification.values.count).to eq(3)
        expect(specification.values[0].value).to eq("Verde")
        expect(specification.values[0].comparison_value).to eq(1)
        expect(specification.values[1].value).to eq("Amarelo")
        expect(specification.values[1].comparison_value).to eq(2)
        expect(specification.values[2].value).to eq("Vermelho")
        expect(specification.values[2].comparison_value).to eq(3)
      end
    end
  end

  describe "update" do
    let(:service) {described_class.new(parameters)}

    before(:each) do
      @specification = FactoryGirl.create(:specification, automatic: false, used_in_filter: false)
    end

    context "with invalid parameters" do
      let(:parameters) {{"name" => "", "automatic" => "false"}}

      it "should return false" do
        service.update(@specification.id)
        
        expect(service.success).to eq(false)
      end

      it "should return the errors" do
        service.update(@specification.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context "with valid parameters" do
      let(:parameters) {{"name" => "Cor", "automatic" => "true", "used_in_filter" => "true", "values" => [
        {"value" => "Verde", "comparison_value" => "1"}, {"value" => "Amarelo", "comparison_value" => "1"}, {"value" => "Vermelho", "comparison_value" => "1"}
      ]}}

      it "does not create a new Specification" do
        expect {
          service.update(@specification.id)
        }.to change(Specification, :count).by(0)
      end

      context "with a new value" do
        before(:each) do
          @value_1 = FactoryGirl.create(:value, specification: @specification, value: "Verde", comparison_value: 2)
          @value_2 = FactoryGirl.create(:value, specification: @specification, value: "Vermelho", comparison_value: 3)
        end

        it "should create one new value" do
          expect {
            service.update(@specification.id)
          }.to change(Value, :count).by(1)
        end
      end

      context "removing a value" do
        let(:parameters) {{"name" => "Cor", "values" => [
          {"value" => "Verde"}
        ]}}

        before(:each) do
          @value_1 = FactoryGirl.create(:value, specification: @specification, value: "Verde", comparison_value: 2)
          @value_2 = FactoryGirl.create(:value, specification: @specification, value: "Vermelho", comparison_value: 2)
        end

        it "should logically delete a value" do
          service.update(@specification.id)

          @value_2.reload

          expect(@specification.values.count).to eq(1)
          expect(@value_2.deleted_at).to_not be_nil
        end
      end

      context "adding a deleted value" do
        let(:parameters) {{"name" => "Cor", "values" => [
          {"value" => "Verde", "comparison_value" => "2"}, {"value" => "Vermelho", "comparison_value" => "3"}
        ]}}

        before(:each) do
          @value_1 = FactoryGirl.create(:value, specification: @specification, value: "Verde", comparison_value: 2)
          @value_2 = FactoryGirl.create(:value, specification: @specification, value: "Vermelho", comparison_value: 2, deleted_at: Time.now)
        end

        it "does not create a new value" do
          expect {
            service.update(@specification.id)
          }.to_not change{Value.unscoped.count}
        end

        it "should enable the deleted value" do
          service.update(@specification.id)

          @value_2.reload

          expect(@specification.values.count).to eq(2)
          expect(@value_2.deleted_at).to be_nil
        end
      end

      context "editing a value" do
        let(:parameters) {{"name" => "Cor", "values" => [
          {"value" => "Amarelo", "comparison_value" => "2", "id" => "#{@value_1.id}"}, {"value" => "Vermelho", "comparison_value" => "3", "id" => "#{@value_2.id}"}
        ]}}

        before(:each) do
          @value_1 = FactoryGirl.create(:value, specification: @specification, value: "Verde", comparison_value: 2)
          @value_2 = FactoryGirl.create(:value, specification: @specification, value: "Vermelho", comparison_value: 2, deleted_at: Time.now)
        end

        it "does not create a new value" do
          expect {
            service.update(@specification.id)
          }.to_not change{Value.unscoped.count}
        end

        it "should edit the value" do
          service.update(@specification.id)

          @value_1.reload

          expect(@specification.values.count).to eq(2)
          expect(@value_1.value).to eq('Amarelo')
        end
      end

      it "should return the specification" do
        service.update(@specification.id)

        expect(service.record).to eq(Specification.last)
      end

      it "should return true" do
        service.update(@specification.id)
        
        expect(service.success).to eq(true)
      end

      it "populates the data" do
        service.update(@specification.id)

        specification = service.record

        expect(specification.name).to eq("Cor")
        expect(specification.automatic?).to eq(true)
        expect(specification.used_in_filter?).to eq(true)
      end
    end
  end
end
