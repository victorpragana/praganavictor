require 'rails_helper'

RSpec.describe OrderService, type: :service do
  before do
    @factory = create(:factory, name: 'F1')
    create(:administrator, username: 'F1').tap do |administrator|
      administrator.update!(selected_workplace: create(:user_workplace, workplace: @factory, user: administrator))
    end

    @family = create(:family)
    
    @category = create(:category, name: 'C1')
    @category_2 = create(:category, name: 'C2')
    @category_3 = create(:category, name: 'C3')

    @line = create(:line, family: @family)

    @line.categories << build(:line_category, category: @category, required: true)
    @line.categories << build(:line_category, category: @category_3, required: false)

    @line.save!

    @component_1 = create(:component, unit: :cm, category: @category, erasable: true, cost: BigDecimal.new('32.28'), name: 'CC1')
    @component_2 = create(:component, unit: :cm, category: @category_3, erasable: true, name: 'CC2')
    @component_3 = create(:component, unit: :cm, category: @category_3, erasable: false, name: 'CC3')
    @component_4 = create(:component, unit: :cm, category: @category_2, erasable: false, name: 'CC4')

    @line_rule = create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: 5..30, 
      total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_+2', width_consumption: '_ALTURA_*3', 
      height_consumption: '_LARGURA_ + 3', quantity_consumption: '_ALTURA_*2')
    @rule = create(:rule, line_rule: @line_rule, component: @component_1, control_method: Rule.control_methods[:width], 
      width: 5..30, total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_+2', width_consumption: '_ALTURA_*3', 
      height_consumption: '_LARGURA_ + 3', quantity_consumption: '_ALTURA_*2')

    @distributor = create(:distributor, name: 'D1')
    @store       = create(:store, distributor: @distributor, name: 'S1')
    @customer    = create(:customer, store_id: @store.id, name: 'C1', allow_special_discount: true)

    @order = create(:order, environments: [], seller: @store, client: @customer, billing_client: @customer, user: @factory.users.first,
      status: :to_send, production_status: :not_started)
  end

  describe '#change_billing_client' do
    let(:service) { described_class.new(data) }
    let(:data)    { { 'billing_client_id' => @distributor.id, 'billing_client_type' => 'Distributor' } }

    context 'with valid data' do
      it 'returns true' do
        service.change_billing_client(@order.id)

        expect(service.success).to eq(true)
      end

      it 'updates billing client' do
        service.change_billing_client(@order.id)

        expect(service.record.billing_client).to eq(@distributor)
      end
    end

    context 'with invalid data' do
      before do
        @order.update(status: Order.statuses[:sending])
      end

      it 'returns false' do
        service.change_billing_client(@order.id)

        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.change_billing_client(@order.id)

        expect(service.errors).to_not be_empty
      end
    end
  end

  describe '#update_special_discount' do
    let(:service) { described_class.new(data) }
    let(:data)    { { 'special_discount' => '50.85' } }

    context 'with valid data' do
      it 'returns true' do
        service.update_special_discount(@order.id)

        expect(service.success).to eq(true)
      end

      it 'updates the special discount' do
        service.update_special_discount(@order.id)

        expect(service.record.special_discount).to eq(BigDecimal.new('50.85'))
      end
    end

    context 'with invalid data' do
      before do
        @order.update(status: Order.statuses[:sending])
      end

      it 'returns false' do
        service.update_special_discount(@order.id)

        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.update_special_discount(@order.id)

        expect(service.errors).to_not be_empty
      end
    end
  end

  describe '#destroy' do
    let(:service) { described_class.new }
    let(:trello_integration_service) {double(:trello_integration_service, delete_trello_card: true)}

    before do
      allow(TrelloIntegrationService).to receive(:new).and_return(trello_integration_service)
    end

    context 'with valid data' do
      it 'returns true' do
        service.destroy(@order.id)

        expect(service.success).to eq(true)
      end

      it "destroys the trello card" do
        expect(trello_integration_service).to receive(:delete_trello_card)
        
        service.destroy(@order.id)
      end

      it 'destroys the order' do
        expect {
          service.destroy(@order.id)
        }.to change(Order, :count).by(-1)
      end
    end

    context 'with invalid data' do
      before do
        @order.update(status: Order.statuses[:sending])
      end

      it 'returns false' do
        service.destroy(@order.id)

        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.destroy(@order.id)

        expect(service.errors).to_not be_empty
      end
    end
  end

  describe '#update_production_status' do
    let(:service) { described_class.new(data) }
    let(:data) { { 'transition' => transition } }
    let(:transition) { 'send_to_factory' }

    context 'with valid data' do
      before do
        allow(Order).to receive(:find).and_return(@order)

        allow(@order).to receive(:balance_value).and_return(1000)
        allow(@order).to receive(:factory_total_with_discount).and_return(1500)
        allow(@order).to receive(:distributor_total_with_discount).and_return(1800)
        allow(@order).to receive(:store_total_with_discount).and_return(2000)
      end

      it 'calls the transition bang method' do
        expect_any_instance_of(Order).to receive("#{transition}!").and_call_original

        service.update_production_status(@order.id)
      end

      it 'returns true' do
        service.update_production_status(@order.id)

        expect(service.success).to eq(true)
      end
    end

    context 'with invalid data' do
      let(:transition) { 'receive' }

      it 'returns false' do
        service.update_production_status(@order.id)

        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.update_production_status(@order.id)

        expect(service.errors).to_not be_empty
      end
    end
  end
end