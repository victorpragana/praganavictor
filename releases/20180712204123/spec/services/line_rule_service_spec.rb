require 'rails_helper'

RSpec.describe LineRuleService, type: :service do
  describe "setting rules" do
    before do
      @family = FactoryGirl.create(:family)
      
      @category = FactoryGirl.create(:category, name: 'C1')
      @category2 = FactoryGirl.create(:category, name: 'C2')
      @category3 = FactoryGirl.create(:category, name: 'C3')

      @line = FactoryGirl.create(:line, family: @family)

      @line.dimensions << FactoryGirl.build(:dimension, height: 10..50, width: 10..20)
      @line.dimensions << FactoryGirl.build(:dimension, height: 70..90, width: 30..40)
      @line.dimensions << FactoryGirl.build(:dimension, height: 60..60, width: 10..40)

      @line.categories << FactoryGirl.build(:line_category, category: @category)
      @line.categories << FactoryGirl.build(:line_category, category: @category2)

      @line.save!

      @specification = FactoryGirl.build(:specification, automatic: true)
      @specification.values << FactoryGirl.build(:value, value: 'V4', comparison_value: 1)
      @specification.save!

      @component_1 = FactoryGirl.create(:component, category: @category)
      @component_2 = FactoryGirl.create(:component, category: @category2)
      @component_3 = FactoryGirl.create(:component, category: @category3)
      @component_4 = FactoryGirl.create(:component, category: @category3)
      @component_5 = FactoryGirl.create(:component, category: @category2, values: @specification.values)
      @component_6 = FactoryGirl.create(:component, category: @category3)
    end

    let(:service) {described_class.new(parameters)}

    context "with invalid parameters" do
      let(:parameters) {[
        {"component_ids"=>[@component_1.id, @component_3.id], "control_method"=>{"id"=>LineRule.control_methods[:width]}, 
          "initial_width"=>5, "final_width"=>20, "initial_height"=>0, "final_height"=>0, "width_consumption"=>"", "height_consumption"=>"", 
          "total_consumption"=>"", "cost"=>"", "quantity_consumption"=>"", "description" => "Fooo"}, 
        {"component_ids"=>[@component_2.id], "control_method"=>{"id"=>""}, "initial_width"=>0, "final_width"=>0, "initial_height"=>5, 
          "final_height"=>1, "width_consumption"=>"", "height_consumption"=>"", "total_consumption"=>"", "cost"=>"",
          "quantity_consumption"=>"", "description" => "Baar"},
      ]}

      it "does not create new Line Rules" do
        expect {
          service.set_line_rules_for(@line.id)
        }.to change(LineRule, :count).by(0)
      end

      it "does not create new Rules" do
        expect {
          service.set_line_rules_for(@line.id)
        }.to change(Rule, :count).by(0)
      end

      it "returns false" do
        service.set_line_rules_for(@line.id)
        
        expect(service.success).to eq(false)
      end

      it "returns the errors" do
        service.set_line_rules_for(@line.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context "with valid parameters" do
      let(:parameters) {[
        {"component_ids"=>[@component_1.id], "required"=>true, "automatic"=>"false", "control_method"=>{"id"=>LineRule.control_methods[:width]}, 
          "initial_width"=>5, "final_width"=>20, "initial_height"=>0, "final_height"=>0, "width_consumption"=>"1", "height_consumption"=>"2", 
          "total_consumption"=>"2 * _LARGURA_", "cost"=>"3 * _ALTURA_", "quantity_consumption"=>"", "description" => "Foo",
          "category_id" => "#{@category.id}", "weight" => "0"},
        {"component_ids"=>[@component_2.id], "required"=>false, "automatic"=>"false", "control_method"=>{"id"=>LineRule.control_methods[:height]},
          "initial_width"=>0, "final_width"=>0, "initial_height"=>5, "final_height"=>10, "width_consumption"=>"3", "height_consumption"=>"4",
          "total_consumption"=>"1", "cost"=>"3", "quantity_consumption"=>"_ALTURA_ * 3", "weight" => "1"},
        {"component_ids"=>[@component_4.id, @component_3.id], "automatic"=>"false", "required"=>false, "control_method"=>{"id"=>LineRule.control_methods[:area]},
          "initial_width"=>2, "final_width"=>10, "initial_height"=>5, "final_height"=>20, "width_consumption"=>"_ALTURA_", "height_consumption"=>"_LARGURA_",
          "total_consumption"=>"_ALTURA_", "cost"=>"2*_ALTURA_", "quantity_consumption"=>"_LARGURA_ + _ALTURA_", "weight" => "2"},
        {"component_ids"=>[@component_5.id], "required"=>true, "automatic"=>"true", "control_method"=>{"id"=>LineRule.control_methods[:width]},
          "initial_width"=>"", "final_width"=>"", "initial_height"=>"", "final_height"=>"", "width_consumption"=>"1", "height_consumption"=>"2",
          "total_consumption"=>"2 * _LARGURA_", "cost"=>"3 * _ALTURA_", "quantity_consumption"=>"", "weight" => "0"},
        {"component_ids"=>[@component_6.id], "required"=>true, "automatic"=>"false", "control_method"=>{"id"=>LineRule.control_methods[:total_area]}, 
          "initial_area"=>5, "final_area"=>20, "initial_height"=>0, "final_height"=>0, "width_consumption"=>"1", "height_consumption"=>"2", 
          "total_consumption"=>"2 * _LARGURA_", "cost"=>"3 * _ALTURA_", "quantity_consumption"=>"", "description" => "Foo",
          "category_id" => "#{@category.id}", "weight" => "0"}
      ]}

      context "for new rules" do
        it "creates five new Line Rules" do
          expect {
            service.set_line_rules_for(@line.id)
          }.to change(LineRule, :count).by(5)
        end

        it "creates six new Rules" do
          expect {
            service.set_line_rules_for(@line.id)
          }.to change(Rule, :count).by(6)
        end

        it "populates the data" do
          service.set_line_rules_for(@line.id)

          line   = service.record
          
          line_rules = line.line_rules.order(id: :asc)

          expect(line.line_rules).to_not be_empty
          expect(line.line_rules.count).to eq(5)

          expect(line_rules[0].width).to eq(5...21)
          expect(line_rules[0].height).to be_nil
          expect(line_rules[0].width_consumption).to eq("1")
          expect(line_rules[0].height_consumption).to eq("2")
          expect(line_rules[0].total_consumption).to eq("2 * _LARGURA_")
          expect(line_rules[0].cost).to eq("3 * _ALTURA_")
          expect(line_rules[0].required).to eq(true)
          expect(line_rules[0].automatic).to eq(false)
          expect(line_rules[0].quantity_consumption).to be_empty
          expect(line_rules[0].description).to eq("Foo")
          expect(line_rules[0].category_id).to eq(@category.id)
          expect(LineRule.control_methods[line_rules[0].control_method]).to eq(LineRule.control_methods[:width])
          expect(line_rules[0].general?).to be_truthy
          
          expect(line_rules[1].width).to be_nil
          expect(line_rules[1].height).to eq(5...11)
          expect(line_rules[1].width_consumption).to eq("3")
          expect(line_rules[1].height_consumption).to eq("4")
          expect(line_rules[1].total_consumption).to eq("1")
          expect(line_rules[1].cost).to eq("3")
          expect(line_rules[1].required).to eq(false)
          expect(line_rules[1].automatic).to eq(false)
          expect(line_rules[1].quantity_consumption).to eq("_ALTURA_ * 3")
          expect(LineRule.control_methods[line_rules[1].control_method]).to eq(LineRule.control_methods[:height])
          expect(line_rules[1].light?).to be_truthy

          expect(line_rules[2].width).to eq(2...11)
          expect(line_rules[2].height).to eq(5...21)
          expect(line_rules[2].width_consumption).to eq("_ALTURA_")
          expect(line_rules[2].height_consumption).to eq("_LARGURA_")
          expect(line_rules[2].total_consumption).to eq("_ALTURA_")
          expect(line_rules[2].cost).to eq("2*_ALTURA_")
          expect(line_rules[2].required).to eq(false)
          expect(line_rules[2].automatic).to eq(false)
          expect(line_rules[2].quantity_consumption).to eq ("_LARGURA_ + _ALTURA_")
          expect(LineRule.control_methods[line_rules[2].control_method]).to eq(LineRule.control_methods[:area])
          expect(line_rules[2].medium?).to be_truthy

          expect(line_rules[3].width).to be_nil
          expect(line_rules[3].height).to be_nil
          expect(line_rules[3].width_consumption).to eq("1")
          expect(line_rules[3].height_consumption).to eq("2")
          expect(line_rules[3].total_consumption).to eq("2 * _LARGURA_")
          expect(line_rules[3].cost).to eq("3 * _ALTURA_")
          expect(line_rules[3].required).to eq(true)
          expect(line_rules[3].automatic).to eq(true)
          expect(line_rules[3].quantity_consumption).to be_empty
          expect(LineRule.control_methods[line_rules[3].control_method]).to eq(LineRule.control_methods[:width])
          expect(line_rules[3].general?).to be_truthy

          expect(line_rules[4].area).to eq(5...21)
          expect(line_rules[4].width).to be_nil
          expect(line_rules[4].height).to be_nil
          expect(line_rules[4].width_consumption).to eq("1")
          expect(line_rules[4].height_consumption).to eq("2")
          expect(line_rules[4].total_consumption).to eq("2 * _LARGURA_")
          expect(line_rules[4].cost).to eq("3 * _ALTURA_")
          expect(line_rules[4].required).to eq(true)
          expect(line_rules[4].automatic).to eq(false)
          expect(line_rules[4].quantity_consumption).to be_empty
          expect(line_rules[4].description).to eq("Foo")
          expect(line_rules[4].category_id).to eq(@category.id)
          expect(LineRule.control_methods[line_rules[4].control_method]).to eq(LineRule.control_methods[:total_area])
          expect(line_rules[4].general?).to be_truthy
        end
      end

      context "for existing rules" do
        let(:parameters) {[
          {"id"=>"#{@line_rule_1.id}", "component_ids"=>[@component_1.id], "required"=>true, "automatic"=>"false", "control_method"=>{"id"=>LineRule.control_methods[:width]}, 
            "initial_width"=>5, "final_width"=>20, "initial_height"=>0, "final_height"=>0, 
            "width_consumption"=>"1", "height_consumption"=>"2", "total_consumption"=>"2 * _LARGURA_", "cost"=>"3 * _ALTURA_",
            "quantity_consumption"=>"", "description" => "Bar", "weight" => "2"},
          {"component_ids"=>[@component_2.id], "required"=>false, "automatic"=>"false", "control_method"=>{"id"=>LineRule.control_methods[:height]},
            "initial_width"=>0, "final_width"=>0, "initial_height"=>5, "final_height"=>10, "width_consumption"=>"3", "height_consumption"=>"4",
            "total_consumption"=>"1", "cost"=>"3", "quantity_consumption"=>"_ALTURA_ * 3", "weight" => "3"},
          {"component_ids"=>[@component_4.id, @component_3.id], "automatic"=>"false", "required"=>false, "control_method"=>{"id"=>LineRule.control_methods[:area]},
            "initial_width"=>2, "final_width"=>10, "initial_height"=>5, "final_height"=>20, "width_consumption"=>"_ALTURA_", "height_consumption"=>"_LARGURA_",
            "total_consumption"=>"_ALTURA_", "cost"=>"2*_ALTURA_", "quantity_consumption"=>"_LARGURA_ + _ALTURA_", "weight" => "3"}
        ]}

        before(:each) do
          @line_rule_1 = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: 5..20, height: nil,
            total_consumption: "_LARGURA_*2", width_consumption: "_ALTURA_*3", height_consumption: "_LARGURA_ + 3",
            cost: "_CUSTO_BASE_+2", quantity_consumption: "_ALTURA_ * 2", automatic: false, required: true)
          @rule_1 = FactoryGirl.create(:rule, line_rule: @line_rule_1, component: @component_1, control_method: Rule.control_methods[:width], width: 5..60, height: nil,
            total_consumption: "_LARGURA_*2", width_consumption: "_ALTURA_*3", height_consumption: "_LARGURA_ + 3",
            cost: "_CUSTO_BASE_+2", quantity_consumption: "_ALTURA_ * 2", automatic: false, required: true)

          @line_rule_2 = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: 5..20,
            total_consumption: "_LARGURA_*2", width_consumption: "_ALTURA_*3", height_consumption: "_LARGURA_ + 3",
            cost: "_CUSTO_BASE_+2", quantity_consumption: "_ALTURA_ * 2", automatic: true, required: true)
          FactoryGirl.create(:rule, line_rule: @line_rule_2, component: @component_5, control_method: Rule.control_methods[:width], width: 5..60, 
            total_consumption: "_LARGURA_*2", width_consumption: "_ALTURA_*3", height_consumption: "_LARGURA_ + 3",
            cost: "_CUSTO_BASE_+2", quantity_consumption: "_ALTURA_ * 2", automatic: true, required: true)
        end

        it "creates two new Line Rules" do
          expect {
            service.set_line_rules_for(@line.id)
          }.to change(LineRule, :count).from(2).to(3)
        end

        it "creates three new Rules" do
          expect {
            service.set_line_rules_for(@line.id)
          }.to change(Rule, :count).from(2).to(4)
        end

        it "populates the data" do
          service.set_line_rules_for(@line.id)

          line   = service.record
          
          line_rules = line.line_rules.order(:id => :asc)

          expect(line.line_rules).to_not be_empty
          expect(line.line_rules.count).to eq(3)

          expect(line_rules[0].id).to eq(@line_rule_1.id)
          expect(line_rules[0].width).to eq(5...21)
          expect(line_rules[0].height).to be_nil
          expect(line_rules[0].width_consumption).to eq("1")
          expect(line_rules[0].height_consumption).to eq("2")
          expect(line_rules[0].total_consumption).to eq("2 * _LARGURA_")
          expect(line_rules[0].cost).to eq("3 * _ALTURA_")
          expect(line_rules[0].required).to eq(true)
          expect(line_rules[0].automatic).to eq(false)
          expect(line_rules[0].quantity_consumption).to be_empty
          expect(line_rules[0].description).to eq("Bar")
          expect(line_rules[0].rules.count).to eq(1)
          expect(line_rules[0].rules[0].id).to eq(@rule_1.id)
          expect(LineRule.control_methods[line_rules[0].control_method]).to eq(LineRule.control_methods[:width])
          expect(line_rules[0].medium?).to be_truthy
          
          expect(line_rules[1].width).to be_nil
          expect(line_rules[1].height).to eq(5...11)
          expect(line_rules[1].width_consumption).to eq("3")
          expect(line_rules[1].height_consumption).to eq("4")
          expect(line_rules[1].total_consumption).to eq("1")
          expect(line_rules[1].cost).to eq("3")
          expect(line_rules[1].required).to eq(false)
          expect(line_rules[1].automatic).to eq(false)
          expect(line_rules[1].quantity_consumption).to eq("_ALTURA_ * 3")
          expect(line_rules[1].rules.count).to eq(1)
          expect(LineRule.control_methods[line_rules[1].control_method]).to eq(LineRule.control_methods[:height])
          expect(line_rules[1].heavy?).to be_truthy

          expect(line_rules[2].width).to eq(2...11)
          expect(line_rules[2].height).to eq(5...21)
          expect(line_rules[2].width_consumption).to eq("_ALTURA_")
          expect(line_rules[2].height_consumption).to eq("_LARGURA_")
          expect(line_rules[2].total_consumption).to eq("_ALTURA_")
          expect(line_rules[2].cost).to eq("2*_ALTURA_")
          expect(line_rules[2].required).to eq(false)
          expect(line_rules[2].automatic).to eq(false)
          expect(line_rules[2].quantity_consumption).to eq ("_LARGURA_ + _ALTURA_")
          expect(line_rules[2].rules.count).to eq(2)
          expect(LineRule.control_methods[line_rules[2].control_method]).to eq(LineRule.control_methods[:area])
          expect(line_rules[2].heavy?).to be_truthy
        end
      end

      it "returns true" do
        service.set_line_rules_for(@line.id)
        
        expect(service.success).to eq(true)
      end
    end
  end

  describe 'duplicate' do
    let(:parameters) { { 'line_rule_id' => @line_rule.id.to_s } }
    let(:service)    { described_class.new(parameters) }

    before do
      @family = FactoryGirl.create(:family)
      
      @category = FactoryGirl.create(:category, name: 'C1')
      
      @line = FactoryGirl.create(:line, family: @family, categories: [
        FactoryGirl.build(:line_category, category: @category)
      ])

      @component_1 = FactoryGirl.create(:component, category: @category, name: 'C1')
      @component_2 = FactoryGirl.create(:component, category: @category, name: 'C2')

      @line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], 
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_',
        width_consumption: '_LARGURA_', height_consumption: '_ALTURA_')
      @rule_1 = FactoryGirl.create(:rule, line_rule: @line_rule, component: @component_1, control_method: Rule.control_methods[:width], 
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_',
        width_consumption: '_LARGURA_', height_consumption: '_ALTURA_')
      @rule_2 = FactoryGirl.create(:rule, line_rule: @line_rule, component: @component_2, control_method: Rule.control_methods[:width], 
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_',
        width_consumption: '_LARGURA_', height_consumption: '_ALTURA_')
    end

    it 'creates a new line rule' do
      expect {
        service.duplicate(@line.id)
      }.to change(LineRule, :count).by(1)
    end

    it 'creates another line rule with the same data' do
      service.duplicate(@line.id)

      line_rule = service.record

      (LineRule.column_names - ['id', 'width', 'height', 'created_at', 'updated_at', 'description']).each do |field|
        expect(line_rule.send(field.to_sym)).to eq(@line_rule.send(field.to_sym))
      end

      expect(line_rule.description).to eq("#{@line_rule.description} (clone)")
      expect(line_rule.width).to be === @line_rule.width
      expect(line_rule.height).to be === @line_rule.height
    end

    it 'creates two new rules' do
      expect {
        service.duplicate(@line.id)
      }.to change(Rule, :count).by(2)
    end

    it 'creates the rules with the same data as the original rule' do
      service.duplicate(@line.id)

      line_rule = service.record

      rule_1 = line_rule.rules.find_by_component_id(@component_1.id)
      rule_2 = line_rule.rules.find_by_component_id(@component_2.id)

      (Rule.column_names - ['id', 'width', 'height', 'created_at', 'updated_at', 'line_rule_id', 'description']).each do |field|
        expect(rule_1.send(field.to_sym)).to eq(@rule_1.send(field.to_sym))
        expect(rule_2.send(field.to_sym)).to eq(@rule_2.send(field.to_sym))
      end

      expect(rule_1.description).to eq("#{@rule_1.description} (clone)")
      expect(rule_1.width).to be === @rule_1.width
      expect(rule_1.height).to be === @rule_1.height

      expect(rule_2.description).to eq("#{@rule_2.description} (clone)")
      expect(rule_2.width).to be === @rule_2.width
      expect(rule_2.height).to be === @rule_2.height
    end
  end

  describe 'import' do
    let(:parameters) { { 'line_rule_ids' => [@line_rule.id.to_s, @line_rule_2.id.to_s] } }
    let(:service)    { described_class.new(parameters) }

    before do
      @family = FactoryGirl.create(:family)
      
      @category = FactoryGirl.create(:category, name: 'C1')
      
      @line = FactoryGirl.create(:line, family: @family, categories: [
        FactoryGirl.build(:line_category, category: @category)
      ])

      @line_2 = FactoryGirl.create(:line, family: @family, categories: [
        FactoryGirl.build(:line_category, category: @category)
      ])

      @component_1 = FactoryGirl.create(:component, category: @category, name: 'C1')
      @component_2 = FactoryGirl.create(:component, category: @category, name: 'C2')

      @component_3 = FactoryGirl.create(:component, category: @category, name: 'C3')
      @component_4 = FactoryGirl.create(:component, category: @category, name: 'C4')

      @line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], 
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*2',
        width_consumption: '_LARGURA_', height_consumption: '_ALTURA_')
      @rule_1 = FactoryGirl.create(:rule, line_rule: @line_rule, component: @component_1, control_method: Rule.control_methods[:width], 
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*2',
        width_consumption: '_LARGURA_', height_consumption: '_ALTURA_')
      @rule_2 = FactoryGirl.create(:rule, line_rule: @line_rule, component: @component_2, control_method: Rule.control_methods[:width], 
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*2',
        width_consumption: '_LARGURA_', height_consumption: '_ALTURA_')

      @line_rule_2 = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:height], 
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*4',
        width_consumption: '_LARGURA_', height_consumption: '_ALTURA_')
      @rule_3 = FactoryGirl.create(:rule, line_rule: @line_rule_2, component: @component_3, control_method: Rule.control_methods[:height], 
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*4',
        width_consumption: '_LARGURA_', height_consumption: '_ALTURA_')
      @rule_4 = FactoryGirl.create(:rule, line_rule: @line_rule_2, component: @component_4, control_method: Rule.control_methods[:height], 
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*4',
        width_consumption: '_LARGURA_', height_consumption: '_ALTURA_')
    end

    it 'creates two new line rules' do
      expect {
        service.import(@line_2.id)
      }.to change(LineRule, :count).by(2)
    end

    it 'creates two line rules with the same data' do
      service.import(@line_2.id)

      line_rule   = @line_2.line_rules.find_by_cost('_CUSTO_BASE_*2')
      line_rule_2 = @line_2.line_rules.find_by_cost('_CUSTO_BASE_*4')

      (LineRule.column_names - ['id', 'width', 'height', 'created_at', 'updated_at', 'line_id']).each do |field|
        expect(line_rule.send(field.to_sym)).to eq(@line_rule.send(field.to_sym))
      end

      expect(line_rule.line_id).to eq(@line_2.id)
      expect(line_rule.width).to be === @line_rule.width
      expect(line_rule.height).to be === @line_rule.height

      (LineRule.column_names - ['id', 'width', 'height', 'created_at', 'updated_at', 'line_id']).each do |field|
        expect(line_rule_2.send(field.to_sym)).to eq(@line_rule_2.send(field.to_sym))
      end

      expect(line_rule_2.line_id).to eq(@line_2.id)
      expect(line_rule_2.width).to be === @line_rule_2.width
      expect(line_rule_2.height).to be === @line_rule_2.height
    end

    it 'creates four new rules' do
      expect {
        service.import(@line_2.id)
      }.to change(Rule, :count).by(4)
    end

    it 'creates the rules with the same data as the original rule' do
      service.import(@line_2.id)

      line_rule = @line_2.line_rules.find_by_cost('_CUSTO_BASE_*2')

      rule_1 = line_rule.rules.find_by_component_id(@component_1.id)
      rule_2 = line_rule.rules.find_by_component_id(@component_2.id)

      (Rule.column_names - ['id', 'width', 'height', 'created_at', 'updated_at', 'line_rule_id']).each do |field|
        expect(rule_1.send(field.to_sym)).to eq(@rule_1.send(field.to_sym))
        expect(rule_2.send(field.to_sym)).to eq(@rule_2.send(field.to_sym))
      end

      expect(rule_1.width).to be === @rule_1.width
      expect(rule_1.height).to be === @rule_1.height

      expect(rule_2.width).to be === @rule_2.width
      expect(rule_2.height).to be === @rule_2.height

      line_rule = @line_2.line_rules.find_by_cost('_CUSTO_BASE_*4')

      rule_1 = line_rule.rules.find_by_component_id(@component_3.id)
      rule_2 = line_rule.rules.find_by_component_id(@component_4.id)

      (Rule.column_names - ['id', 'width', 'height', 'created_at', 'updated_at', 'line_rule_id']).each do |field|
        expect(rule_1.send(field.to_sym)).to eq(@rule_3.send(field.to_sym))
        expect(rule_2.send(field.to_sym)).to eq(@rule_4.send(field.to_sym))
      end

      expect(rule_1.width).to be === @rule_3.width
      expect(rule_1.height).to be === @rule_3.height

      expect(rule_2.width).to be === @rule_4.width
      expect(rule_2.height).to be === @rule_4.height
    end
  end
end