require 'rails_helper'

RSpec.describe ComponentSelectionService, type: :service do
  let(:width)  {650}
  let(:height) {1100}

  before(:each) do
    @family = FactoryGirl.create(:family)

    @category = FactoryGirl.create(:category, name: 'C1')
    @category_2 = FactoryGirl.create(:category, name: 'C2')
    @category_3 = FactoryGirl.create(:category, name: 'C3')

    @line = FactoryGirl.create(:line, family: @family)

    @line.categories << FactoryGirl.build(:line_category, category: @category, required: true)
    @line.categories << FactoryGirl.build(:line_category, category: @category_3, required: false)

    @line.save!

    @lines_information = [SimulationOption.new(@line.id, width, height), SimulationOption.new(@line.id, width, height)]
  end

  describe 'validation' do
    before(:each) do
      @component_1 = FactoryGirl.create(:component, unit: :cm, category: @category, erasable: true, cost: BigDecimal.new('32.28'))
      @component_2 = FactoryGirl.create(:component, unit: :cm, category: @category_3, erasable: true)
      @component_3 = FactoryGirl.create(:component, unit: :cm, category: @category_3, erasable: false)
      @component_4 = FactoryGirl.create(:component, unit: :cm, category: @category_2, erasable: false)
    end

    it 'fails if the line information is not present' do
      expect(described_class.new([@component_1.id], nil, false)).to be_invalid
    end

    it 'fails if the components are not present' do
      expect(described_class.new(nil, @lines_information, false)).to be_invalid
    end

    it 'fails if the components are blank' do
      expect(described_class.new('', @lines_information, false)).to be_invalid
    end

    context 'for automatic components' do
      before(:each) do
        @component_1 = FactoryGirl.create(:component, unit: :cm, category: @category, erasable: true, cost: BigDecimal.new('32.28'), name: 'C1')
        @component_2 = FactoryGirl.create(:component, unit: :cm, category: @category, erasable: true, cost: BigDecimal.new('32.28'), name: 'C2')
        @component_3 = FactoryGirl.create(:component, unit: :cm, category: @category_3, erasable: true, name: 'C3')

        @specification_1 = FactoryGirl.build(:specification, automatic: false)
        @specification_1.values << FactoryGirl.build(:value, value: 'V1')
        @specification_1.values << FactoryGirl.build(:value, value: 'V2')
        @specification_1.values << FactoryGirl.build(:value, value: 'V3')
        @specification_1.save!

        @specification_2 = FactoryGirl.build(:specification, automatic: true)
        @specification_2.values << FactoryGirl.build(:value, value: 'V4', comparison_value: 1000)
        @specification_2.save!

        @component_1.values = [@specification_1.values.first, @specification_1.values.last, @specification_2.values.first]
        @component_1.save!

        @component_2.values = [@specification_1.values.first, @specification_1.values.last, @specification_2.values.first]
        @component_2.save!

        @component_3.values = [@specification_1.values.first, @specification_1.values.last, @specification_2.values.first]
        @component_3.save!

        @category.update!(use_cut_table: true)
        @category_3.update!(use_cut_table: true)
      end

      it 'fails if the components are not from the same category' do
        expect(described_class.new([@component_1.id, @component_3.id], @lines_information, false)).to be_invalid
      end

      it 'fails if the component does not have a cut table selected' do
        expect(described_class.new([@component_1.id, @component_2.id], @lines_information, false)).to be_invalid
      end

      it 'fails if the component has rules with different control methods' do
        line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], 
          total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*3', automatic: true)
        FactoryGirl.create(:rule, line_rule: line_rule, component: @component_1, control_method: Rule.control_methods[:width], 
          total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*3', automatic: true)

        line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:height],
         total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*3', automatic: true)
        FactoryGirl.create(:rule, line_rule: line_rule, component: @component_2, control_method: Rule.control_methods[:height], 
          total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*3', automatic: true)

        expect(described_class.new([@component_1.id, @component_2.id], @lines_information, false)).to be_invalid
      end
    end
  end

  describe '.perform' do
    context 'for non automatic components' do
      before(:each) do
        @component_1 = FactoryGirl.create(:component, unit: :cm, category: @category, erasable: true, cost: BigDecimal.new('32.28'))
        
        @line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: 50..300, 
          total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*3',
          width_consumption: '_ALTURA_*3', height_consumption: '_LARGURA_ + 3')
        @rule = FactoryGirl.create(:rule, line_rule: @line_rule, component: @component_1, control_method: Rule.control_methods[:width], 
          width: 50..300, total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*3',
          width_consumption: '_ALTURA_*3', height_consumption: '_LARGURA_ + 3')
      end

      it 'returns true' do
        expect(described_class.new([@component_1.id], @lines_information, false).perform).to eq(true)
      end

      it 'selects the same component' do
        service = described_class.new([@component_1.id], @lines_information, false)
        service.perform

        expect(service.component).to eq(@component_1)
      end
    end

    if (![true, 'true'].include?(ENV['DEPLOY']))
      describe 'for automatic components' do
        before(:each) do
          @category.update!(use_cut_table: true)
          @category_3.update!(use_cut_table: true)

          @cut_table = FactoryGirl.create(:cut_table, size: 5000)

          @component_1 = FactoryGirl.create(:component, unit: :cm, cut_table: @cut_table, category: @category, erasable: true,
            cost: BigDecimal.new('60'), name: 'C180')
          @component_2 = FactoryGirl.create(:component, unit: :cm, cut_table: @cut_table, category: @category, erasable: true,
            cost: BigDecimal.new('100'), name: 'C200')
          @component_3 = FactoryGirl.create(:component, unit: :cm, cut_table: @cut_table, category: @category, erasable: true,
            cost: BigDecimal.new('140'), name: 'C250')

          @specification = FactoryGirl.build(:specification, automatic: true)
          @value_1 = FactoryGirl.build(:value, value: 'V1', comparison_value: 1800)
          @value_2 = FactoryGirl.build(:value, value: 'V2', comparison_value: 2000)
          @value_3 = FactoryGirl.build(:value, value: 'V3', comparison_value: 2500)
          @specification.values = [@value_1, @value_2, @value_3]
          @specification.save!

          @component_1.values = [@value_1]
          @component_1.save!

          @component_2.values = [@value_2]
          @component_2.save!

          @component_3.values = [@value_3]
          @component_3.save!

          line_rule = FactoryGirl.create(:line_rule, automatic: true, line: @line, control_method: LineRule.control_methods[:width], 
            total_consumption: '_LARGURA_', cost: '_CUSTO_BASE_',
            width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', quantity_consumption: '1')
          FactoryGirl.create(:rule, automatic: true, line_rule: line_rule, component: @component_1, control_method: Rule.control_methods[:width], 
            total_consumption: '_LARGURA_', cost: '_CUSTO_BASE_',
            width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', quantity_consumption: '1')
          FactoryGirl.create(:rule, automatic: true, line_rule: line_rule, component: @component_2, control_method: Rule.control_methods[:width], 
            total_consumption: '_LARGURA_', cost: '_CUSTO_BASE_',
            width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', quantity_consumption: '1')
          FactoryGirl.create(:rule, automatic: true, line_rule: line_rule, component: @component_3, control_method: Rule.control_methods[:width], 
            total_consumption: '_LARGURA_', cost: '_CUSTO_BASE_',
            width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', quantity_consumption: '1')

          @lines_information = [[600, 900], [650, 1100], [700, 1250], [650, 1150], [550, 1100]].map do |option|
            SimulationOption.new(@line.id, option[0], option[1])
          end
        end

        it 'returns true' do
          expect(described_class.new([@component_1.id, @component_2.id, @component_3.id], @lines_information, false).perform).to eq(true)
        end

        context 'for width controlled components' do
          context 'without optimization' do
            it 'selects the component with the lowest total cost' do
              service = described_class.new([@component_1.id, @component_2.id, @component_3.id], @lines_information, false)
              service.perform

              expect(service.component).to eq(@component_1)
            end

            it 'returns the selected packing' do
              service = described_class.new([@component_1.id, @component_2.id, @component_3.id], @lines_information, false)
              service.perform

              expect(service.packing).to_not be_nil
            end

            it 'returns the calculated total cost' do
              service = described_class.new([@component_1.id, @component_2.id, @component_3.id], @lines_information, false)
              service.perform

              expect(service.total_cost).to eq(BigDecimal.new('18900'))
            end
          end

          context 'with optimization' do
            it 'selects the component with the lowest total cost for the given option' do
              service = described_class.new([@component_1.id, @component_2.id, @component_3.id], @lines_information, true)
              service.perform(@lines_information[1])

              expect(service.component).to eq(@component_2)
            end

            it 'returns the selected packing fo the given option' do
              service = described_class.new([@component_1.id, @component_2.id, @component_3.id], @lines_information, true)
              service.perform(@lines_information[1])

              expect(service.packing).to_not be_nil
            end

            it 'returns the calculated total cost for the given option' do
              service = described_class.new([@component_1.id, @component_2.id, @component_3.id], @lines_information, true)
              service.perform(@lines_information[1])

              expect(service.total_cost).to eq(BigDecimal.new('11000'))
            end

            it 'selects the component with the lowest total cost for the given option' do
              service = described_class.new([@component_1.id, @component_2.id, @component_3.id], @lines_information, true)
              service.perform(@lines_information[2])

              expect(service.component).to eq(@component_1)
            end

            it 'returns the selected packing fo the given option' do
              service = described_class.new([@component_1.id, @component_2.id, @component_3.id], @lines_information, true)
              service.perform(@lines_information[2])

              expect(service.packing).to_not be_nil
            end

            it 'returns the calculated total cost for the given option' do
              service = described_class.new([@component_1.id, @component_2.id, @component_3.id], @lines_information, true)
              service.perform(@lines_information[2])

              expect(service.total_cost).to eq(BigDecimal.new('7500'))
            end
          end
        end

        context 'for height controlled components' do
          context 'without optimization' do
            before(:each) do
              LineRule.destroy_all

              line_rule = FactoryGirl.create(:line_rule, automatic: true, line: @line, control_method: LineRule.control_methods[:height], 
                total_consumption: '_LARGURA_', cost: '_CUSTO_BASE_',
                width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', quantity_consumption: '1')
              FactoryGirl.create(:rule, automatic: true, line_rule: line_rule, component: @component_1, control_method: Rule.control_methods[:height], 
                total_consumption: '_LARGURA_', cost: '_CUSTO_BASE_',
                width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', quantity_consumption: '1')
              FactoryGirl.create(:rule, automatic: true, line_rule: line_rule, component: @component_2, control_method: Rule.control_methods[:height], 
                total_consumption: '_LARGURA_', cost: '_CUSTO_BASE_',
                width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', quantity_consumption: '1')
              FactoryGirl.create(:rule, automatic: true, line_rule: line_rule, component: @component_3, control_method: Rule.control_methods[:height], 
                total_consumption: '_LARGURA_', cost: '_CUSTO_BASE_',
                width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', quantity_consumption: '1')

              @lines_information = [[600, 900], [650, 1100], [700, 1250], [650, 1150], [550, 1100]].map do |option|
                SimulationOption.new(@line.id, option[1], option[0])
              end
            end

            it 'selects the component with the lowest total cost' do
              service = described_class.new([@component_1.id, @component_2.id, @component_3.id], @lines_information, false)
              service.perform

              expect(service.component).to eq(@component_1)
            end

            it 'returns the selected packing' do
              service = described_class.new([@component_1.id, @component_2.id, @component_3.id], @lines_information, false)
              service.perform

              expect(service.packing).to_not be_nil
            end

            it 'returns the calculated total cost' do
              service = described_class.new([@component_1.id, @component_2.id, @component_3.id], @lines_information, false)
              service.perform

              expect(service.total_cost).to eq(BigDecimal.new('18900'))
            end
          end

          context 'with optimization' do
            it 'selects the component with the lowest total cost for the given option' do
              service = described_class.new([@component_1.id, @component_2.id, @component_3.id], @lines_information, true)
              service.perform(@lines_information[1])

              expect(service.component).to eq(@component_2)
            end

            it 'returns the selected packing fo the given option' do
              service = described_class.new([@component_1.id, @component_2.id, @component_3.id], @lines_information, true)
              service.perform(@lines_information[1])

              expect(service.packing).to_not be_nil
            end

            it 'returns the calculated total cost for the given option' do
              service = described_class.new([@component_1.id, @component_2.id, @component_3.id], @lines_information, true)
              service.perform(@lines_information[1])

              expect(service.total_cost).to eq(BigDecimal.new('11000'))
            end

            it 'selects the component with the lowest total cost for the given option' do
              service = described_class.new([@component_1.id, @component_2.id, @component_3.id], @lines_information, true)
              service.perform(@lines_information[2])

              expect(service.component).to eq(@component_1)
            end

            it 'returns the selected packing fo the given option' do
              service = described_class.new([@component_1.id, @component_2.id, @component_3.id], @lines_information, true)
              service.perform(@lines_information[2])

              expect(service.packing).to_not be_nil
            end

            it 'returns the calculated total cost for the given option' do
              service = described_class.new([@component_1.id, @component_2.id, @component_3.id], @lines_information, true)
              service.perform(@lines_information[2])

              expect(service.total_cost).to eq(BigDecimal.new('7500'))
            end
          end
        end
      end
    else
      Kernel.puts 'SKIPPING AUTOMATIC COMPONENT SELECTION TESTS'
    end
  end
end