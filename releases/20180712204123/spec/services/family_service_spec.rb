require 'rails_helper'

RSpec.describe FamilyService, type: :service do
  describe "create" do
    let(:service) {described_class.new(parameters)}

    context "with invalid parameters" do
      let(:parameters) {{"name" => ""}}

      it "does not create a new Family" do
        expect {
          service.create
        }.to change(Family, :count).by(0)
      end

      it "should return false" do
        service.create
        
        expect(service.success).to eq(false)
      end

      it "should return the errors" do
        service.create
        
        expect(service.errors).to_not be_empty
      end
    end

    context "with valid parameters" do
      let(:parameters) {{"name" => "Rolô", "color" => "#122211"}}

      it "should create a new Family" do
        expect {
          service.create
        }.to change(Family, :count).by(1)
      end

      it "should return the created family" do
        service.create

        expect(service.record).to eq(Family.last)
      end

      it "should return true" do
        service.create
        
        expect(service.success).to eq(true)
      end

      it "populates the data" do
        service.create

        family = service.record

        expect(family.name).to eq("Rolô")
        expect(family.color).to eq("#122211")
      end
    end
  end

  describe "update" do
    let(:service) {described_class.new(parameters)}

    before(:each) do
      @family = FactoryGirl.create(:family)
    end

    context "with invalid parameters" do
      let(:parameters) {{"name" => ""}}

      it "does not create a new Family" do
        expect {
          service.update(@family.id)
        }.to change(Family, :count).by(0)
      end

      it "should return false" do
        service.update(@family.id)
        
        expect(service.success).to eq(false)
      end

      it "should return the errors" do
        service.update(@family.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context "with valid parameters" do
      let(:parameters) {{"name" => "Rolô", "color" => "#558899"}}

      it "does not create a new Family" do
        expect {
          service.update(@family.id)
        }.to change(Family, :count).by(0)
      end

      it "should return the family" do
        service.update(@family.id)

        expect(service.record).to eq(Family.last)
      end

      it "should return true" do
        service.update(@family.id)
        
        expect(service.success).to eq(true)
      end

      it "populates the data" do
        service.update(@family.id)

        family = service.record

        expect(family.name).to eq("Rolô")
        expect(family.color).to eq("#558899")
      end
    end
  end
end
