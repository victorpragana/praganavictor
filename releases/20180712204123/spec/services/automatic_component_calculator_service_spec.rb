require 'rails_helper'

RSpec.describe AutomaticComponentCalculatorService, type: :service do
  let(:width)      { 1000 }
  let(:width_2)    { 1100 }
  let(:height)     { 3000 }
  let(:height_2)   { 3300 }
  let(:total_cost) { 20336.4 }

  before(:each) do
    @family = create(:family)

    @category = create(:category, name: 'C1')
    @category_2 = create(:category, name: 'C2')
    @category_3 = create(:category, name: 'C3')

    @line = create(:line, family: @family, sale_price: 15.9)

    @line.categories << build(:line_category, category: @category, required: true)
    @line.categories << build(:line_category, category: @category_3, required: false)

    @line.save!

    @specification_1 = build(:specification, automatic: false, name: 'S1')
    @specification_1.values << build(:value, value: 'V1')
    @specification_1.values << build(:value, value: 'V2')
    @specification_1.values << build(:value, value: 'V3')
    @specification_1.save!

    @specification_2 = build(:specification, automatic: true, name: 'S2')
    @specification_2.values << build(:value, value: 'V4', comparison_value: 1500)
    @specification_2.save!

    @component_1 = create(:component, name: 'CO1', unit: :cm, category: @category, erasable: true, cost: BigDecimal.new('32.28'), values: [@specification_1.values.first, @specification_1.values.last, @specification_2.values.first])
    @component_2 = create(:component, name: 'CO2', unit: :cm, category: @category_3, erasable: true, values: [@specification_1.values.first, @specification_1.values.last, @specification_2.values.first])
    @component_3 = create(:component, name: 'CO3', unit: :cm, category: @category_3, erasable: true, values: [@specification_1.values.first, @specification_1.values.last, @specification_2.values.first])
    @component_4 = create(:component, name: 'CO4', unit: :cm, category: @category_2, erasable: true, values: [@specification_1.values.first, @specification_1.values.last, @specification_2.values.first])

    @line_rule = create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], 
      total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*3',
      width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', quantity_consumption: '1', automatic: true)
    @rule = create(:rule, :line_rule => @line_rule, :component => @component_1, control_method: Rule.control_methods[:width], 
      total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*3',
      width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', quantity_consumption: '1', automatic: true)

    @packing = BoxPacker.container([@specification_2.values.first.comparison_value * @component_1.conversion_factor, 500],
      orientation: :height, remove_exceeding: true)
    @packing.add_item [width * @component_1.conversion_factor, height * @component_1.conversion_factor], allow_rotation: false, id: 1
    @packing.add_item [width_2 * @component_1.conversion_factor, height_2 * @component_1.conversion_factor], allow_rotation: false, id: 2
    @packing.pack!
  end

  describe 'validation' do
    it_should_behave_like 'a base calculator', @packing

    it 'fails if the component category does not belong to line' do
      expect(described_class.new(@line.id, @component_4.id, width, height, 1, @packing)).to be_invalid
    end

    it 'fails if the component is not erasable' do
      @component_1.erasable = false
      @component_1.save!

      expect(described_class.new(@line.id, @component_1.id, width, height, 1, @packing)).to be_invalid
    end

    it 'fails if the component is not automatic' do
      @component_1.values = []
      @component_1.save!

      expect(described_class.new(@line.id, @component_1.id, width, height, 1, @packing)).to be_invalid
    end
  end

  describe 'width_consumption calculation' do
    let(:service) { described_class.new(@line.id, @component_1.id, width, height, 1, @packing) }

    it 'returns the component width_consumption for the given line with the given width and height' do
      expect(service.width_consumption).to eq(BigDecimal.new('100'))
    end
  end

  describe 'height_consumption calculation' do
    let(:service) { described_class.new(@line.id, @component_1.id, width, height, 1, @packing) }

    it 'returns the component height_consumption for the given line with the given width and height' do
      expect(service.height_consumption).to eq(BigDecimal.new('300'))
    end
  end

  describe 'quantity_consumption calculation' do
    let(:service) { described_class.new(@line.id, @component_1.id, width, height, 1, @packing) }

    context 'when configured' do
      it 'returns the component quantity_consumption for the given line with the given width and height' do
        expect(service.quantity_consumption).to eq(BigDecimal.new('1'))
      end

      context 'with user questions' do
        let(:service) { described_class.new(@line.id, @component_1.id, width, height, 1, @packing, { @question1.value => @question1.options[0]['value'], @question2.value => true }) }

        before do
          @question1 = create(:question, type: :dropdown, options: [{ 'label' => 'Foo', 'value' => 'foo' }, { 'label' => 'Bar', 'value' => 'bar' }])
          @question2 = create(:question, type: :boolean)

          @line.question_ids = [@question1.id, @question2.id]
          @line.save!

          LineRule.destroy_all
          Rule.destroy_all

          @line_rule = create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], 
            total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*3',
            width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', quantity_consumption: "IF(#{@question1.as_variable}='#{@question1.options[0]['value']}',IF(#{@question2.as_variable},5,4),1)", automatic: true)
          @rule = create(:rule, :line_rule => @line_rule, :component => @component_1, control_method: Rule.control_methods[:width], 
            total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*3',
            width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', quantity_consumption: "IF(#{@question1.as_variable}='#{@question1.options[0]['value']}',IF(#{@question2.as_variable},5,4),1)", automatic: true)
        end

        it 'calculates the quantity consumption using the user questions' do
          expect(service.quantity_consumption).to eq(BigDecimal.new('5'))
        end
      end
    end

    context 'when not configured' do
      before(:each) do
        @rule.quantity_consumption = nil
        @rule.save!
      end

      it 'returns 1' do
        expect(service.quantity_consumption).to eq(BigDecimal.new('1'))
      end
    end
  end

  describe 'total_consumption calculation' do
    let(:service) { described_class.new(@line.id, @component_1.id, width, height, 1, @packing) }

    it 'returns the used height/width depending on the orientation' do
      expect(service.total_consumption).to eq(300)
    end
  end

  describe 'component cost' do
    it 'returns the component cost considering the area, total cost and the leftovers' do
      expect(described_class.new(@line.id, @component_1.id, width, height, 1, @packing).cost(total_cost)).to eq(BigDecimal.new('9201.990950226245'))
    end

    it 'returns the component cost considering the area, total cost and the leftovers' do
      expect(described_class.new(@line.id, @component_1.id, width_2, height_2, 2, @packing).cost(total_cost).to_d).to eq(BigDecimal.new('11134.4090497738'))
    end
  end

  describe 'component price' do
    it 'returns the component price using the calculate cost' do
      expect(described_class.new(@line.id, @component_1.id, width, height, 1, @packing).price(total_cost).to_s).to eq(BigDecimal.new('10665.107511312217955').to_s)
    end

    it 'returns the component price using the calculate cost' do
      expect(described_class.new(@line.id, @component_1.id, width_2, height_2, 2, @packing).price(total_cost)).to eq(BigDecimal.new('12904.78008868778784'))
    end
  end

  describe 'total calculation' do
    it 'returns the component price multiplied by the quantity consumption' do
      expect(described_class.new(@line.id, @component_1.id, width, height, 1, @packing).total(total_cost)).to eq(BigDecimal.new('10665.11'))
    end

    it 'returns the component price multiplied by the quantity consumption' do
      expect(described_class.new(@line.id, @component_1.id, width_2, height_2, 2, @packing).total(total_cost)).to eq(BigDecimal.new('12904.78'))
    end
  end
end