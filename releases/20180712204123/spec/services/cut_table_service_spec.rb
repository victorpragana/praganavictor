require 'rails_helper'

RSpec.describe CutTableService, type: :service do
  describe '#create' do
    let(:service) { described_class.new(parameters) }

    context 'with invalid parameters' do
      let(:parameters) { {'name' => '', 'size' => '-1'} }

      it 'does not create a new Cut table' do
        expect {
          service.create
        }.to change(CutTable, :count).by(0)
      end

      it 'returns false' do
        service.create
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.create
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { {'name' => 'Table', 'size' => '1000'} }

      it 'creates a new Cut table' do
        expect {
          service.create
        }.to change(CutTable, :count).by(1)
      end

      it 'returns the created Cut table' do
        service.create

        expect(service.record).to eq(CutTable.last)
      end

      it 'returns true' do
        service.create
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.create

        cut_table = service.record

        expect(cut_table.name).to eq('Table')
        expect(cut_table.size).to eq(1000)
      end
    end
  end

  describe 'update' do
    let(:service) {described_class.new(parameters)}

    before(:each) do
      @cut_table = FactoryGirl.create(:cut_table)
    end

    context 'with invalid parameters' do
      let(:parameters) { {'name' => '', 'size' => '-1'} }

      it 'returns false' do
        service.update(@cut_table.id)
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.update(@cut_table.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { {'name' => 'Table', 'size' => '1000'} }

      it 'does not create a new Cut table' do
        expect {
          service.update(@cut_table.id)
        }.to change(CutTable, :count).by(0)
      end

      it 'returns the cut table' do
        service.update(@cut_table.id)

        expect(service.record).to eq(CutTable.last)
      end

      it 'returns true' do
        service.update(@cut_table.id)
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.update(@cut_table.id)

        cut_table = service.record

        expect(cut_table.name).to eq('Table')
        expect(cut_table.size).to eq(1000)
      end
    end
  end
end
