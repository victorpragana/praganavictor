require 'rails_helper'

RSpec.describe OptionService, type: :service do
  before(:each) do
    @family = FactoryGirl.create(:family)
    
    @category = FactoryGirl.create(:category, name: 'C1')
    @category_2 = FactoryGirl.create(:category, name: 'C2')
    @category_3 = FactoryGirl.create(:category, name: 'C3')
    @category_4 = FactoryGirl.create(:category, name: 'C4')

    @line = FactoryGirl.create(:line, family: @family, sale_price: BigDecimal.new('12.50'))

    @line.categories << FactoryGirl.build(:line_category, category: @category, required: true, order: 0)
    @line.categories << FactoryGirl.build(:line_category, category: @category_3, required: false, order: 1)
    @line.categories << FactoryGirl.build(:line_category, category: @category_4, required: true, order: 2)

    @line.save!

    @component_1 = FactoryGirl.create(:component, name: 'C1', unit: :cm, category: @category, erasable: true, cost: BigDecimal.new('10'))
    @component_2 = FactoryGirl.create(:component, name: 'C2', unit: :cm, category: @category_3, erasable: true, cost: BigDecimal.new('12'))
    @component_3 = FactoryGirl.create(:component, name: 'C3', unit: :cm, category: @category_4, erasable: true, cost: BigDecimal.new('14'))

    @line_rule_1 = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: 50..300, required: true,
      width_consumption: '_LARGURA_*3', height_consumption: '_LARGURA_', total_consumption: '_INFORMADO_',
      cost: '_CUSTO_BASE_+2', quantity_consumption: '_LARGURA_*3'
    )

    base = FactoryGirl.create(:rule, line_rule: @line_rule_1, component: @component_1, control_method: Rule.control_methods[:width], width: 50..300, required: true,
      width_consumption: '_LARGURA_*3', height_consumption: '_LARGURA_', total_consumption: '_INFORMADO_',
      cost: '_CUSTO_BASE_+2', quantity_consumption: '_LARGURA_*3'
    )

    FactoryGirl.create(:rule, line_rule: @line_rule_1, component: @component_3, control_method: Rule.control_methods[:width], height: 50..300, required: true,
      width_consumption: '_LARGURA_*3', height_consumption: '_LARGURA_', total_consumption: '_INFORMADO_',
      cost: '_CUSTO_BASE_+2', quantity_consumption: '_LARGURA_*3'
    )

    @line_rule_2 = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:height], height: 50..300, required: false,
      total_consumption: '_ALTURA_*2', cost: '_CUSTO_BASE_*2'
    )

    FactoryGirl.create(:rule, line_rule: @line_rule_2, component: @component_2, control_method: Rule.control_methods[:height], height: 50..300, required: false,
      total_consumption: '_ALTURA_*2', cost: '_CUSTO_BASE_*2'
    )

    @line_rule_3 = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: 50..300, required: false,
      total_consumption: '_ALTURA_*2', cost: '_CUSTO_BASE_*2'
    )

    @child = FactoryGirl.create(:rule, line_rule: @line_rule_1, component: @component_3, control_method: Rule.control_methods[:width], width: 50..300, required: false,
      total_consumption: '_ALTURA_*2', cost: '_CUSTO_BASE_*2'
    )

    @distributor = FactoryGirl.create(:distributor, name: 'D1')
    @store       = FactoryGirl.create(:store, distributor: @distributor, name: 'S1')
    @customer    = FactoryGirl.create(:customer, store_id: @store.id, name: 'C1')
    @estimate    = FactoryGirl.create(:estimate, seller: @store, client: @customer)
    @environment = FactoryGirl.create(:environment, estimate: @estimate)
    @location    = FactoryGirl.create(:location, environment: @environment)
  end

  describe 'create' do
    let(:service) {described_class.new(parameters)}

    context 'with invalid parameters' do
      let(:parameters) { {'line_id' => "#{@line.id}", 'width' => '', 'height' => '120', 'location_id' => "#{@location.id}", 
        'components' => [
          {'component_ids' => ["#{@component_1.id}"]},
          {'component_ids' => ["#{@component_2.id}"]}
        ]
      } }

      it 'does not create a new Option' do
        expect {
          service.create
        }.to_not change(Option, :count)
      end

      it 'does not create a new Option Component' do
        expect {
          service.create
        }.to_not change(OptionComponent, :count)
      end

      it 'returns false' do
        service.create
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.create
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { {'line_id' => "#{@line.id}", 'width' => '100', 'height' => '120',
        'location_id' => "#{@location.id}",
        'components' => [
          {'component_ids' => ["#{@component_1.id}"], 'total_consumption' => '100'},
          {'component_ids' => ["#{@component_2.id}"]},
          {'component_ids' => ["#{@component_3.id}"], 'rule_id' => "#{@child.id}"},
        ]
      } }

      context 'without quantity information' do
        it 'creates a new Option' do
          expect {
            service.create
          }.to change(Option, :count).by(1)
        end

        it 'creates three new option components' do
          expect {
            service.create
          }.to change(OptionComponent, :count).by(3)
        end
      end

      context 'with quantity information' do
        let(:parameters) { {'line_id' => "#{@line.id}", 'width' => '100', 'height' => '120', 'quantity' => '4',
          'location_id' => "#{@location.id}",
          'components' => [
            {'component_ids' => ["#{@component_1.id}"], 'total_consumption' => '100'},
            {'component_ids' => ["#{@component_2.id}"]},
            {'component_ids' => ["#{@component_3.id}"], 'rule_id' => "#{@child.id}"},
          ]
        } }

        it 'creates four new Options' do
          expect {
            service.create
          }.to change(Option, :count).by(4)
        end

        it 'creates twelve new option components' do
          expect {
            service.create
          }.to change(OptionComponent, :count).by(12)
        end
      end

      it 'returns the created Option' do
        service.create

        expect(service.record).to eq(Option.last)
      end


      it 'returns true' do
        service.create
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.create

        option = service.record

        expect(option.line_id).to eq(@line.id)
        expect(option.width).to eq(100)
        expect(option.height).to eq(120)
        expect(option.selected).to eq(false)
        expect(option.components).to_not be_empty
        expect(option.components.count).to eq(3)

        components = option.components.order(:component_name)

        expect(components[2].possible_components.count).to eq(1)
        expect(components[2].possible_components[0]).to eq(@component_3)
        expect(components[2].component).to eq(@component_3)
        expect(components[2].component_name).to eq(@component_3.name)
        expect(components[2].component_characteristics).to eq(@component_3.characteristics)
        expect(components[2].category_name).to eq(@component_3.category.name)
        expect(components[2].unit).to eq(@component_3.unit)
        expect(components[2].total_consumption).to eq(BigDecimal.new('24'))
        expect(components[2].unit_value).to eq(BigDecimal.new('31.5'))
        expect(components[2].width_consumption).to be_nil
        expect(components[2].height_consumption).to be_nil
        expect(components[2].quantity_consumption).to be_nil
        expect(components[2].rule).to eq(@child)
        expect(components[2].required).to eq(false)
        expect(components[1].possible_components.count).to eq(1)
        expect(components[1].possible_components[0]).to eq(@component_2)
        expect(components[1].component).to eq(@component_2)
        expect(components[1].component_name).to eq(@component_2.name)
        expect(components[1].component_characteristics).to eq(@component_2.characteristics)
        expect(components[1].category_name).to eq(@component_2.category.name)
        expect(components[1].unit).to eq(@component_2.unit)
        expect(components[1].total_consumption).to eq(BigDecimal.new('24'))
        expect(components[1].unit_value).to eq(BigDecimal.new('27'))
        expect(components[1].width_consumption).to be_nil
        expect(components[1].height_consumption).to be_nil
        expect(components[1].quantity_consumption).to be_nil
        expect(components[1].required).to eq(false)
        expect(components[0].possible_components.count).to eq(1)
        expect(components[0].possible_components[0]).to eq(@component_1)
        expect(components[0].component).to eq(@component_1)
        expect(components[0].component_name).to eq(@component_1.name)
        expect(components[0].component_characteristics).to eq(@component_1.characteristics)
        expect(components[0].category_name).to eq(@component_1.category.name)
        expect(components[0].unit).to eq(@component_1.unit)
        expect(components[0].total_consumption).to eq(BigDecimal.new('100'))
        expect(components[0].width_consumption).to eq(BigDecimal.new('30'))
        expect(components[0].height_consumption).to eq(BigDecimal.new('10'))
        expect(components[0].quantity_consumption).to eq(BigDecimal.new('30'))
        expect(components[0].unit_value).to eq(BigDecimal.new('13.5'))
        expect(components[0].required).to eq(true)
      end
    end
  end

  describe 'update' do
    let(:service) {described_class.new(parameters)}

    before(:each) do
      @option = FactoryGirl.create(:option, line: @line, location: @location)
    end

    context 'with invalid parameters' do
      let(:parameters) { {'line_id' => "#{@line.id}", 'width' => '', 'height' => '120', 'location_id' => "#{@location.id}", 
        'components' => [
          {'component_ids' => ["#{@component_1.id}"], 'total_consumption' => '100'}, {'component_ids' => ["#{@component_2.id}"]}
        ]
      } }

      it 'does not create a new Option' do
        expect {
          service.update(@option.id)
        }.to_not change(Option, :count)
      end

      it 'does not create a new Option Component' do
        expect {
          service.update(@option.id)
        }.to_not change(OptionComponent, :count)
      end

      it 'returns false' do
        service.update(@option.id)
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.update(@option.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) {{'line_id' => "#{@line.id}", 'width' => '100', 'height' => '120', 'location_id' => "#{@location.id}",
        'components' => [
          {'component_ids' => ["#{@component_1.id}"], 'total_consumption' => '100'},
          {'component_ids' => ["#{@component_2.id}"]},
          {'component_ids' => ["#{@component_3.id}"], 'rule_id' => "#{@child.id}"},
        ]
      }}

      it 'does not create a new Option' do
        expect {
          service.update(@option.id)
        }.to_not change(Option, :count)
      end

      it 'destroys and create three new option components' do
        expect {
          service.update(@option.id)
        }.to change(OptionComponent, :count).by(3)
      end

      it 'returns the created Option' do
        service.update(@option.id)

        expect(service.record).to eq(Option.last)
      end

      it 'returns true' do
        service.update(@option.id)
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.update(@option.id)

        option = service.record

        expect(option.line_id).to eq(@line.id)
        expect(option.width).to eq(100)
        expect(option.height).to eq(120)
        expect(option.selected).to eq(false)
        expect(option.components).to_not be_empty
        expect(option.components.count).to eq(3)

        components = option.components.order(:component_name)

        expect(components[2].possible_components.count).to eq(1)
        expect(components[2].possible_components[0]).to eq(@component_3)
        expect(components[2].component).to eq(@component_3)
        expect(components[2].component_name).to eq(@component_3.name)
        expect(components[2].component_characteristics).to eq(@component_3.characteristics)
        expect(components[2].category_name).to eq(@component_3.category.name)
        expect(components[2].unit).to eq(@component_3.unit)
        expect(components[2].total_consumption).to eq(BigDecimal.new('24'))
        expect(components[2].unit_value).to eq(BigDecimal.new('31.5'))
        expect(components[2].width_consumption).to be_nil
        expect(components[2].height_consumption).to be_nil
        expect(components[2].quantity_consumption).to be_nil
        expect(components[2].rule).to eq(@child)
        expect(components[2].required).to eq(false)
        expect(components[1].possible_components.count).to eq(1)
        expect(components[1].possible_components[0]).to eq(@component_2)
        expect(components[1].component).to eq(@component_2)
        expect(components[1].component_name).to eq(@component_2.name)
        expect(components[1].component_characteristics).to eq(@component_2.characteristics)
        expect(components[1].category_name).to eq(@component_2.category.name)
        expect(components[1].unit).to eq(@component_2.unit)
        expect(components[1].total_consumption).to eq(BigDecimal.new('24'))
        expect(components[1].unit_value).to eq(BigDecimal.new('27'))
        expect(components[1].width_consumption).to be_nil
        expect(components[1].height_consumption).to be_nil
        expect(components[1].quantity_consumption).to be_nil
        expect(components[1].required).to eq(false)
        expect(components[0].possible_components.count).to eq(1)
        expect(components[0].possible_components[0]).to eq(@component_1)
        expect(components[0].component).to eq(@component_1)
        expect(components[0].component_name).to eq(@component_1.name)
        expect(components[0].component_characteristics).to eq(@component_1.characteristics)
        expect(components[0].category_name).to eq(@component_1.category.name)
        expect(components[0].unit).to eq(@component_1.unit)
        expect(components[0].total_consumption).to eq(BigDecimal.new('100'))
        expect(components[0].width_consumption).to eq(BigDecimal.new('30'))
        expect(components[0].height_consumption).to eq(BigDecimal.new('10'))
        expect(components[0].quantity_consumption).to eq(BigDecimal.new('30'))
        expect(components[0].unit_value).to eq(BigDecimal.new('13.5'))
        expect(components[0].required).to eq(true)
      end
    end
  end
end