require 'rails_helper'

RSpec.describe DocumentService, type: :service do
  before(:each) do
    @document       = FactoryGirl.create(:document, name: 'Documento', text: 'Foo Bar')
  end

  describe 'update' do
    let(:service) {described_class.new(parameters)}

    context 'with valid parameters' do
      let(:parameters) { {'text' => 'Foo Bar'
      } }

      it 'does not creates a new document' do
        expect {
          service.update
        }.to change(Document, :count).by(0)
      end

      it 'returns the document' do
        service.update

        expect(service.record).to eq(Document.first)
      end

      it 'returns true' do
        service.update
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.update

        document = service.record

        expect(document.text).to eq(@document.text)
      end
    end
  end
end