require 'rails_helper'

RSpec.describe CustomerService, type: :service do
  before do
    @state         = create(:state)
    @city          = create(:city, state: @state)
    @distributor_1 = create(:distributor, name: 'AA')
    @store_1       = create(:store, name: 'BB', distributor: @distributor_1)
    @store_2       = create(:store, name: 'CC', distributor: @distributor_1)
    @distributor_2 = create(:distributor)
    @store_3       = create(:store, name: 'DD', distributor: @distributor_2)
  end

  describe 'create' do
    let(:service) {described_class.new(parameters)}

    context 'with invalid parameters' do
      let(:parameters) {{'name' => ''}}

      it 'does not create a new Customer' do
        expect {
          service.create
        }.to change(Customer, :count).by(0)
      end

      it 'returns false' do
        service.create
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.create
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { { 'name' => 'Customer 1', 'identification' => '111.623.597-81', 'zip_code' => '12244-888', 
        'street' => 'St. Foo', 'neighborhood' => 'Nb bar', 'number' => 'SN', 'complement' => 'ABC', 'phone' => '(12)99222-8290', 
        'email' => 'foo@bar.com', 'city_id' => "#{@city.id}", 'corporate_name' => '', 'store_id' => "#{@store_1.id}",
        'allow_special_discount' => true, 'special_discount' => '12.12'
      } }

      it 'creates a new customer' do
        expect {
          service.create
        }.to change(Customer, :count).by(1)
      end

      it 'creates a new third party' do
        expect {
          service.create
        }.to change(ThirdParty, :count).by(1)
      end

      it 'returns the created customer' do
        service.create

        expect(service.record).to eq(Customer.last)
      end

      it 'returns true' do
        service.create
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.create

        customer = service.record

        expect(customer.name).to eq('Customer 1')
        expect(customer.identification).to eq('11162359781')
        expect(customer.zip_code).to eq('12244888')
        expect(customer.street).to eq('St. Foo')
        expect(customer.neighborhood).to eq('Nb bar')
        expect(customer.number).to eq('SN')
        expect(customer.complement).to eq('ABC')
        expect(customer.phone).to eq('12992228290')
        expect(customer.email).to eq('foo@bar.com')
        expect(customer.city_id).to eq(@city.id)
        expect(customer.corporate_name).to be_blank
        expect(customer.state_inscription).to be_blank
        expect(customer.municipal_inscription).to be_blank
        expect(customer.store_id).to eq(@store_1.id)
        expect(customer.allow_special_discount?).to eq(true)
        expect(customer.special_discount).to eq(BigDecimal.new('12.12'))
      end
    end
  end

  describe 'update' do
    let(:service) {described_class.new(parameters)}

    before do
      @customer = create(:customer, name: 'ab', store: @store_1)
    end

    context 'with invalid parameters' do
      let(:parameters) {{'name' => ''}}

      it 'returns false' do
        service.update(@customer.id)
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.update(@customer.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { { 'name' => 'Customer 1', 'identification' => '111.623.597-81', 'zip_code' => '12244-888', 
        'street' => 'St. Foo', 'neighborhood' => 'Nb bar', 'number' => 'SN', 'complement' => 'ABC',
        'phone' => '(12)99222-8290', 'email' => 'foo@bar.com', 'city_id' => "#{@city.id}", 'corporate_name' => '', 'store_id' => "#{@store_1.id}",
        'allow_special_discount' => true, 'special_discount' => '12.12'
      } }

      it 'does not create a new customer' do
        expect {
          service.update(@customer.id)
        }.to change(Customer, :count).by(0)
      end

      it 'returns the customer' do
        service.update(@customer.id)

        expect(service.record).to eq(Customer.last)
      end

      it 'returns true' do
        service.update(@customer.id)
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.update(@customer.id)

        customer = service.record

        expect(customer.name).to eq('Customer 1')
        expect(customer.identification).to eq('11162359781')
        expect(customer.zip_code).to eq('12244888')
        expect(customer.street).to eq('St. Foo')
        expect(customer.neighborhood).to eq('Nb bar')
        expect(customer.number).to eq('SN')
        expect(customer.complement).to eq('ABC')
        expect(customer.phone).to eq('12992228290')
        expect(customer.email).to eq('foo@bar.com')
        expect(customer.city_id).to eq(@city.id)
        expect(customer.corporate_name).to be_blank
        expect(customer.state_inscription).to be_blank
        expect(customer.municipal_inscription).to be_blank
        expect(customer.store_id).to eq(@store_1.id)
        expect(customer.allow_special_discount?).to eq(true)
        expect(customer.special_discount).to eq(BigDecimal.new('12.12'))
      end
    end
  end
end