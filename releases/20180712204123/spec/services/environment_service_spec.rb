require 'rails_helper'

RSpec.describe EnvironmentService, type: :service do
  before(:each) do
    @distributor = FactoryGirl.create(:distributor, name: 'D1')
    @store       = FactoryGirl.create(:store, distributor: @distributor, name: 'S1')
    @customer    = FactoryGirl.create(:customer, store_id: @store.id, name: 'C1')
    @estimate    = FactoryGirl.create(:estimate, seller: @store, client: @customer)
  end

  describe 'create' do
    let(:service) {described_class.new(parameters)}

    context 'with invalid parameters' do
      let(:parameters) { {'category' => '', 'name' => 'Foo', 'estimate_id' => "#{@estimate.id}"} }

      it 'does not create a new Environment' do
        expect {
          service.create
        }.to change(Environment, :count).by(0)
      end

      it 'returns false' do
        service.create
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.create
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { {'category' => '0', 'name' => 'Foo', 'estimate_id' => "#{@estimate.id}"} }

      it 'creates a new Environment' do
        expect {
          service.create
        }.to change(Environment, :count).by(1)
      end

      it 'returns the created environment' do
        service.create

        expect(service.record).to eq(Environment.last)
      end

      it 'returns true' do
        service.create
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.create

        environment = service.record

        expect(Environment.categories[environment.category]).to eq(0)
        expect(environment.name).to eq('Foo')
        expect(environment.estimate).to eq(@estimate)
      end
    end
  end

  describe 'update' do
    let(:service) {described_class.new(parameters)}

    before(:each) do
      @environment = FactoryGirl.create(:environment, estimate: @estimate)
    end

    context 'with invalid parameters' do
      let(:parameters) { {'category' => '', 'name' => 'Foo', 'estimate_id' => "#{@estimate.id}"} }

      it 'does not create a new Environment' do
        expect {
          service.update(@environment.id)
        }.to change(Environment, :count).by(0)
      end

      it 'returns false' do
        service.update(@environment.id)
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.update(@environment.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { {'category' => '0', 'name' => 'Foo', 'estimate_id' => "#{@estimate.id}"} }

      it 'does not create a new Estimate' do
        expect {
          service.update(@environment.id)
        }.to change(Estimate, :count).by(0)
      end

      it 'returns the environment' do
        service.update(@environment.id)

        expect(service.record).to eq(Environment.last)
      end

      it 'returns true' do
        service.update(@environment.id)
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.update(@environment.id)

        environment = service.record

        expect(Environment.categories[environment.category]).to eq(0)
        expect(environment.name).to eq('Foo')
        expect(environment.estimate).to eq(@estimate)
      end
    end
  end
end
