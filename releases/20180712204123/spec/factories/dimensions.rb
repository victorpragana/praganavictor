# == Schema Information
#
# Table name: dimensions
#
#  id         :integer          not null, primary key
#  width      :int4range        not null
#  height     :int4range        not null
#  line_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  weight     :integer          default(0), not null
#

FactoryGirl.define do
  factory :dimension do
    width  {Faker::Number.between(1, 10)..Faker::Number.between(10, 20)}
    height {Faker::Number.between(1, 10)..Faker::Number.between(10, 20)}
    weight { :general }
    line   nil
  end
end
