# == Schema Information
#
# Table name: commercial_classifications
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :commercial_classification do
    name { Faker::Name.name }
  end
end
