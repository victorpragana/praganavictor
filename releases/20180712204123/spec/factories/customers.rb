# == Schema Information
#
# Table name: customers
#
#  id         :integer          not null, primary key
#  deleted_at :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  store_id   :integer
#

FactoryGirl.define do
  factory :customer do
    name                  {Faker::Name.name}
    rg                    {"345899869"}
    issuing_entity        {"SSP-SP"}
    identification        {Faker::CPF.numeric}
    zip_code              {Faker::Address.zip_code}
    street                {Faker::Address.street_name}
    neighborhood          {Faker::Address.street_name}
    number                {Faker::Address.building_number}
    complement            {Faker::Address.secondary_address}
    phone                 {Faker::Number.number(9)}
    email                 {Faker::Internet.email}
    corporate_name        {Faker::Company.name}
  end
end
