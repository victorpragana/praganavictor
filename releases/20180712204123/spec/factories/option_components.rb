# == Schema Information
#
# Table name: option_components
#
#  id                        :integer          not null, primary key
#  option_id                 :integer
#  total_consumption         :decimal(10, 5)   not null
#  unit_value                :decimal(10, 5)   not null
#  component_name            :string(255)      not null
#  unit                      :integer          not null
#  component_characteristics :text
#  category_name             :string(255)      not null
#  width_consumption         :decimal(10, 5)
#  height_consumption        :decimal(10, 5)
#  quantity_consumption      :decimal(10, 5)
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  required                  :boolean          default(FALSE), not null
#  component_id              :integer
#  rule_id                   :integer
#  total                     :decimal(10, 2)   default(0.0)
#

FactoryGirl.define do
  factory :option_component do
    option            nil
    total_consumption {Faker::Number.between(0, 5)}
    unit_value        {Faker::Number.between(0, 5)}
    component_name    {Faker::Name.name}
    unit              {Faker::Number.between(0, 2)}
    category_name     {Faker::Name.name}
  end
end
