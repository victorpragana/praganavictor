# == Schema Information
#
# Table name: questions
#
#  id         :integer          not null, primary key
#  label      :string           not null
#  value      :string           not null
#  type       :integer          not null
#  options    :json
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :question do
    label { Faker::Name.name }
    value { Faker::Name.name.parameterize.underscore.upcase }
    type  { Question.types[:boolean] }
    options nil
  end
end
