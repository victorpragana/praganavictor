# == Schema Information
#
# Table name: line_categories
#
#  id          :integer          not null, primary key
#  line_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  required    :boolean          default(TRUE), not null
#  category_id :integer
#  order       :integer          not null
#

FactoryGirl.define do
  factory :line_category do
    line      nil
    category  nil
    required  {[true, false].sample}
    order     {0}
  end
end
