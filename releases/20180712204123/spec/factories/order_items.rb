# == Schema Information
#
# Table name: order_items
#
#  id                :integer          not null, primary key
#  line_id           :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  width             :integer          not null
#  height            :integer          not null
#  order_id          :integer
#  option_id         :integer
#  factory_total     :decimal(10, 2)   default(0.0)
#  distributor_total :decimal(10, 2)   default(0.0)
#  store_total       :decimal(10, 2)   default(0.0)
#  customer_total    :decimal(10, 2)   default(0.0)
#  discount          :decimal(5, 2)    default(0.0)
#

FactoryGirl.define do
  factory :order_item do
    line   nil
    order  nil
    width  {Faker::Number.between(1, 2000)}
    height {Faker::Number.between(1, 2000)}
  end
end
