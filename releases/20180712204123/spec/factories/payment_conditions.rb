# == Schema Information
#
# Table name: payment_conditions
#
#  id                      :integer          not null, primary key
#  installments            :integer          not null
#  additional_tax          :decimal(5, 2)    default(0.0)
#  antecipation_tax        :decimal(5, 2)    default(0.0)
#  credit_antecipation_tax :decimal(5, 2)    default(0.0)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  type                    :integer          default(0), not null
#  credit_tax              :decimal(5, 2)    default(0.0)
#

FactoryGirl.define do
  factory :payment_condition do
    installments { Faker::Number.between(1, 10) }
    additional_tax { Faker::Number.decimal(1, 2) }
    credit_tax { Faker::Number.decimal(1, 2) }
    antecipation_tax { Faker::Number.decimal(1, 2) }
    credit_antecipation_tax { Faker::Number.decimal(1, 2) }
    type { :credit_card }
  end
end
