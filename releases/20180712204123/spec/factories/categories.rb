# == Schema Information
#
# Table name: categories
#
#  id            :integer          not null, primary key
#  name          :string(255)      not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  use_cut_table :boolean          default(FALSE)
#

FactoryGirl.define do
  factory :category do
    name {Faker::Name.name}
  end
end
