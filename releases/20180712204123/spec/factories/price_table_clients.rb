# == Schema Information
#
# Table name: price_table_clients
#
#  id             :integer          not null, primary key
#  price_table_id :integer
#  client_id      :integer
#  client_type    :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

FactoryGirl.define do
  factory :price_table_client do
    price_table nil
    client      nil
  end
end
