# == Schema Information
#
# Table name: locations
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  category       :integer          not null
#  environment_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  width          :integer
#  height         :integer
#  quantity       :integer
#

FactoryGirl.define do
  factory :location do
    name        {Faker::Name.name}
    category    {Location.categories.to_a.sample[1]}
    environment nil
  end
end
