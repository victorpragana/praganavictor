# == Schema Information
#
# Table name: environments
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  category    :integer          not null
#  estimate_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryGirl.define do
  factory :environment do
    name      {Faker::Name.name}
    category  {Environment.categories.to_a.sample[1]}
    estimate  nil
  end
end
