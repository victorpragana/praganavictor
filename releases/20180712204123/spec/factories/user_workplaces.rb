# == Schema Information
#
# Table name: user_workplaces
#
#  id             :integer          not null, primary key
#  user_id        :integer
#  workplace_id   :integer
#  workplace_type :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

FactoryGirl.define do
  factory :user_workplace do
    user nil
    workplace nil
  end
end
