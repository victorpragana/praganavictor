# == Schema Information
#
# Table name: price_tables
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  owner_id   :integer
#  owner_type :string
#  name       :string           default(""), not null
#  type       :integer          default(0), not null
#  default    :boolean          default(FALSE)
#

FactoryGirl.define do
  factory :price_table do
    name  { Faker::Name.name }
    owner nil
    type  :for_lines
  end
end
