# == Schema Information
#
# Table name: orders
#
#  id                     :integer          not null, primary key
#  date                   :date             not null
#  observation            :text
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  seller_id              :integer
#  seller_type            :string
#  client_id              :integer
#  client_type            :string
#  discount               :decimal(5, 2)    default(0.0)
#  status                 :integer          default(0), not null
#  erp_id                 :integer
#  processing_messages    :text             default([]), is an Array
#  user_id                :integer
#  billing_client_id      :integer
#  billing_client_type    :string
#  special_discount       :decimal(5, 2)
#  production_status      :integer          default(0), not null
#  sent_to_production_at  :datetime
#  started_production_at  :datetime
#  finished_production_at :datetime
#  dispatched_at          :datetime
#  received_at            :datetime
#  sent_to_factory_at     :datetime
#

require 'rails_helper'

RSpec.describe OrdersController, type: :controller do
  before do
    @factory = create(:factory, name: 'F1')
    create(:administrator, username: 'F1').tap do |administrator|
      administrator.update!(selected_workplace: create(:user_workplace, workplace: @factory, user: administrator))
    end
    @seller = create(:seller, username: 'S1').tap do |seller|
      seller.update!(selected_workplace: create(:user_workplace, workplace: @factory, user: seller))
    end

    sign_in :administrator, @factory.users.first

    @family = create(:family)

    @category = create(:category, name: 'C1')
    @category_2 = create(:category, name: 'C2')
    @category_3 = create(:category, name: 'C3')

    @line = create(:line, family: @family)

    @line.categories << build(:line_category, category: @category, required: true)
    @line.categories << build(:line_category, category: @category_3, required: false)

    @line.save!

    @component_1 = create(:component, unit: :cm, category: @category, erasable: true, cost: BigDecimal.new('32.28'), name: 'CC1')
    @component_2 = create(:component, unit: :cm, category: @category_3, erasable: true, name: 'CC2')
    @component_3 = create(:component, unit: :cm, category: @category_3, erasable: false, name: 'CC3')
    @component_4 = create(:component, unit: :cm, category: @category_2, erasable: false, name: 'CC4')

    @line_rule = create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: 5..30,
                                    total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_+2', width_consumption: '_ALTURA_*3',
                                    height_consumption: '_LARGURA_ + 3', quantity_consumption: '_ALTURA_*2')
    @rule = create(:rule, line_rule: @line_rule, component: @component_1, control_method: Rule.control_methods[:width],
                          width: 5..30, total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_+2', width_consumption: '_ALTURA_*3',
                          height_consumption: '_LARGURA_ + 3', quantity_consumption: '_ALTURA_*2')

    @distributor = create(:distributor, name: 'D1')
    @store       = create(:store, distributor: @distributor, name: 'S1')
    @customer    = create(:customer, store_id: @store.id, name: 'C1')

    @order = create(:order, environments: [], seller: @store, client: @customer, billing_client: @customer, user: @factory.users.first)
  end

  describe 'GET index' do
    render_views

    before do
      @order_item_1 = create(:order_item, order: @order, line: @line, 
        components: [
          build(:order_item_component, component_id: @component_1.id, component_name: @component_1.name, unit: @component_1.unit),
          build(:order_item_component, component_id: @component_2.id, component_name: @component_2.name, unit: @component_2.unit)
        ]
      )
      @order_item_2 = create(:order_item, order: @order, line: @line,
        components: [
          build(:order_item_component, component_id: @component_3.id, component_name: @component_3.name, unit: @component_3.unit)
        ]
      )
      @order_item_3 = create(:order_item, order: @order, line: @line,
        components: [
          build(:order_item_component, component_id: @component_4.id, component_name: @component_4.name, unit: @component_4.unit)
        ]
      )
      @estimate    = create(:estimate, seller: @store, client: @customer)
      @order.estimate = @estimate
      @order.save!


      @order2 = create(:order, seller: @store, client: @customer, billing_client: @customer, user: @seller)
      @estimate2    = create(:estimate, seller: @store, client: @customer)
      @order2.estimate = @estimate2
      @order2.save!
    end

    it 'returns OK' do
      xhr :get, :index
      expect(response).to have_http_status(:ok)
    end
    context 'when admin user' do 
      it 'returns all records' do 
        xhr :get, :index
        expect(json['recordsTotal']).to eq(2)
      end
    end
    context 'when seller user' do 
      before do 
        sign_out :administrator
        sign_in :seller, @seller
      end
      it 'returns only order from seller' do 
        xhr :get, :index
        expect(json['recordsTotal']).to eq(1)
      end
    end
    
  end

  describe 'GET show' do
    render_views

    let(:order_item) { mock_model(@order_item, total: BigDecimal.new('32.28')) }

    before do
      item_component_1 = build(:order_item_component, component_id: @component_1.id, component_name: @component_1.name, component_characteristics: @component_1.characteristics,
                                                      category_name: @component_1.category.name, unit: @component_1.unit, total_consumption: 100, unit_value: 250, width_consumption: 20,
                                                      height_consumption: 30, quantity_consumption: 100, required: true, total: 25_000)
      item_component_2 = build(:order_item_component, component_id: @component_2.id, component_name: @component_2.name, component_characteristics: @component_2.characteristics,
                                                      category_name: @component_2.category.name, unit: @component_2.unit, total_consumption: 20, unit_value: 65, required: false, total: 1300)
      @order_item      = create(:order_item, order: @order, line: @line, components: [item_component_1, item_component_2])

      @estimate    = create(:estimate, seller: @store, client: @customer)
      @environment = create(:environment, estimate: @estimate)
      @location    = create(:location, environment: @environment)
      @option      = create(:option, location: @location, order_item: @order_item, line: @line)

      @order.estimate = @estimate
      @order.save!

      @payment = create(:payment)
      create(:production_credit, owner: @store.third_party, destination: @distributor.third_party, payment: @payment, order: @order, reason: :order_payment, direction: :input, status: :approved)
    end

    it 'returns the ok status' do
      xhr :get, :show, id: @order.id

      expect(response).to have_http_status(:ok)
    end

    it 'returns the order data' do
      xhr :get, :show, id: @order.id

      expect(json['order']['id']).to eq(@order.id)
      expect(json['order']['date']).to eq(@order.date.strftime('%d/%m/%Y'))
      expect(json['order']['partial_total']).to eq(@order.partial_total)
      expect(json['order']['commercial_discount']).to eq(@order.commercial_discount)
      expect(json['order']['discount']).to eq(@order.discount)
      expect(json['order']['discount_value']).to eq(@order.discount_value)
      expect(json['order']['total_discount']).to eq(@order.total_discount)
      expect(json['order']['total_with_discount']).to eq(@order.total_with_discount)
      expect(json['order']['total_with_special_discount']).to eq(@order.total_with_special_discount)
      expect(json['order']['total']).to eq(@order.total)
      expect(json['order']['special_discount']).to eq(@order.special_discount)
      expect(json['order']['observation']).to eq(@order.observation)
      expect(json['order']['status']).to eq(Order.statuses[@order.status])
      expect(json['order']['production_status']).to eq(Order.production_statuses[@order.production_status])
      expect(json['order']['seller']['id']).to eq(@order.seller_id)
      expect(json['order']['seller']['name']).to eq(@order.seller.name)
      expect(json['order']['seller']['type']).to eq(@order.seller.class.to_s)
      expect(json['order']['estimate_id']).to eq(@order.estimate.try(:id))
      expect(json['order']['client']['id']).to eq(@order.client_id)
      expect(json['order']['client']['name']).to eq(@order.client.name)
      expect(json['order']['client']['type']).to eq(@order.client.class.to_s)
      expect(json['order']['billing_client']['id']).to eq(@order.billing_client_id)
      expect(json['order']['billing_client']['name']).to eq(@order.billing_client.name)
      expect(json['order']['billing_client']['type']).to eq(@order.billing_client.class.to_s)
      expect(json['order']['billing_client']['allow_special_discount']).to eq(@order.billing_client.allow_special_discount?)
      expect(json['order']['billing_client']['special_discount']).to eq(@order.billing_client.special_discount.to_f)
      expect(json['order']['processing_messages']).to eq(@order.processing_messages)

      expect(json['order']['environments'][0]['id']).to eq(@environment.id)
      expect(json['order']['environments'][0]['name']).to eq(@environment.name)
      expect(json['order']['environments'][0]['category']).to eq(Environment.categories[@environment.category])
      expect(json['order']['environments'][0]['has_order_items']).to eq(true)
      expect(json['order']['environments'][0]['total']).to eq(@order_item.total)

      expect(json['order']['environments'][0]['locations'][0]['name']).to eq(@location.name)
      expect(json['order']['environments'][0]['locations'][0]['category']).to eq(Location.categories[@location.category])
      expect(json['order']['environments'][0]['locations'][0]['has_order_items']).to eq(true)

      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['id']).to eq(@order_item.id)
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['line']['id']).to eq(@order_item.line.id)
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['line']['name']).to eq(@order_item.line.name)
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['line']['family']['id']).to eq(@order_item.line.family.id)
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['line']['family']['name']).to eq(@order_item.line.family.name)
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['line']['family']['color']).to eq(@order_item.line.family.color)
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['width']).to eq(@order_item.width_from_mm_to_cm)
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['height']).to eq(@order_item.height_from_mm_to_cm)
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['discount'].to_f).to eq(@order_item.discount)
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['total_with_discount'].to_f).to eq(@order_item.total_with_discount)
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['total'].to_f).to eq(@order_item.total)
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['components']).to_not be_empty
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['components'].size).to eq(2)

      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['components'][1]['component']['name']).to eq(@component_1.name)
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['components'][1]['component']['characteristics']).to eq(@component_1.characteristics)
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['components'][1]['component']['unit']).to eq(Component.units[@component_1.unit])
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['components'][1]['component']['category']['name']).to eq(@category.name)
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['components'][1]['total_consumption']).to eq(BigDecimal.new('100'))
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['components'][1]['width_consumption']).to eq(BigDecimal.new('20'))
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['components'][1]['height_consumption']).to eq(BigDecimal.new('30'))
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['components'][1]['quantity_consumption']).to eq(BigDecimal.new('100'))
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['components'][1]['unit_value']).to eq(BigDecimal.new('250'))
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['components'][1]['total']).to eq(BigDecimal.new('25000'))
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['components'][1]['required']).to eq(true)

      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['components'][0]['component']['name']).to eq(@component_2.name)
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['components'][0]['component']['characteristics']).to eq(@component_2.characteristics)
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['components'][0]['component']['unit']).to eq(Component.units[@component_2.unit])
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['components'][0]['component']['category']['name']).to eq(@category_3.name)
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['components'][0]['total_consumption']).to eq(BigDecimal.new('20'))
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['components'][0]['width_consumption']).to be_nil
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['components'][0]['height_consumption']).to be_nil
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['components'][0]['quantity_consumption']).to be_nil
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['components'][0]['unit_value']).to eq(BigDecimal.new('65'))
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['components'][0]['total']).to eq(BigDecimal.new('1300'))
      expect(json['order']['environments'][0]['locations'][0]['order_items'][0]['components'][0]['required']).to eq(false)

      expect(json['order']['has_payments']).to eq(@order.has_payments?)
      expect(json['order']['payments'][0]['id']).to eq(@payment.id)
      expect(json['order']['payments'][0]['installment_number']).to eq(@payment.installment_number)
      expect(json['order']['payments'][0]['payment_method']).to eq(Payment.payment_methods[@payment.payment_method])
      expect(json['order']['payments'][0]['value']).to eq(@payment.original_value.to_f)
    end

    it 'returns has order items false' do
      @option.order_item = nil
      @option.save!

      xhr :get, :show, id: @order.id

      expect(json['order']['environments'][0]['has_order_items']).to eq(false)
      expect(json['order']['environments'][0]['locations'][0]['has_order_items']).to eq(false)
    end
  end

  describe 'POST generate' do
    render_views

    before do
      @component_1 = create(:component, unit: :cm, category: @category, erasable: true, cost: BigDecimal.new('32.28'), name: 'C1')
      @component_2 = create(:component, unit: :cm, category: @category_3, erasable: true, cost: BigDecimal.new('20'), name: 'C2')

      line_rule = create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: 5..30, required: true,
                                     width_consumption: '_LARGURA_*3', height_consumption: '_LARGURA_', total_consumption: '_LARGURA_*2',
                                     cost: '_CUSTO_BASE_+2', quantity_consumption: '_LARGURA_*2')
      create(:rule, line_rule: @line_rule, component: @component_1, control_method: Rule.control_methods[:width], width: 5..30, required: true,
                    width_consumption: '_LARGURA_*3', height_consumption: '_LARGURA_', total_consumption: '_LARGURA_*2',
                    cost: '_CUSTO_BASE_+2', quantity_consumption: '_LARGURA_*2')

      line_rule = create(:line_rule, line: @line, control_method: LineRule.control_methods[:height], height: 5..30, required: false,
                                     total_consumption: '_ALTURA_*2',
                                     cost: '_CUSTO_BASE_*2')
      create(:rule, line_rule: @line_rule, component: @component_2, control_method: Rule.control_methods[:height], height: 5..30, required: false,
                    total_consumption: '_ALTURA_*2',
                    cost: '_CUSTO_BASE_*2')

      @distributor = create(:distributor, name: 'D2')
      @store       = create(:store, distributor: @distributor, name: 'S3')
      @estimate = create(:estimate, seller: @store, client: @customer)
      @environment = create(:environment, estimate: @estimate)
      @location = create(:location, environment: @environment)

      item_component_1 = build(:option_component, component_id: @component_1.id, component_name: @component_1.name, component_characteristics: @component_1.characteristics, category_name: @component_1.category.name, unit: @component_1.unit, total_consumption: 100, unit_value: 250, width_consumption: 20, height_consumption: 30, quantity_consumption: 100, required: true)
      item_component_2 = build(:option_component, component_id: @component_2.id, component_name: @component_2.name, component_characteristics: @component_2.characteristics, category_name: @component_2.category.name, unit: @component_2.unit, total_consumption: 20, unit_value: 65, required: false)
      @option_1 = create(:option, line: @line, location: @location, components: [item_component_1, item_component_2], selected: false)

      item_component_1 = build(:option_component, component_id: @component_1.id, component_name: @component_1.name, component_characteristics: @component_1.characteristics, category_name: @component_1.category.name, unit: @component_1.unit, total_consumption: 100, unit_value: 250, width_consumption: 20, height_consumption: 30, quantity_consumption: 100, required: true)
      item_component_2 = build(:option_component, component_id: @component_2.id, component_name: @component_2.name, component_characteristics: @component_2.characteristics, category_name: @component_2.category.name, unit: @component_2.unit, total_consumption: 20, unit_value: 65, required: false)
      @option_2 = create(:option, line: @line, location: @location, components: [item_component_1], selected: false)

      item_component_1 = build(:option_component, component_id: @component_1.id, component_name: @component_1.name, component_characteristics: @component_1.characteristics, category_name: @component_1.category.name, unit: @component_1.unit, total_consumption: 100, unit_value: 250, width_consumption: 20, height_consumption: 30, quantity_consumption: 100, required: true)
      item_component_2 = build(:option_component, component_id: @component_2.id, component_name: @component_2.name, component_characteristics: @component_2.characteristics, category_name: @component_2.category.name, unit: @component_2.unit, total_consumption: 20, unit_value: 65, required: false)
      @option_3 = create(:option, line: @line, location: @location, components: [item_component_2], selected: false)
    end

    context 'with invalid data' do
      let(:parameters) { { 'estimate_id' => @estimate.id.to_s } }
      let(:service)    { double(:service, perform: false, errors: ['foo'], success: false) }

      before do
        allow(GenerateOrderService).to receive(:new).and_return(service)
      end

      it 'does not create a new Order' do
        expect do
          xhr :post, :generate, parameters
        end.to_not change(Order, :count)
      end

      it 'does not create a new Order Item' do
        expect do
          xhr :post, :generate, parameters
        end.to_not change(OrderItem, :count)
      end

      it 'does not create a new Order Item Components' do
        expect do
          xhr :post, :generate, parameters
        end.to_not change(OrderItemComponent, :count)
      end

      it 'returns unprocessable entity status' do
        xhr :post, :generate, parameters

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :generate, parameters

        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) { { 'estimate_id' => @estimate.id.to_s } }

      before do
        @option_1.update(selected: true)
        @option_3.update(selected: true)
        allow_any_instance_of(TrelloIntegrationService).to receive(:create_trello_card)
      end

      it 'initializes the generate order service' do
        expect(GenerateOrderService).to receive(:new).with(parameters.merge('user_id' => @factory.users.first.id)).and_call_original

        xhr :post, :generate, parameters
      end

      it 'generates the order' do
        expect_any_instance_of(GenerateOrderService).to receive(:perform).and_call_original

        xhr :post, :generate, parameters
      end

      it 'returns created status' do
        xhr :post, :generate, parameters

        expect(response).to have_http_status(:created)
      end

      it 'renders template show' do
        xhr :post, :generate, parameters

        expect(response).to render_template('orders/show')
      end
    end
  end

  describe 'PUT send_to_erp' do
    let(:payment_condition_id) { '20' }

    context 'when the order is \'to_send\' or \'error\'' do
      before do
        @order.update(status: [Order.statuses[:to_send], Order.statuses[:error]].sample)

        allow(ErpWorker).to receive(:perform_async).and_return('123AAA')
      end

      it 'enqueues the order sending' do
        expect(ErpWorker).to receive(:perform_async).with(@order.id, payment_condition_id)

        xhr :put, :send_to_erp, id: @order.id, payment_condition_id: payment_condition_id
      end

      it 'returns ok status' do
        xhr :put, :send_to_erp, id: @order.id, payment_condition_id: payment_condition_id

        expect(response).to have_http_status(:ok)
      end

      it 'returns the job id' do
        xhr :put, :send_to_erp, id: @order.id, payment_condition_id: payment_condition_id

        expect(json['order']['job_id']).to eq('123AAA')
      end
    end

    context 'when the order is \'sending\'' do
      before do
        @order.update(status: Order.statuses[:sending])
      end

      it 'returns bad request status' do
        xhr :put, :send_to_erp, id: @order.id, payment_condition_id: payment_condition_id

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the sending message' do
        xhr :put, :send_to_erp, id: @order.id, payment_condition_id: payment_condition_id

        expect(json['errors']).to match_array([I18n.t('activerecord.errors.models.order.sending')])
      end
    end

    context 'when the order is \'sent\'' do
      before do
        @order.update(status: Order.statuses[:sent], erp_id: 10)
      end

      it 'returns bad request status' do
        xhr :put, :send_to_erp, id: @order.id, payment_condition_id: payment_condition_id

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the already processed message' do
        xhr :put, :send_to_erp, id: @order.id, payment_condition_id: payment_condition_id

        expect(json['errors']).to match_array([I18n.t('activerecord.errors.models.order.already_sent')])
      end
    end
  end

  describe 'PUT cancel' do
    context 'when the order is \'sent\'' do
      let(:orders) { double(:orders, cancel: double(:cancel_response, status: 200, payload: nil)) }
      let(:client) { double(:service, orders: orders) }

      before do
        allow(Varejonline).to receive(:new).and_return(client)

        @order.update(status: Order.statuses[:sent], erp_id: 10)
      end

      it 'cancels the order in the erp' do
        expect(orders).to receive(:cancel).with(@order.erp_id)

        xhr :put, :cancel, id: @order.id
      end

      context 'with success' do
        it 'changes the order status to \'to_send\'' do
          expect do
            xhr :put, :cancel, id: @order.id
            @order.reload
          end.to change(@order, :status).to('to_send')
        end

        it 'returns no content status' do
          xhr :put, :cancel, id: @order.id

          expect(response).to have_http_status(:no_content)
        end
      end

      context 'with error' do
        let(:orders) { double(:orders, cancel: double(:cancel_response, status: 400, payload: 'foo')) }

        it 'returns bad request status' do
          xhr :put, :cancel, id: @order.id

          expect(response).to have_http_status(:bad_request)
        end

        it 'returns the error message' do
          xhr :put, :cancel, id: @order.id

          expect(json['errors']).to match_array(['foo'])
        end
      end
    end

    context 'when the order is \'sending\', \'to_send\' or \'error\'' do
      before do
        @order.update(status: [Order.statuses[:sending], Order.statuses[:to_send], Order.statuses[:error]].sample)
      end

      it 'returns bad request status' do
        xhr :put, :cancel, id: @order.id

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the only sent allowed message' do
        xhr :put, :cancel, id: @order.id

        expect(json['errors']).to match_array([I18n.t('activerecord.errors.models.order.only_sent')])
      end
    end
  end

  describe 'PUT change_billing_client' do
    let(:service) { double(:service, change_billing_client: true, success: true) }

    before do
      allow(OrderService).to receive(:new).and_return(service)
    end

    it 'initializes a new Order service' do
      expect(OrderService).to receive(:new).with(billing_client_id: @distributor.id.to_s, billing_client_type: 'Distributor')

      xhr :put, :change_billing_client, id: @order.id, billing_client_id: @distributor.id, billing_client_type: 'Distributor'
    end

    it 'tries to update the billing client' do
      expect(service).to receive(:change_billing_client).with(@order.id.to_s)

      xhr :put, :change_billing_client, id: @order.id, billing_client_id: @distributor.id, billing_client_type: 'Distributor'
    end

    context 'with invalid data' do
      let(:service) { double(:service, change_billing_client: false, success: false, errors: ['foo']) }

      it 'returns unprocessable entity status' do
        xhr :put, :change_billing_client, id: @order.id, billing_client_id: @distributor.id, billing_client_type: 'Distributor'

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :put, :change_billing_client, id: @order.id, billing_client_id: @distributor.id, billing_client_type: 'Distributor'

        expect(json['errors']).to match_array(['foo'])
      end
    end

    context 'with valid data' do
      it 'returns ok status' do
        xhr :put, :change_billing_client, id: @order.id, billing_client_id: @distributor.id, billing_client_type: 'Distributor'

        expect(response).to have_http_status(:ok)
      end

      it 'renders nothing' do
        xhr :put, :change_billing_client, id: @order.id, billing_client_id: @distributor.id, billing_client_type: 'Distributor'

        expect(response.body).to be_blank
      end
    end
  end

  describe 'PUT update_special_discount' do
    let(:service) { double(:service, update_special_discount: true, success: true) }
    let(:data)    { '85.26' }

    before do
      allow(OrderService).to receive(:new).and_return(service)
    end

    it 'initializes a new Order service' do
      expect(OrderService).to receive(:new).with(special_discount: data)

      xhr :put, :update_special_discount, id: @order.id, special_discount: data
    end

    it 'tries to update the special discount client' do
      expect(service).to receive(:update_special_discount).with(@order.id.to_s)

      xhr :put, :update_special_discount, id: @order.id, special_discount: data
    end

    context 'with invalid data' do
      let(:service) { double(:service, update_special_discount: false, success: false, errors: ['foo']) }

      it 'returns unprocessable entity status' do
        xhr :put, :update_special_discount, id: @order.id, special_discount: data

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :put, :update_special_discount, id: @order.id, special_discount: data

        expect(json['errors']).to match_array(['foo'])
      end
    end

    context 'with valid data' do
      it 'returns ok status' do
        xhr :put, :update_special_discount, id: @order.id, special_discount: data

        expect(response).to have_http_status(:ok)
      end

      it 'renders nothing' do
        xhr :put, :update_special_discount, id: @order.id, special_discount: data

        expect(response.body).to be_blank
      end
    end
  end

  describe 'DELETE destroy' do
    let(:service) { double(:service, destroy: true, success: true) }

    before do
      allow(OrderService).to receive(:new).and_return(service)
    end

    it 'initializes a new Order service' do
      expect(OrderService).to receive(:new)

      xhr :delete, :destroy, id: @order.id
    end

    it 'tries to delete the order' do
      expect(service).to receive(:destroy).with(@order.id.to_s)

      xhr :delete, :destroy, id: @order.id
    end

    context 'with invalid data' do
      let(:service) { double(:service, destroy: false, success: false, errors: ['foo']) }

      it 'returns bad request status' do
        xhr :delete, :destroy, id: @order.id

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the errors' do
        xhr :delete, :destroy, id: @order.id

        expect(json['errors']).to match_array(['foo'])
      end
    end

    context 'with valid data' do
      it 'returns no content status' do
        xhr :delete, :destroy, id: @order.id

        expect(response).to have_http_status(:no_content)
      end

      it 'renders nothing' do
        xhr :delete, :destroy, id: @order.id

        expect(response.body).to be_blank
      end
    end
  end

  describe 'GET lines_label' do
    let(:service) { double(:service, lines_label: nil, success: true) }

    before do
      allow(OrderLabelService).to receive(:new).and_return(service)
    end

    it 'initializes a new Order Label service' do
      expect(OrderLabelService).to receive(:new).with(@order.id.to_s, @factory.users.first.id)

      xhr :get, :lines_label, id: @order.id
    end

    it 'generates the lines label' do
      expect(service).to receive(:lines_label)

      xhr :get, :lines_label, id: @order.id
    end

    context 'with invalid data' do
      let(:service) { double(:service, lines_label: nil, success: false) }

      it 'returns bad request' do
        xhr :get, :lines_label, id: @order.id

        expect(response).to have_http_status(:bad_request)
      end
    end

    context 'with valid data' do
      it 'returns ok status' do
        xhr :get, :lines_label, id: @order.id

        expect(response).to have_http_status(:ok)
      end

      it 'renders nothing' do
        xhr :get, :lines_label, id: @order.id

        expect(response.body).to be_empty
      end
    end
  end

  describe 'GET components_label' do
    let(:service) { double(:service, components_label: nil, success: true) }

    before do
      allow(OrderLabelService).to receive(:new).and_return(service)
    end

    it 'initializes a new Order Label service' do
      expect(OrderLabelService).to receive(:new).with(@order.id.to_s, @factory.users.first.id)

      xhr :get, :components_label, id: @order.id
    end

    it 'generates the lines label' do
      expect(service).to receive(:components_label)

      xhr :get, :components_label, id: @order.id
    end

    context 'with invalid data' do
      let(:service) { double(:service, components_label: nil, success: false) }

      it 'returns bad request' do
        xhr :get, :components_label, id: @order.id

        expect(response).to have_http_status(:bad_request)
      end
    end

    context 'with valid data' do
      it 'returns ok status' do
        xhr :get, :components_label, id: @order.id

        expect(response).to have_http_status(:ok)
      end

      it 'renders nothing' do
        xhr :get, :components_label, id: @order.id

        expect(response.body).to be_empty
      end
    end
  end

  describe 'GET cut_map' do
    let(:service) { double(:service, generate: 'foo/bar') }

    before do
      allow(OrderCutMapService).to receive(:new).and_return(service)
    end

    it 'initializes a new Order Cut Map service' do
      expect(OrderCutMapService).to receive(:new).with(@order.id.to_s)

      xhr :get, :cut_map, id: @order.id
    end

    it 'generates the cut map' do
      expect(service).to receive(:generate)

      xhr :get, :cut_map, id: @order.id
    end

    context 'with invalid data' do
      let(:service) { double(:service, generate: nil) }

      it 'returns bad request' do
        xhr :get, :cut_map, id: @order.id

        expect(response).to have_http_status(:bad_request)
      end
    end

    context 'with valid data' do
      it 'returns ok status' do
        xhr :get, :cut_map, id: @order.id

        expect(response).to have_http_status(:ok)
      end

      it 'renders nothing' do
        xhr :get, :cut_map, id: @order.id

        expect(json['path']).to eq('foo/bar')
      end
    end
  end

  describe 'PUT production_status' do
    let(:service) { double(:service, update_production_status: true, success: true) }
    let(:data)    { 'send_to_production' }

    before do
      allow(OrderService).to receive(:new).and_return(service)
    end

    it 'initializes a new Order service' do
      expect(OrderService).to receive(:new).with(transition: data)

      xhr :put, :production_status, id: @order.id, transition: data
    end

    it 'tries to update the production status' do
      expect(service).to receive(:update_production_status).with(@order.id.to_s)

      xhr :put, :production_status, id: @order.id, transition: data
    end

    context 'with invalid data' do
      let(:service) { double(:service, update_production_status: false, success: false, errors: ['foo']) }

      it 'returns unprocessable entity status' do
        xhr :put, :production_status, id: @order.id, transition: data

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :put, :production_status, id: @order.id, transition: data

        expect(json['errors']).to match_array(['foo'])
      end
    end

    context 'with valid data' do
      it 'returns ok status' do
        xhr :put, :production_status, id: @order.id, transition: data

        expect(response).to have_http_status(:ok)
      end

      it 'renders nothing' do
        xhr :put, :production_status, id: @order.id, transition: data

        expect(response.body).to be_blank
      end
    end
  end
end
