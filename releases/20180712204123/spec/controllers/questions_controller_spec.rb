# == Schema Information
#
# Table name: questions
#
#  id         :integer          not null, primary key
#  label      :string           not null
#  value      :string           not null
#  type       :integer          not null
#  options    :json
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe QuestionsController, type: :controller do
  before do
    @factory = create(:factory, name: 'F1')

    create(:administrator, username: 'F1').tap do |administrator|
      administrator.update!(selected_workplace: create(:user_workplace, workplace: @factory, user: administrator))
    end

    sign_in :administrator, @factory.users.first
  end
  
  describe "GET index" do
    render_views

    before do
      @question1 = create(:question, label: 'b')
      @question2 = create(:question, label: 'a')
      @question3 = create(:question, label: 'c')
    end

    it "returns OK" do
      xhr :get, :index

      expect(response).to have_http_status(:ok)
    end
  end

  describe "POST create" do
    render_views

    context "with invalid params" do
      let(:parameters) { { "label" => "", "value" => "", 'type' => '', 'options' => '' } }

      it "does not create a new Question" do
        expect {
          xhr :post, :create, parameters
        }.to_not change(Question, :count)
      end

      it "returns unprocessable entity status" do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "returns the errors" do
        xhr :post, :create, parameters
        
        expect(json["errors"]).to_not be_empty
      end
    end

    context "with valid params" do
      let(:parameters) { { "label" => "Local instalação", "value" => "LOCAL_INSTALACAO", 'type' => type, 'options' => options } }
      let(:type) { "#{Question.types[:dropdown]}" }
      let(:options) { [{ 'label' => 'Foo', 'value' => 'foo' }, { 'label' => 'Bar', 'value' => 'ba' }] }

      it "initializes the question service" do
        expect(QuestionService).to receive(:new).with(parameters).and_call_original

        xhr :post, :create, parameters
      end

      it "creates the question" do
        expect_any_instance_of(QuestionService).to receive(:create).and_call_original
        
        xhr :post, :create, parameters
      end

      it "returns created status" do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:created)
      end
    end
  end

  describe "GET show" do
    render_views

    before do
      @family = create(:family, name: 'F1')

      @line1 = create(:line, family: @family, name: 'L1', horizontal_increment: 100, vertical_increment: 150, active: true)
      @line2 = create(:line, family: @family, name: 'L2', horizontal_increment: 200, vertical_increment: 250, active: false)

      @question = create(:question, type: :dropdown, options: [{ 'label' => 'Foo', 'value' => 'foo' }, { 'label' => 'Bar', 'value' => 'bar' }], line_ids: [@line1.id, @line2.id])
    end

    it "returns ok status" do
      xhr :get, :show, id: @question.id

      expect(response).to have_http_status(:ok)
    end

    it "returns the question data ordered by label" do
      xhr :get, :show, id: @question.id

      expect(json["question"]["id"]).to eq(@question.id)
      expect(json["question"]["label"]).to eq(@question.label)
      expect(json["question"]["value"]).to eq(@question.value)
      expect(json["question"]["type"]).to eq(Question.types[@question.type])
      
      expect(json["question"]["options"].size).to eq(2)
      expect(json["question"]["options"][0]['label']).to eq('Bar')
      expect(json["question"]["options"][0]['value']).to eq('bar')
      expect(json["question"]["options"][1]['label']).to eq('Foo')
      expect(json["question"]["options"][1]['value']).to eq('foo')
      expect(json["question"]["line_ids"]).to match_array([@line1.id, @line2.id])
    end
  end

  describe "PUT update" do
    render_views

    before do
      @question = create(:question, label: 'b')
    end

    context "with invalid params" do
      let(:parameters) { { "label" => "", "value" => "", 'type' => '', 'options' => '' }.merge('id' => @question.id.to_s) }

      it "does not create a new Question" do
        expect {
          xhr :put, :update, parameters
        }.to change(Question, :count).by(0)
      end

      it "returns unprocessable entity status" do
        xhr :put, :update, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "returns the errors" do
        xhr :put, :update, parameters
        
        expect(json["errors"]).to_not be_empty
      end
    end

    context "with valid params" do
      let(:parameters) { { "label" => "Local instalação", "value" => "LOCAL_INSTALACAO", 'type' => type, 'options' => options } }
      let(:type) { "#{Question.types[:dropdown]}" }
      let(:options) { [{ 'label' => 'Foo', 'value' => 'foo' }, { 'label' => 'Bar', 'value' => 'ba' }] }

      it "initializes the question service" do
        expect(QuestionService).to receive(:new).with(parameters).and_call_original

        xhr :put, :update, parameters.merge({"id" => "#{@question.id}"})
      end

      it "updates the question" do
        expect_any_instance_of(QuestionService).to receive(:update).with(@question.id.to_s).and_call_original
        
        xhr :put, :update, parameters.merge({"id" => "#{@question.id}"})
      end

      it "returns ok status" do
        xhr :put, :update, parameters.merge({"id" => "#{@question.id}"})
        
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe "DELETE destroy" do
    before do
      @question = create(:question, label: 'b')
    end

    context "with success" do
      it "destroys the question" do
        expect {
          xhr :delete, :destroy, {id: @question.id}
        }.to change(Question, :count).by(-1)
      end

      it "returns no content status" do
        xhr :delete, :destroy, {id: @question.id}

        expect(response).to have_http_status(:no_content)
      end
    end

    context "with error" do
      let(:question) { mock_model(Question, destroy: false, errors: double(:errors, full_messages: ["foo"])) }

      before do
        allow(Question).to receive(:find).and_return(question)
      end

      it "does not destroy the question" do
        expect {
          xhr :delete, :destroy, { id: question.id }
        }.to_not change(Question, :count)
      end

      it "returns the bad request status" do
        xhr :delete, :destroy, { id: question.id }

        expect(response).to have_http_status(:bad_request)
      end

      it "returns the errors" do
        xhr :delete, :destroy, { id: question.id }
        
        expect(json["errors"]).to_not be_empty
      end
    end
  end

  describe "POST set_lines" do
    render_views

    before do
      @question = create(:question, label: 'b')

      @family = create(:family, name: 'F1')

      @line1 = create(:line, family: @family, name: 'L1', horizontal_increment: 100, vertical_increment: 150, active: true)
      @line2 = create(:line, family: @family, name: 'L2', horizontal_increment: 200, vertical_increment: 250, active: false)
    end

    context "with invalid params" do
      let(:parameters) { { "line_ids" => "" }.merge('id' => @question.id.to_s) }
      let(:service) { double(:service, set_lines: nil, success: false, errors: ['foo']) }

      before do
        allow(QuestionService).to receive(:new).and_return(service)
      end

      it "does not set the lines" do
        expect {
          xhr :post, :set_lines, parameters
          @question.reload
        }.to_not change(@question, :line_ids)
      end

      it "returns unprocessable entity status" do
        xhr :post, :set_lines, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "returns the errors" do
        xhr :post, :set_lines, parameters
        
        expect(json["errors"]).to_not be_empty
      end
    end

    context "with valid params" do
      let(:parameters) { { "line_ids" => [@line1.id.to_s, @line2.id.to_s] } }

      it "initializes the question service" do
        expect(QuestionService).to receive(:new).with(parameters).and_call_original

        xhr :post, :set_lines, parameters.merge('id' => @question.id.to_s)
      end

      it "sets the lines" do
        expect_any_instance_of(QuestionService).to receive(:set_lines).and_call_original
        
        xhr :post, :set_lines, parameters.merge('id' => @question.id.to_s)
      end

      it "returns ok status" do
        xhr :post, :set_lines, parameters.merge('id' => @question.id.to_s)
        
        expect(response).to have_http_status(:ok)
      end
    end
  end
end
