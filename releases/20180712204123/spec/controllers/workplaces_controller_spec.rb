require 'rails_helper'

RSpec.describe WorkplacesController, type: :controller do
  before do
    allow_any_instance_of(Paperclip::Attachment).to receive(:save).and_return(true)
  end
  
  describe 'GET index' do
    render_views

    before do
      @factory = FactoryGirl.create(:factory, name: 'F1')
      FactoryGirl.create(:administrator, username: 'F1').tap do |administrator|
        administrator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @factory, user: administrator))
      end

      @distributor = FactoryGirl.create(:distributor, name: 'D1')
      FactoryGirl.create(:administrator, username: 'D1').tap do |administrator|
        administrator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @distributor, user: administrator))
      end

      @store1 = FactoryGirl.create(:store, name: 'S1', distributor: @distributor)
      FactoryGirl.create(:administrator, username: 'S1').tap do |administrator|
        administrator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @store1, user: administrator))
      end

      @store2 = FactoryGirl.create(:store, name: 'S2', distributor: @distributor)
      FactoryGirl.create(:administrator, username: 'S2').tap do |administrator|
        administrator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @store2, user: administrator))
      end

      @customer = FactoryGirl.create(:customer, name: 'C1', store_id: @store1.id)
    end

    context 'with factory, distributor or store logged' do
      it 'returns the ok status' do
        @user = [@factory.users.first, @distributor.users.first, @store1.users.first, @store2.users.first].sample

        sign_in @user.class.to_s.underscore.to_sym, @user

        xhr :get, :index

        expect(response).to have_http_status(:ok)
      end

      context 'with logged factory' do
        before(:each) do
          sign_in :administrator, @factory.users.first
        end

        it 'returns the factories, distributors and stores' do
          xhr :get, :index

          expect(json['workplaces'].size).to eq(4)
          expect(json['workplaces'][0]['id']).to eq(@factory.id)
          expect(json['workplaces'][0]['type']).to eq(@factory.class.to_s)
          expect(json['workplaces'][0]['name']).to eq(@factory.name)
          expect(json['workplaces'][1]['id']).to eq(@distributor.id)
          expect(json['workplaces'][1]['type']).to eq(@distributor.class.to_s)
          expect(json['workplaces'][1]['name']).to eq(@distributor.name)
          expect(json['workplaces'][2]['id']).to eq(@store1.id)
          expect(json['workplaces'][2]['type']).to eq(@store1.class.to_s)
          expect(json['workplaces'][2]['name']).to eq(@store1.name)
          expect(json['workplaces'][3]['id']).to eq(@store2.id)
          expect(json['workplaces'][3]['type']).to eq(@store2.class.to_s)
          expect(json['workplaces'][3]['name']).to eq(@store2.name)
        end
      end

      context 'with logged distributor' do
        before(:each) do
          sign_in :administrator, @distributor.users.first
        end

        it 'returns the distributor, and stores' do
          xhr :get, :index

          expect(json['workplaces'].size).to eq(3)
          expect(json['workplaces'][0]['id']).to eq(@distributor.id)
          expect(json['workplaces'][0]['type']).to eq(@distributor.class.to_s)
          expect(json['workplaces'][0]['name']).to eq(@distributor.name)
          expect(json['workplaces'][1]['id']).to eq(@store1.id)
          expect(json['workplaces'][1]['type']).to eq(@store1.class.to_s)
          expect(json['workplaces'][1]['name']).to eq(@store1.name)
          expect(json['workplaces'][2]['id']).to eq(@store2.id)
          expect(json['workplaces'][2]['type']).to eq(@store2.class.to_s)
          expect(json['workplaces'][2]['name']).to eq(@store2.name)
        end
      end

      context 'with logged store' do
        before(:each) do
          sign_in :administrator, @store1.users.first
        end

        it 'returns the store' do
          xhr :get, :index

          expect(json['workplaces'].size).to eq(1)
          expect(json['workplaces'][0]['id']).to eq(@store1.id)
          expect(json['workplaces'][0]['type']).to eq(@store1.class.to_s)
          expect(json['workplaces'][0]['name']).to eq(@store1.name)
        end
      end
    end
  end

  describe 'GET current' do
    render_views

    context 'when there is a workplace for the subdomain' do
      before do
        @factory = FactoryGirl.create(:factory, name: 'F1', subdomain: 'fabrica', logo: fixture_file_upload('/files/avatar.jpg', 'image/jpg'))
        FactoryGirl.create(:administrator, username: 'F1').tap do |administrator|
          administrator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @factory, user: administrator))
        end

        @request.host = "fabrica.test.host"
      end

      it 'returns ok status' do
        xhr :get, :current

        expect(response).to have_http_status(:ok)
      end

      it 'returns the workplace third party data' do
        xhr :get, :current

        expect(json['workplace']['id']).to eq(@factory.acting_as.id)
        expect(json['workplace']['logo_url']).to eq(@factory.acting_as.logo.url)
      end
    end

    context 'when there is no workplace for the subdomain' do
      before do
        @factory = FactoryGirl.create(:factory, name: 'F1', subdomain: 'fabrica', logo: fixture_file_upload('/files/avatar.jpg', 'image/jpg'))
        FactoryGirl.create(:administrator, username: 'F1').tap do |administrator|
          administrator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @factory, user: administrator))
        end

        @request.host = "fabrica-f2.test.host"
      end

      it 'returns not found status' do
        xhr :get, :current

        expect(response).to have_http_status(:not_found)
      end
    end
  end
end
