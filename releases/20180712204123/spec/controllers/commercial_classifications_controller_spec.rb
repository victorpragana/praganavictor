# == Schema Information
#
# Table name: commercial_classifications
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe CommercialClassificationsController, type: :controller do
  before do
    sign_in :administrator, create(:administrator)

    family = create(:family)

    @line1 = create(:line, family: family, name: 'L1')
    @line2 = create(:line, family: family, name: 'L2')
  end

  describe 'GET index' do
    render_views

    before do
      @commercial_classification1 = create(:commercial_classification, name: 'C1')
      @commercial_classification2 = create(:commercial_classification, name: 'C2')
      @commercial_classification3 = create(:commercial_classification, name: 'C3')
      @commercial_classification4 = create(:commercial_classification, name: 'C4')
    end

    it 'returns OK' do
      xhr :get, :index

      expect(response).to have_http_status(:ok)
    end
  end

  describe 'POST create' do
    render_views

    context 'with invalid params' do
      let(:parameters) { { 'name' => '' } }

      it 'does not create a new commercial classification' do
        expect {
          xhr :post, :create, parameters
        }.to change(CommercialClassification, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :create, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) { {'name' => 'Table', 'line_ids' => [@line1.id.to_s, @line2.id.to_s]} }

      it 'initializes the commercial classification service' do
        expect(CommercialClassificationService).to receive(:new).with(parameters).and_call_original

        xhr :post, :create, parameters
      end

      it 'creates the commercial classification' do
        expect_any_instance_of(CommercialClassificationService).to receive(:create).and_call_original
        
        xhr :post, :create, parameters
      end

      it 'returns created status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:created)
      end

      it 'returns nothing' do
        xhr :post, :create, parameters

        expect(response.body).to be_empty
      end
    end
  end

  describe 'PUT update' do
    render_views

    before do
      @commercial_classification = create(:commercial_classification)
    end

    context 'with invalid params' do
      let(:parameters) { { 'name' => '' }.merge('id' => "#{@commercial_classification.id}") }

      it 'does not create a new commercial classification' do
        expect {
          xhr :put, :update, parameters
        }.to change(CommercialClassification, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :put, :update, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :put, :update, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) { {'name' => 'Table', 'line_ids' => [@line1.id.to_s, @line2.id.to_s]} }

      it 'initializes the commercial classification service' do
        expect(CommercialClassificationService).to receive(:new).with(parameters).and_call_original

        xhr :put, :update, parameters.merge({'id' => "#{@commercial_classification.id}"})
      end

      it 'updates the commercial classification' do
        expect_any_instance_of(CommercialClassificationService).to receive(:update).with(@commercial_classification.id.to_s).and_call_original
        
        xhr :put, :update, parameters.merge({'id' => "#{@commercial_classification.id}"})
      end

      it 'returns ok status' do
        xhr :put, :update, parameters.merge({'id' => "#{@commercial_classification.id}"})
        
        expect(response).to have_http_status(:ok)
      end

      it 'returns nothing' do
        xhr :put, :update, parameters.merge({'id' => "#{@commercial_classification.id}"})

        expect(response.body).to be_empty
      end
    end
  end

  describe 'GET show' do
    render_views

    before do
      @commercial_classification = create(:commercial_classification, lines: [@line1, @line2])
    end

    it 'should ok status' do
      xhr :get, :show, id: @commercial_classification.id

      expect(response).to have_http_status(:ok)
    end

    it 'returns the commercial classification data' do
      xhr :get, :show, id: @commercial_classification.id

      expect(json['commercial_classification']['id']).to eq(@commercial_classification.id)
      expect(json['commercial_classification']['name']).to eq(@commercial_classification.name)
      expect(json['commercial_classification']['line_ids']).to match_array(@commercial_classification.line_ids)
    end
  end

  describe 'DELETE destroy' do
    render_views

    before do
      @commercial_classification = create(:commercial_classification)
    end

    context 'with success' do
      it 'destroys the commercial classification' do
        expect {
          xhr :delete, :destroy, { id: @commercial_classification.id }
        }.to change(CommercialClassification, :count).by(-1)
      end

      it 'returns no content status' do
        xhr :delete, :destroy, { id: @commercial_classification.id }

        expect(response).to have_http_status(:no_content)
      end
    end

    context 'with error' do
      let(:commercial_classification) {mock_model(CommercialClassification, destroy: false, errors: double(:errors, full_messages: ['foo']))}

      before do
        allow(CommercialClassification).to receive(:find).and_return(commercial_classification)
      end

      it 'does not destroy the commercial classification' do
        expect {
          xhr :delete, :destroy, { id: commercial_classification.id }
        }.to_not change(CommercialClassification, :count)
      end

      it 'returns the bad request status' do
        xhr :delete, :destroy, { id: commercial_classification.id }

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the errors' do
        xhr :delete, :destroy, { id: commercial_classification.id }
        
        expect(json['errors']).to_not be_empty
      end
    end
  end
end
