# == Schema Information
#
# Table name: lines
#
#  id                   :integer          not null, primary key
#  name                 :string           not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  family_id            :integer
#  sale_price           :decimal(8, 3)    default(0.0), not null
#  horizontal_increment :integer          default(0), not null
#  vertical_increment   :integer          default(0), not null
#  erp_categories       :json
#  ncm                  :string(8)
#  cst                  :integer
#  active               :boolean          default(TRUE)
#  finished             :boolean          default(FALSE)
#

require 'rails_helper'

RSpec.describe LinesController, type: :controller do
  before do
    factory = create(:factory, name: 'F1')
    user = create(:administrator).tap do |administrator|
      administrator.update!(selected_workplace: create(:user_workplace, workplace: factory, user: administrator))
    end

    sign_in :administrator, user
    
    @category1 = create(:category, name: 'C1')
    @category2 = create(:category, name: 'C2')
    @category3 = create(:category, name: 'C3')
  end

  describe 'GET index' do
    render_views

    before do
      @family = create(:family, name: 'F1')
      @family2 = create(:family, name: 'F2')

      @line1 = create(:line, family: @family, name: 'L1', horizontal_increment: 100, vertical_increment: 150, active: true)

      @line1.categories << build(:line_category, category: @category1)
      @line1.categories << build(:line_category, category: @category2)

      @line1.dimensions << build(:dimension, height: 1000..5000, width: 1000..2000, weight: :general)
      @line1.dimensions << build(:dimension, height: 7000..9000, width: 3000..4000, weight: :light)

      @line1.save!

      @line2 = create(:line, family: @family, name: 'L2', horizontal_increment: 200, vertical_increment: 250, active: false)

      @line2.dimensions << build(:dimension, height: 1000..5000, width: 2100..3000, weight: :heavy)

      @line2.save!

      @line3 = create(:line, family: @family2, name: 'L3', horizontal_increment: 280, vertical_increment: 350, active: true)

      @line4 = create(:line, family: @family, name: 'L4', horizontal_increment: 100, vertical_increment: 150, active: false)

      @line4.dimensions << build(:dimension, height: 1000..5000, width: 1000..2000, weight: :medium)
      @line4.dimensions << build(:dimension, height: 7000..9000, width: 3000..4000, weight: :general)

      @line4.save!

      @value1 = build(:value, value: 'V1', comparison_value: 100)
      @value2 = build(:value, value: 'V2', comparison_value: 150)
      @value3 = build(:value, value: 'V3', comparison_value: 200)

      @specification = build(:specification, automatic: true)
      @specification.values = [@value1, @value2, @value3]
      @specification.save!

      @component1 = create(:component, name: 'C1', unit: :cm, category: @category1, erasable: true,
        cost: BigDecimal.new('10'), values: [@value1])

      @line_rule = create(:line_rule, line: @line4, control_method: LineRule.control_methods[:width], 
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_',
        width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', automatic: true)
      create(:rule, line_rule: @line_rule, component: @component1, control_method: Rule.control_methods[:width], 
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_',
        width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', automatic: true)
    end

    it 'returns OK' do
      xhr :get, :index

      expect(response).to have_http_status(:ok)
    end

    context 'without parameters' do
      it 'lists the lines converting the dimensions to centimeters' do
        xhr :get, :index

        expect(json['lines'].size).to eq(4)

        expect(json['lines'][0]['id']).to eq(@line4.id)
        expect(json['lines'][0]['name']).to eq(@line4.name)
        expect(json['lines'][0]['sale_price']).to eq(@line4.sale_price.to_f)
        expect(json['lines'][0]['horizontal_increment']).to eq(@line4.horizontal_increment_from_mm_to_cm)
        expect(json['lines'][0]['vertical_increment']).to eq(@line4.vertical_increment_from_mm_to_cm)
        expect(json['lines'][0]['active']).to eq(@line4.active?)
        expect(json['lines'][0]['family']['name']).to eq(@family.name)
        expect(json['lines'][0]['family']['color']).to eq(@family.color)
        expect(json['lines'][0]['dimensions'][0]['initial_width']).to eq(@line4.dimensions[0].width_from_mm_to_cm.min)
        expect(json['lines'][0]['dimensions'][0]['final_width']).to eq(@line4.dimensions[0].width_from_mm_to_cm.max)
        expect(json['lines'][0]['dimensions'][0]['initial_height']).to eq(@line4.dimensions[0].height_from_mm_to_cm.min)
        expect(json['lines'][0]['dimensions'][0]['final_height']).to eq(@line4.dimensions[0].height_from_mm_to_cm.max)
        expect(json['lines'][0]['dimensions'][0]['weight']).to eq(Dimension.weights[@line4.dimensions[0].weight])

        expect(json['lines'][1]['id']).to eq(@line3.id)
        expect(json['lines'][1]['name']).to eq(@line3.name)
        expect(json['lines'][1]['sale_price']).to eq(@line3.sale_price.to_f)
        expect(json['lines'][1]['horizontal_increment']).to eq(@line3.horizontal_increment_from_mm_to_cm)
        expect(json['lines'][1]['vertical_increment']).to eq(@line3.vertical_increment_from_mm_to_cm)
        expect(json['lines'][1]['active']).to eq(@line3.active?)
        expect(json['lines'][1]['family']['name']).to eq(@family2.name)
        expect(json['lines'][1]['family']['color']).to eq(@family2.color)

        expect(json['lines'][2]['id']).to eq(@line2.id)
        expect(json['lines'][2]['name']).to eq(@line2.name)
        expect(json['lines'][2]['sale_price']).to eq(@line2.sale_price.to_f)
        expect(json['lines'][2]['horizontal_increment']).to eq(@line2.horizontal_increment_from_mm_to_cm)
        expect(json['lines'][2]['vertical_increment']).to eq(@line2.vertical_increment_from_mm_to_cm)
        expect(json['lines'][2]['active']).to eq(@line2.active?)
        expect(json['lines'][2]['family']['name']).to eq(@family.name)
        expect(json['lines'][2]['family']['color']).to eq(@family.color)

        expect(json['lines'][3]['id']).to eq(@line1.id)
        expect(json['lines'][3]['name']).to eq(@line1.name)
        expect(json['lines'][3]['sale_price']).to eq(@line1.sale_price.to_f)
        expect(json['lines'][3]['horizontal_increment']).to eq(@line1.horizontal_increment_from_mm_to_cm)
        expect(json['lines'][3]['vertical_increment']).to eq(@line1.vertical_increment_from_mm_to_cm)
        expect(json['lines'][3]['active']).to eq(@line1.active?)
        expect(json['lines'][3]['family']['name']).to eq(@family.name)
        expect(json['lines'][3]['family']['color']).to eq(@family.color)
        expect(json['lines'][3]['categories'][0]['category_id']).to eq(@category1.id)
        expect(json['lines'][3]['categories'][0]['category_name']).to eq(@category1.name)
        expect(json['lines'][3]['categories'][1]['category_id']).to eq(@category2.id)
        expect(json['lines'][3]['categories'][1]['category_name']).to eq(@category2.name)
        expect(json['lines'][3]['dimensions'][0]['initial_width']).to eq(@line1.dimensions[0].width_from_mm_to_cm.min)
        expect(json['lines'][3]['dimensions'][0]['final_width']).to eq(@line1.dimensions[0].width_from_mm_to_cm.max)
        expect(json['lines'][3]['dimensions'][0]['initial_height']).to eq(@line1.dimensions[0].height_from_mm_to_cm.min)
        expect(json['lines'][3]['dimensions'][0]['final_height']).to eq(@line1.dimensions[0].height_from_mm_to_cm.max)
        expect(json['lines'][3]['dimensions'][0]['weight']).to eq(Dimension.weights[@line1.dimensions[0].weight])

        expect(json['lines'][3]['dimensions'][1]['initial_width']).to eq(@line1.dimensions[1].width_from_mm_to_cm.min)
        expect(json['lines'][3]['dimensions'][1]['final_width']).to eq(@line1.dimensions[1].width_from_mm_to_cm.max)
        expect(json['lines'][3]['dimensions'][1]['initial_height']).to eq(@line1.dimensions[1].height_from_mm_to_cm.min)
        expect(json['lines'][3]['dimensions'][1]['final_height']).to eq(@line1.dimensions[1].height_from_mm_to_cm.max)
        expect(json['lines'][3]['dimensions'][1]['weight']).to eq(Dimension.weights[@line1.dimensions[1].weight])
      end
    end

    context 'with dimension parameters' do
      it 'lists the lines that meet the criteria and are active' do
        xhr :get, :index, { 'width' => '200', 'height' => '250' }

        expect(json['lines'].size).to eq(1)

        expect(json['lines'][0]['id']).to eq(@line1.id)
        expect(json['lines'][0]['name']).to eq(@line1.name)
        expect(json['lines'][0]['horizontal_increment']).to eq(@line1.horizontal_increment_from_mm_to_cm)
        expect(json['lines'][0]['vertical_increment']).to eq(@line1.vertical_increment_from_mm_to_cm)
        expect(json['lines'][0]['active']).to eq(@line1.active?)
        expect(json['lines'][0]['family']['name']).to eq(@family.name)
        expect(json['lines'][0]['family']['color']).to eq(@family.color)
        expect(json['lines'][0]['categories'][0]['category_id']).to eq(@category1.id)
        expect(json['lines'][0]['categories'][0]['category_name']).to eq(@category1.name)
        expect(json['lines'][0]['categories'][1]['category_id']).to eq(@category2.id)
        expect(json['lines'][0]['categories'][1]['category_name']).to eq(@category2.name)

        expect(json['lines'][0]['dimensions'][0]['initial_width']).to eq(@line1.dimensions[0].width_from_mm_to_cm.min)
        expect(json['lines'][0]['dimensions'][0]['final_width']).to eq(@line1.dimensions[0].width_from_mm_to_cm.max)
        expect(json['lines'][0]['dimensions'][0]['initial_height']).to eq(@line1.dimensions[0].height_from_mm_to_cm.min)
        expect(json['lines'][0]['dimensions'][0]['final_height']).to eq(@line1.dimensions[0].height_from_mm_to_cm.max)
        expect(json['lines'][0]['dimensions'][0]['weight']).to eq(Dimension.weights[@line1.dimensions[0].weight])

        expect(json['lines'][0]['dimensions'][1]['initial_width']).to eq(@line1.dimensions[1].width_from_mm_to_cm.min)
        expect(json['lines'][0]['dimensions'][1]['final_width']).to eq(@line1.dimensions[1].width_from_mm_to_cm.max)
        expect(json['lines'][0]['dimensions'][1]['initial_height']).to eq(@line1.dimensions[1].height_from_mm_to_cm.min)
        expect(json['lines'][0]['dimensions'][1]['final_height']).to eq(@line1.dimensions[1].height_from_mm_to_cm.max)
        expect(json['lines'][0]['dimensions'][1]['weight']).to eq(Dimension.weights[@line1.dimensions[1].weight])
      end
    end
  end

  describe 'POST create' do
    render_views

    context 'with invalid params' do
      let(:parameters) { {'name' => ''} }

      it 'does not create a new Line' do
        expect {
          xhr :post, :create, parameters
        }.to change(Line, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :create, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      before do
        @family = create(:family)
      end

      let(:parameters) { {'family_id' => "#{@family.id}", 'name' => 'Rolô', 'sale_price' => '5.54', 'horizontal_increment' => '1', 'vertical_increment' => '1', 'dimensions' => [
          {'initial_width' => '10', 'final_width' => '30', 'initial_height' => '10', 'final_height' => '30', 'weight' => '0'},
          {'initial_width' => '40', 'final_width' => '50', 'initial_height' => '40', 'final_height' => '60', 'weight' => '1'}
        ], 'categories' => [
          {'category' => "#{@category1.id}", 'required' => true},
          {'category' => "#{@category2.id}", 'required' => true},
          {'category' => "#{@category3.id}", 'required' => false}
        ]
      } }

      let(:parsed_parameters) { {'family_id' => "#{@family.id}", 'name' => 'Rolô', 'sale_price' => '5.54', 'horizontal_increment' => 10, 'vertical_increment' => 10, 'dimensions' => [
          {'initial_width' => 100, 'final_width' => 300, 'initial_height' => 100, 'final_height' => 300, 'weight' => '0'},
          {'initial_width' => 400, 'final_width' => 500, 'initial_height' => 400, 'final_height' => 600, 'weight' => '1'}
        ], 'categories' => [
          {'category' => "#{@category1.id}", 'required' => true},
          {'category' => "#{@category2.id}", 'required' => true},
          {'category' => "#{@category3.id}", 'required' => false}
        ]
      } }

      it 'initializes the line service' do
        expect(LineService).to receive(:new).with(parsed_parameters).and_call_original

        xhr :post, :create, parameters
      end

      it 'creates the line' do
        expect_any_instance_of(LineService).to receive(:create).and_call_original
        
        xhr :post, :create, parameters
      end

      it 'returns created status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:created)
      end

      it 'renders nothing' do
        xhr :post, :create, parameters

        expect(response.body).to be_blank
      end
    end
  end

  describe 'PUT update' do
    render_views

    before do
      @family = create(:family)
      @line = create(:line, family: @family)

      @line.dimensions << build(:dimension, height: 10..50, width: 10..20)
      @line.dimensions << build(:dimension, height: 70..90, width: 30..40)
      @line.dimensions << build(:dimension, height: 60..65, width: 10..40)

      @line.save!
    end

    context 'with invalid params' do
      let(:parameters) { {'name' => ""}.merge({'id' => "#{@line.id}"}) }

      it 'does not create a new Line' do
        expect {
          xhr :put, :update, parameters
        }.to change(Line, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :put, :update, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :put, :update, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) { {'family_id' => "#{@family.id}", 'name' => 'Rolô', 'sale_price' => '5.54', 'horizontal_increment' => '1', 'vertical_increment' => '1', 'dimensions' => [
          {'initial_width' => '10', 'final_width' => '30', 'initial_height' => '10', 'final_height' => '30', 'weight' => '0'},
          {'initial_width' => '40', 'final_width' => '50', 'initial_height' => '40', 'final_height' => '60', 'weight' => '2'}
        ], 'categories' => [
          {'category' => "#{@category1.id}", 'required' => true},
          {'category' => "#{@category2.id}", 'required' => true},
          {'category' => "#{@category3.id}", 'required' => false}
        ], 'erp_categories' => {"LINHA DE PRODUTO"=>"PRODUTO ACABADO", "GRUPO"=>"ADAPTADOR", "SUBGRUPO"=>"ROMANA", "TRIBUTAÇÃO"=>"TRIBUTADO ST"},
        'ncm' => '25697805', 'cst' => '5'
      } }

      let(:parsed_parameters) { {'family_id' => "#{@family.id}", 'name' => 'Rolô', 'sale_price' => '5.54', 'horizontal_increment' => 10, 'vertical_increment' => 10, 'dimensions' => [
          {'initial_width' => 100, 'final_width' => 300, 'initial_height' => 100, 'final_height' => 300, 'weight' => '0'},
          {'initial_width' => 400, 'final_width' => 500, 'initial_height' => 400, 'final_height' => 600, 'weight' => '2'}
        ], 'categories' => [
          {'category' => "#{@category1.id}", 'required' => true},
          {'category' => "#{@category2.id}", 'required' => true},
          {'category' => "#{@category3.id}", 'required' => false}
        ], 'erp_categories' => {"LINHA DE PRODUTO"=>"PRODUTO ACABADO", "GRUPO"=>"ADAPTADOR", "SUBGRUPO"=>"ROMANA", "TRIBUTAÇÃO"=>"TRIBUTADO ST"},
        'ncm' => '25697805', 'cst' => '5'
      } }

      it 'initializes the line service' do
        expect(LineService).to receive(:new).with(parsed_parameters).and_call_original

        xhr :put, :update, parameters.merge({'id' => "#{@line.id}"})
      end

      it 'updates the line' do
        expect_any_instance_of(LineService).to receive(:update).with(@line.id.to_s).and_call_original
        
        xhr :put, :update, parameters.merge({'id' => "#{@line.id}"})
      end

      it 'returns ok status' do
        xhr :put, :update, parameters.merge({'id' => "#{@line.id}"})
        
        expect(response).to have_http_status(:ok)
      end

      it 'renders nothing' do
        xhr :put, :update, parameters.merge({'id' => "#{@line.id}"})

        expect(response.body).to be_blank
      end
    end
  end

  describe 'GET show' do
    render_views

    before do
      @family = create(:family)

      @question1 = create(:question, type: :boolean)
      @question2 = create(:question, type: :dropdown, options: [{ 'label' => 'Bar', 'value' => 'bar' }, { 'label' => 'Foo', 'value' => 'foo' }])

      @line = create(:line, family: @family)

      @line.dimensions << build(:dimension, height: 10..50, width: 10..20, weight: :light)
      @line.dimensions << build(:dimension, height: 70..90, width: 30..40, weight: :heavy)
      @line.dimensions << build(:dimension, height: 60..65, width: 10..40, weight: :general)

      @line.categories << build(:line_category, category: @category1, required: true)
      @line.categories << build(:line_category, category: @category2, required: false)

      @line.erp_categories = {"LINHA DE PRODUTO"=>"PRODUTO ACABADO", "GRUPO"=>"ADAPTADOR", "SUBGRUPO"=>"ROMANA", "TRIBUTAÇÃO"=>"TRIBUTADO ST"}
      @line.ncm = '58974526'
      @line.cst = 7
      @line.question_ids = [@question1.id, @question2.id]

      @line.save!
    end

    it 'returns ok status' do
      xhr :get, :show, id: @line.id

      expect(response).to have_http_status(:ok)
    end

    it 'returns the line data' do
      xhr :get, :show, id: @line.id

      expect(json['line']['id']).to eq(@line.id)
      expect(json['line']['name']).to eq(@line.name)
      expect(json['line']['family_id']).to eq(@line.family_id)
      expect(json['line']['horizontal_increment']).to eq(@line.horizontal_increment_from_mm_to_cm)
      expect(json['line']['vertical_increment']).to eq(@line.vertical_increment_from_mm_to_cm)
      expect(json['line']['active']).to eq(@line.active?)

      expect(json['line']['dimensions'][0]['id']).to eq(@line.dimensions[0].id)
      expect(json['line']['dimensions'][0]['initial_width']).to eq(@line.dimensions[0].width_from_mm_to_cm.min)
      expect(json['line']['dimensions'][0]['final_width']).to eq(@line.dimensions[0].width_from_mm_to_cm.max)
      expect(json['line']['dimensions'][0]['initial_height']).to eq(@line.dimensions[0].height_from_mm_to_cm.min)
      expect(json['line']['dimensions'][0]['final_height']).to eq(@line.dimensions[0].height_from_mm_to_cm.max)
      expect(json['line']['dimensions'][0]['weight']).to eq(Dimension.weights[@line.dimensions[0].weight])

      expect(json['line']['dimensions'][1]['id']).to eq(@line.dimensions[1].id)
      expect(json['line']['dimensions'][1]['initial_width']).to eq(@line.dimensions[1].width_from_mm_to_cm.min)
      expect(json['line']['dimensions'][1]['final_width']).to eq(@line.dimensions[1].width_from_mm_to_cm.max)
      expect(json['line']['dimensions'][1]['initial_height']).to eq(@line.dimensions[1].height_from_mm_to_cm.min)
      expect(json['line']['dimensions'][1]['final_height']).to eq(@line.dimensions[1].height_from_mm_to_cm.max)
      expect(json['line']['dimensions'][1]['weight']).to eq(Dimension.weights[@line.dimensions[1].weight])

      expect(json['line']['dimensions'][2]['id']).to eq(@line.dimensions[2].id)
      expect(json['line']['dimensions'][2]['initial_width']).to eq(@line.dimensions[2].width_from_mm_to_cm.min)
      expect(json['line']['dimensions'][2]['final_width']).to eq(@line.dimensions[2].width_from_mm_to_cm.max)
      expect(json['line']['dimensions'][2]['initial_height']).to eq(@line.dimensions[2].height_from_mm_to_cm.min)
      expect(json['line']['dimensions'][2]['final_height']).to eq(@line.dimensions[2].height_from_mm_to_cm.max)
      expect(json['line']['dimensions'][2]['weight']).to eq(Dimension.weights[@line.dimensions[2].weight])

      expect(json['line']['categories'][0]['category_id']).to eq(@category1.id)
      expect(json['line']['categories'][0]['category_name']).to eq(@category1.name)
      expect(json['line']['categories'][0]['required']).to eq(true)
      expect(json['line']['categories'][1]['category_id']).to eq(@category2.id)
      expect(json['line']['categories'][1]['category_name']).to eq(@category2.name)
      expect(json['line']['categories'][1]['required']).to eq(false)

      expect(json['line']['erp_categories']['LINHA DE PRODUTO']).to eq('PRODUTO ACABADO')
      expect(json['line']['erp_categories']['GRUPO']).to eq('ADAPTADOR')
      expect(json['line']['erp_categories']['SUBGRUPO']).to eq('ROMANA')
      expect(json['line']['erp_categories']['TRIBUTAÇÃO']).to eq('TRIBUTADO ST')

      expect(json['line']['ncm']).to eq('58974526')
      expect(json['line']['cst']).to eq(7)

      expect(json['line']['questions'][0]['id']).to eq(@question1.id)
      expect(json['line']['questions'][0]['type']).to eq(Question.types[@question1.type])
      expect(json['line']['questions'][0]['label']).to eq(@question1.label)
      expect(json['line']['questions'][0]['value']).to eq(@question1.value)
      expect(json['line']['questions'][0]['variable']).to eq(@question1.as_variable)

      expect(json['line']['questions'][1]['id']).to eq(@question2.id)
      expect(json['line']['questions'][1]['type']).to eq(Question.types[@question2.type])
      expect(json['line']['questions'][1]['label']).to eq(@question2.label)
      expect(json['line']['questions'][1]['value']).to eq(@question2.value)
      expect(json['line']['questions'][1]['variable']).to eq(@question2.as_variable)
      expect(json['line']['questions'][1]['options']).to match_array(@question2.options)
    end
  end

  describe 'GET rules' do
    render_views

    before do
      @family = create(:family)
      @line = create(:line, family: @family)

      @line.dimensions << build(:dimension, height: 10..50, width: 10..20, weight: :light)
      @line.dimensions << build(:dimension, height: 70..90, width: 30..40, weight: :heavy)
      @line.dimensions << build(:dimension, height: 60..65, width: 10..40, weight: :general)

      @line.categories << build(:line_category, category: @category1, required: true)
      @line.categories << build(:line_category, category: @category2, required: false)

      @component1 = create(:component, unit: :cm, category: @category1, families: [@family], name: 'C1')
      @component2 = create(:component, unit: :cm, category: @category1, families: [@family], name: 'C2')
      @component3 = create(:component, unit: :cm, category: @category1, families: [@family], name: 'C3')
      @component4 = create(:component, unit: :cm, category: @category2, families: [@family], name: 'C4')
      @component5 = create(:component, unit: :cm, category: @category2, families: [@family], name: 'C5')

      line_rule = build(:line_rule, required: true, control_method: LineRule.control_methods[:width], width: 10..20, width_consumption: '1',
        height_consumption: '2', total_consumption: '2*_LARGURA_', cost: '3+3', quantity_consumption: '2+2', category_id: @category1.id,
        description: 'a')
      line_rule.rules << build(:rule, component: @component1, required: true, control_method: Rule.control_methods[:width], width: 10..20, width_consumption: '1', height_consumption: '2', total_consumption: '2*_LARGURA_', cost: '3+3', quantity_consumption: '2+2')
      @line.line_rules << line_rule

      line_rule = build(:line_rule, required: false, control_method: LineRule.control_methods[:height], height: 10..20, width_consumption: '',
        height_consumption: '', total_consumption: '3*_ALTURA_', cost: '3/_LARGURA_', quantity_consumption: '',
        description: 'b')
      line_rule.rules << build(:rule, component: @component2, required: false, control_method: Rule.control_methods[:height], height: 10..20, width_consumption: '', height_consumption: '', total_consumption: '3*_ALTURA_', cost: '3/_LARGURA_', quantity_consumption: '')
      @line.line_rules << line_rule

      line_rule = build(:line_rule, required: true, control_method: LineRule.control_methods[:unit], width_consumption: '_ALTURA_',
        height_consumption: '_LARGURA_', total_consumption: '1+_LARGURA_', cost: '2*_ALTURA_', quantity_consumption: '',
        description: 'c')
      line_rule.rules << build(:rule, component: @component3, required: true, control_method: Rule.control_methods[:unit], width_consumption: '_ALTURA_', height_consumption: '_LARGURA_', total_consumption: '1+_LARGURA_', cost: '2*_ALTURA_', quantity_consumption: '')
      @line.line_rules << line_rule

      line_rule = build(:line_rule, required: false, control_method: LineRule.control_methods[:area], width: 10..20, height: 10..30,
        width_consumption: '_AREA_', height_consumption: '', total_consumption: '3/_ALTURA_', cost: '2*_ALTURA_+1',
        quantity_consumption: '2+_ALTURA_', category_id: @category2.id, description: 'd')
      line_rule.rules << build(:rule, component: @component4, required: false, control_method: Rule.control_methods[:area], width: 10..20, height: 10..30, width_consumption: '_AREA_', height_consumption: '', total_consumption: '3/_ALTURA_', cost: '2*_ALTURA_+1', quantity_consumption: '2+_ALTURA_')
      @line.line_rules << line_rule

      line_rule = build(:line_rule, required: false, control_method: LineRule.control_methods[:total_area], area: 10..20,
        width_consumption: '_AREA_', height_consumption: '', total_consumption: '3/_ALTURA_', cost: '2*_ALTURA_+1',
        quantity_consumption: '2+_ALTURA_', category_id: @category2.id, description: 'e')
      line_rule.rules << build(:rule, component: @component5, required: false, control_method: Rule.control_methods[:total_area], area: 10..20, width_consumption: '_AREA_', height_consumption: '', total_consumption: '3/_ALTURA_', cost: '2*_ALTURA_+1', quantity_consumption: '2+_ALTURA_')
      @line.line_rules << line_rule

      @line.save!
    end

    it 'returns ok status' do
      xhr :get, :rules, id: @line.id

      expect(response).to have_http_status(:ok)
    end

    it 'returns the line rules data' do
      xhr :get, :rules, id: @line.id

      line_rules = @line.line_rules.order(:weight, :description, :control_method, :width, :height)

      expect(json['rules'][0]['id']).to eq(line_rules[0].id)
      expect(json['rules'][0]['component_ids']).to match_array([@component1.id])
      expect(json['rules'][0]['control_method']['id']).to eq(LineRule.control_methods[line_rules[0].control_method])
      expect(json['rules'][0]['initial_width']).to eq(line_rules[0].width_from_mm_to_cm.min)
      expect(json['rules'][0]['final_width']).to eq(line_rules[0].width_from_mm_to_cm.max)
      expect(json['rules'][0]['width_consumption']).to eq('1')
      expect(json['rules'][0]['height_consumption']).to eq('2')
      expect(json['rules'][0]['total_consumption']).to eq('2*_LARGURA_')
      expect(json['rules'][0]['cost']).to eq('3+3')
      expect(json['rules'][0]['required']).to eq(true)
      expect(json['rules'][0]['automatic']).to eq(false)
      expect(json['rules'][0]['quantity_consumption']).to eq(line_rules[0].quantity_consumption)
      expect(json['rules'][0]['description']).to eq(line_rules[0].description)
      expect(json['rules'][0]['category_id']).to eq(line_rules[0].category_id)
      expect(json['rules'][0]['weight']).to eq(LineRule.weights[line_rules[0].weight])
      
      expect(json['rules'][1]['id']).to eq(line_rules[1].id)
      expect(json['rules'][1]['component_ids']).to match_array([@component2.id])
      expect(json['rules'][1]['control_method']['id']).to eq(LineRule.control_methods[line_rules[1].control_method])
      expect(json['rules'][1]['initial_height']).to eq(line_rules[1].height_from_mm_to_cm.min)
      expect(json['rules'][1]['final_height']).to eq(line_rules[1].height_from_mm_to_cm.max)
      expect(json['rules'][1]['width_consumption']).to eq('')
      expect(json['rules'][1]['height_consumption']).to eq('')
      expect(json['rules'][1]['total_consumption']).to eq('3*_ALTURA_')
      expect(json['rules'][1]['cost']).to eq('3/_LARGURA_')
      expect(json['rules'][1]['required']).to eq(false)
      expect(json['rules'][1]['automatic']).to eq(false)
      expect(json['rules'][1]['quantity_consumption']).to be_empty
      expect(json['rules'][1]['description']).to eq(line_rules[1].description)
      expect(json['rules'][1]['category_id']).to eq(line_rules[1].category_id)
      expect(json['rules'][1]['weight']).to eq(LineRule.weights[line_rules[1].weight])

      expect(json['rules'][2]['id']).to eq(line_rules[2].id)
      expect(json['rules'][2]['component_ids']).to match_array([@component3.id])
      expect(json['rules'][2]['control_method']['id']).to eq(LineRule.control_methods[line_rules[2].control_method])
      expect(json['rules'][2]['width_consumption']).to eq('_ALTURA_')
      expect(json['rules'][2]['height_consumption']).to eq('_LARGURA_')
      expect(json['rules'][2]['total_consumption']).to eq('1+_LARGURA_')
      expect(json['rules'][2]['cost']).to eq('2*_ALTURA_')
      expect(json['rules'][2]['required']).to eq(true)
      expect(json['rules'][2]['automatic']).to eq(false)
      expect(json['rules'][2]['quantity_consumption']).to be_empty
      expect(json['rules'][2]['description']).to eq(line_rules[2].description)
      expect(json['rules'][2]['category_id']).to eq(line_rules[2].category_id)
      expect(json['rules'][2]['weight']).to eq(LineRule.weights[line_rules[2].weight])

      expect(json['rules'][3]['id']).to eq(line_rules[3].id)
      expect(json['rules'][3]['component_ids']).to match_array([@component4.id])
      expect(json['rules'][3]['control_method']['id']).to eq(LineRule.control_methods[line_rules[3].control_method])
      expect(json['rules'][3]['initial_width']).to eq(line_rules[3].width_from_mm_to_cm.min)
      expect(json['rules'][3]['final_width']).to eq(line_rules[3].width_from_mm_to_cm.max)
      expect(json['rules'][3]['initial_height']).to eq(line_rules[3].height_from_mm_to_cm.min)
      expect(json['rules'][3]['final_height']).to eq(line_rules[3].height_from_mm_to_cm.max)
      expect(json['rules'][3]['width_consumption']).to eq('_AREA_')
      expect(json['rules'][3]['height_consumption']).to eq('')
      expect(json['rules'][3]['total_consumption']).to eq('3/_ALTURA_')
      expect(json['rules'][3]['cost']).to eq('2*_ALTURA_+1')
      expect(json['rules'][3]['required']).to eq(false)
      expect(json['rules'][3]['automatic']).to eq(false)
      expect(json['rules'][3]['quantity_consumption']).to eq(line_rules[3].quantity_consumption)
      expect(json['rules'][3]['description']).to eq(line_rules[3].description)
      expect(json['rules'][3]['category_id']).to eq(line_rules[3].category_id)
      expect(json['rules'][3]['weight']).to eq(LineRule.weights[line_rules[3].weight])

      expect(json['rules'][4]['id']).to eq(line_rules[4].id)
      expect(json['rules'][4]['component_ids']).to match_array([@component5.id])
      expect(json['rules'][4]['control_method']['id']).to eq(LineRule.control_methods[line_rules[4].control_method])
      expect(json['rules'][4]['initial_area']).to eq(line_rules[4].area_from_mm2_to_cm2.min)
      expect(json['rules'][4]['final_area']).to eq(line_rules[4].area_from_mm2_to_cm2.max)
      expect(json['rules'][4]['width_consumption']).to eq('_AREA_')
      expect(json['rules'][4]['height_consumption']).to eq('')
      expect(json['rules'][4]['total_consumption']).to eq('3/_ALTURA_')
      expect(json['rules'][4]['cost']).to eq('2*_ALTURA_+1')
      expect(json['rules'][4]['required']).to eq(false)
      expect(json['rules'][4]['automatic']).to eq(false)
      expect(json['rules'][4]['quantity_consumption']).to eq(line_rules[4].quantity_consumption)
      expect(json['rules'][4]['description']).to eq(line_rules[4].description)
      expect(json['rules'][4]['category_id']).to eq(line_rules[4].category_id)
      expect(json['rules'][4]['weight']).to eq(LineRule.weights[line_rules[4].weight])
    end
  end

  describe 'GET components' do
    render_views

    before do
      @family = create(:family)
      @line = create(:line, family: @family)

      @line.categories << build(:line_category, category: @category1)
      @line.categories << build(:line_category, category: @category3)

      @line.save!

      @component1 = create(:component, unit: :cm, category: @category1, families: [@family])
      @component2 = create(:component, unit: :cm, category: @category1, families: [@family])
      @component3 = create(:component, unit: :cm, category: @category2, families: [@family])
      @component4 = create(:component, unit: :cm, category: @category3, families: [@family])

      @value1 = build(:value, value: 'V1', comparison_value: 100)
      @value2 = build(:value, value: 'V2', comparison_value: 150)
      @value3 = build(:value, value: 'V3', comparison_value: 200)

      @specification = build(:specification, automatic: true)
      @specification.values = [@value1, @value2, @value3]
      @specification.save!
    end

    context 'without search parameters' do
      it 'initializes the component searcher' do
        expect(ComponentSearcher).to receive(:new).and_call_original

        xhr :get, :components, {id: @line.id}
      end
    end

    context 'with search parameters' do
      let(:params)         {{'width' => '1', 'height' => '2', 'specifications' => [@value1.id.to_s]}}
      let(:parsed_params)  {{'width' => 10, 'height' => 20, 'specifications' => [@value1.id.to_s]}}

      it 'initializes the component searcher with the params' do
        expect(ComponentSearcher).to receive(:new).with(parsed_params).and_call_original

        xhr :get, :components, {id: @line.id}.merge(params)
      end
    end

    it 'searches the components for the line' do
      expect_any_instance_of(ComponentSearcher).to receive(:search_for_line).with(@line.id.to_s).and_call_original

      xhr :get, :components, {id: @line.id}
    end

    it 'returns the components for the line categories' do
      xhr :get, :components, {id: @line.id}

      expect(json['components'][0]['id']).to eq(@component1.id)
      expect(json['components'][0]['name']).to eq(@component1.name)
      expect(json['components'][0]['automatic']).to eq(@component1.automatic?)
      expect(json['components'][0]['characteristics']).to eq(@component1.characteristics)
      expect(json['components'][0]['unit']).to eq(Component.units[@component1.unit])
      expect(json['components'][0]['category']['id']).to eq(@component1.category.id)
      expect(json['components'][0]['category']['name']).to eq(@component1.category.name)
      expect(json['components'][0]['families'][0]['name']).to eq(@family.name)
      expect(json['components'][0]['families'][0]['color']).to eq(@family.color)

      expect(json['components'][1]['id']).to eq(@component2.id)
      expect(json['components'][1]['name']).to eq(@component2.name)
      expect(json['components'][1]['automatic']).to eq(@component2.automatic?)
      expect(json['components'][1]['characteristics']).to eq(@component2.characteristics)
      expect(json['components'][1]['unit']).to eq(Component.units[@component2.unit])
      expect(json['components'][1]['category']['id']).to eq(@component2.category.id)
      expect(json['components'][1]['category']['name']).to eq(@component2.category.name)
      expect(json['components'][1]['families'][0]['name']).to eq(@family.name)
      expect(json['components'][1]['families'][0]['color']).to eq(@family.color)
      
      expect(json['components'][2]['id']).to eq(@component4.id)
      expect(json['components'][2]['name']).to eq(@component4.name)
      expect(json['components'][2]['automatic']).to eq(@component4.automatic?)
      expect(json['components'][2]['characteristics']).to eq(@component4.characteristics)
      expect(json['components'][2]['unit']).to eq(Component.units[@component4.unit])
      expect(json['components'][2]['category']['id']).to eq(@component4.category.id)
      expect(json['components'][2]['category']['name']).to eq(@component4.category.name)
      expect(json['components'][2]['families'][0]['name']).to eq(@family.name)
      expect(json['components'][2]['families'][0]['color']).to eq(@family.color)
    end

    it 'returns the ok status' do
      xhr :get, :components, {id: @line.id}

      expect(response).to have_http_status(:ok)
    end
  end

  describe 'POST set_rules' do
    render_views

    before do
      @family = create(:family)
      @line = create(:line, family: @family)

      @line.categories << build(:line_category, category: @category1, required: false)
      @line.categories << build(:line_category, category: @category3, required: false)

      @line.dimensions << build(:dimension, height: 10..50, width: 10..20)
      @line.dimensions << build(:dimension, height: 70..90, width: 30..40)
      @line.dimensions << build(:dimension, height: 60..65, width: 10..40)

      @line.save!

      @specification = build(:specification, automatic: true)
      @specification.values << build(:value, value: 'V4', comparison_value: 1)
      @specification.save!

      @component1 = create(:component, unit: :cm, category: @category1, name: 'AA')
      @component2 = create(:component, unit: :cm, category: @category1, name: 'BB')
      @component3 = create(:component, unit: :cm, category: @category2, name: 'CC')
      @component4 = create(:component, unit: :cm, category: @category3, name: 'DD')
      @component5 = create(:component, unit: :cm, category: @category2, :values => @specification.values, name: 'EE')
      @component6 = create(:component, unit: :cm, category: @category3, name: 'FF')
    end

    context 'with invalid data' do
      let(:parameters) { {'rules' => [
          {'initial_width'=>5, 'final_width'=>20, 'initial_height'=>0, 'final_height'=>0, 'component_ids'=>[@component1.id], 'control_method'=>{'id'=>LineRule.control_methods[:width]}, 'width_consumption'=>'', 'height_consumption'=>'', 'total_consumption'=>'', 'cost'=>'', 'quantity_consumption'=>''}, 
          {'initial_width'=>0, 'final_width'=>0, 'initial_height'=>5, 'final_height'=>10, 'component_ids'=>[@component2.id], 'control_method'=>{'id'=>''}, 'width_consumption'=>'', 'height_consumption'=>'', 'total_consumption'=>'', 'cost'=>'', 'quantity_consumption'=>''},
        ]}.merge({'id' => "#{@line.id}"})
      }

      it 'does not create new Rules' do
        expect {
          xhr :post, :set_rules, parameters
        }.to change(Rule, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :post, :set_rules, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :set_rules, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid data' do
      let(:parameters) {{'rules' => [
          {'initial_width'=>'5', 'final_width'=>'20', 'initial_height'=>'0', 'final_height'=>'0', 'component_ids'=>[@component1.id.to_s], 
            'control_method'=>{'id'=>LineRule.control_methods[:width].to_s}, 'width_consumption'=>'1', 'height_consumption'=>'3', 
            'total_consumption'=>'_ALTURA_+1', 'cost'=>'_ALTURA_*2', 'required' => true, 'automatic' => false, 
            'quantity_consumption'=>'_ALTURA_ * 2', 'description' => 'foo', 'category_id' => "#{@category1.id}", 'weight' => '1'},
          {'initial_width'=>'0', 'final_width'=>'0', 'initial_height'=>'5', 'final_height'=>'10', 'component_ids'=>[@component2.id.to_s], 
            'control_method'=>{'id'=>LineRule.control_methods[:height].to_s}, 'width_consumption'=>'', 'height_consumption'=>'', 
            'total_consumption'=>'_LARGURA_*2', 'cost'=>'_LARGURA_/3', 'required' => false, 'automatic' => false, 
            'quantity_consumption'=>'', 'description' => 'bar', 'weight' => '0'},
          {'initial_width'=>'0', 'final_width'=>'0', 'initial_height'=>'0', 'final_height'=>'0', 'component_ids'=>[@component3.id.to_s, @component4.id.to_s],
            'control_method'=>{'id'=>LineRule.control_methods[:unit].to_s}, 'width_consumption'=>'_LARGURA_ * 9', 'height_consumption'=>'_ALTURA_',
            'total_consumption'=>'_AREA_', 'cost'=>'_AREA_*2', 'automatic' => false, 'required' => true,
            'quantity_consumption'=>'_LARGURA_ * 2', 'weight' => '1'},
          {'component_ids'=>[@component5.id.to_s], 'control_method'=>{'id'=>LineRule.control_methods[:height].to_s}, 'width_consumption'=>'', 'height_consumption'=>'',
            'total_consumption'=>'_LARGURA_*2', 'cost'=>'_LARGURA_/3', 'required' => false, 'automatic' => true, 'quantity_consumption'=>'', 'weight' => '3'},
          {'initial_area'=>'5', 'final_area'=>'20', 'component_ids'=>[@component6.id.to_s], 
            'control_method'=>{'id'=>LineRule.control_methods[:total_area].to_s}, 'width_consumption'=>'1', 'height_consumption'=>'3', 
            'total_consumption'=>'_ALTURA_+1', 'cost'=>'_ALTURA_*2', 'required' => true, 'automatic' => false, 
            'quantity_consumption'=>'_ALTURA_ * 2', 'description' => 'foo', 'weight' => '1'}
        ]}.merge({'id' => "#{@line.id}"})
      }

      let(:parsed_parameters)  {{'rules' => [
          {
            'initial_width'=>50, 'final_width'=>200, 'initial_height'=>0, 'final_height'=>0, 'description' => 'foo',
            'width_consumption'=>'1', 'height_consumption'=>'3', 'total_consumption'=>'_ALTURA_+1', 'quantity_consumption'=>'_ALTURA_ * 2',
            'cost'=>'_ALTURA_*2', 'required' => true, 'automatic' => false, 'category_id' => "#{@category1.id}", 'weight' => '1',
            'component_ids'=>[@component1.id.to_s], 'control_method'=>{'id'=>LineRule.control_methods[:width].to_s}
          },
          {
            'initial_width'=>0, 'final_width'=>0, 'initial_height'=>50, 'final_height'=>100, 'description' => 'bar',
            'width_consumption'=>'', 'height_consumption'=>'', 'total_consumption'=>'_LARGURA_*2', 'quantity_consumption'=>'',
            'cost'=>'_LARGURA_/3', 'required' => false, 'automatic' => false, 'weight' => '0',
            'component_ids'=>[@component2.id.to_s], 'control_method'=>{'id'=>LineRule.control_methods[:height].to_s}, 
          },
          {
            'initial_width'=>0, 'final_width'=>0, 'initial_height'=>0, 'final_height'=>0,
            'width_consumption'=>'_LARGURA_ * 9', 'height_consumption'=>'_ALTURA_', 'total_consumption'=>'_AREA_', 'quantity_consumption'=>'_LARGURA_ * 2',
            'cost'=>'_AREA_*2', 'required' => true, 'automatic' => false, 'weight' => '1',
            'component_ids'=>[@component3.id.to_s, @component4.id.to_s], 'control_method'=>{'id'=>LineRule.control_methods[:unit].to_s}
          },
          {
            'width_consumption'=>'', 'height_consumption'=>'', 'total_consumption'=>'_LARGURA_*2', 'quantity_consumption'=>'',
            'cost'=>'_LARGURA_/3', 'required' => false, 'automatic' => true, 'weight' => '3',
            'component_ids'=>[@component5.id.to_s], 'control_method'=>{'id'=>LineRule.control_methods[:height].to_s}, 
          },
          {
            'initial_area'=>500, 'final_area'=>2000, 'description' => 'foo',
            'width_consumption'=>'1', 'height_consumption'=>'3', 'total_consumption'=>'_ALTURA_+1', 'quantity_consumption'=>'_ALTURA_ * 2',
            'cost'=>'_ALTURA_*2', 'required' => true, 'automatic' => false, 'weight' => '1',
            'component_ids'=>[@component6.id.to_s], 'control_method'=>{'id'=>LineRule.control_methods[:total_area].to_s},
          }
        ]}.merge({'id' => "#{@line.id}"})
      }

      it 'initializes the line rule service' do
        expect(LineRuleService).to receive(:new).with(parsed_parameters['rules']).and_call_original

        xhr :post, :set_rules, parameters
      end

      it 'sets the line rules' do
        expect_any_instance_of(LineRuleService).to receive(:set_line_rules_for).with(@line.id).and_call_original
        
        xhr :post, :set_rules, parameters
      end

      it 'returns ok status' do
        xhr :post, :set_rules, parameters
        
        expect(response).to have_http_status(:ok)
      end

      it 'renders nothing' do
        xhr :post, :set_rules, parameters

        expect(response.body).to be_blank
      end
    end
  end

  describe 'POST duplicate_rule' do
    render_views

    before do
      @family = create(:family)
      @line = create(:line, family: @family)

      @line.categories << build(:line_category, category: @category1, required: false)
      @line.categories << build(:line_category, category: @category3, required: false)

      @line.dimensions << build(:dimension, height: 10..50, width: 10..20)
      @line.dimensions << build(:dimension, height: 70..90, width: 30..40)
      @line.dimensions << build(:dimension, height: 60..65, width: 10..40)

      @line.save!

      @specification = build(:specification, automatic: true)
      @specification.values << build(:value, value: 'V4', comparison_value: 1)
      @specification.save!

      @component1 = create(:component, unit: :cm, category: @category1, name: 'AA')

      @line_rule_1 = create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: 5..20, height: nil,
        total_consumption: "_LARGURA_*2", width_consumption: "_ALTURA_*3", height_consumption: "_LARGURA_ + 3",
        cost: "_CUSTO_BASE_+2", quantity_consumption: "_ALTURA_ * 2", automatic: false, required: true)
      @rule_1 = create(:rule, line_rule: @line_rule_1, component: @component1, control_method: Rule.control_methods[:width], width: 5..60, height: nil,
        total_consumption: "_LARGURA_*2", width_consumption: "_ALTURA_*3", height_consumption: "_LARGURA_ + 3",
        cost: "_CUSTO_BASE_+2", quantity_consumption: "_ALTURA_ * 2", automatic: false, required: true)
    end

    context 'with invalid data' do
      let(:parameters) { { 'line_rule_id' => @line_rule_1.id }.merge({'id' => "#{@line.id}"}) }
      let(:service)    { double(:service, duplicate: false, success: false, errors: ['foo']) }

      before do
        allow(LineRuleService).to receive(:new).and_return(service)
      end

      it 'does not create new Line Rules' do
        expect {
          xhr :post, :duplicate_rule, parameters
        }.to change(LineRule, :count).by(0)
      end

      it 'does not create new Rules' do
        expect {
          xhr :post, :duplicate_rule, parameters
        }.to change(Rule, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :post, :duplicate_rule, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :duplicate_rule, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid data' do
      let(:parameters) { { 'line_rule_id' => @line_rule_1.id }.merge({'id' => "#{@line.id}"}) }

      it 'initializes the line rule service' do
        expect(LineRuleService).to receive(:new).with({'line_rule_id' => @line_rule_1.id.to_s}).and_call_original

        xhr :post, :duplicate_rule, parameters
      end

      it 'duplicates the line rules' do
        expect_any_instance_of(LineRuleService).to receive(:duplicate).with(@line.id.to_s).and_call_original
        
        xhr :post, :duplicate_rule, parameters
      end

      it 'returns ok status' do
        xhr :post, :duplicate_rule, parameters
        
        expect(response).to have_http_status(:ok)
      end

      it 'renders nothing' do
        xhr :post, :duplicate_rule, parameters

        expect(response.body).to be_blank
      end
    end
  end

  describe 'POST import_rules' do
    render_views

    before do
      @family = create(:family)
      @line = create(:line, family: @family)

      @line.categories << build(:line_category, category: @category1, required: false)
      @line.categories << build(:line_category, category: @category3, required: false)

      @line.dimensions << build(:dimension, height: 10..50, width: 10..20)
      @line.dimensions << build(:dimension, height: 70..90, width: 30..40)
      @line.dimensions << build(:dimension, height: 60..65, width: 10..40)

      @line.save!

      @specification = build(:specification, automatic: true)
      @specification.values << build(:value, value: 'V4', comparison_value: 1)
      @specification.save!

      @component1 = create(:component, unit: :cm, category: @category1, name: 'AA')

      @line_rule_1 = create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: 5..20, height: nil,
        total_consumption: "_LARGURA_*2", width_consumption: "_ALTURA_*3", height_consumption: "_LARGURA_ + 3",
        cost: "_CUSTO_BASE_+2", quantity_consumption: "_ALTURA_ * 2", automatic: false, required: true)
      @rule_1 = create(:rule, line_rule: @line_rule_1, component: @component1, control_method: Rule.control_methods[:width], width: 5..60, height: nil,
        total_consumption: "_LARGURA_*2", width_consumption: "_ALTURA_*3", height_consumption: "_LARGURA_ + 3",
        cost: "_CUSTO_BASE_+2", quantity_consumption: "_ALTURA_ * 2", automatic: false, required: true)
    end

    context 'with invalid data' do
      let(:parameters) { { 'line_rule_ids' => [@line_rule_1.id] }.merge({'id' => "#{@line.id}"}) }
      let(:service)    { double(:service, import: false, success: false, errors: ['foo']) }

      before do
        allow(LineRuleService).to receive(:new).and_return(service)
      end

      it 'does not create new Line Rules' do
        expect {
          xhr :post, :import_rules, parameters
        }.to change(LineRule, :count).by(0)
      end

      it 'does not create new Rules' do
        expect {
          xhr :post, :import_rules, parameters
        }.to change(Rule, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :post, :import_rules, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :import_rules, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid data' do
      let(:parameters) { { 'line_rule_ids' => [@line_rule_1.id] }.merge({'id' => "#{@line.id}"}) }

      it 'initializes the line rule service' do
        expect(LineRuleService).to receive(:new).with({'line_rule_ids' => [@line_rule_1.id.to_s]}).and_call_original

        xhr :post, :import_rules, parameters
      end

      it 'duplicates the line rules' do
        expect_any_instance_of(LineRuleService).to receive(:import).with(@line.id.to_s).and_call_original
        
        xhr :post, :import_rules, parameters
      end

      it 'returns ok status' do
        xhr :post, :import_rules, parameters
        
        expect(response).to have_http_status(:ok)
      end

      it 'renders nothing' do
        xhr :post, :import_rules, parameters

        expect(response.body).to be_blank
      end
    end
  end

  describe 'DELETE destroy' do
    before do
      @family = create(:family)
      @line = create(:line, family: @family, name: 'L1')
    end

    context 'with success' do
      it 'destroys the line' do
        expect {
          xhr :delete, :destroy, {id: @line.id}
        }.to change(Line, :count).by(-1)
      end

      it 'returns no content status' do
        xhr :delete, :destroy, {id: @line.id}

        expect(response).to have_http_status(:no_content)
      end
    end

    context 'with error' do
      let(:line) {mock_model(Line, destroy: false, errors: double(:errors, full_messages: ['foo']))}

      before do
        allow(Line).to receive(:find).and_return(line)
      end

      it 'does not destroy the line' do
        expect {
          xhr :delete, :destroy, {id: line.id}
        }.to_not change(Line, :count)
      end

      it 'returns the bad request status' do
        xhr :delete, :destroy, {id: line.id}

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the errors' do
        xhr :delete, :destroy, {id: line.id}
        
        expect(json['errors']).to_not be_empty
      end
    end
  end

  describe 'POST duplicate' do
    render_views

    before do
      @family = create(:family)
      @line = create(:line, family: @family)

      @line.dimensions << build(:dimension, height: 10..50, width: 10..20)
      @line.dimensions << build(:dimension, height: 70..90, width: 30..40)
      @line.dimensions << build(:dimension, height: 60..65, width: 10..40)

      @line.save!
    end

    context 'with invalid params' do
      let(:service) { double(:service, duplicate: false, success: false, errors: ['foo']) }

      before do
        allow(LineService).to receive(:new).and_return(service)
      end

      it 'does not create a new Line' do
        expect {
          xhr :post, :duplicate, {'id' => "#{@line.id}"}
        }.to change(Line, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :post, :duplicate, {'id' => "#{@line.id}"}
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :duplicate, {'id' => "#{@line.id}"}
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      it 'initializes the line service' do
        expect(LineService).to receive(:new).and_call_original

        xhr :post, :duplicate, {'id' => "#{@line.id}"}
      end

      it 'duplicates the line' do
        expect_any_instance_of(LineService).to receive(:duplicate).with(@line.id.to_s).and_call_original
        
        xhr :post, :duplicate, {'id' => "#{@line.id}"}
      end

      it 'returns ok status' do
        xhr :post, :duplicate, {'id' => "#{@line.id}"}
        
        expect(response).to have_http_status(:ok)
      end

      it 'renders the line id' do
        xhr :post, :duplicate, {'id' => "#{@line.id}"}

        expect(json['line']['id']).to eq(Line.last.id)
      end
    end
  end

  describe 'PUT activate' do
    render_views

    before do
      @family = create(:family)
      @line = create(:line, family: @family, active: false)

      @line.dimensions << build(:dimension, height: 10..50, width: 10..20)
      @line.dimensions << build(:dimension, height: 70..90, width: 30..40)
      @line.dimensions << build(:dimension, height: 60..65, width: 10..40)

      @line.save!
    end

    context 'with invalid params' do
      let(:service) { double(:service, activate: false, success: false, errors: ['foo']) }

      before do
        allow(LineService).to receive(:new).and_return(service)
      end

      it 'does not change the line status' do
        expect {
          xhr :put, :activate, {'id' => "#{@line.id}"}
        }.to_not change(@line, :active)
      end

      it 'returns unprocessable entity status' do
        xhr :put, :activate, {'id' => "#{@line.id}"}
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :put, :activate, {'id' => "#{@line.id}"}
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      it 'initializes the line service' do
        expect(LineService).to receive(:new).and_call_original

        xhr :put, :activate, {'id' => "#{@line.id}"}
      end

      it 'activates the line' do
        expect_any_instance_of(LineService).to receive(:activate).with(@line.id.to_s).and_call_original
        
        xhr :put, :activate, {'id' => "#{@line.id}"}
      end

      it 'returns ok status' do
        xhr :put, :activate, {'id' => "#{@line.id}"}
        
        expect(response).to have_http_status(:ok)
      end

      it 'renders nothing' do
        xhr :put, :activate, {'id' => "#{@line.id}"}

        expect(response.body).to be_blank
      end
    end
  end

  describe 'PUT inactivate' do
    render_views

    before do
      @family = create(:family)
      @line = create(:line, family: @family, active: false)

      @line.dimensions << build(:dimension, height: 10..50, width: 10..20)
      @line.dimensions << build(:dimension, height: 70..90, width: 30..40)
      @line.dimensions << build(:dimension, height: 60..65, width: 10..40)

      @line.save!
    end

    context 'with invalid params' do
      let(:service) { double(:service, inactivate: false, success: false, errors: ['foo']) }

      before do
        allow(LineService).to receive(:new).and_return(service)
      end

      it 'does not change the line status' do
        expect {
          xhr :put, :inactivate, {'id' => "#{@line.id}"}
        }.to_not change(@line, :active)
      end

      it 'returns unprocessable entity status' do
        xhr :put, :inactivate, {'id' => "#{@line.id}"}
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :put, :inactivate, {'id' => "#{@line.id}"}
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      it 'initializes the line service' do
        expect(LineService).to receive(:new).and_call_original

        xhr :put, :inactivate, {'id' => "#{@line.id}"}
      end

      it 'inactivates the line' do
        expect_any_instance_of(LineService).to receive(:inactivate).with(@line.id.to_s).and_call_original
        
        xhr :put, :inactivate, {'id' => "#{@line.id}"}
      end

      it 'returns ok status' do
        xhr :put, :inactivate, {'id' => "#{@line.id}"}
        
        expect(response).to have_http_status(:ok)
      end

      it 'renders nothing' do
        xhr :put, :inactivate, {'id' => "#{@line.id}"}

        expect(response.body).to be_blank
      end
    end
  end
end
