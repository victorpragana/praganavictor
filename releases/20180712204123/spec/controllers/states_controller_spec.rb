# == Schema Information
#
# Table name: states
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  acronym    :string           not null
#  ibge_code  :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe StatesController, type: :controller do
  before do
    @administrator = create(:administrator)

    (0..6).each do |k|
      @state = create(:state, name: "state_#{k}")
    end    

    (0..2).each do |i|
      create(:city, state: State.last, name: "city_#{i}")
    end

    sign_in :administrator, @administrator
  end

  describe 'GET index' do
    render_views

    it 'returns ok status' do
      xhr :get, :index

      expect(response).to have_http_status(:ok)

    end
    it 'lists the states' do
      xhr :get, :index

      expect(assigns(:states)).to match_array(State.all)
    end
  end
end
