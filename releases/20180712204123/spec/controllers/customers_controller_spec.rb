# == Schema Information
#
# Table name: customers
#
#  id         :integer          not null, primary key
#  deleted_at :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  store_id   :integer
#

require 'rails_helper'

RSpec.describe CustomersController, type: :controller do
  before do
    @factory = create(:factory, name: 'F1')

    create(:administrator, username: 'F1').tap do |administrator|
      administrator.update!(selected_workplace: create(:user_workplace, workplace: @factory, user: administrator))
    end

    @distributor_1 = create(:distributor, name: 'D1')
    @store         = create(:store, distributor: @distributor_1, name: 'C1')
  end

  describe 'third party access control' do
    before do
      create(:administrator, username: 'E1').tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: @distributor_1, user: administrator))
      end

      sign_in :administrator, @distributor_1.users.first
    end

    it 'renders unauthorized if the user is not a store or factory' do
      xhr :get, :index

      expect(response).to have_http_status(:unauthorized)
    end
  end
  
  describe 'GET index' do
    render_views

    before do
      @customer_1 = create(:customer, name: 'aa', store: @store)
      @customer_2 = create(:customer, name: 'bb', store: @store)
      @customer_3 = create(:customer, name: 'cc', store: @store)

      sign_in :administrator, @factory.users.first
    end

    it 'returns OK' do
      xhr :get, :index

      expect(response).to have_http_status(:ok)
    end

    context 'with datatable search parameters' do
      let(:params) { { 'draw'=>'1', 'columns'=>{'0'=>{'data'=>'0', 'name'=>'', 'searchable'=>'false', 'orderable'=>'false', 
        'search'=>{'value'=>'', 'regex'=>'false'}}, '1'=>{'data'=>'1', 'name'=>'id', 'searchable'=>'true', 'orderable'=>'true', 
        'search'=>{'value'=>'', 'regex'=>'false'}}, '2'=>{'data'=>'2', 'name'=>'', 'searchable'=>'true', 'orderable'=>'true', 
        'search'=>{'value'=>'', 'regex'=>'false'}}}, 'order'=>{'0'=>{'column'=>'1', 'dir'=>'asc'}}, 'start'=>'0', 'length'=>'100', 
        'search'=>{'value'=>'', 'regex'=>'false'}, '_'=>'1456860760040' } }

      it 'initializes the customer datatable' do
        expect(CustomerDatatable).to receive(:new)

        xhr :get, :index, params
      end
    end

    context 'without search parameters' do
      it 'lists the customers' do
        xhr :get, :index

        expect(json['customers'].size).to eq(3)
        expect(json['customers'][0]['id']).to eq(@customer_1.id)
        expect(json['customers'][0]['name']).to eq(@customer_1.name)
        expect(json['customers'][1]['id']).to eq(@customer_2.id)
        expect(json['customers'][1]['name']).to eq(@customer_2.name)
        expect(json['customers'][2]['id']).to eq(@customer_3.id)
        expect(json['customers'][2]['name']).to eq(@customer_3.name)
      end
    end
  end

  describe 'POST create' do
    render_views

    before do
      @state         = create(:state)
      @city          = create(:city, state: @state)
    end

    context 'with invalid params' do
      before do
        sign_in :administrator, @factory.users.first
      end

      let(:parameters) {{'name' => ''}}

      it 'does not create a new customer' do
        expect {
          xhr :post, :create, parameters
        }.to change(Customer, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :create, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      context 'with factory user' do
        before do
          sign_in :administrator, @factory.users.first
        end

        let(:parameters) { { 'name' => 'Customer 1', 'identification' => '111.623.597-81', 'zip_code' => '12244-888', 
          'street' => 'St. Foo', 'neighborhood' => 'foo', 'number' => 'SN', 'complement' => 'ABC', 'phone' => '(12)99222-8290',
          'email' => 'foo@bar.com', 'city_id' => "#{@city.id}", 'store_id' => "#{@store.id}", 'allow_special_discount' => true, 'special_discount' => '89.55'
        } }

        it 'initializes the customer service with the selected store' do
          expect(CustomerService).to receive(:new).with(parameters).and_call_original

          xhr :post, :create, parameters
        end

        it 'creates the customer' do
          expect_any_instance_of(CustomerService).to receive(:create).and_call_original
          
          xhr :post, :create, parameters
        end

        it 'returns created status' do
          xhr :post, :create, parameters
          
          expect(response).to have_http_status(:created)
        end
      end

      context 'with store user' do
        before do
          @external = create(:administrator).tap do |administrator|
            administrator.update!(selected_workplace: create(:user_workplace, workplace: @store, user: administrator))
          end

          sign_in :administrator, @external
        end

        let(:parameters) { {'name' => 'Customer 1', 'identification' => '111.623.597-81', 'zip_code' => '12244-888', 
          'street' => 'St. Foo', 'neighborhood' => 'foo', 'number' => 'SN', 'complement' => 'ABC', 'phone' => '(12)99222-8290',
          'email' => 'foo@bar.com', 'city_id' => "#{@city.id}", 'allow_special_discount' => true, 'special_discount' => '89.55'
        } }

        it 'initializes the customer service with the store' do
          expect(CustomerService).to receive(:new).with(parameters.merge({'store_id' => @store.id})).and_call_original

          xhr :post, :create, parameters
        end

        it 'creates the customer' do
          expect_any_instance_of(CustomerService).to receive(:create).and_call_original
          
          xhr :post, :create, parameters
        end

        it 'returns created status' do
          xhr :post, :create, parameters
          
          expect(response).to have_http_status(:created)
        end
      end
    end
  end

  describe 'GET show' do
    render_views

    before do
      @state       = create(:state)
      @city        = create(:city, state: @state)
      @customer    = create(:customer, name: 'bb', city_id: @city.id, store_id: @store.id, allow_special_discount: true, special_discount: 22)
      @user        = create(:administrator).tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: @customer, user: administrator))
      end

      sign_in :administrator, @factory.users.first
    end

    it 'should ok status' do
      xhr :get, :show, id: @customer.id

      expect(response).to have_http_status(:ok)
    end

    it 'returns the customer data' do
      xhr :get, :show, id: @customer.id

      expect(json['customer']['id']).to eq(@customer.id)
      expect(json['customer']['name']).to eq(@customer.name)
      expect(json['customer']['identification']).to eq(@customer.identification)
      expect(json["customer"]["rg"]).to eq(@customer.rg)
      expect(json["customer"]["issuing_entity"]).to eq(@customer.issuing_entity)
      expect(json['customer']['zip_code']).to eq(@customer.zip_code)
      expect(json['customer']['street']).to eq(@customer.street)
      expect(json['customer']['neighborhood']).to eq(@customer.neighborhood)
      expect(json['customer']['number']).to eq(@customer.number)
      expect(json['customer']['complement']).to eq(@customer.complement)
      expect(json['customer']['phone']).to eq(@customer.phone)
      expect(json['customer']['email']).to eq(@customer.email)
      expect(json['customer']['corporate_name']).to eq(@customer.corporate_name)
      expect(json['customer']['state_inscription']).to eq(@customer.state_inscription)
      expect(json['customer']['municipal_inscription']).to eq(@customer.municipal_inscription)
      expect(json['customer']['store_id']).to eq(@customer.store_id)
      expect(json['customer']['city']['id']).to eq(@customer.city.id)
      expect(json['customer']['city']['name']).to eq(@customer.city.name)
      expect(json['customer']['state']['id']).to eq(@customer.state.id)
      expect(json['customer']['state']['name']).to eq(@customer.state.name)
      expect(json['customer']['allow_special_discount']).to eq(@customer.allow_special_discount?)
      expect(json['customer']['special_discount']).to eq(@customer.special_discount)
    end
  end

  describe 'PUT update' do
    render_views

    before do
      @customer = create(:customer, name: 'bb', store: @store)
      @state = create(:state)
      @city  = create(:city, state: @state)
    end

    context 'with invalid params' do
      before do
        sign_in :administrator, @factory.users.first
      end

      let(:parameters) { { 'name' => '' }.merge({ 'id' => "#{@customer.id}" }) }

      it 'does not create a new customer' do
        expect {
          xhr :put, :update, parameters
        }.to change(Customer, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :put, :update, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :put, :update, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      context 'with factory user' do
        before do
          sign_in :administrator, @factory.users.first
        end

        let(:parameters) { { 'name' => 'Customer 1', 'identification' => '111.623.597-81', 'zip_code' => '12244-888', 
          'street' => 'St. Foo', 'neighborhood' => 'foo', 'number' => 'SN', 'complement' => 'ABC', 'phone' => '(12)99222-8290',
          'email' => 'foo@bar.com', 'city_id' => "#{@city.id}", 'store_id' => "#{@store.id}", 'allow_special_discount' => true, 'special_discount' => '89.55'
        } }

        it 'initializes the customer service with the selected store' do
          expect(CustomerService).to receive(:new).with(parameters).and_call_original

          xhr :put, :update, parameters.merge({ 'id' => "#{@customer.id}" })
        end

        it 'updates the customer' do
          expect_any_instance_of(CustomerService).to receive(:update).with(@customer.id.to_s).and_call_original
          
          xhr :put, :update, parameters.merge({ 'id' => "#{@customer.id}" })
        end

        it 'returns ok status' do
          xhr :put, :update, parameters.merge({ 'id' => "#{@customer.id}" })
          
          expect(response).to have_http_status(:ok)
        end
      end

      context 'with store user' do
        before do
          @external = create(:administrator).tap do |administrator|
            administrator.update!(selected_workplace: create(:user_workplace, workplace: @store, user: administrator))
          end

          sign_in :administrator, @external
        end

        let(:parameters) { { 'name' => 'Customer 1', 'identification' => '111.623.597-81', 'zip_code' => '12244-888', 
          'street' => 'St. Foo', 'neighborhood' => 'foo', 'number' => 'SN', 'complement' => 'ABC', 'phone' => '(12)99222-8290',
          'email' => 'foo@bar.com', 'city_id' => "#{@city.id}", 'allow_special_discount' => true, 'special_discount' => '89.55'
        } }

        it 'initializes the customer service with the store' do
          expect(CustomerService).to receive(:new).with(parameters.merge({'store_id' => @store.id})).and_call_original

          xhr :put, :update, parameters.merge({ 'id' => "#{@customer.id}" })
        end

        it 'updates the customer' do
          expect_any_instance_of(CustomerService).to receive(:update).with(@customer.id.to_s).and_call_original
          
          xhr :put, :update, parameters.merge({ 'id' => "#{@customer.id}" })
        end

        it 'returns ok status' do
          xhr :put, :update, parameters.merge({ 'id' => "#{@customer.id}" })
          
          expect(response).to have_http_status(:ok)
        end
      end
    end
  end

  describe 'DELETE destroy' do
    before do
      @customer = create(:customer, name: 'bb', store: @store)

      sign_in :administrator, @factory.users.first
    end

    context 'with success' do
      it 'destroys the customer' do
        expect {
          xhr :delete, :destroy, { id: @customer.id }
        }.to change(Customer, :count).by(-1)
      end

      it 'returns no content status' do
        xhr :delete, :destroy, { id: @customer.id }

        expect(response).to have_http_status(:no_content)
      end
    end

    context 'with error' do
      before do
        allow(@customer).to receive(:destroy).and_return(false)
        allow(@customer).to receive(:errors).and_return(double(:errors, full_messages: ['foo']))

        allow(Customer).to receive_message_chain(:joins, :all, :order, :find).and_return(@customer)
      end

      it 'does not destroy the customer' do
        expect {
          xhr :delete, :destroy, { id: @customer.id }
        }.to_not change(Customer, :count)
      end

      it 'returns the bad request status' do
        xhr :delete, :destroy, { id: @customer.id }

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the errors' do
        xhr :delete, :destroy, { id: @customer.id }
        
        expect(json['errors']).to_not be_empty
      end
    end
  end

  describe 'DELETE batch_destroy' do
    render_views

    before do
      @customer_1 = create(:customer, name: 'S1', store: @store)
      @customer_2 = create(:customer, name: 'S2', store: @store)

      sign_in :administrator, @factory.users.first
    end

    context 'with success' do
      it 'destroys the customers' do
        expect {
          xhr :delete, :batch_destroy, { ids: [@customer_1.id, @customer_2.id] }
        }.to change(Customer, :count).by(-2)
      end

      it 'returns no content status' do
        xhr :delete, :batch_destroy, { ids: [@customer_1.id, @customer_2.id] }

        expect(response).to have_http_status(:no_content)
      end
    end

    context 'with error' do
      before do
        allow(@customer_1).to receive(:destroy!).and_raise(StandardError)
        allow(Customer).to receive_message_chain(:joins, :all, :order, :find).and_return(@customer)
      end

      it 'does not destroy the customers' do
        expect {
          xhr :delete, :batch_destroy, { ids: [@customer_1.id] }
        }.to_not change(Customer, :count)
      end

      it 'returns the bad request status' do
        xhr :delete, :batch_destroy, { ids: [@customer_1.id] }

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the errors' do
        xhr :delete, :batch_destroy, { ids: [@customer_1.id] }
        
        expect(json['errors']).to_not be_empty
      end
    end
  end

  describe 'POST apply_price_table' do
    before do
      @price_table = create(:price_table, name: 'ab', owner: @factory)

      @store = create(:store, name: 'SA', distributor: @distributor_1)

      @customer_1 = create(:customer, name: 'CA', store_id: @store.id)
      @customer_2 = create(:customer, name: 'CB', store_id: @store.id)
      @customer_3 = create(:customer, name: 'CC', store_id: @store.id)

      @price_table.clients << PriceTableClient.new(client: @distributor)
      @price_table.clients << PriceTableClient.new(client: @store)

      sign_in :administrator, @factory.users.first
    end

    context 'with invalid params' do
      let(:parameters) {{ 'ids' => [''] }}

      it 'returns unprocessable entity status' do
        xhr :post, :apply_price_table, parameters.merge({'price_table_id' => @price_table.id})
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :apply_price_table, parameters.merge({'price_table_id' => @price_table.id})
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) {{ 'ids' => ["#{@customer_1.id}", "#{@customer_2.id}", "#{@customer_3.id}"] }}

      it 'initializes the Price Table Service' do
        expect(PriceTableService).to receive(:new).with({'owner' => @factory}).and_call_original

        xhr :post, :apply_price_table, parameters.merge({'price_table_id' => @price_table.id})
      end

      it 'should apply the price table' do
        expect_any_instance_of(PriceTableService).to receive(:apply_to_customers).with("#{@price_table.id}", parameters['ids']).and_call_original
        
        xhr :post, :apply_price_table, parameters.merge({'price_table_id' => @price_table.id})
      end

      it 'returns ok status' do
        xhr :post, :apply_price_table, parameters.merge({'price_table_id' => @price_table.id})
        
        expect(response).to have_http_status(:ok)
      end
    end
  end
end
