# == Schema Information
#
# Table name: specifications
#
#  id             :integer          not null, primary key
#  name           :string           not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  deleted_at     :datetime
#  automatic      :boolean          default(FALSE)
#  used_in_filter :boolean          default(FALSE)
#

require 'rails_helper'

RSpec.describe SpecificationsController, type: :controller do
  before do
    sign_in :administrator, create(:administrator)
  end
  
  describe "GET index" do
    render_views

    before do
      @spec_1 = build(:specification, name: 'a')
      @spec_1.values << build(:value, value: "V1")
      @spec_1.values << build(:value, value: "V2")
      @spec_1.values << build(:value, value: "V3")
      @spec_1.save!

      @spec_2 = create(:specification, name: 'b')
      @spec_3 = create(:specification, name: 'c')
    end

    it "returns OK" do
      xhr :get, :index

      expect(response).to have_http_status(:ok)
    end

    it "lists the specifications" do
      xhr :get, :index

      expect(json["specifications"].size).to eq(3)

      expect(json["specifications"][0]["id"]).to eq(@spec_1.id)
      expect(json["specifications"][0]["name"]).to eq(@spec_1.name)
      expect(json["specifications"][0]["automatic"]).to eq(@spec_1.automatic?)
      expect(json["specifications"][0]["used_in_filter"]).to eq(@spec_1.used_in_filter?)
      expect(json["specifications"][0]["values"][0]["id"]).to eq(@spec_1.values[0].id)
      expect(json["specifications"][0]["values"][0]["value"]).to eq(@spec_1.values[0].value)
      expect(json["specifications"][0]["values"][0]["comparison_value"]).to eq(@spec_1.values[0].comparison_value_from_mm_to_cm)
      expect(json["specifications"][0]["values"][1]["id"]).to eq(@spec_1.values[1].id)
      expect(json["specifications"][0]["values"][1]["value"]).to eq(@spec_1.values[1].value)
      expect(json["specifications"][0]["values"][1]["comparison_value"]).to eq(@spec_1.values[1].comparison_value_from_mm_to_cm)
      expect(json["specifications"][0]["values"][2]["id"]).to eq(@spec_1.values[2].id)
      expect(json["specifications"][0]["values"][2]["value"]).to eq(@spec_1.values[2].value)
      expect(json["specifications"][0]["values"][2]["comparison_value"]).to eq(@spec_1.values[2].comparison_value_from_mm_to_cm)

      expect(json["specifications"][1]["id"]).to eq(@spec_2.id)
      expect(json["specifications"][1]["name"]).to eq(@spec_2.name)
      expect(json["specifications"][1]["automatic"]).to eq(@spec_2.automatic?)
      expect(json["specifications"][1]["used_in_filter"]).to eq(@spec_2.used_in_filter?)

      expect(json["specifications"][2]["id"]).to eq(@spec_3.id)
      expect(json["specifications"][2]["name"]).to eq(@spec_3.name)
      expect(json["specifications"][2]["automatic"]).to eq(@spec_3.automatic?)
      expect(json["specifications"][2]["used_in_filter"]).to eq(@spec_3.used_in_filter?)
    end
  end

  describe "GET for_filtering" do
    render_views

    before do
      @spec_1 = build(:specification, name: 'a', used_in_filter: true)
      @spec_1.values << build(:value, value: "V1")
      @spec_1.values << build(:value, value: "V2")
      @spec_1.values << build(:value, value: "V3")
      @spec_1.save!

      @spec_2 = build(:specification, name: 'b', used_in_filter: false)
      @spec_2.values << build(:value, value: "V1")
      @spec_2.values << build(:value, value: "V2")
      @spec_2.values << build(:value, value: "V3")
      @spec_2.save!

      @spec_3 = build(:specification, name: 'c', used_in_filter: true)
      @spec_3.values << build(:value, value: "V1")
      @spec_3.values << build(:value, value: "V2")
      @spec_3.values << build(:value, value: "V3")
      @spec_3.save!
    end

    it "returns OK" do
      xhr :get, :for_filtering

      expect(response).to have_http_status(:ok)
    end

    it "lists the specifications that are used in filter with its values" do
      xhr :get, :for_filtering

      expect(json["specifications"].size).to eq(2)

      expect(json["specifications"][0]["id"]).to eq(@spec_1.id)
      expect(json["specifications"][0]["name"]).to eq(@spec_1.name)
      expect(json["specifications"][0]["automatic"]).to eq(@spec_1.automatic?)
      expect(json["specifications"][0]["used_in_filter"]).to eq(@spec_1.used_in_filter?)
      expect(json["specifications"][0]["values"][0]["id"]).to eq(@spec_1.values[0].id)
      expect(json["specifications"][0]["values"][0]["value"]).to eq(@spec_1.values[0].value)
      expect(json["specifications"][0]["values"][0]["comparison_value"]).to eq(@spec_1.values[0].comparison_value_from_mm_to_cm)
      expect(json["specifications"][0]["values"][1]["id"]).to eq(@spec_1.values[1].id)
      expect(json["specifications"][0]["values"][1]["value"]).to eq(@spec_1.values[1].value)
      expect(json["specifications"][0]["values"][1]["comparison_value"]).to eq(@spec_1.values[1].comparison_value_from_mm_to_cm)
      expect(json["specifications"][0]["values"][2]["id"]).to eq(@spec_1.values[2].id)
      expect(json["specifications"][0]["values"][2]["value"]).to eq(@spec_1.values[2].value)
      expect(json["specifications"][0]["values"][2]["comparison_value"]).to eq(@spec_1.values[2].comparison_value_from_mm_to_cm)

      expect(json["specifications"][1]["id"]).to eq(@spec_3.id)
      expect(json["specifications"][1]["name"]).to eq(@spec_3.name)
      expect(json["specifications"][1]["automatic"]).to eq(@spec_3.automatic?)
      expect(json["specifications"][1]["used_in_filter"]).to eq(@spec_3.used_in_filter?)
      expect(json["specifications"][1]["values"][0]["id"]).to eq(@spec_3.values[0].id)
      expect(json["specifications"][1]["values"][0]["value"]).to eq(@spec_3.values[0].value)
      expect(json["specifications"][1]["values"][0]["comparison_value"]).to eq(@spec_3.values[0].comparison_value_from_mm_to_cm)
      expect(json["specifications"][1]["values"][1]["id"]).to eq(@spec_3.values[1].id)
      expect(json["specifications"][1]["values"][1]["value"]).to eq(@spec_3.values[1].value)
      expect(json["specifications"][1]["values"][1]["comparison_value"]).to eq(@spec_3.values[1].comparison_value_from_mm_to_cm)
      expect(json["specifications"][1]["values"][2]["id"]).to eq(@spec_3.values[2].id)
      expect(json["specifications"][1]["values"][2]["value"]).to eq(@spec_3.values[2].value)
      expect(json["specifications"][1]["values"][2]["comparison_value"]).to eq(@spec_3.values[2].comparison_value_from_mm_to_cm)
    end
  end

  describe "POST create" do
    render_views

    context "with invalid params" do
      let(:parameters) {{"name" => "", "automatic" => "false"}}

      it "does not create a new Specification" do
        expect {
          xhr :post, :create, parameters
        }.to change(Specification, :count).by(0)
      end

      it "returns unprocessable entity status" do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "returns the errors" do
        xhr :post, :create, parameters
        
        expect(json["errors"]).to_not be_empty
      end
    end

    context "with valid params" do
      let(:parameters) {{"name" => "Cor", "automatic" => "true", "used_in_filter" => "true", "values" => [
        {"value" => "Verde", "comparison_value" => "2"}, {"value" => "Amarelo", "comparison_value" => "2"}, {"value" => "Vermelho", "comparison_value" => "2"}
      ]}}

      let(:parsed_parameters)  {{"name" => "Cor", "automatic" => "true", "used_in_filter" => "true", "values" => [
        {"value" => "Verde", "comparison_value" => 20}, {"value" => "Amarelo", "comparison_value" => 20}, {"value" => "Vermelho", "comparison_value" => 20}
      ]}}

      it "should initialize the specification service" do
        expect(SpecificationService).to receive(:new).with(parsed_parameters).and_call_original

        xhr :post, :create, parameters
      end

      it "should create the specification" do
        expect_any_instance_of(SpecificationService).to receive(:create).and_call_original
        
        xhr :post, :create, parameters
      end

      it "returns created status" do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:created)
      end

      it "returns the specification data" do
        xhr :post, :create, parameters

        specification = Specification.last

        expect(json["specification"]["id"]).to eq(specification.id)
        expect(json["specification"]["name"]).to eq(specification.name)
        expect(json["specification"]["automatic"]).to eq(specification.automatic?)
        expect(json["specification"]["used_in_filter"]).to eq(specification.used_in_filter?)
      end
    end
  end

  describe "GET show" do
    render_views

    before do
      @spec = build(:specification, automatic: true, used_in_filter: true)

      @spec.values << build(:value, value: "V1", comparison_value: 10)
      @spec.values << build(:value, value: "V2", comparison_value: 20)
      @spec.values << build(:value, value: "V3", comparison_value: 30)

      @spec.save!
    end

    it "should ok status" do
      xhr :get, :show, :id => @spec.id

      expect(response).to have_http_status(:ok)
    end

    it "returns the specification data" do
      xhr :get, :show, :id => @spec.id

      expect(json["specification"]["id"]).to eq(@spec.id)
      expect(json["specification"]["name"]).to eq(@spec.name)
      expect(json["specification"]["automatic"]).to eq(@spec.automatic?)
      expect(json["specification"]["used_in_filter"]).to eq(@spec.used_in_filter?)

      expect(json["specification"]["values"][0]["value"]).to eq(@spec.values[0].value)
      expect(json["specification"]["values"][0]["comparison_value"]).to eq(@spec.values[0].comparison_value_from_mm_to_cm)
      expect(json["specification"]["values"][1]["value"]).to eq(@spec.values[1].value)
      expect(json["specification"]["values"][1]["comparison_value"]).to eq(@spec.values[1].comparison_value_from_mm_to_cm)
      expect(json["specification"]["values"][2]["value"]).to eq(@spec.values[2].value)
      expect(json["specification"]["values"][2]["comparison_value"]).to eq(@spec.values[2].comparison_value_from_mm_to_cm)
    end
  end

  describe "PUT update" do
    render_views

    before do
      @spec = create(:specification, automatic: false)

      @v1 = create(:value, value: "V1", specification: @spec)
      create(:value, value: "V2", specification: @spec)
      create(:value, value: "V3", specification: @spec)
    end

    context "with invalid params" do
      let(:parameters) {{"name" => "", "automatic" => "true"}.merge({"id" => "#{@spec.id}"})}

      it "does not create a new Specification" do
        expect {
          xhr :put, :update, parameters
        }.to change(Specification, :count).by(0)
      end

      it "returns unprocessable entity status" do
        xhr :put, :update, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "returns the errors" do
        xhr :put, :update, parameters
        
        expect(json["errors"]).to_not be_empty
      end
    end

    context "with valid params" do
      let(:parameters) {{"name" => "Cor", "automatic" => "true", "used_in_filter" => "true", "values" => [
        {"value" => "V5", "comparison_value" => "2", "id" => "#{@v1.id}"}, {"value" => "V2", "comparison_value" => "1"}
      ]}}

      let(:parsed_parameters)  {{"name" => "Cor", "automatic" => "true", "used_in_filter" => "true", "values" => [
        {"value" => "V5", "comparison_value" => 20, "id" => "#{@v1.id}"}, {"value" => "V2", "comparison_value" => 10}
      ]}}

      it "should initialize the specification service" do
        expect(SpecificationService).to receive(:new).with(parsed_parameters).and_call_original

        xhr :put, :update, parameters.merge({"id" => "#{@spec.id}"})
      end

      it "should update the specification" do
        expect_any_instance_of(SpecificationService).to receive(:update).with(@spec.id.to_s).and_call_original
        
        xhr :put, :update, parameters.merge({"id" => "#{@spec.id}"})
      end

      it "returns ok status" do
        xhr :put, :update, parameters.merge({"id" => "#{@spec.id}"})
        
        expect(response).to have_http_status(:ok)
      end

      it "returns the specification data" do
        xhr :put, :update, parameters.merge({"id" => "#{@spec.id}"})

        specification = Specification.last

        expect(json["specification"]["id"]).to eq(specification.id)
        expect(json["specification"]["name"]).to eq(specification.name)
        expect(json["specification"]["automatic"]).to eq(specification.automatic?)
        expect(json["specification"]["used_in_filter"]).to eq(specification.used_in_filter?)
      end
    end
  end

  describe "DELETE destroy" do
    before do
      @spec = create(:specification)
    end

    context "with success" do
      it "should destroy the specification" do
        expect {
          xhr :delete, :destroy, {:id => @spec.id}
        }.to change(Specification, :count).by(-1)
      end

      it "returns no content status" do
        xhr :delete, :destroy, {:id => @spec.id}

        expect(response).to have_http_status(:no_content)
      end
    end

    context "with error" do
      let(:spec)    {mock_model(Specification, :destroy => false, :errors => double(:errors, :full_messages => ["foo"]))}

      before do
        allow(Specification).to receive(:find).and_return(spec)
      end

      it "does not destroy the specification" do
        expect {
          xhr :delete, :destroy, {:id => spec.id}
        }.to_not change(Specification, :count)
      end

      it "returns the bad request status" do
        xhr :delete, :destroy, {:id => spec.id}

        expect(response).to have_http_status(:bad_request)
      end

      it "returns the errors" do
        xhr :delete, :destroy, {:id => spec.id}
        
        expect(json["errors"]).to_not be_empty
      end
    end
  end
end
