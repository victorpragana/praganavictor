set :stage, :production
set :branch, :master

# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary server in each group
# is considered to be the first unless any hosts have the primary
# property set.  Don't declare `role :all`, it's a meta role.
role :app, %w{rails@159.203.125.246}
role :web, %w{rails@159.203.125.246}
role :db,  %w{rails@159.203.125.246}


# Extended Server Syntax
# ======================
# This can be used to drop a more detailed server definition into the
# server list. The second argument is a, or duck-types, Hash and is
# used to set extended properties on the server.

server '159.203.125.246', user: 'rails', roles: %w{web app db}