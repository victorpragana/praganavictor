# config valid only for current version of Capistrano
lock '3.4.1'

set :application, 'luss'
set :repo_url, 'git@bitbucket.org:coyosoftware/luss.git'

set :deploy_to, '/home/rails'
set :scm, :git
set :user, "rails"  # The server's user for deploys

set :default_env, { path: "/usr/local/rvm/bin:$PATH" }

set :bundle_flags, "--deployment"

set :linked_dirs, %w{public/system tmp log public/print_server}

set :sidekiq_config, 'config/sidekiq.yml'
set :keep_releases, 3

set :puma_threads, [1, 6]
set :puma_workers, 4
set :puma_bind, 'tcp://127.0.0.1:8443'
set :puma_preload_app, true
set :puma_init_active_record, true

namespace :deploy do
  desc "Build grunt"
  task :grunt_build do
    on roles(:app) do
      execute "source $HOME/.profile && cd '#{release_path}/client'; bower install"
      execute "source $HOME/.profile && cd '#{release_path}/client'; grunt build --force"
    end
  end

  desc "Load profile"
  task :load_profile do
    on roles(:app) do
      execute "source $HOME/.profile"
    end
  end

  desc "Stop faye server"
  task :stop_faye_server do
    on roles(:app) do
      begin
        execute "source $HOME/.profile && cd #{deploy_to}/current && bundle exec thin stop"
      rescue
        execute "echo 'Faye server not running!'"
      end
    end
  end

  desc "Start faye server"
  task :start_faye_server do
    on roles(:app) do
      #execute "source $HOME/.profile && cd #{release_path} && bundle exec thin start -R faye.ru -p 9191 -d -e #{fetch(:stage)} --ssl --ssl-key-file /home/rails/privkey.pem --ssl-cert-file /home/rails/cert.pem"
      execute "source $HOME/.profile && cd #{release_path} && bundle exec thin start -R faye.ru -p 9191 -d -e #{fetch(:stage)}"
    end
  end

  desc "Kill puma server"
  task :kill_puma do
    on roles(:app) do
      begin
        pid = capture 'lsof', '-i:8443', '-t'
        
        execute "kill -QUIT #{pid}" if pid
      rescue
        execute "echo 'Puma not running!'"
      end
    end
  end

  after :deploy, 'deploy:load_profile'
  after 'deploy:finished', 'deploy:kill_puma'
  after 'deploy:load_profile', 'deploy:start_faye_server'
  after 'deploy:updated', 'deploy:grunt_build'
  after 'deploy:started', 'deploy:stop_faye_server'
end