class ItemDatatable < AjaxDatatablesRails::Base
  def sortable_columns
    @sortable_columns ||= %w{Item.id Item.code Item.description Item.unit}
  end

  def searchable_columns
    @searchable_columns ||= %w{Item.id Item.code Item.description Item.unit}
  end

  private

  def data
    records.map do |record|
      [
        '',
        record.id,
        record.code,
        record.description,
        Item.units[record.unit],
        Item.types[record.type]
      ]
    end
  end

  def get_raw_records
    Item.all.order(:type)
  end
end
