class BaseComponentDatatable < AjaxDatatablesRails::Base
  def sortable_columns
    @sortable_columns ||= %w{BaseComponent.id Category.name BaseComponent.name }
  end

  def searchable_columns
    @searchable_columns ||= %w{BaseComponent.id Category.name BaseComponent.name }
  end

  private

  def data
    records.map do |record|
      [
        record.id,
        record.category.name,
        record.name,
        record.families.map do |family|
          { name: family.name, color: family.color }
        end,
        record.cost.to_f,
        BaseComponent.units[record.unit]
      ]
    end
  end

  def get_raw_records
    scope = BaseComponent.select('base_components.*, categories.name as category_name').joins("LEFT JOIN base_components_families ON base_components_families.base_component_id = base_components.id 
      LEFT JOIN categories ON categories.id = base_components.category_id")
      .includes(:families, :category).distinct

    scope = scope.only_deleted if show_deleted?

    scope
  end

  def show_deleted?
    [true, 'true'].include?(params[:show_deleted])
  end
end
