class CustomerDatatable < AjaxDatatablesRails::Base
  def_delegator :@view, :current_seller

  def sortable_columns
    @sortable_columns ||= %w{Customer.id ThirdParty.name ThirdParty.identification ThirdParty.phone}
  end

  def searchable_columns
    @searchable_columns ||= %w{Customer.id ThirdParty.name ThirdParty.identification ThirdParty.phone}
  end

  private

  def data
    records.map do |record|
      [
        "",
        record.id,
        record.name,
        record.formated_identification,
        record.phone,
        record.price_table_for(current_seller.workplace).try(:name),
        record.price_table_for(current_seller.workplace).try(:id),
        record.items_price_table_for(current_seller.workplace).try(:name),
        record.items_price_table_for(current_seller.workplace).try(:id)
      ]
    end
  end

  def get_raw_records
    current_seller.workplace.customers.joins(:third_party)
  end
end
