class LineDatatable < AjaxDatatablesRails::Base
  def_delegator :@view, :current_worker

  def initialize(view_context, active)
    @active  = active if !active.blank?

    super(view_context)
  end

  def sortable_columns
    @sortable_columns ||= %w{Line.id Family.name Line.name Line.updated_at Line.finished Line.active}
  end

  def searchable_columns
    @searchable_columns ||= %w{Line.id Family.name Line.name Line.finished Line.active }
  end

  private

  def data
    records.map do |record|
      [
        record.id,
        record.family.name,
        record.name,
        record.updated_at.strftime("%d/%m/%Y"),
        record.finished,
        record.active,
        record.family.color
      ]
    end
  end

  def get_raw_records
    queryLines = if workplace.is_a?(Factory)
      Line.joins(:family).includes(:family).all
    else
      workplace.lines.joins(:family).includes(:family)
    end

    queryLines = queryLines.where(active: @active) if !@active.blank?

    queryLines
  end

  def workplace
    current_worker.workplace
  end
end
