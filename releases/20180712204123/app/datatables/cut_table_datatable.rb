class CutTableDatatable < AjaxDatatablesRails::Base
  def sortable_columns
    @sortable_columns ||= %w{CutTable.id CutTable.name CutTable.size}
  end

  def searchable_columns
    @searchable_columns ||= %w{CutTable.id CutTable.name CutTable.size}
  end

  private
  def data
    records.map do |record|
      [
        record.id,
        record.name,
        record.size_from_mm_to_cm
      ]
    end
  end

  def get_raw_records
    CutTable.all
  end
end
