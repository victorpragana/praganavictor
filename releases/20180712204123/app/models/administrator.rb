# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  provider               :string           default("email"), not null
#  uid                    :string           default(""), not null
#  email                  :string           default(""), not null
#  username               :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  name                   :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  tokens                 :json
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  type                   :string
#  maximum_discount       :decimal(5, 2)    default(0.0)
#  erp_id                 :integer
#  selected_workplace_id  :integer
#

class Administrator < User
  include DeviseTokenAuth::Concerns::User

  validates_numericality_of :maximum_discount, greater_than_or_equal_to: 0, less_than_or_equal_to: 100

  has_many :orders, dependent: :restrict_with_error, foreign_key: :user_id

  def permissions
    case self.workplace.try(:actable_type)
    when 'Factory'
      {
        'dashboard' => %w{index},
        'families' => %w{index},
        'attributes' => %w{index new edit},
        'base_components' => %w{index new edit},
        'components' => %w{index new edit},
        'lines' => %w{index new edit rules},
        'estimates' => %w{index new edit},
        'orders' => %w{index new edit},
        'production_orders' => %w{index new edit},
        'users' => %w{index new edit},
        'stores' => %w{index new edit},
        'distributors' => %w{index new edit},
        'customers' => %w{index new edit},
        'price_tables' => %w{index new edit},
        'items' => %w{index new edit},
        'cut_tables' => %w{index new edit},
        'document' => %w{index new edit},
        'printers' => %w{index new edit},
        'questions' => %w{index new edit},
        'classifications' => %w{index new edit},
        'conceded_credits' => %w{index},
        'payment_conditions' => %w{index}
      }
    when 'Distributor'
      {
        'dashboard' => %w{index},
        'estimates' => %w{index new edit},
        'orders' => %w{index edit},
        'users' => %w{index new edit},
        'stores' => %w{index new edit},
        'price_tables' => %w{index new edit},
        'items' => %w{index new edit},
        'credits' => %w{index},
        'conceded_credits' => %w{index}
      }
    when 'Store'
      {
        'dashboard' => %w{index},
        'estimates' => %w{index new edit},
        'orders' => %w{index edit},
        'users' => %w{index new edit},
        'customers' => %w{index new edit},
        'price_tables' => %w{index new edit},
        'items' => %w{index new edit},
        'credits' => %w{index}
      }
    else
      {
        
      }
    end
  end
end
