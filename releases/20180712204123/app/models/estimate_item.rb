# == Schema Information
#
# Table name: estimate_items
#
#  id                :integer          not null, primary key
#  estimate_id       :integer
#  item_code         :string
#  item_description  :string           not null
#  unit              :integer          not null
#  quantity          :decimal(8, 2)    not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  type              :integer          default(0)
#  factory_value     :decimal(10, 2)   default(0.0)
#  distributor_value :decimal(10, 2)   default(0.0)
#  store_value       :decimal(10, 2)   default(0.0)
#  customer_value    :decimal(10, 2)   default(0.0)
#  discount          :decimal(5, 2)    default(0.0)
#  item_id           :integer
#

class EstimateItem < ActiveRecord::Base
  include HasUnit

  self.inheritance_column = :_type_disabled

  enum type: {
    product: 0,
    service: 1
  }

  belongs_to :estimate, required: true
  belongs_to :item, required: true

  validates_presence_of :item_description, allow_blank: false
  validates_presence_of :unit, allow_blank: false
  validates_presence_of :type, allow_blank: false
  validates :quantity, presence: true, numericality: { greater_than: 0 }, allow_blank: false
  validates_numericality_of :factory_value, greater_than_or_equal_to: 0, allow_blank: true
  validates_numericality_of :distributor_value, greater_than_or_equal_to: 0, allow_blank: true
  validates_numericality_of :store_value, greater_than_or_equal_to: 0, allow_blank: true
  validates_numericality_of :customer_value, greater_than_or_equal_to: 0, allow_blank: true
  validates_numericality_of :discount, greater_than_or_equal_to: 0, less_than_or_equal_to: 100
  validate :discount_allowed?, unless: Proc.new{ |ei| ei.item_id.blank? }

  def value
    return distributor_value if estimate.client.is_a?(Distributor)
    return store_value       if estimate.client.is_a?(Store)
    return customer_value    if estimate.client.is_a?(Customer)
  end

  def factory_total
    (factory_value * quantity).try(:round, 2)
  end

  def factory_total_discount
    (factory_total * (discount / 100)).try(:round, 2)
  end

  def factory_total_with_discount
    factory_total - factory_total_discount
  end

  def distributor_total
    (distributor_value * quantity).try(:round, 2)
  end

  def distributor_total_discount
    (distributor_total * (discount / 100)).try(:round, 2)
  end

  def distributor_total_with_discount
    distributor_total - distributor_total_discount
  end

  def store_total
    (store_value * quantity).try(:round, 2)
  end

  def store_total_discount
    (store_total * (discount / 100)).try(:round, 2)
  end

  def store_total_with_discount
    store_total - store_total_discount
  end

  def customer_total
    (customer_value * quantity).try(:round, 2)
  end

  def customer_total_discount
    (customer_total * (discount / 100)).try(:round, 2)
  end

  def customer_total_with_discount
    customer_total - customer_total_discount
  end

  def total
    return distributor_total if estimate.client.is_a?(Distributor)
    return store_total       if estimate.client.is_a?(Store)
    return customer_total    if estimate.client.is_a?(Customer)
  end

  def total_discount
    return distributor_total_discount if estimate.client.is_a?(Distributor)
    return store_total_discount       if estimate.client.is_a?(Store)
    return customer_total_discount    if estimate.client.is_a?(Customer)
  end

  def total_with_discount
    return total - total_discount
  end

  private
  def discount_allowed?
    price_table = estimate.client.items_price_table_for(estimate.seller)

    price_table_for_item = PriceTableItem.select(:maximum_discount).find_by(item_id: item.id, price_table_id: price_table.try(:id))

    maximum_discount = price_table_for_item.try(:maximum_discount) || 0

    errors.add(:base, I18n.t('activerecord.errors.models.estimate_item.discount_greater_than_price_table')) if maximum_discount < self.discount
  end
end
