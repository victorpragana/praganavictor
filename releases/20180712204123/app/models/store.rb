# == Schema Information
#
# Table name: stores
#
#  id                           :integer          not null, primary key
#  deleted_at                   :datetime
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  distributor_id               :integer
#  commercial_classification_id :integer
#

class Store < ActiveRecord::Base
  include ThirdPartyWithParanoiaConcern
  include IsPriceTableClient
  include IsWorkplace
  
  belongs_to :distributor, required: true
  
  has_many :customers, dependent: :destroy

  has_many :price_tables, as: :owner, dependent: :destroy
  
  has_many :purchasing_estimates, as: :client, dependent: :destroy, class_name: Estimate
  has_many :selling_estimates, as: :seller, dependent: :destroy, class_name: Estimate

  has_many :purchasing_orders, as: :client, dependent: :destroy, class_name: Order
  has_many :selling_orders, as: :seller, dependent: :destroy, class_name: Order

  belongs_to :commercial_classification
  has_many :lines, through: :commercial_classification

  def owner
    distributor
  end

  def clients
    customers
  end

  def workplaces
    [self]
  end

  def estimates
    Estimate.where(seller_id: workplaces.map{|w| w.third_party.actable_id}, seller_type: workplaces.map{|w| w.third_party.actable_type})
  end

  def orders
    Order.where(seller_id: workplaces.map{|w| w.third_party.actable_id}, seller_type: workplaces.map{|w| w.third_party.actable_type})
  end
end
