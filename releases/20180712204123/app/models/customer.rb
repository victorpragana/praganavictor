# == Schema Information
#
# Table name: customers
#
#  id         :integer          not null, primary key
#  deleted_at :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  store_id   :integer
#

class Customer < ActiveRecord::Base
  include ThirdPartyWithParanoiaConcern
  include IsPriceTableClient
  
  belongs_to :store, required: true

  has_many :purchasing_estimates, as: :client, dependent: :destroy, class_name: Estimate
  has_many :purchasing_orders, as: :client, dependent: :destroy, class_name: Order

  def owner
    store
  end

  def distributor
    distributor_scope.first
  end

  def distributor_id
    distributor_scope.pluck(:id).first
  end

  private
  def distributor_scope
    Distributor.joins(:stores).where(stores: {id: self.store_id})
  end
end
