# == Schema Information
#
# Table name: cities
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  state_id   :integer
#  ibge_code  :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class City < ActiveRecord::Base
  belongs_to :state, required: true

  validates :name, presence: true, uniqueness: {case_sensitive: false}, allow_blank: false
  validates :ibge_code, presence: true, uniqueness: true, allow_blank: false
end
