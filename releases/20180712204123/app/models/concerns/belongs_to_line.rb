module BelongsToLine
  extend ActiveSupport::Concern

  included do
    belongs_to :line, required: true
    scope :for_line, ->(line) { where(line_id: line) }
  end
end