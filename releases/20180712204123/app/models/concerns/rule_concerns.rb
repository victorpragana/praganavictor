module RuleConcerns
  extend ActiveSupport::Concern

  included do
    include HasFormula

    scope :with_width,    ->(width)        {where('width @> ?', width)}
    scope :with_height,   ->(height)       {where('height @> ?', height)}

    before_validation      :reset_width_height_and_area, if: 'unit?'
    before_validation      :reset_width_height, if: 'total_area?'

    validates_presence_of  :control_method, allow_blank: false
    validates_presence_of  :total_consumption, allow_blank: false
    validates_presence_of  :cost, allow_blank: false
    validates_inclusion_of :required, in: [true, false]
    validates_inclusion_of :automatic, in: [true, false]

    validates_presence_of  :width, if: '(width? || area?) && !automatic?'
    validates_presence_of  :height, if: '(height? || area?) && !automatic?'
    validates_presence_of  :area, if: 'total_area? && !automatic?'
    validate               :valid_total_consumption_formula?
    validate               :valid_height_consumption_formula?
    validate               :valid_width_consumption_formula?
    validate               :valid_quantity_consumption_formula?
    validate               :valid_cost_formula?
    validate               :valid_control_method?, if: 'automatic?'

    def informed_consumption?
      return total_consumption.include? '_INFORMADO_'
    end

    private

    def reset_width_height
      self.width, self.height = nil, nil
    end

    def reset_width_height_and_area
      self.width, self.height, self.area = nil, nil, nil
    end

    def valid_height_consumption_formula?
      begin
        validate_field_formula(:height_consumption, ['_LARGURA_', '_ALTURA_', '_AREA_'].concat(questions_variables))
      rescue => e
        errors.add(:base, I18n.t('activerecord.errors.models.rule.invalid_height_consumption_formula'))
      end
    end

    def valid_width_consumption_formula?
      begin
        validate_field_formula(:width_consumption, ['_LARGURA_', '_ALTURA_', '_AREA_'].concat(questions_variables))
      rescue => e
        errors.add(:base, I18n.t('activerecord.errors.models.rule.invalid_width_consumption_formula'))
      end
    end

    def valid_quantity_consumption_formula?
      begin
        validate_field_formula(:quantity_consumption, ['_LARGURA_', '_ALTURA_', '_AREA_'].concat(questions_variables))
      rescue => e
        errors.add(:base, I18n.t('activerecord.errors.models.rule.invalid_quantity_consumption_formula'))
      end
    end

    def valid_total_consumption_formula?
      begin
        validate_field_formula(:total_consumption, ['_LARGURA_', '_ALTURA_', '_AREA_', '_CONSUMO_LARGURA_', '_CONSUMO_ALTURA_', '_CONSUMO_QUANTIDADE_', '_INFORMADO_'].concat(questions_variables))
      rescue => e
        errors.add(:base, I18n.t('activerecord.errors.models.rule.invalid_total_consumption_formula'))
      end
    end

    def valid_cost_formula?
      begin
        validate_field_formula(:cost, ['_LARGURA_', '_ALTURA_', '_AREA_', '_CUSTO_BASE_', '_CUSTO_FIXO_', '_CONSUMO_QUANTIDADE_'].concat(questions_variables))
      rescue Exception => e
        errors.add(:base, I18n.t('activerecord.errors.models.rule.invalid_cost_formula'))
      end
    end

    def valid_control_method?
      errors.add(:base, I18n.t('activerecord.errors.models.rule.invalid_control_method')) unless (height? || width?)
    end

    def questions_variables
      questions.map(&:as_variable)
    end
  end
end