module Orderable
  extend ActiveSupport::Concern

  included do
    belongs_to :seller, polymorphic: true, required: true
    belongs_to :client, polymorphic: true, required: true

    validates_presence_of :date, allow_blank: false

    def seller_type=(class_name)
      super(class_name.constantize.base_class.to_s)
    end
  end
end
