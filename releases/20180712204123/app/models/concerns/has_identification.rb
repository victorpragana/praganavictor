module HasIdentification
  extend ActiveSupport::Concern

  included do
    def formated_identification
      return "" if identification.blank?
      return "#{identification[0..2]}.#{identification[3..5]}.#{identification[6..8]}-#{identification[9..11]}" if identification.length == 11
      return "#{identification[0..1]}.#{identification[2..4]}.#{identification[5..7]}/#{identification[8..11]}-#{identification[12..14]}" if identification.length == 14
    end
  end
end