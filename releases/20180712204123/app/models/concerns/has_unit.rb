module HasUnit
  extend ActiveSupport::Concern

  included do
    enum unit: {
      m:  0, 
      mm: 1, 
      un: 2, 
      m2: 3,
      cm: 4,
      km: 5,
      pleat: 6
    }

    def conversion_factor
      case unit
      when "m"
        return 1.0 / 1000.0
      when "cm"
        return 1.0 / 10.0
      when "km"
        return 1.0 / 1_000_000.0
      else
        return 0.1
      end
    end
  end
end