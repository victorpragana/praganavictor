module IsPriceTableClient
  extend ActiveSupport::Concern

  included do
    has_many :price_table_clients, as: :client, dependent: :destroy
    
    def price_table_for(owner, only_id = false)
      price_table = if only_id
        owner.price_tables.for_lines.select(:id).joins(:clients).find_by(price_table_clients: {client_id: self.id, client_type: self.class})
      else
        owner.price_tables.for_lines.joins(:clients).find_by(price_table_clients: {client_id: self.id, client_type: self.class})
      end

      price_table || owner.price_tables.for_lines.where(default: true).first
    end

    def items_price_table_for(owner, only_id = false)
      price_table = if only_id
        owner.price_tables.for_items.select(:id).joins(:clients).find_by(price_table_clients: {client_id: self.id, client_type: self.class})
      else
        owner.price_tables.for_items.joins(:clients).find_by(price_table_clients: {client_id: self.id, client_type: self.class})
      end

      price_table || owner.price_tables.for_items.where(default: true).first
    end
  end
end