# == Schema Information
#
# Table name: cut_tables
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  size       :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class CutTable < ActiveRecord::Base
  include HasConversion

  validates :name, presence: true, allow_blank: false
  validates :size, presence: true, allow_blank: false, numericality: { greater_than: 0, only_integer: true }

  has_conversion_for :size
end
