# == Schema Information
#
# Table name: printers
#
#  id          :integer          not null, primary key
#  name        :string           not null
#  real_name   :string           not null
#  server_name :string           not null
#  server_id   :string           not null
#  default     :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'eventmachine'

class Printer < ActiveRecord::Base
  validates :name, presence: true, allow_blank: false
  validates :real_name, presence: true, allow_blank: false
  validates :server_name, presence: true, allow_blank: false
  validates :server_id, presence: true, allow_blank: false

  after_save :mark_others_as_not_default, if: Proc.new { |printer| printer.default? }

  WS_PORT = "9191"
  WS_URL  = Rails.env.production? ? '127.0.0.1' : "0.0.0.0"

  def print(data, user_id)
    EM.run do
      ws = Faye::Client.new("ws://" + WS_URL + ":" + WS_PORT + "/faye")

      message = { data: Base64.encode64(data), user_id: user_id, server_id: 1, action: 'print', print_server: server_id, printer: real_name }

      publication = ws.publish("/printers", message)

      publication.callback do
        Rails.logger.info "[WS INFO] Message sent"
      end

      publication.errback do |error|
        Rails.logger.error "[WS ERROR] #{error.inspect}"
      end
    end
  end

  private

  def mark_others_as_not_default
    Printer.where.not(id: self.id).update_all(default: false)
  end
end
