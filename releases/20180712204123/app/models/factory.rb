# == Schema Information
#
# Table name: factories
#
#  id         :integer          not null, primary key
#  deleted_at :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Factory < ActiveRecord::Base
  include ThirdPartyWithParanoiaConcern
  include IsWorkplace

  has_many :price_tables, as: :owner, dependent: :destroy

  has_many :selling_estimates, as: :seller, dependent: :destroy, class_name: Estimate
  has_many :selling_orders, as: :seller, dependent: :destroy, class_name: Order

  def distributors
    Distributor.joins(:third_party).all.order(:id)
  end

  def stores
    Store.joins(:third_party).all.order(:id)
  end

  def customers
    Customer.joins(:third_party).all.order(:id)
  end

  def clients
    distributors.concat(stores).concat(customers)
  end

  def workplaces
    [self].concat(distributors).concat(stores)
  end

  def estimates
    Estimate.where(seller_id: workplaces.map{|w| w.third_party.actable_id}, seller_type: workplaces.map{|w| w.third_party.actable_type})
  end

  def orders
    Order.where(seller_id: workplaces.map{|w| w.third_party.actable_id}, seller_type: workplaces.map{|w| w.third_party.actable_type})
  end
end
