# == Schema Information
#
# Table name: categories
#
#  id            :integer          not null, primary key
#  name          :string(255)      not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  use_cut_table :boolean          default(FALSE)
#

class Category < ActiveRecord::Base
  validates :name, presence: true, uniqueness: {case_sensitive: false}, allow_blank: false, length: 1..255

  before_destroy :has_components?
  after_create :create_non_erasable_component

  has_many :components, dependent: :delete_all
  has_many :line_categories, dependent: :destroy

  def order_for_line(line_id)
    line_categories.find_by_line_id(line_id).try(:order)
  end

  private

  def has_components?
    unless components.where(erasable: true).count.zero?
      errors.add(:base, I18n.t("activerecord.errors.models.category.has_components"))
      return false
    end
  end

  def create_non_erasable_component
    Component.create({name: I18n.t("helpers.none_selected"), cost: 0, erasable: false, category_id: self.id})
  end
end
