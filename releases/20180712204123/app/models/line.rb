# == Schema Information
#
# Table name: lines
#
#  id                   :integer          not null, primary key
#  name                 :string           not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  family_id            :integer
#  sale_price           :decimal(8, 3)    default(0.0), not null
#  horizontal_increment :integer          default(0), not null
#  vertical_increment   :integer          default(0), not null
#  erp_categories       :json
#  ncm                  :string(8)
#  cst                  :integer
#  active               :boolean          default(TRUE)
#  finished             :boolean          default(FALSE)
#

class Line < ActiveRecord::Base
  include HasConversion
  include HasCST

  belongs_to :family, required: true

  has_many :dimensions, dependent: :destroy
  has_many :line_rules, dependent: :destroy
  has_many :rules, through: :line_rules
  has_many :components, through: :rules
  has_many :categories, class_name: 'LineCategory', dependent: :destroy
  has_many :price_table_lines, dependent: :destroy
  has_many :price_tables, through: :price_table_lines
  has_many :options, dependent: :destroy

  has_and_belongs_to_many :questions
  has_and_belongs_to_many :commercial_classifications

  validates :name, presence: { allow_blank: false }, uniqueness: { case_sensitive: false }, length: 1..255
  validates :sale_price, presence: { allow_blank: false }, numericality: { greater_than_or_equal_to: 0 }
  validates :vertical_increment, presence: true, numericality: { greater_than_or_equal_to: 0, only_integer: true }
  validates :horizontal_increment, presence: true, numericality: { greater_than_or_equal_to: 0, only_integer: true }
  validate :dimensions_overlaps, unless: 'dimensions.empty? || vertical_increment.blank? || horizontal_increment.blank?'
  validates :ncm, format: { with: /\A[0-9]{8}\z/, message: "deve ter 8 dígitos númericos" }, allow_blank: true

  validates_associated :rules

  scope :with_dimension,  ->(width, height) { joins(:dimensions).where('dimensions.width @> ? AND dimensions.height @> ?', width, height) }
  scope :active,          ->{ where(active: true) }
  scope :inactive,        ->{ where(active: false) }
  scope :for_family,      ->(family) { where(family_id: family) }

  before_update :remove_rules_with_different_family

  has_conversion_for :horizontal_increment
  has_conversion_for :vertical_increment

  private
  def dimensions_overlaps
    dimensions.each_with_index do |d, index|
      next_dimensions = dimensions[index + 1..dimensions.size] unless index == dimensions.size

      next_dimensions.each do |od|
        next if d.weight != od.weight
        box = d.to_box
        other_box = od.to_box

        errors.add(:base, I18n.t('activerecord.errors.models.line.overlaps')) unless (other_box & box).empty?
      end
    end
  end

  def remove_rules_with_different_family
    Rule.joins(line_rule: [:line]).where(line_rules: { line_id: self.id }).each do |rule|
      rule.destroy unless rule.component.family_ids.include?(self.family_id)
    end
  end
end
