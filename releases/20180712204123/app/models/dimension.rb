# == Schema Information
#
# Table name: dimensions
#
#  id         :integer          not null, primary key
#  width      :int4range        not null
#  height     :int4range        not null
#  line_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  weight     :integer          default(0), not null
#

class Dimension < ActiveRecord::Base
  include BelongsToLine
  include HasConversion
  include HasWeight

  validates :width, presence: { allow_blank: false }
  validates :height, presence: { allow_blank: false }
  validate :overlaps?, unless: "width.blank? || height.blank? || weight.blank?"

  has_conversion_for :width
  has_conversion_for :height

  def to_box
    (width.min..width.max).step(line.horizontal_increment).to_a.product((height.min..height.max).step(line.vertical_increment).to_a)
  end

  private
  def overlaps?
    errors.add(:base, I18n.t("activerecord.errors.models.dimension.overlaps")) unless Dimension.send(self.weight).for_line(line_id).where("box(point(lower(width), lower(height)),point(CASE WHEN upper_inc(width) IS FALSE THEN (upper(width) - 1) ELSE upper(width) END, CASE WHEN upper_inc(height) IS FALSE THEN (upper(height) - 1) ELSE upper(height) END)) && box(point(?, ?), point(?, ?))", width.min, height.min, width.max, height.max).blank?
  end
end
