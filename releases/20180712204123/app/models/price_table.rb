# == Schema Information
#
# Table name: price_tables
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  owner_id   :integer
#  owner_type :string
#  name       :string           default(""), not null
#  type       :integer          default(0), not null
#  default    :boolean          default(FALSE)
#

class PriceTable < ActiveRecord::Base
  self.inheritance_column = :_type_disabled

  has_paper_trail

  enum type: {
    for_lines: 0, 
    for_items: 1
  }

  belongs_to :owner, polymorphic: true, required: true
  
  has_many :clients, class_name: PriceTableClient, dependent: :destroy
  has_many :lines, class_name: PriceTableLine, dependent: :destroy
  has_many :items, class_name: PriceTableItem, dependent: :destroy

  validates :name, presence: { allow_blank: false }, uniqueness: { scope: [:owner_id, :owner_type], case_sensitive: false }
  validates_presence_of :type, allow_blank: false

  after_save :mark_others_as_not_default, if: Proc.new { |pt| pt.default? }

  private
  def mark_others_as_not_default
    PriceTable.where(owner_id: self.owner_id, owner_type: self.owner_type, type: PriceTable.types[self.type]).where.not(id: self.id).update_all(default: false)
  end
end
