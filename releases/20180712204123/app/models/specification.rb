# == Schema Information
#
# Table name: specifications
#
#  id             :integer          not null, primary key
#  name           :string           not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  deleted_at     :datetime
#  automatic      :boolean          default(FALSE)
#  used_in_filter :boolean          default(FALSE)
#

class Specification < ActiveRecord::Base
  acts_as_paranoid

  has_many :values, dependent: :destroy
  
  validates :name, presence: { allow_blank: false }, uniqueness: { case_sensitive: false }

  scope :used_in_filter, -> { where(used_in_filter: true) }
end
