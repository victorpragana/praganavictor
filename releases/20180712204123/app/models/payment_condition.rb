# == Schema Information
#
# Table name: payment_conditions
#
#  id                      :integer          not null, primary key
#  installments            :integer          not null
#  additional_tax          :decimal(5, 2)    default(0.0)
#  antecipation_tax        :decimal(5, 2)    default(0.0)
#  credit_antecipation_tax :decimal(5, 2)    default(0.0)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  type                    :integer          default(0), not null
#  credit_tax              :decimal(5, 2)    default(0.0)
#

class PaymentCondition < ActiveRecord::Base
  self.inheritance_column = :_type_disabled

  validates :type, presence: true, allow_blank: false
  validate :more_than_one_bank_slip?, if: :bank_slip?

  validates_numericality_of :installments, greater_than: 0, only_integer: true
  validates_numericality_of :additional_tax, greater_than_or_equal_to: 0, less_than_or_equal_to: 100
  validates_numericality_of :credit_tax, greater_than_or_equal_to: 0, less_than_or_equal_to: 100
  validates_numericality_of :antecipation_tax, greater_than_or_equal_to: 0, less_than_or_equal_to: 100
  validates_numericality_of :credit_antecipation_tax, greater_than_or_equal_to: 0, less_than_or_equal_to: 100
  validates_uniqueness_of :installments, scope: :type

  enum type: {
    credit_card: 0,
    bank_slip:   1
  }

  def apply_additional_tax(value)
    add_tax(additional_tax, value)
  end

  def apply_credit_tax(value)
    add_tax(credit_tax, value)
  end

  def apply_antecipation_tax(value)
    subtract_tax(antecipation_tax, value)
  end

  def apply_credit_antecipation_tax(value)
    subtract_tax(credit_antecipation_tax, value)
  end

  private

  def more_than_one_bank_slip?
    errors.add(:base, 'Só é permitida uma condição de pagamento para boleto') if !PaymentCondition.bank_slip.count.zero?
  end

  def add_tax(tax, value)
    (1.0 + (tax / 100.0)) * value
  end

  def subtract_tax(tax, value)
    ((1.0 - (tax / 100.0)) * value).round(2)
  end
end
