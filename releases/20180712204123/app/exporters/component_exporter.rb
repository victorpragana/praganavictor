class ComponentExporter
  FILE_PATH = 'system/components.xlsx'

  def export
    p = Axlsx::Package.new

    p.workbook.add_worksheet(name: 'Componentes') do |sheet|
      sheet.add_row %W{ID Código Categoria Unidade Nome Famílias Custo Custo\ Referencial Custo\ Fixo Características Perda\ Esquerda(cm) Perda\ Topo(cm) Perda\ Direita(cm) Perda\ Baixo(cm) Permite\ Rotacionar? Imprimir\ na\ etiqueta\ de\ persiana? Imprimir\ na\ etiqueta\ de\ componentes? Câmbio Perda\ Percentual}

      Component.includes(:families).user_components.find_each do |component|
        sheet.add_row [component.id, component.code, component.category_id, component.unit, component.name, 
          component.family_ids.join(','), component.cost, component.reference_cost, component.fix_cost, component.characteristics,
          component.offset_left_from_mm_to_cm, component.offset_top_from_mm_to_cm, component.offset_right_from_mm_to_cm, component.offset_bottom_from_mm_to_cm,
          component.allow_rotation?, component.print_on_lines_label?, component.print_on_components_label?, component.exchange, component.loss
        ]
      end
    end

    p.workbook.add_worksheet(name: 'Categorias (Apenas consulta)') do |sheet|
      sheet.add_row %W{ID Nome}

      Category.find_each do |category|
        sheet.add_row [category.id, category.name]
      end
    end

    p.workbook.add_worksheet(name: 'Famílias (Apenas consulta)') do |sheet|
      sheet.add_row %W{ID Nome}

      Family.find_each do |family|
        sheet.add_row [family.id, family.name]
      end
    end

    p.use_shared_strings = true
    result = p.serialize(Rails.root.join('public', FILE_PATH).to_s)

    return FILE_PATH if result
    return nil
  end
end