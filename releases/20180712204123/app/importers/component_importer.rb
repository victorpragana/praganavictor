class ComponentImporter
  ID_INDEX              = 0
  CODE_INDEX            = 1
  CATEGORY_INDEX        = 2
  UNIT_INDEX            = 3
  NAME_INDEX            = 4
  FAMILIES_INDEX        = 5
  COST_INDEX            = 6
  REFERENCE_COST_INDEX  = 7
  FIX_COST_INDEX        = 8
  CHARACTERISTICS_INDEX = 9
  OFFSET_LEFT_INDEX     = 10
  OFFSET_TOP_INDEX      = 11
  OFFSET_RIGHT_INDEX    = 12
  OFFSET_BOTTOM_INDEX   = 13
  ALLOW_ROTATION_INDEX  = 14
  PRINT_ON_LINES_LABEL_INDEX  = 15
  PRINT_ON_COMPONENTS_LABEL_INDEX = 16
  EXCHANGE_INDEX        = 17
  LOSS_INDEX            = 18

  attr_reader :success, :errors

  def initialize(file)
    @file   = file
    @errors = []
  end

  def import
    begin
      excel = case File.extname(@file.original_filename)
      when ".xls" then Roo::Excel.new(@file.path, { file_warning: :ignore })
      when ".xlsx" then Roo::Excelx.new(@file.path, { file_warning: :ignore })
      else raise I18n.t('errors.importers.component.unsupported_type', type: "#{@file.original_filename}")
      end

      line = 1

      excel.each_row_streaming({ pad_cells: true, offset: 1 }) do |row|
        line += 1

        component = Component.find_by_id(row[ID_INDEX].value) unless row[ID_INDEX].blank? || row[ID_INDEX].value.blank?

        if component.nil?
          @errors << I18n.t('errors.importers.component.ignored_line', line: "#{line}")
          next
        else
          family_ids = row[FAMILIES_INDEX].cell_value.nil? ? [] : row[FAMILIES_INDEX].cell_value.split(',')

          component.assign_attributes({
            code:            row[CODE_INDEX].try(:value),
            category_id:     row[CATEGORY_INDEX].try(:value),
            unit:            row[UNIT_INDEX].try(:value),
            name:            row[NAME_INDEX].try(:value),
            family_ids:      family_ids,
            cost:            row[COST_INDEX].try(:value),
            reference_cost:  row[REFERENCE_COST_INDEX].try(:value),
            fix_cost:                   row[FIX_COST_INDEX].try(:value),
            characteristics:            row[CHARACTERISTICS_INDEX].try(:value),
            offset_left:                row[OFFSET_LEFT_INDEX].try(:value).to_i * 10,
            offset_top:                 row[OFFSET_TOP_INDEX].try(:value).to_i * 10,
            offset_right:               row[OFFSET_RIGHT_INDEX].try(:value).to_i * 10,
            offset_bottom:              row[OFFSET_BOTTOM_INDEX].try(:value).to_i * 10,
            allow_rotation:             ['Sim', 'sim', 'S', 's', 'True', 'true', 'T', 't', 'X', 'x', '1', 1].include?(row[ALLOW_ROTATION_INDEX].try(:value).to_s.downcase) ? true : false,
            print_on_lines_label:       ['Sim', 'sim', 'S', 's', 'True', 'true', 'T', 't', 'X', 'x', '1', 1].include?(row[PRINT_ON_LINES_LABEL_INDEX].try(:value).to_s.downcase) ? true : false,
            print_on_components_label:  ['Sim', 'sim', 'S', 's', 'True', 'true', 'T', 't', 'X', 'x', '1', 1].include?(row[PRINT_ON_COMPONENTS_LABEL_INDEX].try(:value).to_s.downcase) ? true : false,
            exchange:                   row[EXCHANGE_INDEX].try(:value),
            loss:                       row[LOSS_INDEX].try(:value)
          })

          unless component.save
            @errors << I18n.t('errors.importers.component.update_error', line: "#{line}", message: "#{component.errors.full_messages.join(',')}")
          end
        end
      end

      @success = true
    rescue => e
      @success = false
      @errors = [e.message]
    end
  end
end