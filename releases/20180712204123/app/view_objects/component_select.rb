class ComponentSelect
  include Virtus.model
  include ActiveModel::Validations

  attribute :total_consumption, Decimal
  attribute :width_consumption, Decimal
  attribute :height_consumption, Decimal
  attribute :quantity_consumption, Decimal
  attribute :cost, Decimal
  attribute :price, Decimal
  attribute :total, Decimal
  attribute :component, String

  validate :has_invalid_formulas?

  private
  def has_invalid_formulas?
    if total_consumption.to_f < 0 || cost.to_f < 0 || price.to_f < 0 || total.to_f < 0
      errors.add(:base, I18n.t('virtus.models.component_select.errors.nil_formula', component: component)) 
    end
  end
end