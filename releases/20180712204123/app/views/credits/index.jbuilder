json.data @credits do |credit|
  json.id          credit[:id]
  json.date        credit[:date]
  json.owner       credit[:owner]
  json.destination credit[:destination]
  json.value       credit[:value].to_f
  json.balance     credit[:balance].to_f
  json.reason      credit[:reason]
  json.order_id    credit[:order_id]
  json.antecipated credit[:antecipated]

  if credit[:payment_method].present?
    json.set! :payment_method  do
      json.installment        credit[:payment_method][:installment]
      json.installment_number credit[:payment_method][:installment_number]
    end
  end
end

json.previous_balance @previous_balance.to_f