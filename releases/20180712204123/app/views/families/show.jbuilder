json.set! :family do
  json.id    @family.id
  json.name  @family.name
  json.color @family.color
end