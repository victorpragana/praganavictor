json.set! :item do
  json.(@item, :id, :code, :description)

  json.unit Item.units[@item.unit]
  json.type Item.types[@item.type]
  json.ncm  @item.ncm
  json.cst  Item.csts[@item.cst]

  if !@item.erp_categories.nil?
    json.set! :erp_categories do
      @item.erp_categories.each do |key, value|
        json.set! key, value
      end
    end
  end
end