json.set! :commercial_classification do
  json.id   @commercial_classification.id
  json.name @commercial_classification.name
  json.line_ids @commercial_classification.line_ids
end