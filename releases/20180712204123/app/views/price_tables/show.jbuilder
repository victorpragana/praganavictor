json.set! :price_table do
  json.id      @price_table.id
  json.name    @price_table.name
  json.default @price_table.default?
  json.type    PriceTable.types[@price_table.type]
  
  json.price_table_lines @lines do |ptl|
    json.price            ptl.price
    json.maximum_discount ptl.maximum_discount.to_f

    json.set! :line do
      json.id   ptl.line.id
      json.name ptl.line.name
    end
  end

  json.price_table_items @items do |pti|
    json.price            pti.price.blank? ? '' : pti.price.to_f
    json.maximum_discount pti.maximum_discount.blank? ? '' : pti.maximum_discount.to_f

    json.set! :item do
      json.id          pti.item.id
      json.code        pti.item.code
      json.description pti.item.description
      json.type        Item.types[pti.item.type]
    end
  end
end