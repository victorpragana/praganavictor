json.price_tables @price_tables do |price_table|
  json.id         price_table.id
  json.name       price_table.name
  json.default    price_table.default?
  json.type       PriceTable.types[price_table.type]
end