json.set! :component do
  json.id                  @component.id
  json.name                @component.name
  json.code                @component.code
  json.unit                Component.units[@component.unit]
  json.characteristics     @component.characteristics
  json.cost                @component.cost.to_f
  json.reference_cost      @component.reference_cost.to_f
  json.exchange            @component.exchange.nil? ? nil : @component.exchange.to_f
  json.loss                @component.loss.nil? ? nil : @component.loss.to_f
  json.fix_cost            @component.fix_cost.to_f
  json.category_id         @component.category_id
  json.automatic           @component.automatic?
  json.family_ids          @component.family_ids
  json.offset_left         @component.offset_left_from_mm_to_cm
  json.offset_top          @component.offset_top_from_mm_to_cm
  json.offset_right        @component.offset_right_from_mm_to_cm
  json.offset_bottom       @component.offset_bottom_from_mm_to_cm
  json.allow_rotation      @component.allow_rotation?
  json.deleted             @component.deleted?
  json.cut_table_id        @component.cut_table_id
  json.print_on_components_label   @component.print_on_components_label?
  json.print_on_lines_label        @component.print_on_lines_label?

  json.lines               @component.lines.order(:name).select("DISTINCT lines.id, lines.name") do |line|
    json.id                line.id
    json.name              line.name
  end

  if @component.base
    json.set! :base do
      json.id                @component.base.id
      json.name              @component.base.name
    end
  end

  json.child_ids           @component.child_ids
end