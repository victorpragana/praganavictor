json.children children.each do |child|
  json.id             child.value.id
  json.value          child.value.value
  json.automatic      child.value.automatic?
  json.partial!       'base_components/children', children: child.children
end