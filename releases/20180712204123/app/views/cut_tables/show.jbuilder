json.set! :cut_table do
  json.id   @cut_table.id
  json.name @cut_table.name
  json.size @cut_table.size_from_mm_to_cm
end