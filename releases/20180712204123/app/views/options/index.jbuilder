json.options @options do |option|
  json.id               option.id
  json.width            option.width_from_mm_to_cm
  json.height           option.height_from_mm_to_cm
  json.total            option.total
  json.selected         option.selected

  json.set! :line do
    json.id             option.line.id
    json.name           option.line.name

    json.set! :family do
      json.id           option.line.family.id
      json.name         option.line.family.name
      json.color        option.line.family.color
    end
  end

  json.questions option.parsed_questions do |question|
    json.label question['label']
    json.value question['value']
  end

  json.components option.components do |component|
    json.set! :component do
      json.id               component.component_id
      json.name             component.component_name
      json.characteristics  component.component_characteristics
      json.unit             OptionComponent.units[component.unit]

      json.set! :category do
        json.name component.category_name
      end
    end

    json.total_consumption    component.total_consumption.to_f
    json.width_consumption    component.width_consumption.try(:to_f)
    json.height_consumption   component.height_consumption.try(:to_f)
    json.quantity_consumption component.quantity_consumption.try(:to_f)
    json.unit_value           component.unit_value.to_f
    json.total                component.total.to_f
    json.required             component.required
  end
end