json.payment_conditions @payment_conditions do |payment_condition|
  json.id             payment_condition.id
  json.type           PaymentCondition.types[payment_condition.type]
  json.installments   payment_condition.installments
  json.additional_tax payment_condition.additional_tax.to_f
  json.credit_tax     payment_condition.credit_tax.to_f
end