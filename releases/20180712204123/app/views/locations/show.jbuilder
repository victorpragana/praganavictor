json.set! :location do
  json.id             @location.id
  json.name           @location.name
  json.width          @location.width_from_mm_to_cm
  json.height         @location.height_from_mm_to_cm
  json.quantity       @location.quantity
  json.total          @location.total.to_f
  json.category       Location.categories[@location.category]
  json.environment_id @location.environment_id
  json.discount       @location.discount.to_f
end