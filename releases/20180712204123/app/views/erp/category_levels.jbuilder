json.category_levels @category_levels do |category_level|
  json.id    category_level['id']
  json.name  category_level['nome']
  json.level category_level['nivel']
end