json.set! :document do
  json.id     @document.id?
  json.name   @document.name
  json.text   @document.text
end