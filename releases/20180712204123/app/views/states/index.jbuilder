json.states @states do |state|
  json.id             state.id
  json.name           state.name
  json.acronym        state.acronym
  json.ibge_code      state.ibge_code

  json.cities state.cities.order(:name) do |city|
    json.id           city.id
    json.name         city.name
    json.ibge_code    city.ibge_code
    json.state_id     city.state_id
  end
end