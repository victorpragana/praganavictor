json.id                 rule.id
json.required           rule.required?
json.line_rule_id       rule.line_rule_id
json.category_id        rule.line_rule.category_id
json.weight             Rule.weights[rule.weight]

json.set! :component do
  json.id               rule.component.id
  json.name             rule.component.name

  json.set! :category do
    json.id             rule.component.category.id
    json.name           rule.component.category.name
    json.order          rule.component.category.order_for_line(rule.line_rule.line_id)
  end
end