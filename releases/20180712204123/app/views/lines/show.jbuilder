json.set! :line do
  json.id                   @line.id
  json.name                 @line.name
  json.family_id            @line.family_id
  json.sale_price           @line.sale_price.to_f
  json.horizontal_increment @line.horizontal_increment_from_mm_to_cm
  json.vertical_increment   @line.vertical_increment_from_mm_to_cm
  json.active               @line.active?
  json.finished             @line.finished?

  json.dimensions @line.dimensions do |dimension|
    json.id                   dimension.id
    json.initial_width        dimension.width_from_mm_to_cm.min
    json.final_width          dimension.width_from_mm_to_cm.max
    json.initial_height       dimension.height_from_mm_to_cm.min
    json.final_height         dimension.height_from_mm_to_cm.max
    json.weight               Dimension.weights[dimension.weight]
  end

  json.categories @line.categories.includes(:category) do |lc|
    json.category             lc.category.id
    json.category_id          lc.category.id
    json.category_name        lc.category.name
    json.required             lc.required
  end

  if !@line.erp_categories.nil?
    json.set! :erp_categories do
      @line.erp_categories.each do |key, value|
        json.set! key, value
      end
    end
  end

  json.ncm                   @line.ncm
  json.cst                   Line.csts[@line.cst]

  json.questions @line.questions do |question|
    json.(question, :id, :label, :value)

    json.type Question.types[question.type]
    json.variable question.as_variable

    json.options question.options do |option|
      json.label option['label']
      json.value option['value']
    end
  end
end