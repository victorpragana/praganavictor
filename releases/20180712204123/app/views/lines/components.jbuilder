json.components @components do |component|
  json.partial!             "lines/component_information", component: component
end