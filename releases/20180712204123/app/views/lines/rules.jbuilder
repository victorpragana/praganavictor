json.rules @line_rules do |line_rule|
  unless line_rule.automatic?
    case line_rule[:control_method]
      when LineRule.control_methods[:width]
        json.initial_width    line_rule.width_from_mm_to_cm.min
        json.final_width      line_rule.width_from_mm_to_cm.max
      when LineRule.control_methods[:height]
        json.initial_height   line_rule.height_from_mm_to_cm.min
        json.final_height     line_rule.height_from_mm_to_cm.max
      when LineRule.control_methods[:area]
        json.initial_width    line_rule.width_from_mm_to_cm.min
        json.final_width      line_rule.width_from_mm_to_cm.max
        json.initial_height   line_rule.height_from_mm_to_cm.min
        json.final_height     line_rule.height_from_mm_to_cm.max
      when LineRule.control_methods[:total_area]
        json.initial_area    line_rule.area_from_mm2_to_cm2.min
        json.final_area      line_rule.area_from_mm2_to_cm2.max
    end
  end

  json.id                   line_rule.id
  json.width_consumption    line_rule.width_consumption
  json.height_consumption   line_rule.height_consumption
  json.quantity_consumption line_rule.quantity_consumption
  json.total_consumption    line_rule.total_consumption
  json.cost                 line_rule.cost
  json.required             line_rule.required
  json.automatic            line_rule.automatic?
  json.description          line_rule.description
  json.category_id          line_rule.category_id
  json.weight               LineRule.weights[line_rule.weight]

  json.component_ids        line_rule.component_ids

  json.set! :control_method do
    json.id                 LineRule.control_methods[line_rule.control_method]
  end

  json.rules line_rule.rules do |rule|
    json.partial!           "lines/rule_information", rule: rule
  end
end