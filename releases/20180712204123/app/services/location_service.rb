class LocationService
  attr_reader :success, :errors, :record

  def initialize(parameters)
    @parameters = parameters

    @parameters["category"] = @parameters["category"].to_i if @parameters["category"].present?
  end

  def update(location_id)
    location = Location.find(location_id)

    location.assign_attributes(@parameters)

    save_record(location)
  end

  def create
    location = Location.new(@parameters)

    save_record(location)
  end

  def apply_discount(location_id)
    begin
      ActiveRecord::Base.transaction do
        location = Location.find(location_id)

        location.options.each do |o|
          o.discount = @parameters['discount']

          o.save!
        end

        @success = true
        @record = location.reload
      end
    rescue ActiveRecord::RecordInvalid => e
      @success = false
      @errors = e.record.errors.full_messages
    rescue => e
      @success = false
      @errors = [e.message]
    end
  end

  private
  def save_record(location)
    if location.save
      @success = true
      @record = location.reload
    else
      @success = false
      @errors = location.errors.full_messages
    end
  end
end