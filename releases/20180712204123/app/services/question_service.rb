class QuestionService
  attr_reader :success, :errors, :record

  def initialize(parameters = {})
    @parameters = parameters

    @parameters['type'] = @parameters['type'].to_i if @parameters['type'].present?
  end

  def create
    question = Question.new

    save_record(question)
  end

  def update(question_id)
    question = Question.find(question_id)

    save_record(question)
  end

  def set_lines(question_id)
    question = Question.find(question_id)

    question.line_ids = @parameters['line_ids']

    save_record(question)
  end

  private
 
  def save_record(question)
    question.assign_attributes(@parameters)
    
    if question.save
      @success = true
      @record = question.reload
    else
      @success = false
      @errors = question.errors.full_messages
    end
  end
end