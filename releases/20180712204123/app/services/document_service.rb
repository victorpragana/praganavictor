class DocumentService
  attr_reader :success, :errors, :record

  def initialize(parameters)
    @parameters = parameters
  end

  def update
    document = Document.first

    document.assign_attributes(@parameters)

    save_record(document)
  end

  private

  def save_record(document)
    if document.save
      @success = true
      @record = document.reload
    else
      @success = false
      @errors = document.errors.full_messages
    end
  end
end