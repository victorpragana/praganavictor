class PriceTableService
  attr_reader :success, :errors, :record

  def initialize(parameters)
    @parameters = parameters

    @parameters['type'] = @parameters['type'].to_i if @parameters['type'].present?

    @lines = @parameters.delete('price_table_lines')
    @items = @parameters.delete('price_table_items')
  end

  def create
    begin
      ActiveRecord::Base.transaction do
        price_table = PriceTable.new(@parameters)

        save_record(price_table)
      end
    rescue ActiveRecord::RecordInvalid => e
      @success = false
      @errors  = e.record.errors.full_messages
    rescue => e
      @success = false
      @errors = [e.message]
    end
  end

  def update(price_table_id)
    begin
      ActiveRecord::Base.transaction do
        price_table = find_price_table(price_table_id)

        price_table.assign_attributes(name: @parameters['name'], default: @parameters['default'])

        save_record(price_table)
      end
    rescue ActiveRecord::RecordInvalid => e
      @success = false
      @errors  = e.record.errors.full_messages
    rescue => e
      @success = false
      @errors = [e.message]
    end
  end

  def apply_to_distributors(price_table_id, ids)
    begin
      ActiveRecord::Base.transaction do
        price_table = find_price_table(price_table_id)

        ids.each do |id|
          distributor = Distributor.find(id)

          if price_table.for_lines?
            PriceTableClient.where(price_table_id: @parameters['owner'].price_tables.for_lines.pluck(:id), client: distributor).destroy_all
          elsif price_table.for_items?
            PriceTableClient.where(price_table_id: @parameters['owner'].price_tables.for_items.pluck(:id), client: distributor).destroy_all
          end
          
          ptc = PriceTableClient.find_or_initialize_by(price_table_id: price_table.id, client: distributor)

          ptc.save!
        end

        @success = true
        @record = price_table.reload
      end
    rescue ActiveRecord::RecordInvalid => e
      @success = false
      @errors  = e.record.errors.full_messages
    rescue => e
      @success = false
      @errors = [e.message]
    end
  end

  def apply_to_stores(price_table_id, ids)
    begin
      ActiveRecord::Base.transaction do
        price_table = find_price_table(price_table_id)

        ids.each do |id|
          store = Store.find(id)

          if price_table.for_lines?
            PriceTableClient.where(price_table_id: @parameters['owner'].price_tables.for_lines.pluck(:id), client: store).destroy_all
          elsif price_table.for_items?
            PriceTableClient.where(price_table_id: @parameters['owner'].price_tables.for_items.pluck(:id), client: store).destroy_all
          end
          
          ptc = PriceTableClient.find_or_initialize_by(price_table_id: price_table.id, client: store)

          ptc.save!
        end

        @success = true
        @record = price_table.reload
      end
    rescue ActiveRecord::RecordInvalid => e
      @success = false
      @errors  = e.record.errors.full_messages
    rescue => e
      @success = false
      @errors = [e.message]
    end
  end

  def apply_to_customers(price_table_id, ids)
    begin
      ActiveRecord::Base.transaction do
        price_table = find_price_table(price_table_id)

        ids.each do |id|
          customer = Customer.find(id)

          if price_table.for_lines?
            PriceTableClient.where(price_table_id: @parameters['owner'].price_tables.for_lines.pluck(:id), client: customer).destroy_all
          elsif price_table.for_items?
            PriceTableClient.where(price_table_id: @parameters['owner'].price_tables.for_items.pluck(:id), client: customer).destroy_all
          end
          
          ptc = PriceTableClient.find_or_initialize_by(price_table_id: price_table.id, client: customer)

          ptc.save!
        end

        @success = true
        @record = price_table.reload
      end
    rescue ActiveRecord::RecordInvalid => e
      @success = false
      @errors  = e.record.errors.full_messages
    rescue => e
      @success = false
      @errors = [e.message]
    end
  end

  private
    def find_price_table(price_table_id)
      PriceTable.find_by(id: price_table_id, owner: @parameters['owner'])
    end

    def save_record(price_table)
      if price_table.save
        if @lines.present?
          @lines.each do |line|
            ptl = PriceTableLine.find_or_initialize_by(line_id: line['line']['id'], price_table: price_table)
            ptl.price = line['price'].try{gsub(/\s+/, '')}
            ptl.maximum_discount = line['maximum_discount']
            ptl.save!
          end
        end

        if @items.present?
          @items.each do |item|
            ptp = PriceTableItem.find_or_initialize_by(item_id: item['item']['id'], price_table: price_table)
            ptp.price = item['price']
            ptp.maximum_discount = item['maximum_discount']
            ptp.save!
          end
        end

        @success = true
        @record = price_table.reload
      else
        @success = false
        @errors  = price_table.errors.full_messages

        raise ActiveRecord::Rollback
      end
    end
end