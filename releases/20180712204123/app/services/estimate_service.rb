class EstimateService
  attr_reader :success, :errors, :record

  LOG_TIMES = false

  def initialize(parameters = {})
    @parameters = parameters

    @option_ids  = @parameters.delete('option_ids')
  end

  def update(estimate_id)
    begin
      ActiveRecord::Base.transaction do
        estimate = find_estimate(estimate_id)

        estimate.assign_attributes(@parameters)

        unless @option_ids.blank?
          estimate.options.update_all(selected: false)

          estimate.options.where(id: @option_ids).update_all(selected: true)
        end

        recalculate_totals(estimate_id)

        save_record(estimate)
      end
    rescue => e
      @success = false
      @errors = [e.message]
    end
  end

  def create
    begin
      ActiveRecord::Base.transaction do
        estimate = Estimate.new(@parameters)

        save_record(estimate)
      end
    rescue => e
      @success = false
      @errors = [e.message]
    end
  end

  def update_prices(estimate_id)
    begin
      @errors = []
      @success = false

      estimate = find_estimate(estimate_id)

      options = estimate.options.where({ selected: true })

      count = OptionComponent.where(option_id: options.pluck(:id)).count

      estimate.packing_configurations.destroy_all

      ActiveRecord::Base.connection.close unless Rails.env.test?

      Parallel.each_with_index(OptionComponent.where(option_id: options.pluck(:id)), in_processes: Rails.env.test? ? 0 : 2) do |opc, index|
        ActiveRecord::Base.connection_pool.with_connection do
          container = SidekiqStatus::Container.load(estimate.job_id)

          #Sidekiq.logger.info "[WORKER #{Parallel.worker_number}] Processing #{opc.component_name}"

          yield(index + 1, count, opc.id, opc.component_name, Parallel.worker_number) if block_given?

          next if container.kill_requested?

          time = Time.zone.now.to_f if LOG_TIMES
          grouper = OptionComponentGrouper.new(opc.component_id, options, estimate.optimize?)

          if grouper.group
            Sidekiq.logger.info "[OPTIONCOMPONENTGROUPER] #{Time.zone.now.to_f - time}" if LOG_TIMES
            line_id = opc.option.line_id

            time = Time.zone.now.to_f if LOG_TIMES
            service = ComponentSelectionService.new(grouper.components, grouper.options, estimate.optimize?)

            if service.perform(opc.option, estimate.id)
              Sidekiq.logger.info "[COMPONENTSELECTIONSERVICE] #{Time.zone.now.to_f - time}" if LOG_TIMES
              if service.component.automatic?
                time = Time.zone.now.to_f if LOG_TIMES
                calculator = AutomaticComponentCalculatorService.new(line_id, service.component.id, opc.option.width, opc.option.height, opc.option.id, service.packing, opc.option.questions)

                if calculator.valid?
                  opc.unit_value        = calculator.price(service.total_cost)
                  opc.total_consumption = calculator.total_consumption
                  opc.total             = calculator.total(service.total_cost)

                  opc.width_consumption         = calculator.width_consumption
                  opc.height_consumption        = calculator.height_consumption
                  opc.quantity_consumption      = calculator.quantity_consumption
                  opc.component_id              = service.component.id
                  opc.component_name            = service.component.name
                  opc.component_characteristics = service.component.characteristics
                  opc.unit                      = service.component.unit
                  opc.category_name             = service.component.category.name

                  Sidekiq.logger.info "[AUTOMATICCOMPONENTCALCULATORSERVICE] #{Time.zone.now.to_f - time}" if LOG_TIMES

                  time = Time.zone.now.to_f if LOG_TIMES
                  opc.save!
                  Sidekiq.logger.info "[OPC.SAVE!] #{Time.zone.now.to_f - time}" if LOG_TIMES

                  time = Time.zone.now.to_f if LOG_TIMES
                  packed_options_ids = calculator.packing_for_option.map(&:id)

                  estimate_packing = PackingConfiguration.where(packable: estimate).where("items = ARRAY#{packed_options_ids.to_s}").first

                  if estimate_packing.nil?
                    filename = Rails.root.join('tmp', "e_#{estimate_id}_#{service.component.id}_#{Time.now.to_f}")
                    file = service.packing.draw!(filename, {
                      desired_id: opc.option.id,
                      scale_size: :longest,
                      scale_size_to: 180
                    })

                    estimate_packing = PackingConfiguration.new(
                      packable: estimate,
                      items: packed_options_ids,
                      automatic_dimension: calculator.pack_dimension,
                      component_dimension: calculator.component_dimension,
                      component_name: service.component.name,
                      unit: service.component.unit
                    )

                    File.open("#{filename}.svg") do |f|
                      estimate_packing.packing = f

                      estimate_packing.save!
                    end
                    Sidekiq.logger.info "[PACKINGCONFIGURATION] #{Time.zone.now.to_f - time}" if LOG_TIMES
                  end
                else
                  calculator.errors.full_messages.each {|message| @errors << message}

                  Sidekiq.logger.error "[ERROR][AutomaticComponentCalculatorService] #{@errors}"

                  estimate.update_attribute(:processing_message, estimate.reload.processing_message.concat(@errors))

                  set_error(estimate.id)

                  raise ActiveRecord::Rollback
                end
              else
                time = Time.zone.now.to_f if LOG_TIMES
                calculator = ComponentCalculatorService.new(line_id, service.component.id, opc.option.width, opc.option.height, opc.rule, opc.option.questions)

                if calculator.valid?
                  opc.unit_value                = calculator.price
                  opc.total_consumption         = calculator.total_consumption unless calculator.informed_total_consumption?
                  opc.total                     = calculator.total
                  opc.width_consumption         = calculator.width_consumption
                  opc.height_consumption        = calculator.height_consumption
                  opc.quantity_consumption      = calculator.quantity_consumption
                  opc.component_id              = service.component.id
                  opc.component_name            = service.component.name
                  opc.component_characteristics = service.component.characteristics
                  opc.unit                      = service.component.unit
                  opc.category_name             = service.component.category.name

                  Sidekiq.logger.info "[COMPONENTCALCULATORSERVICE] #{Time.zone.now.to_f - time}" if LOG_TIMES

                  time = Time.zone.now.to_f if LOG_TIMES
                  opc.save!
                  Sidekiq.logger.info "[OPC.SAVE!] #{Time.zone.now.to_f - time}" if LOG_TIMES
                else
                  calculator.errors.full_messages.each {|message| @errors << message}

                  Sidekiq.logger.error "[ERROR][ComponentCalculatorService] #{@errors}"

                  estimate.update_attribute(:processing_message, estimate.reload.processing_message.concat(@errors))

                  set_error(estimate.id)

                  raise ActiveRecord::Rollback
                end
              end
            else
              service.errors.full_messages.each {|message| @errors << message}

              Sidekiq.logger.error "[ERROR][ComponentSelectionService] #{@errors}"

              estimate.update_attribute(:processing_message, estimate.reload.processing_message.concat(@errors))

              set_error(estimate.id)

              raise ActiveRecord::Rollback
            end
          else
            grouper.errors.full_messages.each {|message| @errors << message}

            Sidekiq.logger.error "[ERROR][OptionComponentGrouper] #{@errors}"

            estimate.update_attribute(:processing_message, estimate.reload.processing_message.concat(@errors))

            set_error(estimate.id)

            raise ActiveRecord::Rollback
          end
        end
      end

      unless Rails.env.test?
        begin
          ActiveRecord::Base.connection.reconnect!
        rescue StandardError
          ActiveRecord::Base.connection.reconnect!
        end
      end

      filename = Rails.root.join('tmp', "e_#{estimate_id}_*.svg")
      FileUtils.rm_rf(Dir.glob(filename))

      recalculate_totals(estimate_id)

      @success = true
      @record = estimate.reload
    rescue => e
      @success = false
      @errors = [e.message]

      Sidekiq.logger.error "ERRO! #{e.message} - #{e.backtrace}"
    end
  end

  def recalculate_totals(estimate_id)
    estimate = find_estimate(estimate_id)

    estimate.options.where(selected: true).each do |option|
      seller = estimate.seller
      client = estimate.client
      line   = option.line

      option.factory_total = (option.components.reduce(0){|total, component| total += component.total} * (1 + (line.sale_price / 100))).try(:round, 2)

      if seller.is_a?(Factory)
        calculate_totals_for_factory(seller, client, line, option)
      elsif seller.is_a?(Distributor)
        calculate_totals_for_distributor(seller, client, line, option)
      elsif seller.is_a?(Store)
        calculate_totals_for_store(seller, client, line, option)
      end

      option.save!
    end
  end

  def duplicate(estimate_id)
    estimate = find_estimate(estimate_id)

    begin
      ActiveRecord::Base.transaction do
        new_estimate = estimate.deep_clone(include: [{ environments: :locations }], except: :total)
        new_estimate.status = :free

        new_estimate.save!

        @success = true
        @record = new_estimate.reload
      end
    rescue ActiveRecord::RecordInvalid => e
      @success = false
      @errors = e.record.errors.full_messages
    rescue => e
      @success = false
      @errors = [e.message]
    end
  end

  def apply_discount(estimate_id)
    estimate = find_estimate(estimate_id)

    estimate.discount = @parameters['discount']

    save_record(estimate)
  end

  private
  def save_record(estimate)
    if estimate.save
      @success = true
      @record = estimate.reload
    else
      @success = false
      @errors = estimate.errors.full_messages
    end
  end

  def find_estimate(estimate_id)
    Estimate.find(estimate_id)
  end

  def calculate_totals_for_factory(seller, client, line, option)
    price_table = load_price_table(seller, client, line)

    if client.is_a?(Distributor)
      option.distributor_total = calculate_from_price_table(price_table, option.factory_total)
    elsif client.is_a?(Store)
      option.store_total = calculate_from_price_table(price_table, option.factory_total)
    elsif client.is_a?(Customer)
      option.customer_total = calculate_from_price_table(price_table, option.factory_total)
    end
  end

  def calculate_totals_for_distributor(seller, client, line, option)
    factory_price_table = load_price_table(factory, seller, line)

    option.distributor_total = calculate_from_price_table(factory_price_table, option.factory_total)

    price_table = load_price_table(seller, client, line)

    if client.is_a?(Store)
      option.store_total = calculate_from_price_table(price_table, option.distributor_total)
    elsif client.is_a?(Customer)
      option.customer_total = calculate_from_price_table(price_table, option.distributor_total)
    end
  end

  def calculate_totals_for_store(seller, client, line, option)
    factory_price_table = load_price_table(factory, seller.distributor, line)

    option.distributor_total = calculate_from_price_table(factory_price_table, option.factory_total)

    distributor_price_table = load_price_table(seller.distributor, seller, line)

    option.store_total = calculate_from_price_table(distributor_price_table, option.distributor_total)

    price_table = load_price_table(seller, client, line)

    if client.is_a?(Customer)
      option.customer_total = calculate_from_price_table(price_table, option.store_total)
    end
  end

  def load_price_table(seller, client, line)
    price_table = client.price_table_for(seller, true)

    raise Exceptions::NoPriceTableError.new(I18n.t('services.errors.estimate_service.no_price_table', 
      seller: seller.name, client: client.name, line: line.name)) if price_table.nil?

    price_table_for_line = PriceTableLine.find_by(line_id: line.id, price_table_id: price_table.id)

    raise Exceptions::NoPriceTableError.new(I18n.t('services.errors.estimate_service.no_price_table',
      seller: seller.name, client: client.name, line: line.name)) if price_table_for_line.nil?

    return price_table_for_line
  end

  def calculate_from_price_table(price_table, value)
    return Dentaku::Calculator.new.evaluate(price_table.price, {'_CMV_' => value})
  end

  def factory
    @factory ||= Factory.first
  end

  def set_error(estimate_id)
    Redis.new.set("estimate_#{estimate_id}_error", true)
  end
end