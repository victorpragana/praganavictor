class BaseComponentService
  attr_reader :success, :errors, :record

  def initialize(parameters = {})
    @parameters = parameters

    @parameters['unit'] = @parameters['unit'].to_i
    @values = @parameters.delete('values')
    @create_children = @parameters.delete('create_children')
    @values_used = []
    @attributes_to_update = @parameters.delete('edit')
    @attributes_to_update ||= {}
  end

  def create
    begin
      ActiveRecord::Base.transaction do
        @base_component = BaseComponent.new(@parameters)

        set_base_component_values

        @base_component.save!

        create_child_components if create_children?

        @success = true
        @record = @base_component.reload
      end
    rescue => e
      @success = false
      @errors = [e.message]
    end
  end

  def update(base_component_id)
    @base_component = BaseComponent.find(base_component_id)

    begin
      ActiveRecord::Base.transaction do
        @base_component.values.map(&:really_destroy!)

        @base_component.assign_attributes(@parameters)

        set_base_component_values

        @base_component.save!
        @base_component.reload

        update_child_cut_table if attribute_true?(@attributes_to_update['cut_table_id'])

        if create_children?
          create_child_components
          remove_not_used_components
        else
          @base_component.components.find_each do |component|
            save_component(component)
          end
        end

        @success = true
        @record = @base_component.reload
      end
    rescue ActiveRecord::RecordNotDestroyed => e
      @success = false
      @errors = e.record.errors.full_messages
    rescue => e
      @success = false
      @errors = [e.message]
    end
  end

  def activate(base_component_id, include_children)
    @base_component = BaseComponent.with_deleted.find(base_component_id)

    begin
      ActiveRecord::Base.transaction do
        @base_component.restore(recursive: include_children)

        @success = true
        @record = @base_component.reload
      end
    rescue => e
      @success = false
      @errors = [e.message]
    end 
  end

  private
  def set_base_component_values
    unless @values.blank?
      @values.each do |value|
        parent_value = build_base_component_value(value['id'].to_i)

        set_base_component_value_children(parent_value, value['children'])

        @base_component.values << parent_value
      end
    end
  end

  def build_base_component_value(value_id)
    BaseComponentValue.new({value_id: value_id.to_i})
  end

  def set_base_component_value_children(parent_value, children)
    unless children.blank?
      children.each do |value|
        children_value = build_base_component_value(value['id'].to_i)

        set_base_component_value_children(children_value, value['children'])

        parent_value.children << children_value
        @base_component.values << children_value
      end
    end
  end

  def create_child_components
    @base_component.values.only_parents.each_with_index do |parent_value, index|
      component_name = "#{parent_value.value.value}"
      component_code = @base_component.code.blank? ? nil : "#{index}"
      value_ids = [parent_value.value_id]

      if parent_value.children.empty?
        create_component(component_name, component_code, value_ids)
      else
        parent_value.children.each_with_index do |child_value, index|
          append_component_data(child_value, component_name, component_code, value_ids, index)
        end
      end
    end
  end

  def append_component_data(component_value, component_name, component_code, value_ids, index)
    name = "#{component_name} #{component_value.value.value}"
    code = "#{component_code}-#{index}" unless component_code.blank?
    ids  = value_ids + [component_value.value_id]

    if component_value.children.empty?
      create_component(name, code, ids)
    else
      component_value.children.each_with_index do |child_value, index|
        append_component_data(child_value, name, code, ids, index)
      end
    end
  end

  def update_child_cut_table
    @base_component.reload.components.update_all(cut_table_id: @base_component.cut_table_id)
  end

  def create_component(name, code, value_ids)
    component = Component.with_deleted.find_or_initialize_by({name: name, category_id: @base_component.category_id})

    component.code            = code
    component.base            = @base_component
    component.values          = Value.with_deleted.where(id: value_ids)
    component.deleted_at      = nil

    @values_used << value_ids.map(&:to_i).sort

    save_component(component)
  end

  def save_component(component)
    component.cost            = @base_component.cost            if attribute_true?(@attributes_to_update['cost'] || component.new_record?)
    component.reference_cost  = @base_component.reference_cost  if attribute_true?(@attributes_to_update['reference_cost'] || component.new_record?)
    component.exchange        = @base_component.exchange        if attribute_true?(@attributes_to_update['exchange'] || component.new_record?)
    component.loss            = @base_component.loss            if attribute_true?(@attributes_to_update['loss'] || component.new_record?)
    component.fix_cost        = @base_component.fix_cost      if attribute_true?(@attributes_to_update['fix_cost'] || component.new_record?)
    component.characteristics = @base_component.characteristics if attribute_true?(@attributes_to_update['characteristics'] || component.new_record?)
    component.unit            = @base_component.unit            if attribute_true?(@attributes_to_update['unit'] || component.new_record?)
    component.offset_left     = @base_component.offset_left     if attribute_true?(@attributes_to_update['offset_left'] || component.new_record?)
    component.offset_top      = @base_component.offset_top      if attribute_true?(@attributes_to_update['offset_top'] || component.new_record?)
    component.offset_right    = @base_component.offset_right    if attribute_true?(@attributes_to_update['offset_right'] || component.new_record?)
    component.offset_bottom   = @base_component.offset_bottom   if attribute_true?(@attributes_to_update['offset_bottom'] || component.new_record?)
    component.allow_rotation  = @base_component.allow_rotation  if attribute_true?(@attributes_to_update['allow_rotation'] || component.new_record?)
    component.family_ids      = @base_component.family_ids      if attribute_true?(@attributes_to_update['family_ids'] || component.new_record?)
    component.cut_table       = @base_component.cut_table       if attribute_true?(@attributes_to_update['cut_table'] || component.new_record?)
    component.category_id     = @base_component.category_id     if attribute_true?(@attributes_to_update['category_id'] || component.new_record?)

    component.print_on_lines_label       = @base_component.print_on_lines_label       if attribute_true?(@attributes_to_update['print_on_lines_label'] || component.new_record?)
    component.print_on_components_label  = @base_component.print_on_components_label  if attribute_true?(@attributes_to_update['print_on_components_label'] || component.new_record?)

    component.save!
  end

  def remove_not_used_components
    (@base_component.components.map{|c| c.value_ids.sort} - @values_used).each do |v|
      component_to_remove = @base_component.components.to_a.find{|c| c.value_ids.sort == v.sort}

      component_to_remove.destroy! if component_to_remove
    end
  end

  def create_children?
    attribute_true?(@create_children)
  end

  def attribute_true?(attribute)
    [true, 'true'].include?(attribute)
  end
end