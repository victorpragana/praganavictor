class OptionService
  attr_reader :success, :errors, :record

  def initialize(parameters)
    @parameters = parameters

    @components = @parameters.delete('components')

    @quantity   = @parameters.delete('quantity')

    if @quantity.present?
      @batch    = true
      @quantity = @quantity.to_i
    else
      @batch = false
    end

    @quantity = (@quantity.blank? || @quantity.zero?) ? 1 : @quantity
  end

  def create
    begin
      ActiveRecord::Base.transaction do
        Option.where(location_id: @parameters['location_id']).destroy_all if @batch

        @quantity.times do
          option = Option.new(@parameters)

          save_record(option)
        end
      end
    rescue ActiveRecord::RecordInvalid => e
      @success = false

      add_validation_errors(e)
    rescue => e
      @success = false
      @errors = [e.message]
    end
  end

  def update(id)
    begin
      ActiveRecord::Base.transaction do
        option = Option.find(id)

        option.assign_attributes(@parameters)

        save_record(option)
      end
    rescue ActiveRecord::RecordInvalid => e
      @success = false
      
      add_validation_errors(e)
    rescue => e
      @success = false
      @errors = [e.message]
    end
  end

  private
  def save_record(option)
    option.selected = false

    set_components(option)

    option.save!
      
    @success = true
    @record = option.reload
  end

  def set_components(option)
    unless @components.blank?
      option.components = @components.map do |_component|
        component = Component.find(_component['component_ids'].first)

        rule = if _component['rule_id'].blank?
          Rule.joins(:line_rule, :component).where(component_id: component.id, line_rules: {line_id: option.line_id}).first
        else
          Rule.joins(:line_rule, :component).where(component_id: component.id, id: _component['rule_id']).first
        end

        if component.automatic?
          OptionComponent.new({component: component, component_name: component.name, component_characteristics: component.characteristics,
            unit: component.unit, category_name: component.category.name, total_consumption: 1,
            width_consumption: nil, height_consumption: nil, quantity_consumption: nil,
            unit_value: 1, required: (rule.try(:required) || false), possible_component_ids: _component['component_ids'].uniq, rule: rule})
        else
          calculator = ComponentCalculatorService.new(option.line_id, component.id, option.width, option.height, rule.try(:id), option.questions)

          if calculator.valid?
            total_consumption = if !rule.blank? && rule.informed_consumption? && _component['total_consumption'].present? && !_component['total_consumption'].blank?
              _component['total_consumption']
            else
              calculator.total_consumption
            end

            OptionComponent.new({component: component, component_name: component.name, component_characteristics: component.characteristics,
              unit: component.unit, category_name: component.category.name, total_consumption: total_consumption,
              width_consumption: calculator.width_consumption, height_consumption: calculator.height_consumption, quantity_consumption: calculator.quantity_consumption,
              unit_value: calculator.price, required: (rule.try(:required) || false), possible_component_ids: _component['component_ids'].uniq, rule: rule})
          else
            @success = false
            @errors = calculator.errors.full_messages

            raise ActiveRecord::Rollback
          end
        end
      end
    end
  end

  def add_validation_errors(e)
    @errors = []

    add_errors(e.record.errors.full_messages)

    e.record.components.each do |c|
      add_errors(c.errors.full_messages.map {|msg| "#{msg} para o componente #{c.component_name}"}) unless c.valid?
    end
  end

  def add_errors(errors)
    unless errors.empty?
      errors.compact.each do |msg| 
        @errors << msg
      end
    end
  end
end