class ComponentService
  attr_reader :success, :errors, :record

  def initialize(parameters = {})
    @parameters = parameters

    @parameters['unit'] = @parameters['unit'].to_i
  end

  def create
    component = Component.new(@parameters)

    save_record(component)
  end

  def update(component_id)
    component = Component.find(component_id)

    component.assign_attributes(@parameters)

    save_record(component)
  end

  def clone_kit(component_id)
    if @parameters['component_id'].present?
      component = Component.find(component_id)

      component_to_clone = Component.find(@parameters['component_id'])

      component.child_ids = component_to_clone.child_ids

      save_record(component)
    else
      @success = false
      @errors = [I18n.t('services.errors.component_service.origin_not_set')]
    end
  end

  def create_rules(component_id)
    @errors = []
    @success = false

    if @parameters['rule_ids'].present? && !@parameters['rule_ids'].empty?
      @parameters['rule_ids'].each do |rule_id|
        lr = LineRule.find(rule_id)

        rule = lr.rules.find_or_initialize_by(component_id: component_id).tap do |rule|
          rule.assign_attributes(lr.attributes.except("id", "created_at", "updated_at", "line_id", "category_id"))
        end

        if rule.save == false
          @errors << "Erro ao inserir o componente na regra #{lr.id}"

          @errors << rule.errors.full_messages
        end
      end

      @errors.flatten
      @success = true
    else
      @errors = [I18n.t('services.errors.component_service.create_rules.none_selected')]
    end
  end

  def activate(component_id)
    component = Component.with_deleted.find(component_id)

    begin
      ActiveRecord::Base.transaction do
        component.restore

        @success = true
        @record = component.reload
      end
    rescue => e
      @success = false
      @errors = [e.message]
    end 
  end

  private

  def save_record(component)
    if component.save
      @success = true
      @record = component.reload
    else
      @success = false
      @errors = component.errors.full_messages
    end
  end
end