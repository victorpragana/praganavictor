class SpecificationService
  attr_reader :success, :errors, :record

  def initialize(parameters)
    @parameters = parameters
    @values = @parameters.delete('values')
  end

  def update(specification_id)
    begin
      ActiveRecord::Base.transaction do
        specification = Specification.find(specification_id)

        save_record(specification)

        @success = true
        @record = specification.reload
      end
    rescue ActiveRecord::RecordInvalid => e
      @success = false
      @errors  = e.record.errors.full_messages
    rescue => e
      @success = false
      @errors = [e.message]
    end
  end

  def create
    begin
      ActiveRecord::Base.transaction do
        specification = Specification.with_deleted.find_or_initialize_by({ name: @parameters['name'] })

        save_record(specification)

        @success = true
        @record = specification.reload
      end
    rescue => e
      @success = false
      @errors = [e.message]
    end
  end

  private
  def set_values(specification)
    specification.values.destroy_all
    
    unless @values.blank?
      @values.each do |value|
        v = Value.with_deleted.find_or_create_by(value: value['value'].capitalize, specification_id: specification.id) if !value['id'].present?
        v = Value.with_deleted.find_by_id(value['id']) if value['id'].present?

        v.tap do |v|
          v.value = value['value'].capitalize
          v.comparison_value = value['comparison_value'].present? ? value['comparison_value'] : nil
          v.deleted_at = nil
          v.save!
        end
      end
    end
  end

  def save_record(specification)
    specification.assign_attributes(@parameters)
    specification.deleted_at = nil
    
    specification.save!

    set_values(specification)
  end
end