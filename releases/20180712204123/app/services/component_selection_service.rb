class ComponentSelectionService
  include ActiveModel::Validations
  extend ActiveModel::Translation

  attr_reader :lines_information, :component_ids, :component, :packing, :total_cost

  validates_presence_of :lines_information, :component_ids, allow_blank: false
  validate :same_category_components?, if: '!component_ids.blank? && automatic?'
  validate :components_with_same_control_method?, unless: 'component_ids.blank? || lines_information.blank? || !automatic?'
  validate :components_have_cut_table?, if: '!component_ids.blank? && automatic?'

  SLICE = 6

  def initialize(component_ids, lines_information, optimize)
    @component_ids, @lines_information, @optimize = component_ids, lines_information, optimize

    @component    = nil
    @packing      = nil

    @available_lines      = []
    @available_components = []

    if lines_information.nil? || lines_information[0].is_a?(SimulationOption)
      @use_cache = false
    else
      @lines_information = @lines_information.sort_by { |li| li.width * li.height }
      @use_cache = true
    end
  end

  def perform(desired_option = nil, estimate_id = nil)
    @estimate_id = estimate_id

    return false unless valid?

    if automatic?
      if @optimize
        perform_full_packing(desired_option)

        return errors.empty?
      else
        selected_pack = perform_simple_packing

        if errors.empty?
          @component = Component.find(selected_pack[0])
          @packing = selected_pack[1][:packing]
          @total_cost = selected_pack[1][:cost]
        end

        return errors.empty?
      end
    else
      @component = Component.find(component_ids.first)

      return true
    end
  end

  private
    def perform_simple_packing
      components_data = {}

      if @use_cache
        cache_key = Digest::MD5.hexdigest("group_for_#{component_ids.join('_')}_#{lines_information.map(&:id).join('_')}")

        cached_packings = estimate_cache.fetch(cache_key)
      else
        cached_packings = nil
      end

      if cached_packings.nil?
        @available_lines = Array.new(lines_information)
        @available_components = Array.new(component_ids)

        perform_packing(components_data)

        estimate_cache.write(cache_key, components_data, expires_in: 3.hours) if @use_cache
      else
        components_data = cached_packings
      end

      if components_data.empty?
        errors.add(:base, I18n.t('activemodel.errors.models.component_selection_service.cannot_pack_any_lines'))
      else
        components_data.each do |id, cache_key|
          components_data[id] = estimate_cache.fetch(cache_key)
        end if @use_cache

        return components_data.min_by{|id, data| data[:cost]}
      end
    end

    def perform_full_packing(desired_option)
      valid_packings = []

      estimate_cache_key = Digest::MD5.hexdigest("estimate_#{@estimate_id}")

      if @use_cache
        cache_key = Digest::MD5.hexdigest("group_optimization_for_#{@component_ids.sort.join('_')}_#{@lines_information.map(&:id).sort.join('_')}")

        cached_packings = estimate_cache.fetch(cache_key)

        estimate_cached = [true, "true"].include?(estimate_cache.fetch(estimate_cache_key))
      else
        estimate_cached = false
      end

      option_cache_key = Digest::MD5.hexdigest("option_cache_#{desired_option.id}")

      option_cache = estimate_cache.fetch(option_cache_key)

      if estimate_cached && !option_cache.nil?
        Sidekiq.logger.info "CACHE FOUND FOR #{desired_option.id} - #{option_cache_key}"
        @component = Component.find(option_cache[:component_id])
        @packing = option_cache[:packing]
        @total_cost = option_cache[:total_cost]
      else
        Sidekiq.logger.info "CACHE NOT FOUND FOR #{desired_option.id} - #{option_cache_key}"
        if cached_packings.nil?
          combinations = []

          lines_information.each_slice(SLICE) do |lines_info|
            _combinations = []

            options = Array.new(lines_info)

            if options.size == 1
              _combinations = component_ids.map do |component|
                [[options[0], component]]
              end
            else
              [options.delete_at(0)].product(component_ids).each do |o|
                combine_array_elements(Array.new(options), component_ids, o, [], _combinations)
              end
            end

            combinations.concat(_combinations)
          end

          combinations.each_with_index do |combination, index|
          #Parallel.each_with_index(combinations, in_threads: 4) do |combination, index|
            Sidekiq.logger.info "[COMBINATION] #{index}/#{combinations.size}"
            time = Time.zone.now.to_f
            break if redis.get("estimate_#{@estimate_id}_error") == "true"

            temporary_packing = {}

            grouped_options = combination.group_by { |element| element[1] }

            cache_str = "combination_for_"

            grouped_options.each do |component_id, options|
              cache_str << "#{component_id}_#{options.map{|e| e[0]}.map(&:id).sort.join('_')}"
            end

            combination_cache_key = Digest::MD5.hexdigest(cache_str)

            cached_combination = estimate_cache.fetch(combination_cache_key)

            if !@use_cache || cached_combination.nil?
              grouped_options.keys.each do |component_id|
                @available_components = [component_id]
                @available_lines = grouped_options[component_id].map do |value|
                  value[0]
                end

                perform_packing(temporary_packing)
              end

              if temporary_packing.keys.count == grouped_options.keys.count
                if @use_cache
                  estimate_cache.write(combination_cache_key, temporary_packing, expires_in: 3.hours)

                  valid_packings << combination_cache_key
                else
                  valid_packings << temporary_packing
                end
              end
            else
              valid_packings << combination_cache_key
            end
            Sidekiq.logger.info "[COMBINATION] #{Time.zone.now.to_f - time}"
          end

          if @use_cache
            estimate_cache.write(cache_key, valid_packings, expires_in: 3.hours)
            estimate_cache.write(estimate_cache_key, true, expires_in: 3.hours)
          end
        else
          valid_packings = cached_packings
        end

        if valid_packings.empty?
          errors.add(:base, I18n.t('activemodel.errors.models.component_selection_service.cannot_pack_any_lines'))
        else
          valid_packings = valid_packings.map do |_vp|
            vp = estimate_cache.fetch(_vp)

            vp.each do |component_id, _packing_data|
              vp[component_id] = estimate_cache.fetch(_packing_data)
            end

            vp
          end if @use_cache

          packings_for_line = valid_packings.select do |packing|
            found = false

            packing.each do |component_id, packing_data|
              if found == false
                option = packing_data[:lines].find do |line|
                  desired_option.id == line
                end

                found = true unless option.nil?
              end
            end

            found
          end

          selected_packing = packings_for_line.min_by do |packing_data|
            packing_data.map { |e| e[1][:cost] }.reduce(:+)
          end

          if selected_packing
            selected_packing.each do |component_id, packing_data|
              option = packing_data[:lines].find do |line|
                desired_option.id == line
              end

              if option
                @component = Component.find(component_id)
                @packing = packing_data[:packing]
                @total_cost = packing_data[:cost]

                if @use_cache
                  packing_data[:lines].each do |option|
                    data = {
                      component_id: component_id,
                      packing: packing_data[:packing],
                      total_cost: packing_data[:cost]
                    }

                    option_cache_key = Digest::MD5.hexdigest("option_cache_#{option}")

                    Sidekiq.logger.info("CACHING OPTION #{option} - #{option_cache_key}")

                    estimate_cache.write(option_cache_key, data, expires_in: 3.hours)
                  end
                end

                break
              end
            end
          else
            service = AutomaticComponentCalculatorService.new(line_id, @available_components.first, desired_option.width, desired_option.height, desired_option.id, nil, desired_option.questions)

            width = service.width_consumption
            height = service.height_consumption

            errors.add(:base, I18n.t('activemodel.errors.models.component_selection_service.cannot_pack_line', id: desired_option.id, width: desired_option.width_from_mm_to_cm,
              height: desired_option.height_from_mm_to_cm, real_width: width, real_height: height, line: desired_option.line.name, line_id: desired_option.line_id))
          end
        end
      end
    end

    def same_category_components?
      errors.add(:component_ids, I18n.t('activemodel.errors.models.component_selection_service.different_category_components')) if Component.where(id: component_ids).pluck(:category_id).uniq.length > 1
    end

    def components_with_same_control_method?
      errors.add(:component_ids, I18n.t('activemodel.errors.models.component_selection_service.components_with_different_control_method')) if rules.pluck(:control_method).uniq.size > 1
    end

    def components_have_cut_table?
      components_without_cut_table = Component.where(id: component_ids, cut_table_id: nil).pluck(:id)
      if !components_without_cut_table.empty?
        errors.add(:component_ids, I18n.t('activemodel.errors.models.component_selection_service.components_do_not_have_cut_table', ids: components_without_cut_table.join(', ')))
      end
    end

    def automatic?
      @automatic ||= component_ids.all?{|component_id| Component.find(component_id).automatic? }
    end

    def valid_lines_information?
      lines_information.each do |li|
        unless li.valid?
          li.errors.full_messages.each {|msg| errors.add(:base, msg)}
        end
      end
    end

    def combine_array_elements(options, components_ids, el, line, combinations)
      line << el

      if options.size == 1
        options.product(components_ids).each do |other|
          combinations << (Array.new(line).push(other))
        end
      else
        [options.delete_at(0)].product(components_ids).each do |o|
          combine_array_elements(Array.new(options), components_ids, o, Array.new(line), combinations)
        end
      end
    end

    def perform_packing(components_data)
      if rules.first.width?
        control_method = :width
        orientation = :height
      elsif rules.first.height?
        control_method = :height
        orientation = :width
      end

      @available_components.each do |component_id|
        if @use_cache
          cache_key = Digest::MD5.hexdigest("optimization_for_#{component_id}_#{@available_lines.map(&:id).sort.join('_')}")

          cached_packing = estimate_cache.fetch(cache_key)
        else
          cached_packing = nil
        end

        if cached_packing.nil?
          _component = Component.find(component_id)

          packed_lines = pack_lines(_component, orientation, _component.cut_table.size_from_mm_to_cm)

          next if packed_lines.nil?

          width  = control_method == :width ? converted_value(_component.automatic_value, _component) : packed_lines[:box].used_width.inject(0){|total, width| total = total + width}
          height = control_method == :height ? converted_value(_component.automatic_value, _component) : packed_lines[:box].used_height.inject(0){|total, height| total = total + height}

          calculator = ComponentTotalCostCalculatorService.new(line_id, component_id, width, height)

          if calculator.valid?
            if @use_cache
              data = {cost: calculator.cost, packing: packed_lines[:box], lines: packed_lines[:lines_packed].map(&:id), errors: []}

              estimate_cache.write(cache_key, data, expires_in: 3.hours)

              components_data[component_id] = cache_key
            else
              data = {cost: calculator.cost, packing: packed_lines[:box], lines: packed_lines[:lines_packed].map(&:id), errors: []}

              components_data[component_id] = data
            end
          else
            data = {errors: calculator.errors.full_messages}

            estimate_cache.write(cache_key, data, expires_in: 3.hours) if @use_cache

            data[:errors].each {|e| errors.add(:base, e) }
          end
        else
          if cached_packing[:errors].empty?
            components_data[component_id] = @use_cache ? cache_key : cached_packing
          else
            cached_packing[:errors].each {|e| errors.add(:base, e) }
          end
        end
      end
    end

    def pack_lines(_component, orientation, limit)
      box = nil
      automatic_value = converted_value(_component.automatic_value, _component)
      lines_packed = []

      lines_consumption_information = @available_lines.map do |line_information|
        lines_packed << line_information
        service = AutomaticComponentCalculatorService.new(line_id, _component.id, line_information.width, line_information.height, line_information.id, nil, line_information.questions)

        width = service.width_consumption || converted_value(line_information.width, _component)
        height = service.height_consumption || converted_value(line_information.height, _component)

        [width, height, service.quantity_consumption, line_information.id]
      end

      packing = calculate_min_packing(lines_consumption_information, _component.cut_table.size_from_mm_to_cm, automatic_value)

      calculate_min_dimension(orientation, lines_consumption_information, limit)

      offsets = {
        left:   converted_value(_component.offset_left, _component),
        top:    converted_value(_component.offset_top, _component),
        right:  converted_value(_component.offset_right, _component),
        bottom: converted_value(_component.offset_bottom, _component)
      }

      step = converted_value(1, _component)

      if @min_dimension > limit
        Sidekiq.logger.error "[MESA DE CORTE] - O tamanho mínimo #{@min_dimension} é maior que a mesa de corte #{limit}"
        errors.add(:base, "[MESA DE CORTE] - A menor persiana (#{@min_dimension}cm) é maior que a mesa de corte (#{limit}cm)")
      else
        loop do
          lower_box = BoxPacker.container(orientation == :width ? [@dimension - step, automatic_value] : [automatic_value, @dimension - step], 
            {
              orientation: orientation == :width ? :height : :width,
              packings_limit: packing,
              offsets: offsets,
              remove_exceeding: true
            }
          )

          lines_consumption_information.each do |line_consumption|
            line_consumption[2].times do |index|
              lower_box.add_item [line_consumption[0], line_consumption[1]], 
                label: "#{line_consumption[0]}x#{line_consumption[1]}",
                allow_rotation: _component.allow_rotation?,
                id: line_consumption[3]
            end
          end
          
          lower_box.pack!

          upper_box = BoxPacker.container(orientation == :width ? [@dimension + step, automatic_value] : [automatic_value, @dimension + step], 
            {
              orientation: orientation == :width ? :height : :width,
              packings_limit: packing,
              offsets: offsets,
              remove_exceeding: true
            }
          )

          lines_consumption_information.each do |line_consumption|
            line_consumption[2].times do |index|
              upper_box.add_item [line_consumption[0], line_consumption[1]],
                label: "#{line_consumption[0]}x#{line_consumption[1]}",
                allow_rotation: _component.allow_rotation?,
                id: line_consumption[3]
            end
          end
          
          upper_box.pack!

          if @min_dimension == limit && !lower_box.packed_successfully && !upper_box.packed_successfully
            if (packing == lines_packed.count)
              box = nil

              Sidekiq.logger.error "[ComponentSelectionService] Cannot pack for component #{_component.name}! Canvas limit (#{packing}) reached!"
              break
            else
              packing = packing + 1

              #Sidekiq.logger.info "[ComponentSelectionService] Cannot pack component #{_component.name}! Increasing canvas to #{packing}"

              calculate_min_dimension(orientation, lines_consumption_information, _component.cut_table.size_from_mm_to_cm)
            end
          else
            if (!lower_box.packed_successfully && !upper_box.packed_successfully)
              @min_dimension = @dimension
              @dimension = ((limit + @min_dimension) / 2).round(2)
            elsif (!lower_box.packed_successfully && upper_box.packed_successfully)
              box = upper_box
              break
            elsif (lower_box.packed_successfully && !upper_box.packed_successfully)
              box = lower_box
              break
            elsif (lower_box.packed_successfully && upper_box.packed_successfully)
              limit = @dimension
              @dimension = ((limit + @min_dimension) / 2).round(2)
            end
          end
        end
      end

      if box
        #box.draw! "#{_component.id}_#{automatic_value}"

        return {box: box, lines_packed: lines_packed}
      end

      return nil
    end

    def calculate_min_dimension(orientation, lines_consumption_information, limit)
      @min_dimension = lines_consumption_information.min_by do |line_consumption|
        orientation == :height ? line_consumption[1] : line_consumption[0]
      end

      @min_dimension  = orientation == :height ? @min_dimension[1] : @min_dimension[0]
      @dimension      = ((limit + @min_dimension) / 2).round(2)
    end

    def calculate_min_packing(lines_consumption_information, cut_table_size, component_size)
      lines_area = lines_consumption_information.inject(0) { |total, line_consumption| total + (line_consumption[0] * line_consumption[1]) * line_consumption[2] }

      component_area = cut_table_size * component_size

      number_of_packs = (lines_area / component_area).to_i

      number_of_packs.zero? ? 1 : number_of_packs
    end

    def line_id
      @line_id ||= lines_information.first.line_id
    end

    def rules
      @rules ||= Rule.joins(:line_rule, :component).where(component_id: component_ids, line_rules: {line_id: lines_information.map(&:line_id).uniq})
    end

    def converted_value(value, component)
      value * component.conversion_factor
    end

    def redis
      @redis ||= Redis.new
    end

    def estimate_cache
      @estimate_cache ||= Rails.cache
      #@estimate_cache ||= ActiveSupport::Cache::FileStore.new("#{Rails.root.join('tmp', 'cache')}")
    end
end