class EnvironmentService
  attr_reader :success, :errors, :record

  def initialize(parameters)
    @parameters = parameters

    @parameters["category"] = @parameters["category"].to_i if @parameters["category"].present?
  end

  def update(environment_id)
    environment = Environment.find(environment_id)

    environment.assign_attributes(@parameters)

    save_record(environment)
  end

  def create
    environment = Environment.new(@parameters)

    save_record(environment)
  end

  private
  def save_record(environment)
    if environment.save
      @success = true
      @record = environment.reload
    else
      @success = false
      @errors = environment.errors.full_messages
    end
  end
end