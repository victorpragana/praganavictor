class ComponentCalculatorService < BaseCalculatorService
  validate :non_automatic_component?, unless: 'component_id.blank?'
  validate :nil_cost?, if: proc { |c| c.component_id.present? && c.line_id.present? && c.width.present? && c.height.present? }

  def quantity_consumption
    return 0 if rule.nil?

    return calculator.evaluate(rule.quantity_consumption, user_questions({
        '_LARGURA_' => converted_value(:width),
        '_ALTURA_' => converted_value(:height),
        '_AREA_' => converted_value(:width) * converted_value(:height)
      })
    )
  end

  def total_consumption
    return 0 if rule.nil?

    return calculator.evaluate(rule.total_consumption, user_questions({
        '_LARGURA_' => converted_value(:width),
        '_ALTURA_' => converted_value(:height),
        '_AREA_' => converted_value(:width) * converted_value(:height),
        '_CONSUMO_LARGURA_' => width_consumption,
        '_CONSUMO_ALTURA_' => height_consumption,
        '_CONSUMO_QUANTIDADE_' => quantity_consumption,
        '_INFORMADO_' => 1
      })
    )
  end

  def informed_total_consumption?
    return !rule.nil? && rule.informed_consumption?
  end

  def cost
    return 0 if rule.nil?

    return calculator.evaluate(rule.cost, user_questions({
        '_LARGURA_' => converted_value(:width),
        '_ALTURA_' => converted_value(:height),
        '_AREA_' => converted_value(:width) * converted_value(:height),
        '_CUSTO_BASE_' => component.final_cost,
        '_CUSTO_FIXO_' => component.final_fix_cost,
        '_CONSUMO_QUANTIDADE_' => quantity_consumption
      })
    )
  end

  def price
    return 0 if rule.nil?

    return cost * line_markup
  end

  def total
    begin
      return price * total_consumption if valid?
    rescue => e
      -1
    end
  end

  private

  def non_automatic_component?
    errors.add(:base, I18n.t('activemodel.errors.models.component_calculator_service.automatic_component')) if component.automatic?
  end

  def nil_cost?
    errors.add(:base, I18n.t('activemodel.errors.models.component_calculator_service.nil_cost', component: component.name)) if cost.nil?
  end
end