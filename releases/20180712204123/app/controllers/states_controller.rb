# == Schema Information
#
# Table name: states
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  acronym    :string           not null
#  ibge_code  :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class StatesController < ApplicationController
  before_action :authenticate_worker!

  def index
    @states = State.all.order(:name)
  end
end
