# == Schema Information
#
# Table name: distributors
#
#  id                           :integer          not null, primary key
#  deleted_at                   :datetime
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  commercial_classification_id :integer
#

class DistributorsController < ApplicationController
  include VerifyWorkplace

  before_action :authenticate_seller!
  before_action(except: [:index]) { |c| c.ensure_workplace_is(Factory) }
  before_action(only: [:index]) { |c| c.ensure_workplace_is(Factory, Distributor, Store) }

  before_action :load_distributor, only: [:destroy, :show]
  before_action :authenticate_administrator!, only: [:upload_logo]

  def index
    if params['columns'].present?
      render json: DistributorDatatable.new(view_context)
    else
      @distributors = Distributor.all
    end
  end

  def create
    service = DistributorService.new(distributor_params)

    service.create

    if service.success
      head :created
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def show
  end

  def update
    service = DistributorService.new(distributor_params)

    service.update(params[:id])

    if service.success
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def upload_logo
    service = DistributorService.new(logo: params[:file])

    service.update(params[:id])

    if service.success
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @distributor.destroy
      head :no_content
    else
      render json: { errors: @distributor.errors.full_messages }, status: :bad_request
    end
  end

  def batch_destroy
    begin
      ActiveRecord::Base.transaction do
        params[:ids].each do |id|
          distributor = Distributor.find(id)

          distributor.destroy!
        end
      end

      head :no_content
    rescue => e
      render json: { errors: [e.message] }, status: :bad_request
    end
  end

  def apply_price_table
    service = PriceTableService.new({'owner' => current_seller.workplace})

    service.apply_to_distributors(params[:price_table_id], params[:ids])

    if service.success
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def apply_commercial_classification
    service = CommercialClassificationService.new

    service.apply_to_distributors(params[:commercial_classification_id], params[:ids])

    if service.success
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  private

  def load_distributor
    @distributor = Distributor.find(params[:id])
  end

  def distributor_params
    params.permit(:name, :identification, :zip_code, :street, :neighborhood, :number, :complement, :phone, :email, :corporate_name, 
      :state_inscription, :municipal_inscription, :city_id, :allow_special_discount, :special_discount, :subdomain, :current_account, :agency, :bank)
  end
end
