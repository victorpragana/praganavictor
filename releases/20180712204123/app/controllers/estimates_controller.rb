# == Schema Information
#
# Table name: estimates
#
#  id                 :integer          not null, primary key
#  date               :date             not null
#  observation        :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  order_id           :integer
#  seller_id          :integer
#  seller_type        :string
#  client_id          :integer
#  client_type        :string
#  optimize           :boolean          default(FALSE)
#  discount           :decimal(5, 2)    default(0.0)
#  status             :integer          default(0), not null
#  job_id             :string
#  processing_message :text             default([]), is an Array
#

class EstimatesController < ApplicationController
  include VerifyWorkplace

  before_action :authenticate_seller!
  before_action {|c| c.ensure_workplace_is(Factory, Distributor, Store)}
  
  before_action :load_estimate, only: [:show, :calculate]
  before_action :verify_user_maximum_discount, only: [:apply_discount]

  def index
    @estimates = render json: EstimateDatatable.new(view_context)
  end

  def show
  end

  def create
    service = EstimateService.new(estimate_params)

    service.create

    if service.success
      @estimate = service.record

      render template: 'estimates/show', status: :created
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def update
    service = EstimateService.new(estimate_params)

    service.update(params[:id])

    if service.success
      @estimate = service.record

      render template: 'estimates/show', status: :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def calculate
    if @estimate.free? || @estimate.error?
      service = EstimateService.new({'option_ids' => @estimate.option_ids, 'optimize' => params[:optimize]})

      service.update(params[:id])

      if service.success
        @estimate = service.record

        @estimate.enqueued!

        job_id = EstimatePriceCalculatorWorker.perform_async(@estimate.id)

        @estimate.update_attribute(:job_id, job_id)

        render json: { estimate: { job_id: job_id } }, status: :ok
      else
        render json: { errors: service.errors }, status: :unprocessable_entity
      end
    else
      error = if @estimate.enqueued?
        I18n.t('activerecord.errors.models.estimate.enqueued')
      else
        I18n.t('activerecord.errors.models.estimate.processing')
      end

      render json: { errors: [error] }, status: :bad_request
    end
  end

  def duplicate
    service = EstimateService.new

    service.duplicate(params[:id])

    if service.success
      @estimate = service.record

      render template: 'estimates/show', status: :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def apply_discount
    service = EstimateService.new(estimate_discount_params)

    service.apply_discount(params[:id])

    if service.success
      head :no_content
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  private
    def load_estimate
      @estimate = Estimate.find(params[:id])
    end

    def estimate_params
      params.permit(:date, :client_id, :client_type, :observation, :optimize, option_ids: []).merge({'seller' => current_worker.workplace})
    end

    def estimate_discount_params
      params.permit(:discount)
    end

    def verify_user_maximum_discount
      render json: { errors: [I18n.t('errors.discount_greater_than_user')] }, status: :unprocessable_entity if current_seller.maximum_discount < BigDecimal.new(params[:discount].to_d)
    end
end
