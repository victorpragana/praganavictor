module Overrides
  class TokenValidationsController < DeviseTokenAuth::TokenValidationsController
    def render_validate_token_success
      render json: {
        data: @resource.token_validation_response.merge({
          permissions: @resource.permissions,
          type: @resource.type,
          workplace: { type: @resource.workplace.actable_type, name: @resource.workplace.name },
          workplaces: @resource.workplaces.map { |w| {id: w.id, type: w.workplace_type, name: w.workplace.name} }
        })
      }
    end
  end
end