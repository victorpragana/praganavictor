# == Schema Information
#
# Table name: options
#
#  id                :integer          not null, primary key
#  line_id           :integer
#  location_id       :integer
#  width             :integer          not null
#  height            :integer          not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  selected          :boolean          default(FALSE), not null
#  factory_total     :decimal(10, 2)   default(0.0)
#  distributor_total :decimal(10, 2)   default(0.0)
#  store_total       :decimal(10, 2)   default(0.0)
#  customer_total    :decimal(10, 2)   default(0.0)
#  discount          :decimal(5, 2)    default(0.0)
#  questions         :json
#

class OptionsController < ApplicationController
  include HasConversionToMm
  include VerifyWorkplace

  before_action :authenticate_seller!
  before_action {|c| c.ensure_workplace_is(Factory, Distributor, Store)}

  before_action :load_option, only: [:show, :destroy, :apply_discount]

  def index
    @location = Location.find(params[:location_id])

    if @location.quantity.to_i > 0
      @options = @location.options.includes(:components, line: [:family]).order(:id).limit(1)
    else
      @options = @location.options.includes(:components, line: [:family]).order(:id)
    end
  end

  def show
  end

  def create
    service = OptionService.new(option_params)

    service.create

    if service.success
      @option = service.record

      render template: 'options/show', status: :created
    else
      render json: { errors: service.errors, width: params[:width], height: params[:height] }, status: :unprocessable_entity
    end
  end

  def update
    service = OptionService.new(option_params)

    service.update(params[:id])

    if service.success
      @option = service.record

      render template: 'options/show', status: :ok
    else
      render json: { errors: service.errors, width: params[:width], height: params[:height] }, status: :unprocessable_entity
    end
  end

  def destroy
    if @option.destroy
      EstimateService.new().update_prices(@option.location.environment.estimate_id)

      head :no_content
    else
      render json: { errors: @option.errors.full_messages }, status: :bad_request
    end
  end
  
  private
  
  def load_option
    @option = Option.find(params[:id])
  end

  def option_params
    params.permit(:line_id, :width, :height, :quantity, :location_id, components: [:total_consumption, :rule_id, component_ids: []]).tap do |parameters|
      convert_parameter_to_mm(parameters, :width)
      convert_parameter_to_mm(parameters, :height)

      parameters['questions'] = params['questions'] if params['questions'].present?
    end
  end
end
