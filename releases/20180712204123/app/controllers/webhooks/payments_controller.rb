module Webhooks
  class PaymentsController < ApplicationController
    skip_before_action :verify_authenticity_token
    skip_before_action :change_workplace_by_subdomain

    def status_changed
      service = PaymentService.new(iugu_params)

      service.status_changed

      if service.success
        head :ok
      else
        Rails.logger.error "Erros: #{service.errors.join(', ')}"
        head :ok
      end
    end

    private

    def iugu_params
      params.require(:data).permit(:id, :status, :account_id, :subscription_id)
    end
  end
end
