# == Schema Information
#
# Table name: base_components
#
#  id                        :integer          not null, primary key
#  name                      :string(255)      not null
#  cost                      :decimal(10, 5)   default(0.0), not null
#  category_id               :integer
#  characteristics           :text
#  code                      :string(20)
#  unit                      :integer          default(2), not null
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  deleted_at                :datetime
#  reference_cost            :decimal(10, 5)   default(0.0)
#  offset_left               :integer          default(0)
#  offset_top                :integer          default(0)
#  offset_right              :integer          default(0)
#  offset_bottom             :integer          default(0)
#  allow_rotation            :boolean          default(FALSE)
#  exchange                  :decimal(10, 5)
#  loss                      :decimal(10, 5)
#  cut_table_id              :integer
#  print_on_lines_label      :boolean          default(FALSE)
#  fix_cost                  :decimal(10, 5)   default(0.0)
#  print_on_components_label :boolean          default(FALSE)
#

class BaseComponentsController < ApplicationController
  include HasConversionToMm

  before_action :authenticate_administrator!
  before_filter :load_base_component, only: [:destroy, :show, :really_destroy]

  def index
    if params['columns'].present?
      render json: BaseComponentDatatable.new(view_context)
    else
      @base_components = BaseComponent.includes(:families, :category).order(:category_id)
    end
  end

  def create
    service = BaseComponentService.new(base_component_params)

    service.create

    if service.success
      @base_component = service.record

      render template: 'base_components/show', status: :created
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def show
  end

  def update
    service = BaseComponentService.new(update_base_component_params)

    service.update(params[:id])

    if service.success
      @base_component = service.record

      render template: 'base_components/show', status: :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    begin
      if @base_component.destroy
        head :no_content
      else
        render json: { errors: @base_component.errors.full_messages }, status: :bad_request
      end
    rescue ActiveRecord::RecordNotDestroyed => e
      render json: { errors: e.record.errors.full_messages }, status: :bad_request
    end
  end

  def really_destroy
    begin
      if @base_component.really_destroy!
        head :no_content
      else
        render json: { errors: @base_component.errors.full_messages }, status: :bad_request
      end
    rescue ActiveRecord::RecordNotDestroyed => e
      render json: { errors: e.record.errors.full_messages }, status: :bad_request
    end
  end

  def activate
    service = BaseComponentService.new

    service.activate(params[:id], params[:include_children])

    if service.success
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  private

  def base_component_params
    params.permit(:name, :category_id, :unit, :cost, :reference_cost, :code, :exchange, :create_children,
      :offset_left, :offset_right, :offset_top, :offset_bottom, :allow_rotation, :print_on_lines_label, :print_on_components_label, :characteristics, :loss, :fix_cost, :cut_table_id, family_ids: []).tap do |whitelisted|
      whitelisted.merge!({ values: params[:values] }) if params[:values].present?
      convert_parameter_to_mm(whitelisted, :offset_left)
      convert_parameter_to_mm(whitelisted, :offset_right)
      convert_parameter_to_mm(whitelisted, :offset_top)
      convert_parameter_to_mm(whitelisted, :offset_bottom)
    end
  end

  def update_base_component_params
    params.permit(:name, :category_id, :unit, :cost, :reference_cost, :exchange, :create_children,
      :offset_left, :offset_right, :offset_top, :offset_bottom, :allow_rotation, :print_on_lines_label, :print_on_components_label, :characteristics, :loss, :fix_cost, :cut_table_id, family_ids: [],
      edit: [:category_id, :unit, :cut_table_id, :allow_rotation, :cost, :reference_cost, :exchange, :loss, :fix_cost, :family_ids, :characteristics,
        :offset_top, :offset_left, :offset_right, :offset_bottom, :print_on_lines_label, :print_on_components_label
      ]).tap do |whitelisted|
      whitelisted.merge!({ values: params[:values] }) if params[:values].present?
      convert_parameter_to_mm(whitelisted, :offset_left)
      convert_parameter_to_mm(whitelisted, :offset_right)
      convert_parameter_to_mm(whitelisted, :offset_top)
      convert_parameter_to_mm(whitelisted, :offset_bottom)
    end
  end

  def load_base_component
    @base_component = BaseComponent.with_deleted.includes(values: [:value, :children]).find(params[:id])
  end
end
