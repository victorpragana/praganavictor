# == Schema Information
#
# Table name: categories
#
#  id            :integer          not null, primary key
#  name          :string(255)      not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  use_cut_table :boolean          default(FALSE)
#

class CategoriesController < ApplicationController
  before_action :authenticate_administrator!
  before_filter :load_categories, only: [:index]
  before_filter :load_category, only: [:destroy, :show]

  def index
  end

  def create
    service = CategoryService.new(category_params)

    service.create

    if service.success
      head :created
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def show
  end

  def update
    service = CategoryService.new(category_params)

    service.update(params[:id])

    if service.success
      @category = service.record

      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @category.destroy
      head :no_content
    else
      render json: { errors: @category.errors.full_messages }, status: :bad_request
    end
  end

  private

  def load_categories
    @categories = Category.all.order(:name)
  end

  def load_category
    @category = Category.find(params[:id])
  end

  def category_params
    params.permit(:name, :use_cut_table)
  end
end
