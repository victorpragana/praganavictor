# == Schema Information
#
# Table name: order_items
#
#  id                :integer          not null, primary key
#  line_id           :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  width             :integer          not null
#  height            :integer          not null
#  order_id          :integer
#  option_id         :integer
#  factory_total     :decimal(10, 2)   default(0.0)
#  distributor_total :decimal(10, 2)   default(0.0)
#  store_total       :decimal(10, 2)   default(0.0)
#  customer_total    :decimal(10, 2)   default(0.0)
#  discount          :decimal(5, 2)    default(0.0)
#

class OrderItemsController < ApplicationController
  include VerifyWorkplace

  before_action :authenticate_seller!
  before_action {|c| c.ensure_workplace_is(Factory, Distributor, Store)}
  

  def apply_total
    service = OrderItemService.new(order_item_total_params)

    service.apply_total(params[:id])

    if service.success
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  private
    def order_item_total_params
      params.permit(:total)
    end
end
