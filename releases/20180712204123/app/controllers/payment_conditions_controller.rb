# == Schema Information
#
# Table name: payment_conditions
#
#  id                      :integer          not null, primary key
#  installments            :integer          not null
#  additional_tax          :decimal(5, 2)    default(0.0)
#  antecipation_tax        :decimal(5, 2)    default(0.0)
#  credit_antecipation_tax :decimal(5, 2)    default(0.0)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  type                    :integer          default(0), not null
#  credit_tax              :decimal(5, 2)    default(0.0)
#

class PaymentConditionsController < ApplicationController
  include VerifyWorkplace

  before_action :authenticate_seller!, only: [:index]
  before_action(only: [:index]) { |c| c.ensure_workplace_is(Factory, Distributor, Store) }

  before_action :authenticate_administrator!, except: [:index]
  before_action(except: [:index]) { |c| c.ensure_workplace_is(Factory) }

  before_action :load_payment_condition, only: [:destroy, :show]

  def index
    if params['columns'].present?
      render json: PaymentConditionDatatable.new(view_context)
    else
      @payment_conditions = PaymentCondition.all.order(:type, :installments, :id)
    end
  end

  def create
    service = PaymentConditionService.new(payment_condition_params)

    service.create

    if service.success
      head :created
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def show
  end

  def update
    service = PaymentConditionService.new(payment_condition_params)

    service.update(params[:id])

    if service.success
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

 def destroy
    if @payment_condition.destroy
      head :no_content
    else
      render json: { errors: @payment_condition.errors.full_messages }, status: :bad_request
    end
  end

  private

  def payment_condition_params
    params.permit(:installments, :additional_tax, :credit_tax, :antecipation_tax, :credit_antecipation_tax, :type)
  end

  def load_payment_condition
    @payment_condition = PaymentCondition.find(params[:id])
  end
end
