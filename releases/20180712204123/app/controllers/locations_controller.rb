# == Schema Information
#
# Table name: locations
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  category       :integer          not null
#  environment_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  width          :integer
#  height         :integer
#  quantity       :integer
#

class LocationsController < ApplicationController
  include HasConversionToMm
  include VerifyWorkplace

  before_action :authenticate_seller!
  before_action {|c| c.ensure_workplace_is(Factory, Distributor, Store)}

  before_action :load_location, only: [:destroy, :show, :reset]
  before_action :verify_user_maximum_discount, only: [:apply_discount]

  def index
    @locations = Environment.find(params[:environment_id]).locations.order(:id)
  end

  def show
  end

  def create
    service = LocationService.new(location_params)

    service.create

    if service.success
      @location = service.record

      render template: 'locations/show', status: :created
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def update
    service = LocationService.new(location_params)

    service.update(params[:id])

    if service.success
      @location = service.record

      render template: 'locations/show', status: :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @location.destroy
      EstimateService.new().update_prices(@location.estimate_id)

      head :no_content
    else
      render json: { errors: @location.errors.full_messages }, status: :bad_request
    end
  end

  def reset
    begin
      @location.options.destroy_all

      EstimateService.new().update_prices(@location.estimate_id)

      head :no_content
    rescue ActiveRecord::RecordInvalid => e
      render json: { errors: e.record.errors.full_messages }, status: :bad_request
    rescue => e
      render json: { errors: [e.message] }, status: :bad_request
    end
  end

  def apply_discount
    service = LocationService.new(location_discount_params)

    service.apply_discount(params[:id])

    if service.success
      @location = service.record

      render template: 'locations/show', status: :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  private
    def load_location
      @location = Location.find(params[:id])
    end

    def location_params
      params.permit(:category, :name, :width, :height, :quantity, :environment_id).tap do |parameters|
        convert_parameter_to_mm(parameters, :width)
        convert_parameter_to_mm(parameters, :height)
      end
    end

    def location_discount_params
      params.permit(:discount)
    end

    def verify_user_maximum_discount
      render json: { errors: [I18n.t('errors.discount_greater_than_user')] }, status: :unprocessable_entity if current_seller.maximum_discount < BigDecimal.new(params[:discount].to_d)
    end
end
