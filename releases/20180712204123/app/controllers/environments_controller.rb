# == Schema Information
#
# Table name: environments
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  category    :integer          not null
#  estimate_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class EnvironmentsController < ApplicationController
  include VerifyWorkplace

  before_action :authenticate_seller!
  before_action {|c| c.ensure_workplace_is(Factory, Distributor, Store)}
  before_action :load_environment, only: [:destroy, :show]

  def index
    @environments = Estimate.find(params[:estimate_id]).environments.order(:id)
  end

  def show
  end

  def create
    service = EnvironmentService.new(environment_params)

    service.create

    if service.success
      @environment = service.record

      render template: 'environments/show', status: :created
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def update
    service = EnvironmentService.new(environment_params)

    service.update(params[:id])

    if service.success
      @environment = service.record

      render template: 'environments/show', status: :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @environment.destroy
      EstimateService.new().update_prices(@environment.estimate_id)
      
      head :no_content
    else
      render json: { errors: @environment.errors.full_messages }, status: :bad_request
    end
  end

  private
    def load_environment
      @environment = Environment.find(params[:id])
    end

    def environment_params
      params.permit(:category, :name, :estimate_id)
    end
end
