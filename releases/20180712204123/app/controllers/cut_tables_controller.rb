# == Schema Information
#
# Table name: cut_tables
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  size       :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class CutTablesController < ApplicationController
  include HasConversionToMm

  before_action :authenticate_administrator!
  before_action :load_cut_table, only: [:destroy, :show]

  def index
    if params['columns'].present?
      render json: CutTableDatatable.new(view_context)
    else
      @cut_tables = CutTable.all.order('name ASC, size ASC')
    end
  end

  def create
    service = CutTableService.new(cut_table_params)

    service.create

    if service.success
      @cut_table = service.record

      render template: 'cut_tables/show', status: :created
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def show
  end

  def update
    service = CutTableService.new(cut_table_params)

    service.update(params[:id])

    if service.success
      @cut_table = service.record

      render template: 'cut_tables/show', status: :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @cut_table.destroy
      head :no_content
    else
      render json: { errors: @cut_table.errors.full_messages }, status: :bad_request
    end
  end

  private

  def cut_table_params
    params.permit(:name, :size).tap do |parameters|
      convert_parameter_to_mm(parameters, :size)
    end
  end

  def load_cut_table
    @cut_table = CutTable.find(params[:id])
  end
end
