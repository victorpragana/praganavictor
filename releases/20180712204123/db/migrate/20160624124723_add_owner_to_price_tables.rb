class AddOwnerToPriceTables < ActiveRecord::Migration
  def change
    add_reference :price_tables, :owner, index: true
  end
end
