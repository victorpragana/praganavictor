class AddHorizontalAndVerticalIncrementToLines < ActiveRecord::Migration
  def change
    add_column :lines, :horizontal_increment, :integer, :null => false, :precision => 5, :scale => 2, :default => 0
    add_column :lines, :vertical_increment, :integer, :null => false, :precision => 5, :scale => 2, :default => 0

    Line.find_each do |line|
      line.horizontal_increment = line.dimensions.first.horizontal_increment
      line.vertical_increment = line.dimensions.first.vertical_increment
      line.save!
    end

  end
end
