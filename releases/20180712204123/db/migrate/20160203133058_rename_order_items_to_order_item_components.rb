class RenameOrderItemsToOrderItemComponents < ActiveRecord::Migration
  def change
    rename_table :order_items, :order_item_components

    rename_column :order_item_components, :order_id, :order_item_id
  end
end
