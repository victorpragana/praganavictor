class RemoveClientNameFromEstimates < ActiveRecord::Migration
  def change
    remove_column :estimates, :client_name, :string
  end
end
