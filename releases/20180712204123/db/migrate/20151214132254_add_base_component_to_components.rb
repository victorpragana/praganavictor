class AddBaseComponentToComponents < ActiveRecord::Migration
  def change
    add_reference :components, :base_component, index: true, foreign_key: true
  end
end
