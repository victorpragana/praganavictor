class AddStatusToEstimates < ActiveRecord::Migration
  def change
    add_column :estimates, :status, :integer, null: false, default: 0
  end
end
