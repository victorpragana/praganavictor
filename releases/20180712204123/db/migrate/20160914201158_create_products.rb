class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :code
      t.string :description, null: false
      t.integer :unit, null: false

      t.timestamps null: false
    end
  end
end
