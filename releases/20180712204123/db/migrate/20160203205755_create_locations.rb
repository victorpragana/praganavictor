class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :name, :null => false, :limit => 255
      t.integer :width, :null => false
      t.integer :height, :null => false
      t.integer :category, :null => false
      t.belongs_to :environment, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
