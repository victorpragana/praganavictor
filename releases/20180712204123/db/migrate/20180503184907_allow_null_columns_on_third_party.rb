class AllowNullColumnsOnThirdParty < ActiveRecord::Migration
  def change
    change_column :third_parties, :identification, :string, :null => true
    change_column :third_parties, :street, :string, :null => true
    change_column :third_parties, :number, :string, :null => true
    change_column :third_parties, :phone, :string, :null => true
  end
end
