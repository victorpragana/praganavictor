class AddSellerNameToEstimates < ActiveRecord::Migration
  def change
    add_column :estimates, :seller_name, :string, :null => false, :limit => 255
  end
end
