class ChangeProductsToItems < ActiveRecord::Migration
  def up
    rename_table :products, :items
  end

  def down
    rename_table :items, :products
  end
end
