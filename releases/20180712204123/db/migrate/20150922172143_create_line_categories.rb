class CreateLineCategories < ActiveRecord::Migration
  def change
    create_table :line_categories do |t|
      t.belongs_to :line, index: true, foreign_key: true
      t.integer :category, :null => false

      t.timestamps null: false
    end
  end
end
