class RemoveNameNullConstraintFromLocations < ActiveRecord::Migration
  def change
    change_column_null :locations, :name, true
  end
end
