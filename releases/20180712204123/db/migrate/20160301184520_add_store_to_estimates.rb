class AddStoreToEstimates < ActiveRecord::Migration
  def change
    add_reference :estimates, :store, index: true, foreign_key: true
  end
end
