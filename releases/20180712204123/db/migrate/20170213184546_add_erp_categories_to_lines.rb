class AddErpCategoriesToLines < ActiveRecord::Migration
  def change
    add_column :lines, :erp_categories, :json
  end
end
