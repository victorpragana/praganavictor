class CreateCommercialClassifications < ActiveRecord::Migration
  def change
    create_table :commercial_classifications do |t|
      t.string :name, null: false, index: true

      t.timestamps null: false
    end
  end
end
