class ChangePriceTableProductsToPriceTableItems < ActiveRecord::Migration
  def up
    rename_table :price_table_products, :price_table_items
  end

  def down
    rename_table :price_table_items, :price_table_products
  end
end
