class RemoveLineAndPriceFromPriceTables < ActiveRecord::Migration
  def change
    remove_reference :price_tables, :line, index: true, foreign_key: true
    remove_column :price_tables, :price, :string
  end
end
