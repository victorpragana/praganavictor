class RemovePackingFromOrderItemComponents < ActiveRecord::Migration
  def self.up
    remove_attachment :order_item_components, :packing
  end

  def self.down
    change_table :order_item_components do |t|
      t.attachment :packing
    end
  end
end
