class AddComponentCharacteristicsAndCategoryNameToOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :component_characteristics, :text
    add_column :order_items, :category_name, :string, :null => false, :limit => 255
  end
end
