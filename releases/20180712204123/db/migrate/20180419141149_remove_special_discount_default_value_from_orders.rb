class RemoveSpecialDiscountDefaultValueFromOrders < ActiveRecord::Migration
  def change
    change_column_default :orders, :special_discount, nil
  end
end
