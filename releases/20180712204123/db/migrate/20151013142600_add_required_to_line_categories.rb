class AddRequiredToLineCategories < ActiveRecord::Migration
  def change
    add_column :line_categories, :required, :boolean, :default => true, :null => false
  end
end
