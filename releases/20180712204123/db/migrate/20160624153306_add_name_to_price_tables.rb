class AddNameToPriceTables < ActiveRecord::Migration
  def change
    add_column :price_tables, :name, :string, null: false, default: ''
  end
end
