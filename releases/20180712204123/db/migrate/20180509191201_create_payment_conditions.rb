class CreatePaymentConditions < ActiveRecord::Migration
  def change
    create_table :payment_conditions do |t|
      t.integer :installments, null: false
      t.decimal :additional_tax, precision: 5, scale: 2, default: 0
      t.decimal :antecipation_tax, precision: 5, scale: 2, default: 0
      t.decimal :credit_antecipation_tax, precision: 5, scale: 2, default: 0

      t.timestamps null: false
    end
  end
end
