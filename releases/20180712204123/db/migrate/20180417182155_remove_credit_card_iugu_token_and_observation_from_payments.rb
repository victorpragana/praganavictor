class RemoveCreditCardIuguTokenAndObservationFromPayments < ActiveRecord::Migration
  def change
    remove_column :payments, :credit_card_iugu_token, :string
    remove_column :payments, :observation, :string
  end
end
