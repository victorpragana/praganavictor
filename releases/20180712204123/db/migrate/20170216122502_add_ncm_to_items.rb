class AddNcmToItems < ActiveRecord::Migration
  def change
    add_column :items, :ncm, :string, limit: 8
  end
end
