class AddAreaToLineRules < ActiveRecord::Migration
  def change
    add_column :line_rules, :area, :int4range
  end
end
