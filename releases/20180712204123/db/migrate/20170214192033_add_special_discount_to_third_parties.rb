class AddSpecialDiscountToThirdParties < ActiveRecord::Migration
  def change
    add_column :third_parties, :special_discount, :boolean, default: false
  end
end
