class ChangeSpecialDiscountInThirdParties < ActiveRecord::Migration
  def change
    remove_column :third_parties, :special_discount, :boolean
    add_column :third_parties, :special_discount, :decimal, precision: 5, scale: 2, default: 0
  end
end
