class AddBillingClientToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :billing_client_id, :integer, index: true
    add_column :orders, :billing_client_type, :string, index: true

    Order.reset_column_information

    Order.find_each do |order|
      order.update_columns(billing_client_id: order.client_id, billing_client_type: order.client_type)
    end
  end
end
