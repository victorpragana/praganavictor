class AddHeightConsumptionToRules < ActiveRecord::Migration
  def change
    add_column :rules, :height_consumption, :string
  end
end
