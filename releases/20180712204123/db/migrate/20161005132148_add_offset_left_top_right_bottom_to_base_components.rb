class AddOffsetLeftTopRightBottomToBaseComponents < ActiveRecord::Migration
  def change
    add_column :base_components, :offset_left, :integer, default: 0
    add_column :base_components, :offset_top, :integer, default: 0
    add_column :base_components, :offset_right, :integer, default: 0
    add_column :base_components, :offset_bottom, :integer, default: 0
  end
end
