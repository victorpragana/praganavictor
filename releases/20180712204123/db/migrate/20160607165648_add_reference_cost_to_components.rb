class AddReferenceCostToComponents < ActiveRecord::Migration
  def change
    add_column :components, :reference_cost, :decimal, precision: 8, scale: 3, default: 0
  end
end
