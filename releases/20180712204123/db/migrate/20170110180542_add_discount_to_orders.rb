class AddDiscountToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :discount, :decimal, precision: 5, scale: 2, default: 0
  end
end
