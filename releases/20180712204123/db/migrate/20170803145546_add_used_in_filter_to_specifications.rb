class AddUsedInFilterToSpecifications < ActiveRecord::Migration
  def change
    add_column :specifications, :used_in_filter, :boolean, default: false
  end
end
