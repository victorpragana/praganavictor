class ChangePriceInPriceTableProducts < ActiveRecord::Migration
  def up
    change_column :price_table_products, :price, :decimal, precision: 8, scale: 2, null: true
  end

  def down
    change_column :price_table_products, :price, :decimal, null: false
  end
end
