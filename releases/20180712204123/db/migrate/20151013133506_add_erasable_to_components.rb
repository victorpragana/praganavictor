class AddErasableToComponents < ActiveRecord::Migration
  def change
    add_column :components, :erasable, :boolean, :default => true
  end
end
