class AddSellerNameToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :seller_name, :string, :null => false, :limit => 255
  end
end
