class CreatePriceTableLines < ActiveRecord::Migration
  def change
    create_table :price_table_lines do |t|
      t.belongs_to :price_table, index: true, foreign_key: true
      t.belongs_to :line, index: true, foreign_key: true
      t.string :price, null: false

      t.timestamps null: false
    end
  end
end
