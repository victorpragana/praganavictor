class AddCodeToComponents < ActiveRecord::Migration
  def change
    add_column :components, :code, :string, :limit => 20
  end
end
