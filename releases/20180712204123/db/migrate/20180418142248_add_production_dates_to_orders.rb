class AddProductionDatesToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :sent_to_production_at, :datetime
    add_column :orders, :started_production_at, :datetime
    add_column :orders, :finished_production_at, :datetime
    add_column :orders, :dispatched_at, :datetime
    add_column :orders, :received_at, :datetime
  end
end
