class AddLineRuleToRules < ActiveRecord::Migration
  def change
    add_reference :rules, :line_rule, index: true, foreign_key: true

    Line.find_each do |line|
      results = ActiveRecord::Base.connection.execute("SELECT id, line_id FROM rules")

      results.each do |rule|
        rule = Rule.find(rule["id"])

        lr = LineRule.new(rule.attributes.slice("width", "height", "control_method", "total_consumption", 
          "cost", "required", "height_consumption", "width_consumption", "sale_price"))
        
        lr.line_id = rule["line_id"]
        lr.rules << rule

        rule.quantity_consumptions.find_each do |qc|
          lr.quantity_consumptions << LineRuleQuantityConsumption.new(qc.attributes.slice("height", "width", "quantity"))
        end

        lr.save!
      end
    end
  end
end
