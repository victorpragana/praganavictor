class AddConsumptionsToOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :width_consumption, :decimal, :precision => 10, :scale => 3
    add_column :order_items, :height_consumption, :decimal, :precision => 10, :scale => 3
    add_column :order_items, :quantity_consumption, :decimal, :precision => 10, :scale => 3
  end
end
