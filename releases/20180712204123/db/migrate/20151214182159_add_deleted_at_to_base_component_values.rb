class AddDeletedAtToBaseComponentValues < ActiveRecord::Migration
  def change
    add_column :base_component_values, :deleted_at, :datetime
    add_index :base_component_values, :deleted_at
  end
end
