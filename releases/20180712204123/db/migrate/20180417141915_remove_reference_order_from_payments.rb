class RemoveReferenceOrderFromPayments < ActiveRecord::Migration
  def change
    remove_reference :payments, :order, index: true
  end
end
