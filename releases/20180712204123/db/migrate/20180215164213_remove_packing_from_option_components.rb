class RemovePackingFromOptionComponents < ActiveRecord::Migration
  def self.up
    remove_attachment :option_components, :packing
  end

  def self.down
    change_table :option_components do |t|
      t.attachment :packing
    end
  end
end
