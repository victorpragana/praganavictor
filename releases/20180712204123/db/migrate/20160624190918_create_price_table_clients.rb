class CreatePriceTableClients < ActiveRecord::Migration
  def change
    create_table :price_table_clients do |t|
      t.belongs_to :price_table, index: true, foreign_key: true
      t.integer :client_id, index: true
      t.string :client_type, index: true

      t.timestamps null: false
    end
  end
end
