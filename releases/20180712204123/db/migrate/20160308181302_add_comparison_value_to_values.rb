class AddComparisonValueToValues < ActiveRecord::Migration
  def change
    add_column :values, :comparison_value, :integer
  end
end
