class AddAllowSpecialDiscountToThirdParties < ActiveRecord::Migration
  def change
    add_column :third_parties, :allow_special_discount, :boolean

    ThirdParty.reset_column_information

    ThirdParty.with_deleted.find_each do |tp|
      tp.update_column(:allow_special_discount, tp.read_attribute(:special_discount))
    end
  end
end
