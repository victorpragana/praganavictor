class AddUseCutTableToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :use_cut_table, :boolean, default: false

    Category.reset_column_information

    Category.where('name ilike ?', '%tecido%').update_all(use_cut_table: true)
  end
end
