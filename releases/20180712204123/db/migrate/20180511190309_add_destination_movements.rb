class AddDestinationMovements < ActiveRecord::Migration
  def change
    add_reference :movements, :destination, index: true, null: false

    rename_column :movements, :third_party_id, :owner_id
  end
end
