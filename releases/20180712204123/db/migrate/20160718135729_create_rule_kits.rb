class CreateRuleKits < ActiveRecord::Migration
  def change
    create_table :rule_kits do |t|
      t.integer :base_id, null: false, index: true
      t.integer :child_id, null: false, index: true

      t.timestamps null: false
    end

    add_foreign_key :rule_kits, :rules, column: :base_id
    add_foreign_key :rule_kits, :rules, column: :child_id

    add_index :rule_kits, [:base_id, :child_id],
      name: 'rule_kits_index',
      unique: true

    remove_column :rules, :parent_id, :integer 
  end
end
