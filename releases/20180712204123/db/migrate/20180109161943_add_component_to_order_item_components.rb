class AddComponentToOrderItemComponents < ActiveRecord::Migration
  def change
    add_reference :order_item_components, :component, index: true, foreign_key: true
  end
end
