class RemoveNotNullPriceFromPriceTableLines < ActiveRecord::Migration
  def change
    change_column :price_table_lines, :price, :string, null: true
  end
end
