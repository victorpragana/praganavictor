class CreateSpecifications < ActiveRecord::Migration
  def change
    create_table :specifications do |t|
      t.string :name, :null => false

      t.timestamps null: false
    end
  end
end
