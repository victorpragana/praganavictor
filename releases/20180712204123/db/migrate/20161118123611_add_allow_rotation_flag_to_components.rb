class AddAllowRotationFlagToComponents < ActiveRecord::Migration
  def change
    add_column :components, :allow_rotation, :boolean, default: false
  end
end
