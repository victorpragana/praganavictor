class AddCostToComponents < ActiveRecord::Migration
  def change
    add_column :components, :cost, :decimal, :precision => 8, :scale => 3, :null => false, :default => 0
  end
end
