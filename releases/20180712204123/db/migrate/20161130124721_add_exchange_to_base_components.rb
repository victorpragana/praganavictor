class AddExchangeToBaseComponents < ActiveRecord::Migration
  def change
    add_column :base_components, :exchange, :decimal, precision: 10, scale: 5
  end
end
