class CreateColorsRules < ActiveRecord::Migration
  def change
    create_table :colors_rules, :id => false do |t|
      t.references :color, :rule
    end

    add_index :colors_rules, [:color_id, :rule_id],
      name: "colors_rules_index",
      unique: true
  end
end
