class CreateEnvironments < ActiveRecord::Migration
  def change
    create_table :environments do |t|
      t.string :name, :null => false, :limit => 255
      t.decimal :total, :precision => 8, :scale => 2, :null => false
      t.integer :category, :null => false
      t.belongs_to :estimate, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
