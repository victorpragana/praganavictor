class AddWorkplaceToUsers < ActiveRecord::Migration
  def change
    add_column :users, :workplace_id, :integer
    add_index :users, :workplace_id
    add_column :users, :workplace_type, :string
    add_index :users, :workplace_type
  end
end
