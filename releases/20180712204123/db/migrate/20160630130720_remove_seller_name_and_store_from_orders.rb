class RemoveSellerNameAndStoreFromOrders < ActiveRecord::Migration
  def change
    remove_column :orders, :seller_name, :string
    remove_reference :orders, :store, index: true, foreign_key: true
  end
end
