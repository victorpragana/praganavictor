class AddSellerIdAndSellerTypeToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :seller_id, :integer
    add_index :orders, :seller_id
    add_column :orders, :seller_type, :string
    add_index :orders, :seller_type
  end
end
