class ChangeCostAndReferenceCostInComponents < ActiveRecord::Migration
  def change
    change_column :components, :cost, :decimal, precision: 10, scale: 5, default: 0
    change_column :components, :reference_cost, :decimal, precision: 10, scale: 5, default: 0
  end
end
