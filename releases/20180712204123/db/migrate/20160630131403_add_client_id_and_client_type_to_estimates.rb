class AddClientIdAndClientTypeToEstimates < ActiveRecord::Migration
  def change
    add_column :estimates, :client_id, :integer
    add_index :estimates, :client_id
    add_column :estimates, :client_type, :string
    add_index :estimates, :client_type
  end
end
