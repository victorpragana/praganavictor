class AddVpsaTokenToUsers < ActiveRecord::Migration
  def change
    add_column :users, :vpsa_token, :string
  end
end
