class DropStoresUsersTable < ActiveRecord::Migration
  def change
    drop_table :stores_users
  end
end
