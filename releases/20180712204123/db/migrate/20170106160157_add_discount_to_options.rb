class AddDiscountToOptions < ActiveRecord::Migration
  def change
    add_column :options, :discount, :decimal, precision: 5, scale: 2, default: 0
  end
end
