class ChangeProductCodeAndProductDescriptionToItemCodeAndItemDescriptionInOrderLooseItems < ActiveRecord::Migration
  def change
    rename_column :order_loose_items, :product_code, :item_code
    rename_column :order_loose_items, :product_description, :item_description
  end
end
