class RenameConsumptionToTotalConsumptionInRules < ActiveRecord::Migration
  def change
    rename_column :rules, :consumption, :total_consumption
  end
end
