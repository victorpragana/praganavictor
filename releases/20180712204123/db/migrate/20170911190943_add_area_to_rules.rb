class AddAreaToRules < ActiveRecord::Migration
  def change
    add_column :rules, :area, :int4range
  end
end
