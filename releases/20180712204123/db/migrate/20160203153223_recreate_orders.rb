class RecreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :client_name, :null => false, :limit => 255
      t.date :date, :null => false
      t.decimal :total, :precision => 8, :scale => 2, :null => false
      t.text :observation

      t.timestamps null: false
    end
  end
end
