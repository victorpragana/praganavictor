class AddExchangeToComponents < ActiveRecord::Migration
  def change
    add_column :components, :exchange, :decimal, precision: 10, scale: 5
  end
end
