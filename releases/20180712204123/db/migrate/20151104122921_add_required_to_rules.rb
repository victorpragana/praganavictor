class AddRequiredToRules < ActiveRecord::Migration
  def change
    add_column :rules, :required, :boolean, :default => true, :null => false
  end
end
