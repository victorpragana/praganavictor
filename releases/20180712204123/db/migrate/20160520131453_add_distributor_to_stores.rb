class AddDistributorToStores < ActiveRecord::Migration
  def change
    add_reference :stores, :distributor, index: true, foreign_key: true
  end
end
