'use strict';

angular.module('lussApp')
  .directive('customerForm', function (CustomerModel, StateModel, StoreModel, $rootScope, $stateParams, $state) {
    return {
      templateUrl: '/views/directives/customer_form.html',
      restrict: 'E',
      scope: {
        'inModal':'=',
        'onCreate': '=?'
      },
      link: function(scope) {
        var self = scope;

        CustomerModel.mix({
          $hooks: {
            'after-create': function(response) {
              if (response.status === 201) {
                toastr.remove();
                toastr.success("Cliente criado com sucesso!");

                self.model          = new CustomerModel();
                self.selected_state = null;
                self.selected_city  = null;
                self.cities         = [];

                if (self.inModal === false) {
                  $state.go($state.current, {}, {reload: true});
                } else {
                  if (self.onCreate !== undefined) {
                    self.onCreate();
                  }

                  angular.element('#customer_modal').modal('toggle');
                }
              }
            },
            'after-update': function(response) {
              if (response.status === 200) {
                toastr.remove();
                toastr.success("Cliente atualizado com sucesso!");
                $state.go("customers_index");
              }
            },
            'after-destroy': function(response) {
              if (response.status === 204) {
                toastr.remove();
                toastr.success("Cliente excluído com sucesso!");
                $state.go("customers_index");
              }
            }
          }
        });

        self.id           = $stateParams.id;
        self.states       = [];
        self.cities       = [];
        self.stores       = [];
        self.user         = $rootScope.user;
        self.selected_state = null;
        self.selected_city  = null;

        if (self.id === undefined) {
          self.model         = new CustomerModel();
          self.customer_type = "pf";
        } else {
          self.model = CustomerModel.$find(self.id).$then(function(data) {
            self.customer_type = data.identification.length === 11 ? "pf" : "pj";
            self._setCities();
          });
        }

        self.autocomplete_state = {
          suggest: function(term) {
            var q = term.toLowerCase().trim();
            var results = [];

            for (var i = 0; i < self.states.length && results.length < 10; i++) {
              var state = self.states[i];
              if (state.name.toLowerCase().indexOf(q) === 0 || state.acronym.toLowerCase().indexOf(q) === 0) {
                results.push({ label: state.name + " (" + state.acronym + ")", value: state.name, id: state.id, cities: state.cities });
              }
            }

            return results;
          },
          on_select: function (selected) {
            self.cities         = selected.cities;
            self.selected_city  = "";
          }
        };

        self.autocomplete_city = {
          suggest: function(term) {
            var q = term.toLowerCase().trim();
            var results = [];

            for (var i = 0; i < self.cities.length && results.length < 10; i++) {
              var city = self.cities[i];
              if (city.name.toLowerCase().indexOf(q) === 0) {
                results.push({ label: city.name, value: city.name, id: city.id });
              }
            }

            return results;
          },
          on_select: function (selected) {
            self.model.city_id = selected.id;
          }
        };

        self.init = function() {
          self.states = StateModel.$search().$then(function() {
            self._setCities();
          });

          if (self.user.type === 'Administrator' && self.user.workplace.type === 'Factory') {
            self.stores = StoreModel.$search();
          }

          self.initRgMask();
        };

        self.initRgMask = function() {
          angular.element("input[data-plugin='masked-input-rg']").inputmask('99.999.999-9|X', {
            showMaskOnHover: false,
            showMaskOnFocus: false,
            placeholder: ' ',
            rightAlign: false,
            autoUnmask: true,
            definitions: {
              "X": {
                validator: "[xX]",
                cardinality: 1,
                casing: "upper"
              }
            }
          });
        };

        self.save = function() {
          if (self.selected_state === undefined || self.selected_state === "") {
            self.model.city_id = null;
          }

          self.model.$save();
        };

        self.openDestroyModal = function() {
          $("#modal_delete_customer").modal();
        };

        self.destroy = function() {
          self.model.$destroy();
        };

        self._setCities = function() {
          if (self.model.state !== undefined && self.states.length > 0) {
            self.selected_state = self.model.state.name;
            self.selected_city  = self.model.city.name;

            var index = self.states.map(function(state) {
              return state.id;
            }).indexOf(self.model.state.id);

            self.cities = self.states[index].cities;
          }
        };

        self.init();
      }
    };
  });