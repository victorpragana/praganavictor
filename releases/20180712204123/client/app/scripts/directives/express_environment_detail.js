'use strict';

angular.module('lussApp')
  .directive('expressEnvironmentDetail', function (LocationModel, OptionModel, Enums, $state) {

    return {
      templateUrl: '/views/directives/express_environment_detail.html',
      restrict: 'E',
      scope: {
        'environment':'=',
        'index':'=',
        'hasOrder':'=',
        'hideDescription':'='
      },
      link: function($scope) {
        var self = $scope;

        self.environmentEnum     = Enums.EnvironmentCategory;
        self.locationEnum        = Enums.LocationCategory;
        self.locations           = [];
        self.subtotal            = 0;

        self.loadLocations = function() {
          self.environment.locations.$search().$then(function(data) {
            self.locations = data;

            angular.forEach(self.locations, function(location) {
              location.options.$search().$then(function(options) {
                location.options = options;

                angular.forEach(location.options, function(option) {
                  option.environment_id = self.environment.id;
                  option.identification = String('00000' + option.environment_id).slice(-5) + '-' + String('000000' + option.id).slice(-6);
                });

                self.calculateLocationTotals(location);
              });
            });
          });
        };

        OptionModel.mix({
          $hooks: {
            'after-destroy': function(response) {
              if (response.status === 204) {
                $state.reload();
              }
            }
          }
        });

        self.init = function() {
          self.location_model = new LocationModel();
          self.location_model.environment_id = self.environment.id;
          self.location_model.category = self.locationEnum.OPENING.id;

          self.loadLocations();
          self.initMaskedInputs();
        };

        self.addLocation = function() {
          self.location_model.$save().$then(function(data) {
            if (data.$response.status === 201) {
              self.location_model = new LocationModel();
              self.location_model.environment_id = self.environment.id;
              self.location_model.category = self.locationEnum.OPENING.id;

              self.loadLocations();

              angular.element("#txtWidth_" + self.index)[0].focus();
            }
          });
        };

        self.validLocation = function() {
          return self.location_model.width && self.location_model.width !== '' &&
            self.location_model.height && self.location_model.height !== '' &&
            self.location_model.quantity && self.location_model.quantity !== '';
        };

        self.updateLocation = function(newName, location) {
          location.name = newName;

          location.$save();
        };

        self.openOptionModal = function(location) {
          $scope.$emit('locationSelected', location, true, true);
        };

        self.calculateSubtotal = function() {
          self.subtotal = 0;

          angular.forEach(self.locations, function(location) {
            self.subtotal += location.total_with_discount;
          });
        };

        self.updateEnvironment = function(newName, environment) {
          environment.name = newName;

          environment.$save();
        };

        self.deleteRecord = function(record) {
          record.$destroy().$then(function(data) {
            if (data.$response.status === 204) {
              $state.reload();
            }
          });
        };

        self.resetRecord = function(record) {
          record.$reset().$then(function(data) {
            if (data.$response.status === 204) {
              $state.reload();
            }
          });
        };

        self.initMaskedInputs = function() {
          angular.element("input[data-plugin='masked-input-decimal']").inputmask("decimal", {
            radixPoint: ',',
            digits: 2,
            rightAlign: false,
            autoUnmask: true,
            unmaskAsNumber: true,
            allowMinus: false
          });

          angular.element("input[data-plugin='masked-input']").inputmask("integer", {
            rightAlign: false,
            autoUnmask: true,
            unmaskAsNumber: true,
            allowMinus: false
          });
        };

        self.questionsData = function(questions) {
          var names = questions.map(function(question) {
            if (typeof(question.value) === 'boolean') {
              return '<strong>' + question.label + ':</strong> ' + (question.value === true ? 'Sim' : 'Não');
            } else {
              return '<strong>' + question.label + ':</strong> ' + question.value;
            }
          });

          return names.join(', ');
        };

        self.componentNames = function(components) {
          var not_required = components.filter(function(component) {
            return component.required === false;
          });

          var names = not_required.map(function(component) {
            return component.component.name;
          });

          return names.join(', ');
        };

        self.calculateLocationTotals = function(location) {
          location.subtotal = location.total_with_discount / location.quantity;

          self.calculateSubtotal();
        };

        self.updateLocationSelection = function(location, $event) {
          $scope.$emit('locationSelected', location, $event.currentTarget.checked, false);
        };

        self.updateDiscount = function(discount, type, location) {
          var calculatedDiscount = 0;

          if (type === 'value') {
            calculatedDiscount = parseFloat(((discount * 100) / location.total).toFixed(2));
          } else {
            calculatedDiscount = discount;
          }

          location.$applyDiscount(calculatedDiscount).$then(function() {
            toastr.remove();
            toastr.success('Desconto aplicado com sucesso');

            $state.reload();
          });

          return false;
        };

        self.init();
      }
    };
  });