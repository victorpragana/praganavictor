'use strict';

angular.module('lussApp')
  .directive('errorToastr', function () {
    return {
      templateUrl: '/views/directives/error_toastr.html',
      restrict: 'E',
      scope: {
        model: "="
      }
    };
  })
  .factory('lussError', function ($compile, $rootScope) {
    return {
      show: function(errors, remove) {
        var _remove = remove === undefined ? true : remove;

        var scope = $rootScope.$new();

        scope.errors = errors;

        var link = $compile('<error-toastr model="errors"></error-toastr>');
        var content = link(scope);

        if (_remove) {
          toastr.remove();
        }

        toastr.warning(content).css('width', '500px');
      }
    };
  });