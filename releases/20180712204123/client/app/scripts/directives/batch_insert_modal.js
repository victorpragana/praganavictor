'use strict';

angular.module('lussApp')
  .directive('batchInsertModal', function (LineModel, Enums, lussError, $state) {
    return {
      templateUrl: '/views/directives/batch_insert_modal.html',
      restrict: 'E',
      scope: {
        model: "="
      },
      link: function(scope) {
        var self = scope;

        self.lines = [];
        self.rules = [];
        self.selectedLine = null;
        self.selectedRule = null;
        self.selectedRules = [];

        self.controlMethodEnum = Enums.ControlMethod;
        self.weightEnum = Enums.Weight;

        LineModel.mix({
          $hooks: {
            'after-update': function(response) {
              if (response.status === 200) {
                toastr.remove();
                toastr.success("Regras importadas com sucesso!");
                
                $state.go($state.current, {}, {reload: true});
              }
            }
          }
        });

        self.init = function() {
          self.lines = [];
          self.rules = [];
          self.selectedLine = null;
          self.selectedRule = null;
          self.selectedRules = [];
        };

        self.save = function() {
          var ruleIds = self.selectedRules.map(function(r) {
            return r.id;
          });

          self.model.$batchInsert(ruleIds).$then(function(response) {
            if (response.$response.data.errors.length > 0) {
              lussError.show(response.$response.data.errors, false);
            } else {
              toastr.remove();
              toastr.success("Regras criadas com sucesso!");
            }

            $state.go($state.current, {}, {reload: true});
          }, function(response) {
            lussError.show(response.$response.data.errors);
          });
        };

        self.setRule = function($item) {
          self.selectedRule = $item;
        };

        self.addRule = function() {
          if (self.selectedRule !== undefined && self.selectedRule !== null) {
            var index = self.selectedRules.map(function(selectedRule) {
              return selectedRule.id;
            }).indexOf(self.selectedRule.id);

            if (index === -1) {
              self.selectedRule.control_method.name = self.controlMethodEnum.findById(self.selectedRule.control_method.id).name;
              self.selectedRule.line = self.selectedLine.name;

              self.selectedRules.push(self.selectedRule);
            }
          }
        };

        self.loadRules = function($item, $model) {
          LineModel.$new($model).$rules().$then(function(response) {
            var rules = response.$response.data.rules;

            self.rules = rules.filter(function(rule) {
              var present = (rule.category_id === undefined || rule.category_id === null || self.model.category_id === rule.category_id);
              var hasComponent = false;

              if (present) {
                hasComponent = rule.rules.some(function(r) {
                  return r.component.id === self.model.id;
                });
              }

              return present && !hasComponent;
            });

            self.selectedLine = $item;
            self.selectedRule = null;
          });
        };

        self.loadLines = function() {
          self.lines = [];
          self.rules = [];
          self.selectedRules = [];
          self.selectedRule = null;

          LineModel.$search({'family_id[]': self.model.family_ids, category_id: self.model.category_id}).$then(function(data) {
            angular.forEach(data, function(line) {
              if (line.id !== self.model.id) {
                self.lines.push({
                  id: line.id,
                  name: line.name
                });
              }
            });

            $("#modal_batch_insert").modal();
          });
        };

        self.removeRule = function(rule) {
          var index = self.selectedRules.map(function(selectedRule) {
            return selectedRule.id;
          }).indexOf(rule.id);

          if (index !== -1) {
            self.selectedRules.splice(index, 1);
          }
        };

        self.openModal = function() {
          App.initSlimScroll('.scroller');

          self.init();
          self.loadLines();
        };
      }
    };
  });