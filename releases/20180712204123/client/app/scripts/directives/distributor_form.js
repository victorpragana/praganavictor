'use strict';

angular.module('lussApp')
  .directive('distributorForm', function (DistributorModel, StateModel, Enums, $stateParams, $state, $rootScope) {
    return {
      templateUrl: '/views/directives/distributor_form.html',
      restrict: 'E',
      scope: {
        'inModal':'=',
        'onCreate': '=?'
      },
      link: function(scope) {
        var self = scope;

        DistributorModel.mix({
          $hooks: {
            'after-create': function(response) {
              if (response.status === 201) {
                if (self.logo) {
                  self.model = DistributorModel.$find(response.data.store.id).$then(function() {
                    self._setCities();
                    self.upload();
                  });  
                } else {
                  self.onSuccessCreate();
                } 
              }
            },
            'after-update': function(response) {
              if (response.status === 200) {
                toastr.remove();
                
                if (self.logo) {
                  self.upload();
                } else {
                  self.onSuccessUpdate();
                }
              }
            },
            'after-destroy': function(response) {
              if (response.status === 204) {
                toastr.remove();
                toastr.success("Distribuidor removido com sucesso!");
                $state.go("distributors_index");
              }
            }
          }
        });

        self.id       = $stateParams.id;
        self.states   = [];
        self.cities   = [];
        self.user     = $rootScope.user;
        self.logo     = null;
        self.banks    = Enums.Bank.enums;
        self.selected_state = null;
        self.selected_city  = null;

        if (self.id === undefined) {
          self.model = new DistributorModel();
        } else {
          self.model = DistributorModel.$find(self.id).$then(function() {
            self._setCities();
          });
        }

        self.autocomplete_state = {
          suggest: function(term) {
            var q = term.toLowerCase().trim();
            var results = [];

            for (var i = 0; i < self.states.length && results.length < 10; i++) {
              var state = self.states[i];
              if (state.name.toLowerCase().indexOf(q) === 0 || state.acronym.toLowerCase().indexOf(q) === 0) {
                results.push({ label: state.name + " (" + state.acronym + ")", value: state.name, id: state.id, cities: state.cities });
              }
            }

            return results;
          },
          on_select: function (selected) {
            self.cities         = selected.cities;
            self.selected_city  = "";
          }
        };

        self.autocomplete_city = {
          suggest: function(term) {
            var q = term.toLowerCase().trim();
            var results = [];

            for (var i = 0; i < self.cities.length && results.length < 10; i++) {
              var city = self.cities[i];
              if (city.name.toLowerCase().indexOf(q) === 0) {
                results.push({ label: city.name, value: city.name, id: city.id });
              }
            }

            return results;
          },
          on_select: function (selected) {
            self.model.city_id = selected.id;
          }
        };

        self.init = function() {
          self.states = StateModel.$search().$then(function() {
            self._setCities();
          });
        };

        self.save = function() {
          if (self.selected_state === undefined || self.selected_state === "") {
            self.model.city_id = null;
          }
          
          self.model.$save();
        };

        self.openDestroyModal = function() {
          $("#modal_delete_distributor").modal();
        };

        self.destroy = function() {
          self.model.$destroy();
        };

        self._setCities = function() {
          if (self.model.state !== undefined && self.states.length > 0) {
            self.selected_state = self.model.state.name;
            self.selected_city  = self.model.city.name;

            var index = self.states.map(function(state) {
              return state.id;
            }).indexOf(self.model.state.id);

            self.cities = self.states[index].cities;
          }
        };

        self.upload = function () {
          if (self.logo !== null) {
            DistributorModel.$uploadLogo(self.model.id, self.logo, self.model).then(function(response) {
              if (!response.data.errors || response.data.errors.length === 0) {
                if(self.id) {
                  self.onSuccessUpdate();
                } else {
                  self.onSuccessCreate();
                }
              } else {
                self.errors = response.data.errors;

                angular.element('.alert').show();
              }
            }, function() {});
          }
        };

        self.onSuccessCreate = function() {
          toastr.remove();
          toastr.success("Distribuidor criado com sucesso!");

          self.model          = new DistributorModel();
          self.selected_state = null;
          self.selected_city  = null;
          self.cities         = [];

          if (self.inModal === false) {
            $state.go($state.current, {}, {reload: true});
          } else {
            if (self.onCreate !== undefined) {
              self.onCreate();
            }

            angular.element('#distributor_modal').modal('toggle');
          }
        };

        self.onSuccessUpdate = function() {
          toastr.success("Distribuidor atualizado com sucesso!");
          $state.go("distributors_index");
        };

        self.logoSource = function() {
          return (self.model && self.model.logo_file_name) ? self.model.logo_url : '/images/drop-logo.jpg';
        };

        self.init();
      }
    };
  });