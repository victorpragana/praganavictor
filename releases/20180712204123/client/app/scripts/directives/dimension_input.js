'use strict';

angular.module('lussApp')
  .directive('dimensionInput', function() {

    return {
      templateUrl: '/views/directives/dimension_input.html',
      restrict: 'E',
      scope: {
        'model':'=',
        'collection':'=',
        'columns':'='
      },
      link: function(scope) {
        var self = scope;

        self.removeDimension = function(model) {
          var index = scope.collection.indexOf(model);

          scope.collection.splice(index, 1);
        };

        self.fieldSize = function() {
          var dimensions = Object.keys(self.columns);

          switch(dimensions.length) {
            case 0: {
              return 'col-sm-1';
            } break;
            case 1: {
              return 'col-sm-1';
            } break;
            case 2: {
              return 'col-sm-2';
            } break;
            case 3: {
              return 'col-sm-3';
            } break;
            case 4: {
              return 'col-sm-4';
            } break;
          }
        };

        $("input[data-plugin='masked-input']").inputmask("decimal", {
          radixPoint: ',',
          digits: 2,
          rightAlign: false,
          autoUnmask: true,
          unmaskAsNumber: true,
          allowMinus: false
        });
      }
    };
  });