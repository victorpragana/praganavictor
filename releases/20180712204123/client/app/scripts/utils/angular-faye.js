'use strict';

angular.module("lussApp").factory("$faye", [
  "$q", "$rootScope", function($q, $rootScope) {
    return function(url, modifiers, fun) {
      if (typeof Faye === 'undefined') {
        console.error("Faye not found! Remote printing will not work");

        return {
          client: null,
          publish: function() {
            return null;
          },
          subscribe: function() {
            return null;
          },
          get: function() {
            return null;
          }
        };
      } else {
        var client, scope;

        scope = $rootScope;

        client = new Faye.Client(url, modifiers);

        if (typeof fun === "function") {
          fun(client);
        }

        return {
          client: client,
          publish: function(channel, data) {
            return this.client.publish(channel, data);
          },
          subscribe: function(channel, callback) {
            return this.client.subscribe(channel, function(data) {
              return scope.$apply(function() {
                return callback(data);
              });
            });
          },
          get: function(channel) {
            var deferred, sub;
            deferred = $q.defer();
            sub = this.client.subscribe(channel, function(data) {
              scope.$apply(function() {
                return deferred.resolve(data);
              });
              return sub.cancel();
            });
            return deferred.promise;
          }
        };
      }
    };
  }
]);
