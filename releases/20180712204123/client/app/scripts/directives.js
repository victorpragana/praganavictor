'use strict';

angular
  .module("lussApp")
  .directive('ngSpinnerBar', function($rootScope) {
    return {
      link: function(scope, element) {
        element.addClass('hide');

        $rootScope.$on('$locationChangeStart', function(event) {
          if (Window.hasUnsavedData()) {
            if (!confirm(Window.confirmationMessage)) {
              event.preventDefault();
            } else {
              element.removeClass('hide');
            }
          }
        });

        $rootScope.$on('$stateChangeSuccess', function() {
          element.addClass('hide');
          $('body').removeClass('page-on-load');
          Layout.setSidebarMenuActiveLink('match');

          setTimeout(function() {
            App.scrollTop();
          }, $rootScope.settings.layout.pageAutoScrollOnLoad);
        });

        $rootScope.$on('$stateNotFound', function() {
          element.addClass('hide');
        });

        $rootScope.$on('$stateChangeError', function() {
          element.addClass('hide');
        });
      }
    };
  })
  .directive('a',
    function() {
      return {
        restrict: 'E',
        link: function(scope, elem, attrs) {
          if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
            elem.on('click', function(e) {
              e.preventDefault();
            });
          }
        }
      };
    })
  .directive('dropdownMenuHover', function() {
    return {
      link: function(scope, elem) {
        elem.dropdownHover();
      }
    };
  })
  .directive("form", function() {
    return {
      restrict: 'E',
      link: function() {
        Window.verifyUnsavedData();
      }
    };
  })
  .directive('ngEnter', function() {
    return function(scope, element, attrs) {
      element.bind("keydown keypress", function(event) {
        if (event.which === 13) {
          scope.$apply(function() {
            scope.$eval(attrs.ngEnter);
          });

          event.preventDefault();
        }
      });
    };
  })
  .directive('selectFirstElement', function() {
    return {
      restrict: 'A',
      require: 'uiSelect',
      link: function(scope, element, attrs, $select) {
        attrs.$observe('disabled', function(val) {
          if (val === false) {
            if ($select.items.length === 1) {
              $select.select($select.items[0]);
            } else {
              var fromKit = $select.items.find(function(item) {
                return item.from_kit === true;
              });

              if (fromKit !== undefined) {
                $select.select(fromKit);
              }
            }
          }
        });
      }
    };
  })
  .directive('selectFirstRadioElement', function($timeout) {
    return {
      restrict: 'A',
      scope: {
        collection: '='
      },
      link: function(scope, element, attrs) {
        attrs.$observe('disabled', function(val) {
          if (val === false) {
            var $radio = null;

            if (scope.collection.length === 1) {
              $radio = angular.element("input[type=radio]#" + attrs.id);

              $timeout(function() {
                $radio.click();
                $radio.triggerHandler('click');
              });
            } else {
              var fromKit = scope.collection.find(function(item) {
                return item.from_kit === true;
              });

              if (fromKit !== undefined) {
                $radio = angular.element("input[type=radio]#" + fromKit.id);

                $timeout(function() {
                  $radio.click();
                  $radio.triggerHandler('click');

                  angular.forEach(scope.collection, function(component) {
                    component.from_kit = false;
                  });
                });
              }
            }
          }
        });
      }
    };
  });