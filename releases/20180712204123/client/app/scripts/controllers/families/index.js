'use strict';

angular
  .module("lussApp")
  .controller("FamilyIndexCtrl", function(FamilyModel, $state) {
    var self = this;

    FamilyModel.mix({
      $hooks: {
        'after-create': function(response) {
          if (response.status === 201) {
            toastr.remove();
            toastr.success("Família criada com sucesso!");
            self.resetForm();
            self.families = FamilyModel.$search();
          }
        },
        'after-update': function(response) {
          if (response.status === 200) {
            toastr.remove();
            toastr.success("Família atualizada com sucesso!");
            self.resetForm();
            self.families = FamilyModel.$search();
          }
        }
      }
    });

    self.families = FamilyModel.$search();
    self.model = new FamilyModel();
    self.model.color = "#000000";

    self.init = function() {
    };

    self.saveFamily = function() {
      self.model.$save();
    };

    self.loadFamily = function(family_id) {
      self.model = FamilyModel.$find(family_id); 
    };

    self.resetForm = function() {
      $state.go($state.current, {}, {reload: true});
      
      self.model = new FamilyModel();
      self.model.color = "#000000";
    };

    self.init();
  });