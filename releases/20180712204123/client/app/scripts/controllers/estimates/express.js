'use strict';

angular
  .module("lussApp")
  .controller("EstimateExpressFormCtrl", function($scope, $stateParams, $state, EstimateModel, EnvironmentModel, OrderModel, Enums,
      EstimateItemModel, ClientModel, ItemModel, lussError, $timeout, $rootScope, $window, $interval, JobStatusModel) {
    var self = this;

    self.estimate_model      = null;
    self.selected_options    = [];
    self.items               = [];
    self.total               = 0;
    self.can_generate_order  = true;
    self.hasSelectedLocation = false;
    self.selected_locations  = [];
    self.modalLocations      = [];
    self.interval            = null;
    self.inProgress          = false;
    self.processingMessage   = null;
    self.workerMessages      = [];
    self.at                  = 0;
    self.pct_complete        = 0;
    self.progress            = 0;
    self.cancelling          = false;
    self.jobId               = null;

    self.loadClients = function() {
      self.clients = ClientModel.$search().$then(function() {
        self._selectClient();
      });
    };

    self.loadEnvironments = function() {
      self.environments = self.estimate_model.environments.$search().$then(function() {
        self.locations = [];
      });
    };

    EstimateModel.mix({
      $hooks: {
        'after-create': function(response) {
          if (response.status === 201) {

            $state.go("estimates_express_edit", {"id": self.estimate_model.id}, {reload: true});
          }
        },
        'after-update': function(response) {
          if (response.status === 200) {
            toastr.remove();
            toastr.success("Orçamento atualizado com sucesso!");

            $state.reload();
          }
        }
      }
    });

    EnvironmentModel.mix({
      $hooks: {
        'after-create': function(response) {
          if (response.status === 201) {
            self.environment_model = new EnvironmentModel();
            self.environment_model.estimate_id = self.id;

            $state.reload();
          }
        },
        'after-destroy': function(response) {
          if (response.status === 204) {
            $state.reload();
          }
        }
      }
    });

    EstimateItemModel.mix({
      $hooks: {
        'after-create': function(response) {
          if (response.status === 201) {
            toastr.remove();
            toastr.success('Produto/Serviço adicionado com sucesso!');

            $state.reload();
          }
        },
        'after-destroy': function(response) {
          if (response.status === 204) {
            toastr.remove();
            toastr.success('Produto/Serviço removido com sucesso!');

            $state.reload();
          }
        }
      }
    });

    JobStatusModel.mix({
      $hooks: {
        'after-destroy': function(response) {
          if (response.status === 204) {
            toastr.remove();
            toastr.success("O processamento será cancelado, por favor aguarde alguns instantes");

            $state.reload();
          }
        }
      }
    });

    self.environmentEnum    = Enums.EnvironmentCategory;
    self.unitEnum           = Enums.Unit;
    self.itemTypeEnum       = Enums.ItemType;
    self.estimateStatusEnum = Enums.EstimateStatus;
    self.environment_model = new EnvironmentModel();

    self.id = $stateParams.id;
    if (self.id === undefined) {
      self.estimate_model = new EstimateModel();
      self.estimate_model.optimized = false;

      self.environments = [];
    } else {
      self.estimate_model = EstimateModel.$find(self.id).$then(function(data) {
        self._selectClient();

        angular.forEach(data.items, function(item) {
          item.total_with_discount = item.total * (1 - (item.discount / 100));
        });

        self.estimate_model.items_total = data.items.reduce(function(total, item) {
          return total + item.total_with_discount;
        }, 0);

        if (data.status === self.estimateStatusEnum.ENQUEUED.id || data.status === self.estimateStatusEnum.PROCESSING.id) {
          var message = 'Por favor aguarde o processamento do orçamento';

          if (self.cancelling === false) {
            message = message + '<br><input type="button" value="Cancelar" data-action="cancel-processing" class="btn btn-warning">';
          }

          App.blockUI({
            target: 'div.portlet',
            boxed: true,
            message: message
          });

          angular.element("input[type=button][data-action='cancel-processing']").on("click", function() {
            JobStatusModel.$new(self.jobId).$destroy();
          });
        }
      });

      self.environment_model.estimate_id = self.id;
      self.loadEnvironments();
      ItemModel.$search().$then(function(items) {
        self.items = items;

        angular.forEach(self.items, function(item) {
          item.type_name = self.itemTypeEnum.findById(item.type).name;
          item.type_color = self.itemTypeEnum.findById(item.type).color;
        });
      });
    }

    self._selectClient = function() {
      if (self.clients !== undefined && self.clients !== null && self.estimate_model !== null) {
        var client = self.clients.filter(function(client) {
          return client.id === self.estimate_model.client_id && client.type === self.estimate_model.client_type;
        })[0];

        self.estimate_model.client = client;
      }
    };

    self.init = function() {
      $('.date-picker').datepicker({
        rtl: App.isRTL(),
        orientation: "left",
        format: "dd/mm/yyyy",
        autoclose: true,
        language: 'pt-BR',
        todayBtn: true,
        todayHighlight: true
      }).datepicker('update', new Date());

      if (self.id === undefined) {
        self.estimate_model.date = angular.element('.date-picker').datepicker().val();
      }

      self.loadClients();

      var offset = 300;
      var duration = 500;

      if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {
        angular.element(window).bind("touchend touchcancel touchleave", function() {
          if (angular.element(this).scrollTop() > offset) {
            angular.element('.floating-button').fadeIn(duration);
          } else {
            angular.element('.floating-button').fadeOut(duration);
          }
        });
      } else {
        angular.element(window).scroll(function() {
          if (angular.element(this).scrollTop() > offset) {
            angular.element('.floating-button').fadeIn(duration);
          } else {
            angular.element('.floating-button').fadeOut(duration);
          }
        });
      }

      self.jobId = $window.localStorage.getItem('estimate_' + self.id);

      if (self.jobId !== undefined && self.jobId !== null) {
        self.workerMessages = [];

        self.interval = $interval(function() {
          JobStatusModel.$find(self.jobId).$then(function(status) {
            self.inProgress = status.in_progress;

            self.cancelling = status.cancelling === 0;

            if (self.cancelling) {
              var $cancelButton = angular.element("input[type=button][data-action='cancel-processing']");

              if ($cancelButton.length !== 0) {
                $cancelButton.val("Cancelando...");
                $cancelButton.attr("disabled", "disabled");
                $cancelButton.off();
              }
            }

            if (self.inProgress) {
              self.at           = status.at;
              self.pct_complete = status.pct_complete;
              self.progress     = status.total;

              var messages = JSON.parse(status.message);

              if (messages !== null) {
                self.workerMessages[Object.keys(messages)[0]] = Object.values(messages)[0];

                self.processingMessage = self.workerMessages.join('<br>');
              }
            } else {
              self.processingMessage = status.message;
              self.workerMessages = [];

              $window.localStorage.removeItem('estimate_' + self.id);

              $interval.cancel(self.interval);

              self.interval = null;

              toastr.remove();
              toastr.info('Processamento concluído');

              self.can_generate_order = true;

              $state.reload();
            }
          });
        }, 2000);
      }
    };

    self.initMaskedInput = function() {
      angular.element("input[data-plugin='masked-input-decimal']").inputmask("decimal", {
        radixPoint: ',',
        digits: 2,
        rightAlign: false,
        autoUnmask: true,
        unmaskAsNumber: true,
        allowMinus: false
      });
    };

    self.readonly = function() {
      return self.estimate_model.order_id !== undefined && self.estimate_model.order_id !== null;
    };

    self.setClient = function($item) {
      if ($item !== undefined && $item !== null) {
        self.estimate_model.client_id = $item.id;
        self.estimate_model.client_type = $item.type;
      } else {
        self.estimate_model.client_id = $item.id;
        self.estimate_model.client_type = $item.type;
      }

      if (self.estimate_model.id !== undefined && self.estimate_model.id !== null) {
        self.saveEstimate();
      }
    };

    self.saveEstimate = function() {
      self.estimate_model.$save();
    };

    self.lockEstimate = function() {
      self.can_generate_order = false;

      toastr.remove();
      toastr.success('Para calcular o valor do orçamento, clique em \'CALCULAR E SALVAR\'');
    };

    self.addEnvironment = function() {
      self.environment_model.$save();
    };

    self.generateOrder = function() {
      if (self.estimate_model.total === 0) {
        toastr.remove();
        toastr.error('Adicione ao menos uma opção');
        return false;
      }

      OrderModel.$generate(self.estimate_model.id, self.selected_options).error(function(data) {
        lussError.show(data.errors);
      }).success(function(response) {
        toastr.remove();
        toastr.success("Pedido gerado com sucesso!");
        $state.go("orders_show", {"id": response.order.id});
      });
    };

    self.duplicateEstimate = function() {
      self.estimate_model.$duplicate().$then(function(response) {
        toastr.remove();
        toastr.success('Orçamento duplicado com sucesso!');
        $state.go('estimates_express_edit', {'id': response.$response.data.estimate.id}, {reload: true});
      }, function(response) {
        lussError.show(response.$response.data.errors);
      });
    };

    self.addItem = function() {
      var estimate_item_model = new EstimateItemModel();

      estimate_item_model.item_id = self.item.id;
      estimate_item_model.estimate_id = self.estimate_model.id;
      estimate_item_model.quantity = self.item_quantity;

      estimate_item_model.$save();
    };

    self.deleteItem = function(item) {
      var estimate_item_model = EstimateItemModel.$new(item.id);

      estimate_item_model.$destroy();
    };


    self.calculateEstimate = function() {
      self.estimate_model.$calculate().$then(function(response) {
        toastr.remove();
        toastr.success('Orçamento marcado para processamento');

        $window.localStorage.setItem('estimate_' + self.id, response.$response.data.estimate.job_id);

        $state.reload();
      });
    };

    self.openOptionModal = function() {
      self.modalLocations = angular.copy(self.selected_locations);

      $('#modal_option').modal();
    };

    self.optionCreatedCallback = function() {
      self.can_generate_order = false;

      toastr.remove();
      toastr.success('Opções criadas com sucesso. Para calcular o valor do orçamento, clique em \'CALCULAR E SALVAR\'');

      angular.forEach(angular.element("input[id^='checkbox_location_'][type=checkbox]:checked"), function(el) {
        $timeout(function() {
          angular.element(el).attr('checked', false);
          angular.element(el).triggerHandler('click');
        });
      });
    };

    self.updateItemDiscount = function(discount, type, item) {
      var calculatedDiscount = 0;

      if (type === 'value') {
        calculatedDiscount = parseFloat(((discount * 100) / item.total).toFixed(2));
      } else {
        calculatedDiscount = discount;
      }

      var estimateItem = EstimateItemModel.$new(item.id);

      estimateItem.$applyDiscount(calculatedDiscount).$then(function() {
        toastr.remove();
        toastr.success('Desconto aplicado com sucesso');

        $state.reload();
      });

      return false;
    };

    self.updateEstimateDiscount = function(discount, type) {
      var calculatedDiscount = 0;

      if (type === 'value') {
        calculatedDiscount = parseFloat(((discount * 100) / self.estimate_model.partial_total).toFixed(2));
      } else {
        calculatedDiscount = discount;
      }

      self.estimate_model.$applyDiscount(calculatedDiscount).$then(function() {
        toastr.remove();
        toastr.success('Desconto aplicado com sucesso');

        $state.reload();
      });

      return false;
    };

    self.discountValue = function() {
      return parseFloat((self.estimate_model.partial_total * (self.estimate_model.discount / 100)).toFixed(2));
    };

    $scope.$on('locationSelected', function($event, location, selected, single_selection) {
      if (single_selection === true) {
        self.selected_locations = [location];

        if (location !== null) {
          self.openOptionModal();
        }
      } else {
        if (selected === true) {
          self.selected_locations.push(location);
        } else {
          var index = self.selected_locations.map(function(_location) {
            return _location.id;
          }).indexOf(location.id);

          if (index !== -1) {
            self.selected_locations.splice(index, 1);
          }
        }
      }
    });

    $rootScope.$on('$stateChangeStart', function() {
      $interval.cancel(self.interval);

      self.interval = null;
    });

    self.init();
  });