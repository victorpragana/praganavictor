'use strict';

angular
  .module("lussApp")
  .controller("DocIndexCtrl", function($scope, $rootScope, $stateParams, $state, $window, $interval, DocumentModel) {
    var self = this;
    
    self._loadModel = function() {
      self.model = DocumentModel.$fetch();
    };

    self.init = function() {
      self._loadModel();
    };

    self.init();
  });