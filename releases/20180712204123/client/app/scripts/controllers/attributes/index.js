'use strict';

angular
  .module("lussApp")
  .controller("AttributeIndexCtrl", function(AttributeModel) {
    var self = this;

    self.attributes = [];

    self.attributes = AttributeModel.$search();

    self.init = function() {
      $(".modal-backdrop").remove();
    };

    self.init();
  });