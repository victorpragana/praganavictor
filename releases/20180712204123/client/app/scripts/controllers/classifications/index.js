'use strict';

angular
  .module("lussApp")
  .controller("ClassificationIndexCtrl", function(ClassificationModel, ipCookie, $filter) {
    var self = this;

    self.$table   = null;

    self.init = function() {
      self.$table = $('#table_classifications').dataTable({
        "language": {
          "aria": {
              "sortAscending": "clique para ordenar de forma crescente",
              "sortDescending": "clique para ordenar de forma decrescente"
          },
          "emptyTable": "Não existem dados para serem exibidos",
          "info": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
          "infoEmpty": "Nenhum registro foi encontrado",
          "infoFiltered": "(filtrado de _MAX_ registros no total)",
          "lengthMenu": "Mostrar _MENU_ registros",
          "search": "Buscar:",
          "zeroRecords": "Não foram encontrados resultados",
          "processing": "Processando...",
        },

        buttons: [
          { extend: 'colvis', className: 'btn dark btn-outline', text: "Colunas"}
        ],

        processing: true,
        serverSide: true,
        searchDelay: 800,
        columnDefs: [
          { targets: 0, name: "id", class: "text-center", width: 20, title: "ID", render: function(data, type, row) {
            return "<a href='/classifications/" + row[0] + "/edit'>" + row[0] + "</a>";
          }},
          { targets: 1, data: 1, title: "Nome" }
        ],

        ajax: {
          url: "/api/commercial_classifications.json",
          headers: ipCookie("auth_headers")
        },

        order: [
          [1, 'asc']
        ],
        
        lengthMenu: [
            [5, 10, 15, 20, 300, -1],
            [5, 10, 15, 20, 300, "Todos"] // change per page values here
        ],

        pageLength: 100,

        dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
      });
    };

    self.init();
  });