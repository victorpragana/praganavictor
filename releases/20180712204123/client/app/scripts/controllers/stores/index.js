'use strict';

angular
  .module("lussApp")
  .controller("StoreIndexCtrl", function(StoreModel, PriceTableModel, ClassificationModel, ipCookie, Enums, $timeout, $scope, lussError, $rootScope) {
    var self = this;

    self.selected = [];
    self.$table = null;
    self.priceTables = [];
    self.priceTableId = null;
    self.priceTableTypeEnum = Enums.PriceTableType;

    self.classifications = [];
    self.classificationId = null;
    self.workplaceTypeEnum = Enums.WorkplaceType;
    self.user = $rootScope.user;

    self.selectAll = function() {
      $("input[type=checkbox]").trigger("click");
    };

    self.init = function() {
      $(".modal-backdrop").remove();

      self.priceTables = PriceTableModel.$search();
      self.classifications = ClassificationModel.$search();

      self.$table = $('#table_stores').dataTable({
        language: {
          aria: {
            sortAscending: "clique para ordenar de forma crescente",
            sortDescending: "clique para ordenar de forma decrescente"
          },
          emptyTable: "Não existem dados para serem exibidos",
          info: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
          infoEmpty: "Nenhum registro foi encontrado",
          infoFiltered: "(filtrado de _MAX_ registros no total)",
          lengthMenu: "Mostrar _MENU_ registros",
          search: "Buscar:",
          zeroRecords: "Não foram encontrados resultados",
          processing: "Processando...",
        },

        buttons: [
          { extend: 'excel', className: 'btn yellow btn-outline ' },
          { extend: 'colvis', className: 'btn dark btn-outline', text: "Colunas"}
        ],

        processing: true,
        serverSide: true,
        searchDelay: 800,
        columnDefs: [
          { targets: 0, orderable: false, searchable: false, width: 5, render: function(data, type, row) {
            return "<div class='md-checkbox-list'><div class='md-checkbox'><input type='checkbox' id='checkbox_" + row[1] + "' value='" + row[1] + "' class='md-check'><label for='checkbox_" + row[1] + "'><span></span><span class='check'></span> <span class='box'></span></label></div>";
          }},
          { targets: 1, name: "id", class: "text-center", width: 20, title: "ID", render: function(data, type, row) {
            return "<a href='/stores/" + row[1] + "/edit'>" + row[1] + "</a>";
          }},
          { targets: 2, data: 2, title: "Nome" },
          { targets: 3, data: 3, title: "CNPJ" },
          { targets: 4, data: 4, title: "Telefone" },
          { targets: 5, data: 5, title: "Subdomínio" },
          { targets: 6, orderable: false, searchable: false, data: 6, title: "Tabelas de Preço (Linhas/Produtos e Serviços)", class: "text-center", render: function(data, type, row) {
            var html = '';

            if (row[7] !== null) {
              html += "<a href='/price_tables/" + row[7] + "/edit'>" + row[6] + " </a>";
            } else {
              html += "Nenhuma";
            }

            html += ' / ';

            if (row[9] !== null) {
              html += "<a href='/price_tables/" + row[9] + "/edit'>" + row[8] + " </a>";
            } else {
              html += "Nenhuma";
            }

            return html;
          }},
          { targets: 7, orderable: false, searchable: false, data: 10, title: "Class. comercial", class: "text-center", render: function(data, type, row) {
            var html = '';

            if (row[11] !== null) {
              if (self.user.workplace.type === self.workplaceTypeEnum.FACTORY.id) {
                html += "<a href='/classifications/" + row[11] + "/edit'>" + row[10] + " </a>";
              } else {
                html += row[10];
              }
            } else {
              html += "Nenhuma";
            }

            return html;
          }}
        ],

        ajax: {
          url: "/api/stores.json",
          headers: ipCookie("auth_headers")
        },

        order: [
        [1, 'asc']
        ],
        
        lengthMenu: [
          [5, 10, 15, 20, 300, -1],
          [5, 10, 15, 20, 300, "Todos"] // change per page values here
        ],
        pageLength: 100,

        dom: "<'row' <'col-md-12 margin-top-15'B>><'row'<'col-md-6 col-sm-12'><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
      });
    };

    self.openDeleteModal = function() {
      $("#modal_delete_stores").modal();
    };

    self.removeSelected = function() {
      StoreModel.$batchDestroy(self.selected).error(function(data) {
        lussError.show(data.errors);
      }).success(function() {
        toastr.remove();
        toastr.success("Lojas removidas com sucesso!");

        self.$table.api().ajax.reload();
      });

      self.selected = [];
    };

    self.openApplyPriceTableModal = function() {
      $("#modal_apply_price_table").modal();
    };

    self.openApplyClassificationModal = function() {
      $("#modal_apply_classification").modal();
    };

    self.applyToSelected = function() {
      if (self.priceTableId !== null) {
        PriceTableModel.$new(self.priceTableId).$applyToStores(self.selected).$then(function() {
          toastr.remove();
          toastr.success("Tabela aplicada com sucesso!");

          self.$table.api().ajax.reload();

          self.selected = [];
        }, function(response) {
          lussError.show(response.$response.data.errors);
        });
      }
    };

    self.applyToSelectedClassification = function() {
      if (self.classificationId !== null) {
        ClassificationModel.$new(self.classificationId).$applyToStores(self.selected).$then(function() {
          toastr.remove();
          toastr.success("Classificação aplicada com sucesso!");

          self.$table.api().ajax.reload();

          self.selected = [];
        }, function(response) {
          lussError.show(response.$response.data.errors);
        });
      }
    };

    angular.element(document).on("change", "input[type=checkbox]", function() {
      if ($(this).prop('checked')) {
        self.selected.push($(this).val());

        $(this).closest('tr').addClass("selected");
      } else {
        self.selected.splice(self.selected.indexOf($(this).val()), 1);

        $(this).closest('tr').removeClass("selected");
      }

      $timeout(function() {
        $scope.$digest();
      });
    });

    self.init();
  });