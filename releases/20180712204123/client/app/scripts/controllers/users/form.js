'use strict';

angular
  .module("lussApp")
  .controller("UsersFormCtrl", function($stateParams, $state, $rootScope, UserModel, WorkplaceModel, ErpModel, Enums) {
    var self = this;

    self.sellers = ErpModel.sellers.$search();
    self.workplaces = [];

    WorkplaceModel.$search().$then(function(workplaces) {
      self.workplaces = workplaces.map(function(workplace) {
        return {
          workplace_id: workplace.id,
          workplace_type: workplace.type,
          workplace_name: workplace.name
        }
      });
    });

    UserModel.mix({
      $hooks: {
        'after-create': function(response) {
          if (response.status === 201) {
            toastr.remove();
            toastr.success("Usuário criado com sucesso!");

            $state.go($state.current, {}, {reload: true});
            
            self.model = new UserModel();
          }
        },
        'after-update': function(response) {
          if (response.status === 200) {
            toastr.remove();
            toastr.success("Usuário atualizado com sucesso!");
            $state.go($state.current, {}, {reload: true});
          }
        },
        'after-destroy': function(response) {
          if (response.status === 204) {
            toastr.remove();
            toastr.success("Usuário excluído com sucesso!");
            $state.go("users_index", {}, {reload: true});
          }
        }
      }
    });

    self.id = $stateParams.id;
    self.userTypeEnum = Enums.UserType;
    self.user = $rootScope.user;

    if (self.id === undefined) {
      self.model = new UserModel();
      self.model.workplaces = [];
    } else {
      self.model = UserModel.$find(self.id);
    }

    self.init = function() {
      angular.element("input[data-plugin='masked-input-decimal']").inputmask("decimal", {
        radixPoint: ',',
        digits: 2,
        rightAlign: false,
        autoUnmask: true,
        unmaskAsNumber: true,
        allowMinus: false,
        min: 0,
        max: 100
      });
    };
		
    self.saveUser = function() {
      self.model.$save();
    };

    self.openDestroyModal = function() {
      $("#modal_delete_user").modal();
    };

    self.destroy = function() {
      self.model.$destroy();
    };

    self.notOperator = function() {
      return self.userTypeEnum.sellers.find(function(type) {
        return self.model.type === type.id;
      });
    };

    self.workplaceData = function(workplace) {
      return workplace.workplace_id + workplace.type
    };

    self.filterUserTypes = function() {
      if (self.user.workplace.type === 'Factory') {
        return self.userTypeEnum.enums;
      } else {
        return self.userTypeEnum.sellers;
      }
    };

    self.filterWorkplaces = function() {
      if (self.notOperator()) {
        return self.workplaces;
      } else {
        return self.workplaces.filter(function(workplace) {
          return workplace.workplace_type === 'Factory';
        });
      }
    }

    self.init();
  });