'use strict';

angular
  .module("lussApp")
  .controller("PrinterIndexCtrl", function(PrinterModel, ipCookie) {
    var self = this;

    self.$table = null;
    self.lastVersion = null;
    self.onlineServers = null;

    self.init = function() {
      self.onlineServers = [];

      self.$table = $('#table_printers').dataTable({
        "language": {
          "aria": {
              "sortAscending": "clique para ordenar de forma crescente",
              "sortDescending": "clique para ordenar de forma decrescente"
          },
          "emptyTable": "Não existem dados para serem exibidos",
          "info": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
          "infoEmpty": "Nenhum registro foi encontrado",
          "infoFiltered": "(filtrado de _MAX_ registros no total)",
          "lengthMenu": "Mostrar _MENU_ registros",
          "search": "Buscar:",
          "zeroRecords": "Não foram encontrados resultados",
          "processing": "Processando...",
        },

        buttons: [  
          { extend: 'colvis', className: 'btn dark btn-outline', text: "Colunas"}
        ],

        processing: true,
        serverSide: true,
        searchDelay: 800,
        columnDefs: [
          { targets: 0, name: "id", class: "text-center", width: 20, title: "ID", render: function(data, type, row) {
            return "<a href='/printers/" + row[0] + "/edit'>" + row[0] + "</a>";
          }},
          { targets: 1, data: 1, title: "Nome", render: function(data, type, row) {
            var html = $('<span>').html(row[1]);

            if (row[5] === true) {
              html.append($('<label>').addClass('label label-success pull-right').html('Padrão'));
            }

            return html[0].outerHTML;
          } },
          { targets: 2, data: 2, title: "Impressora" },
          { targets: 3, data: 3, title: "Servidor" }
        ],

        ajax: {
          url: "/api/printers.json",
          headers: ipCookie("auth_headers")
        },

        order: [
          [1, 'asc'],
          [3, 'asc']
        ],
        
        lengthMenu: [
          [5, 10, 15, 20, 300, -1],
          [5, 10, 15, 20, 300, "Todos"] // change per page values here
        ],

        pageLength: 100,

        dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
      });

      PrinterModel.lastVersion().then(function(response) {
        self.lastVersion = response.data.trim();
      });

      PrinterModel.showVersions(function(data) {
        var found = self.onlineServers.map(function(server) {
          return server.server_id;
        }).indexOf(data.server_id);

        if (found === -1) {
          self.onlineServers.push(data);
        }

        angular.element('.tooltips').tooltip();
      });
    };

    self.init();
  });