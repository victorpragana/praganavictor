'use strict';

angular
  .module("lussApp")
  .controller("LineIndexCtrl", function(LineModel, ColorUtils, ipCookie, $scope, $compile) {
    var self = this;

    self.lines = [];
    self.type = null;
    self.$table = null;

    self.init = function() {
      $(".modal-backdrop").remove();

      self.filter(null);

      self.$table = $('#table_lines').DataTable({
        "language": {
          "aria": {
            "sortAscending": "clique para ordenar de forma crescente",
            "sortDescending": "clique para ordenar de forma decrescente"
          },
          "emptyTable": "Não existem dados para serem exibidos",
          "info": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
          "infoEmpty": "Nenhum registro foi encontrado",
          "infoFiltered": "(filtrado de _MAX_ registros no total)",
          "lengthMenu": "Mostrar _MENU_ registros",
          "search": "Buscar:",
          "zeroRecords": "Não foram encontrados resultados",
          "processing": "Processando...",
        },

        buttons: [{
          action: function() {
            self.filter(null);
          },
          className: 'actions btn btn-default',
          text: "TODAS"
        }, {
          action: function() {
            self.filter(true);
          },
          className: 'actions btn btn-success',
          text: "APENAS ATIVAS"
        }, {
          action: function() {
            self.filter(false);
          },
          className: 'actions btn yellow',
          text: "APENAS INATIVAS"
        }, {
          action: function() {
            self.newLine();
          },
          className: 'btn blue',
          text: "<i class='fa fa-plus'></i> NOVA"
        }],

        processing: true,
        serverSide: true,
        searchDelay: 800,
        columnDefs: [{
          targets: 0,
          name: "id",
          class: "text-center",
          width: "10%",
          data: 0,
          title: "ID",
          render: function(data, type, row) {
            return "<a href='/lines/" + row[0] + "/edit'>#" + row[0] + "</a>";
          }
        }, {
          targets: 1,
          title: "Família",
          class: "text-center",
          width: "10%",
          render: function(data, type, row) {
            return '<span class="label margin-right-10" style="background-color: ' + row[6] + '">' + row[1] + "</span>";
          }
        }, {
          targets: 2,
          name: "line",
          class: "text-left",
          data: 2,
          title: "Linha"
        }, {
          targets: 3,
          name: "updated_at",
          class: "text-center",
          width: "15%",
          title: "Data de alteração",
          render: function(data, type, row) {
            return row[3];
          }
        }, {
          targets: 4,
          name: "finished",
          class: "text-center",
          width: "15%",
          title: "Finalizado",
          render: function(data, type, row) {
            return row[4] ? "Sim" : "Não";
          }
        }, {
          targets: 5,
          name: "status",
          class: "text-center",
          width: "15%",
          title: "Status",
          render: function(data, type, row) {
            return row[5] ? "Ativo" : "Inativo";;
          }
        }],

        ajax: {
          url: "/api/lines.json",
          headers: ipCookie("auth_headers"),
          data: function(d) {
            d.active = self.type;
          }
        },

        "order": [
          [1, 'asc']
        ],

        "lengthMenu": [
          [5, 10, 15, 20, 300, -1],
          [5, 10, 15, 20, 300, "Todos"] // change per page values here
        ],
        "pageLength": 100,

        "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
      });
    };

    self.filter = function(type) {
      self.type = type;

      if (self.$table !== null) {
        self.$table.ajax.reload();
      }
    };

    self.newLine = function() {
      window.location = "/lines/new"
    }

    self.init();
  });