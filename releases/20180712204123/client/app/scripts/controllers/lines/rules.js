'use strict';

angular
  .module("lussApp")
  .controller("LineRulesCtrl", function(LineModel, RuleModel, Enums, ArrayUtils, $scope, $stateParams, $state, $filter, $timeout) {
    var self = this;

    self.loadRules = function(id, callback) {
      LineModel.$new(id).$rules().$then(function(response) {
        self.model.rules = response.$response.data.rules;

        angular.forEach(self.model.rules, function(rule) {
          rule.control_method.name = self.controlMethodEnum.findById(rule.control_method.id).name;
        });

        if (callback !== undefined) {
          callback();
        }
      });
    };

    self.loadModel = function(callback) {
      self.model = LineModel.$find(self.id).$then(function(data) {
        self.loadRules(self.id, callback);

        self.init();
      });
    };

    self.id = $stateParams.id;

    self.loadModel();
    
    self.components                 = self.model.components.$search();
    self.controlMethodEnum          = Enums.ControlMethod;
    self.weightEnum                 = Enums.Weight;
    
    // View objects
    self.selectedComponent          = null;
    self.selectedControlMethod      = null;
    self.selectedCategoryId         = null;
    self.selectedWeight             = self.weightEnum.GENERAL.id;
    self.selectedRule               = {};
    self.initial_width              = 0;
    self.final_width                = 0;
    self.initial_height             = 0;
    self.final_height               = 0;
    self.initial_area               = 0;
    self.final_area                 = 0;
    self.widthConsumptionFormula    = "";
    self.heightConsumptionFormula   = "";
    self.totalConsumptionFormula    = "";
    self.quantityConsumptionFormula = "";
    self.costFormula                = "";
    self.required                   = false;
    self.automatic                  = false;
    self.description                = null;

    self.consumptionShow            = true;
    self.ruleIndex                  = null;
    self.ruleId                     = null;
    self.componentIds               = [];

    self.totalConsumptionVariable = null;
    self.totalConsumptionOptions = [];
    self.popoverTotalConsumptionText = "";

    self.costVariable = null;
    self.costOptions = [];
    self.popoverCostText = "";

    self.heightConsumptionVariable = null;
    self.heightConsumptionOptions = [];
    self.popoverHeightConsumptionText = "";

    self.widthConsumptionVariable = null;
    self.widthConsumptionOptions = [];
    self.popoverWidthConsumptionText = "";

    self.quantityConsumptionVariable = null;
    self.quantityConsumptionOptions = [];
    self.popoverQuantityConsumptionText = "";

    self.addRule = function() {
      if (!self.validate()) {
        return;
      }

      var rule                      = {};
      rule.control_method           = {};
      rule.id                       = self.ruleId;

      rule.control_method.id        = self.selectedControlMethod.id;
      rule.control_method.name      = self.selectedControlMethod.name;
      rule.initial_width            = self.initial_width;
      rule.final_width              = self.final_width;
      rule.initial_height           = self.initial_height;
      rule.final_height             = self.final_height;
      rule.initial_area             = self.initial_area;
      rule.final_area               = self.final_area;
      rule.width_consumption        = self.widthConsumptionFormula;
      rule.height_consumption       = self.heightConsumptionFormula;
      rule.quantity_consumption     = self.quantityConsumptionFormula;
      rule.required                 = self.required;
      rule.automatic                = self.automatic;
      rule.category_id              = self.selectedCategoryId;
      rule.weight                   = self.selectedWeight;

      if (rule.automatic) {
        self.costFormula             = "_CUSTO_BASE_";
        self.totalConsumptionFormula = "1";
      } else {
        if (self.costFormula === "") {
          self.costFormula = "_CUSTO_BASE_";
        }
      }

      rule.total_consumption        = self.totalConsumptionFormula;
      rule.cost                     = self.costFormula;
      rule.description              = self.description;
      rule.component_ids            = self.componentIds;

      if (self.ruleIndex === null) {
        self.model.rules.push(rule);
      } else {
        self.model.rules[self.ruleIndex] = rule;
      }

      self.setRules(function() {
        self.ruleId                     = null;
        self.ruleIndex                  = null;
        self.componentIds               = [];
        self.selectedControlMethod      = null;
        self.selectedCategoryId         = null;
        self.initial_width              = 0;
        self.final_width                = 0;
        self.initial_height             = 0;
        self.final_height               = 0;
        self.initial_area               = 0;
        self.final_area                 = 0;
        self.required                   = false;
        self.automatic                  = false;
        self.description                = null;
        self.widthConsumptionFormula    = "";
        self.quantityConsumptionFormula = "";
        self.heightConsumptionFormula   = "";
        self.totalConsumptionFormula    = "";
        self.costFormula                = "";
      }, function() {
        var index = self.model.rules.indexOf(rule);
        
        self.model.rules.splice(index, 1);
      });
    };

    self.removeRule = function(rule) {
      if(confirm("Tem certeza que deseja apagar essa regra?")) {
        var index = self.model.rules.indexOf(rule);
        self.model.rules.splice(index, 1);

        self.setRules();
      }
    };

    self.cloneRule = function(rule) {
      if(confirm("Tem certeza que deseja duplicar essa regra?")) {
        self.model.$duplicateRule(rule.id).$then(function() {
          toastr.remove();
          toastr.success("Regra duplicada com sucesso!");

          $state.go($state.current, {}, {reload: true});
        }, function() {
          toastr.remove();
          toastr.error("Erro ao duplicar a regra");
        });
      }
    }

    self.editRule = function(rule) {
      var index = self.model.rules.indexOf(rule);

      self.ruleIndex                  = index;
      self.ruleId                     = rule.id;
      self.componentIds               = rule.component_ids;
      self.selectedControlMethod      = {};
      self.selectedControlMethod.id   = rule.control_method.id;
      self.selectedControlMethod.name = rule.control_method.name;
      self.initial_width              = rule.initial_width;
      self.final_width                = rule.final_width;
      self.initial_height             = rule.initial_height;
      self.final_height               = rule.final_height;
      self.initial_area               = rule.initial_area;
      self.final_area                 = rule.final_area;
      self.widthConsumptionFormula    = rule.width_consumption;
      self.heightConsumptionFormula   = rule.height_consumption;
      self.quantityConsumptionFormula = rule.quantity_consumption;
      self.required                   = rule.required;
      self.automatic                  = rule.automatic;
      self.totalConsumptionFormula    = rule.total_consumption;
      self.costFormula                = rule.cost;
      self.description                = rule.description;
      self.selectedCategoryId         = rule.category_id;
      self.selectedWeight             = rule.weight;
    };

    self.setRules = function(callback, error_callback) {
      self.model.$setRules(self.model.rules).$then(function() {
        toastr.remove();
        toastr.success("Regras salvas com sucesso!");

        self.loadRules(self.id)

        if (callback !== undefined) {
          callback();
        }
      }, function() {
        if (error_callback !== undefined) {
          error_callback();
        }
      });
    };

    self.validate = function() {
      if (self.selectedControlMethod === null) {
        toastr.warning("Selecione um método de controle.");
        return false; 
      }

      if (self.selectedWeight === null) {
        toastr.warning("Selecione um peso.");
        return false; 
      }

      if (!self.automatic) {
        switch(self.selectedControlMethod.id) {
          case self.controlMethodEnum.WIDTH.id: {
            if (self.initial_width < 0 || self.final_width < 0) {
              toastr.warning("Preencha a informação de largura.");
              return false; 
            }
          } break;
          case self.controlMethodEnum.HEIGHT.id: {
            if (self.initial_height < 0 || self.final_height < 0) {
              toastr.warning("Preencha a informação de altura.");
              return false; 
            }
          } break;
          case self.controlMethodEnum.AREA.id: {
            if (self.initial_width < 0 || self.final_width < 0 || self.initial_height < 0 || self.final_height < 0) {
              toastr.warning("Preencha as informações de altura e largura.");
              return false; 
            }
          } break;
          case self.controlMethodEnum.TOTAL_AREA.id: {
            if (self.initial_area < 0 || self.final_area < 0) {
              toastr.warning("Preencha a informação de área total.");
              return false; 
            }
          } break;
        }

        if (self.totalConsumptionFormula === "") {
          toastr.warning("Preencha o consumo total.");
          return false; 
        }
      }

      return true;
    };

    self.initMaskedInputs = function() {
      angular.element("input[data-plugin='masked-input']").inputmask("decimal", {
        radixPoint: ',',
        digits: 2,
        rightAlign: false,
        autoUnmask: true,
        unmaskAsNumber: true,
        allowMinus: false
      });
    };

    self.init = function() {
      $(".modal-backdrop").remove();
      
      self.initPopovers();
      self.initMaskedInputs();
      self.initTotalConsumptionFormulaFields();
      self.initCostFormulaFields();
      self.initHeightConsumptionFormulaFields();
      self.initWidthConsumptionFormulaFields();
      self.initQuantityConsumptionFormulaFields();
    };

    self.initPopovers = function() {
      $(".popovers").popover();
    };

    self.initHeightConsumptionFormulaFields = function() {
      self.heightConsumptionOptions = Enums.CalculationMethod.forHeightConsumption;

      angular.forEach(self.model.questions, function(question) {
        self.heightConsumptionOptions.push({
          name: question.label,
          variable: question.variable
        });
      });

      self.popoverHeightConsumptionText = "Nesse campo você pode inserir uma fórmula para cálculo da quantidade utilizada de cada componente.";
    };

    self.addHeightConsumptionVariable = function() {
      var input = angular.element("#rule_height_consumption");

      var index = input.prop("selectionStart");

      var field = self.heightConsumptionVariable.variable;

      self.heightConsumptionFormula = self.heightConsumptionFormula.slice(0, index) + field + self.heightConsumptionFormula.slice(index);
      
      $timeout(function() {
        self.heightConsumptionVariable = null;
        input[0].setSelectionRange((index + field.length), (index + field.length));
        input.focus();
      });
    };

    self.initWidthConsumptionFormulaFields = function() {
      self.widthConsumptionOptions = Enums.CalculationMethod.forWidthConsumption;

      angular.forEach(self.model.questions, function(question) {
        self.widthConsumptionOptions.push({
          name: question.label,
          variable: question.variable
        });
      });

      self.popoverWidthConsumptionText = "Nesse campo você pode inserir uma fórmula para cálculo da quantidade utilizada de cada componente";
    };

    self.addWidthConsumptionVariable = function() {
      var input = angular.element("#rule_width_consumption");

      var index = input.prop("selectionStart");

      var field = self.widthConsumptionVariable.variable;

      self.widthConsumptionFormula = self.widthConsumptionFormula.slice(0, index) + field + self.widthConsumptionFormula.slice(index);
      
      $timeout(function() {
        self.widthConsumptionVariable = null;
        input[0].setSelectionRange((index + field.length), (index + field.length));
        input.focus();
      });
    };

    self.initQuantityConsumptionFormulaFields = function() {
      self.quantityConsumptionOptions = Enums.CalculationMethod.forQuantityConsumption;

      angular.forEach(self.model.questions, function(question) {
        self.quantityConsumptionOptions.push({
          name: question.label,
          variable: question.variable
        });
      });

      self.popoverQuantityConsumptionText = "Nesse campo você pode inserir uma fórmula para cálculo da quantidade utilizada de cada componente.";
    };

    self.addQuantityConsumptionVariable = function() {
      var input = angular.element("#rule_quantity_consumption");

      var index = input.prop("selectionStart");

      var field = self.quantityConsumptionVariable.variable;

      self.quantityConsumptionFormula = self.quantityConsumptionFormula.slice(0, index) + field + self.quantityConsumptionFormula.slice(index);
      
      $timeout(function() {
        self.quantityConsumptionVariable = null;
        input[0].setSelectionRange((index + field.length), (index + field.length));
        input.focus();
      });
    };

    self.initTotalConsumptionFormulaFields = function() {
      self.totalConsumptionOptions = Enums.CalculationMethod.forTotalConsumption;

      angular.forEach(self.model.questions, function(question) {
        self.totalConsumptionOptions.push({
          name: question.label,
          variable: question.variable
        });
      });

      self.popoverTotalConsumptionText = "Nesse campo você pode inserir uma fórmula para cálculo da quantidade utilizada de cada componente.";
    };

    self.addTotalConsumptionVariable = function() {
      var input = angular.element("#rule_total_consumption");

      var index = input.prop("selectionStart");

      var field = self.totalConsumptionVariable.variable;

      self.totalConsumptionFormula = self.totalConsumptionFormula.slice(0, index) + field + self.totalConsumptionFormula.slice(index);
      
      $timeout(function() {
        self.totalConsumptionVariable = null;
        input[0].setSelectionRange((index + field.length), (index + field.length));
        input.focus();
      });
    };

    self.initCostFormulaFields = function() {
      self.costOptions = Enums.CalculationMethod.forCost;

      angular.forEach(self.model.questions, function(question) {
        self.costOptions.push({
          name: question.label,
          variable: question.variable
        });
      });

      self.popoverCostText = "Nesse campo você pode inserir uma fórmula para cálculo do custo de cada componente.";
    };

    self.addCostVariable = function() {
      var input = angular.element("#rule_cost");

      var index = input.prop("selectionStart");

      var field = self.costVariable.variable;

      self.costFormula = self.costFormula.slice(0, index) + field + self.costFormula.slice(index);
      
      $timeout(function() {
        self.costVariable = null;
        input[0].setSelectionRange((index + field.length), (index + field.length));
        input.focus();
      });
    };

    self.openAddComponentModal = function(rule) {
      self.selectedRule = rule;

      $timeout(function() {
        $("#modal_add_components").modal();
      });
    };
  });