'use strict';

angular
  .module("lussApp")
  .controller("HeaderController", function($scope, $state, $window, $auth, $rootScope, UserModel, Enums) {
    var self = this;

    self.workplaceEnum = Enums.WorkplaceType;
    self.user = UserModel.$find($rootScope.user.id);

    $scope.$on('$includeContentLoaded', function() {
      Layout.initHeader();
    });

    self.logout = function() {
      $auth.signOut().then(function() {
        $state.go("login");
      });
    };

    self.changeWorkplace = function(workplace) {
      var currentSubdomain = $window.location.host.split('.')[0];
      var destinationUrl = $window.location.href.replace(currentSubdomain, workplace.subdomain);

      $window.location.replace(destinationUrl);
    };
  });