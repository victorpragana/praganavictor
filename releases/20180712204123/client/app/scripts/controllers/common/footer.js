'use strict';

angular
  .module("lussApp")
  .controller("FooterController", function($scope) {
    $scope.$on('$includeContentLoaded', function() {
      Layout.initFooter();
    });

    $scope.today = new Date();
  });