'use strict';

angular
  .module("lussApp")
  .factory("ClassificationModel", function(restmod, RMUtils, $state, lussError) {
    return restmod.model('/api/commercial_classifications').mix({
      $extend: {
        Record: {
          $applyToDistributors: function(ids) {
            return this.$send({
              method: 'POST',
              url: RMUtils.joinUrl(this.$url(), "distributors/apply"),
              data: {"ids": ids}
            });
          },
          $applyToStores: function(ids) {
            return this.$send({
              method: 'POST',
              url: RMUtils.joinUrl(this.$url(), "stores/apply"),
              data: {"ids": ids}
            });
          }
        }
      },
      $hooks: {
        'after-request-error': function(response) {
          switch(response.status) {
            case 422: {
              lussError.show(response.data.errors);
            } break;
            case 400: {
              lussError.show(response.data.errors);
            } break;
            case 401: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            case 404: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            default: {
              toastr.error("Erro interno do sistema.");
            }
          }
        }
      }
    });
  });