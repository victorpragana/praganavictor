'use strict';

angular
  .module("lussApp")
  .factory("LocationModel", function(restmod, $state, lussError, RMUtils) {
    return restmod.model('/api/locations').mix({
      options: { 
        hasMany: 'OptionModel'
      },
      $extend: {
        Record: {
          $reset: function() {
            return this.$send({
              method: 'DELETE',
              url: RMUtils.joinUrl(this.$url(), 'reset')
            });
          },
          $applyDiscount: function(discount) {
            return this.$send({
              method: 'PATCH',
              url: RMUtils.joinUrl(this.$url(), 'apply_discount'),
              data: {"discount": discount}
            });
          }
        }
      },
      $hooks: {
        'after-request-error': function(response) {
          switch(response.status) {
            case 422: {
              lussError.show(response.data.errors);
            } break;
            case 400: {
              lussError.show(response.data.errors);
            } break;
            case 401: {
              toastr.remove();
              toastr.warning('Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.');
              $state.go('dashboard');
            } break;
            case 404: {
              toastr.remove();
              toastr.warning('Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.');
              $state.go('dashboard');
            } break;
            default: {
              toastr.error('Erro interno do sistema.');
            }
          }
        }
      }
    });
  });