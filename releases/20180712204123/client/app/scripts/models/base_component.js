'use strict';

angular
  .module("lussApp")
  .factory("BaseComponentModel", function(restmod, RMUtils, $http, $state, lussError) {
    return restmod.model('/api/base_components').mix({
      $extend: {
        Model: {
          $destroy: function(base_component_id) {
            return $http.delete(RMUtils.joinUrl(this.$url(), base_component_id));
          }
        },
        Record: {
          $reallyDestroy: function() {
            return this.$send({
              method: 'DELETE',
              url: RMUtils.joinUrl(this.$url(), 'really_destroy')
            });
          },
          $activate: function(includeChildren) {
            return this.$send({
              method: 'PATCH',
              url: RMUtils.joinUrl(this.$url(), 'activate'),
              data: {
                "include_children": includeChildren
              }
            });
          }
        }
      },
      $hooks: {
        'after-request-error': function(response) {
          switch (response.status) {
            case 422:
              {
                lussError.show(response.data.errors);
              }
              break;
            case 400:
              {
                lussError.show(response.data.errors);
              }
              break;
            case 401:
              {
                toastr.remove();
                toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
                $state.go('dashboard');
              }
              break;
            case 404:
              {
                toastr.remove();
                toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
                $state.go('dashboard');
              }
              break;
            default:
              {
                toastr.error("Erro interno do sistema.");
              }
          }
        }
      }
    });
  });