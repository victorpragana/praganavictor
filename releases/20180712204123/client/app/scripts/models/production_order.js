'use strict';

angular
  .module("lussApp")
  .factory("ProductionOrderModel", function(restmod, RMUtils, $http, $state, lussError) {
    return restmod.model('/api/production_orders').mix({
      $extend: {
        Model: {
          $purchasePlan: function(order_ids) {
            return $http.get(RMUtils.joinUrl(this.$url(), 'purchase_planning'),
              {
                params: {"order_ids[]": order_ids}
              }
            );
          }
        }
      },
      $hooks: {
        'after-request-error': function(response) {
          switch(response.status) {
            case 422: {
              lussError.show(response.data.errors);
            } break;
            case 400: {
              lussError.show(response.data.errors);
            } break;
            case 401: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            case 404: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            default: {
              toastr.error("Erro interno do sistema.");
            }
          }
        }
      }
    });
  });