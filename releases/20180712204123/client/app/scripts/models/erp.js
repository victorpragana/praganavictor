'use strict';

angular
  .module("lussApp")
  .factory("ErpModel", function(restmod, $state, lussError) {
    return restmod.model().mix({
      sellers: { hasMany: restmod.model().mix({
        $config: {
          name: 'seller'
        }
      }), path: 'sellers' },
      categoryLevels: { hasMany: restmod.model().mix({
        $config: {
          name: 'category_level'
        }
      }), path: 'category_levels' },
      productCategories: { hasMany: restmod.model().mix({
        $config: {
          name: 'product_category'
        }
      }), path: 'product_categories' },
      paymentConditions: { hasMany: restmod.model().mix({
        $config: {
          name: 'payment_condition'
        }
      }), path: 'payment_conditions' },
      $hooks: {
        'after-request-error': function(response) {
          switch(response.status) {
            case 422: {
              lussError.show(response.data.errors);
            } break;
            case 400: {
              lussError.show(response.data.errors);
            } break;
            case 401: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            case 404: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            default: {
              toastr.error("Erro interno do sistema.");
            }
          }
        }
      }
    }).single('api/erp');
  });