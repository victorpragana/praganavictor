'use strict';

angular
  .module("lussApp")
  .factory("OptionModel", function(restmod, RMUtils, $http, $state, lussError) {
    return restmod.model('/api/options').mix({
      $extend: {
        Model: {
          $calculatePrice: function(line_id, component_id, width, height) {
            return $http.get("/api/lines/" + line_id + "/components/" + component_id + "/price", 
              {params: {width: width, height: height}}
            );
          }
        }
      },
      $hooks: {
        'after-request-error': function(response) {
          switch(response.status) {
            case 422: {
              var errors = ['Opção de ' + response.data.width + 'cm x ' + response.data.height + 'cm'];

              lussError.show(errors.concat(response.data.errors), false);
            } break;
            case 400: {
              lussError.show(response.data.errors);
            } break;
            case 401: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            case 404: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            default: {
              toastr.error("Erro interno do sistema.");
            }
          }
        }
      }
    });
  });