'use strict';

angular
  .module("lussApp")
  .factory("DistributorModel", function(restmod, RMUtils, $http, $state, lussError, Upload) {
    return restmod.model('/api/distributors').mix({
      $configurePriceTable: function(price_table) {
        return this.$send({
          method: 'POST',
          url: RMUtils.joinUrl(this.$url(), 'configure_price_table'),
          data: price_table
        }, function() {
        });
      },
      price_tables: { 
        hasMany: 'PriceTableModel'
      },
      $extend: {
        Model: {
          $batchDestroy: function(ids) {
            return $http.delete(RMUtils.joinUrl(this.$url(), "batch_destroy"), {
              params: {"ids[]": ids}
            });
          },
          $uploadLogo: function(id, file) {
            return Upload.upload({

              url: RMUtils.joinUrl(this.$url(), id + "/upload_logo"),
              method: 'PUT',
              data: { file: file }
            });
          }
        }
      },
      $hooks: {
        'after-request-error': function(response) {
          switch(response.status) {
            case 422: {
              lussError.show(response.data.errors);
            } break;
            case 400: {
              lussError.show(response.data.errors);
            } break;
            case 401: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            case 404: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            default: {
              toastr.error("Erro interno do sistema.");
            }
          }
        }
      }
    });
  });