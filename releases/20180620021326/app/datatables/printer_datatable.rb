class PrinterDatatable < AjaxDatatablesRails::Base
  def sortable_columns
    @sortable_columns ||= %w{Printer.id Printer.name Printer.real_name Printer.server_name}
  end

  def searchable_columns
    @searchable_columns ||= %w{Printer.id Printer.name Printer.real_name Printer.server_name}
  end

  private

  def data
    records.map do |record|
      [
        record.id,
        record.name,
        record.real_name,
        record.server_name,
        record.server_id,
        record.default?
      ]
    end
  end

  def get_raw_records
    Printer.all
  end
end
