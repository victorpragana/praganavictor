class PriceTableDatatable < AjaxDatatablesRails::Base
  def_delegator :@view, :current_administrator

  def sortable_columns
    @sortable_columns ||= %w{PriceTable.id PriceTable.name PriceTable.type}
  end

  def searchable_columns
    @searchable_columns ||= %w{PriceTable.id PriceTable.name PriceTable.type}
  end

  private

  def data
    records.map do |record|
      [
        "",
        record.id,
        record.name,
        PriceTable.types[record.type],
        record.default?
      ]
    end
  end

  def get_raw_records
    current_administrator.workplace.price_tables.order(:type, :name)
  end
end
