class CommercialClassificationDatatable < AjaxDatatablesRails::Base
  def sortable_columns
    @sortable_columns ||= %w{CommercialClassification.id CommercialClassification.name}
  end

  def searchable_columns
    @searchable_columns ||= %w{CommercialClassification.id CommercialClassification.name}
  end

  private
  def data
    records.map do |record|
      [
        record.id,
        record.name
      ]
    end
  end

  def get_raw_records
    CommercialClassification.all
  end
end
