class ComponentSearcher
  attr_reader :width, :height

  def initialize(parameters = {})
    @width, @height = parameters[:width].to_i, parameters[:height].to_i if parameters[:width].present? && parameters[:height].present?
    @components = []
    @values = parameters[:specifications]
  end

  def search_for_line(line_id)
    line = Line.find(line_id)

    line_categories = line.categories.pluck(:category_id)

    if width.blank? || height.blank?
      return search_line_components_without_rules(line, line_categories)
    else
      return search_line_components_applying_the_rules(line, line_categories)
    end
  end

  private
  def search_line_components_without_rules(line, line_categories)
    Component.includes(:families, :category, values: [:specification]).user_components.where(category: line_categories, families: { id: line.family_id }).order(:id)
  end

  def search_line_components_applying_the_rules(line, line_categories)
    available_weights = line.dimensions.where('width @> ? AND height @> ?', width, height).pluck(:weight)

    components = line.rules.includes(component: [:families]).where(weight: available_weights).map do |rule|
      if line_categories.include?(rule.component.category_id) && rule.component.family_ids.include?(line.family_id)
        case rule[:control_method]
          when Rule.control_methods[:width]
            if (rule.automatic? && dimension_with_losses(:width, width, rule.component) <= rule.component.automatic_value) || (!rule.automatic? && rule.width.include?(width))
              component = rule.component.clone
              component.rule_required = rule.required if !rule.required.blank?
              component.informed_consumption = rule.informed_consumption?
              component.rule_id = rule.id
              component
            end
          when Rule.control_methods[:height]
            if (rule.automatic? && dimension_with_losses(:height, height, rule.component) <= rule.component.automatic_value) || (!rule.automatic? && rule.height.include?(height))
              component = rule.component.clone
              component.rule_required = rule.required if !rule.required.blank?
              component.informed_consumption = rule.informed_consumption?
              component.rule_id = rule.id
              component
            end
          when Rule.control_methods[:area]
            if rule.width.include?(width) && rule.height.include?(height)
              component = rule.component.clone
              component.rule_required = rule.required if !rule.required.blank?
              component.informed_consumption = rule.informed_consumption?
              component.rule_id = rule.id
              component
            end
          when Rule.control_methods[:unit]
            component = rule.component.clone
            component.rule_required = rule.required if !rule.required.blank?
            component.informed_consumption = rule.informed_consumption?
            component.rule_id = rule.id
            component
          when Rule.control_methods[:total_area]
            if rule.area.include?(width * height)
              component = rule.component.clone
              component.rule_required = rule.required if !rule.required.blank?
              component.informed_consumption = rule.informed_consumption?
              component.rule_id = rule.id
              component
            end
        end
      end
    end

    components << line.categories.where(required: false).map do |lc|
      Component.find_by(erasable: false, category: lc.category_id).tap do |_c|
        _c.rule_required = false
      end
    end

    components = components.flatten.compact.sort_by(&:id).uniq

    grouped_components = {}
    
    grouped_components = components.group_by{|component| component.category_id}

    filter_by_attributes(grouped_components) if @values.present? && !@values.empty?

    grouped_components.each_pair do |category_id, components|
      grouped_components[category_id] = components.group_by{|component| component.name_without_automatic_value}
    end

    components = []

    grouped_components.each_pair do |category_id, components_by_category|
      components_by_category.each_pair do |name, _components|
        if _components.first.automatic?
          component = _components.first

          component.name = name
          component.component_ids = _components.map(&:id).uniq

          components << component
        else
          _components.uniq{ |c| c.rule_id }.each do |component|
            component.name = name
            component.component_ids = _components.map(&:id).uniq

            components << component
          end
        end
      end
    end

    components.delete_if do |component|
      same_category_components = components.select { |_c| _c.category_id == component.category_id }

      not_required = same_category_components.select {|_c| _c.rule_required != true }

      same_category_components.any? {|_c| _c.rule_required == true } && not_required.count == 1 && !component.erasable?
    end

    components.flatten
  end

  def filter_by_attributes(grouped_components)
    grouped_components.each_pair do |category_id, components_by_category|
      filtered_components = components_by_category.dup
      not_erable_component = filtered_components.find { |_c| !_c.erasable? }

      @values.each do |value_id|
        value = Value.includes(:specification).with_deleted.find(value_id)
        specification = value.specification

        filtered_components.delete_if do |component|
          if component.erasable?
            has_specification = component.values.where(specification_id: specification.id).exists?

            has_specification && !component.values.where(id: value_id).exists?
          else
            true
          end
        end
      end

      grouped_components[category_id] = if filtered_components.empty?
        components_by_category
      else
        not_erable_component.nil? ? filtered_components : filtered_components.unshift(not_erable_component)
      end
    end
  end

  def dimension_with_losses(dimension, value, component)
    return value - component.offset_left.to_i - component.offset_right.to_i if dimension == :width
    return value - component.offset_top.to_i - component.offset_bottom.to_i if dimension == :height
    return value
  end
end