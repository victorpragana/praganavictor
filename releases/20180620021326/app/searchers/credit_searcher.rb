class CreditSearcher
  CREDIT_EXTRACT = 'credit_extract'
  CONCEDED_CREDIT = 'conceded_credit'

  def initialize(parameters = {})
    @parameters = parameters
  end

  def search(workplace)
    scope = if search_by_destination?(workplace)
      scope = credit_type.where(destination_id: workplace.third_party.id)

      scope = scope.where(owner_id: @parameters['third_party_id']) if parameter_present?('third_party_id')

      scope
    else
      credit_type.where(owner_id: workplace.third_party.id)
    end

    scope = scope.approved_or_antecipated
    scope = scope.where("date >= ?", Time.parse(@parameters['from']).beginning_of_day) if parameter_present?('from')
    scope = scope.where("date <= ?", Time.parse(@parameters['until']).end_of_day) if parameter_present?('until')

    scope.order(date: :asc)
  end

  def previous_balance(workplace)
    return 0 unless parameter_present?('from')

    scope = if search_by_destination?(workplace)
      scope = credit_type.where(destination_id: workplace.third_party.id)

      scope = scope.where(owner_id: @parameters['third_party_id']) if parameter_present?('third_party_id')

      scope
    else
      credit_type.where(owner_id: workplace.third_party.id)
    end

    scope = scope.approved_or_antecipated
    scope = scope.where("date < ?", Time.parse(@parameters['from']).beginning_of_day)

    scope.sum(:value) * (search_by_destination?(workplace) ? -1 : 1)
  end

  def search_by_destination?(workplace)
    @search_by_destination ||= workplace.is_a?(Factory) || (workplace.is_a?(Distributor) && @parameters['type'] == CONCEDED_CREDIT)
  end

  private

  def credit_type
    @parameters['credit_type'].constantize
  end

  def parameter_present?(parameter)
    @parameters.present? && @parameters[parameter].present?
  end
end