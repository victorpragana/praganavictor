class LineSearcher
  attr_reader :width, :height

  def initialize(parameters = {})
    @width, @height = parameters[:width].to_i, parameters[:height].to_i if parameters[:width].present? && parameters[:height].present?
    @values = parameters[:specifications]
    @family_id = parameters[:family_id]
    @category_id = parameters[:category_id]
    @active = parameters[:active]
  end

  def search(workplace)
    results = if workplace.is_a?(Factory)
      Line.includes(categories: [:category]).all
    else
      workplace.lines.includes(categories: [:category])
    end

    results = results.order(updated_at: :desc)

    if @values.present?
      results = results.joins(line_rules: [rules: [component: [:values]]])
        .joins("INNER JOIN (SELECT component_id , ARRAY_AGG(value_id) AS values FROM components_values WHERE value_id IN (#{@values.join(',')}) GROUP BY component_id) t ON components_values.component_id = t.component_id")
        .where("t.values = ARRAY#{@values.map(&:to_i)}")
        .group("lines.id")
    end

    results = results.for_family(@family_id)                         if @family_id.present?
    results = results.with_dimension(width, height).active.to_a.uniq if width && height
    results = results.joins(:line_rules).where('line_rules.category_id is null or line_rules.category_id in(?)', @category_id) if @category_id.present?
    results = results.where(active: @active) unless @active.nil?

    return results
  end
end