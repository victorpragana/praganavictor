class WorkplacesController < ApplicationController
  include VerifyWorkplace
  include ParseSubdomain

  before_action :authenticate_administrator!, except: :current
  before_action :verify_workplace, except: :current

  def index
    @workplaces = current_administrator.workplace.workplaces
  end

  def current
    @workplace = ThirdParty.find_by(subdomain: subdomain)

    return head :not_found if @workplace.nil?
  end
  
  private 
  def verify_workplace
    ensure_workplace_is(Factory, Distributor, Store)
  end
end
