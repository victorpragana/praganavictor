# == Schema Information
#
# Table name: estimate_items
#
#  id                :integer          not null, primary key
#  estimate_id       :integer
#  item_code         :string
#  item_description  :string           not null
#  unit              :integer          not null
#  quantity          :decimal(8, 2)    not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  type              :integer          default(0)
#  factory_value     :decimal(10, 2)   default(0.0)
#  distributor_value :decimal(10, 2)   default(0.0)
#  store_value       :decimal(10, 2)   default(0.0)
#  customer_value    :decimal(10, 2)   default(0.0)
#  discount          :decimal(5, 2)    default(0.0)
#  item_id           :integer
#

class EstimateItemsController < ApplicationController
  include VerifyWorkplace

  before_action :authenticate_administrator!
  before_action {|c| c.ensure_workplace_is(Factory, Distributor, Store)}

  before_action :verify_user_maximum_discount, only: [:apply_discount]

  def create
    service = EstimateItemService.new(estimate_item_params)

    service.add_to(params[:estimate_id])

    if service.success
      EstimateService.new().recalculate_totals(service.record.estimate_id)
      
      head :created
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    service = EstimateItemService.new

    service.destroy(params[:id])

    if service.success
      EstimateService.new().recalculate_totals(service.record.estimate_id)

      head :no_content
    else
      render json: { errors: service.errors }, status: :bad_request
    end
  end

  def apply_discount
    service = EstimateItemService.new(estimate_item_discount_params)

    service.apply_discount(params[:id])

    if service.success
      head :no_content
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  private

  def estimate_item_params
    params.permit(:item_id, :quantity)
  end

  def estimate_item_discount_params
    params.permit(:discount)
  end

  def verify_user_maximum_discount
    render json: { errors: [I18n.t('errors.discount_greater_than_user')] }, status: :unprocessable_entity if current_administrator.maximum_discount < BigDecimal.new(params[:discount].to_d)
  end
end
