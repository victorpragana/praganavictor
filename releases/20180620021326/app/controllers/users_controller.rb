# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  provider               :string           default("email"), not null
#  uid                    :string           default(""), not null
#  email                  :string           default(""), not null
#  username               :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  name                   :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  tokens                 :json
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  type                   :string
#  maximum_discount       :decimal(5, 2)    default(0.0)
#  erp_id                 :integer
#  selected_workplace_id  :integer
#

class UsersController < ApplicationController
  include VerifyWorkplace

  before_action :authenticate_administrator!, except: [:show, :change_workplace]
  before_action :authenticate_worker!, only: [:show, :change_workplace]

  before_action {|c| c.ensure_workplace_is(Factory, Distributor, Store)}
  before_action :load_user, only: [:destroy, :show, :change_workplace]

  def index
    @users = if current_administrator.workplace.is_a?(Factory)
      User.all
    else
      workplaces = current_administrator.workplace.workplaces.map do |workplace|
        { workplace_id: workplace.id, workplace_type: workplace.actable_type }
      end

      workplace_types = ['Factory']
      workplace_types << 'Distributor' if current_administrator.workplace.is_a?(Store)

      forbidden_users = UserWorkplace.where('workplace_type in(?) OR (workplace_type = ? AND workplace_id != ?)', workplace_types, current_administrator.workplace.actable_type, current_administrator.workplace.id).pluck(:user_id)

      @users = User.joins(:workplaces).where(user_workplaces: { workplace_id: workplaces.map{ |w| w[:workplace_id] }, workplace_type: workplaces.map{ |w| w[:workplace_type] }})
      @users = @users.where.not(user_workplaces: { user_id: forbidden_users })
    end

    @users = @users.select(:id, :name, :username, :email, :type).distinct.order(:id)
  end

  def create
    service = UserService.new(user_params)

    service.create

    if service.success
      head :created
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def show
  end

  def update
    service = UserService.new(user_params)

    service.update(params[:id])

    if service.success
      @user = service.record

      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @user.destroy
      head :no_content
    else
      render json: { errors: @user.errors.full_messages }, status: :bad_request
    end
  end

  def change_workplace
    return render json: { errors: ['Authorized users only.'] }, status: 401 unless params[:id].to_i == current_worker.id

    if @user.update(selected_workplace_id: params[:workplace_id])
      head :no_content
    else
      render json: { errors: @user.errors.full_messages }, status: :bad_request
    end
  end

  private

  def load_user
    @user = User.find(params[:id])
  end

  def user_params
    params.permit(:name, :username, :email, :maximum_discount, :password, :password_confirmation, :type, :erp_id, workplaces: [:workplace_id, :workplace_type])
  end
end
