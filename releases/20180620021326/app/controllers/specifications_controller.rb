# == Schema Information
#
# Table name: specifications
#
#  id             :integer          not null, primary key
#  name           :string           not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  deleted_at     :datetime
#  automatic      :boolean          default(FALSE)
#  used_in_filter :boolean          default(FALSE)
#

class SpecificationsController < ApplicationController
  include HasConversionToMm
  
  before_action :authenticate_administrator!, except: [:for_filtering]
  before_action :authenticate_seller!, only: [:for_filtering]
  before_filter :load_specification, only: [:destroy]

  def index
    @specifications = Specification.includes(:values).all.order(:name)
  end

  def for_filtering
    @specifications = if params[:family_id].present?
      Specification.joins(values: [components: [:families]]).used_in_filter.where("families.id = ?", params[:family_id]).distinct.order(:name)
    else
      Specification.includes(:values).used_in_filter.order(:name)
    end

    render template: 'specifications/index', status: :ok
  end

  def create
    service = SpecificationService.new(specification_params)

    service.create

    if service.success
      @specification = service.record

      render template: 'specifications/show', status: :created
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def update
    service = SpecificationService.new(specification_params)

    service.update(params[:id])

    if service.success
      @specification = service.record

      render template: 'specifications/show', status: :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def show
    @specification = Specification.find(params[:id])
  end

  def destroy
    if @specification.destroy
      head :no_content
    else
      render json: { errors: @specification.errors.full_messages}, status: :bad_request
    end
  end

  private
    def specification_params
      params.permit(:name, :automatic, :used_in_filter, values: [:value, :comparison_value, :id]).tap do |parameters|
        convert_parameter_to_mm(parameters[:values], :comparison_value)
      end
    end

    def load_specification
      @specification = Specification.find(params[:id])
    end
end
