# == Schema Information
#
# Table name: price_tables
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  owner_id   :integer
#  owner_type :string
#  name       :string           default(""), not null
#  type       :integer          default(0), not null
#  default    :boolean          default(FALSE)
#

class PriceTablesController < ApplicationController
  include VerifyWorkplace

  before_action :authenticate_seller!
  before_action {|c| c.ensure_workplace_is(Factory, Distributor, Store)}
  before_action :load_price_table, only: [:destroy, :show]

  def index
    if params['columns'].present?
      render json: PriceTableDatatable.new(view_context)
    else
      @price_tables = current_seller.workplace.price_tables.order(:type, :name)
    end
  end

  def create
    service = PriceTableService.new(price_table_params)

    service.create

    if service.success
      head :created
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def show
    @lines = []
    @items = []

    if @price_table.for_lines?
      @lines = Line.active.pluck(:id).map do |line|
        PriceTableLine.find_or_initialize_by(line_id: line, price_table_id: @price_table.id) do |ptl|
          ptl.price = ''
        end
      end
    elsif @price_table.for_items?
      @items = Item.all.pluck(:id).map do |item|
        PriceTableItem.find_or_initialize_by(item_id: item, price_table_id: @price_table.id) do |ptp|
          ptp.price = ''
        end
      end
    end
  end

  def update
    service = PriceTableService.new(price_table_params)

    service.update(params[:id])

    if service.success
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @price_table.destroy
      head :no_content
    else
      render json: { errors: @price_table.errors.full_messages }, status: :bad_request
    end
  end

  def batch_destroy
    begin
      ActiveRecord::Base.transaction do
        params[:ids].each do |id|
          price_table = current_seller.workplace.price_tables.find(id)

          price_table.destroy!
        end
      end

      head :no_content
    rescue => e
      render json: { errors: [e.message] }, status: :bad_request
    end
  end

  private

  def load_price_table
    @price_table = current_seller.workplace.price_tables.find(params[:id])
  end

  def price_table_params
    params.permit(:name, :type, :default, price_table_lines: [:price, :maximum_discount, line: [:id]], 
      price_table_items: [:price, :maximum_discount, item: [:id]]).merge({'owner' => current_seller.workplace})
  end
end
