# == Schema Information
#
# Table name: order_loose_items
#
#  id                :integer          not null, primary key
#  order_id          :integer
#  item_code         :string
#  item_description  :string           not null
#  unit              :integer          not null
#  quantity          :decimal(8, 2)    not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  type              :integer          default(0)
#  factory_value     :decimal(10, 2)   default(0.0)
#  distributor_value :decimal(10, 2)   default(0.0)
#  store_value       :decimal(10, 2)   default(0.0)
#  customer_value    :decimal(10, 2)   default(0.0)
#  discount          :decimal(5, 2)    default(0.0)
#  item_id           :integer
#

class OrderLooseItemsController < ApplicationController
  include VerifyWorkplace

  before_action :authenticate_administrator!
  before_action {|c| c.ensure_workplace_is(Factory, Distributor, Store)}
  

  def apply_value
    service = OrderLooseItemService.new(item_value_params)

    service.apply_value(params[:id])

    if service.success
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  private
    def item_value_params
      params.permit(:value)
    end
end
