class MenusController < ApplicationController
  before_action :authenticate_worker!

  def index
    render json: { menus: MenuService.new.load_for(current_worker.type, current_worker.workplace.class.to_s) }
  end
end
