class CreditsController < ApplicationController
  include VerifyWorkplace

  before_action :authenticate_administrator!
  before_action {|c| c.ensure_workplace_is(Factory, Distributor, Store)}

  def index
    searcher = CreditSearcher.new(search_params)

    @credits = searcher.search(current_administrator.workplace).map do |record|
      {
        date: record.date.strftime("%d/%m/%Y"),
        owner: record.owner.name,
        destination: record.destination.name,
        reason: Movement.reasons[record.reason],
        value: searcher.search_by_destination?(current_administrator.workplace) ? -1 * record.value : record.value,
        balance: 0,
        order_id: record.order_id,
        id: record.id,
        antecipated: record.antecipated?
      }.tap do |hash|
        hash[:payment_method] = {
          installment: record.installment_number,
          installment_number: record.payment.installment_number
        } if record.payment_id.present?
      end
    end

    @previous_balance = searcher.previous_balance(current_administrator.workplace)

    @credits.each_with_index do |record, index|
      previous_balance = index == 0 ? @previous_balance : @credits[index - 1][:balance]

      record[:balance] = previous_balance + record[:value]
    end
  end

  def create
    service = PaymentService.new(payment_params)

    service.create(params[:order_id])

    if service.success
      @payment = service.record

      if @payment&.bank_slip_url.blank?
        head :created
      else
        render json: { credit: { bank_slip_url: @payment.bank_slip_url } }, status: :created
      end
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def antecipate
    service = CreditAntecipationService.new(antecipation_params[:movement_ids], current_administrator.workplace.third_party.id)

    if service.antecipate
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  private

  def antecipation_params
    params.permit(movement_ids: [])
  end

  def search_params
    params.permit(:third_party_id, :from, :until, :credit_type, :type)
  end

  def payment_params
    params.permit(:value, :credit_card_iugu_token, :payment_method, :payment_condition_id, :credit_type).merge(third_party_id: current_administrator.workplace.third_party.id)
  end
end