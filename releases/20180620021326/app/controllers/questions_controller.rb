# == Schema Information
#
# Table name: questions
#
#  id         :integer          not null, primary key
#  label      :string           not null
#  value      :string           not null
#  type       :integer          not null
#  options    :json
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class QuestionsController < ApplicationController
  include VerifyWorkplace

  before_action :authenticate_administrator!
  before_action { |c| c.ensure_workplace_is(Factory) }
  before_filter :load_question, only: [:show, :destroy]

  def index
    render json: QuestionDatatable.new(view_context)
  end

  def create
    service = QuestionService.new(question_params)

    service.create

    if service.success
      head :created
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def update
    service = QuestionService.new(question_params)

    service.update(params[:id])

    if service.success
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def show
  end

  def destroy
    if @question.destroy
      head :no_content
    else
      render json: { errors: @question.errors.full_messages }, status: :bad_request
    end
  end

  def set_lines
    service = QuestionService.new(question_lines_params)

    service.set_lines(params[:id])

    if service.success
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  private

  def load_question
    @question = Question.find(params[:id])
  end

  def question_params
    params.permit(:label, :value, :type, options: %i[label value])
  end

  def question_lines_params
    params.permit(:line_ids, line_ids: [])
  end
end
