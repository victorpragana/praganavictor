# == Schema Information
#
# Table name: lines
#
#  id                   :integer          not null, primary key
#  name                 :string           not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  family_id            :integer
#  sale_price           :decimal(8, 3)    default(0.0), not null
#  horizontal_increment :integer          default(0), not null
#  vertical_increment   :integer          default(0), not null
#  erp_categories       :json
#  ncm                  :string(8)
#  cst                  :integer
#  active               :boolean          default(TRUE)
#  finished             :boolean          default(FALSE)
#

class LinesController < ApplicationController
  include HasConversionToMm

  before_action :authenticate_administrator!, except: [:index, :components]
  before_action :authenticate_worker!, only: [:index, :components]
  before_filter :load_line, only: [:destroy, :components, :set_rules]

  def index
    if params["columns"]
      render json: LineDatatable.new(view_context, params['active']), status: :ok
    else
      searcher = LineSearcher.new(line_search_params)
      @lines = searcher.search(current_worker.workplace)
    end
  end

  def create
    service = LineService.new(line_params)

    service.create

    if service.success
      @line = service.record

      head :created
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def show
    @line = Line.find(params[:id])
  end

  def rules
    @line_rules = LineRule.includes(rules: [component: [:category]]).where(line_id: params[:id]).order(:weight, :description, :control_method, :width, :height)
  end

  def update
    service = LineService.new(line_params)

    service.update(params[:id])

    if service.success
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def components
    searcher = ComponentSearcher.new(component_search_params)
    @line_id = params[:id]
    @components = searcher.search_for_line(params[:id])
  end

  def set_rules
    service = LineRuleService.new(rules_params)

    service.set_line_rules_for(@line.id)

    if service.success
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @line.destroy
      head :no_content
    else
      render json: { errors: @line.errors.full_messages }, status: :bad_request
    end
  end

  def duplicate
    service = LineService.new

    service.duplicate(params[:id])

    if service.success
      @line = service.record

      render json: { line: {id: @line.id } }, status: :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def activate
    service = LineService.new

    service.activate(params[:id])

    if service.success
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def inactivate
    service = LineService.new

    service.inactivate(params[:id])

    if service.success
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def duplicate_rule
    service = LineRuleService.new(duplicate_rules_params)

    service.duplicate(params[:id])

    if service.success
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  def import_rules
    service = LineRuleService.new(import_rules_params)

    service.import(params[:id])

    if service.success
      head :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  private

  def line_params
    params.permit(:family_id, :name, :sale_price, :horizontal_increment, :vertical_increment, :finished, dimensions: [:initial_width, :final_width, :initial_height, :final_height, :weight], 
      categories: [:category, :required]).tap do |parameters|
      convert_parameter_to_mm(parameters, :horizontal_increment)
      convert_parameter_to_mm(parameters, :vertical_increment)
      convert_parameter_to_mm(parameters[:dimensions], :initial_width)
      convert_parameter_to_mm(parameters[:dimensions], :initial_height)
      convert_parameter_to_mm(parameters[:dimensions], :final_width)
      convert_parameter_to_mm(parameters[:dimensions], :final_height)

      parameters.merge!(erp_categories: params[:erp_categories]) if !params[:erp_categories].nil?
      parameters.merge!(ncm: params[:ncm])                       if !params[:ncm].nil?
      parameters.merge!(cst: params[:cst])                       if !params[:cst].nil?
    end
  end

  def rules_params
    params.permit(:rules, rules: [:id, :initial_width, :final_width, :initial_height, :final_height, :initial_area, :final_area, :description, :width_consumption, :height_consumption,
      :total_consumption, :quantity_consumption, :cost, :sale_price, :required, :automatic, :category_id, :weight,
      :component_ids, component_ids: [], control_method: [:id]])[:rules].tap do |parameters|
      convert_parameter_to_mm(parameters, :initial_width)
      convert_parameter_to_mm(parameters, :initial_height)
      convert_parameter_to_mm(parameters, :final_width)
      convert_parameter_to_mm(parameters, :final_height)

      convert_parameter_to_mm2(parameters, :initial_area)
      convert_parameter_to_mm2(parameters, :final_area)
    end
  end

  def component_search_params
    params.permit(:width, :height, specifications: []).tap do |parameters|
      convert_parameter_to_mm(parameters, :width)
      convert_parameter_to_mm(parameters, :height)
    end
  end

  def line_search_params
    params.permit(:width, :height, :status, :family_id, :category_id, :active, family_id: [], specifications: []).tap do |parameters|
      convert_parameter_to_mm(parameters, :width)
      convert_parameter_to_mm(parameters, :height)
    end
  end

  def duplicate_rules_params
    params.permit(:line_rule_id)
  end

  def import_rules_params
    params.permit(line_rule_ids: [])
  end

  def load_line
    @line = Line.find(params[:id])
  end
end
