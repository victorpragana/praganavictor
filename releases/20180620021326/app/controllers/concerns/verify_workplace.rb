module VerifyWorkplace
  extend ActiveSupport::Concern

  included do
    protected
      def ensure_workplace_is(*types)
        unless types.include? current_worker.workplace.class
          return render json: {
            errors: ['Authorized users only.']
          }, status: 401
        end
      end
  end
end