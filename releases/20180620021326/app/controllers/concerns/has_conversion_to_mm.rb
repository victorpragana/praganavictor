module HasConversionToMm
  extend ActiveSupport::Concern

  included do
    def convert_parameter_to_mm(parameters, parameter_name)
      if parameters.is_a?(Hash)
        parameters[parameter_name.to_sym] = (parameters[parameter_name.to_sym].to_f * 10).to_i if parameters[parameter_name.to_sym].present?
      elsif parameters.is_a?(Array)
        parameters.each do |parameter|
          parameter[parameter_name.to_sym] = (parameter[parameter_name.to_sym].to_f * 10).to_i if parameter[parameter_name.to_sym].present?
        end
      end
    end

    def convert_parameter_to_mm2(parameters, parameter_name)
      if parameters.is_a?(Hash)
        parameters[parameter_name.to_sym] = (parameters[parameter_name.to_sym].to_f * 100).to_i if parameters[parameter_name.to_sym].present?
      elsif parameters.is_a?(Array)
        parameters.each do |parameter|
          parameter[parameter_name.to_sym] = (parameter[parameter_name.to_sym].to_f * 100).to_i if parameter[parameter_name.to_sym].present?
        end
      end
    end
  end
end