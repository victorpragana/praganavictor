class DocumentController < ApplicationController
  before_action :authenticate_administrator!
  
  def index
    @document = Document.first
  end

  def update
    service = DocumentService.new(document_params)

    service.update

    if service.success
      @document = Document.first

      render template: 'document/index', status: :ok
    else
      render json: { errors: service.errors }, status: :unprocessable_entity
    end
  end

  private
    def document_params
      params.permit(:text)
    end
end
