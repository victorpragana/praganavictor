json.clients @clients do |client|
  json.id               client.id
  json.name             client.name
  json.type             client.class.to_s
  json.allow_special_discount client.allow_special_discount?
  json.special_discount client.special_discount.to_f
  json.third_party_id   client.third_party.id

  if client.owner.present?
    json.owner do
      json.id             client.owner.id
      json.name           client.owner.name
      json.type           client.owner.class.to_s
    end
  end
end