json.price_tables @price_tables do |price_table|
  json.id              price_table.id
  json.line_id         price_table.line.id
  json.line_name       price_table.line.name
  json.price           price_table.price 
end