json.set! :distributor do
  json.id                     @distributor.id
  json.name                   @distributor.name
  json.identification         @distributor.identification
  json.zip_code               @distributor.zip_code
  json.street                 @distributor.street
  json.neighborhood           @distributor.neighborhood
  json.number                 @distributor.number
  json.complement             @distributor.complement
  json.phone                  @distributor.phone
  json.email                  @distributor.email
  json.corporate_name         @distributor.corporate_name
  json.state_inscription      @distributor.state_inscription
  json.municipal_inscription  @distributor.municipal_inscription
  json.allow_special_discount @distributor.allow_special_discount?
  json.special_discount       @distributor.special_discount.to_f
  json.subdomain              @distributor.subdomain
  json.logo_url               @distributor.logo.url
  json.logo_file_name         @distributor.logo_file_name
  json.current_account        @distributor.current_account
  json.agency                 @distributor.agency
  json.bank                   ThirdParty.banks[@distributor.bank]

  unless @distributor.city_id.nil?
    json.set! :city do
      json.id           @distributor.city.id
      json.name         @distributor.city.name
    end

    json.set! :state do
      json.id           @distributor.state.id
      json.name         @distributor.state.name
    end
  end
end