json.set! :specification do
  json.id               @specification.id
  json.name             @specification.name
  json.automatic        @specification.automatic?
  json.used_in_filter   @specification.used_in_filter?
  json.values           @specification.values.order(:value) do |value|
    json.id               value.id
    json.value            value.value
    json.comparison_value value.comparison_value_from_mm_to_cm
  end
end