json.workplaces @workplaces do |workplace|
  json.id   workplace.id
  json.name workplace.name
  json.type workplace.class.to_s
end