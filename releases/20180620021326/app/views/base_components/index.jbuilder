json.base_components @base_components do |base_component|
  json.(base_component, :id, :name, :code, :characteristics)

  json.cost     base_component.cost.to_f
  json.unit     BaseComponent.units[base_component.unit]
  
  json.set! :category do
    json.id     base_component.category.id
    json.name   base_component.category.name
  end

  json.families base_component.families do |family|
    json.name   family.name
    json.color  family.color
  end
end