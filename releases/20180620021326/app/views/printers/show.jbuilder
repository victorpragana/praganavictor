json.set! :printer do
  json.(@printer, :id, :name, :real_name, :server_name, :server_id)

  json.default @printer.default?
end