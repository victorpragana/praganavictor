json.set! :user do
  json.id               @user.id
  json.name             @user.name
  json.username         @user.username
  json.email            @user.email
  json.type             @user.type
  json.maximum_discount @user.maximum_discount.to_f
  json.erp_id           @user.erp_id

  if @user.workplace
    json.set! :workplace do
      json.id @user.workplace.id
      json.name @user.workplace.name
      json.type @user.workplace.actable_type
      json.subdomain @user.workplace.subdomain
      json.logo_url Rails.env.development? ? '' : @user.workplace.logo.url
    end
  end

  json.workplaces @user.workplaces do |workplace|
    json.id             workplace.id
    json.workplace_id   workplace.workplace_id
    json.workplace_type workplace.workplace_type
    json.workplace_name workplace.workplace.name
    json.subdomain      workplace.subdomain
  end
end
