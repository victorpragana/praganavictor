json.set! :component do
  json.id                     @component.id
  json.name                   @component.name
  json.automatic              @component.automatic?

  json.set! :price  do
    json.total_consumption    @component_select.total_consumption.to_f
    json.width_consumption    @component_select.width_consumption.to_f
    json.height_consumption   @component_select.height_consumption.to_f
    json.quantity_consumption @component_select.quantity_consumption.to_f
    json.cost                 @component_select.cost.to_f
    json.price                @component_select.price.to_f
    json.total                @component_select.total.to_f.round(2)
  end
end