json.cache! ['v1/lines', @lines], expires_in: 2.minutes do
  json.lines @lines do |line|
    json.(line, :id, :name, :family_id)

    json.active line.active?
    json.finished line.finished?

    json.sale_price line.sale_price.to_f

    json.horizontal_increment line.horizontal_increment_from_mm_to_cm
    json.vertical_increment   line.vertical_increment_from_mm_to_cm

    json.family do
      json.name line.family.name
      json.color line.family.color
    end

    json.categories line.categories do |lc|
      json.category_id          lc.category.id
      json.category_name        lc.category.name
      json.required             lc.required
    end

    json.dimensions line.dimensions do |dimension|
      json.initial_width        dimension.width_from_mm_to_cm.min
      json.final_width          dimension.width_from_mm_to_cm.max
      json.initial_height       dimension.height_from_mm_to_cm.min
      json.final_height         dimension.height_from_mm_to_cm.max
      json.weight               Dimension.weights[dimension.weight]
    end
  end
end