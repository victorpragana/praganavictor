json.commercial_classifications @commercial_classifications do |commercial_classification|
  json.(commercial_classification, :id, :name) 
end