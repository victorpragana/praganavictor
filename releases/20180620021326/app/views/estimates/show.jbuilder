json.set! :estimate do
  json.id            @estimate.id
  json.date          @estimate.date.strftime("%d/%m/%Y")
  json.observation   @estimate.observation
  json.client_id     @estimate.client_id
  json.client_type   @estimate.client_type
  json.order_id      @estimate.order_id
  json.seller_id     @estimate.seller_id
  json.seller_type   @estimate.seller_type
  json.total         @estimate.total.to_f
  json.discount      @estimate.discount.to_f
  json.optimize      @estimate.optimize
  json.options_count @estimate.options.count
  json.partial_total @estimate.partial_total.to_f

  json.total_discount      @estimate.total_discount.to_f
  json.total_with_discount @estimate.total_with_discount.to_f

  json.status        Estimate.statuses[@estimate.status]

  json.message       @estimate.processing_message.uniq

  json.items         @estimate.items do |item|
    json.id                  item.id
    json.item_code           item.item_code
    json.item_description    item.item_description
    json.unit                EstimateItem.units[item.unit]
    json.type                EstimateItem.types[item.type]
    json.quantity            item.quantity.to_f
    json.value               item.value.to_f
    json.discount            item.discount.to_f
    json.total               item.total.to_f
  end
end