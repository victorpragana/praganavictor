json.environments @environments do |environment|
  json.id          environment.id
  json.name        environment.name
  json.category    Environment.categories[environment.category]
  json.estimate_id environment.estimate_id
end