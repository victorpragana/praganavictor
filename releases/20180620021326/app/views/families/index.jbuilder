json.families @families do |family|
  json.(family, :id, :name, :color)
end