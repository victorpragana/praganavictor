class ErpWorker
  include Sidekiq::Worker
  include SidekiqStatus::Worker

  sidekiq_options queue: :erp, retry: 0, backtrace: true

  PRODUCT_CLASSIFICATION = 'REVENDA'
  CONTROL_METHOD         = 'NAO_ESTOCAVEL'
  ENTITY_ID              = 1

  def perform(order_id, payment_condition_id)
    order = Order.find(order_id)

    begin
      @progress  = 0

      self.total = 7 + (3 * order.order_items.count) + (3 * order.items.count)

      @third_party_id = nil
      @products       = []
      @items          = []
      
      log_progress("#{Time.now.strftime('%H:%M:%S')} - Iniciando envio")

      if order.to_send? || order.error?
        order.sending!

        response = create_or_update_customer(order.billing_client)

        if response == true
          log_progress("#{Time.now.strftime('%H:%M:%S')} - Criando produtos")

          response = create_products(order)

          if response == true
            log_progress("#{Time.now.strftime('%H:%M:%S')} - Criando o pedido")

            response = create_order(order, payment_condition_id)

            if response == true
              order.processing_messages << "#{Time.now.strftime('%d/%m/%Y - %H:%M')} - Processamento concluído com sucesso"

              order.save!

              self.payload = "#{Time.now.strftime('%H:%M:%S')} - Processamento concluído com sucesso"
            else
              order.error!

              order.processing_messages << "#{Time.now.strftime('%d/%m/%Y - %H:%M')} - #{encoded_response(response)}"

              order.save!

              self.payload = "#{Time.now.strftime('%H:%M:%S')} - #{encoded_response(response)}"
            end
          else
            order.error!

            if response.is_a?(String)
              order.processing_messages << "#{Time.now.strftime('%d/%m/%Y - %H:%M')} - #{encoded_response(response)}"

              order.save!

              self.payload = "#{Time.now.strftime('%H:%M:%S')} - #{encoded_response(response)}"
            else
              order.processing_messages << "#{Time.now.strftime('%d/%m/%Y - %H:%M')} - #{response['codigoMensagem']}: #{encoded_response(response['mensagem'])}"

              order.save!

              self.payload = "#{Time.now.strftime('%H:%M:%S')} - #{response['codigoMensagem']}: #{encoded_response(response['mensagem'])}"
            end
          end
        else
          order.error!

          order.processing_messages << "#{Time.now.strftime('%d/%m/%Y - %H:%M')} - #{encoded_response(response)}"

          order.save!

          self.payload = "#{Time.now.strftime('%H:%M:%S')} - #{encoded_response(response)}"
        end
      end
    rescue => e
      order.error!

      order.processing_messages << "#{Time.now.strftime('%d/%m/%Y - %H:%M')} - #{e.message}"

      order.save!
          
      self.payload = "#{Time.now.strftime('%H:%M:%S')} - #{e.message}"
    end
  end

  private
  def encoded_response(message)
    message.to_s.force_encoding("ISO-8859-1").encode("UTF-8")
  end

  def log_progress(message)
    @progress = @progress + 1

    at(@progress, message)
  end

  def erp_client
    Varejonline.new(Rails.application.secrets.varejonline["access_token"])
  end

  def create_or_update_customer(client)
    searcher = Varejonline::Searcher::Administrative::ThirdPartySearcher.new({
      documento: client.identification, quantidade: 1
    })

    log_progress("#{Time.now.strftime('%H:%M:%S')} - Verificando existência do cliente #{client.name}")

    result = erp_client.third_parties.list(searcher)

    if result.status == 200
      if result.payload.empty?
        log_progress("#{Time.now.strftime('%H:%M:%S')} - Cliente #{client.name} não encontrado. Criando...")

        result = erp_client.third_parties.create(client_data(client))

        if result.status == 201
          @third_party_id = result.payload

          log_progress("#{Time.now.strftime('%H:%M:%S')} - Cliente #{client.name} criado com sucesso!")

          return true
        else
          return result.payload
        end
      else
        id = result.payload.first['id']

        log_progress("#{Time.now.strftime('%H:%M:%S')} - Cliente #{client.name} encontrado. Atualizando dados...")

        result = erp_client.third_parties.update(id, client_data(client))

        if result.status == 200
          @third_party_id = result.payload

          log_progress("#{Time.now.strftime('%H:%M:%S')} - Cliente #{client.name} atualizado com sucesso!")

          return true
        else
          return result.payload
        end
      end
    else
      return result.payload
    end
  end

  def client_data(client)
    {
      'nome' => client.name, 'documento' => client.identification,
      'emails' => [client.email], 'rg' => client.rg, 'dataNascimento' => nil, 'ie' => client.state_inscription, 'enderecos' => [ {
        'tipo' => 'RUA', 'logradouro' => client.street, 'numero' => client.number, 'bairro' => client.neighborhood,
        'complemento' => client.complement, 'cep' => client.zip_code, 'codigoIBGECidade' => client.city.try(:ibge_code),
        'tipoEndereco' => 'ENDERECO_COBRANCA' } ],
      'telefones' => [ {'ddi' => '55', 'ddd' => client.phone[0..1], 'numero' => client.phone[2..-1]} ],
      'classes' => ['CLIENTE']
    }
  end

  def create_products(order)
    success = true

    order.order_items.each do |order_item|
      if success == true
        searcher = Varejonline::Searcher::Commercial::ProductSearcher.new({
          codigoInterno: order_item.id.to_s.rjust(6, '0'), quantidade: 1
        })

        log_progress("#{Time.now.strftime('%H:%M:%S')} - Verificando existência do item #{order_item.name_for_erp}")

        result = erp_client.products.list(searcher)

        if result.status == 200
          if result.payload.empty?
            log_progress("#{Time.now.strftime('%H:%M:%S')} - Item #{order_item.name_for_erp} não encontrado. Criando...")

            result = erp_client.products.save(product_data(order_item))

            if result.status == 201
              @products << {id: order_item.id, erp_id: result.payload['idRecurso']}

              log_progress("#{Time.now.strftime('%H:%M:%S')} - Item #{order_item.name_for_erp} criado!")
            else
              success = result.payload
            end
          else
            id = result.payload.first['id']

            log_progress("#{Time.now.strftime('%H:%M:%S')} - Item #{order_item.name_for_erp} encontrado. Atualizando dados...")

            result = erp_client.products.update(id, product_data(order_item))

            if result.status == 200
              @products << {id: order_item.id, erp_id: result.payload['idRecurso']}

              log_progress("#{Time.now.strftime('%H:%M:%S')} - Item #{order_item.name_for_erp} atualizado com sucesso!")
            else
              success = result.payload
            end
          end
        else
          success = result.payload
        end
      end
    end

    order.items.each do |item|
      if success == true
        searcher = Varejonline::Searcher::Commercial::ProductSearcher.new({
          codigoInterno: item.item_code, quantidade: 1
        })

        log_progress("#{Time.now.strftime('%H:%M:%S')} - Verificando existência do produto/serviço #{item.identification}")

        result = erp_client.products.list(searcher)

        if result.status == 200
          if result.payload.empty?
            log_progress("#{Time.now.strftime('%H:%M:%S')} - Produto/Serviço #{item.identification} não encontrado. Criando...")

            result = erp_client.products.save(item_data(item))

            if result.status == 201
              @items << { id: item.id, erp_id: result.payload['idRecurso'] }

              log_progress("#{Time.now.strftime('%H:%M:%S')} - Produto/Serviço #{item.identification} criado!")
            else
              success = result.payload
            end
          else
            id = result.payload.first['id']

            log_progress("#{Time.now.strftime('%H:%M:%S')} - Produto/Serviço #{item.identification} encontrado. Atualizando dados...")

            result = erp_client.products.update(id, item_data(item))

            if result.status == 200
              @items << { id: item.id, erp_id: result.payload['idRecurso'] }

              log_progress("#{Time.now.strftime('%H:%M:%S')} - Produto/Serviço #{item.identification} atualizado com sucesso!")
            else
              success = result.payload
            end
          end
        else
          success = result.payload
        end
      end
    end

    success
  end

  def product_data(order_item)
    line = order_item.line

    erp_categories = order_item.line.erp_categories.map{|k,v| {'nome' => v, 'nivel' => k} } if !line.erp_categories.nil?

    raise Exceptions::ErpLineCategoriesNotConfiguredError.new(I18n.t('workers.erp.errors.erp_line_category_not_configured', line: line.name)) if erp_categories.nil?

    {
      'descricao' => order_item.name_for_erp, 'especificacao' => order_item.specification,
      'metodoControle' => ErpWorker::CONTROL_METHOD, 'codigoNcm' => line.ncm,
      'origem' => Line.csts[line.cst], 'codigoInterno' => "#{order_item.id.to_s.rjust(6, '0')}",
      'unidade' => 'UN', 'classificacao' => ErpWorker::PRODUCT_CLASSIFICATION,
      'custoReferencial' => "#{order_item.total_cost.to_d}", 'listCustoReferencial' => [
        {'entidade' => ErpWorker::ENTITY_ID, 'precoCusto' => "#{order_item.total_cost.to_d}"}
      ], 'categorias' => erp_categories
    }
  end

  def item_data(item)
    erp_categories = item.item.erp_categories.map{|k,v| {'nome' => v, 'nivel' => k} } if !item.item.erp_categories.nil?
    
    raise Exceptions::ErpItemCategoriesNotConfiguredError.new(I18n.t('workers.erp.errors.erp_item_category_not_configured', item: item.item.description)) if erp_categories.nil?

    {
      'descricao' => item.identification, 'especificacao' => '',
      'metodoControle' => ErpWorker::CONTROL_METHOD, 'codigoNcm' => item.item.ncm,
      'origem' => Item.csts[item.item.cst], 'codigoInterno' => "#{item.item_code}",
      'unidade' => "#{item.unit.upcase}", 'classificacao' => ErpWorker::PRODUCT_CLASSIFICATION,
      'custoReferencial' => "#{item.value.to_d}", 'listCustoReferencial' => [
        {'entidade' => ErpWorker::ENTITY_ID, 'precoCusto' => "#{item.value.to_d}"}
      ], 'categorias' => erp_categories
    }
  end

  def create_order(order, payment_condition_id)
    result = erp_client.orders.save(order_data(order, payment_condition_id))

    if result.status == 201
      log_progress("#{Time.now.strftime('%H:%M:%S')} - Pedido criado com sucesso!")

      order.update_attribute(:erp_id, result.payload)

      order.sent!

      return true
    else
      return result.payload
    end
  end

  def order_data(order, payment_condition_id)
    {
      'numeroPedidoCliente' => "#{order.id}", 'data' => "#{order.date.strftime('%d-%m-%Y')}", 'horario' => "#{Time.current.strftime('%H:%M:%S')}",
      'idEntidade' => ErpWorker::ENTITY_ID, 'idPlanoPagamento' => payment_condition_id, 'idRepresentante' => "#{order.user.erp_id}",
      'idTerceiro' => @third_party_id, 'observacao' => "#{order.observation}", 'valorFrete' => 0, 'valorOutros' => 0, 'valorSeguro' => 0,
      'vendaConsumidorFinal' => order.client.is_a?(Customer), 'itens' => itens_data(order)
    }
  end

  def itens_data(order)
    return_array = []

    return_array << @products.map do |product|
      order_item = order.order_items.find_by(id: product[:id])

      if order_item.nil?
        { 'idProduto' => nil, 'quantidade' => 1, 'valorUnitario' => nil }
      else
        item_total    = order_item.total
        item_discount = order_item.total_discount

        if !order.special_discount.to_f.to_d.zero? && order.billing_client.allow_special_discount?
          item_with_discount = order_item.total_with_discount * (1 - (order.discount / 100))

          item_total = (item_with_discount * (1 - (order.special_discount / 100))).round(2)

          item_discount = 0
        elsif !order.discount.to_d.zero?
          item_discount = ((order_item.total_with_discount * (order.discount / 100)) + item_discount).round(2)
        end

        { 'idProduto' => product[:erp_id], 'quantidade' => 1, 'valorUnitario' => item_total.to_d, 'valorDesconto' => item_discount.to_d }
      end
    end

    return_array << @items.map do |item|
      i = order.items.find_by(id: item[:id])

      if i.nil?
        { 'idProduto' => nil, 'quantidade' => 1, 'valorUnitario' => nil }
      else
        item_value    = i.value
        item_discount = i.total_discount

        if !order.special_discount.to_f.to_d.zero? && order.billing_client.allow_special_discount?
          item_with_discount = (i.total_with_discount * (1 - (order.discount / 100))).round(2)

          item_value = ((item_with_discount * (1 - (order.special_discount / 100))) / i.quantity).round(2)

          item_discount = 0
        elsif !order.discount.to_d.zero?
          item_discount = (((i.total_with_discount * (order.discount / 100)) + item_discount) / i.quantity).round(2)
        end

        { 'idProduto' => item[:erp_id], 'quantidade' => i.quantity.to_d, 'valorUnitario' => item_value.to_d, 'valorDesconto' => item_discount.to_d }
      end
    end

    return_array.flatten
  end
end
