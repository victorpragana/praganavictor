class EstimatePriceCalculatorWorker
  include Sidekiq::Worker
  include SidekiqStatus::Worker

  sidekiq_options queue: :estimate, retry: 0, backtrace: true

  def perform(estimate_id)
    estimate = Estimate.find(estimate_id)

    estimate.update_attribute(:processing_message, [])

    reset_options_prices(estimate)

    estimate.processing!

    Redis.new.set("estimate_#{estimate_id}_error", false)
    ActiveSupport::Cache::FileStore.new("#{Rails.root.join('tmp', 'cache')}").clear

    service = EstimateService.new

    service.update_prices(estimate_id) do |current, total, item_id, item_name, worker_number|
      self.total = total

      at(current, { "#{worker_number}" => "Worker ##{worker_number}: Processando item #{item_id} (#{item_name})" }.to_json)
    end

    unless Rails.env.test?
      begin
        ActiveRecord::Base.connection.reconnect!
      rescue StandardError
        ActiveRecord::Base.connection.reconnect!
      end
    end

    estimate.update_attribute(:job_id, nil)

    remove_duplicate_packings(estimate)

    if service.success
      estimate.free!

      self.payload = "#{Time.now.strftime('%H:%M:%S')} - Processamento concluído com sucesso"
    else
      estimate.error!

      self.payload = "#{Time.now.strftime('%H:%M:%S')} - #{service.errors.join(', ')}"
    end
  end

  private

  def reset_options_prices(estimate)
    estimate.options.where(selected: true).update_all(factory_total: 0, distributor_total: 0, store_total: 0, customer_total: 0)
  end

  def remove_duplicate_packings(estimate)
    estimate.packing_configurations.select(:items).group(:items).pluck(:items).each do |packing|
      same_packings = estimate.packing_configurations.where("items = ARRAY#{packing}")

      same_packings.select(:component_name).group(:component_name).pluck(:component_name).each do |name|
        duplicated = same_packings.where(component_name: name)

        duplicated.where.not(id: duplicated.first.id).destroy_all
      end
    end
  end
end
