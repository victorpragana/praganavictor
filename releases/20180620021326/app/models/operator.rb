# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  provider               :string           default("email"), not null
#  uid                    :string           default(""), not null
#  email                  :string           default(""), not null
#  username               :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  name                   :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  tokens                 :json
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  type                   :string
#  maximum_discount       :decimal(5, 2)    default(0.0)
#  erp_id                 :integer
#  selected_workplace_id  :integer
#

class Operator < User
  include DeviseTokenAuth::Concerns::User

  before_validation :remove_not_used_information

  validate :no_discount_given
  validate :no_erp_id

  def permissions
    case self.workplace.try(:actable_type)
    when 'Factory'
      {
        "dashboard" => %w{index},
        "production_orders" => %w{index new edit}
      }
    when 'Distributor'
      {
        "dashboard" => %w{index},
        "production_orders" => %w{index new edit}
      }
    when 'Store'
      {
        "dashboard" => %w{index},
        "production_orders" => %w{index new edit}
      }
    else
      {
        
      }
    end
  end

  private
  def remove_not_used_information
    self.maximum_discount = 0
    self.erp_id           = nil
  end

  def no_discount_given
    errors.add(:base, I18n.t('activerecord.errors.models.operator.cannot_give_discount')) if !maximum_discount.zero?
  end

  def no_erp_id
    errors.add(:base, I18n.t('activerecord.errors.models.operator.cannot_sell')) if !erp_id.blank?
  end
end
