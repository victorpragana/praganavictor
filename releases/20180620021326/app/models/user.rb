# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  provider               :string           default("email"), not null
#  uid                    :string           default(""), not null
#  email                  :string           default(""), not null
#  username               :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  name                   :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  tokens                 :json
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  type                   :string
#  maximum_discount       :decimal(5, 2)    default(0.0)
#  erp_id                 :integer
#  selected_workplace_id  :integer
#

class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable

  has_many :workplaces, class_name: UserWorkplace, dependent: :destroy
  belongs_to :selected_workplace, class_name: UserWorkplace
  
  validates_uniqueness_of :email, case_sensitive: false
  validates :username, presence: true, uniqueness: { case_sensitive: false }, allow_blank: false
  validates_presence_of :name, allow_blank: false
  validates :type, presence: true, allow_blank: false, inclusion: { in: %w(Administrator Operator Manager Seller) }
  validate :allowed_workplace?, on: :update, if: Proc.new { |u| u.selected_workplace_id.present? }

  def workplace
    selected_workplace.try(:workplace)
  end

  def user_workplace_by_subdomain(subdomain)
    workplaces.detect { |user_workplace| user_workplace.subdomain == subdomain }
  end
 
  private
  def allowed_workplace?
    errors.add(:base, I18n.t('activerecord.errors.models.user.not_allowed_workplace')) unless workplaces.exists?(id: selected_workplace.id, user_id: selected_workplace.user_id)
  end
end
