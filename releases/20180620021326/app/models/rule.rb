# == Schema Information
#
# Table name: rules
#
#  id                   :integer          not null, primary key
#  component_id         :integer
#  width                :int4range
#  height               :int4range
#  control_method       :integer          not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  total_consumption    :string           default(""), not null
#  cost                 :string           default(""), not null
#  required             :boolean          default(TRUE), not null
#  height_consumption   :string
#  width_consumption    :string
#  line_rule_id         :integer
#  automatic            :boolean          default(FALSE)
#  quantity_consumption :string
#  description          :string
#  weight               :integer          default(0), not null
#  area                 :int4range
#

class Rule < ActiveRecord::Base
  include HasControlMethod
  include RuleConcerns
  include HasConversion
  include HasWeight

  belongs_to :component, required: true
  belongs_to :line_rule, required: true

  has_paper_trail

  has_many :option_components, dependent: :nullify
  has_many :questions, through: :line_rule

  validate :automatic_component?, unless: 'component_id.blank?'
  validate :automatic_line_rule?, unless: 'line_rule_id.blank?'
  validate :same_control_method_for_component, unless: 'component_id.blank? || line_rule_id.blank?'
  
  scope :for_component, ->(component_id) {where(component_id: component_id)}

  has_conversion_for :width, HasConversion::DIMENSION_CONVERSION
  has_conversion_for :height, HasConversion::DIMENSION_CONVERSION
  has_conversion_for :area, HasConversion::AREA_CONVERSION

  private

  def automatic_component?
    errors.add(:base, I18n.t('activerecord.errors.models.rule.not_automatic_for_automatic_component')) if self.component.automatic? && !self.automatic?
  end

  def automatic_line_rule?
    errors.add(:base, I18n.t('activerecord.errors.models.rule.not_automatic_for_automatic_line_rule')) if self.line_rule.automatic? && !self.automatic?
  end

  def same_control_method_for_component
    errors.add(:base, I18n.t('activerecord.errors.models.rule.different_control_methods')) if Rule.joins(:line_rule)
      .for_component(self.component_id).where(line_rules: { line_id: self.line_rule.line_id })
      .where.not(control_method: Rule.control_methods[self.control_method], id: self.id).exists?
  end

  def rule_order(rule)
    Line.find(self.line_rule.line_id).categories.find_by_category_id(rule.component.category_id).order
  end
end
