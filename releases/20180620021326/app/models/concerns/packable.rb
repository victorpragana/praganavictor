module Packable
  extend ActiveSupport::Concern

  included do
    has_many :packing_configurations, as: :packable
  end
end