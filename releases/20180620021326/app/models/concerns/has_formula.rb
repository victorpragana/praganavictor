module HasFormula
  extend ActiveSupport::Concern

  included do
    def validate_field_formula(field, variables)
      calculator = Dentaku::Calculator.new

      hash = {}

      variables.each do |v|
        hash[v.to_sym] = 1
      end
      
      calculator.evaluate!(self.send(field), hash)
    end
  end
end