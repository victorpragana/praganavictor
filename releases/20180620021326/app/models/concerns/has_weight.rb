module HasWeight
  extend ActiveSupport::Concern

  included do
    enum weight: {
      general: 0,
      light:   1,
      medium:  2,
      heavy:   3
    }

    validates :weight, presence: true
  end
end