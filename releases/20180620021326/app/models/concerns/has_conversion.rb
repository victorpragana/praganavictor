module HasConversion
  extend ActiveSupport::Concern

  DIMENSION_CONVERSION = 0
  AREA_CONVERSION      = 1

  class_methods do
    def has_conversion_for(field_name, type = DIMENSION_CONVERSION)
      if type == DIMENSION_CONVERSION
        define_method("#{field_name.to_s}_from_mm_to_cm") do
          value = send(field_name.to_sym)
          
          if (value.is_a?(Range))
            return (value.min.to_f / 10)..(value.max.to_f / 10)
          elsif (value.is_a?(Numeric))
            return value.to_f / 10
          end
        end
      elsif type == AREA_CONVERSION
        define_method("#{field_name.to_s}_from_mm2_to_cm2") do
          value = send(field_name.to_sym)
          
          if (value.is_a?(Range))
            return (value.min.to_f / 100)..(value.max.to_f / 100)
          elsif (value.is_a?(Numeric))
            return value.to_f / 100
          end
        end
      end
    end
  end
end