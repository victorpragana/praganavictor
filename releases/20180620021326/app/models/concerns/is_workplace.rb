module IsWorkplace
  extend ActiveSupport::Concern

  included do
    has_many :permitted_administrators, as: :workplace, dependent: :destroy, class_name: UserWorkplace
    has_many :permitted_managers, as: :workplace, dependent: :destroy, class_name: UserWorkplace
    has_many :permitted_sellers, as: :workplace, dependent: :destroy, class_name: UserWorkplace
    has_many :permitted_operators, as: :workplace, dependent: :destroy, class_name: UserWorkplace
    has_many :permitted_users, as: :workplace, dependent: :destroy, class_name: UserWorkplace

    has_many :administrators, through: :permitted_administrators, class_name: Administrator
    has_many :managers, through: :permitted_managers, class_name: Manager
    has_many :sellers, through: :permitted_sellers, class_name: Seller
    has_many :operators, through: :permitted_operators, class_name: Operator
    has_many :users, through: :permitted_users, class_name: User
  end
end