module BelongsToCategory
  extend ActiveSupport::Concern

  included do
    belongs_to :category, required: true
    scope :for_category, ->(category) { where(category_id: category) }
  end
end