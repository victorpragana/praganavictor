module ThirdPartyWithParanoiaConcern
  extend ActiveSupport::Concern

  included do
    acts_as_paranoid
    acts_as :third_party
    
    after_restore :restore_third_party

    private
      def restore_third_party
        ThirdParty.with_deleted.find_by(actable_type: self.class, actable_id: self.id).restore!
      end
  end
end