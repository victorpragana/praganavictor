# == Schema Information
#
# Table name: order_items
#
#  id                :integer          not null, primary key
#  line_id           :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  width             :integer          not null
#  height            :integer          not null
#  order_id          :integer
#  option_id         :integer
#  factory_total     :decimal(10, 2)   default(0.0)
#  distributor_total :decimal(10, 2)   default(0.0)
#  store_total       :decimal(10, 2)   default(0.0)
#  customer_total    :decimal(10, 2)   default(0.0)
#  discount          :decimal(5, 2)    default(0.0)
#

class OrderItem < ActiveRecord::Base
  include BelongsToLine
  include HasConversion
  include HasTotals

  belongs_to :order, required: true
  belongs_to :option, required: false

  delegate :client, to: :order
  delegate :seller, to: :order

  has_many :components, class_name: OrderItemComponent, dependent: :destroy

  validates :width, presence: true, numericality: { only_integer: true, greater_than: 0 }, allow_blank: false
  validates :height, presence: true, numericality: { only_integer: true, greater_than: 0 }, allow_blank: false
  validates_numericality_of :discount, greater_than_or_equal_to: 0, less_than_or_equal_to: 100
  validate :line_active?, if: Proc.new { |object| object.line.present? }

  has_conversion_for :width
  has_conversion_for :height

  def identification
    order_id.to_s.rjust(5, '0') + '-' + id.to_s.rjust(6, '0')
  end

  def specification
    components.where(required: false).order(:id).map(&:component_name).join(', ')
  end

  def name_for_erp
    "#{identification} #{line.name} #{width_from_mm_to_cm}cm x #{height_from_mm_to_cm}cm"
  end

  def total_cost
    return factory_total     if seller.is_a?(Factory)
    return distributor_total if seller.is_a?(Distributor)
    return store_total       if seller.is_a?(Store)
  end

  private
  def line_active?
    errors.add(:base, I18n.t('activerecord.errors.models.order_item.inactive_line', line: self.line.name)) if !self.line.active?
  end
end
