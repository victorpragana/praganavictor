# == Schema Information
#
# Table name: commercial_classifications
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class CommercialClassification < ActiveRecord::Base
  validates :name, presence: true, uniqueness: { case_sensitive: false }, allow_blank: false

  has_and_belongs_to_many :lines, validate: false
  has_many :distributors
  has_many :stores
end
