# == Schema Information
#
# Table name: movements
#
#  id             :integer          not null, primary key
#  owner_id       :integer          not null
#  order_id       :integer
#  payment_id     :integer
#  value          :decimal(8, 2)    not null
#  direction      :integer          not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  status         :integer          default(1), not null
#  date           :datetime         not null
#  type           :string
#  destination_id :integer          not null
#  reason         :integer          default(0), not null
#

class ProductionCredit < Movement
end
