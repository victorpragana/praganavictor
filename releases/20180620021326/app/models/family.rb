# == Schema Information
#
# Table name: families
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  color      :string(7)        not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Family < ActiveRecord::Base
  has_and_belongs_to_many :base_components
  has_many :lines
  has_and_belongs_to_many :components

  validates :name, presence: true, uniqueness: {case_sensitive: false}, allow_blank: false
  validates :color, color: true, presence: true, uniqueness: {case_sensitive: false}, allow_blank: false
end
