# == Schema Information
#
# Table name: states
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  acronym    :string           not null
#  ibge_code  :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class State < ActiveRecord::Base
  validates :name, presence: true, uniqueness: {case_sensitive: false}, allow_blank: false
  validates :ibge_code, presence: true, uniqueness: true, allow_blank: false
  validates :acronym, presence: true

  has_many :cities, dependent: :destroy
end
