# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  provider               :string           default("email"), not null
#  uid                    :string           default(""), not null
#  email                  :string           default(""), not null
#  username               :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  name                   :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  tokens                 :json
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  type                   :string
#  maximum_discount       :decimal(5, 2)    default(0.0)
#  erp_id                 :integer
#  selected_workplace_id  :integer
#

class Seller < User
  include DeviseTokenAuth::Concerns::User

  validates_numericality_of :maximum_discount, greater_than_or_equal_to: 0, less_than_or_equal_to: 100

  has_many :orders, dependent: :restrict_with_error, foreign_key: :user_id

  def permissions
    case self.workplace.try(:actable_type)
    when 'Factory'
      {
        'dashboard' => %w{index},
        'estimates' => %w{index new edit},
        'orders' => %w{index new edit},
        'stores' => %w{index new edit},
        'distributors' => %w{index new edit},
        'customers' => %w{index new edit}
      }
    when 'Distributor'
      {
        'dashboard' => %w{index},
        'estimates' => %w{index new edit},
        'orders' => %w{index edit},
        'stores' => %w{index new edit}
      }
    when 'Store'
      {
        'dashboard' => %w{index},
        'estimates' => %w{index new edit},
        'orders' => %w{index edit},
        'customers' => %w{index new edit}
      }
    else
      {
        
      }
    end
  end
end
