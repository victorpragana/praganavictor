# == Schema Information
#
# Table name: estimates
#
#  id                 :integer          not null, primary key
#  date               :date             not null
#  observation        :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  order_id           :integer
#  seller_id          :integer
#  seller_type        :string
#  client_id          :integer
#  client_type        :string
#  optimize           :boolean          default(FALSE)
#  discount           :decimal(5, 2)    default(0.0)
#  status             :integer          default(0), not null
#  job_id             :string
#  processing_message :text             default([]), is an Array
#

class Estimate < ActiveRecord::Base
  include Orderable
  include Packable

  has_many :environments, dependent: :destroy
  has_many :locations, through: :environments
  has_many :options, through: :locations
  has_many :components, through: :options
  has_many :items, class_name: EstimateItem, dependent: :destroy

  belongs_to :order

  validates_numericality_of :discount, greater_than_or_equal_to: 0, less_than_or_equal_to: 100

  enum status: {
    free:       0,
    enqueued:   1,
    processing: 2,
    error:      3
  }

  def express?
    true
  end

  def factory_total
    options_total = options.where(selected: true).sum(:factory_total)

    items_total = items.reduce(0) do |total, item|
      total += item.factory_total
    end

    options_total + items_total
  end

  def factory_total_discount
    options_total_discount = options.where(selected: true).sum('options.factory_total * (options.discount / 100)')

    items_total_discount = items.reduce(0) do |total, item|
      total += item.factory_total_discount
    end

    options_total_discount + items_total_discount
  end

  def factory_total_with_discount
    (factory_total - factory_total_discount).try(:round, 2)
  end

  def distributor_total
    options_total = options.where(selected: true).sum(:distributor_total)

    items_total = items.reduce(0) do |total, item|
      total += item.distributor_total
    end

    options_total + items_total
  end

  def distributor_total_discount
    options_total = options.where(selected: true).sum('options.distributor_total * (options.discount / 100)')

    items_total = items.reduce(0) do |total, item|
      total += item.distributor_total_discount
    end

    options_total + items_total
  end

  def distributor_total_with_discount
    (distributor_total - distributor_total_discount).try(:round, 2)
  end

  def store_total
    options_total = options.where(selected: true).sum(:store_total)

    items_total = items.reduce(0) do |total, item|
      total += item.store_total
    end

    options_total + items_total
  end

  def store_total_discount
    options_total = options.where(selected: true).sum('options.store_total * (options.discount / 100)')

    items_total = items.reduce(0) do |total, item|
      total += item.store_total_discount
    end

    options_total + items_total
  end

  def store_total_with_discount
    (store_total - store_total_discount).try(:round, 2)
  end

  def customer_total
    options_total = options.where(selected: true).sum(:customer_total)

    items_total = items.reduce(0) do |total, item|
      total += item.customer_total
    end

    options_total + items_total
  end

  def customer_total_discount
    options_total = options.where(selected: true).sum('options.customer_total * (options.discount / 100)')

    items_total = items.reduce(0) do |total, item|
      total += item.customer_total_discount
    end

    options_total + items_total
  end

  def customer_total_with_discount
    (customer_total - customer_total_discount).try(:round, 2)
  end

  def total
    return distributor_total if client.is_a?(Distributor)
    return store_total       if client.is_a?(Store)
    return customer_total    if client.is_a?(Customer)
  end

  def partial_total
    return distributor_total_with_discount if client.is_a?(Distributor)
    return store_total_with_discount       if client.is_a?(Store)
    return customer_total_with_discount    if client.is_a?(Customer)
  end

  def total_discount
    return (distributor_total_discount + (partial_total * (self.discount / 100))).try(:round, 2) if client.is_a?(Distributor)
    return (store_total_discount + (partial_total * (self.discount / 100))).try(:round, 2)       if client.is_a?(Store)
    return (customer_total_discount + (partial_total * (self.discount / 100))).try(:round, 2)    if client.is_a?(Customer)
  end

  def total_with_discount
    (partial_total * (1 - (self.discount / 100))).try(:round, 2)
  end
end
