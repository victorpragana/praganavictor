# == Schema Information
#
# Table name: base_component_values
#
#  id                      :integer          not null, primary key
#  base_component_id       :integer
#  value_id                :integer
#  base_component_value_id :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  deleted_at              :datetime
#

class BaseComponentValue < ActiveRecord::Base
  acts_as_paranoid
  
  belongs_to :base_component, required: true
  belongs_to :value, -> { with_deleted }, required: true
  belongs_to :parent, class_name: self, foreign_key: 'base_component_value_id'

  delegate :specification_id, to: :value
  delegate :automatic?, to: :value

  has_many :children, class_name: self, foreign_key: 'base_component_value_id'

  scope :only_parents, -> { where(base_component_value_id: nil) }
end
