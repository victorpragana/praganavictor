# == Schema Information
#
# Table name: options
#
#  id                :integer          not null, primary key
#  line_id           :integer
#  location_id       :integer
#  width             :integer          not null
#  height            :integer          not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  selected          :boolean          default(FALSE), not null
#  factory_total     :decimal(10, 2)   default(0.0)
#  distributor_total :decimal(10, 2)   default(0.0)
#  store_total       :decimal(10, 2)   default(0.0)
#  customer_total    :decimal(10, 2)   default(0.0)
#  discount          :decimal(5, 2)    default(0.0)
#  questions         :json
#

class Option < ActiveRecord::Base
  include BelongsToLine
  include HasConversion
  include HasTotals
  
  belongs_to :location, required: true
  
  delegate :client, to: :location
  delegate :seller, to: :location

  has_one :order_item
  has_many :components, class_name: OptionComponent, dependent: :destroy

  scope :selected, -> { where(selected: true) }

  validates :width, presence: true, numericality: { only_integer: true, greater_than: 0 }, allow_blank: false
  validates :height, presence: true, numericality: { only_integer: true, greater_than: 0 }, allow_blank: false
  validates :selected, inclusion: { in: [true, false] }
  validates_numericality_of :discount, greater_than_or_equal_to: 0, less_than_or_equal_to: 100
  validate :discount_allowed?, unless: Proc.new{ |o| o.location_id.blank? || o.line_id.blank? }

  has_conversion_for :width
  has_conversion_for :height

  def parsed_questions
    return [] if questions.nil?

    questions.map do |hash|
      question = Question.find_by(value: hash[0])

      { 'label' => question.label, 'value' => hash[1] }
    end
  end

  private

  def discount_allowed?
    price_table = client.price_table_for(seller)

    price_table_for_line = PriceTableLine.select(:maximum_discount).find_by(line_id: line.id, price_table_id: price_table.try(:id))

    maximum_discount = price_table_for_line.try(:maximum_discount) || 0

    errors.add(:base, I18n.t('activerecord.errors.models.option.discount_greater_than_price_table')) if maximum_discount < self.discount
  end
end
