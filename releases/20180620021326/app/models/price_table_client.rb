# == Schema Information
#
# Table name: price_table_clients
#
#  id             :integer          not null, primary key
#  price_table_id :integer
#  client_id      :integer
#  client_type    :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class PriceTableClient < ActiveRecord::Base
  belongs_to :price_table, required: true

  belongs_to :client, polymorphic: true, required: true

  has_paper_trail

  validate :present_on_two_price_tables?, unless: 'price_table.blank?'

  def client_type=(class_name)
    super(class_name.constantize.base_class.to_s)
  end

  private
    def present_on_two_price_tables?
      if PriceTableClient.joins(:price_table).where(client: client, price_tables: {owner_id: price_table.owner_id, owner_type: price_table.owner_type, type: PriceTable.types[price_table.type]}).where.not(id: self.id).exists?
        errors.add(:base, I18n.t('activerecord.errors.models.price_table_client.can_not_be_in_multiple_tables'))
      end
    end
end
