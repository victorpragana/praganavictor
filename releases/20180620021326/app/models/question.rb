# == Schema Information
#
# Table name: questions
#
#  id         :integer          not null, primary key
#  label      :string           not null
#  value      :string           not null
#  type       :integer          not null
#  options    :json
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Question < ActiveRecord::Base
  self.inheritance_column = :_type_disabled

  OPTIONS_JSON_SCHEMA = {
    type: 'array',
    items: {
      type: 'object',
      additionalProperties: false,
      properties: {
        label: { type: 'string' },
        value: { type: 'string', pattern: '^[a-zA-Z_-]*$' }
      },
      required: %w[label value]
    }
  }.freeze

  has_and_belongs_to_many :lines

  before_validation :remove_trailing_underscore, if: proc { |f| f.value.present? }
  before_validation :remove_options, unless: proc { |f| f.dropdown? }

  validates :label, presence: true, allow_blank: false
  validates :value, presence: true, allow_blank: false, format: { with: /\A[a-zA-Z_]*\z/ }
  validates :type, presence: true, allow_blank: false

  validates :options, presence: true, allow_blank: false, json: { schema: OPTIONS_JSON_SCHEMA }, if: proc { |f| f.dropdown? }
  validates :options, absence: true, unless: proc { |f| f.dropdown? }

  enum type: {
    boolean:  0,
    dropdown: 1
  }

  def as_variable
    "_#{value}_"
  end

  def options
    self[:options]&.sort_by { |option| option['label'] }
  end

  private

  def remove_trailing_underscore
    self.value = value.gsub(/^_/, '').gsub(/_$/, '').upcase
  end

  def remove_options
    self.options = nil
  end
end
