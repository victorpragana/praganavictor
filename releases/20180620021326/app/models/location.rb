# == Schema Information
#
# Table name: locations
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  category       :integer          not null
#  environment_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  width          :integer
#  height         :integer
#  quantity       :integer
#

class Location < ActiveRecord::Base
  include HasConversion

  belongs_to :environment, required: true

  delegate :estimate_id, to: :environment
  delegate :client, to: :environment
  delegate :seller, to: :environment

  has_many :options, dependent: :destroy

  enum category: {
    wall:     0,
    opening:  1,
    floor:    2
  }

  validates_presence_of :category, allow_blank: false
  validates_numericality_of :width, only_integer: true, allow_blank: true, greater_than: 0
  validates_numericality_of :height, only_integer: true, allow_blank: true, greater_than: 0
  validates_numericality_of :quantity, only_integer: true, allow_blank: true, greater_than: 0

  has_conversion_for :width
  has_conversion_for :height

  def total
    options.reduce(0) { |total, o| total += o.total }
  end

  def total_with_discount
    options.reduce(0) { |total, o| total += (o.total * (1 - (o.discount / 100))) }.try(:round, 2)
  end

  def discount
    options.first.try(:discount) || 0
  end
end
