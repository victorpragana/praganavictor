# == Schema Information
#
# Table name: price_table_items
#
#  id               :integer          not null, primary key
#  price_table_id   :integer
#  item_id          :integer
#  price            :decimal(8, 2)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  maximum_discount :decimal(5, 2)    default(0.0)
#

class PriceTableItem < ActiveRecord::Base
  belongs_to :price_table, required: true
  belongs_to :item, required: true

  has_paper_trail

  validates :price, numericality: { greater_than: 0 }, allow_blank: true
  validate :price_table_for_lines?, unless: 'price_table_id.blank?'
  validates_numericality_of :maximum_discount, greater_than_or_equal_to: 0, less_than_or_equal_to: 100

  private
  def price_table_for_lines?
    errors.add(:base, I18n.t('activerecord.errors.models.price_table_item.price_table_for_lines')) if price_table.for_lines?
  end
end
