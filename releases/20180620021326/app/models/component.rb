# == Schema Information
#
# Table name: components
#
#  id                        :integer          not null, primary key
#  name                      :string(255)      not null
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  cost                      :decimal(10, 5)   default(0.0), not null
#  erasable                  :boolean          default(TRUE)
#  category_id               :integer
#  characteristics           :text
#  code                      :string(20)
#  unit                      :integer          default(2), not null
#  deleted_at                :datetime
#  base_component_id         :integer
#  reference_cost            :decimal(10, 5)   default(0.0)
#  offset_left               :integer          default(0)
#  offset_top                :integer          default(0)
#  offset_right              :integer          default(0)
#  offset_bottom             :integer          default(0)
#  allow_rotation            :boolean          default(FALSE)
#  exchange                  :decimal(10, 5)
#  loss                      :decimal(10, 5)
#  cut_table_id              :integer
#  print_on_lines_label      :boolean          default(FALSE)
#  fix_cost                  :decimal(10, 5)   default(0.0)
#  print_on_components_label :boolean          default(FALSE)
#

class Component < ActiveRecord::Base
  include BelongsToCategory
  include HasUnit
  include HasConversion
  include BelongsToCutTable
  include TranslateEnum

  translate_enum :unit

  acts_as_paranoid

  has_paper_trail

  has_and_belongs_to_many :families
  has_and_belongs_to_many :values, -> { with_deleted }
  has_and_belongs_to_many :option_components
  
  has_and_belongs_to_many :parents, class_name: Component, join_table: 'components_components', foreign_key: :child_id, association_foreign_key: :parent_id
  has_and_belongs_to_many :children, class_name: Component, join_table: 'components_components', foreign_key: :parent_id, association_foreign_key: :child_id

  has_many :rules, dependent: :destroy
  has_many :line_rules, through: :rules
  has_many :lines, through: :line_rules
  belongs_to :base, class_name: 'BaseComponent', foreign_key: 'base_component_id'

  validates :name, presence: { allow_blank: false }, uniqueness: { case_sensitive: false, scope: :category_id }, length: 1..255
  validates :cost, presence: { allow_blank: false }, numericality: { greater_than_or_equal_to: 0 }
  validates :reference_cost, numericality: { greater_than_or_equal_to: 0 }
  validates :fix_cost, numericality: { greater_than_or_equal_to: 0 }
  validates :code, length: 0..20, allow_blank: true
  validates :unit, presence: { allow_blank: false }
  validates :offset_left, numericality: { greater_than_or_equal_to: 0, only_integer: true }
  validates :offset_top, numericality: { greater_than_or_equal_to: 0, only_integer: true }
  validates :offset_right, numericality: { greater_than_or_equal_to: 0, only_integer: true }
  validates :offset_bottom, numericality: { greater_than_or_equal_to: 0, only_integer: true }
  validate :same_cut_table?, if: Proc.new { |object| object.cut_table_id.present? && base.present? }

  after_update :remove_different_category_rules, if: Proc.new{ |c| c.category_id_changed? }

  scope :user_components, -> { where(erasable: true) }

  has_conversion_for :offset_left
  has_conversion_for :offset_top
  has_conversion_for :offset_right
  has_conversion_for :offset_bottom

  attr_accessor :rule_required, :component_ids, :informed_consumption, :rule_id

  def final_cost(new_cost = nil)
    exchange = self.exchange.blank? || self.exchange.zero? ? 1 : self.exchange
    loss     = self.loss.blank? || self.loss.zero? ? 1 : self.loss

    return (cost * exchange * loss).try(:round, 5) if new_cost.nil?
    return (new_cost * exchange * loss).try(:round, 5)
  end

  def final_fix_cost
    exchange = self.exchange.blank? || self.exchange.zero? ? 1 : self.exchange
    loss     = self.loss.blank? || self.loss.zero? ? 1 : self.loss

    return (fix_cost.to_f * exchange * loss).try(:round, 5)
  end

  def automatic?
    return Specification.select(:automatic).joins(values: :components).where(components_values: { component_id: self.id}).pluck(:automatic).include?(true)
  end

  def automatic_value
    return 0 unless automatic?
    return self.values.where.not(comparison_value: nil).pluck(:comparison_value).first
  end

  def name_without_automatic_value
    return name if !automatic? || base_component_id.blank?

    return Value.select(:value).joins(:specification).where(specifications: { automatic: false }).where(id: self.value_ids).pluck(:value).join(' ')
  end

  private
  def same_cut_table?
    errors.add(:base, I18n.t('activerecord.errors.models.component.different_cut_table')) if base.components.pluck(:cut_table_id).any?{|id| !id.nil? && id != self.cut_table_id}
  end

  def remove_different_category_rules
    Rule.joins(:line_rule).where(component_id: self.id).where(line_rules: { category_id: self.category_id_was }).destroy_all
  end
end
