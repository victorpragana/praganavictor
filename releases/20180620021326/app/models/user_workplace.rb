# == Schema Information
#
# Table name: user_workplaces
#
#  id             :integer          not null, primary key
#  user_id        :integer
#  workplace_id   :integer
#  workplace_type :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class UserWorkplace < ActiveRecord::Base
  belongs_to :user, required: true
  belongs_to :workplace, polymorphic: true, required: true
  delegate :subdomain, to: :workplace

  validate :operator_and_not_factory?, if: Proc.new { |user_workplace| user_workplace.user.present? && user_workplace.workplace.present? }

  private
  def operator_and_not_factory?
    errors.add(:base, I18n.t('activerecord.errors.models.user_workplace.operator_must_be_at_factory')) if user.is_a?(Operator) && workplace.actable_type != 'Factory'
  end
end
