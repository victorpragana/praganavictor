# == Schema Information
#
# Table name: line_rules
#
#  id                   :integer          not null, primary key
#  line_id              :integer
#  width                :int4range
#  height               :int4range
#  control_method       :integer          not null
#  required             :boolean          default(TRUE), not null
#  cost                 :string           not null
#  height_consumption   :string
#  width_consumption    :string
#  total_consumption    :string           not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  automatic            :boolean          default(FALSE)
#  quantity_consumption :string
#  description          :string
#  category_id          :integer
#  weight               :integer          default(0), not null
#  area                 :int4range
#

class LineRule < ActiveRecord::Base
  include BelongsToLine
  include HasControlMethod
  include RuleConcerns
  include HasConversion
  include HasWeight

  belongs_to :category, required: false

  has_paper_trail
  
  scope :for_category, ->(category) { where(category_id: category) }

  has_many :rules, dependent: :destroy
  has_many :components, through: :rules
  has_many :questions, through: :line

  has_conversion_for :width, HasConversion::DIMENSION_CONVERSION
  has_conversion_for :height, HasConversion::DIMENSION_CONVERSION
  has_conversion_for :area, HasConversion::AREA_CONVERSION
end
