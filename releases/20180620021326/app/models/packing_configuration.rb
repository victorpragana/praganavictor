# == Schema Information
#
# Table name: packing_configurations
#
#  id                   :integer          not null, primary key
#  packable_id          :integer
#  packable_type        :string
#  items                :integer          is an Array
#  automatic_dimension  :decimal(10, 5)   not null
#  component_dimension  :decimal(10, 5)   not null
#  component_name       :string           not null
#  unit                 :integer          not null
#  packing_file_name    :string
#  packing_content_type :string
#  packing_file_size    :integer
#  packing_updated_at   :datetime
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class PackingConfiguration < ActiveRecord::Base
  include HasUnit

  belongs_to :packable, polymorphic: true, required: true

  validates :automatic_dimension, presence: true, numericality: { greater_than: 0 }, allow_blank: false
  validates :component_dimension, presence: true, numericality: { greater_than: 0 }, allow_blank: false
  validates :component_name, presence: true, allow_blank: false
  validates :unit, presence: true, allow_blank: false

  has_attached_file :packing
  validates_attachment_content_type :packing, content_type: /\Aimage\/.*\Z/
end
