# == Schema Information
#
# Table name: distributors
#
#  id                           :integer          not null, primary key
#  deleted_at                   :datetime
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  commercial_classification_id :integer
#

class Distributor < ActiveRecord::Base
  include ThirdPartyWithParanoiaConcern
  include IsPriceTableClient
  include IsWorkplace

  has_many :stores
  has_many :customers, through: :stores
  has_many :price_tables, as: :owner, dependent: :destroy

  has_many :purchasing_estimates, as: :client, dependent: :destroy, class_name: Estimate
  has_many :selling_estimates, as: :seller, dependent: :destroy, class_name: Estimate

  has_many :purchasing_orders, as: :client, dependent: :destroy, class_name: Order
  has_many :selling_orders, as: :seller, dependent: :destroy, class_name: Order

  belongs_to :commercial_classification
  has_many :lines, through: :commercial_classification

  def owner
    Factory.first
  end

  def clients
    stores.distinct.to_a.concat(customers.distinct.to_a)
  end

  def workplaces
    [self].concat(stores)
  end

  def estimates
    Estimate.where(seller_id: workplaces.map{|w| w.third_party.actable_id}, seller_type: workplaces.map{|w| w.third_party.actable_type})
  end

  def orders
    Order.where(seller_id: workplaces.map{|w| w.third_party.actable_id}, seller_type: workplaces.map{|w| w.third_party.actable_type})
  end
end
