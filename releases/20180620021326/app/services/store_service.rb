class StoreService
  attr_reader :success, :errors, :record

  def initialize(parameters)
    @parameters = parameters

    @parameters['bank'] = @parameters['bank'].to_i if @parameters['bank'].present?
  end

  def update(store_id)
    ActiveRecord::Base.transaction do
      store = Store.find(store_id)

      store.assign_attributes(@parameters)

      save_record(store)
    end
  end

  def create
    ActiveRecord::Base.transaction do
      store = Store.with_deleted.find_or_initialize_by(name: @parameters['name']).tap do |s|
        s.assign_attributes(@parameters)
        s.deleted_at = nil
      end

      save_record(store)
    end
  end

  private

  def save_record(store)
    if store.save
      @success = true
      @record = store.reload
    else
      @success = false
      @errors = store.errors.full_messages
    end
  end
end