class CustomerService
  attr_reader :success, :errors, :record

  def initialize(parameters)
    @parameters = parameters
  end

  def update(customer_id)
    ActiveRecord::Base.transaction do
      customer = Customer.find(customer_id)

      customer.assign_attributes(@parameters)

      save_record(customer)
    end
  end

  def create
    ActiveRecord::Base.transaction do
      customer = Customer.with_deleted.find_or_initialize_by(name: @parameters['name']).tap do |s|
        s.assign_attributes(@parameters)
        s.deleted_at = nil
      end

      save_record(customer)
    end
  end

  private
  def save_record(customer)
    if customer.save
      @success = true
      @record = customer.reload
    else
      @success = false
      @errors = customer.errors.full_messages
    end
  end
end