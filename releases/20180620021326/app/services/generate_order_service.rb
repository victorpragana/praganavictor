class GenerateOrderService
  attr_reader :success, :errors, :record

  def initialize(parameters)
    @parameters = parameters
  end

  def perform
    begin
      ActiveRecord::Base.transaction do
        order = Order.new(estimate.attributes.slice('seller_id', 'seller_type', 'client_id', 'client_type', 'observation', 'discount'))

        order.order_items = estimate.options.where(selected: true).map do |option|
          order_item = OrderItem.new(option.attributes.slice('line_id', 'width', 'height', 'factory_total', 'distributor_total', 
            'store_total', 'customer_total', 'discount'))
          order_item.option_id = option.id

          order_item.components = option.components.map do |component|
            OrderItemComponent.new(component.attributes.slice('total_consumption', 'unit_value', 'component_name', 'unit', 
              'component_characteristics', 'category_name', 'width_consumption', 'height_consumption', 'quantity_consumption', 'required', 'component_id'))
          end

          order_item
        end

        order.items = estimate.items.map do |estimate_item|
          OrderLooseItem.new(estimate_item.attributes.slice('item_id', 'item_code', 'item_description', 'unit', 'quantity', 
            'factory_value', 'distributor_value', 'store_value', 'customer_value', 'type', 'discount'))
        end

        order.date    = Date.today
        order.status  = Order.statuses[:to_send]
        order.production_status = order.seller.is_a?(Store) ? Order.production_statuses[:not_started] : Order.production_statuses[:sent_to_factory]
        order.user_id = @parameters['user_id']
        order.billing_client_id   = order.client_id
        order.billing_client_type = order.client_type

        order.save!

        estimate.packing_configurations.find_each do |packing|
          new_packing = PackingConfiguration.new(packing.attributes.slice('automatic_dimension', 'component_dimension', 'items', 'component_name', 'unit'))

          new_packing.items = new_packing.items.map do |option_id|
            order.order_items.find_by(option_id: option_id).try(:id)
          end

          new_packing.packing = packing.packing
          new_packing.packable = order

          new_packing.save!
        end

        estimate.order_id = order.id

        estimate.save!
        
        TrelloIntegrationService.new.create_trello_card(order)

        @success = true
        @record = order.reload

        rename_uploaded_files
      end
    rescue => e
      Rails.logger.error e.backtrace
      @success = false
      @errors = [e.message]
    end
  end

  private

  def estimate
    @estimate ||= Estimate.find(@parameters['estimate_id'])
  end

  def rename_uploaded_files
    old_names = []

    @record.packing_configurations.each_with_index do |packing, index|
      new_file_name = "order_#{@record.id}_#{index + 1}.svg"

      old_name = nil

      (packing.packing.styles.keys + [:original]).each do |style|
        path = packing.packing.path(style)
        FileUtils.cp(path, File.join(File.dirname(path), new_file_name))
        old_name = packing.packing.path
        old_names << old_name
      end

      packing.packing_file_name = new_file_name
      packing.save!
    end

    FileUtils.rm old_names, force: true
  end
end