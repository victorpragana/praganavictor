class CreditAntecipationService
  include ActiveModel::Validations
  extend ActiveModel::Translation

  validate :has_movement_in_the_past?, if: proc { |service| service.movement_ids.present? }
  validate :has_valid_reason?, if: proc { |service| service.movement_ids.present? }
  validate :has_output_movement?, if: proc { |service| service.movement_ids.present? }
  validate :has_unapproved_movement?, if: proc { |service| service.movement_ids.present? }
  validate :has_movement_with_different_owner?, if: proc { |service| service.movement_ids.present? }
  validate :has_movement_not_belonging_to_owner?, if: proc { |service| service.movement_ids.present? }
  validate :has_movement_with_different_destination?, if: proc { |service| service.movement_ids.present? }
  validate :exists_payment_condition?, if: proc { |service| service.movement_ids.present? }

  attr_reader :movement_ids, :owner_id

  def initialize(movement_ids, owner_id)
    @movement_ids = movement_ids
    @owner_id = owner_id
  end

  def antecipate
    if valid?
      ActiveRecord::Base.transaction do
        process_production_credits if !production_credits.empty?
        process_money_credits if !money_credits.empty?
      end

      true
    else
      false
    end
  end

  private

  def process_production_credits
    total_value = production_credits.sum(:value)

    ProductionCredit.create!(date: Time.zone.now, owner: production_credits.first.owner, destination: production_credits.first.destination,
      value: -1 * total_value, direction: :output, reason: Movement.reasons[:antecipation])

    condition = payment_condition(total_months(production_credits))

    ProductionCredit.create!(date: Time.zone.now, owner: production_credits.first.owner, destination: production_credits.first.destination,
      value: condition.apply_antecipation_tax(total_value), direction: :input, reason: Movement.reasons[:antecipation])

    production_credits.each(&:antecipated!)
  end

  def process_money_credits
    total_value = money_credits.sum(:value)

    MoneyCredit.create!(date: Time.zone.now, owner: money_credits.first.owner, destination: money_credits.first.destination,
      value: -1 * total_value, direction: :output, reason: Movement.reasons[:antecipation])

    if money_credits.first.owner.specific.is_a?(Store)
      MoneyCredit.create!(date: Time.zone.now, owner: money_credits.first.destination, destination: factory.third_party,
        value: -1 * total_value, direction: :output, reason: Movement.reasons[:store_antecipation])
    end

    condition = payment_condition(total_months(money_credits))

    MoneyCredit.create!(date: Time.zone.now, owner: money_credits.first.owner, destination: money_credits.first.destination,
      value: condition.apply_credit_antecipation_tax(total_value), direction: :input, reason: Movement.reasons[:antecipation])

    if money_credits.first.owner.specific.is_a?(Store)
      MoneyCredit.create!(date: Time.zone.now, owner: money_credits.first.destination, destination: factory.third_party,
        value: condition.apply_credit_antecipation_tax(total_value), direction: :input, reason: Movement.reasons[:store_antecipation])

      money_credits.each{ |credit| credit.equivalent_movement.try(:antecipated!) }
    end

    money_credits.each(&:antecipated!)
  end

  def movements
    @movements ||= Movement.where(id: movement_ids)
  end

  def production_credits
    @production_credits ||= ProductionCredit.where(id: movement_ids)
  end

  def money_credits
    @money_credits ||= MoneyCredit.where(id: movement_ids)
  end

  def total_months(_movements)
    latest_date = _movements.order(date: :desc).first.date
    total_months = ((latest_date - Time.zone.now).round / 1.day) / 30

    total_months.zero? ? 1 : total_months
  end

  def payment_condition(installments)
    PaymentCondition.credit_card.find_by(installments: installments)
  end

  def factory
    @factory ||= Factory.first
  end

  def has_movement_in_the_past?
    errors.add(:base, 'Apenas créditos futuros podem ser antecipados') if movements.any?{ |movement| movement.date.past? }
  end

  def has_valid_reason?
    errors.add(:base, 'Créditos oriundos de loja não podem ser antecipados') if movements.where.not(reason: [Movement.reasons[:credit], Movement.reasons[:order_payment]]).exists?
  end

  def has_output_movement?
    errors.add(:base, 'Apenas créditos podem ser antecipados') if movements.output.exists?
  end

  def has_unapproved_movement?
    errors.add(:base, 'Apenas créditos aprovados podem ser antecipados') if movements.pending.exists? || movements.antecipated.exists?
  end

  def has_movement_with_different_owner?
    errors.add(:base, 'Os créditos devem pertencer ao mesmo dono') if movements.pluck(:owner_id).uniq.size > 1
  end

  def has_movement_not_belonging_to_owner?
    errors.add(:base, 'Não é permitido antecipar créditos de outro dono') if movements.pluck(:owner_id).any? { |_owner_id| _owner_id != owner_id }
  end

  def has_movement_with_different_destination?
    errors.add(:base, 'Os créditos devem pertencer ao mesmo destinatário') if movements.pluck(:destination_id).uniq.size > 1
  end

  def exists_payment_condition?
    if production_credits.present?
      months = total_months(production_credits)

      errors.add(:base, "Não existe uma condição de pagamento para #{months} vezes") unless payment_condition(months).present?
    end

    if money_credits.present?
      months = total_months(money_credits)

      errors.add(:base, "Não existe uma condição de pagamento para #{months} vezes") unless payment_condition(months).present?
    end
  end
end