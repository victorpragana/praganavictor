class OrderLabelService
  include ActionView::Helpers::NumberHelper

  STRING_RANGE = 0..30

  attr_reader :success, :errors

  def initialize(order_id, user_id = nil)
    @order_id = order_id
    @user_id = user_id
  end

  def lines_label
    if(!printer)
      @errors = ['Não há impressora padrão configurada.']
      @success = false

      return
    end

    order_items = order.order_items.joins(line: [:family]).order(:line_id).order("lines.family_id")

    labels = ""

    total = order_items.count
    count = 1
    
    order_items.each_slice(2) do |_order_items|
      label_y = 15
      label_x = 23

      label = Zebra::Epl::Label.new print_density: 5, print_speed: 2, width: 1200, length: 80

      generate_order_item_data(label, _order_items[0], label_x, label_y, "#{count} de #{total}")

      count = count + 1

      if (_order_items[1].present?)
        label_y = 15
        label_x = 447

        generate_order_item_data(label, _order_items[1], label_x, label_y, "#{count} de #{total}")

        count = count + 1
      end
      
      label_contents = StringIO.new
      label.dump_contents(label_contents)

      labels << label_contents.string

      print_on_local_printer(label) if Rails.application.config.local_label_printer
    end

    print_label(labels) if !Rails.application.config.local_label_printer

    @success = true
  end

  def components_label
    components = order.components.joins(:component).where(components: { print_on_components_label: true }).order("components.category_id").order(:order_item_id)
    labels = ""

    components.each_slice(2) do |_components|
      label_y = 15
      label_x = 35

      label = Zebra::Epl::Label.new print_density: 5, print_speed: 2, width: 1150, length: 80

      generate_component_item_data(label, _components.first, label_x, label_y)

      if (_components.second.present?)
        label_y = 15
        label_x = 445

        generate_component_item_data(label,  _components.second, label_x, label_y)
      end
      label_contents = StringIO.new
      label.dump_contents(label_contents)

      labels << label_contents.string

      print_on_local_printer(label) if Rails.application.config.local_label_printer
    end
    
    print_label(labels) if !Rails.application.config.local_label_printer

    @success = true
  end

  private

  def print_on_local_printer(label)
    print_job = Zebra::PrintJob.new "Zebra_TLP2844"
    print_job.print label
  end

  def normalize_text(text)
    I18n.transliterate(text.to_s)
  end

  def generate_component_item_data(label, component, label_x, label_y)
    label << Zebra::Epl::Text.new(data: "##{component.order_item.identification}"[STRING_RANGE], position: [label_x, label_y], font: Zebra::Epl::Font::SIZE_2)
    label_y = label_y + 25

    label << Zebra::Epl::Text.new(data: "#{component.component_name}"[STRING_RANGE], position: [label_x, label_y], font: Zebra::Epl::Font::SIZE_2)
    label_y = label_y + 25

    label << Zebra::Epl::Text.new(data: "#{number_with_precision(component.total_consumption, precision: 2, separator: ',')} #{component.component.translated_unit}"[STRING_RANGE], position: [label_x, label_y], font: Zebra::Epl::Font::SIZE_2)
  end

  def generate_order_item_data(label, order_item, label_x, label_y, count)
    label << Zebra::Epl::Text.new(data: "#{count}"[STRING_RANGE], position: [label_x, label_y], font: Zebra::Epl::Font::SIZE_2)
    label_y = label_y + 25

    label << Zebra::Epl::Text.new(data: "##{order_item.identification}"[STRING_RANGE], position: [label_x, label_y], font: Zebra::Epl::Font::SIZE_2)
    label_y = label_y + 25

    if order_item.line.name.size > STRING_RANGE.max
      label << Zebra::Epl::Text.new(data: "#{order_item.line.name}"[STRING_RANGE], position: [label_x, label_y], font: Zebra::Epl::Font::SIZE_2)
      label_y = label_y + 25
      label << Zebra::Epl::Text.new(data: "#{order_item.line.name}"[(STRING_RANGE.max + 1)..-1], position: [label_x, label_y], font: Zebra::Epl::Font::SIZE_2)
      label_y = label_y + 25
    else
      label << Zebra::Epl::Text.new(data: "#{order_item.line.name}"[STRING_RANGE], position: [label_x, label_y], font: Zebra::Epl::Font::SIZE_2)
      label_y = label_y + 25
    end

    label << Zebra::Epl::Text.new(data: "#{normalize_text(order.seller.name)}"[STRING_RANGE], position: [label_x, label_y], font: Zebra::Epl::Font::SIZE_2)
    label_y = label_y + 25

    label << Zebra::Epl::Text.new(data: "#{normalize_text(order.client.name)}"[STRING_RANGE], position: [label_x, label_y], font: Zebra::Epl::Font::SIZE_2)
    label_y = label_y + 25

    label << Zebra::Epl::Text.new(data: "#{normalize_text(order_item.option.location.environment.translated_category)} #{normalize_text(order_item.option.location.environment.name)}"[STRING_RANGE], position: [label_x, label_y], font: Zebra::Epl::Font::SIZE_2)
    label_y = label_y + 25

    label << Zebra::Epl::Text.new(data: "#{number_with_precision(order_item.width_from_mm_to_cm, precision: 2, separator: ',')} cm x #{number_with_precision(order_item.height_from_mm_to_cm, precision: 2, separator: ',')} cm"[STRING_RANGE], position: [label_x, label_y], font: Zebra::Epl::Font::SIZE_4)
    label_y = label_y + 25

    components_label_for_order_item(order_item).scan(/.{1,#{STRING_RANGE.max}}/).each do |components|
      label << Zebra::Epl::Text.new(data: "#{components}"[STRING_RANGE], position: [label_x, label_y], font: Zebra::Epl::Font::SIZE_2)
      label_y = label_y + 25
    end
  end
  
  def components_label_for_order_item(order_item)
    order_item.components.joins(:component).where(components: {print_on_lines_label: true}).map(&:component).map(&:name).join(',')
  end

  def order
    @order ||= Order.find(@order_id)
  end

  def print_label(contents)
    printer.print(contents, @user_id)
  end

  def printer
    @printer ||= PrinterService.new.default_printer
  end
end