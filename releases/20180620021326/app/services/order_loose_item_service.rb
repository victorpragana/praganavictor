class OrderLooseItemService
  attr_reader :success, :errors, :record

  def initialize(parameters)
    @parameters = parameters
  end

  def apply_value(order_loose_item_id)
    item = OrderLooseItem.find(order_loose_item_id)
    
    item.send("#{item.order.client.class.name.underscore}_value=", @parameters['value'])

    save_record(item)
  end



  private
  def save_record(record)
    if record.save
      @success = true
      @record = record.reload
    else
      @success = false
      @errors = record.errors.full_messages
    end
  end
end