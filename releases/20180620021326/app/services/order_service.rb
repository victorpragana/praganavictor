class OrderService
  attr_reader :success, :errors, :record

  def initialize(parameters = {})
    @parameters = parameters
  end

  def destroy(order_id)
    order = Order.find(order_id)
    
    TrelloIntegrationService.new.delete_trello_card(order)

    if order.destroy
      @success = true
    else
      @success = false
      @errors  = order.errors.full_messages
    end
  end

  def change_billing_client(order_id)
    order = Order.find(order_id)

    order.assign_attributes(billing_client_id: @parameters['billing_client_id'], billing_client_type: @parameters['billing_client_type'])

    if order.save
      @success = true
      @record  = order.reload
    else
      @success = false
      @errors  = order.errors.full_messages
    end
  end

  def update_special_discount(order_id)
    order = Order.find(order_id)

    order.assign_attributes(special_discount: @parameters['special_discount'])

    if order.save
      @success = true
      @record  = order.reload
    else
      @success = false
      @errors  = order.errors.full_messages
    end
  end

  def update_production_status(order_id)
    begin
      ActiveRecord::Base.transaction do
        order = Order.find(order_id)

        order.send("#{@parameters['transition']}!")

        @success = true
        @record  = order.reload
      end
    rescue => e
      @success = false
      @errors = [e.message]
    end
  end
end