class OrderCutMapService
  include ActionView::Helpers::NumberHelper

  REPORT_PATH = 'system/plano_de_corte.pdf'
  COLUMNS = 3
  ROWS = 3

  GUTTER = 5

  def initialize(order_id)
    @order_id = order_id
  end

  def generate
    Prawn::Document.generate(Rails.root.join('public', REPORT_PATH).to_s, margin: [10, 20]) do |pdf|
      order.packing_configurations.each_slice(ROWS * COLUMNS).each_with_index do |packs, page|
        pdf.start_new_page unless page.zero?

        pdf.fill_color '39424A'

        pdf.bounding_box [pdf.bounds.left, pdf.cursor], width: pdf.bounds.width do
          pdf.image  "#{Rails.root.join('app', 'assets', 'images', 'report-logo.png').to_s}", fit: [97.5, 37.5]
          pdf.text_box "PLANO DE CORTE - ##{order.id} - #{order.date.strftime('%d/%m/%Y')}", at: [0, 20], align: :right, width: pdf.bounds.width, size: 16
        end

        pdf.move_down 10

        pdf.stroke_horizontal_rule

        pdf.move_down 5

        total_width = pdf.bounds.width - pdf.bounds.left
        width = (total_width / COLUMNS) - (GUTTER * (COLUMNS - 1))
        cursor = pdf.cursor
        max_height = cursor / ROWS

        packs.each_with_index do |pack, index|
          x = index % 3 * (width + GUTTER)

          cursor = pdf.cursor if index % 3 == 0

          pdf.bounding_box [x, cursor], width: width do
            pdf.text "PACOTE #{(page * 6) + (index + 1)}", size: 10
            pdf.text "#{pack.component_name}", size: 8

            pack.items.each do |item|
              component = OrderItemComponent.find_by(component_name: pack.component_name, order_item_id: item)

              pdf.text "• #{component.width_consumption}cm x #{component.height_consumption}cm", size: 8
            end
          end

          pdf.svg IO.read("#{pack.packing.path}"), at: [x, pdf.cursor]

          pdf.move_down 15
        end
      end
    end

    REPORT_PATH
  end

  private

  def order
    @order ||= Order.find(@order_id)
  end
end