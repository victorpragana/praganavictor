class PaymentConditionService
  attr_reader :success, :errors, :record

  def initialize(parameters)
    @parameters = parameters

    @parameters['type'] = @parameters['type'].to_i if @parameters['type'].present?
  end

  def create
    payment_condition = PaymentCondition.new(@parameters)

    save_record(payment_condition)
  end

  def update(payment_condition_id)
    payment_condition = PaymentCondition.find(payment_condition_id)

    payment_condition.assign_attributes(@parameters)

    save_record(payment_condition)
  end

  private

  def save_record(payment_condition)
    if payment_condition.save
      @success = true
      @record = payment_condition.reload
    else
      @success = false
      @errors = payment_condition.errors.full_messages
    end
  end
end