class PrinterService
  attr_reader :success, :errors, :record

  def initialize(parameters = {})
    @parameters = parameters
  end

  def create
    printer = Printer.new

    save_record(printer)
  end

  def update(printer_id)
    printer = Printer.find(printer_id)

    save_record(printer)
  end

  def default_printer
    Printer.find_by(default: true)
  end

  private
 
  def save_record(printer)
    printer.assign_attributes(@parameters)
    
    if printer.save
      @success = true
      @record = printer.reload
    else
      @success = false
      @errors = printer.errors.full_messages
    end
  end
end