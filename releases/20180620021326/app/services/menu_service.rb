class MenuService
  #menu
  ENTITIES           = { link: '#',                  icon: 'fa fa-users',            name: I18n.t('services.menu.entities') }
  SHUTTER            = { link: '#',                  icon: 'fa fa-map',              name: I18n.t('services.menu.shutter') }
  COMERCIAL          = { link: '#',                  icon: 'fa fa-money',            name: I18n.t('services.menu.comercial') }
  ADJUSTMENTS        = { link: '#',                  icon: 'fa fa-cog',              name: I18n.t('services.menu.adjustments') }
  SIMULATION         = { link: 'orders/new',         icon: 'icon-book-open',         name: I18n.t('services.menu.simulation') }
  DOCUMENT           = { link: 'document/new',       icon: 'fa fa-pencil-square-o',  name: I18n.t('services.menu.document') }

  #submenu
  DASHBOARD          = { link: '',                   icon: 'icon-home',       name: I18n.t('services.menu.dashboard') }
  FAMILIES           = { link: 'families',           icon: 'icon-book-open',  name: I18n.t('services.menu.families') }
  ATTRIBUTES         = { link: 'attributes',         icon: 'icon-settings',   name: I18n.t('services.menu.attributes') }
  BASE_COMPONENTS    = { link: 'base_components',    icon: 'icon-puzzle',     name: I18n.t('services.menu.base_components') }
  COMPONENTS         = { link: 'components',         icon: 'icon-puzzle',     name: I18n.t('services.menu.components') }
  LINES              = { link: 'lines',              icon: 'fa fa-tag',       name: I18n.t('services.menu.lines') }
  ESTIMATES          = { link: 'estimates',          icon: 'icon-book-open',  name: I18n.t('services.menu.estimates') }
  ORDERS             = { link: 'orders',             icon: 'icon-basket',     name: I18n.t('services.menu.orders') }
  PRODUCTION_ORDERS  = { link: 'production_orders',  icon: 'icon-wrench',     name: I18n.t('services.menu.production_orders') }
  PRICE_TABLES       = { link: 'price_tables',       icon: 'fa fa-usd',       name: I18n.t('services.menu.price_tables') }
  DISTRIBUTORS       = { link: 'distributors',       icon: 'fa fa-building',  name: I18n.t('services.menu.distributors') }
  STORES             = { link: 'stores',             icon: 'icon-handbag',    name: I18n.t('services.menu.stores') }
  CUSTOMERS          = { link: 'customers',          icon: 'fa fa-male',      name: I18n.t('services.menu.customers') }
  ITEMS              = { link: 'items',              icon: 'icon-present',    name: I18n.t('services.menu.items') }
  USERS              = { link: 'users',              icon: 'icon-user',       name: I18n.t('services.menu.users') }
  CUT_TABLES         = { link: 'cut_tables',         icon: 'fa fa-scissors',  name: I18n.t('services.menu.cut_tables') }
  PRINTERS           = { link: 'printers',           icon: 'fa fa-print',     name: I18n.t('services.menu.printers') }
  QUESTIONS          = { link: 'questions',          icon: 'fa fa-question',  name: I18n.t('services.menu.questions') }
  CLASSIFICATION     = { link: 'classifications',    icon: 'icon-list',       name: I18n.t('services.menu.commercial_classifications') }
  CREDITS            = { link: 'credits',            icon: 'fa fa-usd',       name: I18n.t('services.menu.credits') }
  CONCEDED_CREDITS   = { link: 'conceded_credits',   icon: 'fa fa-usd',       name: I18n.t('services.menu.conceded_credits') }
  PAYMENT_CONDITIONS = { link: 'payment_conditions', icon: 'fa fa-money',     name: I18n.t('services.menu.payment_conditions') }

  def load_for(user_type, workplace)
    case user_type
    when 'Administrator'
      case workplace
      when 'Factory'
        [
          DASHBOARD, 
          ENTITIES.merge({ submenu: [DISTRIBUTORS, STORES, CUSTOMERS] }),
          SHUTTER.merge({ submenu: [ATTRIBUTES, BASE_COMPONENTS, COMPONENTS, LINES] }),
          ITEMS, 
          COMERCIAL.merge({ submenu: [ESTIMATES, ORDERS, PRODUCTION_ORDERS, CONCEDED_CREDITS] }),
          ADJUSTMENTS.merge({ submenu: [USERS, FAMILIES, PRICE_TABLES, CUT_TABLES, PRINTERS, QUESTIONS, CLASSIFICATION, PAYMENT_CONDITIONS] }),
          SIMULATION,
          DOCUMENT
        ]
      when 'Distributor'
        [
          DASHBOARD, 
          ENTITIES.merge({ submenu: [STORES] }),
          ITEMS, 
          COMERCIAL.merge({ submenu: [ESTIMATES, ORDERS, CREDITS, CONCEDED_CREDITS] }),
          ADJUSTMENTS.merge({ submenu: [USERS, PRICE_TABLES] })
         ]
      when 'Store'
        [
          DASHBOARD,
          ENTITIES.merge({ submenu: [CUSTOMERS] }),
          ITEMS,
          COMERCIAL.merge({ submenu: [ESTIMATES, ORDERS, CREDITS] }),
          ADJUSTMENTS.merge({ submenu: [USERS, PRICE_TABLES] })
        ]
      else
        []
      end
    when 'Manager'
      case workplace
      when 'Factory'
        [
          DASHBOARD, 
          ENTITIES.merge({ submenu: [DISTRIBUTORS, STORES, CUSTOMERS] }), 
          COMERCIAL.merge({ submenu: [ESTIMATES, ORDERS] })
        ]
      when 'Distributor'
        [
          DASHBOARD, 
          ENTITIES.merge({ submenu: [STORES] }),
          COMERCIAL.merge({ submenu: [ESTIMATES, ORDERS] })
         ]
      when 'Store'
        [
          DASHBOARD,
          ENTITIES.merge({ submenu: [CUSTOMERS] }),
          COMERCIAL.merge({ submenu: [ESTIMATES, ORDERS] })
        ]
      else
        []
      end
    when 'Seller'
      case workplace
      when 'Factory'
        [
          DASHBOARD, 
          ENTITIES.merge({ submenu: [DISTRIBUTORS, STORES, CUSTOMERS] }), 
          COMERCIAL.merge({ submenu: [ESTIMATES, ORDERS] })
        ]
      when 'Distributor'
        [
          DASHBOARD, 
          ENTITIES.merge({ submenu: [STORES] }),
          COMERCIAL.merge({ submenu: [ESTIMATES, ORDERS] })
         ]
      when 'Store'
        [
          DASHBOARD,
          ENTITIES.merge({ submenu: [CUSTOMERS] }),
          COMERCIAL.merge({ submenu: [ESTIMATES, ORDERS] })
        ]
      else
        []
      end
    when 'Operator'
      case workplace
      when 'Factory'
        [
          DASHBOARD, 
          COMERCIAL.merge({ submenu: [PRODUCTION_ORDERS] })
        ]
      when 'Distributor'
        []
      when 'Store'
        []
      else
        []
      end
    else
      []
    end
  end
end


