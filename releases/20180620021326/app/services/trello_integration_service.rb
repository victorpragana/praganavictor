class TrelloIntegrationService
  attr_reader :success, :errors, :record

  def create_trello_card(order)
    begin
      trello_member = Trello::Member.find(Rails.application.secrets.trello["member"])

      board_luss = trello_member.boards.find do |board|
        board.name == Rails.application.secrets.trello["board"]
      end

      list = if board_luss.has_lists?
        board_luss.lists.find do |list|
          list.name == Rails.application.secrets.trello["list"]
        end
      else
        Trello::List.create(name: Rails.application.secrets.trello["list"], board_id: board_luss.id)
      end

      if order.seller_type == "Distributor"
        seller_type = "Distribuidor"
      elsif order.seller_type == "Factory"
        seller_type = "Fábrica"
      elsif order.seller_type == "Store"
        seller_type = "Loja"
      end

      if order.client_type == "Distributor"
        client_type = "Distribuidor"
        billing_client_type = "Distribuidor"
      elsif order.client_type == "Customer"
        client_type = "Cliente"
        billing_client_type = "Cliente"
      elsif order.client_type == "Store"
        client_type = "Loja"
        billing_client_type = "Loja"
      end

      Trello::Card.create(name: "Pedido ##{order.id}", desc: "Vendedor: #{seller_type}\nCliente: #{client_type}\nDesconto: #{order.discount}\nFaturamento: #{billing_client_type}\nDesconto especial: #{order.special_discount}\nData: #{order.created_at.strftime('%d/%m/%Y')}", list_id: list.id)
    rescue => e
      Rails.logger.error "Erro ao criar card no trello #{e}"
      Rails.logger.error "#{e.backtrace}"
    end
  end

  def delete_trello_card(order)
    begin
      trello_member = Trello::Member.find(Rails.application.secrets.trello["member"])

      board_luss = trello_member.boards.find do |board|
        board.name == Rails.application.secrets.trello["board"]
      end

      order_card = board_luss.cards.find do |card|
        card.name == "Pedido ##{order.id}"
      end

      order_card.delete
    rescue => e
      Rails.logger.error "Erro ao apagar card no trello #{e}"
      Rails.logger.error "#{e.backtrace}"
    end
  end
end