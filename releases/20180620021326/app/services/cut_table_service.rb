class CutTableService
  attr_reader :success, :errors, :record

  def initialize(parameters)
    @parameters = parameters
  end

  def create
    cut_table = CutTable.new(@parameters)

    save_record(cut_table)
  end

  def update(cut_table_id)
    cut_table = CutTable.find(cut_table_id)

    cut_table.assign_attributes(@parameters)

    save_record(cut_table)
  end

  private
  def save_record(cut_table)
    if cut_table.save
      @success = true
      @record = cut_table.reload
    else
      @success = false
      @errors = cut_table.errors.full_messages
    end
  end
end