class PaymentService
  attr_reader :success, :errors, :record

  DESTINATION_EMAIL = 'swang@luss.com'

  def initialize(parameters = {})
    @parameters = parameters
    
    @payment_condition_id = @parameters.delete('payment_condition_id')
    @third_party_id = @parameters.delete('third_party_id')
    @credit_type = @parameters.delete('credit_type')

    @credit_card_iugu_token = @parameters.delete('credit_card_iugu_token')
  end

  def create(order_id)
    ActiveRecord::Base.transaction do
      order = Order.find_by(id: order_id)

      description = nil

      if order.present?
        @third_party_id = order.seller.third_party.id

        description = "#{order.billing_client.third_party.name} - Nº DO PEDIDO: #{order.id.to_s}"
      else
        description = "Crédito para: #{third_party.name}"
      end

      payment = Payment.new(@parameters)

      third_party_email = if Rails.env.development?
        'ti@coyo.com.br'
      else
        'swang@luss.com'
      end

      if valid_payment?(third_party, payment)
        Iugu.api_key = Rails.application.secrets.iugu_api_token

        payment.original_value = payment.value
        payment.tax = ProductionCredit ? payment_condition.credit_tax : payment_condition.additional_tax
        payment.value = credit_type == ProductionCredit ? payment_condition.apply_credit_tax(payment.original_value) : payment_condition.apply_additional_tax(payment.original_value)
        payment.installment_number = payment_condition.installments

        if payment.credit_card?
          payment.status = Payment.statuses[:approved]

          items = [{
            'description' => description,
            'quantity' => '1',
            'price_cents' => (payment.value * 100).to_i.to_s
          }]

          charge = Iugu::Charge.create(
            'token' => @credit_card_iugu_token,
            'email' => third_party_email,
            'cc_emails' => third_party.email,
            'months' => payment.installment_number.to_s,
            'items' => items
              
          )
        elsif payment.bank_slip?
          items = [{
            'description' => description,
            'quantity' => '1',
            'price_cents' => (payment.value * 100).to_i.to_s
          }]

          charge = Iugu::Charge.create(
            'method' => 'bank_slip',
            'email' => third_party_email,
            'cc_emails' => third_party.email,
            'items' => items,
            'payer' =>
              {
                'cpf_cnpj' => third_party.identification,
                'name' => third_party.name,
                'email' => third_party.email,
                'address' => {
                  'street' => third_party.street,
                  'number' => third_party.number,
                  'city' => third_party.city.try(:name),
                  'state' => third_party.city.try(:state).try(:name),
                  'zip_code' => third_party.zip_code
                }
              }
          )
        end

        if !charge || !charge.success
          @errors = [charge.try(:message)].compact

          charge_errors = charge.errors

          if charge_errors.is_a?(String)
            @errors << charge_errors
          elsif !charge_errors.empty?
            charge_errors.each do |key, value|
              @errors << "#{key}, #{value}"
            end
          end

          @success = false
        else
          payment.iugu_invoice_id = charge.invoice_id

          payment.bank_slip_url = charge.url if charge.url && payment.bank_slip?

          payment.save!

          creator = MovementCreatorService.new(credit_type, payment, order)

          destination_third_party = if third_party.specific.is_a?(Store)
            third_party.specific.distributor.third_party
          else third_party.specific.is_a?(Distributor)
            Factory.first.third_party
          end

          creator.input(third_party, destination_third_party)

          if creator.success
            @success = true
            @record = payment
          else
            @success = false
            creator.errors.each { |message| @errors << message }

            raise ActiveRecord::Rollback
          end
        end
      end
    end
  rescue StandardError => e
    @success = false
    @errors = [e.message]
  end

  def valid_payment?(third_party, payment)
    if !third_party.has_address? && payment.bank_slip?
      @errors = ['Cliente sem endereço cadastrado']
      @success = false
      return false
    end

    if !third_party.identification
      @errors = ['Cliente sem CPF/CNPJ cadastrado']
      @success = false
      return false
    end

    if third_party.specific.is_a?(Factory)
      @errors = ['A fábrica não pode realizar pagamentos']
      @success = false
      return false
    end

    return true

  end

  def status_changed
    ActiveRecord::Base.transaction do
      payment = Payment.find_by(iugu_invoice_id:  @parameters['id'])
      movements = Movement.where(payment_id: payment.id)

      if payment.present?
        @success = true

        if @parameters['status'] == 'paid'
          payment.approved!
          movements.update_all(status: Movement.statuses[:approved])
        end
      else
        @success = false
        @errors = ["Pagamento ##{@parameters['id']} não encontrado!"]
      end
    end
  rescue StandardError => e
    @success = false
    @errors = [e.message]
  end

  private

  def third_party
    @third_party ||= ThirdParty.find(@third_party_id)
  end

  def payment_condition
    @payment_condition ||= PaymentCondition.find(@payment_condition_id)
  end

  def credit_type
    @credit_type.constantize
  end
end