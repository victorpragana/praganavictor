class PurchasePlanningService
  include ActionView::Helpers::NumberHelper

  REPORT_PATH = 'system/planejamento_compra.pdf'

  def initialize(order_ids)
    @order_ids = order_ids.sort
  end

  def generate
    Prawn::Document.generate(Rails.root.join('public', REPORT_PATH).to_s, margin: [10, 20]) do |pdf|
      pdf.fill_color '39424A'

      pdf.bounding_box [pdf.bounds.left, pdf.cursor], width: pdf.bounds.width do
        pdf.image  "#{Rails.root.join('app', 'assets', 'images', 'report-logo.png').to_s}", fit: [97.5, 37.5]
        pdf.text_box "PLANEJAMENTO DE COMPRA", at: [0, 20], align: :center, width: pdf.bounds.width, size: 16
      end

      pdf.move_down 5

      pdf.bounding_box [pdf.bounds.left, pdf.cursor], width: pdf.bounds.width do
        pdf.fill_color '7E8691'
        pdf.text_box "#{@order_ids.size > 1 ? 'PEDIDOS' : 'PEDIDO'} #{@order_ids.map{|order| '#' + order}.join(' ')}", align: :right, width: pdf.bounds.width, style: :bold
      end

      pdf.move_down 20

      pdf.bounding_box [pdf.bounds.left, pdf.cursor], width: pdf.bounds.width do
        pdf.fill_color '39424A'
        table_data = []

        table_data << [
          {
            content: 'DESCRIÇÃO', align: :center, text_color: '9ea8b7'
          },
          {
            content: 'CONSUMO', align: :center, text_color: '9ea8b7'
          },
          {
            content: 'UN', align: :center, text_color: '9ea8b7'
          }
        ]

        components.select(:component_name).group(:component_name).pluck(:component_name).each do |component_name|
          automatic_components = PackingConfiguration.where(packable_id: @order_ids, packable_type: Order, component_name: component_name)

          unless automatic_components.size.zero?
            table_data << [
              {
                content: component_name, text_color: '9ea8b7', size: 10
              },
              {
                content: format_number(automatic_components.sum(:automatic_dimension)), text_color: '9ea8b7', size: 10, align: :right
              },
              {
                content: format_unit(automatic_components.first), text_color: '9ea8b7', size: 10, align: :center
              }
            ]
          end

          non_automatic_components = components.where(component_name: component_name).joins(order_item: [:order])
            .joins("LEFT JOIN packing_configurations pc ON pc.packable_id = orders.id AND pc.packable_type = 'Order' AND pc.component_name = '#{component_name}'")
            .select("order_item_components.*, pc.id").where("pc.id is null")

          unless non_automatic_components.size.zero? || non_automatic_components.sum(:total_consumption).zero?
            table_data << [
              {
                content: component_name, text_color: '9ea8b7', size: 10
              },
              {
                content: format_number(non_automatic_components.sum(:total_consumption)), text_color: '9ea8b7', size: 10, align: :right
              },
              {
                content: format_unit(non_automatic_components.first), text_color: '9ea8b7', size: 10, align: :center
              }
            ]
          end
        end

        pdf.make_table(table_data, width: pdf.bounds.width, cell_style: { border_color: 'e7ecf1' }, header: true).draw
      end
    end

    REPORT_PATH
  end

  private

  def components
    @components ||= OrderItemComponent.joins(:order_item).where(order_items: { order_id: @order_ids }).order(:component_name)
  end

  def format_number(number)
    number_with_precision(number, precision: 2, separator: ',', delimiter: '.')
  end

  def format_unit(component)
    component.unit.to_s.upcase
  end
end