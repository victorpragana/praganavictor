'use strict';

/**
 * @ngdoc overview
 * @name lussApp
 * @description
 * # lussApp
 *
 * Main module of the application.
 */
angular
  .module('lussApp', [
    'ngResource',
    'ui.router',
    'ui.bootstrap',
    'ngSanitize',
    'restmod',
    'ui.select',
    'checklist-model',
    'angular.filter',
    'ng-token-auth',
    'ipCookie',
    'blockUI',
    'ui.utils.masks',
    'MassAutoComplete',
    'ngFileUpload',
    'xeditable',
    'ngLetterAvatar',
    'summernote',
    'mgo-angular-wizard'
  ])
  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state("login", {
        url: "/login",
        views: {
          "layout@": {
            templateUrl: "views/layouts/login.html",
          },
          "content@login": {
            templateUrl: "views/login/form.html"
          }
        }
      })
      .state("dashboard", {
        url: "/",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@dashboard": {
            templateUrl: "views/dashboard/index.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("users_index", {
        url: "/users",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@users_index": {
            templateUrl: "views/users/index.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("users_new", {
        url: "/users/new",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@users_new": {
            templateUrl: "views/users/new.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("users_edit", {
        url: "/users/:id/edit",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@users_edit": {
            templateUrl: "views/users/edit.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("families_index", {
        url: "/families",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@families_index": {
            templateUrl: "views/families/index.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("lines_index", {
        url: "/lines",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@lines_index": {
            templateUrl: "views/lines/index.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("lines_new", {
        url: "/lines/new",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@lines_new": {
            templateUrl: "views/lines/new.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("lines_edit", {
        url: "/lines/:id/edit",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@lines_edit": {
            templateUrl: "views/lines/edit.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("lines_rules", {
        url: "/lines/:id/rules",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@lines_rules": {
            templateUrl: "views/lines/rules.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("components_index", {
        url: "/components",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@components_index": {
            templateUrl: "views/components/index.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("components_new", {
        url: "/components/new",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@components_new": {
            templateUrl: "views/components/new.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("components_edit", {
        url: "/components/:id/edit",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@components_edit": {
            templateUrl: "views/components/edit.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("components_kit", {
        url: "/components/:id/kit",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@components_kit": {
            templateUrl: "views/components/kit.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("orders_index", {
        url: "/orders",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@orders_index": {
            templateUrl: "views/orders/index.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("orders_new", {
        url: "/orders/new",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@orders_new": {
            templateUrl: "views/orders/new.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("orders_show", {
        url: "/orders/:id",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@orders_show": {
            templateUrl: "views/orders/show.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("orders_print", {
        url: "/orders/:id/print",
        views: {
          "layout@": {
            templateUrl: "views/layouts/printable.html",
          },
          "content@orders_print": {
            templateUrl: "views/orders/print.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("document_new", {
        url: "/document/new",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@document_new": {
            templateUrl: "views/document/new.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("document_index", {
        url: "/document",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@document_index": {
            templateUrl: "views/document/index.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("production_orders_index", {
        url: "/production_orders",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@production_orders_index": {
            templateUrl: "views/production_orders/index.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("production_orders_show", {
        url: "/production_orders/:id",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@production_orders_show": {
            templateUrl: "views/production_orders/show.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("production_orders_print", {
        url: "/production_orders/:id/print",
        views: {
          "layout@": {
            templateUrl: "views/layouts/printable.html",
          },
          "content@production_orders_print": {
            templateUrl: "views/production_orders/print.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("attributes_index", {
        url: "/attributes",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@attributes_index": {
            templateUrl: "views/attributes/index.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("attributes_new", {
        url: "/attributes/new",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@attributes_new": {
            templateUrl: "views/attributes/new.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("attributes_edit", {
        url: "/attributes/:id/edit",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@attributes_edit": {
            templateUrl: "views/attributes/edit.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("base_components_index", {
        url: "/base_components",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@base_components_index": {
            templateUrl: "views/base_components/index.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("base_components_edit", {
        url: "/base_components/:id/edit",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@base_components_edit": {
            templateUrl: "views/base_components/edit.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("base_components_new", {
        url: "/base_components/new",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@base_components_new": {
            templateUrl: "views/base_components/new.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("price_tables_index", {
        url: "/price_tables",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@price_tables_index": {
            templateUrl: "views/price_tables/index.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("price_tables_new", {
        url: "/price_tables/new",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@price_tables_new": {
            templateUrl: "views/price_tables/new.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("price_tables_edit", {
        url: "/price_tables/:id/edit",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@price_tables_edit": {
            templateUrl: "views/price_tables/edit.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("third_parties_index", {
        url: "/third_parties",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@third_parties_index": {
            templateUrl: "views/third_parties/index.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("third_parties_new", {
        url: "/third_parties/new",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@third_parties_new": {
            templateUrl: "views/third_parties/new.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("third_parties_edit", {
        url: "/third_parties/:id/edit",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@third_parties_edit": {
            templateUrl: "views/third_parties/edit.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("estimates_index", {
        url: "/estimates",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@estimates_index": {
            templateUrl: "views/estimates/index.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("estimates_express", {
        url: "/estimates/express",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@estimates_express": {
            templateUrl: "views/estimates/express.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("estimates_express_edit", {
        url: "/estimates/:id/express_edit",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@estimates_express_edit": {
            templateUrl: "views/estimates/express.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("stores_index", {
        url: "/stores",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@stores_index": {
            templateUrl: "views/stores/index.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("stores_new", {
        url: "/stores/new",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@stores_new": {
            templateUrl: "views/stores/new.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("stores_edit", {
        url: "/stores/:id/edit",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@stores_edit": {
            templateUrl: "views/stores/edit.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("distributors_index", {
        url: "/distributors",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@distributors_index": {
            templateUrl: "views/distributors/index.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("distributors_new", {
        url: "/distributors/new",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@distributors_new": {
            templateUrl: "views/distributors/new.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("distributors_edit", {
        url: "/distributors/:id/edit",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@distributors_edit": {
            templateUrl: "views/distributors/edit.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("customers_index", {
        url: "/customers",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@customers_index": {
            templateUrl: "views/customers/index.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("customers_new", {
        url: "/customers/new",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@customers_new": {
            templateUrl: "views/customers/new.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("customers_edit", {
        url: "/customers/:id/edit",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@customers_edit": {
            templateUrl: "views/customers/edit.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("items_index", {
        url: "/items",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@items_index": {
            templateUrl: "views/items/index.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("items_new", {
        url: "/items/new",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@items_new": {
            templateUrl: "views/items/new.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("items_edit", {
        url: "/items/:id/edit",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@items_edit": {
            templateUrl: "views/items/edit.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("cut_tables_index", {
        url: "/cut_tables",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@cut_tables_index": {
            templateUrl: "views/cut_tables/index.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("cut_tables_new", {
        url: "/cut_tables/new",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@cut_tables_new": {
            templateUrl: "views/cut_tables/new.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("cut_tables_edit", {
        url: "/cut_tables/:id/edit",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@cut_tables_edit": {
            templateUrl: "views/cut_tables/edit.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      }).state("printers_index", {
        url: "/printers",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@printers_index": {
            templateUrl: "views/printers/index.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("printers_new", {
        url: "/printers/new",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@printers_new": {
            templateUrl: "views/printers/new.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("printers_edit", {
        url: "/printers/:id/edit",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@printers_edit": {
            templateUrl: "views/printers/edit.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      }).state("questions_index", {
        url: "/questions",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@questions_index": {
            templateUrl: "views/questions/index.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("questions_new", {
        url: "/questions/new",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@questions_new": {
            templateUrl: "views/questions/new.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("questions_edit", {
        url: "/questions/:id/edit",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@questions_edit": {
            templateUrl: "views/questions/edit.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("classifications_index", {
        url: "/classifications",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@classifications_index": {
            templateUrl: "views/classifications/index.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("classifications_new", {
        url: "/classifications/new",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@classifications_new": {
            templateUrl: "views/classifications/new.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("classifications_edit", {
        url: "/classifications/:id/edit",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@classifications_edit": {
            templateUrl: "views/classifications/edit.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("credits_index", {
        url: "/credits",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@credits_index": {
            templateUrl: "views/credits/index.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("conceded_credits_index", {
        url: "/conceded_credits",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@conceded_credits_index": {
            templateUrl: "views/conceded_credits/index.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      })
      .state("payment_conditions_index", {
        url: "/payment_conditions",
        views: {
          "layout@": {
            templateUrl: "views/layouts/main.html",
          },
          "content@payment_conditions_index": {
            templateUrl: "views/payment_conditions/index.html"
          }
        },
        resolve: {
          auth: function($auth, $state) {
            return $auth.validateUser().catch(function(){$state.go('login');});
          }
        }
      });

      $urlRouterProvider.otherwise('/login');
  })
  .config(function(restmodProvider) {
    restmodProvider.rebase('AMSApi');
  })
  .config(function($authProvider) {
    $authProvider.configure({
      apiUrl: 'api'
    });
  })
  .factory('settings', function($rootScope) {
    var settings = {
      layout: {
        pageSidebarClosed: false, // sidebar state
        pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
      },
      assetsPath: '/assets',
      globalPath: '/assets/global',
      layoutPath: '/assets/layouts/layout4',
    };

    $rootScope.settings = settings;

    return settings;
  })
  .factory('Faye', function($faye) {
    return $faye("http://persianas.ind.br:9191/faye");
  })
  .config(function(restmodProvider) {
    restmodProvider.rebase({
      $extend: {
        Model: {
          encodeName: function(_name) { return _name; }, // or null to disable renaming.
          decodeName: function(_name) { return _name; } // or null to disable renaming.
        }
      }
    });
  })
  .config(function($locationProvider) {
    $locationProvider.html5Mode({
      enabled: true
    });
  })
  .config(function(blockUIConfig) {
    blockUIConfig.message = 'Carregando...';
    blockUIConfig.delay = 20;
    blockUIConfig.cssClass = 'block-ui block-ui-custom-z-index';
    blockUIConfig.requestFilter = function(config) {
      if(config.url.match(/^\/api\/job_status($|\/).*/)) {
        return false;
      }
    };
  })
  .run(function($rootScope, settings, editableOptions, $state, $auth) {
    toastr.options = {
      closeButton: true,
      positionClass: 'toast-top-center',
      preventDuplicates: true,
      escapeHtml: true
    };

    $auth.initialize();

    $rootScope.$on('$stateChangeSuccess', function(event, toState){ 
      var user_permissions = event.currentScope.user.permissions;

      if (user_permissions !== undefined) {
        var array = toState.url.substring(1).split("/");

        if (toState.name !== "login" && toState.name !== "dashboard") {
          var has_permission = false;

          angular.forEach(user_permissions, function(permissions, resource) {
            if (resource === array[0]) {
              has_permission = true;
            }
          });

          if (!has_permission) {
            event.preventDefault();

            toastr.remove();
            toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");

            $state.go("dashboard");
          }
        }
      }
    });

    editableOptions.theme = 'bs3';

    $rootScope.$state = $state; // state to be accessed from view
  });