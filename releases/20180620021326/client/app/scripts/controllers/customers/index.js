'use strict';

angular
  .module("lussApp")
  .controller("CustomerIndexCtrl", function(CustomerModel, PriceTableModel, Enums, ipCookie, lussError, $timeout, $scope) {
    var self = this;

    self.selected = [];
    self.$table = null;
    self.priceTables = [];
    self.priceTableId = null;
    self.priceTableTypeEnum = Enums.PriceTableType;

    self.selectAll = function() {
      $("input[type=checkbox]").trigger("click");
    };

    self.init = function() {
      $(".modal-backdrop").remove();

      self.priceTables = PriceTableModel.$search();

      self.$table = $('#table_customers').dataTable({
        "language": {
          "aria": {
              "sortAscending": "clique para ordenar de forma crescente",
              "sortDescending": "clique para ordenar de forma decrescente"
          },
          "emptyTable": "Não existem dados para serem exibidos",
          "info": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
          "infoEmpty": "Nenhum registro foi encontrado",
          "infoFiltered": "(filtrado de _MAX_ registros no total)",
          "lengthMenu": "Mostrar _MENU_ registros",
          "search": "Buscar:",
          "zeroRecords": "Não foram encontrados resultados",
          "processing": "Processando...",
        },

        buttons: [
            { extend: 'excel', className: 'btn yellow btn-outline ' },
            { extend: 'colvis', className: 'btn dark btn-outline', text: "Colunas"}
        ],

        processing: true,
        serverSide: true,
        searchDelay: 800,
        columnDefs: [
          { targets: 0, orderable: false, searchable: false, width: 5, render: function(data, type, row) {
            return "<div class='md-checkbox-list'><div class='md-checkbox'><input type='checkbox' id='checkbox_" + row[1] + "' value='" + row[1] + "' class='md-check'><label for='checkbox_" + row[1] + "'><span></span><span class='check'></span> <span class='box'></span></label></div>";
          }},
          { targets: 1, name: "id", class: "text-center", width: 20, title: "ID", render: function(data, type, row) {
            return "<a href='/customers/" + row[1] + "/edit'>" + row[1] + "</a>";
          }},
          { targets: 2, data: 2, title: "Nome" },
          { targets: 3, data: 3, title: "Documento" },
          { targets: 4, data: 4, title: "Telefone" },
          { targets: 5, orderable: false, searchable: false, data: 5, title: "Tabelas de Preço (Linhas/Produtos)", class: "text-center", render: function(data, type, row) {
            var html = '';

            if (row[6] !== null) {
              html += "<a href='/price_tables/" + row[6] + "/edit'>" + row[5] + " </a>";
            } else {
              html += "Nenhuma";
            }

            html += ' / ';

            if (row[8] !== null) {
              html += "<a href='/price_tables/" + row[8] + "/edit'>" + row[7] + " </a>";
            } else {
              html += "Nenhuma";
            }

            return html;
          }}
        ],

        ajax: {
          url: "/api/customers.json",
          headers: ipCookie("auth_headers")
        },

        order: [
            [1, 'asc']
        ],
        
        lengthMenu: [
            [5, 10, 15, 20, 300, -1],
            [5, 10, 15, 20, 300, "Todos"] // change per page values here
        ],

        pageLength: 100,

        dom: "<'row' <'col-md-12 margin-top-15'B>><'row'<'col-md-6 col-sm-12'><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
      });
    };

    self.openDeleteModal = function() {
      $("#modal_delete_customers").modal();
    };

    self.removeSelected = function() {
      CustomerModel.$batchDestroy(self.selected).error(function(data) {
        lussError.show(data.errors);
      }).success(function() {
        toastr.remove();
        toastr.success("Clientes removidos com sucesso!");

        self.$table.api().ajax.reload();
      });

      self.selected = [];
    };

    self.openApplyPriceTableModal = function() {
      $("#modal_apply_price_table").modal();
    };

    self.applyToSelected = function() {
      if (self.priceTableId !== null) {
        PriceTableModel.$new(self.priceTableId).$applyToCustomers(self.selected).$then(function() {
          toastr.remove();
          toastr.success("Tabela aplicada com sucesso!");

          self.$table.api().ajax.reload();

          self.selected = [];
        }, function(response) {
          lussError.show(response.$response.data.errors);
        });
      }
    };

    angular.element(document).on("change", "input[type=checkbox]", function() {
      if ($(this).prop('checked')) {
        self.selected.push($(this).val());

        $(this).closest('tr').addClass("selected");
      } else {
        self.selected.splice(self.selected.indexOf($(this).val()), 1);

        $(this).closest('tr').removeClass("selected");
      }

      $timeout(function() {
        $scope.$digest();
      });
    });

    self.init();
  });