'use strict';

angular
  .module("lussApp")
  .controller("PageHeadController", function($scope) {
    $scope.$on('$includeContentLoaded', function() {        
      Demo.init();
    });
  });