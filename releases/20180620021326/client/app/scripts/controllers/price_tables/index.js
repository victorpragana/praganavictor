'use strict';

angular
  .module("lussApp")
  .controller("PriceTableIndexCtrl", function(PriceTableModel, ipCookie, $timeout, $scope, lussError, Enums) {
    var self = this;

    self.selected = [];
    self.$table = null;

    self.selectAll = function() {
      $("input[type=checkbox]").trigger("click");
    };

    self.init = function() {
      $(".modal-backdrop").remove();

      self.$table = $('#table_price_tables').dataTable({
        "language": {
          "aria": {
              "sortAscending": "clique para ordenar de forma crescente",
              "sortDescending": "clique para ordenar de forma decrescente"
          },
          "emptyTable": "Não existem dados para serem exibidos",
          "info": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
          "infoEmpty": "Nenhum registro foi encontrado",
          "infoFiltered": "(filtrado de _MAX_ registros no total)",
          "lengthMenu": "Mostrar _MENU_ registros",
          "search": "Buscar:",
          "zeroRecords": "Não foram encontrados resultados",
          "processing": "Processando...",
        },

        buttons: [
            { extend: 'excel', className: 'btn yellow btn-outline ' },
            { extend: 'colvis', className: 'btn dark btn-outline', text: "Colunas"}
        ],

        processing: true,
        serverSide: true,
        searchDelay: 800,
        columnDefs: [
          { targets: 0, orderable: false, searchable: false, width: 5, render: function(data, type, row) {
            return "<div class='md-checkbox-list'><div class='md-checkbox'><input type='checkbox' id='checkbox_" + row[1] + "' value='" + row[1] + "' class='md-check'><label for='checkbox_" + row[1] + "'><span></span><span class='check'></span> <span class='box'></span></label></div>";
          }},
          { targets: 1, name: 'id', class: 'text-center', width: 20, title: 'ID', render: function(data, type, row) {
            return "<a href='/price_tables/" + row[1] + "/edit'>" + row[1] + "</a>";
          }},
          { targets: 2, data: 2, title: 'Nome' },
          { targets: 3, data: 3, class: "text-left", title: 'Tipo', "render": function(data, type, row) {
            var html = '<label class="margin-right-10 label ' + Enums.PriceTableType.findById(row[3]).color + '">' + Enums.PriceTableType.findById(row[3]).name + '</label>';

            if (row[4] === true) {
              html += '<label class="label label-success">Padrão</label>';
            }

            return html;
          }}
        ],

        ajax: {
          url: "/api/price_tables.json",
          headers: ipCookie('auth_headers')
        },

        "order": [
            [1, 'asc']
        ],
        
        "lengthMenu": [
            [5, 10, 15, 20, 300, -1],
            [5, 10, 15, 20, 300, "Todos"] // change per page values here
        ],
        "pageLength": 100,

        "dom": "<'row' <'col-md-12 margin-top-15'B>><'row'<'col-md-6 col-sm-12'><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
      });
    };

    self.openDeleteModal = function() {
      $("#modal_delete_price_tables").modal();
    };

    self.removeSelected = function() {
      PriceTableModel.$batchDestroy(self.selected).error(function(data) {
        lussError.show(data.errors);
      }).success(function() {
        toastr.remove();
        toastr.success("Tabelas removidas com sucesso!");

        self.$table.api().ajax.reload();
      });

      self.selected = [];
    };

    angular.element(document).on("change", "input[type=checkbox]", function() {
      if ($(this).prop('checked')) {
        self.selected.push($(this).val());

        $(this).closest('tr').addClass("selected");
      } else {
        self.selected.splice(self.selected.indexOf($(this).val()), 1);

        $(this).closest('tr').removeClass("selected");
      }

      $timeout(function() {
        $scope.$digest();
      });
    });

    self.init();
  });