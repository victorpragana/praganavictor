'use strict';

angular
  .module("lussApp")
  .controller("ClassificationFormCtrl", function(ClassificationModel, LineModel, $scope, $stateParams, $state) {
    var self = this;

    ClassificationModel.mix({
      $hooks: {
        'after-create': function(response) {
          if (response.status === 201) {
            toastr.remove();
            toastr.success("Classificação comercial criada com sucesso!");

            $state.go($state.current, {}, {reload: true});

            self.model = new ClassificationModel();
          }
        },
        'after-update': function(response) {
          if (response.status === 200) {
            toastr.remove();
            toastr.success("Classificação comercial atualizada com sucesso!");
            $state.go("classifications_index");
          }
        },
        'after-destroy': function(response) {
          if (response.status === 204) {
            toastr.remove();
            toastr.success("Classificação comercial excluída com sucesso!");
            $state.go("classifications_index");
          }
        }
      }
    });

    self.id = $stateParams.id;
    self.classificationName = null;
    self.lines = [];
    self.addedLines = [];
    self.selected = [];
    self.selectedToRemove = [];
    
    if (self.id === undefined) {
      self.model = new ClassificationModel();
      self.model.line_ids = [];
    } else {
      self.model = ClassificationModel.$find(self.id).$then(function(data) {
        self.classificationName = data.name;

        self._selectLines();
      });
    }

    self.init = function() {
      LineModel.$search({active: true}).$then(function(data) {
        self.lines = data;

        self._selectLines();
      });
    };

    self.addSelected = function() {
      angular.forEach(self.selected, function(lineId) {
        if (lineId !== null) {
          var index = self.model.line_ids.indexOf(lineId);

          if (index === -1) {
            self.model.line_ids.push(lineId);
          }
        }
      });

      self.selected = [];

      angular.element("#chkSelectAll").attr("checked", false);

      self._selectLines();
    };

    self.selectAllToAdd = function($event) {
      self._selectAll($event.target, "input[type=checkbox][id^=chk_line_]:visible");
    };

    self.selectAllAdded = function($event) {
      self._selectAll($event.target, "input[type=checkbox][id^=added_]:visible");
    };

    self._selectAll = function($target, selector) {
      if ($target.checked) {
        angular.forEach(angular.element(selector), function(element) {
          if (!element.checked) {
            element.click();
            angular.element(element).triggerHandler('click');
          }
        });
      } else {
        angular.forEach(angular.element(selector), function(element) {
          if (element.checked) {
            element.click();
            angular.element(element).triggerHandler('click');
          }
        });
      }
    };

    self.removeSelected = function() {
      angular.forEach(self.selectedToRemove, function(lineId) {
        var index = self.model.line_ids.indexOf(lineId);

        self.model.line_ids.splice(index, 1);

        self.selectedToRemove = [];
      });

      angular.element("#chkSelectAllAdded").attr("checked", false);

      self._selectLines();
    };

    self._selectLines = function() {
      self.addedLines = [];

      angular.forEach(self.model.line_ids, function(lineId) {
        if (lineId !== null) {
          var line = self.lines.find(function(element) {
            return element.id === lineId;
          });

          if (line !== undefined) {
            self.addedLines.push({
              name: line.name,
              id: line.id
            });
          }
        }
      });
    };

    self.save = function() {
      self.model.$save();
    };

    self.destroy = function() {
      self.model.$destroy();
    };

    self.init();
  });
