'use strict';

angular
  .module("lussApp")
  .controller("DashboardIndexCtrl", function($scope) {
    $scope.$on('$viewContentLoaded', function() {
      Metronic.initAjax();
    });
  });