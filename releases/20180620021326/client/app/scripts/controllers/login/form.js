'use strict';

angular
  .module("lussApp")
  .controller("LoginController", function($scope, $stateParams, $state, $auth, WorkplaceModel) {
    var self = this;

    self.user = {};
    self.logo_url = '/images/logo-lg.png';

    self.init = function() {
      WorkplaceModel.$current().then(function(response){
        self.logo_url = response.data.workplace.logo_url;
      }, function(error) {
        console.log(error)
      });
    };

		$scope.$on('auth:login-success', function() {
			$state.go("dashboard");
		});

    $scope.$on('auth:login-error', function(ev, reason) {
      self.errors = reason.errors;
    });

		self.login = function() {
			$auth.submitLogin(self.user);
		};

    self.init();
  });