'use strict';

angular
  .module("lussApp")
  .controller("PaymentConditionsIndexCtrl", function(PaymentConditionModel, ipCookie, $filter, $timeout, $scope, $compile, $state, $window,  $rootScope, lussError, Enums) {
    var self = this;

    self.$table = null;
    self.model = null;
    self.paymentMethodEnum = Enums.PaymentMethod;
    self.showInstallments = false;

    PaymentConditionModel.mix({
      $hooks: {
        'after-create': function(response) {
          if (response.status === 201) {
            toastr.remove();
            toastr.success("Condição da pagamento criada com sucesso!");

            self.initModel();
            self.reloadTable();
          }
        },
        'after-update': function(response) {
          if (response.status === 200) {
            toastr.remove();
            toastr.success("Condição da pagamento atualizada com sucesso!");
            
            self.initModel();
            self.reloadTable();
          }
        },
        'after-destroy': function(response) {
          if (response.status === 204) {
            toastr.remove();
            toastr.success("Condição da pagamento excluída com sucesso!");
            
            self.initModel();
            self.reloadTable();
          }
        }
      }
    });
    
    self.init = function() {
      self.initModel();

      self.$table = $('#table_payment_conditions').dataTable({
        language: {
          aria: {
            sortAscending: "clique para ordenar de forma crescente",
            sortDescending: "clique para ordenar de forma decrescente"
          },
          emptyTable: "Não existem dados para serem exibidos",
          info: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
          infoEmpty: "Nenhum registro foi encontrado",
          infoFiltered: "(filtrado de _MAX_ registros no total)",
          lengthMenu: "Mostrar _MENU_ registros",
          search: "Buscar:",
          zeroRecords: "Não foram encontrados resultados",
          processing: "Processando...",
        },

        buttons: [
          { extend: 'excel', className: 'btn yellow btn-outline ' },
          { extend: 'colvis', className: 'btn dark btn-outline', text: "Colunas"}
        ],

        processing: true,
        serverSide: true,
        searchDelay: 800,
        columnDefs: [
          { targets: 0, name: "date", class: "text-center", data: 0, title: "Data criação"},
          { targets: 1, name: "date", class: "text-center", title: "Tipo", render: function(data, type, row) {
            return self.paymentMethodEnum.findById(row[1]).name;
          }},
          { targets: 2, name: "installments", class: "text-center", title: "Condições", render: function(data, type, row) {
            return row[2] + 'x';
          }},
          { targets: 3, class: "text-right", title: "Tx. c. conta (%)", render: function(data, type, row) {
            return $filter('currency')(row[3], "", 2);
          }},
          { targets: 4, class: "text-right", title: "Tx. c. fab. (%)", render: function(data, type, row) {
            return $filter('currency')(row[4], "", 2);
          }},
          { targets: 5, class: "text-right", title: "Ant. c. conta (%)", render: function(data, type, row) {
            return $filter('currency')(row[5], "", 2);
          }},
          { targets: 6, class: "text-right", title: "Ant. c. fab. (%)", render: function(data, type, row) {
            return $filter('currency')(row[6], "", 2);
          }},
          { targets: 7, class: "text-center", searchable: false, orderable: false, render: function() {
            return "";
          }, createdCell: function(td, cellData, rowData) {
            var scope = $scope.$new();

            scope.id = rowData[7];

            var removeButton = $compile('<a href="#" class="tooltips" ng-click="ctrl.deletePaymentCondition(id)" data-original-title="Apagar essa condição"><i class="fa fa-times-circle fa-2x text-danger"></i></a>')(scope);

            angular.element(td).append(removeButton);
          }},
        ],

        ajax: {
          url: "/api/payment_conditions.json",
          headers: ipCookie("auth_headers"),
          statusCode: {
            401: function() {
              toastr.remove();
              toastr.warning('Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.');
              $state.go('dashboard');
            }
          }
        },

        order: [
          [1, 'asc'], [2, 'asc']
        ],
        
        lengthMenu: [
          [5, 10, 15, 20, 300, -1],
          [5, 10, 15, 20, 300, "Todos"]
        ],
        pageLength: 100,

        dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
      });
    };

    self.changeInstallments = function() {
      if (self.model.type === self.paymentMethodEnum.CREDIT_CARD.id) {
        self.showInstallments = true;  
      } else {
        self.showInstallments = false;
        self.model.installments = 1;
      }
    };

    self.initModel = function() {
      self.model = new PaymentConditionModel();
    };

    self.save = function() {
      self.model.$save();
    };

    self.reloadTable = function() {
      self.$table.api().ajax.reload();
    };

    self.deletePaymentCondition = function(id) {
      if (confirm("Deseja apagar essa condição de pagamento?")) {
        PaymentConditionModel.$new(id).$destroy();
      }
    };

    self.init();
  });