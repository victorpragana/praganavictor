'use strict';

angular
  .module("lussApp")
  .controller("ProductionOrderIndexCtrl", function(OrderModel, ProductionOrderModel, ipCookie, lussError, $scope, $compile, $timeout) {
    var self = this;

    self.selected = [];
    self.$table = null;

    self.init = function() {
      self.$table = $('#table_production_orders').dataTable({
        language: {
          aria: {
              "sortAscending": "clique para ordenar de forma crescente",
              "sortDescending": "clique para ordenar de forma decrescente"
          },
          emptyTable: "Não existem dados para serem exibidos",
          info: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
          infoEmpty: "Nenhum registro foi encontrado",
          infoFiltered: "(filtrado de _MAX_ registros no total)",
          lengthMenu: "Mostrar _MENU_ registros",
          search: "Buscar:",
          zeroRecords: "Não foram encontrados resultados",
          processing: "Processando...",
        },

        buttons: [
          { extend: 'excel', className: 'btn yellow btn-outline ' },
          { extend: 'colvis', className: 'btn dark btn-outline', text: "Colunas" },
          { text: 'Planejamento de compra', className: 'btn green btn-outline btn-purchase-planning hide', action: function () {
              if (self.selected.length === 0) {
                alert('Primeiro selecione os pedidos');
              } else {
                ProductionOrderModel.$purchasePlan(self.selected).error(function(data) {
                  lussError.show(data.errors);
                }).success(function(response) {
                  window.open("/" + response.path);
                });
              }
            }
          }
        ],

        processing: true,
        serverSide: true,
        searchDelay: 800,
        columnDefs: [
          { targets: 0, orderable: false, searchable: false, width: 5, render: function(data, type, row) {
            return "<div class='md-checkbox-list'><div class='md-checkbox'><input type='checkbox' id='checkbox_" + row[2] + "' value='" + row[2] + "' class='md-check'><label for='checkbox_" + row[2] + "'><span></span><span class='check'></span> <span class='box'></span></label></div>";
          }},
          { targets: 1, name: "date", class: "text-center", width: "10%", data: 1, title: "Data"},
          { targets: 2, name: "id", class: "text-center", width: "10%", title: "O.P.", render: function(data, type, row) {
            return "<a href='/production_orders/" + row[2] + "'>#" + row[2] + "</a>";
          }},
          { targets: 3, data: 3, title: "Cliente" },
          { targets: 4, name: "estimate_id", class: "text-center", width: 20, title: "Orçamento", render: function(data, type, row) {
            if (row[5] === false) {
              return "<a href='/estimates/" + row[4] + "/edit'>#" + row[4] + "</a>";
            } else {
              return "<a href='/estimates/" + row[4] + "/express_edit'>#" + row[4] + "</a>";
            }
          }},
          { targets: 5, class: "text-center", width: 50, title: "", searchable: false, orderable: false, render: function() {
            return "";
          }, createdCell: function(td, cellData, rowData) {
            var scope = $scope.$new();

            scope.order_id = rowData[2];

            var lineLabelButton = $compile('<a href="javascript:;" class="margin-right-10 tooltips" ng-click="ctrl.generateLineLabel(order_id)" data-original-title="Etiqueta de persiana"><i class="fa fa-download"></i></a>')(scope);
            var componentLabelButton = $compile('<a href="javascript:;" class="margin-right-10 tooltips" ng-click="ctrl.generateComponentLabel(order_id)" data-original-title="Etiqueta de componente"><i class="fa fa-download"></i></a>')(scope);
            var cutMapButton = $compile('<a href="javascript:;" class="tooltips" ng-click="ctrl.generateCutMap(order_id)" data-original-title="Plano de corte"><i class="fa fa-download"></i></a>')(scope);

            angular.element(td).append(cutMapButton);
          }}
        ],

        ajax: {
          url: "/api/production_orders.json",
          headers: ipCookie("auth_headers")
        },

        order: [
            [1, 'asc']
        ],
        
        lengthMenu: [
            [5, 10, 15, 20, 300, -1],
            [5, 10, 15, 20, 300, "Todos"] // change per page values here
        ],
        pageLength: 100,

        dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

        drawCallback: function() {
          angular.element('.tooltips').tooltip();
        }
      });
    };

    angular.element(document).on("change", "input[type=checkbox]", function() {
      if ($(this).prop('checked')) {
        self.selected.push($(this).val());

        $(this).closest('tr').addClass("selected");
      } else {
        self.selected.splice(self.selected.indexOf($(this).val()), 1);

        $(this).closest('tr').removeClass("selected");
      }

      $timeout(function() {
        $scope.$digest();

        if (self.selected.length > 0) {
          angular.element('.btn-purchase-planning').removeClass('hide');
        } else {
          angular.element('.btn-purchase-planning').addClass('hide');
        }
      });
    });

    self.generateLineLabel = function(orderId) {
      OrderModel.$new(orderId).$linesLabel().$then(function(response) {
        window.open("/" + response.$response.data.path);
      });
    };

    self.generateComponentLabel = function(orderId) {
      OrderModel.$new(orderId).$componentsLabel().$then(function(response) {
        window.open("/" + response.$response.data.path);
      });
    };

    self.generateCutMap = function(orderId) {
      OrderModel.$new(orderId).$cutMap().$then(function(response) {
        window.open("/" + response.$response.data.path);
      });
    };

    self.init();
  });