'use strict';

angular
  .module("lussApp")
  .controller("ComponentIndexCtrl", function(ComponentModel, Enums, ipCookie, lussError, $filter, $timeout, $scope, $compile, $location) {
    var self = this;

    self.selected = [];
    self.$table   = null;
    self.errors   = [];
    self.showingDeleted = false;

    self.selectAll = function() {
      $("input[type=checkbox]").trigger("click");
    };

    self.init = function() {
      self.showingDeleted = $location.search().show_deleted === 'true';

      self.$table = $('#table_components').dataTable({
        "language": {
          "aria": {
              "sortAscending": "clique para ordenar de forma crescente",
              "sortDescending": "clique para ordenar de forma decrescente"
          },
          "emptyTable": "Não existem dados para serem exibidos",
          "info": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
          "infoEmpty": "Nenhum registro foi encontrado",
          "infoFiltered": "(filtrado de _MAX_ registros no total)",
          "lengthMenu": "Mostrar _MENU_ registros",
          "search": "Buscar:",
          "zeroRecords": "Não foram encontrados resultados",
          "processing": "Processando...",
        },

        buttons: [
            { text: 'Excel', className: 'btn blue btn-outline', action: function () {
              ComponentModel.$export().error(function() {
                toastr.remove();
                toastr.error("Erro ao exportar os componentes");
              }).success(function(response) {
                window.open("/" + response.path);
              });
            }},
            { extend: 'colvis', className: 'btn dark btn-outline', text: "Colunas"}
        ],

        processing: true,
        serverSide: true,
        searchDelay: 800,
        columnDefs: [
          { targets: 0, orderable: false, searchable: false, width: 5, render: function(data, type, row) {
            return "<div class='md-checkbox-list'><div class='md-checkbox'><input type='checkbox' id='checkbox_" + row[1] + "' value='" + row[1] + "' class='md-check'><label for='checkbox_" + row[1] + "'><span></span><span class='check'></span> <span class='box'></span></label></div>";
          }},
          { targets: 1, name: "id", class: "text-center", width: 20, title: "ID", render: function(data, type, row) {
            return "<a href='/components/" + row[1] + "/edit'>" + row[1] + "</a>";
          }},
          { targets: 2, data: 2, width: "10%", title: "Categoria" },
          { targets: 3, data: 3, title: "Nome" },
          { targets: 4, title: "Família", width: "20%", render: function() {
            return "";
          }, createdCell: function(td, cellData, rowData) {
            var scope = $scope.$new();

            scope.families = rowData[4];

            var badge = $compile('<family-list families="families"></family-list>')(scope);

            angular.element(td).append(badge);
          }},
          { targets: 5, class: "text-right", width: 40, title: "Custo", render: function(data, type, row) {
            return $filter('currency')(row[5], "", 5);
          }},
          { targets: 6, class: "text-center", width: 20, title: "UN", render: function(data, type, row) {
            return Enums.Unit.findById(row[6]).unit;
          }}
        ],

        ajax: {
          url: "/api/components.json",
          headers: ipCookie("auth_headers"),
          data: {
            show_deleted: self.showingDeleted
          }
        },

        order: [
          [2, 'asc'],
          [3, 'asc']
        ],
        
        lengthMenu: [
            [5, 10, 15, 20, 300, -1],
            [5, 10, 15, 20, 300, "Todos"] // change per page values here
        ],

        pageLength: 100,

        dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
      });
    };

    self.openDeleteModal = function() {
      $("#modal_delete_components").modal();
    };

    self.removeSelected = function() {
      ComponentModel.$batchDestroy(self.selected).error(function(data) {
        lussError.show(data.errors);

        self.$table.api().ajax.reload();
      }).success(function() {
        toastr.remove();
        toastr.success("Componentes inativados com sucesso!");

        self.$table.api().ajax.reload();
      });

      self.selected = [];
    };

    self.upload = function (file) {
      if (file !== null) {
        ComponentModel.$import(file).then(function (response) {
          toastr.remove();

          if (response.data.errors.length === 0) {
            toastr.success("Componentes importados com sucesso!");
          } else {
            self.errors = response.data.errors;

            angular.element('.alert').show();
          }

          self.$table.api().ajax.reload();
        }, function (response) {
          if (response.status === 422) {
            lussError.show(response.data.errors);
          }
        });
      }
    };

    self.hideAlert = function() {
      angular.element('.alert').hide();
    };

    angular.element(document).on("change", "input[type=checkbox]", function() {
      if ($(this).prop('checked')) {
        self.selected.push($(this).val());

        $(this).closest('tr').addClass("selected");
      } else {
        self.selected.splice(self.selected.indexOf($(this).val()), 1);

        $(this).closest('tr').removeClass("selected");
      }

      $timeout(function() {
        $scope.$digest();
      });
    });

    self.init();
  });