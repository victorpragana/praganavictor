'use strict';

angular
  .module("lussApp")
  .controller("OrderShowCtrl", function($scope, $rootScope, $stateParams, $state, $window, $interval, OrderModel, OrderLooseItemModel,
    JobStatusModel, Enums, OrderItemModel, ClientModel, ErpModel, UserModel, lussError) {
    var self = this;

    self.total             = 0;
    self.selected_options  = [];
    self.environmentEnum   = Enums.EnvironmentCategory;
    self.locationEnum      = Enums.LocationCategory;
    self.unitEnum          = Enums.Unit;
    self.itemTypeEnum      = Enums.ItemType;
    self.orderStatusEnum   = Enums.OrderStatus;
    self.paymentMethodEnum = Enums.PaymentMethod;
    self.iuguStatusEnum    = Enums.IuguPaymentStatus;
    self.userTypeEnum      = Enums.UserType;
    self.productionStatusEnum = Enums.OrderProductionStatus;
    self.workplaceTypeEnum = Enums.WorkplaceType;
    self.interval          = null;
    self.inProgress        = false;
    self.messages          = [];
    self.at                = 0;
    self.pct_complete      = 0;
    self.progress          = 0;
    self.mustReloadPage    = false;
    self.billingClients    = [];
    self.paymentConditions = [];
    self.user              = UserModel.$find($rootScope.user.id);
    self.selectedPaymentCondition = null;

    self.id = $stateParams.id;

    OrderModel.mix({
      $hooks: {
        'after-destroy': function(response) {
          if (response.status === 204) {
            toastr.remove();
            toastr.success("Pedido excluído com sucesso!");
            $state.go("orders_index");
          }
        }
      }
    });

    self._loadModel = function() {
      self.model = OrderModel.$find(self.id).$then(function(order) {
        self.total = order.total;

        self.model.items_total = order.items.reduce(function(total, item) {
          return total + item.total_with_discount;
        }, 0);

        self.model.environments_total = order.environments.reduce(function(total, environment) {
          return total + environment.total;
        }, 0);
      });
    };

    self.updateTotalWithDiscount = function(newTotal, item) {
      if (item.total !== newTotal) {
        OrderItemModel.$new(item.id).$applyTotal(newTotal).$then(function() {
          toastr.remove();
          toastr.success('Total aplicado com sucesso');

          $state.reload();
        });
      }

      return false;
    };

    self.updateValue = function(newValue, item) {
      OrderLooseItemModel.$new(item.id).$applyValue(newValue).$then(function() {
        toastr.remove();
        toastr.success('Valor unitário aplicado com sucesso');

        $state.reload();
      });

      return false;
    };

    self.updateSpecialDiscount = function(discount, type) {
      var calculatedDiscount = 0;

      if (type === 'value') {
        calculatedDiscount = parseFloat(((discount * 100) / self.model.total_with_discount).toFixed(2));
      } else {
        calculatedDiscount = discount;
      }

      OrderModel.$new(self.id).$updateSpecialDiscount(calculatedDiscount).$then(function() {
        toastr.remove();
        toastr.success('Desconto especial aplicado com sucesso');

        $state.reload();
      });

      return false;
    };

    self.addMessage = function(message) {
      var index = self.messages.indexOf(message);

      if (index === -1) {
        self.messages.unshift(message);
      }
    };

    self.canEdit = function() {
      return (self.model.production_status === self.productionStatusEnum.NOT_STARTED.id) && (self.model.status === self.orderStatusEnum.TO_SEND.id || self.model.status === self.orderStatusEnum.ERROR.id);
    };

    self.init = function() {
      self._loadModel();

      $(".modal-backdrop").remove();

      var jobId = $window.localStorage.getItem(self.id);

      if (jobId !== undefined && jobId !== null) {
        self.interval = $interval(function() {
          JobStatusModel.$find(jobId).$then(function(status) {
            self.inProgress     = status.in_progress;

            self.addMessage(status.message);

            if (self.inProgress) {
              self.at           = status.at;
              self.pct_complete = status.pct_complete;
              self.progress     = status.total;
            } else {
              $window.localStorage.removeItem(self.id);

              $interval.cancel(self.interval);

              self.interval = null;

              toastr.remove();
              toastr.info('Pedido processado');

              self.mustReloadPage = true;

              self._loadModel();
            }
          });
        }, 2000);
      }

      self.billingClients = ClientModel.$search();
    };

    self.loadPaymentConditions = function() {
      if (self.paymentConditions.length === 0) {
        self.paymentConditions = ErpModel.paymentConditions.$search();
      }
    };

    self.sendToERP = function() {
      OrderModel.$new(self.id).$sendToERP(self.selectedPaymentCondition).$then(function(response) {
        toastr.remove();
        toastr.success('O pedido esté sendo enviado');

        $window.localStorage.setItem(self.id, response.$response.data.order.job_id);

        $state.reload();
      });
    };

    self.cancelOrder = function() {
      OrderModel.$new(self.id).$cancel().$then(function() {
        toastr.remove();
        toastr.success('Pedido cancelado no ERP com sucesso!');

        $state.reload();
      });
    };

    self.destroyOrder = function() {
      OrderModel.$new(self.id).$destroy();
    };

    self.reloadState = function() {
      $state.reload();
    };

    self.reloadPage = function() {
      if (self.mustReloadPage) {
        self.reloadState();
      }
    };

    self.selectClient = function() {
      var client = self.billingClients.find(function(bc) {
        return bc.id === self.model.billing_client.id && bc.type === self.model.billing_client.type;
      });

      if (client !== undefined) {
        self.model.billing_client = client;
      }
    };

    self.updateBillingClient = function(newBillingClient) {
      if (newBillingClient.allow_special_discount !== true && self.model.special_discount > 0) {
        toastr.remove();
        toastr.warning('O cliente selecionado não permite desconto especial. Retire o desconto antes de alterar o cliente');

        return false;
      } else {
        if (newBillingClient.id !== self.model.billing_client.id || newBillingClient.type !== self.model.billing_client.type) {
          OrderModel.$new(self.id).$changeBillingClient(newBillingClient.id, newBillingClient.type).$then(function() {
            toastr.remove();
            toastr.success('Cliente para faturamento atualizado com sucesso!');
          });
        }
      }
    };

    $rootScope.$on('$stateChangeStart', function() {
      $interval.cancel(self.interval);

      self.interval = null;
    });

    self.sendToFactory = function() {
      self._updateProductionStatus(self.productionStatusEnum.NOT_STARTED.next).$then(function() {
        toastr.remove();
        toastr.success('Pedido enviado para a fábrica');

        self.model.production_status = self.productionStatusEnum.SENT_TO_FACTORY.id;
      });
    };

    self.cancelSendToFactory = function() {
      self._updateProductionStatus(self.productionStatusEnum.SENT_TO_FACTORY.previous).$then(function() {
        toastr.remove();
        toastr.success('Envio para a fábrica cancelado');

        self.model.production_status = self.productionStatusEnum.NOT_STARTED.id;
      });
    };

    self.sendToProduction = function() {
      self._updateProductionStatus(self.productionStatusEnum.SENT_TO_FACTORY.next).$then(function() {
        toastr.remove();
        toastr.success('Pedido enviado para produção');

        self.model.production_status = self.productionStatusEnum.SENT_TO_PRODUCTION.id;
      });
    };

    self.startProduction = function() {
      self._updateProductionStatus(self.productionStatusEnum.SENT_TO_PRODUCTION.next).$then(function() {
        toastr.remove();
        toastr.success('Pedido em produção');

        self.model.production_status = self.productionStatusEnum.IN_PRODUCTION.id;
      });
    };

    self.cancelSendToProduction = function() {
      self._updateProductionStatus(self.productionStatusEnum.SENT_TO_PRODUCTION.previous).$then(function() {
        toastr.remove();
        toastr.success('Envio para produção cancelado');

        self.model.production_status = self.productionStatusEnum.SENT_TO_FACTORY.id;
      });
    };

    self.finishProduction = function() {
      self._updateProductionStatus(self.productionStatusEnum.IN_PRODUCTION.next).$then(function() {
        toastr.remove();
        toastr.success('Produção finalizada');

        self.model.production_status = self.productionStatusEnum.PRODUCED.id;
      });
    };

    self.cancelProduction = function() {
      self._updateProductionStatus(self.productionStatusEnum.IN_PRODUCTION.previous).$then(function() {
        toastr.remove();
        toastr.success('Início de produção cancelado');

        self.model.production_status = self.productionStatusEnum.SENT_TO_PRODUCTION.id;
      });
    };

    self.dispatch = function() {
      self._updateProductionStatus(self.productionStatusEnum.PRODUCED.next).$then(function() {
        toastr.remove();
        toastr.success('Pedido despachado');

        self.model.production_status = self.productionStatusEnum.DISPATCHED.id;
      });
    };

    self.cancelFinishProduction = function() {
      self._updateProductionStatus(self.productionStatusEnum.PRODUCED.previous).$then(function() {
        toastr.remove();
        toastr.success('Finalização de produção cancelada');

        self.model.production_status = self.productionStatusEnum.IN_PRODUCTION.id;
      });
    };

    self.receive = function() {
      self._updateProductionStatus(self.productionStatusEnum.DISPATCHED.next).$then(function() {
        toastr.remove();
        toastr.success('Pedido recebido');

        self.model.production_status = self.productionStatusEnum.RECEIVED.id;
      });
    };

    self.cancelDispatch = function() {
      self._updateProductionStatus(self.productionStatusEnum.DISPATCHED.previous).$then(function() {
        toastr.remove();
        toastr.success('Despacho cancelado');

        self.model.production_status = self.productionStatusEnum.PRODUCED.id;
      });
    };

    self.cancelReceive = function() {
      self._updateProductionStatus(self.productionStatusEnum.RECEIVED.previous).$then(function() {
        toastr.remove();
        toastr.success('Recebimento cancelado');

        self.model.production_status = self.productionStatusEnum.DISPATCHED.id;
      });
    };

    self._updateProductionStatus = function(transition) {
      return OrderModel.$new(self.id).$updateProductionStatus(transition);
    };

    self.init();
  });