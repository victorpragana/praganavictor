'use strict';

angular
  .module("lussApp")
  .controller("OrderFormCtrl", function($scope, $stateParams, $state, $filter, $timeout, lussError, AttributeModel, FamilyModel, LineModel, OrderModel, OrderItemModel, Enums, ColorUtils) {
    var self = this;

    OrderModel.mix({
      $hooks: {
        'after-create': function(response) {
          if (response.status === 201) {
            toastr.remove();
            toastr.success("Pedido criado com sucesso!");
            $state.go("orders_index");
          }
        },
        'after-update': function(response) {
          if (response.status === 200) {
            toastr.remove();
            toastr.success("Pedido atualizado com sucesso!");
            $state.go("orders_index");
          }
        }
      }
    });

    self.lines = [];
    self.families = FamilyModel.$search();
    self.controlMethodEnum = Enums.ControlMethod;
    self.questionTypeEnum = Enums.QuestionType;
    self.unitEnum = Enums.Unit;
    self.components = [];
    self.lineQuestions = [];
    self.model = new OrderModel();
    self.model.order_item = new OrderItemModel();
    self.model.order_item.components = [];
    self.availableCategories = [];
    self.selectedComponents = [];
    self.disabledCategories = [];
    self.selectedItems = [];
    self.attributesForFiltering = [];
    self.selectedAttributes = [];
    self.questions = {};

    self.dimensionPresent = false;
    self.canSelectComponents = false;

    self.generateOrderItem = function() {
      self.components = [];
      self.availableCategories = [];
      self.disabledCategories = [];
      self.selectedItems = [];

      self.lineQuestions = [];
      self.questions = {};

      self.canSelectComponents = false;

      if (!self.validate()) {
        return;
      }

      LineModel.$find(self.model.order_item.line.id).$then(function(data) {
        self.lineQuestions = data.questions;

        if (self.lineQuestions.length === 0) {
          self._loadLineComponents();
        } else {
          self.canSelectComponents = false;
        }
      });
    };

    self._loadLineComponents = function() {
      var selectedAttributes = self.selectedAttributes.filter(function(attribute) {
        return attribute !== undefined;
      });

      self.model.order_item.line.components.$search({
        width: self.model.order_item.width,
        height: self.model.order_item.height,
        "specifications[]": selectedAttributes
      }).$then(function(componentsResponse) {
        angular.forEach(componentsResponse, function(componentResponse) {
          if (componentResponse.rule_required) {
            self.addToModel(componentResponse);
            angular.forEach(self.model.order_item.components, function(item) {
              self.selectBestComponent(item);
            });
          } else {
            self.addToComponents(componentResponse);
          }
        });

        self._setAvailableCategories();
        self._updateCategoriesStatus();

        self.selectedItems = self.availableCategories.map(function() {
          return new FormData();
        });

        self.canSelectComponents = true;
      });
    };

    self._setAvailableCategories = function() {
      self.availableCategories = Object.keys($filter('groupBy')(self.components, 'category.order')).map(function(key) {
        return parseInt(key);
      });
    };

    self._updateCategoriesStatus = function() {
      angular.forEach(self.availableCategories, function(category_order, index) {
        if (index > 0) {
          var previousCategory = self.availableCategories[index - 1];

          var previousCategoryIndex = self.model.order_item.components.map(function(component) {
            if (component.rule_required === true) {
              return null;
            }

            if (component.category) {
              return component.category.order;
            } else {
              return null;
            }
          }).indexOf(previousCategory);

          self.disabledCategories[category_order] = previousCategoryIndex === -1;
        }
      });
    };

    self.addToComponents = function(componentResponse) {
      var index = self.components.map(function(component) {
        return component.id;
      }).indexOf(componentResponse.id);

      if (index === -1) {
        self.setCategoryColor(componentResponse.category);

        self.components.push({
          id: componentResponse.id,
          name: componentResponse.name,
          characteristics: componentResponse.characteristics,
          category: componentResponse.category,
          unit: componentResponse.unit,
          rule_required: componentResponse.rule_required,
          component_ids: componentResponse.component_ids,
          child_ids: componentResponse.child_ids,
          parent_ids: componentResponse.parent_ids,
          rule_id: componentResponse.rule_id
        });
      }
    };

    self.addToModel = function(componentResponse) {
      self.setCategoryColor(componentResponse.category);

      self.model.order_item.components.push({
        component_id: componentResponse.id,
        name: componentResponse.name,
        characteristics: componentResponse.characteristics,
        category: componentResponse.category,
        unit: componentResponse.unit,
        rule_required: componentResponse.rule_required,
        component_ids: componentResponse.component_ids,
        child_ids: componentResponse.child_ids,
        parent_ids: componentResponse.parent_ids,
        rule_id: componentResponse.rule_id
      });
    };

    self.addToComponentsModel = function(item) {
      var component = {};
      component.component_id = item.id;
      component.rule_id = item.rule_id;
      component.component_ids = item.component_ids;
      component.name = item.name;
      component.characteristics = item.characteristics;
      component.category = item.category;
      component.unit = item.unit;
      component.rule_required = item.rule_required;
      component.child_ids = item.child_ids;
      component.parent_ids = item.parent_ids;

      self.removeNotRequiredComponentOfSameCategory(component);

      var index = self.model.order_item.components.map(function(component) {
        return component.rule_required === true ? -1 : component.component_id;
      }).indexOf(item.id);

      if (index === -1) {
        self.model.order_item.components.push(component);

        self.selectBestComponent(component);

        self.selectChild(item.child_ids);
      }

      self._updateCategoriesStatus();
      self._unselectRemovedComponents();
    };

    self.selectChild = function(childrenIds) {
      angular.forEach(childrenIds, function(childId) {
        var index = self.components.map(function(component) {
          return component.id;
        }).indexOf(childId);

        if (index !== -1) {
          self.components[index].from_kit = true;
        }
      });
    };

    self.setCategoryColor = function(category) {
      category.color = ColorUtils.getColor(category.name);
    };

    self.validate = function() {
      if (self.model.order_item.line === null) {
        toastr.warning("Selecione uma linha.");
        return false;
      }

      if (self.model.order_item.width === null || self.model.order_item.width <= 0) {
        toastr.warning("Preencha corretamente a largura.");
        return false;
      }

      if (self.model.order_item.height === null || self.model.order_item.height <= 0) {
        toastr.warning("Preencha corretamente a altura.");
        return false;
      }

      var valid_width = false;
      var valid_height = false;

      angular.forEach(self.model.order_item.line.dimensions, function(dimension) {
        if (self.model.order_item.width >= dimension.initial_width && self.model.order_item.width <= dimension.final_width) {
          valid_width = true;
        }

        if (self.model.order_item.height >= dimension.initial_height && self.model.order_item.height <= dimension.final_height) {
          valid_height = true;
        }
      });

      if (!valid_width) {
        toastr.warning("A largura especificada encontra-se fora dos limites da linha.");
        return false;
      }

      if (!valid_height) {
        toastr.warning("A altura especificada encontra-se fora dos limites da linha.");
        return false;
      }

      return true;
    };

    self.cancelOrderItem = function() {
      self.resetComponents();

      self.lineQuestions = [];
    };

    self.init = function() {
      $("input[data-plugin='masked-input']").inputmask("decimal", {
        radixPoint: ',',
        digits: 2,
        rightAlign: false,
        autoUnmask: true,
        unmaskAsNumber: true,
        allowMinus: false
      });

      $('.date-picker').datepicker({
        rtl: App.isRTL(),
        orientation: "left",
        format: "dd/mm/yyyy",
        autoclose: true,
        language: 'pt-BR',
        todayBtn: true,
        todayHighlight: true
      }).datepicker('update', new Date());

      self.dimensionPresent = false;
    };

    self.placeOrderItem = function() {
      self.model.order_item.line_id = self.model.order_item.line.id;

      if (!self.allRequiredCategoriesSelected()) {
        toastr.error("Itens obrigatórios não foram selecionados. Contate o administrador.");
      } else {
        self.model.$save();
      }
    };

    self.selectBestComponent = function($item) {
      OrderItemModel.$select(self.model.order_item.line.id, $item, self.model.order_item.width, self.model.order_item.height, self.questions).success(function(data, status) {
        if (status === 200) {
          $item.component_id = data.component.id;
          $item.component_name = data.component.name;
          $item.component_automatic = data.component.automatic;
          $item.cost = data.component.price.cost;
          $item.price = data.component.price.price;
          $item.total_consumption = data.component.price.total_consumption;
          $item.width_consumption = data.component.price.width_consumption;
          $item.height_consumption = data.component.price.height_consumption;
          $item.quantity_consumption = data.component.price.quantity_consumption;
          $item.total = data.component.price.total;

          var index = self.model.order_item.components.map(function(component) {
            return component.component_id;
          }).indexOf($item.component_id);

          if (index !== -1) {
            self.model.order_item.components[index].name = $item.component_name;
            self.model.order_item.components[index].automatic = $item.component_automatic;
          }
        }
      }).error(function(data) {
        lussError.show(data.errors);
      });
    };

    self.calculateTotal = function(markup) {
      var total = 0;

      angular.forEach(self.model.order_item.components, function(component) {
        total += component.total;
      });

      if (markup === undefined || markup === null) {
        return total;
      } else {
        return (total * (1 + (markup / 100)));
      }
    };

    self.filterByDimension = function() {
      var selectedAttributes = self.selectedAttributes.filter(function(attribute) {
        return attribute !== undefined;
      });

      if (self.model.order_item.width !== undefined && self.model.order_item.width !== "" && self.model.order_item.height !== undefined && self.model.order_item.height !== "") {
        self.lines = LineModel.$search({
          width: self.model.order_item.width,
          height: self.model.order_item.height,
          "specifications[]": selectedAttributes,
          family_id: self.model.order_item.family_id
        }).$then(function(lines) {
          if (lines.length === 0) {
            toastr.remove();
            toastr.warning("Não foi encontrada nenhuma linha, por favor verifique as dimensões informadas e tente novamente.");
          } else {
            self.dimensionPresent = true;

            self.attributesForFiltering = AttributeModel.forFiltering(self.model.order_item.family_id).$search();
          }
        });
      }
    };

    self.initTooltip = function() {
      $('.tooltips').tooltip();
    };

    self.allRequiredCategoriesSelected = function() {
      var required_categories = [];

      angular.forEach(self.model.order_item.line.categories, function(category) {
        if (category.required) {
          required_categories.push(category.category_id);
        }
      });

      angular.forEach(self.model.order_item.components, function(item) {
        var index = required_categories.indexOf(item.category.id);

        if (index !== -1) {
          required_categories.splice(index, 1);
        }
      });

      return required_categories.length === 0;
    };

    self.removeComponent = function(component) {
      angular.forEach(self.model.order_item.components, function(item, index) {
        if (item.category.id === component.category.id && item.component_id === component.component_id && !item.rule_required) {
          self.model.order_item.components.splice(index, 1);

          self._removeLowerCategoryComponents(item);
          self._updateCategoriesStatus();

          return;
        }
      });

      self._unselectRemovedComponents();
    };

    self.removeNotRequiredComponentOfSameCategory = function(component) {
      angular.forEach(self.model.order_item.components, function(item, index) {
        if (item.category.id === component.category.id && item.component_id !== component.component_id && !item.rule_required) {
          self.model.order_item.components.splice(index, 1);

          self._removeLowerCategoryComponents(item);
          self._updateCategoriesStatus();

          return;
        }
      });
    };

    self._removeLowerCategoryComponents = function(component) {
      angular.forEach(self.model.order_item.components.slice(0), function(item) {
        if (item.category.order > component.category.order && !item.rule_required) {
          var index = self.model.order_item.components.indexOf(item);

          if (index !== -1) {
            self.model.order_item.components.splice(index, 1);

            self._removeLowerCategoryComponents(item);
            self._updateCategoriesStatus();
          }
        }
      });
    };

    self._unselectRemovedComponents = function() {
      var selectedComponents = self.model.order_item.components.map(function(component) {
        return component.component_id;
      });

      angular.forEach(self.selectedItems, function(item, index) {
        var found = selectedComponents.indexOf(item.id);

        if (found === -1) {
          self.selectedItems[index] = new FormData();
        }
      });
    };

    self.proceed = function() {
      var allSelected = Object.keys(self.lineQuestions).length === Object.keys(self.questions).length;

      if (!allSelected) {
        toastr.remove();
        toastr.warning("Preencha todos os campos.");

        return false;
      } else {
        self._loadLineComponents();

        angular.element('#questions .panel-collapse').collapse('hide');
      }
    };

    self.resetComponents = function() {
      self.components = [];
      self.model.order_item.components = [];
    };

    self.init();
  });