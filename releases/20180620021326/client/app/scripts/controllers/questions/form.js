'use strict';

angular
  .module("lussApp")
  .controller("QuestionFormCtrl", function(QuestionModel, Enums, $stateParams, $state) {
    var self = this;

    self.questionTypeEnum = Enums.QuestionType;
    self.newOption = {};

    QuestionModel.mix({
      $hooks: {
        'after-create': function(response) {
          if (response.status === 201) {
            toastr.remove();
            toastr.success("Pergunta criada com sucesso!");
            
            $state.go($state.current, {}, {reload: true});

            self.model = new QuestionModel();
          }
        },
        'after-update': function(response) {
          if (response.status === 200) {
            toastr.remove();
            toastr.success("Pergunta atualizada com sucesso!");
            $state.go("questions_index");
          }
        },
        'after-destroy': function(response) {
          if (response.status === 204) {
            toastr.remove();
            toastr.success("Pergunta removida com sucesso!");
            $state.go("questions_index");
          }
        }
      }
    });

    self.id = $stateParams.id;

    if (self.id === undefined) {
      self.model = new QuestionModel();

      self.model.options = [];

      angular.element('#question_label')[0].focus();
    } else {
      self.model = QuestionModel.$find(self.id);
    }

    self.addOption = function() {
      if (self.newOption.label && self.newOption.value) {
        self.model.options.push({
          label: self.newOption.label,
          value: self.newOption.value
        });

        self.cancelValue();

        angular.element('#option_label')[0].focus();
        angular.element('#option_value').removeClass('edited');
      } else {
        toastr.remove();
        toastr.warning("Preencha os campos da opção.");
      }
    };

    self.removeOption = function(index) {
      self.model.options.splice(index, 1);
    };

    self.cancelValue = function() {
      self.newOption = {
        label: null,
        value: null
      };
    };

    self.init = function() {
      
    };

    self.save = function() {
      self.model.$save();
    };

    self.openDestroyModal = function() {
      $("#modal_delete_question").modal();
    };

    self.destroy = function() {
      self.model.$destroy();
    };

    self.init();
  });
