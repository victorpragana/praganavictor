'use strict';

angular
  .module("lussApp")
  .controller("CutTableFormCtrl", function(CutTableModel, $scope, $stateParams, $state) {
    var self = this;

    CutTableModel.mix({
      $hooks: {
        'after-create': function(response) {
          if (response.status === 201) {
            toastr.remove();
            toastr.success("Mesa de corte criada com sucesso!");

            $state.go($state.current, {}, {reload: true});

            self.model = new CutTableModel();
          }
        },
        'after-update': function(response) {
          if (response.status === 200) {
            toastr.remove();
            toastr.success("Mesa de corte atualizada com sucesso!");
            $state.go("cut_tables_index");
          }
        },
        'after-destroy': function(response) {
          if (response.status === 204) {
            toastr.remove();
            toastr.success("Mesa de corte excluída com sucesso!");
            $state.go("cut_tables_index");
          }
        }
      }
    });

    self.id = $stateParams.id;
    self.cutTableName = null;
    
    if (self.id === undefined) {
      self.model = new CutTableModel();
    } else {
      self.model = CutTableModel.$find(self.id).$then(function(data) {
        self.cutTableName = data.name;
      });
    }

    self.init = function() {
      angular.element("input[data-plugin='masked-input']").inputmask("decimal", {
        radixPoint: ',',
        digits: 1,
        rightAlign: false,
        autoUnmask: true,
        unmaskAsNumber: true,
        allowMinus: false
      });
    };

    self.save = function() {
      self.model.$save();
    };

    self.destroy = function() {
      self.model.$destroy();
    };

    self.init();
  });
