'use strict';

angular
  .module("lussApp")
  .controller("EstimatesIndexCtrl", function(EstimateModel, Enums, ipCookie, $filter, $state) {
    var self = this;

    self.selected = [];
    self.$table = null;

    self.selectAll = function() {
      $("input[type=checkbox]").trigger("click");
    };

    self.init = function() {
      self.$table = $('#table_estimates').dataTable({
        language: {
          aria: {
            sortAscending: "clique para ordenar de forma crescente",
            sortDescending: "clique para ordenar de forma decrescente"
          },
          emptyTable: "Não existem dados para serem exibidos",
          info: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
          infoEmpty: "Nenhum registro foi encontrado",
          infoFiltered: "(filtrado de _MAX_ registros no total)",
          lengthMenu: "Mostrar _MENU_ registros",
          search: "Buscar:",
          zeroRecords: "Não foram encontrados resultados",
          processing: "Processando...",
        },

        buttons: [
          { extend: 'excel', className: 'btn yellow btn-outline ' },
          { extend: 'colvis', className: 'btn dark btn-outline', text: "Colunas"}
        ],

        processing: true,
        serverSide: true,
        searchDelay: 800,
        columnDefs: [
          { targets: 0, name: "date", class: "text-center", width: "10%", data: 0, title: "Data"},
          { targets: 1, name: "id", class: "text-center", width: "10%", title: "Orçamento", render: function(data, type, row) {
            return "<a href='/estimates/" + row[1] + "/express_edit'>#" + row[1] + "</a>";
          }},
          { targets: 2, data: 2, title: "Vendedor", orderable: false },
          { targets: 3, data: 3, title: "Cliente", orderable: false },
          { targets: 4, class: "text-right", width: "15%", title: "Total", orderable: false, render: function(data, type, row) {
            if (row[4] > 0) {
              return $filter('currency')(row[4], "", 2);
            } else {
              return "-"; 
            }
          }},
          { targets: 5, name: "order_id", class: "text-center", width: 20, title: "Pedido", render: function(data, type, row) {
            if (row[5] !== null) {
              return "<a href='/orders/" + row[5] + "'>#" + row[5] + "</a>";
            } else {
              return "Sem pedido";
            }
          }}
        ],

        ajax: {
          url: "/api/estimates.json",
          headers: ipCookie("auth_headers"),
          statusCode: {
            401: function() {
              toastr.remove();
              toastr.warning('Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.');
              $state.go('dashboard');
            }
          }
        },

        "order": [
            [0, 'desc']
        ],
        
        "lengthMenu": [
            [5, 10, 15, 20, 300, -1],
            [5, 10, 15, 20, 300, "Todos"] // change per page values here
        ],
        "pageLength": 100,

        "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
      });
    };

    self.init();
  });