'use strict';

angular
  .module("lussApp")
  .controller("UsersIndexController", function(UserModel, Enums) {
    var self = this;

    self.users = [];
    self.userTypeEnum = Enums.UserType;
    self.workplaceTypeEnum = Enums.WorkplaceType;
		
    self.init = function() {
      $(".modal-backdrop").remove();
      
      self.loadUsers();
    };

    self.loadUsers = function() {
	    self.users = UserModel.$search();
    };
		
		self.init();
  });
	