'use strict';

angular
  .module("lussApp")
  .controller("AttributeFormCtrl", function(AttributeModel, $scope, $stateParams, $state) {
    var self = this;

    AttributeModel.mix({
      $hooks: {
        'after-create': function(response) {
          if (response.status === 201) {
            toastr.remove();
            toastr.success("Atributo criado com sucesso!");

            $state.go($state.current, {}, {reload: true});
            
            self.model = new AttributeModel();
          }
        },
        'after-update': function(response) {
          if (response.status === 200) {
            toastr.remove();
            toastr.success("Atributo atualizado com sucesso!");
            $state.go("attributes_index");
          }
        },
        'after-destroy': function(response) {
          if (response.status === 204) {
            toastr.remove();
            toastr.success("Atributo excluído com sucesso!");
            $state.go("attributes_index");
          }
        }
      }
    });

    self.id = $stateParams.id;
    self.value = new FormData();

    if (self.id === undefined) {
      self.model = new AttributeModel();
      self.model.values = [];
    } else {
      self.model = AttributeModel.$find(self.id);
    }

    self.init = function() {

    };

    self.addValue = function() {
      var index = self.model.values.map(function(e) { return e.value; }).indexOf(self.value.value);

      if (self.value.id === undefined && index !== -1) {
        toastr.remove();
        toastr.warning("O valor informado já foi adicionado.");

        return;
      } else if (self.model.automatic && self.value.comparison_value === undefined) {
        toastr.remove();
        toastr.warning("O valor de comparação deve ser informado.");

        return;
      }

      if (self.value.id !== undefined) {
        index = self.model.values.map(function(e) { return e.id; }).indexOf(self.value.id);

        self.model.values[index] = self.value;
      } else {
        self.model.values.push({value: self.value.value, comparison_value: self.value.comparison_value});
      }

      self.value = new FormData();

      angular.element("#attribute_value")[0].focus();
    };

    self.removeValue = function(value) {
      var index = self.model.values.indexOf(value);

      self.model.values.splice(index, 1);
    };

    self.editValue = function(value) {
      self.value = angular.copy(value);

      angular.element("#attribute_value")[0].focus();
    };

    self.cancelValue = function() {
      self.value = new FormData();
    };

    self.saveAttribute = function() {
      self.model.$save();
    };

    self.deleteAttribute = function() {
      self.model.$destroy();
    };

    self.openDeleteModal = function() {
      $("#model_delete_attribute").modal();
    };

    self._configMaskedInputs = function() {
      angular.element("input[data-plugin='masked-input']").inputmask("decimal", {
        radixPoint: ',',
        digits: 2,
        rightAlign: false,
        autoUnmask: true,
        unmaskAsNumber: true,
        allowMinus: false
      });
    };    

    self.init();
  });
