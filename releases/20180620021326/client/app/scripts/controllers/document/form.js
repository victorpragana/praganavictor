'use strict';

angular
  .module("lussApp")
  .controller("DocFormCtrl", function($scope, $stateParams, $state, $filter, $timeout, lussError, DocumentModel) {
    var self = this;

    self.model = DocumentModel.$fetch();

    self.init = function() {};

    self.saveDocument = function() {
      self.model.$save().$then(function(data) {
        if (data.$response.status === 200) {
          toastr.success("Documento atualizado com sucesso!");
        }
        else{
          toastr.error("Não foi possível atualizar o documento!");
        }
      });
    };

    self.init();
  });