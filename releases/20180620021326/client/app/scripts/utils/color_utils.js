'use strict';

angular.module('lussApp')
  .service('ColorUtils', function () {
    var self = this;

    self.getColor = function(string) {
      var hash = 0;

      for (var i = 0; i < string.length; i++) {
          hash = string.charCodeAt(i) + ((hash << 5) - hash);
      }

      var color = '#';
      
      for (var j = 0; j < 3; j++) {
          var value = (hash >> (j * 8)) & 0xFF;
          color += ('00' + value.toString(16)).substr(-2);
      }
      
      return color;
    };
    
    return self;
  });