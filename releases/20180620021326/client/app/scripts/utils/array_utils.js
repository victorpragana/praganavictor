'use strict';

angular.module('lussApp')
  .service('ArrayUtils', function () {
    var self = this;

    self.product = function(first_array, second_array) {
      var ret = [];
      
      first_array.forEach(function(a) {
        second_array.forEach(function(b) {
          ret.push([a, b]);
        });
      });

      return ret;
    };
    
    return self;
  });