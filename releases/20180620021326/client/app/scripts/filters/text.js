'use strict';

angular.module('lussApp')
  .filter('textFilter', function ($filter) {
    return function(collection, text) {
      collection = collection || [];
      text = text || '';

      var out = [];
      var terms = null;

      if (text === '') {
        out = collection;
      } else {
        terms = text.split('+');

        angular.forEach(terms, function(term) {
          var results = $filter('filter')(collection, term.trim(), function(actual, expected) {
            var index = expected.split(' ').map(function(query) {
              if (angular.isUndefined(actual)) {
                // No substring matching against `undefined`
                return false;
              }

              if ((actual === null) || (query === null)) {
                // No substring matching against `null`; only match against `null`
                return actual === query;
              }

              if (angular.isObject(query) || (angular.isObject(actual) && !(angular.isFunction(actual.toString) && actual.toString !== angular.toString))) {
                // Should not compare primitives against objects, unless they have custom `toString` method
                return false;
              }

              actual = angular.lowercase('' + actual);
              query = angular.lowercase('' + query);
              return actual.indexOf(query) !== -1;
            }).indexOf(false);

            return index === -1;
          });

          angular.forEach(results, function(result) {
            if (out.indexOf(result) === -1) {
              out.push(result);
            }
          });
        });
      }

      return out;
    };
  });