'use strict';

angular
  .module('lussApp')
  .directive('familyList', function($timeout){
  return {
    restrict: 'E',
    templateUrl: '/views/directives/family_list.html',
    scope: {
      families: '='
    },
    link: function (scope) {
      var self = scope;

      self.familyChars = function(name) {
        var split = name.split(' ');

        return split.length;
      };

      self.init = function() {
        $timeout(function() {
          angular.element('.tooltips').tooltip();
        });
      };

      self.init();
    }
  };
});