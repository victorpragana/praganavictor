'use strict';

angular.module('lussApp')
  .directive('addComponentsModal', function (Enums) {
    return {
      templateUrl: '/views/directives/add_components_modal.html',
      restrict: 'E',
      scope: {
        model: "=",
        components: "=",
        onClose: "&"
      },
      link: function(scope) {
        var self = scope;

        self.controlMethodEnum = Enums.ControlMethod;
        self.selected = [];
        self.selectedToRemove = [];
        self.modelComponents = [];

        self.init = function() {
          App.initSlimScroll('.scroller');

          self._selectComponents();
        };


        self.addSelected = function() {
          angular.forEach(self.selected, function(component_id) {
            if (component_id !== null) {
              var index = self.model.component_ids.indexOf(component_id);

              if (index === -1) {
                self.model.component_ids.push(component_id);
              }
            }
          });

          self.selected = [];

          angular.element("#chkSelectAll").attr("checked", false);

          self._selectComponents();
        };

        self.selectAllToAdd = function($event) {
          self._selectAll($event.target, "input[type=checkbox][id^=chk_component_]:visible");
        };

        self.selectAllAdded = function($event) {
          self._selectAll($event.target, "input[type=checkbox][id^=added_]:visible");
        };

        self._selectAll = function($target, selector) {
          if ($target.checked) {
            angular.forEach(angular.element(selector), function(element) {
              if (!element.checked) {
                element.click();
                angular.element(element).triggerHandler('click');
              }
            });
          } else {
            angular.forEach(angular.element(selector), function(element) {
              if (element.checked) {
                element.click();
                angular.element(element).triggerHandler('click');
              }
            });
          }
        };

        self.removeSelected = function() {
          angular.forEach(self.selectedToRemove, function(component_id) {
            var index = self.model.component_ids.indexOf(component_id);

            self.model.component_ids.splice(index, 1);

            self.selectedToRemove = [];
          });

          angular.element("#chkSelectAllAdded").attr("checked", false);

          self._selectComponents();
        };

        self._selectComponents = function() {
          self.modelComponents = [];

          angular.forEach(self.model.component_ids, function(component_id) {
            if (component_id !== null) {
              var component = self.components.find(function(element) {
                return element.id === component_id;
              });

              if (component !== undefined) {
                self.modelComponents.push({
                  id: component.id,
                  name: component.name,
                  families: component.families
                });
              }
            }
          });
        };

        self.executeCallback = function() {
          self.onClose();
        };

        self.filterComponents = function(item) {
          if (self.model.category_id !== undefined && self.model.category_id !== null) {
            return item.automatic === self.model.automatic && item.category.id === self.model.category_id;
          } else {
            return item.automatic === self.model.automatic;
          }
        };

        scope.$watch("model", function() {
          self._selectComponents();
          $("input[type=checkbox]").removeAttr('checked');
        });

        self.init();
      }
    };
  });