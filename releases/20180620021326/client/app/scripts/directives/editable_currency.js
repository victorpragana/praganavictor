'use strict';

angular.module('xeditable')
  .directive('editableCurrency', ['editableDirectiveFactory', function(editableDirectiveFactory) {
    return editableDirectiveFactory({
      directiveName: 'editableCurrency',
      inputTpl: '<input class="form-control input-xsmall" placeholder="Informe o subtotal">',
      render: function() {
        var self = this;

        this.parent.render.call(this);

        this.inputEl.wrap('<div class="input-group"></div>');

        angular.element(self.inputEl).inputmask("decimal", {
            radixPoint: ',',
            digits: 2,
            rightAlign: false,
            autoUnmask: true,
            unmaskAsNumber: true,
            allowMinus: false,
            min: 0
        });
      }
    });
  }]);