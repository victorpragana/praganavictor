'use strict';

angular.module('lussApp')
  .directive('unitSelection', function (Enums) {

    return {
      templateUrl: '/views/directives/unit_selection.html',
      restrict: 'E',
      scope: {
        'model':'='
      },
      link: function(scope) {
        var self = scope;

        self.unitEnum = Enums.Unit;

        self.updateModel = function($item, $model) {
          self.model = $model;
        };
      }
    };
  });