'use strict';

angular.module('lussApp')
  .directive('categorySelection', function (CategoryModel, ColorUtils, lussError) {

    return {
      templateUrl: '/views/directives/category_selection.html',
      restrict: 'E',
      scope: {
        'model':'=',
        'current':'=?'
      },
      link: function(scope) {
        var self = scope;

        self.categories = [];
        self.new_category = new CategoryModel();
        self.updated_category = null;

        CategoryModel.mix({
          $hooks: {
            'after-create': function(response) {
              if (response.status === 201) {
                toastr.remove();
                toastr.success("Categoria criada com sucesso!");

                self.new_category = new CategoryModel();
                self.loadCategories();
              }
            },
            'after-update': function(response) {
              if (response.status === 200) {
                toastr.remove();
                toastr.success("Categoria atualizada com sucesso!");

                self.updated_category = self.new_category;

                self.new_category = new CategoryModel();
                self.loadCategories();
              }
            }
          }
        });

        self.loadCategories = function() {
          if (self.updated_category !== null && self.updated_category.id === self.model) {
            self.model = null;
          }

          CategoryModel.$search().$then(function(data) {
            self.categories = [];
            
            angular.forEach(data, function(category) {
              self.categories.push({
                id:            category.id,
                name:          category.name,
                use_cut_table: category.use_cut_table,
                color:         ColorUtils.getColor(category.name)
              });
            });

            if (self.updated_category) {
              self.updateModel(self.updated_category, self.updated_category.id);
              self.updated_category = null;
            }
          });
        };

        self.openCategoryModal = function() {
          $("#modal_categories").modal();
        };

        self.createCategory = function() {
          self.new_category.$save();
        };

        self.updateModel = function($item, $model) {
          self.model = $model;
          self.current = $item;
        };

        self.removeCategory = function(category_id) {
          CategoryModel.$destroy(category_id).error(function(data) {
            lussError.show(data.errors);
          }).success(function() {
            toastr.remove();
            toastr.success("Categoria removida com sucesso!");

            self.loadCategories();
          });
        };

        self.editCategory = function(category_id) {
          self.new_category = CategoryModel.$find(category_id);
          $("#category_name")[0].focus();
        };

        self.cancelEdit = function() {
          self.new_category = new CategoryModel();
        };

        self._selectCategory = function() {
          if (self.model !== undefined && self.categories.length > 0) {
            self.current = self.categories.find(function(category) {
              return category.id === self.model;
            });
          }
        };

        scope.$watch('model', function() {
          self._selectCategory();
        });

        scope.$watchCollection('categories', function() {
          self._selectCategory();
        });

        self.loadCategories();
      }
    };
  });