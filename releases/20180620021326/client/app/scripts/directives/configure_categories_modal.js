'use strict';

angular.module('lussApp')
  .directive('configureCategoriesModal', function (CategoryModel, LineModel, $state) {
    return {
      templateUrl: '/views/directives/configure_categories_modal.html',
      restrict: 'E',
      scope: {
        model: "="
      },
      link: function(scope) {
        var self = scope;

        self.categories           = [];
        self.configuredCategories = [];

        LineModel.mix({
          $hooks: {
            'after-update': function(response) {
              if (response.status === 200) {
                toastr.remove();
                toastr.success("Categorias salvas com sucesso!");
                
                $state.go($state.current, {}, {reload: true});
              }
            }
          }
        });

        self.init = function() {
          self.loadCategories();

          $('#category_list').nestable({maxDepth: 1});
        };

        self.initTooltip = function() {
          $('.tooltips').tooltip();
        };

        self.save = function() {
          self.model.categories = $('#category_list').nestable('serialize');

          self.model.$save();
        };

        self.changeRequired = function(category_id) {
          angular.forEach(self.model.categories, function(category) {
            if (category.category_id === category_id) {
              category.required = !category.required;
              angular.element("li[data-category=" + category.category_id + "]").data("required", category.required);
            }
          });
        };

        self.isRequired = function(category_id) {
          var required = false;

          angular.forEach(self.model.categories, function(category) {
            if (category.category_id === category_id) {
              required = category.required;
            }
          });

          return required;
        };

        self.isCategorySelected = function(category_id) {
          var selected = false;

          angular.forEach(self.model.categories, function(category) {
            if (category.category_id === category_id) {
              selected = true;
            }
          });

          return selected;
        };

        self.loadCategories = function() {
          CategoryModel.$search().$then(function(data) {
            self.categories = [];
            
            angular.forEach(data, function(category) {
              self.categories.push({
                category_id:    category.id,
                name:           category.name,
                required:       false
              });
            });
          });
        };

        self.openCategoryModal = function() {
          App.initSlimScroll('.scroller');
          $("#modal_configure_categories").modal();
        };

        self.addToCategories = function() {
          angular.forEach(self.configuredCategories, function(category) {
            var index = self.model.categories.map(function(e) { return e.category_id; }).indexOf(category.category_id);

            if (index === -1) {
              self.model.categories.push({"category_id": category.category_id, "category_name": category.name, "required": false});
            }
          });

          self.configuredCategories = [];
        };

        self.removeCategory = function(category) {
          var index = self.model.categories.indexOf(category);

          self.model.categories.splice(index, 1);
        };

        self.init();
      }
    };
  });