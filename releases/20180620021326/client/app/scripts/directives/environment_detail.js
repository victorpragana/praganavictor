'use strict';

angular.module('lussApp')
  .directive('environmentDetail', function (LocationModel, OptionModel, Enums, $timeout, $state) {

    return {
      templateUrl: '/views/directives/environment_detail.html',
      restrict: 'E',
      scope: {
        'environment':'=',
        'index':'=',
        'hasOrder':'='
      },
      link: function($scope) {
        var self = $scope;

        self.environmentEnum   = Enums.EnvironmentCategory;
        self.locationEnum      = Enums.LocationCategory;
        self.locations         = [];
        self.selected_location = {};
        self.subtotal          = 0;
        self.selected_options  = 0;

        self.loadLocations = function() {
          self.environment.locations.$search().$then(function(data) {
            self.locations = data;

            angular.forEach(self.locations, function(location) {
              location.options.$search().$then(function(options) {
                location.options = options;

                angular.forEach(location.options, function(option) {
                  option.environment_id = self.environment.id;

                  if (option.selected === true) {
                    $timeout(function() {
                      self.calculateSubtotal(self.environment.id);
                    });
                  }
                });
              });
            });            
          });
        };

        OptionModel.mix({
          $hooks: {
            'after-destroy': function(response) {
              if (response.status === 204) {
                $state.reload();
              }
            }
          }
        });

        self.init = function() {
          self.location_model = new LocationModel();
          self.location_model.environment_id = self.environment.id;

          self.loadLocations();
        };

        self.addLocation = function() {
          self.location_model.$save().$then(function(data) {
            if (data.$response.status === 201) {
              self.location_model = new LocationModel();
              self.location_model.environment_id = self.environment.id;

              self.loadLocations();
            }
          });
        };

        self.openOptionModal = function(location, environment_id) {
          self.selected_location = [location];

          $("#modal_option_" + environment_id).modal();
        };

        self.calculateSubtotal = function(environment_id) {
          var selected = angular.element("input[type=checkbox][data-environment-id='" + environment_id + "']:checked");

          self.subtotal = 0;
          self.selected_options = selected.length;

          angular.forEach(selected, function(checkbox) {
            self.subtotal += parseFloat(angular.element(checkbox).val());
          });
        };

        self.deleteRecord = function(record) {
          record.$destroy().$then(function(data) {
            if (data.$response.status === 204) {
              $state.reload();
            }
          });
        };

        self.onCreateOptionCallback = function() {
          self.loadLocations();
        };

        self.init();
      }
    };
  });