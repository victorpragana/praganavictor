'use strict';

angular.module('lussApp')
  .directive('addLinesModal', function(LineModel, QuestionModel, lussError) {
    return {
      templateUrl: '/views/directives/add_lines_modal.html',
      restrict: 'E',
      scope: {
        question: "=",
        callback: '&?'
      },
      link: function(scope) {
        var self = scope;

        self.lines = [];
        self.selected = [];
        self.selectedToRemove = [];
        self.addedLines = [];
        self.model = null;
        
        self.init = function() {
          App.initSlimScroll('.scroller');
        };

        self._addLine = function(lineId) {
          var index = self.addedLines.map(function(line) {
            return line.id;
          }).indexOf(lineId);

          if (index === -1) {
            var line = self.lines.find(function(line) {
              return line.id === lineId;
            });

            self.addedLines.push(line);
          }
        };

        self.addSelected = function() {
          angular.forEach(self.selected, function(lineId) {
            if (lineId !== null) {
              self._addLine(lineId);
            }
          });

          self.selected = [];
        };

        self.removeSelected = function() {
          angular.forEach(self.selectedToRemove, function(lineId) {
            var index = self.addedLines.map(function(line) {
              return line.id;
            }).indexOf(lineId);

            self.addedLines.splice(index, 1);

            self.selectedToRemove = [];
          });
        };

        self.saveLines = function() {
          var lineIds = self.addedLines.map(function(line) {
            return line.id;
          });

          self.model.$setLines(lineIds).$then(function() {
            toastr.remove();
            toastr.success('Linhas adicionadas com sucesso!');

            self.question = null;
            self.addedLines = [];
          }, function(response) {
            lussError.show(response.$response.data.errors);
          });
        };

        self._configureAddedLines = function() {
          if (self.model.line_ids.length !== 0 && self.lines.length !== 0) {
            angular.forEach(self.model.line_ids, function(lineId) {
              self._addLine(lineId);
            });
          }
        };

        scope.$watch("question", function() {
          if (self.question !== null) {
            self.addedLines = [];

            QuestionModel.$find(self.question.id).$then(function(data) {
              self.model = data;

              self._configureAddedLines();
            });

            if (self.lines.length === 0) {
              LineModel.$search({active: true}).$then(function(data) {
                self.lines = data;

                self._configureAddedLines();
              });
            }
          }
        });

        self.init();
      }
    };
  });