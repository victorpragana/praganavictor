'use strict';

angular.module('lussApp')
  .directive('importRulesModal', function (LineModel, Enums, lussError, $state) {
    return {
      templateUrl: '/views/directives/import_rules_modal.html',
      restrict: 'E',
      scope: {
        model: "="
      },
      link: function(scope) {
        var self = scope;

        self.lines = [];
        self.rules = [];
        self.selectedLineId = null;
        self.selectedRuleIds = [];

        self.controlMethodEnum = Enums.ControlMethod;
        self.weightEnum = Enums.Weight;

        LineModel.mix({
          $hooks: {
            'after-update': function(response) {
              if (response.status === 200) {
                toastr.remove();
                toastr.success("Regras importadas com sucesso!");
                
                $state.go($state.current, {}, {reload: true});
              }
            }
          }
        });

        self.init = function() {
          self.lines = [];
          self.rules = [];
          self.selectedLineId = null;
          self.selectedRuleIds = [];
        };

        self.import = function() {
          var ruleIds = [];

          angular.forEach(self.selectedRuleIds, function(r) {
            if (r !== undefined && r !== null) {
              ruleIds.push(r);
            }
          });

          self.model.$importRules(ruleIds).$then(function() {
            toastr.remove();
            toastr.success("Regras importadas com sucesso!");
            
            $state.go($state.current, {}, {reload: true});
          }, function(response) {
            lussError.show(response.$response.data.errors);
          });
        };

        self.loadRules = function($item, $model) {
          LineModel.$new($model).$rules().$then(function(response) {
            var rules = response.$response.data.rules;

            var categories = self.model.categories.map(function(category) {
              return category.category_id;
            });

            self.rules = rules.filter(function(rule) {
              if (rule.category_id === undefined || rule.category_id === null) {
                return true;
              } else {
                return categories.indexOf(rule.category_id) !== -1;
              }
            });

            angular.forEach(self.rules, function(rule) {
              rule.control_method.name = self.controlMethodEnum.findById(rule.control_method.id).name;
            });
          });
        };

        self.loadLines = function() {
          self.lines = [];
          self.rules = [];
          self.selectedRuleIds = [];

          LineModel.$search({family_id: self.model.family_id}).$then(function(data) {
            angular.forEach(data, function(line) {
              if (line.id !== self.model.id) {
                self.lines.push({
                  id: line.id,
                  name: line.name
                });
              }
            });

            $("#modal_import_rules").modal();
          });
        };

        self.openModal = function() {
          App.initSlimScroll('.scroller');

          self.init();
          self.loadLines();
        };
      }
    };
  });