'use strict';

angular.module('lussApp')
  .directive('paymentModal', function(CreditModel, PaymentConditionModel, Enums, lussError, $window) {
    return {
      templateUrl: '/views/directives/payment_modal.html',
      restrict: 'E',
      scope: {
        callback: '&?',
        orderId: '=?',
        valueLeft: '=?'
      },
      link: function(scope) {
        var self = scope;

        CreditModel.mix({
          $hooks: {
            'after-create': function(response) {
              if (response.status === 201) {
                toastr.remove();
                toastr.success("Pagamento salvo com sucesso!");

                self.model = new CreditModel();
                self.initPayment();

                angular.element("#modal_payment").modal('toggle');

                if (response.data.credit) {
                  $window.open(response.data.credit.bank_slip_url, '_blank');
                }

                if (self.callback) {
                  self.callback();
                }
              }
            }
          }
        });

        self.model = new CreditModel();
        self.model.value = 0;

        self.paymentMethodEnum = Enums.PaymentMethod;
        self.creditTypeEnum = Enums.CreditType;
        self.creditCardConditions = [];
        self.billetCondition = null;
        self.selectedPaymentCondition = null;
        
        self.init = function() {
          //Iugu.setTestMode(true);
          App.initSlimScroll('.scroller');

          PaymentConditionModel.$search().$then(function(conditions) {
            self.creditCardConditions = conditions.filter(function(condition) {
              return condition.type === self.paymentMethodEnum.CREDIT_CARD.id;
            });

            self.billetCondition = conditions.find(function(condition) {
              return condition.type === self.paymentMethodEnum.BANK_SLIP.id;
            });
          });

          self.initPayment();
        };

        self.initPayment = function() {
          self.selectedPaymentCondition = null;
        };

        self.initCard = function() {
          new Card({
            form: 'form',
            container: '.card-wrapper',
            width: 120,
            formatting: true,
            messages: {
              validDate: 'valid\ndate',
              monthYear: 'mm/yyyy',
            },
            placeholders: {
              name: 'Nome no cartão'
            },
            debug: false
          });
        };

        self.setPaymentMethod = function($item, $model) {
          self.model.payment_method = $model;
          self.selectedPaymentCondition = null;

          self.calculateInstallment();
        };

        self.setCreditType = function($item, $model) {
          self.model.credit_type = $model;

          self.calculateInstallment();
        };

        self.setPaymentCondition = function($item, $model) {
          self.selectedPaymentCondition = $model;

          self.calculateInstallment();
        };

        self.fill_model_data = function() {
          var splited_name = Iugu.utils.getFirstLastNameByFullName(self.model.card_titular);
          var splited_expiration = Iugu.utils.getMonthYearByFullExpiration(self.model.card_expiration);

          self.model.first_name = splited_name ? splited_name[0] : '';
          self.model.last_name = splited_name ? splited_name[1] : '';
          self.model.expiration_month = splited_expiration[0];
          self.model.expiration_year = splited_expiration[1];
          self.model.brand = Iugu.utils.getBrandByCreditCardNumber(self.model.card_number);

        };

        self.calculateInstallment = function() {
          var tax = 0;

          if (self.model.payment_method === self.paymentMethodEnum.CREDIT_CARD.enumeration) {
            if (self.selectedPaymentCondition !== undefined && self.selectedPaymentCondition !== null) {
              if (self.model.credit_type === self.creditTypeEnum.MONEY.enumeration) {
                tax = self.selectedPaymentCondition.additional_tax;
              } else if (self.model.credit_type === self.creditTypeEnum.PRODUCTION.enumeration) {
                tax = self.selectedPaymentCondition.credit_tax;
              }

              self.total = (self.model.value * (1 + (tax / 100))) / self.selectedPaymentCondition.installments;
              self.installment = self.selectedPaymentCondition.installments + 'x de R$' + self.total.toFixed(2);
            }
          } else {
            self.selectedPaymentCondition = self.billetCondition;

            if (self.model.credit_type === self.creditTypeEnum.MONEY.enumeration) {
              tax = self.selectedPaymentCondition.additional_tax;
            } else if (self.model.credit_type === self.creditTypeEnum.PRODUCTION.enumeration) {
              tax = self.selectedPaymentCondition.credit_tax;
            }

            self.total = (self.model.value * (1 + (tax / 100))) / self.selectedPaymentCondition.installments;
            self.installment = self.total.toFixed(2);
          }
        };

        self.validateForm = function() {
          if (self.model === null || self.model === undefined) {
            return true;
          } else if (self.model.payment_method === null || self.model.payment_method === undefined || self.model.value === null || self.model.value === undefined ||
            self.model.value === '' || self.model.credit_type === null || self.model.credit_type === undefined) {

            return true;
          } else if (self.model.payment_method === self.paymentMethodEnum.CREDIT_CARD.enumeration) {
            if (self.model.card_number === null || self.model.card_number === undefined || self.model.card_number === '' ||
              self.model.card_code === null || self.model.card_code === undefined || self.model.card_code === '' ||
              self.model.card_titular === null || self.model.card_titular === undefined || self.model.card_titular === '' ||
              self.model.card_expiration === null || self.model.card_expiration === undefined || self.model.card_expiration === '') {

              return true;
            }
          }

          return false;
        };

        self.savePayment = function() {
          Iugu.setAccountID('83C032730EC84DE9A47B016F9308E314');

          self.model.order_id = self.orderId;
          self.model.payment_condition_id = self.selectedPaymentCondition.id;

          if (self.model.payment_method === self.paymentMethodEnum.CREDIT_CARD.enumeration) {
            self.fill_model_data();

            if (self.isCreditCardValid()) {
              Iugu.createPaymentToken(
                new Iugu.CreditCard(self.model.card_number,
                  self.model.expiration_month,
                  self.model.expiration_year,
                  self.model.first_name,
                  self.model.last_name,
                  self.model.card_code),
                self.tokenResponseHandler);
            }
          } else if (self.model.payment_method === self.paymentMethodEnum.BANK_SLIP.enumeration) {
            self.model.$save();
          }
        };

        self.tokenResponseHandler = function(data) {
          if (data.errors) {
            lussError.show(["Erro salvando cartão: " + JSON.stringify(data.errors)]);
          } else {
            self.model.credit_card_iugu_token = data.id;

            self.model.$save();
          }
        };

        self.isCreditCardValid = function() {
          var errors = [];

          if (!Iugu.utils.validateCreditCardNumber(self.model.card_number)) {
            errors.push("Número de cartão inválido");
          }
          if (!Iugu.utils.validateCVV(self.model.card_code, self.model.brand)) {
            errors.push("Dígito verificador inválido");
          }
          if (!Iugu.utils.validateExpiration(self.model.expiration_month, self.model.expiration_year)) {
            errors.push("Data de expiração inválida");
          }
          if (!Iugu.utils.validateFirstName(self.model.first_name)) {
            errors.push("Nome inválido");
          }
          if (!Iugu.utils.validateLastName(self.model.last_name)) {
            errors.push("Sobrenome inválido");
          }
          if (errors.length > 0) {
            lussError.show(errors);
            return false;
          } else {
            return true;
          }
        };

        self.initMaskedInputs = function() {
          angular.element("input[data-plugin='masked-input-decimal']").inputmask("decimal", {
            radixPoint: ',',
            digits: 2,
            rightAlign: false,
            autoUnmask: true,
            unmaskAsNumber: true,
            allowMinus: false
          });
        };

        scope.$watch(function() {
          return self.valueLeft;
        }, function(newVal) {
          if (newVal) {
            self.model.value = newVal;
          }
        });

        self.init();
      }
    };
  });