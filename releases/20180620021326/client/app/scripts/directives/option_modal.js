'use strict';

angular.module('lussApp')
  .directive('optionModal', function(AttributeModel, FamilyModel, LineModel, OptionModel, lussError, Enums, $filter, $timeout) {
    return {
      templateUrl: '/views/directives/option_modal.html',
      restrict: 'E',
      scope: {
        selectedLocation: "=",
        express: '=?',
        callback: '&?'
      },
      link: function(scope) {
        var self = scope;

        self.lines = [];
        self.families = FamilyModel.$search();
        self.attributesForFiltering = AttributeModel.forFiltering().$search();
        self.selectedAttributes = [];
        self.components = [];
        self.selectedComponents = [];
        self.disabledCategories = [];

        self.questionTypeEnum = Enums.QuestionType;

        self.model = new OptionModel();
        self.model.components = [];
        self.availableCategories = [];
        self.express = self.express === undefined ? false : self.express;

        self.showWizard = false;
        self.lineQuestions = [];
        self.questions = {};

        self.init = function() {
          self.model = new OptionModel();
          self.model.components = [];
          self.editing = false;

          self.initMaskedInputs();
        };

        self.initMaskedInputs = function() {
          angular.element("input[data-plugin='masked-input']").inputmask("decimal", {
            radixPoint: ',',
            digits: 2,
            rightAlign: false,
            autoUnmask: true,
            unmaskAsNumber: true,
            allowMinus: false
          });
        };

        self.filterByDimension = function() {
          var selectedAttributes = self.selectedAttributes.filter(function(attribute) {
            return attribute !== undefined;
          });

          if (self.model.width !== undefined && self.model.width !== "" && self.model.height !== undefined && self.model.height !== "") {
            self.lines = LineModel.$search({
              width: self.model.width,
              height: self.model.height,
              "specifications[]": selectedAttributes,
              family_id: self.model.family_id
            }).$then(function(lines) {
              if (lines.length === 0) {
                toastr.remove();
                toastr.warning("Não foi encontrada nenhuma linha, por favor verifique as dimensões informadas e tente novamente.");
              } else {
                self.attributesForFiltering = AttributeModel.forFiltering(self.model.family_id).$search();
              }
            });
          } else if (self.model.width === "" && self.model.height === "") {
            self.lines = LineModel.$search({"specifications[]": selectedAttributes, family_id: self.model.family_id});
          }
        };

        self.generateOptionItem = function() {
          self.components = [];
          self.availableCategories = [];
          self.disabledCategories = [];
          self.lineQuestions = [];
          self.questions = {};

          if (!self.validate()) {
            return;
          }

          self.showWizard = false;

          LineModel.$find(self.model.line.id).$then(function(data) {
            self.lineQuestions = data.questions;
            self.showWizard = true;

            if (self.lineQuestions.length === 0) {
              self._loadLineComponents();
            } else {
              if (self.editing) {
                var questions = self.selectedLocation[0].options[0].questions;

                angular.forEach(questions, function(_question) {
                  var question = self.lineQuestions.find(function(lineQuestion) {
                    return lineQuestion.label === _question.label;
                  });

                  if (question) {
                    if (question.type === self.questionTypeEnum.DROPDOWN.id) {
                      var option = question.options.find(function(option) {
                        return option.value === _question.value;
                      });

                      if (option) {
                        self.questions[question.value] = option.value;

                        $timeout(function() {
                          angular.element("#" + question.value + "_" + option.value).trigger('click');
                        });
                      }
                    } else {
                      self.questions[question.value] = _question.value;

                      $timeout(function() {
                        angular.element("#" + question.value + "_" + _question.value).trigger('click');
                      });
                    }
                  }
                });
              }
            }
          });
        };

        self._loadLineComponents = function() {
          var selectedAttributes = self.selectedAttributes.filter(function(attribute) {
            return attribute !== undefined;
          });

          self.model.line.components.$search({
            width: self.model.width,
            height: self.model.height,
            "specifications[]": selectedAttributes
          }).$then(function(componentsResponse) {
            angular.forEach(componentsResponse, function(componentResponse) {
              if (componentResponse.rule_required) {
                self.addToModel(componentResponse);
              } else {
                self.addToComponents(componentResponse);
              }
            });

            if (self.editing) {
              var componentsToAdd = [];

              angular.forEach(self.selectedLocation[0].options[0].components, function(element) {
                var component = self.components.find(function(_component) {
                  return _component.component_ids.includes(element.component.id);
                });

                if (component) {
                  componentsToAdd.push(component);
                }
              });

              angular.forEach($filter('orderBy')(componentsToAdd, 'category.order'), function(component, index) {
                self.selectedComponents[index] = component;
                self.addToComponentsModel(component);

                $timeout(function() {
                  angular.element("#" + component.id).trigger('click');
                });
              });
            }

            self._setAvailableCategories();
            self._updateCategoriesStatus();
          });
        };

        self._setAvailableCategories = function() {
          self.availableCategories = Object.keys($filter('groupBy')(self.components, 'category.order')).map(function(key) {
            return parseInt(key);
          });
        };

        self._updateCategoriesStatus = function() {
          angular.forEach(self.availableCategories, function(category_order, index) {
            if (index > 0) {
              var previousCategory = self.availableCategories[index - 1];

              var previousCategoryIndex = self.model.components.map(function(component) {
                if (component.rule_required === true) {
                  return null;
                }

                if (component.category) {
                  return component.category.order;
                } else {
                  return null;
                }
              }).indexOf(previousCategory);

              self.disabledCategories[category_order] = previousCategoryIndex === -1;
            }
          });
        };

        self.addToComponents = function(componentResponse) {
          var index = self.components.map(function(component) {
            return component.id;
          }).indexOf(componentResponse.id);

          if (index === -1) {
            self.components.push({
              id: componentResponse.id,
              name: componentResponse.name,
              characteristics: componentResponse.characteristics,
              category: componentResponse.category,
              unit: componentResponse.unit,
              rule_required: componentResponse.rule_required,
              informed_consumption: componentResponse.informed_consumption,
              component_ids: componentResponse.component_ids,
              child_ids: componentResponse.child_ids,
              parent_ids: componentResponse.parent_ids,
              rule_id: componentResponse.rule_id
            });
          }
        };

        self.addToModel = function(componentResponse) {
          self.model.components.push({
            component_id: componentResponse.id,
            name: componentResponse.name,
            characteristics: componentResponse.characteristics,
            category: componentResponse.category,
            unit: componentResponse.unit,
            rule_required: componentResponse.rule_required,
            informed_consumption: componentResponse.informed_consumption,
            component_ids: componentResponse.component_ids,
            child_ids: componentResponse.child_ids,
            parent_ids: componentResponse.parent_ids,
            rule_id: componentResponse.rule_id
          });
        };

        self.selectChild = function(childrenIds) {
          angular.forEach(childrenIds, function(childId) {
            var index = self.components.map(function(component) {
              return component.id;
            }).indexOf(childId);

            if (index !== -1) {
              self.components[index].from_kit = true;
            }
          });

          angular.forEach(self.selectedComponents, function(selectedComponent, index) {
            if (selectedComponent !== undefined && selectedComponent !== null) {
              var _index = self.components.map(function(component) {
                return component.id;
              }).indexOf(selectedComponent.id);

              if (_index === -1) {
                for (var i = index; i < self.selectedComponents.length; i++) {
                  self.selectedComponents[index] = null;
                }
              }
            } else {
              for (var x = index; x < self.selectedComponents.length; x++) {
                self.selectedComponents[index] = null;
              }
            }
          });

          self._setAvailableCategories();
        };

        self.validate = function() {
          if (self.model.line === null) {
            toastr.warning("Selecione uma linha.");
            return false;
          }

          if (self.model.width === null || self.model.width <= 0) {
            toastr.warning("Preencha corretamente a largura.");
            return false;
          }

          if (self.model.height === null || self.model.height <= 0) {
            toastr.warning("Preencha corretamente a largura.");
            return false;
          }

          var valid_width = false;
          var valid_height = false;

          angular.forEach(self.model.line.dimensions, function(dimension) {
            if (self.model.width >= dimension.initial_width && self.model.width <= dimension.final_width) {
              valid_width = true;
            }

            if (self.model.height >= dimension.initial_height && self.model.height <= dimension.final_height) {
              valid_height = true;
            }
          });

          if (!valid_width) {
            toastr.warning("A largura especificada encontra-se fora dos limites da linha.");
            return false;
          }

          if (!valid_height) {
            toastr.warning("A altura especificada encontra-se fora dos limites da linha.");
            return false;
          }

          return true;
        };

        self.cancelOptionItem = function() {
          self.resetComponents();

          self.lineQuestions = [];
          self.showWizard = false;
        };

        self.addToComponentsModel = function(item) {
          var component = {};
          component.component_id = item.id;
          component.rule_id = item.rule_id;
          component.name = item.name;
          component.characteristics = item.characteristics;
          component.category = item.category;
          component.unit = item.unit;
          component.component_ids = item.component_ids;
          component.rule_required = item.rule_required;
          component.child_ids = item.child_ids;
          component.parent_ids = item.parent_ids;

          self.removeNotRequiredComponentOfSameCategory(component);

          var index = self.model.components.map(function(component) {
            return component.rule_required === true ? -1 : component.component_id;
          }).indexOf(item.id);

          if (index === -1) {
            self.model.components.push(component);

            self.selectChild(item.child_ids);
          }

          self._updateCategoriesStatus();
          self._unselectRemovedComponents();
        };

        self._unselectRemovedComponents = function() {
          var selectedComponents = self.model.components.map(function(component) {
            return component.component_id;
          });

          angular.forEach(self.selectedComponents, function(item, index) {
            if (item !== null) {
              var found = selectedComponents.indexOf(item.id);

              if (found === -1) {
                self.selectedComponents[index] = new FormData();
              }
            }
          });
        };

        self.removeNotRequiredComponentOfSameCategory = function(component) {
          angular.forEach(self.model.components, function(item, index) {
            if (item.category.id === component.category.id && item.component_id !== component.component_id && !item.rule_required) {
              self.model.components.splice(index, 1);

              self._removeLowerCategoryComponents(item);

              return;
            }
          });
        };

        self._removeLowerCategoryComponents = function(component) {
          angular.forEach(self.model.components.slice(0), function(item) {
            if (item.category.order > component.category.order && !item.rule_required) {
              var index = self.model.components.indexOf(item);

              if (index !== -1) {
                self.model.components.splice(index, 1);

                self._removeLowerCategoryComponents(item);
              }
            }
          });
        };

        self.calculateTotal = function() {
          var total = 0;

          angular.forEach(self.model.components, function(component) {
            total += component.total;
          });

          return total;
        };

        self.createOption = function() {
          self.model.line_id = self.model.line.id;

          angular.forEach(self.selectedComponents, function(component) {
            angular.forEach(self.model.components, function(model_component) {
              if (component.id === model_component.component_id) {
                model_component.total_consumption = component.total_consumption;
              }
            });
          });

          var notSelectedCategories = self.requiredCategoriesNotSelected().map(function(category) {
            return category.category_name;
          });

          if (notSelectedCategories.length !== 0) {
            if (notSelectedCategories.length > 1) {
              toastr.error("As categorias <strong>" + notSelectedCategories.join(', ') + "</strong> são obrigatórias mas nenhum componente foi selecionado para elas");
            } else {
              toastr.error("A categoria <strong>" + notSelectedCategories[0] + "</strong> é obrigatória mas nenhum componente foi selecionado para ela");
            }
            self.cancelOptionItem();
          } else {
            var location = self.selectedLocation[0];

            var data = {
              successes: 0,
              errors: 0,
              total: self.selectedLocation.length,
              pending: self.selectedLocation.length
            };

            var model = angular.copy(self.model);

            self._saveLocation(model, location, data);
          }
        };

        self._saveLocation = function(model, location, data) {
          if (self.express) {
            model.width = location.width;
            model.height = location.height;
            model.quantity = location.quantity;
            model.questions = self.questions;
          }

          model.location_id = location.id;

          model.$save().$then(function(response) {
            data.pending--;

            if (response.$response.status === 201) {
              data.successes++;
            } else {
              data.errors++;
            }

            if (data.pending === 0) {
              if (data.successes !== data.total) {
                toastr.warning('Algumas opções não puderam ser criadas. Tente selecionar outra combinação de locais');
              }

              if (data.successes > 0 && self.callback !== undefined) {
                self.callback();
              }
            } else {
              location = self.selectedLocation[data.total - data.pending];

              var new_model = angular.copy(self.model);

              self._saveLocation(new_model, location, data);
            }
          }, function() {
            data.pending--;
            data.errors++;

            if (data.pending === 0) {
              if (data.successes !== data.total) {
                toastr.warning('Algumas opções não puderam ser criadas. Tente selecionar outra combinação de locais');
              }

              if (data.successes > 0 && self.callback !== undefined) {
                self.callback();
              }
            } else {
              location = self.selectedLocation[data.total - data.pending];

              var new_model = angular.copy(self.model);

              self._saveLocation(new_model, location, data);
            }
          });
        };

        self.requiredCategoriesNotSelected = function() {
          var notSelectedCategories = self.model.line.categories.filter(function(category) {
            return category.required;
          });

          angular.forEach(self.model.components, function(item) {
            var index = notSelectedCategories.map(function(category) {
              return category.category_id;
            }).indexOf(item.category.id);

            if (index !== -1) {
              notSelectedCategories.splice(index, 1);
            }
          });

          return notSelectedCategories;
        };

        self.questionsSelected = function() {
          var allSelected = Object.keys(self.lineQuestions).length === Object.keys(self.questions).length;

          if (!allSelected) {
            toastr.remove();
            toastr.warning("Preencha todos os campos.");

            return false;
          } else {
            return true;
          }
        };

        self.proceed = function() {
          if (self.questionsSelected()) {
            self._loadLineComponents();

            angular.element('#questions .panel-collapse').collapse('hide');
          }
        };

        self.resetComponents = function() {
          self.components = [];
          self.model.components = [];
          self.selectedComponents = [];
          self.editing = false;
        };

        self._populateEstimate = function() {
          var option = self.selectedLocation[0].options[0];

          var line = self.lines.find(function(line) {
            return line.id === option.line.id;
          });

          if (line) {
            self.model.line = line;

            self.generateOptionItem();
          } else {
            self.editing = false;
          }
        };

        scope.$watchCollection("selectedLocation", function(newval) {
          self.selectedLocation = newval;

          if (Array.isArray(self.selectedLocation) && self.selectedLocation.length !== 0) {
            self.model = new OptionModel();

            self.cancelOptionItem();

            var highestDimensionLocation = {
              width: null,
              height: null,
              quantity: null
            };

            angular.forEach(self.selectedLocation, function(location) {
              if (highestDimensionLocation.width === null && highestDimensionLocation.height === null) {
                highestDimensionLocation = location;
              } else {
                if ((highestDimensionLocation.width * highestDimensionLocation.height) <= (location.width * location.height)) {
                  highestDimensionLocation = location;
                }
              }
            });

            self.model.width = highestDimensionLocation.width;
            self.model.height = highestDimensionLocation.height;
            self.model.quantity = highestDimensionLocation.quantity;

            self.lines = LineModel.$search({
              width: self.model.width,
              height: self.model.height
            }).$then(function() {
              if (self.selectedLocation[0].options && self.selectedLocation[0].options.length > 0) {
                self.editing = true;

                self._populateEstimate();
              } else {
                self.editing = false;
              }
            });
          }
        });

        self.init();
      }
    };
  });