'use strict';

angular
  .module("lussApp")
  .factory("EstimateModel", function(restmod, $state, RMUtils, lussError) {
    return restmod.model('/api/estimates').mix({
      environments: { 
        hasMany: 'EnvironmentModel'
      },
      $extend: {
        Record: {
          $calculate: function() {
            return this.$send({
              method: 'PUT',
              url: RMUtils.joinUrl(this.$url(), 'calculate'),
              data: {'optimize': this.optimize}
            });
          },
          $duplicate: function() {
            return this.$send({
              method: 'POST',
              url: RMUtils.joinUrl(this.$url(), 'duplicate')
            });
          },
          $applyDiscount: function(discount) {
            return this.$send({
              method: 'PATCH',
              url: RMUtils.joinUrl(this.$url(), 'apply_discount'),
              data: {"discount": discount}
            });
          }
        }
      },
      $hooks: {
        'after-request-error': function(response) {
          switch(response.status) {
            case 422: {
              lussError.show(response.data.errors);
            } break;
            case 400: {
              lussError.show(response.data.errors);
            } break;
            case 401: {
              toastr.remove();
              toastr.warning('Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.');
              $state.go('dashboard');
            } break;
            case 404: {
              toastr.remove();
              toastr.warning('Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.');
              $state.go('dashboard');
            } break;
            default: {
              toastr.error('Erro interno do sistema.');
            }
          }
        }
      }
    });
  });