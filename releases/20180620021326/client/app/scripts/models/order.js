'use strict';

angular
  .module("lussApp")
  .factory("OrderModel", function(restmod, RMUtils, $http, $state, lussError) {
    return restmod.model('/api/orders').mix({
      order_items: { 
        hasMany: 'OrderItemModel'
      },
      $extend: {
        Model: {
          $generate: function(estimate_id, option_ids) {
            return $http.post(RMUtils.joinUrl(this.$url(), 'generate'), 
              {estimate_id: estimate_id, "option_ids": option_ids}
            );
          }
        },
        Record: {
          $sendToERP: function(paymentConditionId) {
            return this.$send({
              method: 'PUT',
              url: RMUtils.joinUrl(this.$url(), 'send_to_erp'),
              data: {payment_condition_id: paymentConditionId}
            });
          },
          $cancel: function() {
            return this.$send({
              method: 'PUT',
              url: RMUtils.joinUrl(this.$url(), 'cancel')
            });
          },
          $changeBillingClient: function(client_id, client_type) {
            return this.$send({
              method: 'PUT',
              url: RMUtils.joinUrl(this.$url(), 'change_billing_client'),
              data: {'billing_client_id': client_id, 'billing_client_type': client_type}
            });
          },
          $updateSpecialDiscount: function(specialDiscount) {
            return this.$send({
              method: 'PUT',
              url: RMUtils.joinUrl(this.$url(), 'update_special_discount'),
              data: {'special_discount': specialDiscount}
            });
          },
          $linesLabel: function() {
            return this.$send({
              method: 'GET',
              url: RMUtils.joinUrl(this.$url(), 'lines_label')
            });
          },
          $componentsLabel: function() {
            return this.$send({
              method: 'GET',
              url: RMUtils.joinUrl(this.$url(), 'components_label')
            });
          },
          $cutMap: function() {
            return this.$send({
              method: 'GET',
              url: RMUtils.joinUrl(this.$url(), 'cut_map')
            });
          },
          $pay: function(value, credit_card_iugu_token, payment_method, installment_number) {
            return this.$send({
              method: 'POST',
              url: RMUtils.joinUrl(this.$url(), 'pay'),
              data: {'value': value, 'credit_card_iugu_token': credit_card_iugu_token, 'payment_method': payment_method, 'installment_number': installment_number}
            });
          },
          $updateProductionStatus: function(transition) {
            return this.$send({
              method: 'PUT',
              url: RMUtils.joinUrl(this.$url(), 'production_status'),
              data: { transition: transition }
            });
          }
        }
      },
      $hooks: {
        'after-request-error': function(response) {
          switch(response.status) {
            case 422: {
              lussError.show(response.data.errors);
            } break;
            case 400: {
              lussError.show(response.data.errors);
            } break;
            case 401: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            case 404: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            default: {
              toastr.error("Erro interno do sistema.");
            }
          }
        }
      }
    });
  });