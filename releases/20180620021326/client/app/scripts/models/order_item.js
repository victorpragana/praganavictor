'use strict';

angular
  .module("lussApp")
  .factory("OrderItemModel", function(restmod, RMUtils, $http, $state, lussError) {
    return restmod.model('/api/order_items').mix({
      $extend: {
        Model: {
          $select: function(line_id, component, width, height, questions) {
            return $http.get("/api/lines/" + line_id + "/components/select", 
              {params: {
                width: width,
                height: height,
                questions: questions,
                "component_ids[]": component.component_ids,
                "rule_id": component.rule_id,
                "parent_rules_ids[]": component.parent_rules_ids
              }}
            );
          },
          
        },
        Record: {
          $applyTotal: function(total) {
            return this.$send({
              method: 'PATCH',
              url: RMUtils.joinUrl(this.$url(), 'apply_total'),
              data: {"total": total}
            });
          }
        }
      },
      $hooks: {
        'after-request-error': function(response) {
          switch(response.status) {
            case 422: {
              lussError.show(response.data.errors);
            } break;
            case 400: {
              lussError.show(response.data.errors);
            } break;
            case 401: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            case 404: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            default: {
              toastr.error("Erro interno do sistema.");
            }
          }
        }
      }
    });
  });