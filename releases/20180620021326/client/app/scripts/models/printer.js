'use strict';

angular
  .module("lussApp")
  .factory("PrinterModel", function(restmod, $state, $rootScope, lussError, Faye, blockUI, $timeout, $http) {
    var _subscribed = false;
    var _timer = null;

    function errorTimout() {
      blockUI.stop();

      toastr.remove();
      toastr.error("Não houve resposta do servidor de impressão");
    }

    return restmod.model('/api/printers').mix({
      $extend: {
        Model: {
          lastVersion: function() {
            return $http.get("/print_server/version.txt");
          },
          subscribe: function(callback) {
            Faye.subscribe("/printers/*", callback);
          },
          listRemotePrinters: function(userId) {
            Faye.publish("/printers", { action: 'list', user_id: userId });
          },
          showVersions: function(callback) {
            this.subscribe(function(message) {
              const _message = JSON.parse(message);

              if (_message.user_id === $rootScope.user.id) {
                callback(_message);
              }
            });
  
            Faye.publish("/printers", { action: 'status', user_id: $rootScope.user.id });
          },
          print: function(_yield) {
            if (_subscribed === false) {
              _subscribed = true;

              this.subscribe(function(message) {
                const _message = JSON.parse(message);

                if (_message.user_id === $rootScope.user.id) {
                  blockUI.stop();

                  toastr.remove();

                  if (_message.message) {
                    toastr.info(_message.message);
                  } else if (_message.error) {
                    toastr.error(_message.error);
                  }

                  if (_timer) {
                    $timeout.cancel(_timer);

                    _timer = null;
                  }
                }
              });
            }

            _yield();

            _timer = $timeout(errorTimout, 10000);

            blockUI.start('Enviando documento para a impressora');
          }
        }
      },
      $hooks: {
        'after-request-error': function(response) {
          switch(response.status) {
            case 422: {
              lussError.show(response.data.errors);
            } break;
            case 400: {
              lussError.show(response.data.errors);
            } break;
            case 401: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            case 404: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            default: {
              toastr.error("Erro interno do sistema.");
            }
          }
        }
      }
    });
  });