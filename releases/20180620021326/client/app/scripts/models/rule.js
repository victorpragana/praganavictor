'use strict';

angular
  .module("lussApp")
  .factory("RuleModel", function(restmod, RMUtils, $state, lussError) {
    return restmod.model('/api/rules').mix({
      $extend: {
        Record: {
          $configure_kit: function() {
            return this.$send({
              method: 'PATCH',
              url: RMUtils.joinUrl(this.$url(), "configure_kit"),
              data: {"rule_ids": this.rule_ids}
            });
          },
          $cloneRule: function() {
            return this.$send({
              method: 'POST',
              url: RMUtils.joinUrl(this.$url(), "clone")
            });
          }
        }
      },
      $hooks: {
        'after-request-error': function(response) {
          switch(response.status) {
            case 422: {
              lussError.show(response.data.errors);
            } break;
            case 400: {
              lussError.show(response.data.errors);
            } break;
            case 401: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            case 404: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            default: {
              toastr.error("Erro interno do sistema.");
            }
          }
        }
      }
    });
  });