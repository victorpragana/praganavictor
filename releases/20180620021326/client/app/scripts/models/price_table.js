'use strict';

angular
  .module("lussApp")
  .factory("PriceTableModel", function($http, restmod, RMUtils, lussError, $state) {
    return restmod.model('/api/price_tables').mix({
      $extend: {
        Model: {
          $batchDestroy: function(ids) {
            return $http.delete(RMUtils.joinUrl(this.$url(), "batch_destroy"), {
              params: {"ids[]": ids}
            });
          }
        },
        Record: {
          $applyToDistributors: function(ids) {
            return this.$send({
              method: 'POST',
              url: RMUtils.joinUrl(this.$url(), "distributors/apply"),
              data: {"ids": ids}
            });
          },
          $applyToStores: function(ids) {
            return this.$send({
              method: 'POST',
              url: RMUtils.joinUrl(this.$url(), "stores/apply"),
              data: {"ids": ids}
            });
          },
          $applyToCustomers: function(ids) {
            return this.$send({
              method: 'POST',
              url: RMUtils.joinUrl(this.$url(), "customers/apply"),
              data: {"ids": ids}
            });
          }
        }
      },
      $hooks: {
        'after-request-error': function(response) {
          switch(response.status) {
            case 422: {
              lussError.show(response.data.errors);
            } break;
            case 400: {
              lussError.show(response.data.errors);
            } break;
            case 401: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            case 404: {
              toastr.remove();
              toastr.warning("Caminho não encontrado ou seu usuário não possui permissão para acessar esse recurso.");
              $state.go('dashboard');
            } break;
            default: {
              toastr.error("Erro interno do sistema.");
            }
          }
        }
      }
    });
  });