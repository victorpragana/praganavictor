require 'rails_helper'

RSpec.describe OrderItemService, type: :service do
  describe "#apply_total" do
    before(:each) do
      @factory = FactoryGirl.create(:factory, name: 'F1')

      FactoryGirl.create(:administrator, username: 'F1').tap do |administrator|
        administrator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @factory, user: administrator))
      end

      @family = FactoryGirl.create(:family)

      @category = FactoryGirl.create(:category, name: 'C1')
      @category_2 = FactoryGirl.create(:category, name: 'C2')
      @category_3 = FactoryGirl.create(:category, name: 'C3')

      @line = FactoryGirl.create(:line, family: @family)

      @line.categories << FactoryGirl.build(:line_category, category: @category, required: true)
      @line.categories << FactoryGirl.build(:line_category, category: @category_3, required: false)

      @line.save!

      @component_1 = FactoryGirl.create(:component, unit: :cm, category: @category, erasable: true, cost: BigDecimal.new('32.28'), name: 'CC1')

      @distributor = FactoryGirl.create(:distributor, name: 'D1')
      @store       = FactoryGirl.create(:store, distributor: @distributor, name: 'S1')
      @customer    = FactoryGirl.create(:customer, store_id: @store.id, name: 'C1')

      @order = FactoryGirl.create(:order, environments: [], seller: @distributor, client: client, billing_client: client, user: @factory.users.first)
      @order_item_1 = FactoryGirl.create(:order_item, order: @order, line: @line, components:
        [
          FactoryGirl.build(:order_item_component, component_id: @component_1.id, component_name: @component_1.name, unit: @component_1.unit)
        ]
      )
     
    end
    context 'to distributor' do
      let(:parameters) {{'total' => '50.0'}}
      let(:service) {described_class.new(parameters)}
      let(:client) {@distributor}

      it 'applies value to distributor_total' do
        service.apply_total(@order_item_1.id)
        @order_item_1.reload
        expect(@order_item_1.distributor_total).to eq(50)
      end

      it 'does not apply value to store_total' do
        service.apply_total(@order_item_1.id)
        @order_item_1.reload
        expect(@order_item_1.store_total).to eq(0)
      end

      it 'does not apply value to customer_total' do
        service.apply_total(@order_item_1.id)
        @order_item_1.reload
        expect(@order_item_1.customer_total).to eq(0)
      end
    end

    context 'to store' do
      let(:parameters) {{'total' => '50.0'}}
      let(:service) {described_class.new(parameters)}
      let(:client) {@store}

      it 'applies value to store_total' do
        service.apply_total(@order_item_1.id)
        @order_item_1.reload
        expect(@order_item_1.store_total).to eq(50)
      end

      it 'does not apply value to distributor_total' do
        service.apply_total(@order_item_1.id)
        @order_item_1.reload
        expect(@order_item_1.distributor_total).to eq(0)
      end

      it 'does not apply value to customer_total' do
        service.apply_total(@order_item_1.id)
        @order_item_1.reload
        expect(@order_item_1.customer_total).to eq(0)
      end
    end

    context 'to customer' do
      let(:parameters) {{'total' => '60.0'}}
      let(:service) {described_class.new(parameters)}
      let(:client) {@customer}

      it 'applies value to customer_total' do
        service.apply_total(@order_item_1.id)
        @order_item_1.reload
        expect(@order_item_1.customer_total).to eq(60)
      end

      it 'does not apply value to distributor_total' do
        service.apply_total(@order_item_1.id)
        @order_item_1.reload
        expect(@order_item_1.distributor_total).to eq(0)
      end

      it 'does not apply value to store_total' do
        service.apply_total(@order_item_1.id)
        @order_item_1.reload
        expect(@order_item_1.store_total).to eq(0)
      end
    end
  end
end
