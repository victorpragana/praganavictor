require 'rails_helper'

RSpec.describe StoreService, type: :service do
  before do
    @state       = create(:state)
    @city        = create(:city, state: @state)
    @distributor = create(:distributor)
  end

  describe 'create' do
    let(:service) { described_class.new(parameters) }

    context 'with invalid parameters' do
      let(:parameters) { { 'name' => '' } }

      it 'does not create a new Store' do
        expect {
          service.create
        }.to change(Store, :count).by(0)
      end

      it 'returns false' do
        service.create
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.create
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { {'name' => 'Loja 1', 'identification' => '56.220.362/0001-42', 'zip_code' => '12244-888', 
        'street' => 'St. Foo', 'neighborhood' => 'Bar', 'number' => 'SN', 'complement' => 'ABC', 'phone' => '(12)99222-8290',
        'email' => 'foo@bar.com', 'corporate_name' => 'Abc', 'city_id' => "#{@city.id}", 'state_inscription' => '123',
        'municipal_inscription' => '333', 'distributor_id' => "#{@distributor.id}", 'allow_special_discount' => true, 'special_discount' => '12.69',
        'subdomain' => 'store-1', 'current_account' => '1234-5', 'agency' => '1234', 'bank' => "#{ThirdParty.banks[:brasil]}"
      } }

      it 'creates a new store' do
        expect {
          service.create
        }.to change(Store, :count).by(1)
      end

      it 'creates a new third party' do
        expect {
          service.create
        }.to change(ThirdParty, :count).by(1)
      end

      it 'returns the created store' do
        service.create

        expect(service.record).to eq(Store.last)
      end

      it 'returns true' do
        service.create
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.create

        store = service.record

        expect(store.name).to eq('Loja 1')
        expect(store.identification).to eq('56220362000142')
        expect(store.zip_code).to eq('12244888')
        expect(store.street).to eq('St. Foo')
        expect(store.neighborhood).to eq('Bar')
        expect(store.number).to eq('SN')
        expect(store.complement).to eq('ABC')
        expect(store.phone).to eq('12992228290')
        expect(store.email).to eq('foo@bar.com')
        expect(store.city_id).to eq(@city.id)
        expect(store.corporate_name).to eq('Abc')
        expect(store.state_inscription).to eq('123')
        expect(store.municipal_inscription).to eq('333')
        expect(store.distributor_id).to eq(@distributor.id)
        expect(store.allow_special_discount?).to eq(true)
        expect(store.special_discount).to eq(BigDecimal.new('12.69'))
        expect(store.subdomain).to eq('store-1')
        expect(store.current_account).to eq('1234-5')
        expect(store.agency).to eq('1234')
        expect(ThirdParty.banks[store.bank]).to eq(ThirdParty.banks[:brasil])
      end
    end
  end

  describe 'update' do
    let(:service) { described_class.new(parameters) }

    before do
      @store = create(:store, name: 'ab', distributor: @distributor)
    end

    context 'with invalid parameters' do
      let(:parameters) { { 'name' => '' } }

      it 'returns false' do
        service.update(@store.id)
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.update(@store.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { { 'name' => 'Loja 1', 'identification' => '56.220.362/0001-42', 'zip_code' => '12244-888', 
        'street' => 'St. Foo', 'neighborhood' => 'Bar', 'number' => 'SN', 'complement' => 'ABC', 'phone' => '(12)99222-8290',
        'email' => 'foo@bar.com', 'corporate_name' => 'Abc', 'city_id' => "#{@city.id}", 'state_inscription' => '123',
        'municipal_inscription' => '333', 'distributor_id' => "#{@distributor.id}", 'allow_special_discount' => true, 'special_discount' => '12.69',
        'subdomain' => 'store-1', 'current_account' => '1234-5', 'agency' => '1234', 'bank' => "#{ThirdParty.banks[:brasil]}"
      } }

      it 'does not create a new store' do
        expect {
          service.update(@store.id)
        }.to change(Store, :count).by(0)
      end

      it 'returns the store' do
        service.update(@store.id)

        expect(service.record).to eq(Store.last)
      end

      it 'returns true' do
        service.update(@store.id)
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.update(@store.id)

        store = service.record

        expect(store.name).to eq('Loja 1')
        expect(store.identification).to eq('56220362000142')
        expect(store.zip_code).to eq('12244888')
        expect(store.street).to eq('St. Foo')
        expect(store.neighborhood).to eq('Bar')
        expect(store.number).to eq('SN')
        expect(store.complement).to eq('ABC')
        expect(store.phone).to eq('12992228290')
        expect(store.email).to eq('foo@bar.com')
        expect(store.city_id).to eq(@city.id)
        expect(store.corporate_name).to eq('Abc')
        expect(store.state_inscription).to eq('123')
        expect(store.municipal_inscription).to eq('333')
        expect(store.distributor_id).to eq(@distributor.id)
        expect(store.allow_special_discount?).to eq(true)
        expect(store.special_discount).to eq(BigDecimal.new('12.69'))
        expect(store.subdomain).to eq('store-1')
        expect(store.current_account).to eq('1234-5')
        expect(store.agency).to eq('1234')
        expect(ThirdParty.banks[store.bank]).to eq(ThirdParty.banks[:brasil])
      end
    end
  end
end