require 'rails_helper'

RSpec.describe CommercialClassificationService, type: :service do
  before do
    family = create(:family)

    @line1 = create(:line, family: family, name: 'L1')
    @line2 = create(:line, family: family, name: 'L2')
    @line3 = create(:line, family: family, name: 'L3')
  end

  describe '#create' do
    let(:service) { described_class.new(parameters) }

    context 'with invalid parameters' do
      let(:parameters) { { 'name' => '' } }

      it 'does not create a new commercial classification' do
        expect {
          service.create
        }.to change(CommercialClassification, :count).by(0)
      end

      it 'returns false' do
        service.create
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.create
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { { 'name' => 'Table', 'line_ids' => [@line1.id.to_s, @line3.id.to_s] } }

      it 'creates a new commercial classification' do
        expect {
          service.create
        }.to change(CommercialClassification, :count).by(1)
      end

      it 'returns the created commercial classification' do
        service.create

        expect(service.record).to eq(CommercialClassification.last)
      end

      it 'returns true' do
        service.create
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.create

        commercial_classification = service.record

        expect(commercial_classification.name).to eq('Table')
        expect(commercial_classification.lines).to match_array([@line1, @line3])
      end
    end
  end

  describe 'update' do
    let(:service) { described_class.new(parameters) }

    before do
      @commercial_classification = create(:commercial_classification, lines: [@line1, @line2])
    end

    context 'with invalid parameters' do
      let(:parameters) { { 'name' => '' } }

      it 'returns false' do
        service.update(@commercial_classification.id)
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.update(@commercial_classification.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { { 'name' => 'Table', 'line_ids' => [@line1.id.to_s, @line3.id.to_s] } }

      it 'does not create a new commercial classification' do
        expect {
          service.update(@commercial_classification.id)
        }.to change(CommercialClassification, :count).by(0)
      end

      it 'returns the cut table' do
        service.update(@commercial_classification.id)

        expect(service.record).to eq(CommercialClassification.last)
      end

      it 'returns true' do
        service.update(@commercial_classification.id)
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.update(@commercial_classification.id)

        commercial_classification = service.record

        expect(commercial_classification.name).to eq('Table')
        expect(commercial_classification.lines).to match_array([@line1, @line3])
      end
    end
  end

  describe '#apply_to_distributors' do
    let(:service) { described_class.new }

    before do
      @commercial_classification = create(:commercial_classification, name: 'ab')

      @distributor1 = create(:distributor, name: 'DA')
      @distributor2 = create(:distributor, name: 'DB')
      @distributor3 = create(:distributor, name: 'DC')

      @store = create(:store, name: 'SA', distributor: @distributor1)
    end

    context 'with invalid parameters' do
      let(:distributors) { double(:distributors) }

      before do
        allow(Distributor).to receive(:where).and_return(distributors)
        allow(distributors).to receive(:update_all).and_raise(ActiveRecord::InvalidForeignKey)
      end

      it 'does not change the distributors commercial classification' do
        service.apply_to_distributors(@commercial_classification.id, [''])
        
        [@distributor1.reload, @distributor2.reload, @distributor3.reload].each do |distributor|
          expect(distributor.commercial_classification).to be_nil
        end
      end

      it 'returns false' do
        service.apply_to_distributors(@commercial_classification.id, [''])
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.apply_to_distributors(@commercial_classification.id, [''])
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) {["#{@distributor1.id}", "#{@distributor2.id}", "#{@distributor3.id}"]}

      it 'updates the distributors commercial classification' do
        service.apply_to_distributors(@commercial_classification.id, parameters)
        
        [@distributor1.reload, @distributor2.reload, @distributor3.reload].each do |distributor|
          expect(distributor.commercial_classification).to eq(@commercial_classification)
        end
      end

      it 'returns true' do
        service.apply_to_distributors(@commercial_classification.id, parameters)
        
        expect(service.success).to eq(true)
      end
    end
  end

  describe '#apply_to_stores' do
    let(:service) { described_class.new }

    before do
      @commercial_classification = create(:commercial_classification, name: 'ab')

      @distributor = create(:distributor, name: 'DA')

      @store1 = create(:store, name: 'SA', distributor: @distributor)
      @store2 = create(:store, name: 'SB', distributor: @distributor)
      @store3 = create(:store, name: 'SC', distributor: @distributor)
    end

    context 'with invalid parameters' do
      let(:stores) { double(:stores) }

      before do
        allow(Store).to receive(:where).and_return(stores)
        allow(stores).to receive(:update_all).and_raise(ActiveRecord::InvalidForeignKey)
      end

      it 'does not change the stores commercial classification' do
        service.apply_to_stores(@commercial_classification.id, [''])
        
        [@store1.reload, @store2.reload, @store3.reload].each do |store|
          expect(store.commercial_classification).to be_nil
        end
      end

      it 'returns false' do
        service.apply_to_stores(@commercial_classification.id, [''])
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.apply_to_stores(@commercial_classification.id, [''])
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) {["#{@store1.id}", "#{@store2.id}", "#{@store3.id}"]}

      it 'updates the stores commercial classification' do
        service.apply_to_stores(@commercial_classification.id, parameters)
        
        [@store1.reload, @store2.reload, @store3.reload].each do |store|
          expect(store.commercial_classification).to eq(@commercial_classification)
        end
      end

      it 'returns true' do
        service.apply_to_stores(@commercial_classification.id, parameters)
        
        expect(service.success).to eq(true)
      end
    end
  end
end
