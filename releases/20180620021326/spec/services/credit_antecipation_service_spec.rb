require 'rails_helper'

RSpec.describe CreditAntecipationService, type: :service do
  describe '#antecipate' do
    let(:service) { described_class.new(movements, owner) }

    before do
      @factory = create(:factory, name: 'F1')
      @distributor = create(:distributor, name: 'D1')
      @store = create(:store, name: 'S1', distributor: @distributor)

      @user = create(:administrator).tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: @store, user: administrator))
      end

      @order = create(:order, seller: @store, client: @distributor, billing_client: @distributor, user: @user)

      @movimentation3 = create(:production_credit, owner: @store.third_party, destination: @distributor, direction: :output, date: 1.month.from_now, value: -400, order: @order, reason: :send_to_factory)
      @movimentation4 = create(:production_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :input, date: 17.hours.ago, value: 200, reason: :send_to_factory)
      @movimentation5 = create(:production_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :output, date: 15.hours.ago, value: -50, order: @order, reason: :order_production)

      @payment_condition1 = create(:payment_condition, installments: 1, antecipation_tax: 10, credit_antecipation_tax: 11)
      @payment_condition2 = create(:payment_condition, installments: 2, antecipation_tax: 12, credit_antecipation_tax: 13)
      @payment_condition3 = create(:payment_condition, installments: 3, antecipation_tax: 14, credit_antecipation_tax: 15)
    end

    describe 'validation' do
      before do
        @payment = create(:payment, installment_number: 1)
        @movimentation1 = create(:production_credit, owner: @store.third_party, destination: @distributor, direction: :input, date: 1.month.ago, value: 200, reason: :credit, payment: @payment)
        @movimentation2 = create(:production_credit, owner: @store.third_party, destination: @distributor, direction: :input, date: 1.month.from_now, value: 300, order: @order, reason: :order_payment, payment: @payment)

        @payment2 = create(:payment, installment_number: 3)
        @movimentation6 = create(:money_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :input, date: 1.month.from_now, value: 100, reason: :credit, payment: @payment2)
        @movimentation7 = create(:money_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :input, date: 2.months.from_now, value: 100, reason: :credit, payment: @payment2)
        @movimentation8 = create(:money_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :input, date: 3.months.from_now, value: 100, reason: :credit, payment: @payment2)
      end

      it 'fails if any movement is in the past' do
        expect(described_class.new([@movimentation1.id, @movimentation2.id], @store.third_party.id).valid?).to be_falsy
      end

      it 'fails if any movement is output' do
        expect(described_class.new([@movimentation3.id, @movimentation6.id, @movimentation8.id], @store.third_party.id).valid?).to be_falsy
      end

      it 'fails if any movement is pending' do
        movimentation = create(:money_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 3.months.from_now, value: 100, reason: :credit, payment: @payment2, status: :pending)
        expect(described_class.new([movimentation.id], @store.third_party.id).valid?).to be_falsy
      end

      it 'fails if any movement was antecipated' do
        movimentation = create(:money_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 3.months.from_now, value: 100, reason: :credit, payment: @payment2, status: :antecipated)
        expect(described_class.new([movimentation.id], @store.third_party.id).valid?).to be_falsy
      end

      %i[send_to_factory cancel_send_to_factory order_production cancel_order_production antecipation store_credit store_antecipation].each do |type|
        it "fails if any movement has #{type} reason" do
          movimentation = create(:money_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :input, date: 3.months.from_now, value: 100, reason: type, payment: @payment2)
          expect(described_class.new([movimentation.id], @distributor.third_party.id).valid?).to be_falsy
        end
      end

      it 'fails if the movements does not belong to the same owner' do
        expect(described_class.new([@movimentation2.id, @movimentation6.id, @movimentation8.id], @store.third_party.id).valid?).to be_falsy
      end

      it 'fails if the movements does not belong to the given owner' do
        expect(described_class.new([@movimentation6.id, @movimentation7.id, @movimentation8.id], @store.third_party.id).valid?).to be_falsy
      end

      it 'fails if the movements does not have the same destination' do
        expect(described_class.new([@movimentation2.id, @movimentation6.id, @movimentation8.id], @store.third_party.id).valid?).to be_falsy
      end

      context 'for production credit' do
        it 'fails if does not exist a payment condition for the number of installments to be antecipated' do
          movimentation = create(:production_credit, owner: @store.third_party, destination: @distributor, direction: :input, date: 8.months.from_now, value: 300, order: @order, reason: :order_payment, payment: @payment)
          expect(described_class.new([movimentation.id], @store.third_party.id).valid?).to be_falsy 
        end
      end

      context 'for money credit' do
        it 'fails if does not exist a payment condition for the number of installments to be antecipated' do
          movimentation = create(:money_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :input, date: 12.months.from_now, value: 100, reason: :credit, payment: @payment2)
          expect(described_class.new([movimentation.id], @distributor.id).valid?).to be_falsy 
        end
      end
    end

    context 'with valid parameters' do
      context 'for production credit' do
        let(:movements) { [@movimentation1.id, @movimentation6.id, @movimentation8.id] }
        let(:owner) { @store.third_party.id }

        before do
          @payment = create(:payment, installment_number: 1)
          @movimentation1 = create(:production_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 1.month.from_now, value: 200, reason: :credit, payment: @payment)
          @movimentation2 = create(:production_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 1.month.from_now, value: 300, order: @order, reason: :order_payment, payment: @payment)

          @payment2 = create(:payment, installment_number: 3)
          @movimentation6 = create(:production_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 1.month.from_now, value: 100, reason: :credit, payment: @payment2)
          @movimentation7 = create(:production_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 2.months.from_now, value: 100, reason: :credit, payment: @payment2)
          @movimentation8 = create(:production_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 3.months.from_now, value: 100, reason: :credit, payment: @payment2)
        end

        it 'marks the credits as antecipated' do
          service.antecipate

          movements.each do |movement|
            expect(Movement.find(movement).antecipated?).to be_truthy
          end
        end

        it 'creates an output production credit' do
          expect do
            service.antecipate
          end.to change(ProductionCredit.output, :count).by(1)
        end

        it 'creates the output production credit with the summed movements values for today with \'antecipation\' reason' do
          service.antecipate

          output = ProductionCredit.output.last

          expect(output.date).to be_within(1.second).of(Time.zone.now)
          expect(output.value).to eq(-400)
          expect(output.antecipation?).to be_truthy
          expect(output.owner).to eq(@store.third_party)
          expect(output.destination).to eq(@distributor.third_party)
        end

        it 'creates an input production credit' do
          expect do
            service.antecipate
          end.to change(ProductionCredit.input, :count).by(1)
        end

        it 'creates the input production credit with the summed movements values applying the antecipation tax for today with \'antecipation\' reason' do
          service.antecipate

          input = ProductionCredit.input.last

          expect(input.date).to be_within(1.second).of(Time.zone.now)
          expect(input.value).to eq(@payment_condition3.apply_antecipation_tax(400))
          expect(input.antecipation?).to be_truthy
          expect(input.owner).to eq(@store.third_party)
          expect(input.destination).to eq(@distributor.third_party)
        end

        it 'returns true' do
          expect(service.antecipate).to be_truthy
        end
      end

      context 'for money credit' do
        before do
          @payment = create(:payment, installment_number: 1)
          @movimentation1 = create(:money_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 1.month.from_now, value: 200, reason: :credit, payment: @payment)
          @movimentation2 = create(:money_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 1.month.from_now, value: 300, order: @order, reason: :order_payment, payment: @payment)

          create(:money_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :input, date: 1.month.from_now, value: 200, reason: :store_credit, payment: @payment)
          create(:money_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :input, date: 1.month.from_now, value: 300, order: @order, reason: :store_credit, payment: @payment)

          @payment2 = create(:payment, installment_number: 3)
          @movimentation6 = create(:money_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :input, date: 1.month.from_now, value: 100, reason: :credit, payment: @payment2)
          @movimentation7 = create(:money_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :input, date: 2.months.from_now, value: 100, reason: :credit, payment: @payment2)
          @movimentation8 = create(:money_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :input, date: 3.months.from_now, value: 100, reason: :credit, payment: @payment2)
        end

        context 'when the owner of the credit is the distributor' do
          let(:movements) { [@movimentation6.id, @movimentation8.id] }
          let(:owner) { @distributor.third_party.id }

          it 'marks the credits as antecipated' do
            service.antecipate

            movements.each do |movement|
              expect(Movement.find(movement).antecipated?).to be_truthy
            end
          end

          it 'creates an output money credit' do
            expect do
              service.antecipate
            end.to change(MoneyCredit.output, :count).by(1)
          end

          it 'creates the output money credit with the summed movements values for today with \'antecipation\' reason' do
            service.antecipate

            output = MoneyCredit.output.last

            expect(output.date).to be_within(1.second).of(Time.zone.now)
            expect(output.value).to eq(-200)
            expect(output.antecipation?).to be_truthy
            expect(output.owner).to eq(@distributor.third_party)
            expect(output.destination).to eq(@factory.third_party)
          end

          it 'creates an input money credit' do
            expect do
              service.antecipate
            end.to change(MoneyCredit.input, :count).by(1)
          end

          it 'creates the input money credit with the summed movements values applying the antecipation tax for today with \'antecipation\' reason' do
            service.antecipate

            input = MoneyCredit.input.last

            expect(input.date).to be_within(1.second).of(Time.zone.now)
            expect(input.value).to eq(@payment_condition3.apply_credit_antecipation_tax(200))
            expect(input.antecipation?).to be_truthy
            expect(input.owner).to eq(@distributor.third_party)
            expect(input.destination).to eq(@factory.third_party)
          end

          it 'returns true' do
            expect(service.antecipate).to be_truthy
          end
        end

        context 'when the owner of the credit is the store' do
          let(:movements) { [@movimentation1.id, @movimentation2.id] }
          let(:owner) { @store.third_party.id }

          it 'marks the credits as antecipated' do
            service.antecipate

            movements.each do |movement|
              expect(Movement.find(movement).antecipated?).to be_truthy
            end
          end

          it 'marks the equivalent distributor credits as antecipated' do
            service.antecipate

            movements.map{ |movement| Movement.find(movement).equivalent_movement.id }.each do |movement|
              expect(Movement.find(movement).antecipated?).to be_truthy
            end
          end

          it 'creates two output money credit' do
            expect do
              service.antecipate
            end.to change(MoneyCredit.output, :count).by(2)
          end

          it 'creates the output money credit with the summed movements values for today with \'antecipation\' reason for the store' do
            service.antecipate

            output = MoneyCredit.output.where(owner: @store.third_party).last

            expect(output.date).to be_within(1.second).of(Time.zone.now)
            expect(output.value).to eq(-500)
            expect(output.antecipation?).to be_truthy
            expect(output.owner).to eq(@store.third_party)
            expect(output.destination).to eq(@distributor.third_party)
          end

          it 'creates the output money credit with the summed movements values for today with \'antecipation\' reason for the distributor' do
            service.antecipate

            output = MoneyCredit.output.where(owner: @distributor.third_party).last

            expect(output.date).to be_within(1.second).of(Time.zone.now)
            expect(output.value).to eq(-500)
            expect(output.store_antecipation?).to be_truthy
            expect(output.owner).to eq(@distributor.third_party)
            expect(output.destination).to eq(@factory.third_party)
          end

          it 'creates two input money credit' do
            expect do
              service.antecipate
            end.to change(MoneyCredit.input, :count).by(2)
          end

          it 'creates the input money credit with the summed movements values applying the antecipation tax for today with \'antecipation\' reason for the store' do
            service.antecipate

            input = MoneyCredit.input.where(owner: @store.third_party).last

            expect(input.date).to be_within(1.second).of(Time.zone.now)
            expect(input.value).to eq(@payment_condition1.apply_credit_antecipation_tax(500))
            expect(input.antecipation?).to be_truthy
            expect(input.owner).to eq(@store.third_party)
            expect(input.destination).to eq(@distributor.third_party)
          end

          it 'creates the input money credit with the summed movements values applying the antecipation tax for today with \'antecipation\' reason for the distributor' do
            service.antecipate

            input = MoneyCredit.input.where(owner: @distributor.third_party).last

            expect(input.date).to be_within(1.second).of(Time.zone.now)
            expect(input.value).to eq(@payment_condition1.apply_credit_antecipation_tax(500))
            expect(input.store_antecipation?).to be_truthy
            expect(input.owner).to eq(@distributor.third_party)
            expect(input.destination).to eq(@factory.third_party)
          end

          it 'returns true' do
            expect(service.antecipate).to be_truthy
          end
        end
      end
    end
    
    context 'with invalid parameters' do
      let(:movements) { [@movimentation1.id, @movimentation6.id, @movimentation8.id] }
      let(:owner) { @store.third_party.id }

      before do
        @payment = create(:payment, installment_number: 1)
        @movimentation1 = create(:production_credit, owner: @store.third_party, destination: @distributor, direction: :input, date: 1.month.ago, value: 200, reason: :credit, payment: @payment)
        @movimentation2 = create(:production_credit, owner: @store.third_party, destination: @distributor, direction: :input, date: 1.month.from_now, value: 300, order: @order, reason: :order_payment, payment: @payment)

        @payment2 = create(:payment, installment_number: 3)
        @movimentation6 = create(:money_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :input, date: 1.month.from_now, value: 100, reason: :credit, payment: @payment2)
        @movimentation7 = create(:money_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :input, date: 2.months.from_now, value: 100, reason: :credit, payment: @payment2)
        @movimentation8 = create(:money_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :input, date: 3.months.from_now, value: 100, reason: :credit, payment: @payment2)
      end

      it 'does not create any new movement' do
        expect do
          service.antecipate
        end.to_not change(Movement, :count)
      end

      it 'returns false' do
        expect(service.antecipate).to be_falsy

      end

      it 'returns the errors' do
        service.antecipate

        expect(service.errors).to_not be_empty
      end
    end
  end
end
