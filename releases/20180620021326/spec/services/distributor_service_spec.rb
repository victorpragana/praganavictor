require 'rails_helper'

RSpec.describe DistributorService, type: :service do
  before do
    @state = create(:state)
    @city  = create(:city, state: @state)
    @family_1       = create(:family, name: 'F1')
    @family_2       = create(:family, name: 'F2')
    @line_1         = create(:line, family: @family_1, name: 'L1')
    @line_2         = create(:line, family: @family_2, name: 'L2')
    @distributor_1  = create(:distributor, name: 'D1')
    @distributor_2  = create(:distributor, name: 'D2')
  end

  describe 'create' do
    let(:service) { described_class.new(parameters) }

    context 'with invalid parameters' do
      let(:parameters) { { 'name' => '' } }

      it 'does not create a new Distributor' do
        expect {
          service.create
        }.to change(Distributor, :count).by(0)
      end

      it 'returns false' do
        service.create
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.create
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { { 'name' => 'Distributor 1', 'identification' => '56.220.362/0001-42', 'zip_code' => '12244-888', 
        'street' => 'St. Foo', 'neighborhood' => 'Bar', 'number' => 'SN', 'complement' => 'ABC', 'phone' => '(12)99222-8290',
        'email' => 'foo@bar.com', 'corporate_name' => 'Abc', 'city_id' => "#{@city.id}", 'state_inscription' => '123',
        'municipal_inscription' => '333', 'allow_special_discount' => true, 'special_discount' => '15.38', 'subdomain' => 'distributor-1',
        'current_account' => '1234-5', 'agency' => '1234', 'bank' => "#{ThirdParty.banks[:brasil]}"
      } }

      it 'creates a new distributor' do
        expect {
          service.create
        }.to change(Distributor, :count).by(1)
      end

      it 'creates a new third party' do
        expect {
          service.create
        }.to change(ThirdParty, :count).by(1)
      end

      it 'returns the created distributor' do
        service.create

        expect(service.record).to eq(Distributor.last)
      end

      it 'returns true' do
        service.create
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.create

        distributor = service.record

        expect(distributor.name).to eq('Distributor 1')
        expect(distributor.identification).to eq('56220362000142')
        expect(distributor.zip_code).to eq('12244888')
        expect(distributor.street).to eq('St. Foo')
        expect(distributor.neighborhood).to eq('Bar')
        expect(distributor.number).to eq('SN')
        expect(distributor.complement).to eq('ABC')
        expect(distributor.phone).to eq('12992228290')
        expect(distributor.email).to eq('foo@bar.com')
        expect(distributor.city_id).to eq(@city.id)
        expect(distributor.corporate_name).to eq('Abc')
        expect(distributor.state_inscription).to eq('123')
        expect(distributor.municipal_inscription).to eq('333')
        expect(distributor.allow_special_discount?).to eq(true)
        expect(distributor.special_discount).to eq(BigDecimal.new('15.38'))
        expect(distributor.subdomain).to eq('distributor-1')
        expect(distributor.current_account).to eq('1234-5')
        expect(distributor.agency).to eq('1234')
        expect(ThirdParty.banks[distributor.bank]).to eq(ThirdParty.banks[:brasil])
      end
    end
  end

  describe 'update' do
    let(:service) { described_class.new(parameters) }

    before do
      @distributor = create(:distributor, name: 'ab')
    end

    context 'with invalid parameters' do
      let(:parameters) { { 'name' => '' } }

      it 'returns false' do
        service.update(@distributor.id)
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.update(@distributor.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { { 'name' => 'Distributor 1', 'identification' => '56.220.362/0001-42', 'zip_code' => '12244-888', 
        'street' => 'St. Foo', 'neighborhood' => 'Bar', 'number' => 'SN', 'complement' => 'ABC', 'phone' => '(12)99222-8290',
        'email' => 'foo@bar.com', 'corporate_name' => 'Abc', 'city_id' => "#{@city.id}", 'state_inscription' => '123',
        'municipal_inscription' => '333', 'allow_special_discount' => true, 'special_discount' => '15.38', 'subdomain' => 'distributor-1',
        'current_account' => '1234-5', 'agency' => '1234', 'bank' => "#{ThirdParty.banks[:brasil]}"
      } }

      it 'does not create a new distributor' do
        expect {
          service.update(@distributor.id)
        }.to change(Distributor, :count).by(0)
      end

      it 'returns the distributor' do
        service.update(@distributor.id)

        expect(service.record).to eq(Distributor.last)
      end

      it 'returns true' do
        service.update(@distributor.id)
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.update(@distributor.id)

        distributor = service.record

        expect(distributor.name).to eq('Distributor 1')
        expect(distributor.identification).to eq('56220362000142')
        expect(distributor.zip_code).to eq('12244888')
        expect(distributor.street).to eq('St. Foo')
        expect(distributor.neighborhood).to eq('Bar')
        expect(distributor.number).to eq('SN')
        expect(distributor.complement).to eq('ABC')
        expect(distributor.phone).to eq('12992228290')
        expect(distributor.email).to eq('foo@bar.com')
        expect(distributor.city_id).to eq(@city.id)
        expect(distributor.corporate_name).to eq('Abc')
        expect(distributor.state_inscription).to eq('123')
        expect(distributor.municipal_inscription).to eq('333')
        expect(distributor.allow_special_discount?).to eq(true)
        expect(distributor.special_discount).to eq(BigDecimal.new('15.38'))
        expect(distributor.subdomain).to eq('distributor-1')
        expect(distributor.current_account).to eq('1234-5')
        expect(distributor.agency).to eq('1234')
        expect(ThirdParty.banks[distributor.bank]).to eq(ThirdParty.banks[:brasil])
      end
    end
  end
end