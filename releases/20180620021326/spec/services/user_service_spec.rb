require 'rails_helper'

RSpec.describe UserService, type: :service do
  before(:each) do
    @factory = FactoryGirl.create(:factory, name: 'F1')
    distributor = FactoryGirl.create(:distributor, name: 'D1')
    store = FactoryGirl.create(:store, name: 'S1', distributor: distributor)

    @workplace = [@factory, distributor, store].sample
  end

  describe 'create' do
    let(:service) {described_class.new(parameters)}

    context 'with invalid parameters' do
      let(:parameters) {{'name' => '', 'username' => 'foo', 'email' => 'bar@foo.com', 
        'password' => 'abc12311', 'password_confirmation' => 'abc12311', 'type' => 'Administrator', 'erp_id' => '20'}}

      it 'does not create a new User' do
        expect {
          service.create
        }.to change(User, :count).by(0)
      end

      it 'returns false' do
        service.create
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.create
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) {{'name' => 'Foo', 'username' => 'foo', 'email' => 'bar@foo.com', 'maximum_discount' => '12.58',
        'password' => 'abc12311', 'password_confirmation' => 'abc12311', 'type' => 'Administrator', 'erp_id' => '20',
        'workplaces' =>
          [
            'workplace_id' => "#{@workplace.id}", 'workplace_type' => "#{@workplace.actable_type}"
          ]
      }}

      ['Administrator', 'Seller', 'Manager'].each do |user_type|
        context "with #{user_type} type" do
          let(:parameters) {{'name' => 'Foo', 'username' => 'foo', 'email' => 'bar@foo.com', 'maximum_discount' => '12.58',
            'password' => 'abc12311', 'password_confirmation' => 'abc12311', 'type' => user_type, 'erp_id' => '20',
            'workplaces' =>
              [
                'workplace_id' => "#{@workplace.id}", 'workplace_type' => "#{@workplace.actable_type}"
              ]
          }}

          it "creates a new #{user_type}" do
            expect {
              service.create
            }.to change(Object.const_get(user_type), :count).by(1)
          end
        end
      end

      
      context "with Operator type" do
        let(:parameters) {{'name' => 'Foo', 'username' => 'foo', 'email' => 'bar@foo.com', 'maximum_discount' => '12.58',
          'password' => 'abc12311', 'password_confirmation' => 'abc12311', 'type' => 'Operator', 'erp_id' => '20',
          'workplaces' =>
            [
              'workplace_id' => "#{@factory.id}", 'workplace_type' => "#{@factory.actable_type}"
            ]
        }}

        it 'creates a new Operator' do
          expect {
            service.create
          }.to change(Operator, :count).by(1)
        end
      end

      it 'returns the created user' do
        service.create

        expect(service.record).to eq(User.last)
      end

      it 'returns true' do
        service.create
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.create

        user = service.record

        expect(user.name).to eq('Foo')
        expect(user.username).to eq('foo')
        expect(user.maximum_discount).to eq(BigDecimal.new('12.58'))
        expect(user.email).to eq('bar@foo.com')
        expect(user.type).to eq('Administrator')
        expect(user.workplaces).to match_array(UserWorkplace.where(user_id: user.id))
        expect(user.erp_id).to eq(20)
      end
    end
  end

  describe 'update' do
    let(:service) {described_class.new(parameters)}

    before(:each) do
      @user = FactoryGirl.create(:administrator)
    end

    context 'with invalid parameters' do
      let(:parameters) {{'name' => '', 'username' => 'foo', 'email' => 'bar@foo.com', 
        'password' => 'abc12311', 'password_confirmation' => 'abc12311', 'type' => 'Administrator', 'erp_id' => '20'}}

      it 'returns false' do
        service.update(@user.id)
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.update(@user.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) {{'name' => 'Foo', 'username' => 'foo', 'email' => 'bar@foo.com', 
        'password' => 'abc12311', 'password_confirmation' => 'abc12311', 'type' => 'Manager',
        'workplaces' =>
          [
            'workplace_id' => "#{@workplace.id}", 'workplace_type' => "#{@workplace.actable_type}"
          ]
      }}

      it 'does not create a new user' do
        expect {
          service.update(@user.id)
        }.to change(User, :count).by(0)
      end

      it 'returns the user' do
        service.update(@user.id)

        expect(service.record).to eq(User.last)
      end

      it 'returns true' do
        service.update(@user.id)
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.update(@user.id)

        user = service.record

        expect(user.name).to eq('Foo')
        expect(user.username).to eq('foo')
        expect(user.email).to eq('bar@foo.com')
        expect(user.type).to eq('Manager')
        expect(user.workplaces).to match_array(UserWorkplace.where(user_id: user.id))
      end
    end
  end
end