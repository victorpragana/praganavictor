require 'rails_helper'

RSpec.describe ItemService, type: :service do
  describe 'create' do
    let(:service) { described_class.new(parameters) }

    context 'with invalid parameters' do
      let(:parameters) { {'code' => '123-abc', 'description' => '', 'unit' => '0', 'type' => '1'} }

      it 'does not create a new Item' do
        expect {
          service.create
        }.to change(Item, :count).by(0)
      end

      it 'returns false' do
        service.create
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.create
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { {'code' => '123-abc', 'description' => 'Foo', 'unit' => '0', 'type' => '1'} }

      it 'creates a new Item' do
        expect {
          service.create
        }.to change(Item, :count).by(1)
      end

      it 'returns the created item' do
        service.create

        expect(service.record).to eq(Item.last)
      end

      it 'returns true' do
        service.create
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.create

        item = service.record

        expect(item.code).to eq('123-abc')
        expect(item.description).to eq('Foo')
        expect(Item.units[item.unit]).to eq(0)
        expect(Item.types[item.type]).to eq(1)
      end
    end
  end

  describe 'update' do
    let(:service) {described_class.new(parameters)}

    before(:each) do
      @item = FactoryGirl.create(:item)
    end

    context 'with invalid parameters' do
      let(:parameters) { {'code' => '123-abc', 'description' => '', 'unit' => '0', 'type' => '1'} }

      it 'does not create a new Item' do
        expect {
          service.update(@item.id)
        }.to change(Item, :count).by(0)
      end

      it 'returns false' do
        service.update(@item.id)
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.update(@item.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { {'code' => '123-abc', 'description' => 'Foo', 'unit' => '0', 'type' => '1',
        'erp_categories' => {"LINHA DE PRODUTO"=>"PRODUTO ACABADO", "GRUPO"=>"ADAPTADOR", "SUBGRUPO"=>"ROMANA", "TRIBUTAÇÃO"=>"TRIBUTADO ST"},
        'ncm' => '58631254', 'cst' => '4'
      } }

      it 'does not create a new Item' do
        expect {
          service.update(@item.id)
        }.to change(Item, :count).by(0)
      end

      it 'returns the item' do
        service.update(@item.id)

        expect(service.record).to eq(Item.last)
      end

      it 'returns true' do
        service.update(@item.id)
        
        expect(service.success).to eq(true)
      end

      it 'populates the data' do
        service.update(@item.id)

        item = service.record

        expect(item.code).to eq('123-abc')
        expect(item.description).to eq('Foo')
        expect(item.erp_categories).to eq({"LINHA DE PRODUTO"=>"PRODUTO ACABADO", "GRUPO"=>"ADAPTADOR", "SUBGRUPO"=>"ROMANA", "TRIBUTAÇÃO"=>"TRIBUTADO ST"})
        expect(item.ncm).to eq('58631254')
        expect(Item.csts[item.cst]).to eq(4)
        expect(Item.units[item.unit]).to eq(0)
        expect(Item.types[item.type]).to eq(1)
      end
    end
  end
end
