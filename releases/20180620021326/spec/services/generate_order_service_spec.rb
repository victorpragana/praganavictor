require 'rails_helper'

RSpec.describe GenerateOrderService, type: :service do
  before do
    @family = create(:family)
    
    @category = create(:category, name: 'C1')
    @category_2 = create(:category, name: 'C2')
    @category_3 = create(:category, name: 'C3')

    @line = create(:line, family: @family, sale_price: BigDecimal.new('12.50'))

    @line.categories << build(:line_category, category: @category, required: true)
    @line.categories << build(:line_category, category: @category_3, required: false)

    @line.save!

    @value_1 = build(:value, value: 'V1', comparison_value: 100)
    @value_2 = build(:value, value: 'V2', comparison_value: 150)
    @value_3 = build(:value, value: 'V3', comparison_value: 200)

    @specification = build(:specification, automatic: true)
    @specification.values = [@value_1, @value_2, @value_3]
    @specification.save!

    @component_1 = create(:component, name: 'aa', category: @category, erasable: true, cost: BigDecimal.new('32.28'))
    @component_2 = create(:component, name: 'bb', category: @category_3, erasable: true, cost: BigDecimal.new('20'))
    @component_3 = create(:component, name: 'cc', category: @category, erasable: true, cost: BigDecimal.new('15'), values: [@value_3])

    @line_rule_1 = create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: 5..30, required: true,
      width_consumption: '_LARGURA_*3', height_consumption: '_LARGURA_', total_consumption: '_LARGURA_*2',
      cost: '_CUSTO_BASE_+2', quantity_consumption: '_ALTURA_*2'
    )
    create(:rule, line_rule: @line_rule_1, component: @component_1, control_method: Rule.control_methods[:width], width: 5..30, required: true,
      width_consumption: '_LARGURA_*3', height_consumption: '_LARGURA_', total_consumption: '_LARGURA_*2',
      cost: '_CUSTO_BASE_+2', quantity_consumption: '_ALTURA_*2'
    )

    @line_rule_2 = create(:line_rule, line: @line, control_method: LineRule.control_methods[:height], :height => 5..30, required: false,
      total_consumption: '_ALTURA_*2', cost: '_CUSTO_BASE_*2'
    )
    create(:rule, line_rule: @line_rule_2, component: @component_2, control_method: Rule.control_methods[:height], :height => 5..30, required: false,
      total_consumption: '_ALTURA_*2', cost: '_CUSTO_BASE_*2'
    )

    @line_rule_3 = create(:line_rule, line: @line, control_method: LineRule.control_methods[:height], required: false, automatic: true,
      total_consumption: '_ALTURA_*2', cost: '_CUSTO_BASE_*2'
    )
    create(:rule, line_rule: @line_rule_2, component: @component_3, control_method: Rule.control_methods[:height], required: false, automatic: true,
      total_consumption: '_ALTURA_*2', cost: '_CUSTO_BASE_*2'
    )

    @distributor = create(:distributor, name: 'D1')
    @store       = create(:store, distributor: @distributor, name: 'S1')
    @customer    = create(:customer, store_id: @store.id, name: 'C1')
    @estimate    = create(:estimate, seller: @store, client: @customer, discount: 2, date: 2.days.ago)
    @environment = create(:environment, estimate: @estimate)
    @location    = create(:location, environment: @environment)

    @user = create(:administrator, username: 'F1').tap do |administrator|
      administrator.update!(selected_workplace: create(:user_workplace, workplace: @store, user: administrator))
    end

    @price_table = create(:price_table, owner: @store, default: true, type: :for_lines)
    create(:price_table_line, line: @line, price: '_CMV_*2', price_table: @price_table, maximum_discount: BigDecimal.new('50.1'))
    
    item_component_1 = build(:option_component, component_id: @component_3.id, component_name: @component_3.name, component_characteristics: @component_3.characteristics,
      category_name: @component_3.category.name, unit: @component_3.unit, total_consumption: 100, unit_value: 250, width_consumption: 20, height_consumption: 30, quantity_consumption: 100, required: true)

    item_component_2 = build(:option_component, component_id: @component_2.id, component_name: @component_2.name, component_characteristics: @component_2.characteristics, category_name: @component_2.category.name, unit: @component_2.unit, total_consumption: 20, unit_value: 65, required: false)
    @option_1 = create(:option, line: @line, location: @location, components: [item_component_1, item_component_2], selected: false,
      factory_total: BigDecimal.new('1500'), distributor_total: BigDecimal.new('1600'), store_total: BigDecimal.new('1700'), 
      customer_total: BigDecimal.new('1800'), discount: 8)

    @packing_configuration = create(:packing_configuration, packable: @estimate, items: [@option_1.id],
      packing: File.new(Rails.root.join("client/app/images/logo.png")), component_name: item_component_1.component_name)

    item_component_1 = build(:option_component, component_id: @component_1.id, component_name: @component_1.name, component_characteristics: @component_1.characteristics, category_name: @component_1.category.name, unit: @component_1.unit, total_consumption: 100, unit_value: 250, width_consumption: 20, height_consumption: 30, quantity_consumption: 100, required: true)
    item_component_2 = build(:option_component, component_id: @component_2.id, component_name: @component_2.name, component_characteristics: @component_2.characteristics, category_name: @component_2.category.name, unit: @component_2.unit, total_consumption: 20, unit_value: 65, required: false)
    @option_2 = create(:option, line: @line, location: @location, components: [item_component_1], selected: false,
      factory_total: BigDecimal.new('2000'), distributor_total: BigDecimal.new('2300'), store_total: BigDecimal.new('2500'), 
      customer_total: BigDecimal.new('2800'), discount: 9)

    item_component_1 = build(:option_component, component_id: @component_1.id, component_name: @component_1.name, component_characteristics: @component_1.characteristics, category_name: @component_1.category.name, unit: @component_1.unit, total_consumption: 100, unit_value: 250, width_consumption: 20, height_consumption: 30, quantity_consumption: 100, required: true)
    item_component_2 = build(:option_component, component_id: @component_2.id, component_name: @component_2.name, component_characteristics: @component_2.characteristics, category_name: @component_2.category.name, unit: @component_2.unit, total_consumption: 20, unit_value: 65, required: false)
    @option_3 = create(:option, line: @line, location: @location, components: [item_component_2], selected: false,
      factory_total: BigDecimal.new('2200'), distributor_total: BigDecimal.new('2600'), store_total: BigDecimal.new('2800'), 
      customer_total: BigDecimal.new('3000'), discount: 10)
  end

  describe 'perform' do
    let(:service) { described_class.new(parameters) }

    context 'with invalid parameters' do
      let(:parameters) { { 'estimate_id' => "#{@estimate.id}" } }
      let(:order)      { mock_model(Order).as_null_object }

      it 'does not create a new Order' do
        expect {
          service.perform
        }.to_not change(Order, :count)
      end

      it 'does not create a new Order Item' do
        expect {
          service.perform
        }.to_not change(OrderItem, :count)
      end

      it 'does not create a new Order Item Component' do
        expect {
          service.perform
        }.to_not change(OrderItemComponent, :count)
      end

      it 'does not create a new Order Loose Item' do
        expect {
          service.perform
        }.to_not change(OrderLooseItem, :count)
      end

      it 'returns false' do
        service.perform
        
        expect(service.success).to eq(false)
      end

      it 'returns the errors' do
        service.perform
        
        expect(service.errors).to_not be_empty
      end
    end

    context 'with valid parameters' do
      let(:parameters) { { 'estimate_id' => "#{@estimate.id}", 'user_id' => "#{@user.id}" } }
      let(:trello_integration_service) {double(:trello_integration_service, create_trello_card: true)}

      before do
        @item_1 = create(:item)
        @item_2 = create(:item)

        @price_table = create(:price_table, owner: @store, default: true, type: :for_items)
        create(:price_table_item, item: @item_1, price: '200', price_table: @price_table, maximum_discount: BigDecimal.new('50.1'))
        create(:price_table_item, item: @item_2, price: '200', price_table: @price_table, maximum_discount: BigDecimal.new('50.1'))

        @estimate_item_1 = create(:estimate_item, estimate: @estimate, item: @item_1, quantity: 2,
          factory_value: BigDecimal.new('15.22'), distributor_value: BigDecimal.new('16.88'), store_value: BigDecimal.new('17.98'), 
          customer_value: BigDecimal.new('18.98'), discount: 10)
        @estimate_item_2 = create(:estimate_item, estimate: @estimate, item: @item_2, quantity: 1,
          factory_value: BigDecimal.new('25'), distributor_value: BigDecimal.new('26.99'), store_value: BigDecimal.new('28.88'), 
          customer_value: BigDecimal.new('29.65'), discount: 12)
        @option_1.update(selected: true)
        @option_3.update(selected: true)

        @today = Date.today

        allow(Date).to receive(:today).and_return(@today)

        allow(TrelloIntegrationService).to receive(:new).and_return(trello_integration_service)
      end

      it 'creates new trello card' do
        expect(trello_integration_service).to receive(:create_trello_card)
        
        service.perform
      end

      it 'creates a new Order' do
        expect {
          service.perform
        }.to change(Order, :count).by(1)
      end

      it 'creates two new order items' do
        expect {
          service.perform
        }.to change(OrderItem, :count).by(2)
      end

      it 'creates three new order item components' do
        expect {
          service.perform
        }.to change(OrderItemComponent, :count).by(3)
      end

      it 'creates two new order loose item' do
        expect {
          service.perform
        }.to change(OrderLooseItem, :count).by(2)
      end

      it 'returns the created Order' do
        service.perform

        expect(service.record).to eq(Order.last)
      end

      it 'returns true' do
        service.perform
        
        expect(service.success).to eq(true)
      end

      it 'marks the options as selected' do
        service.perform

        expect(@option_1.reload.selected).to eq(true)
        expect(@option_2.reload.selected).to eq(false)
        expect(@option_3.reload.selected).to eq(true)
      end

      context 'when the seller is a store' do
        before do
          @estimate.update!(seller: @store)
        end

        it 'creates the order with \'not_started\' production status' do
          service.perform

          order = service.record

          expect(order.not_started?).to be_truthy
        end
      end

      context 'when the seller is a distributor' do
        before do
          @estimate.update!(seller: @distributor)
        end

        it 'creates the order with \'sent_to_factory\' production status' do
          service.perform

          order = service.record

          expect(order.sent_to_factory?).to be_truthy
        end
      end

      it 'populates the data' do
        service.perform

        order = service.record
        @estimate.reload

        order_items = order.order_items.order(option_id: :asc)

        expect(@estimate.order_id).to eq(order.id)
        expect(order.observation).to eq(@estimate.observation)
        expect(order.date).to eq(@today)
        expect(order.seller).to eq(@estimate.seller)
        expect(order.client).to eq(@estimate.client)
        expect(order.billing_client).to eq(order.client)
        expect(order.discount).to eq(@estimate.discount)
        expect(order.user_id).to eq(@user.id)
        expect(order.to_send?).to be_truthy
        expect(order.not_started?).to be_truthy
        expect(order.special_discount).to be_nil

        expect(order.packing_configurations).to_not be_empty
        expect(order.packing_configurations[0].automatic_dimension).to eq(@packing_configuration.automatic_dimension)
        expect(order.packing_configurations[0].component_dimension).to eq(@packing_configuration.component_dimension)
        expect(order.packing_configurations[0].component_name).to eq(@packing_configuration.component_name)
        expect(order.packing_configurations[0].unit).to eq(@packing_configuration.unit)

        expect(order_items[0].option_id).to eq(@option_1.id)
        expect(order_items[0].line_id).to eq(@option_1.line_id)
        expect(order_items[0].width).to eq(@option_1.width)
        expect(order_items[0].height).to eq(@option_1.height)
        expect(order_items[0].factory_total).to eq(@option_1.factory_total)
        expect(order_items[0].distributor_total).to eq(@option_1.distributor_total)
        expect(order_items[0].store_total).to eq(@option_1.store_total)
        expect(order_items[0].customer_total).to eq(@option_1.customer_total)
        expect(order_items[0].discount).to eq(@option_1.discount)

        components = order_items[0].components.order(component_name: :desc)

        expect(order_items[0].components).to_not be_empty
        expect(order_items[0].components.count).to eq(2)
        expect(components[1].component_id).to eq(@option_1.components[1].component_id)
        expect(components[1].component_name).to eq(@option_1.components[1].component_name)
        expect(components[1].component_characteristics).to eq(@option_1.components[1].component_characteristics)
        expect(components[1].category_name).to eq(@option_1.components[1].category_name)
        expect(components[1].unit).to eq(@option_1.components[1].unit)
        expect(components[1].total_consumption).to eq(@option_1.components[1].total_consumption)
        expect(components[1].width_consumption).to eq(@option_1.components[1].width_consumption)
        expect(components[1].height_consumption).to eq(@option_1.components[1].height_consumption)
        expect(components[1].quantity_consumption).to eq(@option_1.components[1].quantity_consumption)
        expect(components[1].unit_value).to eq(@option_1.components[1].unit_value)
        expect(components[1].required).to eq(@option_1.components[1].required)
        expect(components[0].component_id).to eq(@option_1.components[0].component_id)
        expect(components[0].component_name).to eq(@option_1.components[0].component_name)
        expect(components[0].component_characteristics).to eq(@option_1.components[0].component_characteristics)
        expect(components[0].category_name).to eq(@option_1.components[0].category_name)
        expect(components[0].unit).to eq(@option_1.components[0].unit)
        expect(components[0].total_consumption).to eq(@option_1.components[0].total_consumption)
        expect(components[0].width_consumption).to eq(@option_1.components[0].width_consumption)
        expect(components[0].height_consumption).to eq(@option_1.components[0].height_consumption)
        expect(components[0].quantity_consumption).to eq(@option_1.components[0].quantity_consumption)
        expect(components[0].unit_value).to eq(@option_1.components[0].unit_value)
        expect(components[0].required).to eq(@option_1.components[0].required)

        expect(order_items[1].option_id).to eq(@option_3.id)
        expect(order_items[1].line_id).to eq(@option_3.line_id)
        expect(order_items[1].width).to eq(@option_3.width)
        expect(order_items[1].height).to eq(@option_3.height)
        expect(order_items[1].components).to_not be_empty
        expect(order_items[1].components.count).to eq(1)
        expect(order_items[1].components[0].component_id).to eq(@option_3.components[0].component_id)
        expect(order_items[1].components[0].component_name).to eq(@option_3.components[0].component_name)
        expect(order_items[1].components[0].component_characteristics).to eq(@option_3.components[0].component_characteristics)
        expect(order_items[1].components[0].category_name).to eq(@option_3.components[0].category_name)
        expect(order_items[1].components[0].unit).to eq(@option_3.components[0].unit)
        expect(order_items[1].components[0].total_consumption).to eq(@option_3.components[0].total_consumption)
        expect(order_items[1].components[0].width_consumption).to eq(@option_3.components[0].width_consumption)
        expect(order_items[1].components[0].height_consumption).to eq(@option_3.components[0].height_consumption)
        expect(order_items[1].components[0].quantity_consumption).to eq(@option_3.components[0].quantity_consumption)
        expect(order_items[1].components[0].unit_value).to eq(@option_3.components[0].unit_value)
        expect(order_items[1].components[0].required).to eq(@option_3.components[0].required)

        expect(order.items[0].item_code).to eq(@estimate_item_1.item_code)
        expect(order.items[0].item_description).to eq(@estimate_item_1.item_description)
        expect(order.items[0].unit).to eq(@estimate_item_1.unit)
        expect(order.items[0].type).to eq(@estimate_item_1.type)
        expect(order.items[0].quantity).to eq(@estimate_item_1.quantity)
        expect(order.items[0].factory_value).to eq(@estimate_item_1.factory_value)
        expect(order.items[0].distributor_value).to eq(@estimate_item_1.distributor_value)
        expect(order.items[0].store_value).to eq(@estimate_item_1.store_value)
        expect(order.items[0].customer_value).to eq(@estimate_item_1.customer_value)
        expect(order.items[0].discount).to eq(@estimate_item_1.discount)

        expect(order.items[1].item_code).to eq(@estimate_item_2.item_code)
        expect(order.items[1].item_description).to eq(@estimate_item_2.item_description)
        expect(order.items[1].unit).to eq(@estimate_item_2.unit)
        expect(order.items[1].type).to eq(@estimate_item_2.type)
        expect(order.items[1].quantity).to eq(@estimate_item_2.quantity)
        expect(order.items[1].factory_value).to eq(@estimate_item_2.factory_value)
        expect(order.items[1].distributor_value).to eq(@estimate_item_2.distributor_value)
        expect(order.items[1].store_value).to eq(@estimate_item_2.store_value)
        expect(order.items[1].customer_value).to eq(@estimate_item_2.customer_value)
        expect(order.items[1].discount).to eq(@estimate_item_2.discount)
      end
    end
  end
end