require 'rails_helper'

RSpec.describe QuestionService, type: :service do
  describe '#create' do
    let(:service) { described_class.new(parameters) }

    context "with invalid parameters" do
      let(:parameters) { { "label" => "", "value" => "", 'type' => '', 'options' => '' } }

      it "does not create a new Question" do
        expect {
          service.create
        }.to_not change(Question, :count)
      end

      it "returns false" do
        service.create
        
        expect(service.success).to eq(false)
      end

      it "returns the errors" do
        service.create
        
        expect(service.errors).to_not be_empty
      end
    end

    context "with valid parameters" do
      let(:parameters) { { "label" => "Local instalação", "value" => "LOCAL_INSTALACAO", 'type' => type, 'options' => options } }
      let(:type) { "#{Question.types[:boolean]}" }
      let(:options) { nil }

      it "creates a new question" do
        expect {
          service.create
        }.to change(Question, :count).by(1)
      end

      it "returns the created question" do
        service.create

        expect(service.record).to eq(Question.last)
      end

      it "returns true" do
        service.create
        
        expect(service.success).to eq(true)
      end

      it "populates the data" do
        service.create

        question = service.record

        expect(question.label).to eq('Local instalação')
        expect(question.value).to eq('LOCAL_INSTALACAO')
        expect(Question.types[question.type]).to eq(type.to_i)
      end

      context 'for dropdown question' do
        let(:type) { "#{Question.types[:dropdown]}" }
        let(:options) { [{ 'label' => 'Foo', 'value' => 'foo' }, { 'label' => 'Bar', 'value' => 'ba' }] }

        it 'populates the options' do
          service.create

          question = service.record

          expect(question.options).to match_array(options)
        end
      end
    end
  end

  describe '#update' do
    let(:service) { described_class.new(parameters) }

    before do
      @question = create(:question, label: 'b', value: 'b', type: :boolean)
    end

    context "with invalid parameters" do
      let(:parameters) { { "label" => "", "value" => "", 'type' => '', 'options' => '' } }

      it "returns false" do
        service.update(@question.id)
        
        expect(service.success).to eq(false)
      end

      it "returns the errors" do
        service.update(@question.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context "with valid parameters" do
      let(:parameters) { { "label" => "Local instalação", "value" => "LOCAL_INSTALACAO", 'type' => type, 'options' => options } }
      let(:type) { "#{Question.types[:boolean]}" }
      let(:options) { nil }

      it "does not create a new question" do
        expect {
          service.update(@question.id)
        }.to change(Question, :count).by(0)
      end

      it "returns the question" do
        service.update(@question.id)

        expect(service.record).to eq(Question.last)
      end

      it "returns true" do
        service.update(@question.id)
        
        expect(service.success).to eq(true)
      end

      it "populates the data" do
        service.update(@question.id)

        question = service.record

        expect(question.label).to eq('Local instalação')
        expect(question.value).to eq('LOCAL_INSTALACAO')
        expect(Question.types[question.type]).to eq(type.to_i)
      end

      context 'for dropdown question' do
        let(:type) { "#{Question.types[:dropdown]}" }
        let(:options) { [{ 'label' => 'Foo', 'value' => 'foo' }, { 'label' => 'Bar', 'value' => 'ba' }] }

        it 'populates the options' do
          service.update(@question.id)

          question = service.record

          expect(question.options).to match_array(options)
        end
      end
    end
  end

  describe '#set_lines' do
    let(:service) { described_class.new(parameters) }

    before do
      @question = create(:question, label: 'b', value: 'b', type: :boolean)

      @family = create(:family, name: 'F1')

      @line1 = create(:line, family: @family, name: 'L1', horizontal_increment: 100, vertical_increment: 150, active: true)
      @line2 = create(:line, family: @family, name: 'L2', horizontal_increment: 200, vertical_increment: 250, active: false)
      @line3 = create(:line, family: @family, name: 'L3', horizontal_increment: 280, vertical_increment: 350, active: true)
    end

    context "with invalid parameters" do
      let(:parameters) { { "line_ids" => "" } }

      before do
        allow(Question).to receive(:find).and_return(@question)
        allow(@question).to receive(:save).and_return(false)
        allow(@question).to receive(:errors).and_return(double(:errors, full_messages: ['foo']))
      end

      it "returns false" do
        service.set_lines(@question.id)
        
        expect(service.success).to eq(false)
      end

      it "returns the errors" do
        service.set_lines(@question.id)
        
        expect(service.errors).to_not be_empty
      end
    end

    context "with valid parameters" do
      let(:parameters) { { 'line_ids' => [@line1.id.to_s, @line3.id.to_s] } }

      it "returns the question" do
        service.set_lines(@question.id)

        expect(service.record).to eq(Question.last)
      end

      it "returns true" do
        service.set_lines(@question.id)
        
        expect(service.success).to eq(true)
      end

      it "populates the lines" do
        service.set_lines(@question.id)

        question = service.record

        expect(question.line_ids).to eq([@line1.id, @line3.id])
      end
    end
  end
end
