require 'rails_helper'

RSpec.describe ComponentCalculatorService, type: :service do
  let(:width)  { 2500 }
  let(:height) { 5000 }

  before do
    @family = create(:family)

    @category = create(:category, name: 'C1')
    @category_2 = create(:category, name: 'C2')
    @category_3 = create(:category, name: 'C3')

    @line = create(:line, family: @family, sale_price: 85.9)

    @line.categories << build(:line_category, category: @category, required: true)
    @line.categories << build(:line_category, category: @category_3, required: false)

    @line.save!

    @component_1 = create(:component, name: 'C1', category: @category, erasable: true, cost: BigDecimal.new('32.28'), unit: :cm, fix_cost: BigDecimal.new('0.16'))
    @component_2 = create(:component, name: 'C2', category: @category_3, erasable: true, unit: :cm)
    @component_3 = create(:component, name: 'C3', category: @category_3, erasable: false, unit: :cm)
    @component_4 = create(:component, name: 'C4', category: @category_2, erasable: false, unit: :cm)

    @line_rule = create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: 500..3000, total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*3+_CUSTO_FIXO_', width_consumption: '_ALTURA_*3', height_consumption: '_LARGURA_ + 3', quantity_consumption: '_LARGURA_ + 2')
    @rule = create(:rule, line_rule: @line_rule, component: @component_1, control_method: Rule.control_methods[:width], 
      width: 500..3000, total_consumption: '_LARGURA_*2', cost: '((_CUSTO_BASE_*3+_CUSTO_FIXO_)*10)/_CONSUMO_QUANTIDADE_',
      width_consumption: '_ALTURA_*3', height_consumption: '_LARGURA_ + 3', quantity_consumption: '_LARGURA_ + 2')
  end

  describe 'validation' do
    it_should_behave_like 'a base calculator'

    it 'fails if the component category does not belong to line' do
      expect(described_class.new(@line.id, @component_4.id, width, height)).to be_invalid
    end

    it 'fails if the component is automatic' do
      @specification_1 = build(:specification, automatic: false)
      @specification_1.values << build(:value, value: 'V1')
      @specification_1.values << build(:value, value: 'V2')
      @specification_1.values << build(:value, value: 'V3')
      @specification_1.save!

      @specification_2 = build(:specification, automatic: true)
      @specification_2.values << build(:value, value: 'V4', comparison_value: 1)
      @specification_2.save!

      @component_1.values = [@specification_1.values.first, @specification_1.values.last, @specification_2.values.first]
      @component_1.save!

      expect(described_class.new(@line.id, @component_1.id, width, height)).to be_invalid
    end

    it 'fails if the component cost formula evaluates to nil' do
      @rule.reload
      @rule.quantity_consumption = ""
      @rule.cost = "_CONSUMO_QUANTIDADE_"
      @rule.save!

      expect(described_class.new(@line.id, @component_1.id, width, height)).to be_invalid
    end
  end

  describe 'informed consumption' do
    let(:service) {described_class.new(@line.id, @component_1.id, width, height)}

    before do
      @rule.total_consumption = '_INFORMADO_'
      @rule.save!
    end

    context 'with an erasable component' do
      it 'returns true when the rule has informed consumption' do
        expect(service.informed_total_consumption?).to eq(true)
      end
    end

    context 'with a non erasable component' do
      let(:service) {described_class.new(@line.id, @component_3.id, width, height)}

      it 'returns false' do
        expect(service.informed_total_consumption?).to eq(false)
      end
    end
  end

  describe 'total_consumption calculation' do
    context 'with an erasable component' do
      let(:service) { described_class.new(@line.id, @component_1.id, width, height) }

      it 'returns the component total_consumption for the given line with the given width and height' do
        expect(service.total_consumption).to eq(BigDecimal.new('500'))
      end

      context 'using user questions' do
        let(:service) { described_class.new(@line.id, @component_1.id, width, height, nil, { @question1.value => @question1.options[0]['value'], @question2.value => true }) }

        before do
          @question1 = create(:question, type: :dropdown, options: [{ 'label' => 'Foo', 'value' => 'foo' }, { 'label' => 'Bar', 'value' => 'bar' }])
          @question2 = create(:question, type: :boolean)

          @line.question_ids = [@question1.id, @question2.id]
        end

        it 'calculates correctly' do
          @rule.reload
          @rule.total_consumption = "IF(#{@question1.as_variable}='#{@question1.options[0]['value']}',IF(#{@question2.as_variable},5,4),1)"
          @rule.save!

          expect(service.total_consumption).to eq(BigDecimal.new('5'))
        end
      end

      context 'with consumption formulas' do
        it 'calculates with width consumption' do
          @rule.total_consumption = '_CONSUMO_LARGURA_*2'
          @rule.save!

          expect(service.total_consumption).to eq(BigDecimal.new('3000'))
        end

        it 'calculates with height consumption' do
          @rule.total_consumption = '_CONSUMO_ALTURA_*2'
          @rule.save!

          expect(service.total_consumption).to eq(BigDecimal.new('506'))
        end

        it 'calculates with quantity consumption' do
          @rule.total_consumption = '_CONSUMO_QUANTIDADE_*2'
          @rule.save!

          expect(service.total_consumption).to eq(BigDecimal.new('504'))
        end
      end
    end

    context 'with a non erasable component' do
      let(:service) {described_class.new(@line.id, @component_3.id, width, height)}

      it 'returns zero' do
        expect(service.total_consumption).to eq(0)
      end
    end
  end

  describe 'width_consumption calculation' do
    context 'with an erasable component' do
      let(:service) {described_class.new(@line.id, @component_1.id, width, height)}

      it 'returns the component width_consumption for the given line with the given width and height' do
        expect(service.width_consumption).to eq(BigDecimal.new('1500'))
      end

      context 'using user questions' do
        let(:service) { described_class.new(@line.id, @component_1.id, width, height, nil, { @question1.value => @question1.options[0]['value'], @question2.value => true }) }

        before do
          @question1 = create(:question, type: :dropdown, options: [{ 'label' => 'Foo', 'value' => 'foo' }, { 'label' => 'Bar', 'value' => 'bar' }])
          @question2 = create(:question, type: :boolean)

          @line.question_ids = [@question1.id, @question2.id]
        end

        it 'calculates correctly' do
          @rule.reload
          @rule.width_consumption = "IF(#{@question1.as_variable}='#{@question1.options[0]['value']}',IF(#{@question2.as_variable},12,4),1)"
          @rule.save!

          expect(service.width_consumption).to eq(BigDecimal.new('12'))
        end
      end
    end

    context 'with a non erasable component' do
      let(:service) {described_class.new(@line.id, @component_3.id, width, height)}

      it 'returns zero' do
        expect(service.width_consumption).to eq(0)
      end
    end
  end

  describe 'height_consumption calculation' do
    context 'with an erasable component' do
      let(:service) {described_class.new(@line.id, @component_1.id, width, height)}

      it 'returns the component height_consumption for the given line with the given width and height' do
        expect(service.height_consumption).to eq(BigDecimal.new('253'))
      end

      context 'using user questions' do
        let(:service) { described_class.new(@line.id, @component_1.id, width, height, nil, { @question1.value => @question1.options[0]['value'], @question2.value => true }) }

        before do
          @question1 = create(:question, type: :dropdown, options: [{ 'label' => 'Foo', 'value' => 'foo' }, { 'label' => 'Bar', 'value' => 'bar' }])
          @question2 = create(:question, type: :boolean)

          @line.question_ids = [@question1.id, @question2.id]
        end

        it 'calculates correctly' do
          @rule.reload
          @rule.height_consumption = "IF(#{@question1.as_variable}='#{@question1.options[0]['value']}',IF(#{@question2.as_variable},18,4),1)"
          @rule.save!

          expect(service.height_consumption).to eq(BigDecimal.new('18'))
        end
      end
    end

    context 'with a non erasable component' do
      let(:service) {described_class.new(@line.id, @component_3.id, width, height)}

      it 'returns zero' do
        expect(service.height_consumption).to eq(0)
      end
    end
  end

  describe 'quantity_consumption calculation' do
    context 'with an erasable component' do
      let(:service) {described_class.new(@line.id, @component_1.id, width, height)}

      it 'returns the component quantity_consumption for the given line with the given width and height' do
        expect(service.quantity_consumption).to eq(BigDecimal.new('252'))
      end

      context 'using user questions' do
        let(:service) { described_class.new(@line.id, @component_1.id, width, height, nil, { @question1.value => @question1.options[0]['value'], @question2.value => false }) }

        before do
          @question1 = create(:question, type: :dropdown, options: [{ 'label' => 'Foo', 'value' => 'foo' }, { 'label' => 'Bar', 'value' => 'bar' }])
          @question2 = create(:question, type: :boolean)

          @line.question_ids = [@question1.id, @question2.id]
        end

        it 'calculates correctly' do
          @rule.reload
          @rule.quantity_consumption = "IF(#{@question1.as_variable}='#{@question1.options[0]['value']}',IF(#{@question2.as_variable},18,17),1)"
          @rule.save!

          expect(service.quantity_consumption).to eq(BigDecimal.new('17'))
        end
      end
    end

    context 'with a non erasable component' do
      let(:service) {described_class.new(@line.id, @component_3.id, width, height)}

      it 'returns zero' do
        expect(service.quantity_consumption).to eq(0)
      end
    end
  end

  describe 'component cost' do
    let(:service) {described_class.new(@line.id, @component_1.id, width, height)}

    it 'returns the component cost for the given line with the given width and height' do
      expect(service.cost).to eq(BigDecimal.new('3.849206349206349206'))
    end

    context 'using user questions' do
      let(:service) { described_class.new(@line.id, @component_1.id, width, height, nil, { @question1.value => @question1.options[0]['value'], @question2.value => true }) }

      before do
        @question1 = create(:question, type: :dropdown, options: [{ 'label' => 'Foo', 'value' => 'foo' }, { 'label' => 'Bar', 'value' => 'bar' }])
        @question2 = create(:question, type: :boolean)

        @line.question_ids = [@question1.id, @question2.id]
      end

      it 'calculates correctly' do
        @rule.reload
        @rule.cost = "IF(#{@question1.as_variable}='#{@question1.options[0]['value']}',IF(#{@question2.as_variable},_CUSTO_BASE_*3,_CUSTO_BASE_*2),1)"
        @rule.save!

        expect(service.cost).to eq(BigDecimal.new('96.84'))
      end
    end
  end

  describe 'component price' do
    let(:service) {described_class.new(@line.id, @component_1.id, width, height)}

    it 'returns the component price by applying the line sale price over the component cost formula' do
      expect(service.price).to eq(BigDecimal.new('7.155674603174603173954'))
    end
  end

  describe 'total calculation' do
    let(:service) {described_class.new(@line.id, @component_1.id, width, height)}

    it 'returns the component total' do
      expect(service.total).to eq(BigDecimal.new('7.155674603174603173954') * BigDecimal.new('500'))
    end
  end
end
