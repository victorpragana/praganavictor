require 'rails_helper'

RSpec.describe TrelloIntegrationService, type: :service do
  let(:service) {described_class.new()}
  
  describe 'create_trello_card' do
    context 'with valid parameters' do
      let(:order)  { double(:order, id: 10, seller_type: "Store", client_type: "Store", discount: "0.0", special_discount: "0.0", created_at: Date.today) }
      let(:list)   { double(:list, id: 20) }
      let(:board)  { double(:board, name: Rails.application.secrets.trello["board"], has_lists?: false, id: 99) }
      let(:trello_member) { double(:trello_member, boards: [board]) }

      before(:each) do
        allow(Trello::Member).to receive(:find).and_return(trello_member)
        allow(Trello::List).to receive(:create).and_return(list)
        allow(Trello::Card).to receive(:create).and_return(true)
      end

      context 'trello card creation' do
        context 'with success' do
          it 'finds the trello member' do
            expect(Trello::Member).to receive(:find).with(Rails.application.secrets.trello["member"]).and_return(trello_member)

            service.create_trello_card(order)
          end

          context 'when the board does not have a list' do
            let(:list) { double(:list, name: Rails.application.secrets.trello["list"], id: 20) }

            before do
              allow(board).to receive(:has_lists?).and_return(false)
              allow(board).to receive(:lists).and_return([list])
            end

            it 'creates a trello list' do
              expect(Trello::List).to receive(:create).with({name: Rails.application.secrets.trello["list"], board_id: board.id})

              service.create_trello_card(order)
            end
          end

          it 'creates a trello card' do
            expect(Trello::Card).to receive(:create).with({name: "Pedido ##{order.id}", desc: "Vendedor: Loja\nCliente: Loja\nDesconto: #{order.discount}\nFaturamento: Loja\nDesconto especial: #{order.special_discount}\nData: #{order.created_at.strftime('%d/%m/%Y')}", list_id: list.id})

            service.create_trello_card(order)
          end
        end

        context 'with error' do
          before do
            allow(Trello::Member).to receive(:find).and_raise(Trello::Error)
          end

          it 'raises nothing' do
            expect{service.create_trello_card(order)}.to_not raise_error
          end
        end
      end
    end
  end

  describe "delete_trello_card" do
    let(:order)   {double(:order, id: 10)}
    let(:card) {double(:card, id: 10, name: "Pedido ##{order.id}")}
    let(:list)   {double(:list, id: 20)}
    let(:board)  { double(:board, name: Rails.application.secrets.trello["board"], cards: [card], has_lists?: false, id: 99)}
    let(:trello_member) {double(:trello_member, boards: [board])}

    context 'trello card deletion' do
      before(:each) do
        allow(Trello::Member).to receive(:find).and_return(trello_member)
        allow(Trello::List).to receive(:create).and_return(list)
        allow(card).to receive(:delete).and_return(true)
      end

      it 'finds the trello member' do
        expect(Trello::Member).to receive(:find).with(Rails.application.secrets.trello["member"]).and_return(trello_member)

        service.delete_trello_card(order)
      end

      it 'deletes a trello card' do
        expect(card).to receive(:delete)

        service.delete_trello_card(order)
      end
    end
  end
end