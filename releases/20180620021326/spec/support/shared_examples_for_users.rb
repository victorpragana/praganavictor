RSpec.shared_examples 'an user' do
  context 'validation' do 
    it 'expect to be valid' do
      expect(FactoryGirl.build(described_class)).to be_valid
    end
    
    describe 'email' do
      context 'expect to be invalid' do
        it 'with nil value' do
          expect(FactoryGirl.build(described_class, email: nil)).to be_invalid
        end

        it 'with empty value' do
          expect(FactoryGirl.build(described_class, email: '')).to be_invalid
        end

        it 'with duplicated values' do
          FactoryGirl.create(described_class, email: 'foo@bar.com', username: 'user1')
          expect(FactoryGirl.build(described_class, email: 'fOo@bar.com', username: 'user2')).to be_invalid
        end
      end
    end

    describe 'username' do
      context 'expect to be invalid' do
        it 'with nil value' do
          expect(FactoryGirl.build(described_class, username: nil)).to be_invalid
        end

        it 'with empty value' do
          expect(FactoryGirl.build(described_class, username: '')).to be_invalid
        end

        it 'with duplicated values' do
          FactoryGirl.create(described_class, username: 'foo_bar')

          expect(FactoryGirl.build(described_class, username: 'foO_bAr')).to be_invalid
        end
      end
    end


    describe 'name' do
      context 'expect to be invalid' do
        it 'with nil value' do
          expect(FactoryGirl.build(described_class, name: nil)).to be_invalid
        end
        
        it 'with empty value' do
          expect(FactoryGirl.build(described_class, name: '')).to be_invalid
        end
      end
    end
  end
end