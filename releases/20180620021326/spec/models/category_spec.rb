# == Schema Information
#
# Table name: categories
#
#  id            :integer          not null, primary key
#  name          :string(255)      not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  use_cut_table :boolean          default(FALSE)
#

require 'rails_helper'

RSpec.describe Category, type: :model do
  context "associations" do
    it {should have_many(:components).dependent(:delete_all)}
    it {should have_many(:line_categories).dependent(:destroy)}
  end

  context "destroying" do
    it "should block if the category has erasable components" do
      category = FactoryGirl.create(:category)
      component = FactoryGirl.create(:component, unit: :cm, :category => category, :erasable => true)

      expect {
        category.destroy
      }.to_not change(Category, :count)
    end

    it "should allow if the category has only non erasable components" do
      category = FactoryGirl.create(:category)
      component = FactoryGirl.create(:component, unit: :cm, :category => category, :erasable => false)

      expect {
        category.destroy
      }.to change(Category, :count).by(-1)
    end
  end

  context "validation" do
    it "fails if the name is not present" do
      expect(FactoryGirl.build(:category, :name => nil)).to be_invalid
    end

    it "fails if the name is blank" do
      expect(FactoryGirl.build(:category, :name => nil)).to be_invalid
    end

    it "fails if the name length is greater than 255 characters" do
      expect(FactoryGirl.build(:category, :name => Faker::Lorem.characters(256))).to be_invalid
    end

    it "fails if the name is not unique" do
      FactoryGirl.create(:category, :name => "abc")

      expect(FactoryGirl.build(:category, :name => "AbC")).to be_invalid
    end

    it "shoud pass" do
      expect(FactoryGirl.build(:category)).to be_valid
    end
  end

  context "after creation" do
    it "should create a new non erasable component" do
      expect {
        FactoryGirl.create(:category)
      }.to change(Component, :count).by(1)
    end

    it "should create a new non erasable component for the category" do
      category = FactoryGirl.create(:category)

      component = Component.last

      expect(component.name).to eq(I18n.t("helpers.none_selected"))
      expect(component.cost).to eq(0)
      expect(component.erasable).to eq(false)
      expect(component.category_id).to eq(category.id)
    end
  end

  describe ".order_for_line" do
    before(:each) do
      @category = FactoryGirl.create(:category)
      @family = FactoryGirl.create(:family)
      @line = FactoryGirl.create(:line, :family => @family)
    end

    context "when category is not present for the line" do
      it "should return nil" do
        expect(@category.order_for_line(@line.id)).to be_nil
      end
    end

    context "when category is present on the line" do
      before(:each) do
        @line.categories << FactoryGirl.build(:line_category, :category => @category, :required => true, :order => 2)

        @line.save!
      end

      it "should return the category order" do
        expect(@category.order_for_line(@line.id)).to eq(2)
      end
    end
  end
end
