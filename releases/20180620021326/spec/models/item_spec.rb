# == Schema Information
#
# Table name: items
#
#  id             :integer          not null, primary key
#  code           :string
#  description    :string           not null
#  unit           :integer          not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  deleted_at     :datetime
#  type           :integer          default(0)
#  erp_categories :json
#  ncm            :string(8)
#  cst            :integer
#

require 'rails_helper'

RSpec.describe Item, type: :model do
  describe 'enums' do
    it { should define_enum_for(:unit).with({
      m:  0, 
      mm: 1, 
      un: 2, 
      m2: 3,
      cm: 4,
      km: 5,
      pleat: 6
    }) }

    it { should define_enum_for(:type).with({
      product: 0, 
      service: 1
    }) }

    it { should define_enum_for(:cst).with({
      national:               0, 
      direct_import:          1, 
      foreign_im:             2, 
      national_with_ic_gt_40: 3,
      national_other:         4,
      national_with_ic_lt_40: 5,
      foreign_di_camex:       6,
      foreign_im_camex:       7
    }) }
  end

  describe 'validations' do
    it 'fails if the description is not present' do
      expect(FactoryGirl.build(:item, description: nil)).to be_invalid
    end

    it 'fails if the description is blank' do
      expect(FactoryGirl.build(:item, description: '')).to be_invalid
    end

    it 'fails if the unit is not present' do
      expect(FactoryGirl.build(:item, unit: nil)).to be_invalid
    end

    it 'fails if the unit is blank' do
      expect(FactoryGirl.build(:item, unit: '')).to be_invalid
    end

    it 'fails if the type is not present' do
      expect(FactoryGirl.build(:item, type: nil)).to be_invalid
    end

    it 'fails if the type is blank' do
      expect(FactoryGirl.build(:item, type: '')).to be_invalid
    end

    it 'fails if the ncm does not have only numbers' do
      expect(FactoryGirl.build(:item, ncm: '2222222A')).to be_invalid
    end

    it 'fails if the ncm has less than 8 digits' do
      expect(FactoryGirl.build(:item, ncm: '2222222')).to be_invalid
    end

    it 'fails if the ncm has more than 8 digits' do
      expect(FactoryGirl.build(:item, ncm: '888569899')).to be_invalid
    end

    it 'passes' do
      expect(FactoryGirl.build(:item)).to be_valid
    end

    it 'passes' do
      expect(FactoryGirl.build(:item, ncm: '58943657')).to be_valid
    end
  end
end
