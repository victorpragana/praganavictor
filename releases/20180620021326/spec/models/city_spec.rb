# == Schema Information
#
# Table name: cities
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  state_id   :integer
#  ibge_code  :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe City, type: :model do
  describe "associations" do
    it { should belong_to(:state) }
  end

  context "validation" do
    it "should invalidate if name is not present" do
      expect(FactoryGirl.build(:city, name: nil)).to be_invalid
    end

    it "should invalidate if name is blank" do
      expect(FactoryGirl.build(:city, name: "")).to be_invalid
    end

    it "should invalidate if state_id is not present" do
      expect(FactoryGirl.build(:city, state_id: nil)).to be_invalid
    end

    it "should invalidate if state_id is blank" do
      expect(FactoryGirl.build(:city, state_id: "",)).to be_invalid
    end

    it "should invalidate if ibge_code is not present" do
      expect(FactoryGirl.build(:city, ibge_code: nil)).to be_invalid
    end

    it "should invalidate if ibge_code is blank" do
      expect(FactoryGirl.build(:city, ibge_code: "")).to be_invalid
    end
  end
end
