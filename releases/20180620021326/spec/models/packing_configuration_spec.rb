# == Schema Information
#
# Table name: packing_configurations
#
#  id                   :integer          not null, primary key
#  packable_id          :integer
#  packable_type        :string
#  items                :integer          is an Array
#  automatic_dimension  :decimal(10, 5)   not null
#  component_dimension  :decimal(10, 5)   not null
#  component_name       :string           not null
#  unit                 :integer          not null
#  packing_file_name    :string
#  packing_content_type :string
#  packing_file_size    :integer
#  packing_updated_at   :datetime
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

require 'rails_helper'

RSpec.describe PackingConfiguration, type: :model do
  describe 'associations' do
    it { is_expected.to belong_to(:packable) }
  end

  describe 'enums' do
    it { is_expected.to define_enum_for(:unit).with({
      m:     0, 
      mm:    1, 
      un:    2, 
      m2:    3,
      cm:    4,
      km:    5,
      pleat: 6
    }) }
  end

  before(:each) do
    @category    = FactoryGirl.create(:category)
    @family      = FactoryGirl.create(:family)
    @component   = FactoryGirl.create(:component, unit: :cm, category: @category)
    @distributor = FactoryGirl.create(:distributor, name: 'D1')
    @store       = FactoryGirl.create(:store, distributor: @distributor, name: 'S1')
    @customer    = FactoryGirl.create(:customer, store_id: @store.id, name: 'C1')
    @estimate    = FactoryGirl.create(:estimate, seller: @store, client: @customer)
  end

  describe 'validation' do
    it 'fails if the packable is not present' do
      expect(FactoryGirl.build(:packing_configuration, packable: nil)).to be_invalid
    end

    it 'fails if the component name is not present' do
      expect(FactoryGirl.build(:packing_configuration, packable: @estimate, component_name: nil)).to be_invalid
    end

    it 'fails if the component name is blank' do
      expect(FactoryGirl.build(:packing_configuration, packable: @estimate, component_name: '')).to be_invalid
    end

    it 'fails if the unit is not present' do
      expect(FactoryGirl.build(:packing_configuration, packable: @estimate, unit: nil)).to be_invalid
    end

    it 'fails if the unit is blank' do
      expect(FactoryGirl.build(:packing_configuration, packable: @estimate, unit: '')).to be_invalid
    end

    it 'fails if the automatic_dimension is not present' do
      expect(FactoryGirl.build(:packing_configuration, packable: @estimate, automatic_dimension: nil)).to be_invalid
    end

    it 'fails if the automatic_dimension is blank' do
      expect(FactoryGirl.build(:packing_configuration, packable: @estimate, automatic_dimension: '')).to be_invalid
    end

    it 'fails if the automatic_dimension is negative' do
      expect(FactoryGirl.build(:packing_configuration, packable: @estimate, automatic_dimension: -1)).to be_invalid
    end

    it 'fails if the automatic_dimension is zero' do
      expect(FactoryGirl.build(:packing_configuration, packable: @estimate, automatic_dimension: 0)).to be_invalid
    end

    it 'fails if the component_dimension is not present' do
      expect(FactoryGirl.build(:packing_configuration, packable: @estimate, component_dimension: nil)).to be_invalid
    end

    it 'fails if the component_dimension is blank' do
      expect(FactoryGirl.build(:packing_configuration, packable: @estimate, component_dimension: '')).to be_invalid
    end

    it 'fails if the component_dimension is negative' do
      expect(FactoryGirl.build(:packing_configuration, packable: @estimate, component_dimension: -1)).to be_invalid
    end

    it 'fails if the component_dimension is zero' do
      expect(FactoryGirl.build(:packing_configuration, packable: @estimate, component_dimension: 0)).to be_invalid
    end

    it 'passes' do
      expect(FactoryGirl.build(:packing_configuration, packable: @estimate)).to be_valid
    end
  end
end
