# == Schema Information
#
# Table name: commercial_classifications
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe CommercialClassification, type: :model do
  describe 'associations' do
    it { is_expected.to have_and_belong_to_many(:lines) }
    it { is_expected.to have_many(:distributors) }
    it { is_expected.to have_many(:stores) }
  end

  describe 'validations' do
    it 'fails if the name is not present' do
      expect(build(:commercial_classification, name: nil)).to be_invalid
    end

    it 'fails if the name is blank' do
      expect(build(:commercial_classification, name: '')).to be_invalid
    end

    it 'fails if the name is not unique' do
      create(:commercial_classification, name: 'foo')

      expect(build(:commercial_classification, name: 'Foo')).to be_invalid
    end

    it 'passes' do
      expect(build(:commercial_classification)).to be_valid
    end
  end
end
