# == Schema Information
#
# Table name: values
#
#  id               :integer          not null, primary key
#  value            :string           not null
#  specification_id :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  deleted_at       :datetime
#  comparison_value :integer
#

require 'rails_helper'

RSpec.describe Value, type: :model do
  before(:each) do
    @specification = FactoryGirl.create(:specification)
  end

  describe "associations" do
    it { should belong_to(:specification) }
    it { should have_and_belong_to_many(:components) }
    it { should have_many(:base_component_values) }
    it { should have_many(:base_components).through(:base_component_values) }
    it { should delegate_method(:automatic?).to(:specification) }
  end

  describe "conversions" do
    it { expect(described_class.new).to respond_to(:comparison_value_from_mm_to_cm) }
  end

  context "validation" do
    it "fails if the value is not present" do
      expect(FactoryGirl.build(:value, value: nil, specification: @specification)).to be_invalid
    end

    it "fails if the value is blank" do
      expect(FactoryGirl.build(:value, value: "", specification: @specification)).to be_invalid
    end

    it "fails if the specification is not present" do
      expect(FactoryGirl.build(:value, specification: nil)).to be_invalid
    end

    it "fails if there is already a value with the given value for the same specification" do
      FactoryGirl.create(:value, value: "abC", specification: @specification)

      expect(FactoryGirl.build(:value, value: "aBC", specification: @specification)).to be_invalid
    end

    context "for automatic specification" do
      before(:each) do
        @specification = FactoryGirl.create(:specification, automatic: true)
      end

      it "fails if the comparison value is not present" do
        expect(FactoryGirl.build(:value, value: "aBC", specification: @specification, comparison_value: nil)).to be_invalid
      end

      it "fails if the comparison value is blank" do
        expect(FactoryGirl.build(:value, value: "aBC", specification: @specification, comparison_value: "")).to be_invalid
      end

      it "fails if the comparison value is not an integer" do
        expect(FactoryGirl.build(:value, value: "aBC", specification: @specification, comparison_value: BigDecimal.new("-0.01"))).to be_invalid
      end

      it "fails if the comparison value is negative" do
        expect(FactoryGirl.build(:value, value: "aBC", specification: @specification, comparison_value: -1)).to be_invalid
      end

      it "fails if the comparison value is zero" do
        expect(FactoryGirl.build(:value, value: "aBC", specification: @specification, comparison_value: 0)).to be_invalid
      end
    end

    it "should pass" do
      expect(FactoryGirl.build(:value, specification: @specification)).to be_valid
    end

    it "should pass even if there is already a value with the given value for the other specification" do
      specification = FactoryGirl.create(:specification, name: "Foo")
      FactoryGirl.create(:value, value: "abc", specification: specification)

      expect(FactoryGirl.build(:value, value: "abc", specification: @specification)).to be_valid
    end
  end

  context 'updating value' do
    before do
      @family   = FactoryGirl.create(:family, name: 'F1')
      @category = FactoryGirl.create(:category)

      @specification_1  = FactoryGirl.create(:specification, name: 'A1')
      @specification_2  = FactoryGirl.create(:specification, name: 'A2')
      @specification_3  = FactoryGirl.create(:specification, name: 'A3')

      @value_1_1        = FactoryGirl.create(:value, value: 'V11', specification: @specification_1)
      @value_1_2        = FactoryGirl.create(:value, value: 'V12', specification: @specification_1)
      @value_1_3        = FactoryGirl.create(:value, value: 'V13', specification: @specification_1)

      @value_2_1        = FactoryGirl.create(:value, value: 'V21', specification: @specification_2)
      @value_2_2        = FactoryGirl.create(:value, value: 'V22', specification: @specification_2)
      @value_2_3        = FactoryGirl.create(:value, value: 'V23', specification: @specification_2)

      @value_3_1        = FactoryGirl.create(:value, value: 'V31', specification: @specification_3)
      @value_3_2        = FactoryGirl.create(:value, value: 'V32', specification: @specification_3)
      @value_3_3        = FactoryGirl.create(:value, value: 'V33', specification: @specification_3)

      service = BaseComponentService.new({'name' => 'Manual Mola', 'unit'=>"#{BaseComponent.units[:un]}", 'category_id' => "#{@category.id}", 'cost' => '2.54',
        'fix_cost' => '5.54', 'family_ids' => ["#{@family.id}"], 'code' => 'A12', 'reference_cost' => '1.54', 'exchange' => '2.59', 'create_children' => 'true',
        'values' => [
          {'id' => "#{@value_1_3.id}", 'children' => [{'id' => "#{@value_2_1.id}"}, {'id' => "#{@value_2_3.id}", 'children' => [{'id' => "#{@value_3_1.id}"}, {'id' => "#{@value_3_2.id}"}, {'id' => "#{@value_3_3.id}"}]}]}
        ], 'loss' => '1.22'})

      service.create

      @base_component_1 = service.record

      components = @base_component_1.components.order(created_at: :asc)

      @component_1 = components[0]
      @component_2 = components[1]
      @component_3 = components[2]
      @component_4 = components[3]

      service = BaseComponentService.new({'name' => 'Manual Mola V2', 'unit'=>"#{BaseComponent.units[:un]}", 'category_id' => "#{@category.id}", 'cost' => '2.54',
        'fix_cost' => '5.54', 'family_ids' => ["#{@family.id}"], 'code' => 'A12', 'reference_cost' => '1.54', 'exchange' => '2.59', 'create_children' => 'true',
        'values' => [
          {'id' => "#{@value_2_1.id}"}, {'id' => "#{@value_2_3.id}", 'children' => [{'id' => "#{@value_3_1.id}"}, {'id' => "#{@value_3_2.id}"}, {'id' => "#{@value_3_3.id}"}]}
        ], 'loss' => '1.22'})

      service.create

      @base_component_2 = service.record

      components = @base_component_2.components.order(created_at: :asc)

      @component_5 = components[0]
      @component_6 = components[1]
      @component_7 = components[2]
      @component_8 = components[3]
    end

    it 'updates the component names that use the update value' do
      @value_2_3.update(value: 'Foo')

      expect(@component_1.reload.name).to eq("V13 V21")
      expect(@component_2.reload.name).to eq("V13 Foo V31")
      expect(@component_3.reload.name).to eq("V13 Foo V32")
      expect(@component_4.reload.name).to eq("V13 Foo V33")
      expect(@component_5.reload.name).to eq("V21")
      expect(@component_6.reload.name).to eq("Foo V31")
      expect(@component_7.reload.name).to eq("Foo V32")
      expect(@component_8.reload.name).to eq("Foo V33")
    end
  end
end