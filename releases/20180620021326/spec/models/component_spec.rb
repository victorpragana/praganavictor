# == Schema Information
#
# Table name: components
#
#  id                        :integer          not null, primary key
#  name                      :string(255)      not null
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  cost                      :decimal(10, 5)   default(0.0), not null
#  erasable                  :boolean          default(TRUE)
#  category_id               :integer
#  characteristics           :text
#  code                      :string(20)
#  unit                      :integer          default(2), not null
#  deleted_at                :datetime
#  base_component_id         :integer
#  reference_cost            :decimal(10, 5)   default(0.0)
#  offset_left               :integer          default(0)
#  offset_top                :integer          default(0)
#  offset_right              :integer          default(0)
#  offset_bottom             :integer          default(0)
#  allow_rotation            :boolean          default(FALSE)
#  exchange                  :decimal(10, 5)
#  loss                      :decimal(10, 5)
#  cut_table_id              :integer
#  print_on_lines_label      :boolean          default(FALSE)
#  fix_cost                  :decimal(10, 5)   default(0.0)
#  print_on_components_label :boolean          default(FALSE)
#

require 'rails_helper'

RSpec.describe Component, type: :model do
  before(:each) do
    @category = FactoryGirl.create(:category)
  end

  context 'associations' do
    it { should have_and_belong_to_many(:families) }
    it { should have_and_belong_to_many(:values) }
    it { should have_many(:rules) }
    it { should have_many(:line_rules).through(:rules) }
    it { should have_many(:lines).through(:line_rules) }
    it { should belong_to(:category) }
    it { should belong_to(:base).class_name('BaseComponent') }
    it { should belong_to(:cut_table) }
  end

  describe 'conversions' do
    it { expect(described_class.new).to respond_to(:offset_left_from_mm_to_cm) }
    it { expect(described_class.new).to respond_to(:offset_top_from_mm_to_cm) }
    it { expect(described_class.new).to respond_to(:offset_right_from_mm_to_cm) }
    it { expect(described_class.new).to respond_to(:offset_bottom_from_mm_to_cm) }
  end

  context 'validation' do
    it 'fails if the name is not present' do
      expect(FactoryGirl.build(:component, name: nil, category: @category)).to be_invalid
    end

    it 'fails if the name is blank' do
      expect(FactoryGirl.build(:component, name: '', category: @category)).to be_invalid
    end

    it 'fails if the name length is greater than 255 characters' do
      expect(FactoryGirl.build(:component, name: Faker::Lorem.characters(256), category: @category)).to be_invalid
    end

    it 'fails if the name is not unique for the same category' do
      FactoryGirl.create(:component, name: 'abc', category: @category)

      expect(FactoryGirl.build(:component, name: 'AbC', category: @category)).to be_invalid
    end

    it 'fails if category is not present' do
      expect(FactoryGirl.build(:component, category: nil)).to be_invalid
    end

    it 'fails if the cost is not present' do
      expect(FactoryGirl.build(:component, cost: nil, category: @category)).to be_invalid
    end

    it 'fails if the cost is blank' do
      expect(FactoryGirl.build(:component, cost: '', category: @category)).to be_invalid
    end

    it 'fails if the cost is negative' do
      expect(FactoryGirl.build(:component, cost: BigDecimal.new('-0.01'), category: @category)).to be_invalid
    end

    it 'fails if the reference cost is negative' do
      expect(FactoryGirl.build(:component, reference_cost: BigDecimal.new('-0.01'), category: @category)).to be_invalid
    end

    it 'fails if the fix cost is negative' do
      expect(FactoryGirl.build(:component, fix_cost: BigDecimal.new('-0.01'), category: @category)).to be_invalid
    end

    it 'fails if the code length is greater than 20 characters' do
      expect(FactoryGirl.build(:component, code: Faker::Lorem.characters(21), category: @category)).to be_invalid
    end

    it 'fails if the unit is not present' do
      expect(FactoryGirl.build(:component, unit: nil, category: @category)).to be_invalid
    end

    it 'fails if the unit is blank' do
      expect(FactoryGirl.build(:component, unit: '', category: @category)).to be_invalid
    end

    it 'fails if the offset_left is negative' do
      expect(FactoryGirl.build(:component, offset_left: -1, category: @category)).to be_invalid
    end

    it 'fails if the offset_left is not an integer' do
      expect(FactoryGirl.build(:component, offset_left: BigDecimal.new('0.1'), category: @category)).to be_invalid
    end

    it 'fails if the offset_top is negative' do
      expect(FactoryGirl.build(:component, offset_top: -1, category: @category)).to be_invalid
    end

    it 'fails if the offset_top is not an integer' do
      expect(FactoryGirl.build(:component, offset_top: BigDecimal.new('0.1'), category: @category)).to be_invalid
    end

    it 'fails if the offset_right is negative' do
      expect(FactoryGirl.build(:component, offset_right: -1, category: @category)).to be_invalid
    end

    it 'fails if the offset_right is not an integer' do
      expect(FactoryGirl.build(:component, offset_right: BigDecimal.new('0.1'), category: @category)).to be_invalid
    end

    it 'fails if the offset_bottom is negative' do
      expect(FactoryGirl.build(:component, offset_bottom: -1, category: @category)).to be_invalid
    end

    it 'fails if the offset_bottom is not an integer' do
      expect(FactoryGirl.build(:component, offset_bottom: BigDecimal.new('0.1'), category: @category)).to be_invalid
    end

    context 'with cut table' do
      before do
        @category = FactoryGirl.create(:category, use_cut_table: true)

        @cut_table_1 = FactoryGirl.create(:cut_table)
        @cut_table_2 = FactoryGirl.create(:cut_table)

        @base_component = FactoryGirl.create(:base_component, category: @category, cut_table: @cut_table_1)

        FactoryGirl.create(:component, category: @category, cut_table: @cut_table_1, base: @base_component)
      end

      it 'fails if the cut table is different from the siblings cut table' do
        expect(FactoryGirl.build(:component, category: @category, cut_table: @cut_table_2, base: @base_component)).to be_invalid
      end

      it 'fails if it has cut table but the category does not allow the usage' do
        @category.update!(use_cut_table: false)

        expect(FactoryGirl.build(:component, category: @category, cut_table: @cut_table_1)).to be_invalid
      end

      it 'fails if it the category uses a cut table but it is not present' do
        expect(FactoryGirl.build(:component, category: @category, cut_table: nil)).to be_invalid
      end
    end

    it 'passes' do
      expect(FactoryGirl.build(:component, category: @category)).to be_valid
    end

    it 'passes if the name is not unique for the different category' do
      FactoryGirl.create(:component, name: 'abc', category: @category)

      expect(FactoryGirl.build(:component, name: 'AbC', category: FactoryGirl.create(:category))).to be_valid
    end

    it 'passes if the code length is 0' do
      expect(FactoryGirl.build(:component, code: nil, category: @category)).to be_valid
    end
  end

  context 'final cost' do
    context 'when exchange is nil' do
      it 'calculates the final cost not using the exchange data' do
        expect(FactoryGirl.build(:component, cost: BigDecimal.new('12.58')).final_cost).to eq(BigDecimal.new('12.58'))
      end

      it 'calculates the final cost using the given cost and not using the exchange data' do
        expect(FactoryGirl.build(:component, cost: BigDecimal.new('12.58')).final_cost(BigDecimal.new('18.58'))).to eq(BigDecimal.new('18.58'))
      end
    end

    context 'when exchange is zero' do
      it 'calculates the final cost not using the exchange data' do
        expect(FactoryGirl.build(:component, cost: BigDecimal.new('12.58'), exchange: 0).final_cost).to eq(BigDecimal.new('12.58'))
      end

      it 'calculates the final cost using the given cost and not using the exchange data' do
        expect(FactoryGirl.build(:component, cost: BigDecimal.new('12.58'), exchange: 0).final_cost(BigDecimal.new('18.58'))).to eq(BigDecimal.new('18.58'))
      end
    end

    context 'when exchange is present' do
      it 'calculates the final cost using the exchange data' do
        expect(FactoryGirl.build(:component, cost: BigDecimal.new('12.58'), exchange: BigDecimal.new('2.58')).final_cost).to eq(BigDecimal.new('32.4564'))
      end

      it 'calculates the final cost using the given cost and using the exchange data' do
        expect(FactoryGirl.build(:component, cost: BigDecimal.new('12.58'), exchange: BigDecimal.new('2.58')).final_cost(BigDecimal.new('18.58'))).to eq(BigDecimal.new('47.9364'))
      end
    end

    context 'when loss is nil' do
      it 'calculates the final cost not using the loss data' do
        expect(FactoryGirl.build(:component, cost: BigDecimal.new('12.58')).final_cost).to eq(BigDecimal.new('12.58'))
      end

      it 'calculates the final cost using the given cost and not using the loss data' do
        expect(FactoryGirl.build(:component, cost: BigDecimal.new('12.58')).final_cost(BigDecimal.new('18.58'))).to eq(BigDecimal.new('18.58'))
      end
    end

    context 'when loss is zero' do
      it 'calculates the final cost not using the loss data' do
        expect(FactoryGirl.build(:component, cost: BigDecimal.new('12.58'), loss: 0).final_cost).to eq(BigDecimal.new('12.58'))
      end

      it 'calculates the final cost using the given cost and not using the loss data' do
        expect(FactoryGirl.build(:component, cost: BigDecimal.new('12.58'), loss: 0).final_cost(BigDecimal.new('18.58'))).to eq(BigDecimal.new('18.58'))
      end
    end

    context 'when loss is present' do
      it 'calculates the final cost using the loss data' do
        expect(FactoryGirl.build(:component, cost: BigDecimal.new('12.58'), loss: BigDecimal.new('2.58')).final_cost).to eq(BigDecimal.new('32.4564'))
      end

      it 'calculates the final cost using the given cost and using the loss data' do
        expect(FactoryGirl.build(:component, cost: BigDecimal.new('12.58'), loss: BigDecimal.new('2.58')).final_cost(BigDecimal.new('18.58'))).to eq(BigDecimal.new('47.9364'))
      end
    end
  end

  context 'final fix cost' do
    context 'when exchange is nil' do
      it 'calculates the final fix cost not using the exchange data' do
        expect(FactoryGirl.build(:component, fix_cost: BigDecimal.new('12.58')).final_fix_cost).to eq(BigDecimal.new('12.58'))
      end
    end

    context 'when exchange is zero' do
      it 'calculates the final fix cost not using the exchange data' do
        expect(FactoryGirl.build(:component, fix_cost: BigDecimal.new('12.58'), exchange: 0).final_fix_cost).to eq(BigDecimal.new('12.58'))
      end
    end

    context 'when exchange is present' do
      it 'calculates the final fix cost using the exchange data' do
        expect(FactoryGirl.build(:component, fix_cost: BigDecimal.new('12.58'), exchange: BigDecimal.new('2.58')).final_fix_cost).to eq(BigDecimal.new('32.4564'))
      end
    end

    context 'when loss is nil' do
      it 'calculates the final fix cost not using the loss data' do
        expect(FactoryGirl.build(:component, fix_cost: BigDecimal.new('12.58')).final_fix_cost).to eq(BigDecimal.new('12.58'))
      end
    end

    context 'when loss is zero' do
      it 'calculates the final fix cost not using the loss data' do
        expect(FactoryGirl.build(:component, fix_cost: BigDecimal.new('12.58'), loss: 0).final_fix_cost).to eq(BigDecimal.new('12.58'))
      end
    end

    context 'when loss is present' do
      it 'calculates the final fix cost using the loss data' do
        expect(FactoryGirl.build(:component, fix_cost: BigDecimal.new('12.58'), loss: BigDecimal.new('2.58')).final_fix_cost).to eq(BigDecimal.new('32.4564'))
      end
    end
  end

  context '.automatic?' do
    before(:each) do
      @specification_1 = FactoryGirl.build(:specification, automatic: false)
      @specification_1.values << FactoryGirl.build(:value, value: 'V1')
      @specification_1.values << FactoryGirl.build(:value, value: 'V2')
      @specification_1.values << FactoryGirl.build(:value, value: 'V3')
      @specification_1.save!

      @specification_2 = FactoryGirl.build(:specification, automatic: true)
      @specification_2.values << FactoryGirl.build(:value, value: 'V4', comparison_value: 1)
      @specification_2.save!
    end

    it 'returns true if the component has a value of an automatic specification' do
      expect(FactoryGirl.create(:component, values: [@specification_1.values.first, @specification_2.values.first], category: @category).automatic?).to eq(true)
    end
  end

  context '.automatic_value' do
    before(:each) do
      @specification_1 = FactoryGirl.build(:specification, automatic: false)
      @specification_1.values << FactoryGirl.build(:value, value: 'V1')
      @specification_1.values << FactoryGirl.build(:value, value: 'V2')
      @specification_1.values << FactoryGirl.build(:value, value: 'V3')
      @specification_1.save!

      @specification_2 = FactoryGirl.build(:specification, automatic: true)
      @specification_2.values << FactoryGirl.build(:value, value: 'V4', comparison_value: 1)
      @specification_2.save!
    end

    it 'returns the comparison value if the component is automatic' do
      expect(FactoryGirl.create(:component, values: [@specification_1.values.first, @specification_2.values.first], category: @category).automatic_value).to eq(@specification_2.values.first.comparison_value)
    end

    it 'returns zero if the component is not automatic' do
      expect(FactoryGirl.create(:component, category: @category).automatic_value).to eq(0)
    end
  end

  context '.name_without_automatic_value' do
    before(:each) do
      @specification_1 = FactoryGirl.build(:specification, automatic: false)
      @specification_1.values << FactoryGirl.build(:value, value: 'V1')
      @specification_1.values << FactoryGirl.build(:value, value: 'V2')
      @specification_1.values << FactoryGirl.build(:value, value: 'V3')
      @specification_1.save!

      @specification_2 = FactoryGirl.build(:specification, automatic: true)
      @specification_2.values << FactoryGirl.build(:value, value: 'V4', comparison_value: 1)
      @specification_2.save!
    end

    it 'returns the component name without the automatic attribute' do
      base_component = FactoryGirl.create(:base_component, category: @category, name: 'BASE')
      component = FactoryGirl.create(:component, base: base_component, values: [@specification_1.values.first, @specification_1.values.last, @specification_2.values.first], category: @category)
      expect(component.name_without_automatic_value).to eq('V1 V3')
    end

    it 'returns the component name if it is not automatic' do
      component = FactoryGirl.create(:component, category: @category)
      expect(component.name_without_automatic_value).to eq(component.name)
    end

    it 'returns the component name if it does not belongs to a base component' do
      component = FactoryGirl.create(:component, category: @category, base_component_id: nil)
      expect(component.name_without_automatic_value).to eq(component.name)
    end
  end

  describe 'after_update' do
    context 'when the category has changed' do
      before do
        @category   = FactoryGirl.create(:category, name: 'C1')
        @category_2 = FactoryGirl.create(:category, name: 'C2')

        @component = FactoryGirl.create(:component, category: @category)

        @family    = FactoryGirl.create(:family)
        @line      = FactoryGirl.create(:line, family: @family)
      end

      it 'keeps the rules for the component that dont have category' do
        line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:unit], height: 1..2, width: 2..3, category: nil)
        rule_1 = FactoryGirl.create(:rule, line_rule: line_rule, component: @component)

        @component.update(category: @category_2)

        expect(Rule.find_by_id(rule_1.id)).to_not be_nil
      end

      it 'removes the rules for the component that have category and it is from the old category' do
        line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:unit], height: 1..2, width: 2..3, category: @category)
        rule_1 = FactoryGirl.create(:rule, line_rule: line_rule, component: @component)

        @component.update(category: @category_2)

        expect(Rule.find_by_id(rule_1.id)).to be_nil
      end
    end
  end
end
