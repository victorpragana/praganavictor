# == Schema Information
#
# Table name: order_loose_items
#
#  id                :integer          not null, primary key
#  order_id          :integer
#  item_code         :string
#  item_description  :string           not null
#  unit              :integer          not null
#  quantity          :decimal(8, 2)    not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  type              :integer          default(0)
#  factory_value     :decimal(10, 2)   default(0.0)
#  distributor_value :decimal(10, 2)   default(0.0)
#  store_value       :decimal(10, 2)   default(0.0)
#  customer_value    :decimal(10, 2)   default(0.0)
#  discount          :decimal(5, 2)    default(0.0)
#  item_id           :integer
#

require 'rails_helper'

RSpec.describe OrderLooseItem, type: :model do  
  before(:each) do
    @distributor = FactoryGirl.create(:distributor, name: 'D1')
    @store       = FactoryGirl.create(:store, distributor: @distributor, name: 'S1')
    @customer    = FactoryGirl.create(:customer, store_id: @store.id, name: 'C1')
    @user        = FactoryGirl.create(:administrator, username: 'F1').tap do |administrator|
      administrator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @store, user: administrator))
    end
    @order       = FactoryGirl.create(:order, environments: [], seller: @store, client: @customer, billing_client: @customer, user: @user)
    @item        = FactoryGirl.create(:item)
  end

  describe 'associations' do
    it { should belong_to(:order) }
    it { should belong_to(:item) }
  end

  describe 'enums' do
    it { should define_enum_for(:type).with({
      product: 0, 
      service: 1
    }) }
  end

  describe 'validations' do
    it 'fails if the order is not present' do
      expect(FactoryGirl.build(:order_loose_item, order: nil, item: @item)).to be_invalid
    end

    it 'fails if the item is not present' do
      expect(FactoryGirl.build(:order_loose_item, order: @order, item: nil)).to be_invalid
    end

    it 'fails if the item description is not present' do
      expect(FactoryGirl.build(:order_loose_item, order: @order, item: @item, item_description: nil)).to be_invalid
    end

    it 'fails if the unit is not present' do
      expect(FactoryGirl.build(:order_loose_item, order: @order, item: @item, unit: nil)).to be_invalid
    end

    it 'fails if the quantity is not present' do
      expect(FactoryGirl.build(:order_loose_item, order: @order, item: @item, quantity: nil)).to be_invalid
    end

    it 'fails if the quantity is negative' do
      expect(FactoryGirl.build(:order_loose_item, order: @order, item: @item, quantity: BigDecimal.new('-0.01'))).to be_invalid
    end

    it 'fails if the quantity is zero' do
      expect(FactoryGirl.build(:order_loose_item, order: @order, item: @item, quantity: 0)).to be_invalid
    end

    it 'fails if the factory value is negative' do
      expect(FactoryGirl.build(:order_loose_item, order: @order, item: @item, factory_value: BigDecimal.new('-0.01'))).to be_invalid
    end

    it 'fails if the distributor value is negative' do
      expect(FactoryGirl.build(:order_loose_item, order: @order, item: @item, distributor_value: BigDecimal.new('-0.01'))).to be_invalid
    end

    it 'fails if the store value is negative' do
      expect(FactoryGirl.build(:order_loose_item, order: @order, item: @item, store_value: BigDecimal.new('-0.01'))).to be_invalid
    end

    it 'fails if the customer value is negative' do
      expect(FactoryGirl.build(:order_loose_item, order: @order, item: @item, customer_value: BigDecimal.new('-0.01'))).to be_invalid
    end

    it 'fails if the type is not present' do
      expect(FactoryGirl.build(:order_loose_item, order: @order, item: @item, type: nil)).to be_invalid
    end

    it 'fails if the discount is negative' do
      expect(FactoryGirl.build(:order_loose_item, order: @order, item: @item, discount: -1)).to be_invalid
    end

    it 'fails if the discount is greater than 100' do
      expect(FactoryGirl.build(:order_loose_item, order: @order, item: @item, discount: BigDecimal.new('100.1'))).to be_invalid
    end

    it 'passes' do
      expect(FactoryGirl.build(:order_loose_item, order: @order, item: @item)).to be_valid
    end
  end

  describe '#identification' do
    before do
      @item = FactoryGirl.create(:order_loose_item, order: @order, item: @item, quantity: BigDecimal.new('3'),
        factory_value: 10, distributor_value: 20, store_value: 30, customer_value: 40)
    end

    context 'without code' do
      before do
        @item.update(item_code: nil)
      end

      it 'returns only the description' do
        expect(@item.identification).to eq("#{@item.item_description}")
      end
    end

    context 'with code and description' do
      it 'returns the code with description' do
        expect(@item.identification).to eq("#{@item.item_code} - #{@item.item_description}")
      end
    end
  end

  describe '#value' do
    before do
      @item = FactoryGirl.create(:order_loose_item, order: @order, item: @item, quantity: BigDecimal.new('3'), factory_value: 10, distributor_value: 20, store_value: 30, customer_value: 40)
    end

    context 'for a distributor client' do
      before do
        @order.update(client: @distributor)
      end

      it 'returns the distributor value' do
        expect(@item.value).to eq(20)
      end
    end

    context 'for a store client' do
      before do
        @order.update(client: @store)
      end

      it 'returns the store value' do
        expect(@item.value).to eq(30)
      end
    end

    context 'for a customer client' do
      before do
        @order.update(client: @customer)
      end

      it 'returns the customer value' do
        expect(@item.value).to eq(40)
      end
    end
  end

  describe '#factory_total' do
    it 'should multiply the quantity by the factory value' do
      expect(FactoryGirl.build(:order_loose_item, order: @order, item: @item, quantity: BigDecimal.new('2.3'), factory_value: BigDecimal.new('58.22')).factory_total).to eq(BigDecimal.new('133.91'))
    end
  end

  describe '#factory_total_discount' do
    it 'should multiply the quantity by the factory value and by the discount' do
      expect(FactoryGirl.build(:order_loose_item, order: @order, item: @item, quantity: BigDecimal.new('2.3'), factory_value: BigDecimal.new('58.22'),
        discount: BigDecimal.new('25.8')).factory_total_discount).to eq(BigDecimal.new('34.55'))
    end
  end

  describe '#factory_total_with_discount' do
    it 'should subtract the factory total with the factory total discount' do
      expect(FactoryGirl.build(:order_loose_item, order: @order, item: @item, quantity: BigDecimal.new('2.3'), factory_value: BigDecimal.new('58.22'),
        discount: BigDecimal.new('25.8')).factory_total_with_discount).to eq(BigDecimal.new('99.36'))
    end
  end

  describe '#distributor_total' do
    it 'should multiply the quantity by the distributor value' do
      expect(FactoryGirl.build(:order_loose_item, order: @order, item: @item, quantity: BigDecimal.new('2.4'), distributor_value: BigDecimal.new('58.22')).distributor_total).to eq(BigDecimal.new('139.73'))
    end
  end

  describe '#distributor_total_discount' do
    it 'should multiply the quantity by the distributor value and by the discount' do
      expect(FactoryGirl.build(:order_loose_item, order: @order, quantity: BigDecimal.new('2.4'), distributor_value: BigDecimal.new('58.22'),
        discount: BigDecimal.new('25.8')).distributor_total_discount).to eq(BigDecimal.new('36.05'))
    end
  end

  describe '#distributor_total_with_discount' do
    it 'should subtract the distributor total with the distributor total discount' do
      expect(FactoryGirl.build(:order_loose_item, order: @order, item: @item, quantity: BigDecimal.new('2.4'), distributor_value: BigDecimal.new('58.22'),
        discount: BigDecimal.new('25.8')).distributor_total_with_discount).to eq(BigDecimal.new('103.68'))
    end
  end

  describe '#store_total' do
    it 'should multiply the quantity by the store value' do
      expect(FactoryGirl.build(:order_loose_item, order: @order, item: @item, quantity: BigDecimal.new('2'), store_value: BigDecimal.new('58.22')).store_total).to eq(BigDecimal.new('116.44'))
    end
  end

  describe '#store_total_discount' do
    it 'should multiply the quantity by the store value and by the discount' do
      expect(FactoryGirl.build(:order_loose_item, order: @order, item: @item, quantity: BigDecimal.new('2'), store_value: BigDecimal.new('58.22'),
        discount: BigDecimal.new('25.8')).store_total_discount).to eq(BigDecimal.new('30.04'))
    end
  end

  describe '#store_total_with_discount' do
    it 'should subtract the store total with the store total discount' do
      expect(FactoryGirl.build(:order_loose_item, order: @order, item: @item, quantity: BigDecimal.new('2'), store_value: BigDecimal.new('58.22'),
        discount: BigDecimal.new('25.8')).store_total_with_discount).to eq(BigDecimal.new('86.40'))
    end
  end

  describe '#customer_total' do
    it 'should multiply the quantity by the customer value' do
      expect(FactoryGirl.build(:order_loose_item, order: @order, item: @item, quantity: BigDecimal.new('3'), customer_value: BigDecimal.new('58.22')).customer_total).to eq(BigDecimal.new('174.66'))
    end
  end

  describe '#customer_total_discount' do
    it 'should multiply the quantity by the customer value and by the discount' do
      expect(FactoryGirl.build(:order_loose_item, order: @order, item: @item, quantity: BigDecimal.new('3'), customer_value: BigDecimal.new('58.22'),
        discount: BigDecimal.new('25.8')).customer_total_discount).to eq(BigDecimal.new('45.06'))
    end
  end

  describe '#customer_total_with_discount' do
    it 'should subtract the customer total with the customer total discount' do
      expect(FactoryGirl.build(:order_loose_item, order: @order, item: @item, quantity: BigDecimal.new('3'), customer_value: BigDecimal.new('58.22'),
        discount: BigDecimal.new('25.8')).customer_total_with_discount).to eq(BigDecimal.new('129.60'))
    end
  end

  describe '#total' do
    before do
      @item = FactoryGirl.create(:order_loose_item, order: @order, item: @item, quantity: BigDecimal.new('3'), factory_value: 10, distributor_value: 20, store_value: 30, customer_value: 40)
    end

    context 'for a distributor client' do
      before do
        @order.update(client: @distributor)
      end

      it 'returns the distributor total' do
        expect(@item.total).to eq(60)
      end
    end

    context 'for a store client' do
      before do
        @order.update(client: @store)
      end

      it 'returns the store total' do
        expect(@item.total).to eq(90)
      end
    end

    context 'for a customer client' do
      before do
        @order.update(client: @customer)
      end

      it 'returns the customer total' do
        expect(@item.total).to eq(120)
      end
    end
  end

  describe '#total_discount' do
    before do
      @order_item = FactoryGirl.create(:order_loose_item, order: @order, item: @item, quantity: BigDecimal.new('3'), factory_value: 10,
        distributor_value: 20, store_value: 30, customer_value: 40, discount: BigDecimal.new('25.8'))
    end

    context 'for a distributor client' do
      before do
        @order.update(client: @distributor)
      end

      it 'returns the distributor total discount' do
        expect(@order_item.total_discount).to eq(BigDecimal.new('15.48'))
      end
    end

    context 'for a store client' do
      before do
        @order.update(client: @store)
      end

      it 'returns the store total discount' do
        expect(@order_item.total_discount).to eq(BigDecimal.new('23.22'))
      end
    end

    context 'for a customer client' do
      before do
        @order.update(client: @customer)
      end

      it 'returns the customer total discount' do
        expect(@order_item.total_discount).to eq(BigDecimal.new('30.96'))
      end
    end
  end

  describe '#total_with_discount' do
    before do
      @order_item = FactoryGirl.create(:order_loose_item, order: @order, item: @item, quantity: BigDecimal.new('3'), factory_value: 10,
        distributor_value: 20, store_value: 30, customer_value: 40, discount: BigDecimal.new('25.8'))
    end

    context 'for a distributor client' do
      before do
        @order.update(client: @distributor)
      end

      it 'returns the distributor total' do
        expect(@order_item.total_with_discount).to eq(BigDecimal.new('44.52'))
      end
    end

    context 'for a store client' do
      before do
        @order.update(client: @store)
      end

      it 'returns the store total' do
        expect(@order_item.total_with_discount).to eq(BigDecimal.new('66.78'))
      end
    end

    context 'for a customer client' do
      before do
        @order.update(client: @customer)
      end

      it 'returns the customer total' do
        expect(@order_item.total_with_discount).to eq(BigDecimal.new('89.04'))
      end
    end
  end
end
