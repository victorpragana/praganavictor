# == Schema Information
#
# Table name: distributors
#
#  id                           :integer          not null, primary key
#  deleted_at                   :datetime
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  commercial_classification_id :integer
#

require 'rails_helper'

RSpec.describe Distributor, type: :model do
  describe 'associations' do
    it { is_expected.to act_as(:third_party) }

    it { is_expected.to have_many(:permitted_administrators) }
    it { is_expected.to have_many(:permitted_managers) }
    it { is_expected.to have_many(:permitted_sellers) }
    it { is_expected.to have_many(:permitted_operators) }
    it { is_expected.to have_many(:permitted_users) }

    it { is_expected.to have_many(:administrators).through(:permitted_administrators).class_name(Administrator) }
    it { is_expected.to have_many(:managers).through(:permitted_managers).class_name(Manager) }
    it { is_expected.to have_many(:sellers).through(:permitted_sellers).class_name(Seller) }
    it { is_expected.to have_many(:operators).through(:permitted_operators).class_name(Operator) }
    it { is_expected.to have_many(:users).through(:permitted_users).class_name(User) }

    it { is_expected.to have_many(:price_tables) }
    it { is_expected.to have_many(:price_table_clients).dependent(:destroy) }
    
    it { is_expected.to have_many(:stores) }
    it { is_expected.to have_many(:customers).through(:stores) }

    it { is_expected.to have_many(:purchasing_estimates).class_name(Estimate) }
    it { is_expected.to have_many(:selling_estimates).class_name(Estimate) }

    it { is_expected.to have_many(:purchasing_orders).class_name(Order) }
    it { is_expected.to have_many(:selling_orders).class_name(Order) }

    it { is_expected.to belong_to(:commercial_classification) }
    it { is_expected.to have_many(:lines).through(:commercial_classification) }
  end

  describe '#price_table_for' do
    before(:each) do
      @owner_1 = create(:factory, name: 'admin1')
      @owner_2 = create(:factory, name: 'admin2')
      @distributor = create(:distributor, name: 'D1')

      @store = create(:store, name: 'S1', distributor: @distributor)

      @price_table_1 = create(:price_table, name: 'P1', owner: @owner_1)
      create(:price_table_client, price_table: @price_table_1, client: @distributor)
      create(:price_table_client, price_table: @price_table_1, client: @store)

      @price_table_2 = create(:price_table, name: 'P2', owner: @owner_2)
    end

    it 'returns the price table por the given owner' do
      expect(@distributor.price_table_for(@owner_1)).to eq(@price_table_1)
    end

    context 'with a default price table' do
      before do
        @price_table_2.update(default: true)
      end

      it 'returns the default price table if the distributor does not have a price table configured' do
        expect(@distributor.price_table_for(@owner_2)).to eq(@price_table_2)
      end
    end

    context 'without a default price table' do
      it 'returns nil if the distributor does not have a price table configured' do
        expect(@distributor.price_table_for(@owner_2)).to be_nil
      end
    end
  end

  describe '#items_price_table_for' do
    before(:each) do
      @owner_1 = create(:factory, name: 'admin1')
      @owner_2 = create(:factory, name: 'admin2')
      @distributor = create(:distributor, name: 'D1')

      @store = create(:store, name: 'S1', distributor: @distributor)

      @price_table_1 = create(:price_table, name: 'P1', owner: @owner_1, type: :for_items)
      create(:price_table_client, price_table: @price_table_1, client: @distributor)
      create(:price_table_client, price_table: @price_table_1, client: @store)

      @price_table_2 = create(:price_table, name: 'P2', owner: @owner_2, type: :for_items)
    end

    it 'returns the price table por the given owner' do
      expect(@distributor.items_price_table_for(@owner_1)).to eq(@price_table_1)
    end

    context 'with a default price table' do
      before do
        @price_table_2.update(default: true)
      end

      it 'returns the default price table if the distributor does not have a price table configured' do
        expect(@distributor.items_price_table_for(@owner_2)).to eq(@price_table_2)
      end
    end

    context 'without a default price table' do
      it 'returns nil if the distributor does not have a price table configured' do
        expect(@distributor.items_price_table_for(@owner_2)).to be_nil
      end
    end
  end

  describe '#owner' do
    before(:each) do
      @distributor = create(:distributor, name: 'D1')
    end

    it 'returns the factory' do
      expect(@distributor.owner).to eq(Factory.first)
    end
  end

  describe '#clients' do
    before(:each) do
      @distributor   = create(:distributor, name: 'D1')
      @distributor_2 = create(:distributor, name: 'D2')
      @store_1       = create(:store, name: 's1', distributor: @distributor)
      @store_2       = create(:store, name: 's2', distributor: @distributor_2)
      @store_3       = create(:store, name: 's3', distributor: @distributor)
      @customer_1    = create(:customer, name: 'c1', store_id: @store_1.id)
      @customer_2    = create(:customer, name: 'c2', store_id: @store_2.id)
      @customer_3    = create(:customer, name: 'c3', store_id: @store_3.id)
    end

    it 'returns all stores and customers for the distributor' do
      expect(@distributor.clients).to match_array([@store_1, @store_3, @customer_1, @customer_3])
    end
  end

  describe '#workplaces' do
    before(:each) do
      @distributor   = create(:distributor, name: 'D1')
      @distributor_2 = create(:distributor, name: 'D2')
      @store_1       = create(:store, name: 's1', distributor: @distributor)
      @store_2       = create(:store, name: 's2', distributor: @distributor_2)
      @store_3       = create(:store, name: 's3', distributor: @distributor)
      @customer_1    = create(:customer, name: 'c1', store_id: @store_1.id)
      @customer_2    = create(:customer, name: 'c2', store_id: @store_2.id)
      @customer_3    = create(:customer, name: 'c3', store_id: @store_3.id)
    end

    it 'returns the distributor and it stores' do
      expect(@distributor.workplaces).to match_array([@distributor, @store_1, @store_3])
    end
  end
end
