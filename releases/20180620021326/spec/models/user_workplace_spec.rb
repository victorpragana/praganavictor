# == Schema Information
#
# Table name: user_workplaces
#
#  id             :integer          not null, primary key
#  user_id        :integer
#  workplace_id   :integer
#  workplace_type :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

require 'rails_helper'

RSpec.describe UserWorkplace, type: :model do
  describe 'associations' do
    it { is_expected.to belong_to(:user) }
    it { is_expected.to belong_to(:workplace) }
  end

  describe 'validation' do
    before do
      @user = FactoryGirl.create(:user)
      @factory = FactoryGirl.create(:factory, name: 'F1')
    end

    it 'fails if the user is not present' do
      expect(FactoryGirl.build(:user_workplace, user: nil, workplace: @factory)).to be_invalid
    end

    it 'fails if the workplace is not present' do
      expect(FactoryGirl.build(:user_workplace, user: @user, workplace: nil)).to be_invalid
    end

    it 'fails if the workplace is not a factory and the user is an operator' do
      user = FactoryGirl.create(:operator)

      distributor = FactoryGirl.create(:distributor, name: 'D1')
      store = FactoryGirl.create(:store, name: 'S1', distributor: distributor)

      workplace = [distributor, store].sample

      expect(FactoryGirl.build(:user_workplace, user: user, workplace: workplace)).to be_invalid
    end
  end
end
