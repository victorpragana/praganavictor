# == Schema Information
#
# Table name: price_table_lines
#
#  id               :integer          not null, primary key
#  price_table_id   :integer
#  line_id          :integer
#  price            :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  maximum_discount :decimal(5, 2)    default(0.0)
#

require 'rails_helper'

RSpec.describe PriceTableLine, type: :model do
  describe 'associations' do
    it { should belong_to(:line) }
    it { should belong_to(:price_table) }
  end

  describe 'validation' do
    before do
      @family = create(:family)
      @line = create(:line, family: @family)
      @distributor = create(:distributor)
      @price_table = create(:price_table, owner: @distributor)
    end

    it 'fails if the price_table is not present' do
      expect(build(:price_table_line, price_table: nil, line: @line)).to be_invalid
    end

    it 'fails if the line is not present' do
      expect(build(:price_table_line, price_table: @price_table, line: nil)).to be_invalid
    end

    it 'fails if the price formula is not present' do
      expect(build(:price_table_line, price_table: @price_table, line: @line, price: nil)).to be_invalid
    end

    it 'fails if the price formula is blank' do
      expect(build(:price_table_line, price_table: @price_table, line: @line, price: '')).to be_invalid
    end

    it 'fails if the price formula is invalid' do
      expect(build(:price_table_line, price_table: @price_table, line: @line, price: '2/0')).to be_invalid
    end

    it 'fails if the price formula uses an invalid word' do
      expect(build(:price_table_line, price_table: @price_table, line: @line, price: '2/_UNIT_')).to be_invalid
    end

    it 'should accept the word _CMV_ for price' do
      expect(build(:price_table_line, price_table: @price_table, line: @line, price: '2/_CMV_')).to be_valid
    end

    it 'fails if the price table is for items' do
      @price_table.update(type: :for_items)

      expect(build(:price_table_line, price_table: @price_table, line: @line)).to be_invalid
    end

    it 'fails if the maximum discount is negative' do
      expect(build(:price_table_line, price_table: @price_table, line: @line, maximum_discount: -1)).to be_invalid
    end

    it 'fails if the maximum discount is greater than 100' do
      expect(build(:price_table_line, price_table: @price_table, line: @line, maximum_discount: BigDecimal.new('100.1'))).to be_invalid
    end

    it 'passes' do
      expect(build(:price_table_line, price_table: @price_table, line: @line)).to be_valid
    end
  end
end
