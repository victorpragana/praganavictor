# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  provider               :string           default("email"), not null
#  uid                    :string           default(""), not null
#  email                  :string           default(""), not null
#  username               :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  name                   :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  tokens                 :json
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  type                   :string
#  maximum_discount       :decimal(5, 2)    default(0.0)
#  erp_id                 :integer
#  selected_workplace_id  :integer
#

require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'associations' do
    it { should belong_to(:selected_workplace).class_name(UserWorkplace) }
    it { should have_many(:workplaces).class_name(UserWorkplace).dependent(:destroy) }
  end

  context 'validation' do
    it 'fails if the type is not present' do
      expect(FactoryGirl.build(:user, type: nil)).to be_invalid
    end

    it 'fails if the type is blank' do
      expect(FactoryGirl.build(:user, type: '')).to be_invalid
    end

    it 'fails if the type does not exists' do
      expect(FactoryGirl.build(:user, type: 'Foo')).to be_invalid
    end

    [:administrator, :manager, :seller].each do |user_type|
      context "for #{user_type}" do
        before do
          @user = FactoryGirl.create(user_type)
          @factory = FactoryGirl.create(:factory, name: 'F1')
          @distributor = FactoryGirl.create(:distributor, name: 'D1')
          @store = FactoryGirl.create(:store, name: 'S1', distributor: @distributor)

          all_workplaces = [@factory, @distributor, @store]

          allowed_workplaces = all_workplaces.sample(2)
          @forbidden_workplace = (all_workplaces - allowed_workplaces).first

          allowed_workplaces.each do |workplace|
            UserWorkplace.create!(user: @user, workplace: workplace)
          end
        end

        it 'fails when trying to change the selected workplace to a workplace not allowed' do
          @user.selected_workplace = UserWorkplace.new(workplace: @forbidden_workplace, id: 99)

          expect(@user).to be_invalid
        end
      end
    end
  end
  describe "user_workplace_by_subdomain" do
    before do 
      @user = FactoryGirl.create(:administrator)
      @factory = FactoryGirl.create(:factory, name: 'F1', subdomain: 'factory-f1')
      @distributor = FactoryGirl.create(:distributor, name: 'D1', subdomain: 'distributor-d1')
      @store = FactoryGirl.create(:store, name: 'S1', distributor: @distributor, subdomain: 'store-s1')

      all_workplaces = [@factory, @distributor]
      all_workplaces.each do |workplace|
        UserWorkplace.create!(user: @user, workplace: workplace)
      end
    end
    it 'return workplace if subdomain exists' do 
      expect(@user.user_workplace_by_subdomain('factory-f1').workplace).to eq(@factory)
    end
    it 'returns nil if subdomain does not exists' do 
      expect(@user.user_workplace_by_subdomain('store-s1')).to be_nil
    end
  end

  describe '#workplace' do
    before do
      @factory = FactoryGirl.create(:factory, name: 'F1')
      @user = FactoryGirl.create(:administrator).tap do |user|
        user.update(selected_workplace: UserWorkplace.create!(user: user, workplace: @factory))
      end
    end

    it 'returns the workplace associated to the selected workplace' do
      expect(@user.workplace).to eq(@factory)
    end
  end
end
