# == Schema Information
#
# Table name: line_categories
#
#  id          :integer          not null, primary key
#  line_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  required    :boolean          default(TRUE), not null
#  category_id :integer
#  order       :integer          not null
#

require 'rails_helper'

RSpec.describe LineCategory, type: :model do
  describe "associations" do
    it { should belong_to(:line) }
    it { should belong_to(:category) }
  end

  describe "validation" do
    before(:each) do
      @family = FactoryGirl.create(:family)
      @category = FactoryGirl.create(:category)
      @line = FactoryGirl.create(:line, :family => @family)
    end

    it "fails if the line is not present" do
      expect(FactoryGirl.build(:line_category, :line => nil, :category => @category)).to be_invalid
    end

    it "fails if the category is not present" do
      expect(FactoryGirl.build(:line_category, :line => @line, :category => nil)).to be_invalid
    end

    it "fails if the line and category are not unique" do
      FactoryGirl.create(:line_category, :line => @line, :category => @category)

      expect(FactoryGirl.build(:line_category, :line => @line, :category => @category)).to be_invalid
    end

    it "fails if the required is not present" do
      expect(FactoryGirl.build(:line_category, :line => @line, :category => @category, :required => nil)).to be_invalid
    end

    it "fails if the order is not present" do
      expect(FactoryGirl.build(:line_category, :line => @line, :category => @category, :order => nil)).to be_invalid
    end

    it "fails if the order is not an integer" do
      expect(FactoryGirl.build(:line_category, :line => @line, :category => @category, :order => BigDecimal.new("0.1"))).to be_invalid
    end

    it "fails if the order is negative" do
      expect(FactoryGirl.build(:line_category, :line => @line, :category => @category, :order => -1)).to be_invalid
    end

    it "fails if there is already a line category with the same order for the line" do
      FactoryGirl.create(:line_category, :line => @line, :category => @category, :order => 2)

      expect(FactoryGirl.build(:line_category, :line => @line, :category => @category, :order => 2)).to be_invalid
    end

    it "should pass" do
      expect(FactoryGirl.build(:line_category, :line => @line, :category => @category)).to be_valid
    end
  end
end
