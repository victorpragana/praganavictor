# == Schema Information
#
# Table name: price_table_items
#
#  id               :integer          not null, primary key
#  price_table_id   :integer
#  item_id          :integer
#  price            :decimal(8, 2)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  maximum_discount :decimal(5, 2)    default(0.0)
#

require 'rails_helper'

RSpec.describe PriceTableItem, type: :model do
  describe 'associations' do
    it { should belong_to(:item) }
    it { should belong_to(:price_table) }
  end

  describe 'validation' do
    before do
      @item = create(:item)
      @distributor = create(:distributor)
      @price_table = create(:price_table, owner: @distributor, type: :for_items)
    end

    it 'fails if the price_table is not present' do
      expect(build(:price_table_item, price_table: nil, item: @item)).to be_invalid
    end

    it 'fails if the item is not present' do
      expect(build(:price_table_item, price_table: @price_table, item: nil)).to be_invalid
    end

    it 'fails if the price is negative' do
      expect(build(:price_table_item, price_table: @price_table, item: @item, price: BigDecimal.new('-0.01'))).to be_invalid
    end

    it 'fails if the price is zero' do
      expect(build(:price_table_item, price_table: @price_table, item: @item, price: 0)).to be_invalid
    end

    it 'fails if the price table is for lines' do
      @price_table.update(type: :for_lines)

      expect(build(:price_table_item, price_table: @price_table, item: @item)).to be_invalid
    end

    it 'fails if the maximum discount is negative' do
      expect(build(:price_table_item, price_table: @price_table, item: @item, maximum_discount: -1)).to be_invalid
    end

    it 'fails if the maximum discount is greater than 100' do
      expect(build(:price_table_item, price_table: @price_table, item: @item, maximum_discount: BigDecimal.new('100.1'))).to be_invalid
    end

    it 'passes' do
      expect(build(:price_table_item, price_table: @price_table, item: @item)).to be_valid
    end
  end
end
