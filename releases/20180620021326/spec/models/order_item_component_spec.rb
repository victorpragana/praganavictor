# == Schema Information
#
# Table name: order_item_components
#
#  id                        :integer          not null, primary key
#  order_item_id             :integer
#  total_consumption         :decimal(10, 5)   not null
#  unit_value                :decimal(10, 5)   not null
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  component_name            :string(255)      not null
#  unit                      :integer          not null
#  component_characteristics :text
#  category_name             :string(255)      not null
#  width_consumption         :decimal(10, 5)
#  height_consumption        :decimal(10, 5)
#  quantity_consumption      :decimal(10, 5)
#  required                  :boolean          default(FALSE), not null
#  total                     :decimal(10, 2)   default(0.0)
#  component_id              :integer
#

require 'rails_helper'

RSpec.describe OrderItemComponent, type: :model do
  describe "associations" do
    it { is_expected.to belong_to(:order_item) }
    it { is_expected.to belong_to(:component) }
  end

  describe 'enums' do
    it { is_expected.to define_enum_for(:unit).with({
      m:     0, 
      mm:    1, 
      un:    2, 
      m2:    3,
      cm:    4,
      km:    5,
      pleat: 6
    }) }
  end

  before do
    @category    = FactoryGirl.create(:category)
    @family      = FactoryGirl.create(:family)
    @line        = FactoryGirl.create(:line, family: @family)
    @component   = FactoryGirl.create(:component, category: @category)
    @distributor = FactoryGirl.create(:distributor, name: 'D1')
    @store       = FactoryGirl.create(:store, distributor: @distributor, name: 'S1')
    @customer    = FactoryGirl.create(:customer, store_id: @store.id, name: 'C1')
    @user        = FactoryGirl.create(:administrator, username: 'F1').tap do |user|
      user.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @store, user: user))
    end
    @order       = FactoryGirl.create(:order, seller: @store, client: @customer, billing_client: @customer, user: @user)
    @order_item  = FactoryGirl.create(:order_item, order: @order, line: @line)
  end

  describe "validation" do
    it "fails if the component is not present" do
      expect(FactoryGirl.build(:order_item_component, order_item: @order_item, component: nil)).to be_invalid
    end

    it "fails if the order item is not present" do
      expect(FactoryGirl.build(:order_item_component, order_item: nil, component_id: @component.id)).to be_invalid
    end

    it "fails if the total_consumption is not present" do
      expect(FactoryGirl.build(:order_item_component, order_item: @order_item, component_id: @component.id, total_consumption: nil)).to be_invalid
    end

    it "fails if the total_consumption is blank" do
      expect(FactoryGirl.build(:order_item_component, order_item: @order_item, component_id: @component.id, total_consumption: "")).to be_invalid
    end

    it "fails if the total_consumption is negative" do
      expect(FactoryGirl.build(:order_item_component, order_item: @order_item, component_id: @component.id, total_consumption: "-1")).to be_invalid
    end

    it "fails if the width_consumption is negative" do
      expect(FactoryGirl.build(:order_item_component, order_item: @order_item, component_id: @component.id, width_consumption: "-1")).to be_invalid
    end

    it "fails if the height_consumption is negative" do
      expect(FactoryGirl.build(:order_item_component, order_item: @order_item, component_id: @component.id, height_consumption: "-1")).to be_invalid
    end

    it "fails if the quantity_consumption is negative" do
      expect(FactoryGirl.build(:order_item_component, order_item: @order_item, component_id: @component.id, quantity_consumption: "-1")).to be_invalid
    end

    it "fails if the unit value is not present" do
      expect(FactoryGirl.build(:order_item_component, order_item: @order_item, component_id: @component.id, unit_value: nil)).to be_invalid
    end

    it "fails if the unit value is blank" do
      expect(FactoryGirl.build(:order_item_component, order_item: @order_item, component_id: @component.id, unit_value: "")).to be_invalid
    end

    it "fails if the unit value is negative" do
      expect(FactoryGirl.build(:order_item_component, order_item: @order_item, component_id: @component.id, unit_value: "-1")).to be_invalid
    end

    it "fails if the component name is not present" do
      expect(FactoryGirl.build(:order_item_component, component_name: nil, component_id: @component.id, order_item: @order_item)).to be_invalid
    end

    it "fails if the component name is blank" do
      expect(FactoryGirl.build(:order_item_component, component_name: "", component_id: @component.id, order_item: @order_item)).to be_invalid
    end

    it "fails if the unit is not present" do
      expect(FactoryGirl.build(:order_item_component, unit: nil, component_id: @component.id, order_item: @order_item)).to be_invalid
    end

    it "fails if the unit is blank" do
      expect(FactoryGirl.build(:order_item_component, unit: "", component_id: @component.id, order_item: @order_item)).to be_invalid
    end

    it "fails if the category name is not present" do
      expect(FactoryGirl.build(:order_item_component, category_name: nil, component_id: @component.id, order_item: @order_item)).to be_invalid
    end

    it "fails if the category name is blank" do
      expect(FactoryGirl.build(:order_item_component, category_name: "", component_id: @component.id, order_item: @order_item)).to be_invalid
    end

    it "fails if the required is not present" do
      expect(FactoryGirl.build(:order_item_component, required: nil, component_id: @component.id, order_item: @order_item)).to be_invalid
    end

    it "fails if the required is blank" do
      expect(FactoryGirl.build(:order_item_component, required: "", component_id: @component.id, order_item: @order_item)).to be_invalid
    end

    it "fails if the total is not present" do
      expect(FactoryGirl.build(:order_item_component, order_item: @order_item, component_id: @component.id, total: nil)).to be_invalid
    end

    it "fails if the total is blank" do
      expect(FactoryGirl.build(:order_item_component, order_item: @order_item, component_id: @component.id, total: "")).to be_invalid
    end

    it "fails if the total is negative" do
      expect(FactoryGirl.build(:order_item_component, order_item: @order_item, component_id: @component.id, total: "-1")).to be_invalid
    end

    it "passes" do
      expect(FactoryGirl.build(:order_item_component, order_item: @order_item, component_id: @component.id)).to be_valid
    end
  end

  describe '#packing_configuration' do
    before do
      @order_item_component = FactoryGirl.create(:order_item_component, order_item: @order_item, component_id: @component.id)

      @package1 = FactoryGirl.create(:packing_configuration, packable: @order, items: [@order_item.id + 1, @order_item.id + 2], component_name: @order_item_component.component_name)
      @package2 = FactoryGirl.create(:packing_configuration, packable: @order, items: [@order_item.id + 4, @order_item.id], component_name: 'Foo bar')
      @package3 = FactoryGirl.create(:packing_configuration, packable: @order, items: [@order_item.id + 3, @order_item.id], component_name: @order_item_component.component_name)
    end

    it 'returns the package configuration that contains the order item' do
      expect(@order_item_component.packing_configuration).to eq(@package3)
    end
  end
end
