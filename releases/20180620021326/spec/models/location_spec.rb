# == Schema Information
#
# Table name: locations
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  category       :integer          not null
#  environment_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  width          :integer
#  height         :integer
#  quantity       :integer
#

require 'rails_helper'

RSpec.describe Location, type: :model do
  before(:each) do
    @distributor = FactoryGirl.create(:distributor, name: 'D1')
    @store       = FactoryGirl.create(:store, distributor: @distributor, name: 'S1')
    @customer    = FactoryGirl.create(:customer, store_id: @store.id, name: 'C1')
    @estimate    = FactoryGirl.create(:estimate, seller: @store, client: @customer)
    @environment = FactoryGirl.create(:environment, estimate: @estimate)
  end

  describe "associations" do
    it { should belong_to(:environment) }
    it { should have_many(:options).dependent(:destroy) }
  end

  describe 'conversions' do
    it { expect(described_class.new).to respond_to(:width_from_mm_to_cm) }
    it { expect(described_class.new).to respond_to(:height_from_mm_to_cm) }
  end

  context "validation" do
    it "fails if the environment is not present" do
      expect(FactoryGirl.build(:location, environment: nil)).to be_invalid
    end

    it "fails if the category is not present" do
      expect(FactoryGirl.build(:location, environment: @environment, category: nil)).to be_invalid
    end

    it "fails if the category is blank" do
      expect(FactoryGirl.build(:location, environment: @environment, category: "")).to be_invalid
    end

    it "fails if the width is zero" do
      expect(FactoryGirl.build(:location, environment: @environment, width: 0)).to be_invalid
    end

    it "fails if the width is negative" do
      expect(FactoryGirl.build(:location, environment: @environment, width: -1)).to be_invalid
    end

    it "fails if the width is not an integer" do
      expect(FactoryGirl.build(:location, environment: @environment, width: BigDecimal.new('0.01'))).to be_invalid
    end

    it "fails if the height is zero" do
      expect(FactoryGirl.build(:location, environment: @environment, height: 0)).to be_invalid
    end

    it "fails if the height is negative" do
      expect(FactoryGirl.build(:location, environment: @environment, height: -1)).to be_invalid
    end

    it "fails if the height is not an integer" do
      expect(FactoryGirl.build(:location, environment: @environment, height: BigDecimal.new('0.01'))).to be_invalid
    end

    it "fails if the quantity is zero" do
      expect(FactoryGirl.build(:location, environment: @environment, quantity: 0)).to be_invalid
    end

    it "fails if the quantity is negative" do
      expect(FactoryGirl.build(:location, environment: @environment, quantity: -1)).to be_invalid
    end

    it "fails if the quantity is not an integer" do
      expect(FactoryGirl.build(:location, environment: @environment, quantity: BigDecimal.new('0.01'))).to be_invalid
    end

    it "should pass" do
      expect(FactoryGirl.build(:location, environment: @environment)).to be_valid
    end
  end
end
