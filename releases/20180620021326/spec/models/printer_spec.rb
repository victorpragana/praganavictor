# == Schema Information
#
# Table name: printers
#
#  id          :integer          not null, primary key
#  name        :string           not null
#  real_name   :string           not null
#  server_name :string           not null
#  server_id   :string           not null
#  default     :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe Printer, type: :model do
  context 'validation' do
    it 'fails if name is not present' do
      expect(FactoryGirl.build(:printer, name: nil)).to be_invalid
    end

    it 'fails if real_name is not present' do
      expect(FactoryGirl.build(:printer, real_name: nil)).to be_invalid
    end

    it 'fails if server_name is not present' do
      expect(FactoryGirl.build(:printer, server_name: nil)).to be_invalid
    end

    it 'fails if server_id is not present' do
      expect(FactoryGirl.build(:printer, server_id: nil)).to be_invalid
    end

    it 'passes' do
      expect(FactoryGirl.build(:printer)).to be_valid
    end
  end

  describe 'after saving' do
    before do
      @printer = FactoryGirl.create(:printer, default: true)
    end

    it 'marks the others printers as not default when the new one is the new default printer' do
      new_printer = FactoryGirl.create(:printer, default: true)

      expect(@printer.reload.default?).to eq(false)
      expect(new_printer.reload.default?).to eq(true)
    end
  end
end
