require 'rails_helper'

RSpec.describe OptionComponentGrouper, type: :grouper do
  before do
    @distributor = create(:distributor, name: 'D1')
    @store       = create(:store, distributor: @distributor, name: 'S1')
    @customer    = create(:customer, store_id: @store.id, name: 'C1')
    @estimate    = create(:estimate, seller: @store, client: @customer)

    @environment = create(:environment, estimate: @estimate)
    @location = create(:location, environment: @environment)

    @family = create(:family)
    @line = create(:line, family: @family, sale_price: BigDecimal.new('12.50'))

    @category = create(:category, name: 'C1')
    @category_2 = create(:category, name: 'C2')

    @value_1 = build(:value, value: 'V1', comparison_value: 100)
    @value_2 = build(:value, value: 'V2', comparison_value: 150)
    @value_3 = build(:value, value: 'V3', comparison_value: 200)

    @specification = build(:specification, automatic: true)
    @specification.values = [@value_1, @value_2, @value_3]
    @specification.save!

    @component_1 = create(:component, unit: :cm, name: 'Comp1', category: @category, erasable: true, cost: BigDecimal.new('10'), values: [@value_1])
    @component_2 = create(:component, unit: :cm, name: 'Comp2', category: @category, erasable: true, cost: BigDecimal.new('12'), values: [@value_2])
    @component_3 = create(:component, unit: :cm, name: 'Comp3', category: @category, erasable: true, cost: BigDecimal.new('15'), values: [@value_3])
    @component_4 = create(:component, unit: :cm, name: 'Comp4', category: @category, erasable: true, cost: BigDecimal.new('22'))
    @component_5 = create(:component, unit: :cm, name: 'Comp5', category: @category_2, erasable: true, cost: BigDecimal.new('20'))

    item_component_1_1 = build(:option_component, component_id: @component_1.id, possible_component_ids: [@component_1.id, @component_2.id, @component_3.id], component_name: @component_1.name, component_characteristics: @component_1.characteristics, category_name: @component_1.category.name, unit: @component_1.unit, total_consumption: 100, unit_value: 250, width_consumption: 20, height_consumption: 30, quantity_consumption: 100, required: true)
    item_component_1_2 = build(:option_component, component_id: @component_5.id, possible_component_ids: [@component_5.id], component_name: @component_5.name, component_characteristics: @component_5.characteristics, category_name: @component_5.category.name, unit: @component_5.unit, total_consumption: 20, unit_value: 65, required: false)

    item_component_2_1 = build(:option_component, component_id: @component_1.id, possible_component_ids: [@component_1.id, @component_2.id], component_name: @component_1.name, component_characteristics: @component_1.characteristics, category_name: @component_1.category.name, unit: @component_1.unit, total_consumption: 100, unit_value: 250, width_consumption: 20, height_consumption: 30, quantity_consumption: 100, required: true)
    item_component_2_2 = build(:option_component, component_id: @component_5.id, possible_component_ids: [@component_5.id], component_name: @component_5.name, component_characteristics: @component_5.characteristics, category_name: @component_5.category.name, unit: @component_5.unit, total_consumption: 20, unit_value: 65, required: false)

    item_component_3_1 = build(:option_component, component_id: @component_1.id, possible_component_ids: [@component_1.id, @component_3.id], component_name: @component_1.name, component_characteristics: @component_1.characteristics, category_name: @component_1.category.name, unit: @component_1.unit, total_consumption: 100, unit_value: 250, width_consumption: 20, height_consumption: 30, quantity_consumption: 100, required: true)
    item_component_3_2 = build(:option_component, component_id: @component_5.id, possible_component_ids: [@component_5.id], component_name: @component_5.name, component_characteristics: @component_5.characteristics, category_name: @component_5.category.name, unit: @component_5.unit, total_consumption: 20, unit_value: 65, required: false)

    item_component_4_1 = build(:option_component, component_id: @component_4.id, possible_component_ids: [@component_4.id], component_name: @component_4.name, component_characteristics: @component_4.characteristics, category_name: @component_4.category.name, unit: @component_1.unit, total_consumption: 100, unit_value: 250, width_consumption: 20, height_consumption: 30, quantity_consumption: 100, required: true)

    @option_1 = create(:option, width: 100, height: 300, line: @line, location: @location, selected: true, components: [item_component_1_1, item_component_1_2])
    @option_2 = create(:option, width:  70, height: 100, line: @line, location: @location, selected: true, components: [item_component_2_1, item_component_2_2])
    @option_3 = create(:option, width:  70, height: 200, line: @line, location: @location, selected: false, components: [item_component_3_1, item_component_3_2])
    @option_4 = create(:option, width:  70, height: 120, line: @line, location: @location, selected: true, components: [item_component_4_1])
  end

  describe 'validation' do
    it 'fails if the component id is not present' do
      expect(described_class.new(nil, @estimate.options.selected, @estimate.optimize?)).to be_invalid
    end

    it 'fails if the component id is blank' do
      expect(described_class.new('', @estimate.options.selected, @estimate.optimize?)).to be_invalid
    end

    it 'fails if the component was deleted' do
      component = @option_1.components.first.component
      id = component.id

      component.destroy!

      expect(described_class.new(id, @estimate.options.selected, @estimate.optimize?)).to be_invalid
    end

    it 'fails if the possible options are not present' do
      expect(described_class.new(@option_1.components[0].component.id, nil, @estimate.optimize?)).to be_invalid
    end

    it 'fails if the possible options are blank' do
      expect(described_class.new(@option_1.components[0].component.id, '', @estimate.optimize?)).to be_invalid
    end

    context 'for automatic components' do
      it 'fails if the component has rules with different control methods' do
        line_rule = create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*3', automatic: true)
        create(:rule, line_rule: line_rule, component: @component_1, control_method: Rule.control_methods[:width], total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*3', automatic: true)

        line_rule = create(:line_rule, line: @line, control_method: LineRule.control_methods[:height], total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*3', automatic: true)
        create(:rule, line_rule: line_rule, component: @component_3, control_method: Rule.control_methods[:height], total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_*3', automatic: true)

        expect(described_class.new(@option_1.components[0].component.id, @estimate.options.selected, @estimate.optimize?)).to be_invalid
      end
    end
  end

  describe '#group' do
    context 'with valid data' do
      context 'for non automatic component' do
        it 'returns true' do
          service = described_class.new(@option_1.components[1].component.id, @estimate.options.selected, @estimate.optimize?)

          expect(service.group).to eq(true)
        end

        it 'returns the same component' do
          service = described_class.new(@option_1.components[1].component.id, @estimate.options.selected, @estimate.optimize?)

          result = service.group
          
          expect(service.components).to match_array([@component_5.id])
        end

        it 'returns the options that have the component' do
          service = described_class.new(@option_1.components[1].component.id, @estimate.options.selected, @estimate.optimize?)

          result = service.group
          
          expect(service.options).to match_array([@option_1, @option_2])
        end

        it 'returns the same component' do
          service = described_class.new(@option_4.components[0].component.id, @estimate.options.selected, @estimate.optimize?)

          result = service.group
          
          expect(service.components).to match_array([@component_4.id])
        end

        it 'returns the options that have the component' do
          service = described_class.new(@option_4.components[0].component.id, @estimate.options.selected, @estimate.optimize?)

          result = service.group
          
          expect(service.options).to match_array([@option_4])
        end
      end

      context 'for automatic component' do
        context 'without optimization' do
          before do
            @estimate.update(optimize: false)
          end

          it 'returns the intersection of the selected options components' do
            service = described_class.new(@option_1.components[0].component.id, @estimate.options.selected, @estimate.optimize?)

            result = service.group
            
            expect(service.components).to match_array([@component_1.id, @component_2.id])
          end

          it 'returns the selected options that have the component' do
            service = described_class.new(@option_1.components[0].component.id, @estimate.options.selected, @estimate.optimize?)

            result = service.group
            
            expect(service.options).to match_array([@option_1, @option_2])
          end
        end

        context 'with optimization' do
          before do
            @estimate.update(optimize: true)
          end

          it 'returns the union of the selected options components' do
            service = described_class.new(@option_1.components[0].component.id, @estimate.options.selected, @estimate.optimize?)

            result = service.group
            
            expect(service.components).to match_array([@component_1.id, @component_2.id, @component_3.id])
          end

          it 'returns the selected options that have the component' do
            service = described_class.new(@option_1.components[0].component.id, @estimate.options.selected, @estimate.optimize?)

            result = service.group
            
            expect(service.options).to match_array([@option_1, @option_2])
          end
        end
      end
    end
  end
end