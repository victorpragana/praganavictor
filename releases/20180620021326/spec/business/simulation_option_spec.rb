require 'rails_helper'

RSpec.describe SimulationOption, type: :business do
  before(:each) do
    @distributor = create(:distributor, name: 'D1')
    @store       = create(:store, distributor: @distributor, name: 'S1')
    @customer    = create(:customer, store_id: @store.id, name: 'C1')
    @estimate    = create(:estimate, seller: @store, client: @customer)

    @environment = create(:environment, estimate: @estimate)
    @location = create(:location, environment: @environment)

    @family = create(:family)
    @line = create(:line, family: @family, sale_price: BigDecimal.new('12.50'))
  end

  context 'validation' do
    it 'fails if the line id is not present' do
      expect(described_class.new(nil, 100, 200)).to be_invalid
    end

    it 'fails if the line id is blank' do
      expect(described_class.new('', 100, 200)).to be_invalid
    end

    it 'fails if the width is not present' do
      expect(described_class.new(@line.id, nil, 200)).to be_invalid
    end

    it 'fails if the width is blank' do
      expect(described_class.new(@line.id, '', 200)).to be_invalid
    end

    it 'fails if the width is zero' do
      expect(described_class.new(@line.id, 0, 200)).to be_invalid
    end

    it 'fails if the width is negative' do
      expect(described_class.new(@line.id, -1, 200)).to be_invalid
    end

    it 'fails if the width is not an integer' do
      expect(described_class.new(@line.id, BigDecimal.new('0.01'), 200)).to be_invalid
    end

    it 'fails if the height is not present' do
      expect(described_class.new(@line.id, 100, nil)).to be_invalid
    end

    it 'fails if the height is blank' do
      expect(described_class.new(@line.id, 100, '')).to be_invalid
    end

    it 'fails if the height is zero' do
      expect(described_class.new(@line.id, 100, 0)).to be_invalid
    end

    it 'fails if the height is negative' do
      expect(described_class.new(@line.id, 100, -1)).to be_invalid
    end

    it 'fails if the height is not an integer' do
      expect(described_class.new(@line.id, 100, BigDecimal.new('0.01'))).to be_invalid
    end
  end

  describe '#line_id' do
    it 'returns the line id' do
      expect(described_class.new(@line.id, 100, 200).line_id).to eq(@line.id)
    end
  end

  describe '#width' do
    it 'returns the width' do
      expect(described_class.new(@line.id, 100, 200).width).to eq(100)
    end
  end

  describe '#height' do
    it 'returns the height' do
      expect(described_class.new(@line.id, 100, 200).height).to eq(200)
    end
  end

  describe '#questions' do
    it 'returns the questions' do
      expect(described_class.new(@line.id, 100, 200, {
        'LOCAL_INSTALACAO' => 'banheiro',
        'MAO_DE_OBRA' => true,
        'FRETE' => false
      }).questions).to eq({
        'LOCAL_INSTALACAO' => 'banheiro',
        'MAO_DE_OBRA' => true,
        'FRETE' => false
      })
    end
  end

  describe '#id' do
    it 'returns a random uuid' do
      uuid = nil

      object = described_class.new(@line.id, 100, 200)

      uuid = object.id

      expect(uuid).to_not be_nil
      expect(uuid).to eq(object.id)
    end
  end
end