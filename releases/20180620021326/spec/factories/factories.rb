# == Schema Information
#
# Table name: factories
#
#  id         :integer          not null, primary key
#  deleted_at :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :factory do
    name                  {Faker::Name.name}
    identification        {Faker::CNPJ.numeric}
    zip_code              {Faker::Address.zip_code}
    street                {Faker::Address.street_name}
    neighborhood          {Faker::Address.street_name}
    number                {Faker::Address.building_number}
    complement            {Faker::Address.secondary_address}
    phone                 {Faker::Number.number(9)}
    email                 {Faker::Internet.email}
    corporate_name        {Faker::Company.name}
    subdomain             {"#{name.parameterize}"}
  end
end
