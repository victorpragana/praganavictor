# == Schema Information
#
# Table name: order_loose_items
#
#  id                :integer          not null, primary key
#  order_id          :integer
#  item_code         :string
#  item_description  :string           not null
#  unit              :integer          not null
#  quantity          :decimal(8, 2)    not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  type              :integer          default(0)
#  factory_value     :decimal(10, 2)   default(0.0)
#  distributor_value :decimal(10, 2)   default(0.0)
#  store_value       :decimal(10, 2)   default(0.0)
#  customer_value    :decimal(10, 2)   default(0.0)
#  discount          :decimal(5, 2)    default(0.0)
#  item_id           :integer
#

FactoryGirl.define do
  factory :order_loose_item do
    order nil
    item_code        { Faker::Code.ean }
    item_description { Faker::Commerce.product_name }
    unit             { OrderLooseItem.units.to_a.sample[1] }
    quantity         { Faker::Number.between(1, 5) }
    factory_value     { Faker::Number.between(1, 5) }
    distributor_value { Faker::Number.between(1, 5) }
    store_value       { Faker::Number.between(1, 5) }
    customer_value    { Faker::Number.between(1, 5) }
  end
end
