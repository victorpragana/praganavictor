# == Schema Information
#
# Table name: cities
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  state_id   :integer
#  ibge_code  :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :city do
    name                    {Faker::Address.city}
    ibge_code               {Faker::Number.number(7)}
  end
end
