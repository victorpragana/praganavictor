# == Schema Information
#
# Table name: printers
#
#  id          :integer          not null, primary key
#  name        :string           not null
#  real_name   :string           not null
#  server_name :string           not null
#  server_id   :string           not null
#  default     :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryGirl.define do
  factory :printer do
    name        { Faker::App.name }
    real_name   { Faker::Company.name }
    server_name { Faker::Company.name }
    server_id   { Faker::Bitcoin.address }
  end
end
