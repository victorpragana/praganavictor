# == Schema Information
#
# Table name: families
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  color      :string(7)        not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :family do
    name  {Faker::Name.name}
    color {"##{Faker::Number.hexadecimal(6)}"}
  end
end
