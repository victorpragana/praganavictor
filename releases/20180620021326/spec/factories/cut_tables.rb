# == Schema Information
#
# Table name: cut_tables
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  size       :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :cut_table do
    name { Faker::Name.name }
    size { Faker::Number.between(1, 2000) }
  end
end
