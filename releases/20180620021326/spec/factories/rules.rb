# == Schema Information
#
# Table name: rules
#
#  id                   :integer          not null, primary key
#  component_id         :integer
#  width                :int4range
#  height               :int4range
#  control_method       :integer          not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  total_consumption    :string           default(""), not null
#  cost                 :string           default(""), not null
#  required             :boolean          default(TRUE), not null
#  height_consumption   :string
#  width_consumption    :string
#  line_rule_id         :integer
#  automatic            :boolean          default(FALSE)
#  quantity_consumption :string
#  description          :string
#  weight               :integer          default(0), not null
#  area                 :int4range
#

FactoryGirl.define do
  factory :rule do
    width             {Faker::Number.between(1, 10)..Faker::Number.between(10, 20)}
    height            {Faker::Number.between(1, 10)..Faker::Number.between(10, 20)}
    control_method    {Faker::Number.between(0, 3)}
    total_consumption {"1+1"}
    cost              {"1+1"}
    automatic         {false}
    required          {[false, true].sample}
    description       {Faker::Lorem.sentence}
    weight            {:general}
  end
end
