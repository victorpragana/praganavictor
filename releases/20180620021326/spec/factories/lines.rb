# == Schema Information
#
# Table name: lines
#
#  id                   :integer          not null, primary key
#  name                 :string           not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  family_id            :integer
#  sale_price           :decimal(8, 3)    default(0.0), not null
#  horizontal_increment :integer          default(0), not null
#  vertical_increment   :integer          default(0), not null
#  erp_categories       :json
#  ncm                  :string(8)
#  cst                  :integer
#  active               :boolean          default(TRUE)
#  finished             :boolean          default(FALSE)
#

FactoryGirl.define do
  factory :line do
    name                  {Faker::Name.name}
    sale_price            {Faker::Number.between(0, 5)}
    vertical_increment    {1}
    horizontal_increment  {1}
  end
end
