# == Schema Information
#
# Table name: specifications
#
#  id             :integer          not null, primary key
#  name           :string           not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  deleted_at     :datetime
#  automatic      :boolean          default(FALSE)
#  used_in_filter :boolean          default(FALSE)
#

FactoryGirl.define do
  factory :specification do
    name      {Faker::Name.name}
    automatic {false}
  end
end
