# == Schema Information
#
# Table name: order_item_components
#
#  id                        :integer          not null, primary key
#  order_item_id             :integer
#  total_consumption         :decimal(10, 5)   not null
#  unit_value                :decimal(10, 5)   not null
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  component_name            :string(255)      not null
#  unit                      :integer          not null
#  component_characteristics :text
#  category_name             :string(255)      not null
#  width_consumption         :decimal(10, 5)
#  height_consumption        :decimal(10, 5)
#  quantity_consumption      :decimal(10, 5)
#  required                  :boolean          default(FALSE), not null
#  total                     :decimal(10, 2)   default(0.0)
#  component_id              :integer
#

FactoryGirl.define do
  factory :order_item_component do
    order_item         nil
    total_consumption {Faker::Number.between(0, 5)}
    unit_value        {Faker::Number.between(0, 5)}
    component_name    {Faker::Name.name}
    unit              {Faker::Number.between(0, 2)}
    category_name     {Faker::Name.name}
  end
end
