# == Schema Information
#
# Table name: estimate_items
#
#  id                :integer          not null, primary key
#  estimate_id       :integer
#  item_code         :string
#  item_description  :string           not null
#  unit              :integer          not null
#  quantity          :decimal(8, 2)    not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  type              :integer          default(0)
#  factory_value     :decimal(10, 2)   default(0.0)
#  distributor_value :decimal(10, 2)   default(0.0)
#  store_value       :decimal(10, 2)   default(0.0)
#  customer_value    :decimal(10, 2)   default(0.0)
#  discount          :decimal(5, 2)    default(0.0)
#  item_id           :integer
#

FactoryGirl.define do
  factory :estimate_item do
    estimate          nil
    item_code         { Faker::Code.ean }
    item_description  { Faker::Commerce.product_name }
    unit              { EstimateItem.units.to_a.sample[1] }
    quantity          { Faker::Number.between(1, 5) }
    factory_value     { Faker::Number.between(1, 5) }
    distributor_value { Faker::Number.between(1, 5) }
    store_value       { Faker::Number.between(1, 5) }
    customer_value    { Faker::Number.between(1, 5) }
  end
end
