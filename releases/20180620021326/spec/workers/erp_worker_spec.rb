require 'rails_helper'

Sidekiq::Testing.inline!

RSpec.describe ErpWorker, type: :worker do
  it { is_expected.to be_processed_in :erp }
  it { is_expected.to save_backtrace }
  it { is_expected.to be_retryable 0 }

  describe '#perform' do
    subject { described_class }
    let(:payment_condition_id) { 20 }

    before do
      @factory = create(:factory, name: 'F1')
      create(:administrator, username: 'F1').tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: @factory, user: administrator))
      end
      
      @family = create(:family)
      
      @category = create(:category, name: 'C1')
      @category_2 = create(:category, name: 'C2')

      @line = create(:line, family: @family)

      @line.categories << build(:line_category, category: @category, required: true)
      @line.categories << build(:line_category, category: @category_2, required: false)

      @line.erp_categories = {"LINHA DE PRODUTO"=>"PRODUTO ACABADO", "GRUPO"=>"ADAPTADOR", "SUBGRUPO"=>"ROMANA", "TRIBUTAÇÃO"=>"TRIBUTADO ST"}
      @line.ncm = '25687498'
      @line.cst = 5

      @line.save!

      @component_1 = create(:component, unit: :cm, category: @category, erasable: true, cost: BigDecimal.new('32.28'), name: 'CC1')
      @component_2 = create(:component, unit: :cm, category: @category_2, erasable: true, name: 'CC2')

      @state         = create(:state)
      @city          = create(:city, state: @state)

      @distributor = create(:distributor, name: 'D1')
      @store       = create(:store, distributor: @distributor, name: 'S1')
      @customer    = create(:customer, store_id: @store.id, name: 'C1', identification: '31177002850', city: @city)
      @bcustomer   = create(:customer, store_id: @store.id, name: 'CB1', identification: '40577554204', city: @city,
        allow_special_discount: true, special_discount: 50)

      @order = create(:order, seller: @store, client: @customer, billing_client: @bcustomer, user: @factory.users.first)

      item_component_1 = build(:order_item_component, component_id: @component_1.id, component_name: @component_1.name, component_characteristics: @component_1.characteristics, 
        category_name: @component_1.category.name, unit: @component_1.unit, total_consumption: 100, unit_value: 250, width_consumption: 20, 
        height_consumption: 30, quantity_consumption: 100, required: true, total: 25000)
      item_component_2 = build(:order_item_component, component_id: @component_2.id, component_name: @component_2.name, component_characteristics: @component_2.characteristics, 
        category_name: @component_2.category.name, unit: @component_2.unit, total_consumption: 20, unit_value: 65, required: false, total: 1300)

      @item = create(:item, erp_categories: {"LINHA DE PRODUTO"=>"SERVIÇO", "GRUPO"=>"SERVIÇO GRUPO", "SUBGRUPO"=>"ROMANA", "TRIBUTAÇÃO"=>"TRIBUTADO ST"},
        ncm: '23697854', cst: 3)

      @order_item_1 = create(:order_item, factory_total: 100, distributor_total: 120, line: @line, order: @order,
        components: [item_component_1, item_component_2],
        store_total: BigDecimal.new('263.65'), customer_total: BigDecimal.new('369.11'), discount: 10)
      @order_item_2 = create(:order_item, factory_total: 80, distributor_total: 100, line: @line, order: @order,
        components: [item_component_1, item_component_2],
        store_total: BigDecimal.new('360.36'), customer_total: BigDecimal.new('504.50'), discount: 10)
      @order_item_3 = create(:order_item, factory_total: 80, distributor_total: 100, line: @line, order: @order,
        components: [item_component_1, item_component_2],
        store_total: BigDecimal.new('360.36'), customer_total: BigDecimal.new('504.50'), discount: 10)
      @order_item_4 = create(:order_item, factory_total: 80, distributor_total: 100, line: @line, order: @order,
        components: [item_component_1, item_component_2],
        store_total: BigDecimal.new('360.36'), customer_total: BigDecimal.new('504.50'), discount: 10)
      @order_item_5 = create(:order_item, factory_total: 80, distributor_total: 100, line: @line, order: @order,
        components: [item_component_1, item_component_2],
        store_total: BigDecimal.new('360.36'), customer_total: BigDecimal.new('504.50'), discount: 10)
      @order_item_6 = create(:order_item, factory_total: 80, distributor_total: 100, line: @line, order: @order,
        components: [item_component_1, item_component_2],
        store_total: BigDecimal.new('360.36'), customer_total: BigDecimal.new('504.50'), discount: 10)
      @order_item_7 = create(:order_item, factory_total: 80, distributor_total: 100, line: @line, order: @order,
        components: [item_component_1, item_component_2],
        store_total: BigDecimal.new('149.35'), customer_total: BigDecimal.new('209.09'), discount: 5)
      @order_item_8 = create(:order_item, factory_total: 80, distributor_total: 100, line: @line, order: @order,
        components: [item_component_1, item_component_2],
        store_total: BigDecimal.new('202.70'), customer_total: BigDecimal.new('283.78'), discount: 5)

      @order_loose_item = create(:order_loose_item, order: @order, item: @item, quantity: 3, factory_value: BigDecimal.new('52.22'), distributor_value: BigDecimal.new('60.36'),
        store_value: BigDecimal.new('68.55'), customer_value: BigDecimal.new('500'), discount: 15)
    end

    context 'when the order is \'to_send\' or \'error\'' do
      let(:client)        { double(:service, third_parties: third_parties, products: products, orders: orders) }
      
      let(:third_parties) { double(:third_parties, list: [], create: double(:create_response, status: 201, payload: 28),
        update: double(:update_response, status: 200, payload: 28)) }
      let(:response)      { double(:response, status: 200, payload: [
        {
          "classes"=>[], "id"=>26, "dataAlteracao"=>"20-01-2017 13:01:57", "dataCriacao"=>"20-01-2017 13:01:57", 
          "ufRg"=>"SP", "categoria"=>"PADRÃO", "emails"=>["rodrigo@coyo.com.br"], "enderecos"=>[], "ativo"=>true, 
          "nome"=>"RODRIGO CESAR LIMA ", "documento"=>"311.770.028-50", "rg"=>"363547514", 
          "camposCustomizados"=>{
            "id"=>26, "valoresComposicao"=>[], "valoresLista"=>[], "valoresPrimitivo"=>[]
          }, "tipoPessoa"=>"PF", "telefones"=>[], "dataNascimento"=>"21-03-1983 00:00:00"
        } ])
      }

      let(:products)          { double(:products, list: products_response, 
        update: double(:update_response, status: 200, payload: { 'idRecurso' => 10, 'codigoMensagem' => 0, 'mensagem' => 'Operação realizada com sucesso.'})) }
      let(:products_response) { double(:response, status: 200, payload: []) }
      let(:product_1)         { double(:product_1, status: 201, payload: { 'idRecurso' => 10, 'codigoMensagem' => 0, 'mensagem' => 'Operação realizada com sucesso.'}) }
      let(:product_2)         { double(:product_2, status: 201, payload: { 'idRecurso' => 15, 'codigoMensagem' => 0, 'mensagem' => 'Operação realizada com sucesso.'}) }
      let(:item)              { double(:item, status: 201, payload: { 'idRecurso' => 25, 'codigoMensagem' => 0, 'mensagem' => 'Operação realizada com sucesso.'}) }

      let(:orders)            { double(:orders, save: double(:create_response, status: 201, payload: 10))}

      before do
        allow(Order).to receive(:find).and_return(@order)

        @order.update(status: [Order.statuses[:to_send], Order.statuses[:error]].sample)

        @third_party_searcher = Varejonline::Searcher::Administrative::ThirdPartySearcher.new({
          documento: '40577554204', quantidade: 1
        })

        @item_searcher = Varejonline::Searcher::Commercial::ProductSearcher.new({
          codigoInterno: "#{@order_loose_item.item_code}", quantidade: 1
        })

        allow(Varejonline).to receive(:new).and_return(client)
        allow(Varejonline::Searcher::Administrative::ThirdPartySearcher).to receive(:new).and_return(@third_party_searcher)
        allow(third_parties).to receive(:list).and_return(response)

        allow(Varejonline::Searcher::Commercial::ProductSearcher).to receive(:new).and_return(@item_searcher)

        allow(products).to receive(:save).and_return(product_1, product_2, item)
      end

      it 'changes the order status to \'sending\'' do
        expect(@order).to receive(:sending!)

        subject.perform_async(@order.id, payment_condition_id)
      end

      context 'for client information' do
        it 'searches the client by identification' do
          expect(Varejonline::Searcher::Administrative::ThirdPartySearcher).to receive(:new).with({
            documento: '40577554204', quantidade: 1
          }).and_return(@third_party_searcher)

          expect(third_parties).to receive(:list).with(@third_party_searcher)

          subject.perform_async(@order.id, payment_condition_id)
        end

        context 'with search error' do
          let(:error_response) { double(:error_response, status: 400, payload: 'Error') }

          before do
            allow(third_parties).to receive(:list).and_return(error_response)

            @time = Time.now

            allow(Time).to receive(:now).and_return(@time)
          end

          it 'sets the order as \'error\'' do
            subject.perform_async(@order.id, payment_condition_id)

            expect(@order.reload.error?).to eq(true)
          end

          it 'stores the error with date information' do
            subject.perform_async(@order.id, payment_condition_id)

            expect(@order.reload.processing_messages).to match_array(["#{Time.now.strftime('%d/%m/%Y - %H:%M')} - #{error_response.payload}"])
          end
        end

        context 'when the billing client does not exists in erp' do
          let(:response) { double(:response, status: 200, payload: []) }

          before do
            allow(third_parties).to receive(:list).and_return(response)
          end

          it 'creates a new client in the erp' do
            expect(third_parties).to receive(:create).with({'nome' => @order.billing_client.name, 'documento' => @order.billing_client.identification,
              'emails' => [@order.billing_client.email], 'rg' => @order.billing_client.rg, 'dataNascimento' => nil, 'ie' => @order.billing_client.state_inscription,
              'enderecos' => [ {
                'tipo' => 'RUA', 'logradouro' => @order.billing_client.street, 'numero' => @order.billing_client.number, 'bairro' => @order.billing_client.neighborhood,
                'complemento' => @order.billing_client.complement, 'cep' => @order.billing_client.zip_code, 'codigoIBGECidade' => @order.billing_client.city.ibge_code,
                'tipoEndereco' => 'ENDERECO_COBRANCA' } ],
              'telefones' => [ {'ddi' => '55', 'ddd' => @order.billing_client.phone[0..1], 'numero' => @order.billing_client.phone[2..-1]} ],
              'classes' => ['CLIENTE']
            })

            subject.perform_async(@order.id, payment_condition_id)
          end

          context 'with error' do
            let(:create_response) { double(:create_response, status: 400, payload: 'Error') }

            before do
              allow(third_parties).to receive(:create).and_return(create_response)
              @time = Time.now

              allow(Time).to receive(:now).and_return(@time)
            end

            it 'sets the order as \'error\'' do
              subject.perform_async(@order.id, payment_condition_id)

              expect(@order.reload.error?).to eq(true)
            end

            it 'stores the error with date information' do
              subject.perform_async(@order.id, payment_condition_id)

              expect(@order.reload.processing_messages).to match_array(["#{Time.now.strftime('%d/%m/%Y - %H:%M')} - #{create_response.payload}"])
            end
          end
        end

        context 'when the billing client exists in erp' do
          let(:response) { double(:response, status: 200, payload: [{
            'classes' => [], 'id' => 26, 'categoria' => 'PADRÃO', 'ufRg' => 'SP', 'rg' => '363547514', 'nome' => 'RODRIGO CESAR LIMA ',
            'dataAlteracao' => '20-01-2017 13:01:57','dataCriacao' => '20-01-2017 13:01:57', 'telefones' => [], 'documento' => '311.770.028-50',
            'camposCustomizados' => {
              'id' => 26, 'valoresComposicao' => [], 'valoresPrimitivo' => [], 'valoresLista' => []
            }, 'emails' => ['rodrigo@coyo.com.br'], 'ativo' => true, 'enderecos' => [], 'dataNascimento' => '21-03-1983 00:00:00',
            'tipoPessoa' => 'PF' }
            ])
          }

          before do
            allow(third_parties).to receive(:list).and_return(response)
          end

          it 'updates the billing client in erp' do
            expect(third_parties).to receive(:update).with(26, {'nome' => @order.billing_client.name, 'documento' => @order.billing_client.identification,
              'emails' => [@order.billing_client.email], 'rg' => @order.billing_client.rg, 'dataNascimento' => nil, 'ie' => @order.billing_client.state_inscription,
              'enderecos' => [ {
                'tipo' => 'RUA', 'logradouro' => @order.billing_client.street, 'numero' => @order.billing_client.number, 'bairro' => @order.billing_client.neighborhood,
                'complemento' => @order.billing_client.complement, 'cep' => @order.billing_client.zip_code, 'codigoIBGECidade' => @order.billing_client.city.ibge_code,
                'tipoEndereco' => 'ENDERECO_COBRANCA' } ],
              'telefones' => [ {'ddi' => '55', 'ddd' => @order.billing_client.phone[0..1], 'numero' => @order.billing_client.phone[2..-1]} ],
              'classes' => ['CLIENTE']
            })

            subject.perform_async(@order.id, payment_condition_id)
          end

          context 'with error' do
            let(:error_response) { double(:error_response, status: 400, payload: 'Error') }

            before do
              allow(third_parties).to receive(:update).and_return(error_response)
              @time = Time.now

              allow(Time).to receive(:now).and_return(@time)
            end

            it 'sets the order as \'error\'' do
              subject.perform_async(@order.id, payment_condition_id)

              expect(@order.reload.error?).to eq(true)
            end

            it 'stores the error with date information' do
              subject.perform_async(@order.id, payment_condition_id)

              expect(@order.reload.processing_messages).to match_array(["#{Time.now.strftime('%d/%m/%Y - %H:%M')} - #{error_response.payload}"])
            end
          end
        end
      end

      context 'for products information' do
        let(:product_1_data) { {
          'descricao' => @order_item_1.name_for_erp, 'especificacao' => @order_item_1.specification,
          'metodoControle' => ErpWorker::CONTROL_METHOD, 'codigoNcm' => @line.ncm,
          'origem' => Line.csts[@line.cst], 'codigoInterno' => "#{@order_item_1.id.to_s.rjust(6, '0')}",
          'unidade' => 'UN', 'classificacao' => ErpWorker::PRODUCT_CLASSIFICATION,
          'custoReferencial' => "#{@order_item_1.total_cost.to_d}", 'listCustoReferencial' => [
            {'entidade' => ErpWorker::ENTITY_ID, 'precoCusto' => "#{@order_item_1.total_cost.to_d}"}
          ], 'categorias' => @line.erp_categories.map{|k,v| {'nome' => v, 'nivel' => k} }
        } }
        let(:product_2_data) { {
          'descricao' => @order_item_2.name_for_erp, 'especificacao' => @order_item_2.specification,
          'metodoControle' => ErpWorker::CONTROL_METHOD, 'codigoNcm' => @line.ncm,
          'origem' => Line.csts[@line.cst], 'codigoInterno' => "#{@order_item_2.id.to_s.rjust(6, '0')}",
          'unidade' => 'UN', 'classificacao' => ErpWorker::PRODUCT_CLASSIFICATION,
          'custoReferencial' => "#{@order_item_2.total_cost.to_d}", 'listCustoReferencial' => [
            {'entidade' => ErpWorker::ENTITY_ID, 'precoCusto' => "#{@order_item_2.total_cost.to_d}"}
          ], 'categorias' => @line.erp_categories.map{|k,v| {'nome' => v, 'nivel' => k} }
        } }

        before do
          allow(Varejonline::Entity::Commercial::Product).to receive(:new).and_return(product_1_data, product_2_data)
        end

        it 'searchers the products by the code' do
          expect(Varejonline::Searcher::Commercial::ProductSearcher).to receive(:new).with({
            codigoInterno: "#{@order_item_1.id.to_s.rjust(6, '0')}", quantidade: 1
          }).and_return(@item_searcher)
          expect(Varejonline::Searcher::Commercial::ProductSearcher).to receive(:new).with({
            codigoInterno: "#{@order_item_2.id.to_s.rjust(6, '0')}", quantidade: 1
          }).and_return(@item_searcher)

          expect(products).to receive(:list).with(@item_searcher)

          subject.perform_async(@order.id, payment_condition_id)
        end

        context 'with search error' do
          let(:error_response) { double(:error_response, status: 400, payload: 'Error') }

          before do
            allow(products).to receive(:list).and_return(error_response)

            @time = Time.now

            allow(Time).to receive(:now).and_return(@time)
          end

          it 'sets the order as \'error\'' do
            subject.perform_async(@order.id, payment_condition_id)

            expect(@order.reload.error?).to eq(true)
          end

          it 'stores the error with date information' do
            subject.perform_async(@order.id, payment_condition_id)

            expect(@order.reload.processing_messages).to match_array(["#{Time.now.strftime('%d/%m/%Y - %H:%M')} - #{error_response.payload}"])
          end
        end

        context 'when the item exists in the erp' do
          let(:product_1_response) { double(:product_response, status: 200, payload: [
            { 'id' => 1, 'referenciaBase' => [4,1], 'listCustoReferencial' => [
              { 'entidade' => 3, 'precoCusto' => 1 }, { 'entidade' => 1, 'precoCusto' => 1 }, { 'entidade' => 2, 'precoCusto' => 1 }
            ], 'custoReferencial' => 1, 'preco' => 0, 'margemLucro' => 0, 'permiteVenda' => true, 'origem' => 0, 'descricao' => 'ROLO MONOCOMANDO PADRAO 38 BASE ESTREITA',
            'codigoSistema' => '1.4.1', 'mercadoriaBase' => true, 'descontoMaximo' => 0, 'dataCriacao' => '04-11-2015 10:49:56', 
            'dataAlteracao' => '19-10-2016 15:59:38', 'ativo' => false, 'codigoReferenciaBase' => '1.4.1', 'unidadeCasasDecimais' => 4,
            'peso' => 0, 'categorias' => [
              { 'id' => 4, 'nome' => 'ROLO', 'nivel' => 'GRUPO' }, { 'id' => 1, 'nome' => 'PRODUTO ACABADO', 'nivel' => 'LINHA DE PRODUTO' }
            ], 'unidade' => 'PC', 'estoqueMaximo' => 0, 'estoqueMinimo' => 0, 'classificacao' => 'REVENDA', 'metodoControle' => 'ESTOCAVEL',
            'codigoNcm' => '6303.99.00', 'codigoSKU' => '1.4.1', 'comissao' => 0 }
            ])
          }

          let(:product_2_response) { double(:product_response, status: 200, payload: [
            { 'id' => 3, 'referenciaBase' => [4,1], 'listCustoReferencial' => [
              { 'entidade' => 3, 'precoCusto' => 1 }, { 'entidade' => 1, 'precoCusto' => 1 }, { 'entidade' => 2, 'precoCusto' => 1 }
            ], 'custoReferencial' => 1, 'preco' => 0, 'margemLucro' => 0, 'permiteVenda' => true, 'origem' => 0, 'descricao' => 'ROLO MONOCOMANDO PADRAO 38 BASE ESTREITA',
            'codigoSistema' => '1.4.1', 'mercadoriaBase' => true, 'descontoMaximo' => 0, 'dataCriacao' => '04-11-2015 10:49:56', 
            'dataAlteracao' => '19-10-2016 15:59:38', 'ativo' => false, 'codigoReferenciaBase' => '1.4.1', 'unidadeCasasDecimais' => 4,
            'peso' => 0, 'categorias' => [
              { 'id' => 4, 'nome' => 'ROLO', 'nivel' => 'GRUPO' }, { 'id' => 1, 'nome' => 'PRODUTO ACABADO', 'nivel' => 'LINHA DE PRODUTO' }
            ], 'unidade' => 'PC', 'estoqueMaximo' => 0, 'estoqueMinimo' => 0, 'classificacao' => 'REVENDA', 'metodoControle' => 'ESTOCAVEL',
            'codigoNcm' => '6303.99.00', 'codigoSKU' => '1.4.1', 'comissao' => 0 }
            ])
          }

          before do
            allow(products).to receive(:list).and_return(product_1_response, product_2_response)
          end

          it 'updates the products in the erp' do
            expect(products).to receive(:update).with(1, product_1_data)
            expect(products).to receive(:update).with(3, product_2_data)

            subject.perform_async(@order.id, payment_condition_id)
          end

          context 'with error' do
            let(:update_response) { double(:update_response, status: 400, payload: {
              'idRecurso' => '0', 'codigoMensagem' => 10, 'mensagem' => 'Produto sem descri\xC3\xA7\xC3\xA3o'
            }) }

            before do
              allow(products).to receive(:update).and_return(update_response)
              @time = Time.now

              allow(Time).to receive(:now).and_return(@time)
            end

            it 'sets the order as \'error\'' do
              subject.perform_async(@order.id, payment_condition_id)

              expect(@order.reload.error?).to eq(true)
            end

            it 'stores the error with date information' do
              subject.perform_async(@order.id, payment_condition_id)

              expect(@order.reload.processing_messages).to match_array(["#{Time.now.strftime('%d/%m/%Y - %H:%M')} - #{update_response.payload['codigoMensagem']}: #{update_response.payload['mensagem']}"])
            end
          end
        end

        context 'when the products do not exist in the erp' do
          it 'creates the products in the erp' do
            expect(products).to receive(:save).with(product_1_data)
            expect(products).to receive(:save).with(product_2_data)

            subject.perform_async(@order.id, payment_condition_id)
          end

          context 'with error' do
            let(:create_response) { double(:create_response, status: 400, payload: {
              'idRecurso' => '0', 'codigoMensagem' => 10, 'mensagem' => 'Produto sem descri\xC3\xA7\xC3\xA3o'
            }) }

            before do
              allow(products).to receive(:save).and_return(create_response)
              @time = Time.now

              allow(Time).to receive(:now).and_return(@time)
            end

            it 'sets the order as \'error\'' do
              subject.perform_async(@order.id, payment_condition_id)

              expect(@order.reload.error?).to eq(true)
            end

            it 'stores the error with date information' do
              subject.perform_async(@order.id, payment_condition_id)

              expect(@order.reload.processing_messages).to match_array(["#{Time.now.strftime('%d/%m/%Y - %H:%M')} - #{create_response.payload['codigoMensagem']}: #{create_response.payload['mensagem']}"])
            end
          end
        end
      end

      context 'for loose items information' do
        let(:product_data) { {
          'descricao' => "#{@order_loose_item.item_code} - #{@order_loose_item.item_description}", 'especificacao' => '',
          'metodoControle' => ErpWorker::CONTROL_METHOD, 'codigoNcm' => @item.ncm,
          'origem' => Item.csts[@item.cst], 'codigoInterno' => "#{@order_loose_item.item_code}",
          'unidade' => "#{@order_loose_item.unit.upcase}", 'classificacao' => ErpWorker::PRODUCT_CLASSIFICATION,
          'custoReferencial' => "#{@order_loose_item.value.to_d}", 'listCustoReferencial' => [
            {'entidade' => ErpWorker::ENTITY_ID, 'precoCusto' => "#{@order_loose_item.value.to_d}"}
          ], 'categorias' => @item.erp_categories.map{|k,v| {'nome' => v, 'nivel' => k} }
        } }
        
        before do
          @order.order_items.destroy_all

          allow(Varejonline::Entity::Commercial::Product).to receive(:new).and_return(product_data)
        end

        it 'searchers the item by its code' do
          expect(Varejonline::Searcher::Commercial::ProductSearcher).to receive(:new).with({
            codigoInterno: "#{@order_loose_item.item_code}", quantidade: 1
          }).and_return(@item_searcher)

          expect(products).to receive(:list).with(@item_searcher)

          subject.perform_async(@order.id, payment_condition_id)
        end

        context 'with search error' do
          let(:error_response) { double(:error_response, status: 400, payload: 'Error') }

          before do
            allow(products).to receive(:list).and_return(error_response)

            @time = Time.now

            allow(Time).to receive(:now).and_return(@time)
          end

          it 'sets the order as \'error\'' do
            subject.perform_async(@order.id, payment_condition_id)

            expect(@order.reload.error?).to eq(true)
          end

          it 'stores the error with date information' do
            subject.perform_async(@order.id, payment_condition_id)

            expect(@order.reload.processing_messages).to match_array(["#{Time.now.strftime('%d/%m/%Y - %H:%M')} - #{error_response.payload}"])
          end
        end

        context 'when the item exists in the erp' do
          let(:product_response) { double(:product_response, status: 200, payload: [
            { 'id' => 1, 'referenciaBase' => [4,1], 'listCustoReferencial' => [
              { 'entidade' => 3, 'precoCusto' => 1 }, { 'entidade' => 1, 'precoCusto' => 1 }, { 'entidade' => 2, 'precoCusto' => 1 }
            ], 'custoReferencial' => 1, 'preco' => 0, 'margemLucro' => 0, 'permiteVenda' => true, 'origem' => 0, 'descricao' => 'ROLO MONOCOMANDO PADRAO 38 BASE ESTREITA',
            'codigoSistema' => '1.4.1', 'mercadoriaBase' => true, 'descontoMaximo' => 0, 'dataCriacao' => '04-11-2015 10:49:56', 
            'dataAlteracao' => '19-10-2016 15:59:38', 'ativo' => false, 'codigoReferenciaBase' => '1.4.1', 'unidadeCasasDecimais' => 4,
            'peso' => 0, 'categorias' => [
              { 'id' => 4, 'nome' => 'ROLO', 'nivel' => 'GRUPO' }, { 'id' => 1, 'nome' => 'PRODUTO ACABADO', 'nivel' => 'LINHA DE PRODUTO' }
            ], 'unidade' => 'PC', 'estoqueMaximo' => 0, 'estoqueMinimo' => 0, 'classificacao' => 'REVENDA', 'metodoControle' => 'ESTOCAVEL',
            'codigoNcm' => '6303.99.00', 'codigoSKU' => '1.4.1', 'comissao' => 0 }
            ])
          }

          before do
            allow(products).to receive(:list).and_return(product_response)
          end

          it 'updates the item in the erp' do
            expect(products).to receive(:update).with(1, product_data).at_least(:once)

            subject.perform_async(@order.id, payment_condition_id)
          end

          context 'with error' do
            let(:update_response) { double(:update_response, status: 400, payload: {
              'idRecurso' => '0', 'codigoMensagem' => 10, 'mensagem' => 'Produto sem descri\xC3\xA7\xC3\xA3o'
            }) }

            before do
              allow(products).to receive(:update).and_return(update_response)
              @time = Time.now

              allow(Time).to receive(:now).and_return(@time)
            end

            it 'sets the order as \'error\'' do
              subject.perform_async(@order.id, payment_condition_id)

              expect(@order.reload.error?).to eq(true)
            end

            it 'stores the error with date information' do
              subject.perform_async(@order.id, payment_condition_id)

              expect(@order.reload.processing_messages).to match_array(["#{Time.now.strftime('%d/%m/%Y - %H:%M')} - #{update_response.payload['codigoMensagem']}: #{update_response.payload['mensagem']}"])
            end
          end
        end

        context 'when the item does not exist in the erp' do
          it 'creates the loose items in the erp' do
            expect(products).to receive(:save).with(product_data).at_least(:once)

            subject.perform_async(@order.id, payment_condition_id)
          end

          context 'with error' do
            let(:create_response) { double(:create_response, status: 400, payload: {
              'idRecurso' => '0', 'codigoMensagem' => 10, 'mensagem' => 'Produto sem descri\xC3\xA7\xC3\xA3o'
            }) }

            before do
              allow(products).to receive(:save).and_return(create_response)
              @time = Time.now

              allow(Time).to receive(:now).and_return(@time)
            end

            it 'sets the order as \'error\'' do
              subject.perform_async(@order.id, payment_condition_id)

              expect(@order.reload.error?).to eq(true)
            end

            it 'stores the error with date information' do
              subject.perform_async(@order.id, payment_condition_id)

              expect(@order.reload.processing_messages).to match_array(["#{Time.now.strftime('%d/%m/%Y - %H:%M')} - #{create_response.payload['codigoMensagem']}: #{create_response.payload['mensagem']}"])
            end
          end
        end
      end

      context 'order creation' do
        let(:order_data) { {
          'numeroPedidoCliente' => "#{@order.id}", 'data' => "#{@order.date.strftime('%d-%m-%Y')}", 'horario' => "#{@time.strftime('%H:%M:%S')}",
          'idEntidade' => ErpWorker::ENTITY_ID, 'idPlanoPagamento' => payment_condition_id, 'idRepresentante' => "#{@order.user.erp_id}",
          'idTerceiro' => 28, 'observacao' => "#{@order.observation}", 'valorFrete' => 0, 'valorOutros' => 0, 'valorSeguro' => 0,
          'vendaConsumidorFinal' => true, 'itens' => [
            {'idProduto' => 10, 'quantidade' => 1, 'valorUnitario' => @order_item_1.total.to_d, 'valorDesconto' => @order_item_1.total_discount.to_d},
            {'idProduto' => 15, 'quantidade' => 1, 'valorUnitario' => @order_item_2.total.to_d, 'valorDesconto' => @order_item_2.total_discount.to_d},
            {'idProduto' => 25, 'quantidade' => 1, 'valorUnitario' => @order_item_3.total.to_d, 'valorDesconto' => @order_item_3.total_discount.to_d},
            {'idProduto' => 25, 'quantidade' => 1, 'valorUnitario' => @order_item_4.total.to_d, 'valorDesconto' => @order_item_4.total_discount.to_d},
            {'idProduto' => 25, 'quantidade' => 1, 'valorUnitario' => @order_item_5.total.to_d, 'valorDesconto' => @order_item_5.total_discount.to_d},
            {'idProduto' => 25, 'quantidade' => 1, 'valorUnitario' => @order_item_6.total.to_d, 'valorDesconto' => @order_item_6.total_discount.to_d},
            {'idProduto' => 25, 'quantidade' => 1, 'valorUnitario' => @order_item_7.total.to_d, 'valorDesconto' => @order_item_7.total_discount.to_d},
            {'idProduto' => 25, 'quantidade' => 1, 'valorUnitario' => @order_item_8.total.to_d, 'valorDesconto' => @order_item_8.total_discount.to_d},
            {'idProduto' => 25, 'quantidade' => @order_loose_item.quantity.to_d, 'valorUnitario' => @order_loose_item.value.to_d, 'valorDesconto' => @order_loose_item.total_discount.to_d}
          ]
        } }

        before do
          @time = Time.current

          allow(Time).to receive(:current).and_return(@time)
        end

        context 'without order general discount' do
          it 'creates the order in the erp with the items values and discounts' do
            expect(orders).to receive(:save).with(order_data)

            subject.perform_async(@order.id, payment_condition_id)
          end
        end

        context 'with order general discount' do
          let(:order_data) { {
            'numeroPedidoCliente' => "#{@order.id}", 'data' => "#{@order.date.strftime('%d-%m-%Y')}", 'horario' => "#{@time.strftime('%H:%M:%S')}",
            'idEntidade' => ErpWorker::ENTITY_ID, 'idPlanoPagamento' => payment_condition_id, 'idRepresentante' => "#{@order.user.erp_id}",
            'idTerceiro' => 28, 'observacao' => "#{@order.observation}", 'valorFrete' => 0, 'valorOutros' => 0, 'valorSeguro' => 0,
            'vendaConsumidorFinal' => true, 'itens' => [
              {'idProduto' => 10, 'quantidade' => 1, 'valorUnitario' => @order_item_1.total.to_d, 'valorDesconto' => BigDecimal.new('53.52')},
              {'idProduto' => 15, 'quantidade' => 1, 'valorUnitario' => @order_item_2.total.to_d, 'valorDesconto' => BigDecimal.new('73.15')},
              {'idProduto' => 25, 'quantidade' => 1, 'valorUnitario' => @order_item_3.total.to_d, 'valorDesconto' => BigDecimal.new('73.15')},
              {'idProduto' => 25, 'quantidade' => 1, 'valorUnitario' => @order_item_4.total.to_d, 'valorDesconto' => BigDecimal.new('73.15')},
              {'idProduto' => 25, 'quantidade' => 1, 'valorUnitario' => @order_item_5.total.to_d, 'valorDesconto' => BigDecimal.new('73.15')},
              {'idProduto' => 25, 'quantidade' => 1, 'valorUnitario' => @order_item_6.total.to_d, 'valorDesconto' => BigDecimal.new('73.15')},
              {'idProduto' => 25, 'quantidade' => 1, 'valorUnitario' => @order_item_7.total.to_d, 'valorDesconto' => BigDecimal.new('20.38')},
              {'idProduto' => 25, 'quantidade' => 1, 'valorUnitario' => @order_item_8.total.to_d, 'valorDesconto' => BigDecimal.new('27.67')},
              {'idProduto' => 25, 'quantidade' => @order_loose_item.quantity.to_d, 'valorUnitario' => @order_loose_item.value.to_d,
                'valorDesconto' => BigDecimal.new('96.25')}
            ]
          } }

          before do
            @order.update!(discount: 5)
          end

          it 'creates the order in the erp with the items values and discounts applying the order discount to the items discounts' do
            expect(orders).to receive(:save).with(order_data)

            subject.perform_async(@order.id, payment_condition_id)
          end
        end

        context 'with order special discount' do
          let(:order_data) { {
            'numeroPedidoCliente' => "#{@order.id}", 'data' => "#{@order.date.strftime('%d-%m-%Y')}", 'horario' => "#{@time.strftime('%H:%M:%S')}",
            'idEntidade' => ErpWorker::ENTITY_ID, 'idPlanoPagamento' => payment_condition_id, 'idRepresentante' => "#{@order.user.erp_id}",
            'idTerceiro' => 28, 'observacao' => "#{@order.observation}", 'valorFrete' => 0, 'valorOutros' => 0, 'valorSeguro' => 0,
            'vendaConsumidorFinal' => true, 'itens' => [
              {'idProduto' => 10, 'quantidade' => 1, 'valorUnitario' => BigDecimal.new('284.03'), 'valorDesconto' => 0},
              {'idProduto' => 15, 'quantidade' => 1, 'valorUnitario' => BigDecimal.new('388.21'), 'valorDesconto' => 0},
              {'idProduto' => 25, 'quantidade' => 1, 'valorUnitario' => BigDecimal.new('388.21'), 'valorDesconto' => 0},
              {'idProduto' => 25, 'quantidade' => 1, 'valorUnitario' => BigDecimal.new('388.21'), 'valorDesconto' => 0},
              {'idProduto' => 25, 'quantidade' => 1, 'valorUnitario' => BigDecimal.new('388.21'), 'valorDesconto' => 0},
              {'idProduto' => 25, 'quantidade' => 1, 'valorUnitario' => BigDecimal.new('388.21'), 'valorDesconto' => 0},
              {'idProduto' => 25, 'quantidade' => 1, 'valorUnitario' => BigDecimal.new('169.84'), 'valorDesconto' => 0},
              {'idProduto' => 25, 'quantidade' => 1, 'valorUnitario' => BigDecimal.new('230.50'), 'valorDesconto' => 0},
              {'idProduto' => 25, 'quantidade' => @order_loose_item.quantity.to_d, 'valorUnitario' => BigDecimal.new('363.38'),
                'valorDesconto' => 0}
            ]
          } }

          before do
            @order.update!(discount: BigDecimal.new('5'), special_discount: BigDecimal.new('10'))
          end

          it 'creates the order in the erp with the result of item values, discounts, order discounts and special discount on the item unit value' do
            expect(orders).to receive(:save).with(order_data)

            subject.perform_async(@order.id, payment_condition_id)
          end
        end

        context 'with success' do
          it 'sets the erp id in the order' do
            subject.perform_async(@order.id, payment_condition_id)

            expect(@order.reload.erp_id).to eq(10)
          end

          it 'marks the order as \'sent\'' do
            subject.perform_async(@order.id, payment_condition_id)

            expect(@order.reload.sent?).to eq(true)
          end
        end

        context 'with error' do
          let(:error_response) { double(:error_response, status: 400, payload: 'Error') }

          before do
            allow(orders).to receive(:save).and_return(error_response)
          end

          it 'sets the order as \'error\'' do
            subject.perform_async(@order.id, payment_condition_id)

            expect(@order.reload.error?).to eq(true)
          end

          it 'stores the error with date information' do
            subject.perform_async(@order.id, payment_condition_id)

            expect(@order.reload.processing_messages).to match_array(["#{Time.now.strftime('%d/%m/%Y - %H:%M')} - #{error_response.payload}"])
          end
        end
      end
    end

    context 'when the order is \'sending\'' do
      before do
        @order.update(status: Order.statuses[:sending])
      end

      it 'does nothing' do
        subject.perform_async(@order.id, payment_condition_id)
      end
    end

    context 'when the order is \'sent\'' do
      before do
        @order.update(status: Order.statuses[:sent], erp_id: 10)
      end

      it 'does nothing' do
        subject.perform_async(@order.id, payment_condition_id)
      end
    end
  end
end
