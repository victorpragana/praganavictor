# == Schema Information
#
# Table name: payment_conditions
#
#  id                      :integer          not null, primary key
#  installments            :integer          not null
#  additional_tax          :decimal(5, 2)    default(0.0)
#  antecipation_tax        :decimal(5, 2)    default(0.0)
#  credit_antecipation_tax :decimal(5, 2)    default(0.0)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  type                    :integer          default(0), not null
#  credit_tax              :decimal(5, 2)    default(0.0)
#

require 'rails_helper'

RSpec.describe PaymentConditionsController, type: :controller do
  before do
    @factory = create(:factory, name: 'F1')

    create(:administrator, username: 'F1').tap do |administrator|
      administrator.update!(selected_workplace: create(:user_workplace, workplace: @factory, user: administrator))
    end

    @distributor = create(:distributor, name: 'D1')
    @store       = create(:store, distributor: @distributor, name: 'C1')

    sign_in :administrator, @factory.users.first
  end

  describe 'third party access control' do
    before do
      workplace = [@distributor, @store].sample

      create(:administrator, username: 'E1').tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: workplace, user: administrator))
      end

      sign_in :administrator, workplace.users.first
    end

    it 'renders unauthorized if the user is not the factory' do
      xhr :post, :create

      expect(response).to have_http_status(:unauthorized)
    end
  end
  
  describe 'GET index' do
    render_views

    before do
      @payment_condition1 = create(:payment_condition, installments: 3)
      @payment_condition2 = create(:payment_condition, installments: 1)
      @payment_condition3 = create(:payment_condition, installments: 5)
      @payment_condition4 = create(:payment_condition, installments: 1, type: :bank_slip)
    end

    it 'returns OK' do
      xhr :get, :index

      expect(response).to have_http_status(:ok)
    end

    context 'with datatable search parameters' do
      let(:params) { { 'draw'=>'1', 'columns'=>{'0'=>{'data'=>'0', 'name'=>'', 'searchable'=>'false', 'orderable'=>'false', 
        'search'=>{'value'=>'', 'regex'=>'false'}}, '1'=>{'data'=>'1', 'name'=>'id', 'searchable'=>'true', 'orderable'=>'true', 
        'search'=>{'value'=>'', 'regex'=>'false'}}, '2'=>{'data'=>'2', 'name'=>'', 'searchable'=>'true', 'orderable'=>'true', 
        'search'=>{'value'=>'', 'regex'=>'false'}}}, 'order'=>{'0'=>{'column'=>'1', 'dir'=>'asc'}}, 'start'=>'0', 'length'=>'100', 
        'search'=>{'value'=>'', 'regex'=>'false'}, '_'=>'1456860760040' } }

      it 'initializes the payment condition datatable' do
        expect(PaymentConditionDatatable).to receive(:new)

        xhr :get, :index, params
      end
    end

    context 'without search parameters' do
      it 'lists the payment conditions' do
        xhr :get, :index

        expect(json['payment_conditions'].size).to eq(4)
        expect(json['payment_conditions'][0]['id']).to eq(@payment_condition2.id)
        expect(json['payment_conditions'][0]['type']).to eq(PaymentCondition.types[@payment_condition2.type])
        expect(json['payment_conditions'][0]['installments']).to eq(@payment_condition2.installments)
        expect(json['payment_conditions'][0]['additional_tax']).to eq(@payment_condition2.additional_tax.to_f)
        expect(json['payment_conditions'][0]['credit_tax']).to eq(@payment_condition2.credit_tax.to_f)
        expect(json['payment_conditions'][1]['id']).to eq(@payment_condition1.id)
        expect(json['payment_conditions'][1]['type']).to eq(PaymentCondition.types[@payment_condition1.type])
        expect(json['payment_conditions'][1]['installments']).to eq(@payment_condition1.installments)
        expect(json['payment_conditions'][1]['additional_tax']).to eq(@payment_condition1.additional_tax.to_f)
        expect(json['payment_conditions'][1]['credit_tax']).to eq(@payment_condition1.credit_tax.to_f)
        expect(json['payment_conditions'][2]['id']).to eq(@payment_condition3.id)
        expect(json['payment_conditions'][2]['type']).to eq(PaymentCondition.types[@payment_condition3.type])
        expect(json['payment_conditions'][2]['installments']).to eq(@payment_condition3.installments)
        expect(json['payment_conditions'][2]['additional_tax']).to eq(@payment_condition3.additional_tax.to_f)
        expect(json['payment_conditions'][2]['credit_tax']).to eq(@payment_condition3.credit_tax.to_f)
        expect(json['payment_conditions'][3]['id']).to eq(@payment_condition4.id)
        expect(json['payment_conditions'][3]['type']).to eq(PaymentCondition.types[@payment_condition4.type])
        expect(json['payment_conditions'][3]['installments']).to eq(@payment_condition4.installments)
        expect(json['payment_conditions'][3]['additional_tax']).to eq(@payment_condition4.additional_tax.to_f)
        expect(json['payment_conditions'][3]['credit_tax']).to eq(@payment_condition4.credit_tax.to_f)
      end
    end
  end

  describe 'POST create' do
    render_views

    context 'with invalid params' do
      let(:parameters) { { 'installments' => '' } }

      it 'does not create a new payment condition' do
        expect {
          xhr :post, :create, parameters
        }.to change(PaymentCondition, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :create, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) { { 'installments' => '5', 'additional_tax' => '12.22', 'credit_tax' => '1.22', 'antecipation_tax' => '9.88', 'credit_antecipation_tax' => '25.69', 'type' => '1' } }

      it 'initializes the payment condition service' do
        expect(PaymentConditionService).to receive(:new).with(parameters).and_call_original

        xhr :post, :create, parameters
      end

      it 'creates the payment condition' do
        expect_any_instance_of(PaymentConditionService).to receive(:create).and_call_original
        
        xhr :post, :create, parameters
      end

      it 'returns created status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:created)
      end
    end
  end

  describe 'GET show' do
    render_views

    before do
      @payment_condition = create(:payment_condition)
    end

    it 'returns ok status' do
      xhr :get, :show, id: @payment_condition.id

      expect(response).to have_http_status(:ok)
    end

    it 'returns the payment condition data' do
      xhr :get, :show, id: @payment_condition.id

      expect(json['payment_condition']['id']).to eq(@payment_condition.id)
      expect(json['payment_condition']['type']).to eq(PaymentCondition.types[@payment_condition.type])
      expect(json['payment_condition']['installments']).to eq(@payment_condition.installments)
      expect(json['payment_condition']['additional_tax']).to eq(@payment_condition.additional_tax.to_f)
      expect(json['payment_condition']['credit_tax']).to eq(@payment_condition.credit_tax.to_f)
      expect(json["payment_condition"]["antecipation_tax"]).to eq(@payment_condition.antecipation_tax.to_f)
      expect(json["payment_condition"]["credit_antecipation_tax"]).to eq(@payment_condition.credit_antecipation_tax.to_f)
    end
  end

  describe 'PUT update' do
    render_views

    before do
      @payment_condition = create(:payment_condition)
    end

    context 'with invalid params' do
      let(:parameters) { { 'installments' => '' }.merge({ 'id' => "#{@payment_condition.id}" }) }

      it 'does not create a new payment condition' do
        expect {
          xhr :put, :update, parameters
        }.to change(PaymentCondition, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :put, :update, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :put, :update, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) { { 'installments' => '5', 'additional_tax' => '12.22', 'credit_tax' => '1.22', 'antecipation_tax' => '9.88', 'credit_antecipation_tax' => '25.69', 'type' => '1' } }

      it 'initializes the payment condition service' do
        expect(PaymentConditionService).to receive(:new).with(parameters).and_call_original

        xhr :put, :update, parameters.merge({ 'id' => "#{@payment_condition.id}" })
      end

      it 'updates the payment condition' do
        expect_any_instance_of(PaymentConditionService).to receive(:update).with(@payment_condition.id.to_s).and_call_original
        
        xhr :put, :update, parameters.merge({ 'id' => "#{@payment_condition.id}" })
      end

      it 'returns ok status' do
        xhr :put, :update, parameters.merge({ 'id' => "#{@payment_condition.id}" })
        
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'DELETE destroy' do
    before do
      @payment_condition = create(:payment_condition)
    end

    context 'with success' do
      it 'destroys the payment condition' do
        expect {
          xhr :delete, :destroy, { id: @payment_condition.id }
        }.to change(PaymentCondition, :count).by(-1)
      end

      it 'returns no content status' do
        xhr :delete, :destroy, { id: @payment_condition.id }

        expect(response).to have_http_status(:no_content)
      end
    end

    context 'with error' do
      before do
        allow(@payment_condition).to receive(:destroy).and_return(false)
        allow(@payment_condition).to receive(:errors).and_return(double(:errors, full_messages: ['foo']))

        allow(PaymentCondition).to receive(:find).and_return(@payment_condition)
      end

      it 'does not destroy the payment condition' do
        expect {
          xhr :delete, :destroy, { id: @payment_condition.id }
        }.to_not change(PaymentCondition, :count)
      end

      it 'returns the bad request status' do
        xhr :delete, :destroy, { id: @payment_condition.id }

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the errors' do
        xhr :delete, :destroy, { id: @payment_condition.id }
        
        expect(json['errors']).to_not be_empty
      end
    end
  end
end
