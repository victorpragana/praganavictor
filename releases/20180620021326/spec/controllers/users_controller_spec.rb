# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  provider               :string           default("email"), not null
#  uid                    :string           default(""), not null
#  email                  :string           default(""), not null
#  username               :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  name                   :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  tokens                 :json
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  type                   :string
#  maximum_discount       :decimal(5, 2)    default(0.0)
#  erp_id                 :integer
#  selected_workplace_id  :integer
#

require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  describe 'GET index' do
    render_views

    before do
      @factory = FactoryGirl.create(:factory, name: 'F1')
      @distributor = FactoryGirl.create(:distributor, name: 'D1')
      @store = FactoryGirl.create(:store, name: 'S1', distributor: @distributor)

      @user1 = FactoryGirl.create(:operator, name: 'aa', username: 'aa', email: 'aa@user.com', uid: 'aa@user.com').tap do |operator|
        operator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @factory, user: operator))
      end

      @user2 = FactoryGirl.create(:manager, name: 'bb', username: 'bb', email: 'bb@user.com', uid: 'bb@user.com').tap do |manager|
        manager.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @distributor, user: manager))
      end
      FactoryGirl.create(:user_workplace, workplace: @store, user: @user2)

      @user3 = FactoryGirl.create(:seller, name: 'cc', username: 'cc', email: 'cc@user.com', uid: 'cc@user.com').tap do |seller|
        seller.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @store, user: seller))
      end
    end

    context 'within the factory' do
      before do
        @administrator = FactoryGirl.create(:administrator, name: 'admin', username: 'admin').tap do |administrator|
          administrator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @factory, user: administrator))
        end
        
        sign_in :administrator, @administrator
      end

      it 'returns OK' do
        xhr :get, :index

        expect(response).to have_http_status(:ok)
      end

      it 'lists all users' do
        xhr :get, :index

        expect(json['users'].size).to eq(4)
        expect(json['users'][0]['id']).to eq(@user1.id)
        expect(json['users'][0]['name']).to eq(@user1.name)
        expect(json['users'][0]['username']).to eq(@user1.username)
        expect(json['users'][0]['email']).to eq(@user1.email)
        expect(json['users'][0]['type']).to eq(@user1.type)
        expect(json['users'][0]['workplaces'][0]['id']).to eq(@user1.workplaces[0].id)
        expect(json['users'][0]['workplaces'][0]['workplace_id']).to eq(@user1.workplaces[0].workplace.id)
        expect(json['users'][0]['workplaces'][0]['workplace_name']).to eq(@user1.workplaces[0].workplace.name)
        expect(json['users'][0]['workplaces'][0]['workplace_type']).to eq(@user1.workplaces[0].workplace.actable_type)
        expect(json['users'][1]['id']).to eq(@user2.id)
        expect(json['users'][1]['name']).to eq(@user2.name)
        expect(json['users'][1]['username']).to eq(@user2.username)
        expect(json['users'][1]['email']).to eq(@user2.email)
        expect(json['users'][1]['type']).to eq(@user2.type)
        expect(json['users'][1]['workplaces'][0]['id']).to eq(@user2.workplaces[0].id)
        expect(json['users'][1]['workplaces'][0]['workplace_id']).to eq(@user2.workplaces[0].workplace.id)
        expect(json['users'][1]['workplaces'][0]['workplace_name']).to eq(@user2.workplaces[0].workplace.name)
        expect(json['users'][1]['workplaces'][0]['workplace_type']).to eq(@user2.workplaces[0].workplace.actable_type)
        expect(json['users'][2]['id']).to eq(@user3.id)
        expect(json['users'][2]['name']).to eq(@user3.name)
        expect(json['users'][2]['username']).to eq(@user3.username)
        expect(json['users'][2]['email']).to eq(@user3.email)
        expect(json['users'][2]['type']).to eq(@user3.type)
        expect(json['users'][2]['workplaces'][0]['id']).to eq(@user3.workplaces[0].id)
        expect(json['users'][2]['workplaces'][0]['workplace_id']).to eq(@user3.workplaces[0].workplace.id)
        expect(json['users'][2]['workplaces'][0]['workplace_name']).to eq(@user3.workplaces[0].workplace.name)
        expect(json['users'][2]['workplaces'][0]['workplace_type']).to eq(@user3.workplaces[0].workplace.actable_type)
        expect(json['users'][3]['id']).to eq(@administrator.id)
        expect(json['users'][3]['name']).to eq(@administrator.name)
        expect(json['users'][3]['username']).to eq(@administrator.username)
        expect(json['users'][3]['email']).to eq(@administrator.email)
        expect(json['users'][3]['type']).to eq(@administrator.type)
        expect(json['users'][3]['workplaces'][0]['id']).to eq(@administrator.workplaces[0].id)
        expect(json['users'][3]['workplaces'][0]['workplace_id']).to eq(@administrator.workplaces[0].workplace.id)
        expect(json['users'][3]['workplaces'][0]['workplace_name']).to eq(@administrator.workplaces[0].workplace.name)
        expect(json['users'][3]['workplaces'][0]['workplace_type']).to eq(@administrator.workplaces[0].workplace.actable_type)
      end
    end

    context 'within the distributor' do
      before do
        @administrator = FactoryGirl.create(:administrator, name: 'admin', username: 'admin').tap do |administrator|
          administrator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @distributor, user: administrator))
        end
        
        sign_in :administrator, @administrator
      end

      it 'returns OK' do
        xhr :get, :index

        expect(response).to have_http_status(:ok)
      end

      it 'lists the users from the distributor and the stores that are owned by the it' do
        xhr :get, :index

        expect(json['users'].size).to eq(3)
        expect(json['users'][0]['id']).to eq(@user2.id)
        expect(json['users'][0]['name']).to eq(@user2.name)
        expect(json['users'][0]['username']).to eq(@user2.username)
        expect(json['users'][0]['email']).to eq(@user2.email)
        expect(json['users'][0]['type']).to eq(@user2.type)
        expect(json['users'][0]['workplaces'][0]['id']).to eq(@user2.workplaces[0].id)
        expect(json['users'][0]['workplaces'][0]['workplace_id']).to eq(@user2.workplaces[0].workplace.id)
        expect(json['users'][0]['workplaces'][0]['workplace_name']).to eq(@user2.workplaces[0].workplace.name)
        expect(json['users'][0]['workplaces'][0]['workplace_type']).to eq(@user2.workplaces[0].workplace.actable_type)
        expect(json['users'][1]['id']).to eq(@user3.id)
        expect(json['users'][1]['name']).to eq(@user3.name)
        expect(json['users'][1]['username']).to eq(@user3.username)
        expect(json['users'][1]['email']).to eq(@user3.email)
        expect(json['users'][1]['type']).to eq(@user3.type)
        expect(json['users'][1]['workplaces'][0]['id']).to eq(@user3.workplaces[0].id)
        expect(json['users'][1]['workplaces'][0]['workplace_id']).to eq(@user3.workplaces[0].workplace.id)
        expect(json['users'][1]['workplaces'][0]['workplace_name']).to eq(@user3.workplaces[0].workplace.name)
        expect(json['users'][1]['workplaces'][0]['workplace_type']).to eq(@user3.workplaces[0].workplace.actable_type)
        expect(json['users'][2]['id']).to eq(@administrator.id)
        expect(json['users'][2]['name']).to eq(@administrator.name)
        expect(json['users'][2]['username']).to eq(@administrator.username)
        expect(json['users'][2]['email']).to eq(@administrator.email)
        expect(json['users'][2]['type']).to eq(@administrator.type)
        expect(json['users'][2]['workplaces'][0]['id']).to eq(@administrator.workplaces[0].id)
        expect(json['users'][2]['workplaces'][0]['workplace_id']).to eq(@administrator.workplaces[0].workplace.id)
        expect(json['users'][2]['workplaces'][0]['workplace_name']).to eq(@administrator.workplaces[0].workplace.name)
        expect(json['users'][2]['workplaces'][0]['workplace_type']).to eq(@administrator.workplaces[0].workplace.actable_type)
      end
    end

    context 'within the store' do
      before do
        @administrator = FactoryGirl.create(:administrator, name: 'admin', username: 'admin').tap do |administrator|
          administrator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @store, user: administrator))
        end
        
        sign_in :administrator, @administrator
      end

      it 'returns OK' do
        xhr :get, :index

        expect(response).to have_http_status(:ok)
      end

      it 'lists the users from the store only' do
        xhr :get, :index

        expect(json['users'].size).to eq(2)
        expect(json['users'][0]['id']).to eq(@user3.id)
        expect(json['users'][0]['name']).to eq(@user3.name)
        expect(json['users'][0]['username']).to eq(@user3.username)
        expect(json['users'][0]['email']).to eq(@user3.email)
        expect(json['users'][0]['type']).to eq(@user3.type)
        expect(json['users'][0]['workplaces'][0]['id']).to eq(@user3.workplaces[0].id)
        expect(json['users'][0]['workplaces'][0]['workplace_id']).to eq(@user3.workplaces[0].workplace.id)
        expect(json['users'][0]['workplaces'][0]['workplace_name']).to eq(@user3.workplaces[0].workplace.name)
        expect(json['users'][0]['workplaces'][0]['workplace_type']).to eq(@user3.workplaces[0].workplace.actable_type)
        expect(json['users'][1]['id']).to eq(@administrator.id)
        expect(json['users'][1]['name']).to eq(@administrator.name)
        expect(json['users'][1]['username']).to eq(@administrator.username)
        expect(json['users'][1]['email']).to eq(@administrator.email)
        expect(json['users'][1]['type']).to eq(@administrator.type)
        expect(json['users'][1]['workplaces'][0]['id']).to eq(@administrator.workplaces[0].id)
        expect(json['users'][1]['workplaces'][0]['workplace_id']).to eq(@administrator.workplaces[0].workplace.id)
        expect(json['users'][1]['workplaces'][0]['workplace_name']).to eq(@administrator.workplaces[0].workplace.name)
        expect(json['users'][1]['workplaces'][0]['workplace_type']).to eq(@administrator.workplaces[0].workplace.actable_type)
      end
    end
  end

  describe 'POST create' do
    render_views

    before do
      @factory = FactoryGirl.create(:factory, name: 'F1')
      @distributor = FactoryGirl.create(:distributor, name: 'D1')
      @store = FactoryGirl.create(:store, name: 'S1', distributor: @distributor)

      @workplace = [@factory, @distributor, @store].sample

      @administrator = FactoryGirl.create(:administrator, name: 'admin', username: 'admin').tap do |administrator|
        administrator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @workplace, user: administrator))
      end
      
      sign_in :administrator, @administrator
    end

    context 'with invalid params' do
      let(:parameters) {{'name' => '', 'username' => 'foo', 'email' => 'bar@foo.com', 'maximum_discount' => '12.58',
        'password' => 'abc12311', 'password_confirmation' => 'abc12311', 'type' => 'Administrator', 'erp_id' => '20',
        'workplaces' =>
          [
            'workplace_id' => "#{@workplace.id}", 'workplace_type' => "#{@workplace.actable_type}"
          ]
      }}

      it 'does not create a new user' do
        expect {
          xhr :post, :create, parameters
        }.to change(User, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :create, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) {{'name' => 'Foo', 'username' => 'foo', 'email' => 'bar@foo.com', 'maximum_discount' => '12.58',
        'password' => 'abc12311', 'password_confirmation' => 'abc12311', 'type' => 'Manager', 'erp_id' => '20',
        'workplaces' =>
          [
            'workplace_id' => "#{@workplace.id}", 'workplace_type' => "#{@workplace.actable_type}"
          ]
      }}

      it 'initializes the user service' do
        expect(UserService).to receive(:new).with(parameters).and_call_original

        xhr :post, :create, parameters
      end

      it 'creates the user' do
        expect_any_instance_of(UserService).to receive(:create).and_call_original
        
        xhr :post, :create, parameters
      end

      it 'returns created status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:created)
      end
    end
  end

  describe 'GET show' do
    render_views

    before do
      @factory = FactoryGirl.create(:factory, name: 'F1')
      @distributor = FactoryGirl.create(:distributor, name: 'D1')
      @store = FactoryGirl.create(:store, name: 'S1', distributor: @distributor)

      @workplace = [@factory, @distributor, @store].sample

      @administrator = FactoryGirl.create(:administrator, name: 'admin', username: 'admin').tap do |administrator|
        administrator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @workplace, user: administrator))
      end
      
      sign_in :administrator, @administrator

      @user = FactoryGirl.create(:user, name: 'bb').tap do |user|
        user.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @workplace, user: user))
      end
    end

    it 'returns ok status' do
      xhr :get, :show, id: @user.id

      expect(response).to have_http_status(:ok)
    end

    it 'returns the user data' do
      xhr :get, :show, id: @user.id

      expect(json['user']['id']).to eq(@user.id)
      expect(json['user']['name']).to eq(@user.name)
      expect(json['user']['username']).to eq(@user.username)
      expect(json['user']['email']).to eq(@user.email)
      expect(json['user']['type']).to eq(@user.type)
      expect(json['user']['workplace']['id']).to eq(@user.workplace.id)
      expect(json['user']['workplace']['type']).to eq(@user.workplace.actable_type)
      expect(json['user']['workplace']['name']).to eq(@user.workplace.name)
      expect(json['user']['workplace']['logo_url']).to eq(@user.workplace.logo.url)
      expect(json['user']['workplaces'][0]['id']).to eq(@user.workplaces[0].id)
      expect(json['user']['workplaces'][0]['workplace_id']).to eq(@user.workplaces[0].workplace.id)
      expect(json['user']['workplaces'][0]['workplace_name']).to eq(@user.workplaces[0].workplace.name)
      expect(json['user']['workplaces'][0]['workplace_type']).to eq(@user.workplaces[0].workplace.actable_type)
      expect(json['user']['maximum_discount']).to eq(@user.maximum_discount.to_f)
      expect(json['user']['erp_id']).to eq(@user.erp_id)
    end
  end

  describe 'PUT update' do
    render_views

    before do
      @factory = FactoryGirl.create(:factory, name: 'F1')
      @distributor = FactoryGirl.create(:distributor, name: 'D1')
      @store = FactoryGirl.create(:store, name: 'S1', distributor: @distributor)

      @workplace = [@factory, @distributor, @store].sample

      @administrator = FactoryGirl.create(:administrator, name: 'admin', username: 'admin').tap do |administrator|
        administrator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @workplace, user: administrator))
      end
      
      sign_in :administrator, @administrator

      @user = FactoryGirl.create(:administrator, name: 'bb')
    end

    context 'with invalid params' do
      let(:parameters) {{'name' => '', 'username' => 'foo', 'email' => 'bar@foo.com', 'maximum_discount' => '12.58',
        'password' => 'abc12311', 'password_confirmation' => 'abc12311', 'type' => 'Administrator', 'erp_id' => '20',
        'workplaces' =>
          [
            'workplace_id' => "#{@workplace.id}", 'workplace_type' => "#{@workplace.actable_type}"
          ]
        }.merge({'id' => "#{@user.id}"})
      }

      it 'does not create a new user' do
        expect {
          xhr :put, :update, parameters
        }.to change(User, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :put, :update, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :put, :update, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) {{'name' => 'Foo', 'username' => 'foo', 'email' => 'bar@foo.com', 'maximum_discount' => '12.58', 
        'password' => 'abc12311', 'password_confirmation' => 'abc12311', 'type' => 'Manager', 'erp_id' => '20',
        'workplaces' =>
          [
            'workplace_id' => "#{@workplace.id}", 'workplace_type' => "#{@workplace.actable_type}"
          ]
      }}

      it 'initializes the user service' do
        expect(UserService).to receive(:new).with(parameters).and_call_original

        xhr :put, :update, parameters.merge({'id' => "#{@user.id}"})
      end

      it 'updates the user' do
        expect_any_instance_of(UserService).to receive(:update).with(@user.id.to_s).and_call_original
        
        xhr :put, :update, parameters.merge({'id' => "#{@user.id}"})
      end

      it 'returns ok status' do
        xhr :put, :update, parameters.merge({'id' => "#{@user.id}"})
        
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'DELETE destroy' do
    before do
      @factory = FactoryGirl.create(:factory, name: 'F1')
      @distributor = FactoryGirl.create(:distributor, name: 'D1')
      @store = FactoryGirl.create(:store, name: 'S1', distributor: @distributor)

      @workplace = [@factory, @distributor, @store].sample

      @administrator = FactoryGirl.create(:administrator, name: 'admin', username: 'admin').tap do |administrator|
        administrator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @workplace, user: administrator))
      end
      
      sign_in :administrator, @administrator

      @user = FactoryGirl.create(:user, name: 'bb')
    end

    context 'with success' do
      it 'destroys the user' do
        expect {
          xhr :delete, :destroy, {id: @user.id}
        }.to change(User, :count).by(-1)
      end

      it 'returns no content status' do
        xhr :delete, :destroy, {id: @user.id}

        expect(response).to have_http_status(:no_content)
      end
    end

    context 'with error' do
      let(:user) {mock_model(User, destroy: false, errors: double(:errors, full_messages: ['foo']))}

      before do
        allow(User).to receive(:find).and_return(user)
      end

      it 'does not destroy the user' do
        expect {
          xhr :delete, :destroy, {id: user.id}
        }.to_not change(User, :count)
      end

      it 'returns the bad request status' do
        xhr :delete, :destroy, {id: user.id}

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the errors' do
        xhr :delete, :destroy, {id: user.id}
        
        expect(json['errors']).to_not be_empty
      end
    end
  end

  describe 'PATCH change_workplace' do
    before do
      @factory = FactoryGirl.create(:factory, name: 'F1')
      @distributor = FactoryGirl.create(:distributor, name: 'D1')
      @store = FactoryGirl.create(:store, name: 'S1', distributor: @distributor)

      @workplace = [@factory, @distributor, @store].sample

      @administrator = FactoryGirl.create(:administrator, name: 'admin', username: 'admin').tap do |administrator|
        administrator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @workplace, user: administrator))
      end
      
      sign_in :administrator, @administrator

      @factory_workplace = FactoryGirl.create(:user_workplace, workplace: @factory, user: @administrator)
      @administrator.update!(selected_workplace: @factory_workplace)
      @distributor_workplace = FactoryGirl.create(:user_workplace, workplace: @distributor, user: @administrator)

      @user = FactoryGirl.create(:user, name: 'bb')

      @user.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @factory, user: @user))
      @other_workplace = FactoryGirl.create(:user_workplace, workplace: @distributor, user: @user)
    end

    context 'when the user is not the logged user' do
      it 'does not updates the user selected workplace' do
        expect {
          xhr :patch, :change_workplace, { id: @user.id, workplace_id: @other_workplace.id }
          @user.reload
        }.to_not change(@user, :selected_workplace_id)
      end

      it 'returns the unauthorized status' do
        xhr :patch, :change_workplace, { id: @user.id, workplace_id: @other_workplace.id }

        expect(response).to have_http_status(:unauthorized)
      end

      it 'returns the errors' do
        xhr :patch, :change_workplace, { id: @user.id, workplace_id: @other_workplace.id }
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'when the user does not have access to the selected workplace' do
      it 'does not updates the user selected workplace' do
        expect {
          xhr :patch, :change_workplace, { id: @administrator.id, workplace_id: @other_workplace.id }
          @administrator.reload
        }.to_not change(@administrator, :selected_workplace_id)
      end

      it 'returns the unauthorized status' do
        xhr :patch, :change_workplace, { id: @administrator.id, workplace_id: @other_workplace.id }

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the errors' do
        xhr :patch, :change_workplace, { id: @administrator.id, workplace_id: @other_workplace.id }
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with success' do
      it 'updates the user selected workplace' do
        expect {
          xhr :patch, :change_workplace, { id: @administrator.id, workplace_id: @distributor_workplace.id }
          @administrator.reload
        }.to change(@administrator, :selected_workplace_id).from(@factory_workplace.id).to(@distributor_workplace.id)
      end

      it 'returns the no content status' do
        xhr :patch, :change_workplace, { id: @administrator.id, workplace_id: @distributor_workplace.id }

        expect(response).to have_http_status(:no_content)
      end
    end
  end
end
