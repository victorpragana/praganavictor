# == Schema Information
#
# Table name: cut_tables
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  size       :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe CutTablesController, type: :controller do
  before do
    sign_in :administrator, create(:administrator)
  end

  describe 'GET index' do
    render_views

    before do
      @cut_table_1 = create(:cut_table)
      @cut_table_2 = create(:cut_table)
      @cut_table_3 = create(:cut_table)
      @cut_table_4 = create(:cut_table)
    end

    it 'returns OK' do
      xhr :get, :index

      expect(response).to have_http_status(:ok)
    end
  end

  describe 'POST create' do
    render_views

    context 'with invalid params' do
      let(:parameters) {{'name' => '', 'size' => '-1'}}

      it 'does not create a new Cut table' do
        expect {
          xhr :post, :create, parameters
        }.to change(CutTable, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :create, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:size)           {50}
      let(:converted_size) {size * 10}
      let(:parameters)     { {'name' => 'Table', 'size'=>"#{size}"} }

      it 'initializes the cut table service' do
        expect(CutTableService).to receive(:new).with(
          parameters.dup.tap do |p|
            p['size'] = converted_size
          end
        ).and_call_original

        xhr :post, :create, parameters
      end

      it 'creates the cut table' do
        expect_any_instance_of(CutTableService).to receive(:create).and_call_original
        
        xhr :post, :create, parameters
      end

      it 'returns created status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:created)
      end

      it 'returns the cut table data' do
        xhr :post, :create, parameters

        cut_table = CutTable.last

        expect(json['cut_table']['id']).to eq(cut_table.id)
        expect(json['cut_table']['name']).to eq(cut_table.name)
        expect(json['cut_table']['size']).to eq(cut_table.size_from_mm_to_cm)
      end
    end
  end

  describe 'PUT update' do
    render_views

    before do
      @cut_table = create(:cut_table)
    end

    context 'with invalid params' do
      let(:parameters) {{'name' => ''}.merge({'id' => "#{@cut_table.id}"})}

      it 'does not create a new Cut table' do
        expect {
          xhr :put, :update, parameters
        }.to change(CutTable, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :put, :update, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :put, :update, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:size)           {50}
      let(:converted_size) {size * 10}
      let(:parameters)     { {'name' => 'Table', 'size'=>"#{size}"} }

      it 'initializes the cut table service' do
        expect(CutTableService).to receive(:new).with(
          parameters.dup.tap do |p|
            p['size'] = converted_size
          end
        ).and_call_original

        xhr :put, :update, parameters.merge({'id' => "#{@cut_table.id}"})
      end

      it 'updates the cut table' do
        expect_any_instance_of(CutTableService).to receive(:update).with(@cut_table.id.to_s).and_call_original
        
        xhr :put, :update, parameters.merge({'id' => "#{@cut_table.id}"})
      end

      it 'returns ok status' do
        xhr :put, :update, parameters.merge({'id' => "#{@cut_table.id}"})
        
        expect(response).to have_http_status(:ok)
      end

      it 'returns the cut table data' do
        xhr :put, :update, parameters.merge({'id' => "#{@cut_table.id}"})

        cut_table = CutTable.last

        expect(json['cut_table']['id']).to eq(cut_table.id)
        expect(json['cut_table']['name']).to eq(cut_table.name)
        expect(json['cut_table']['size']).to eq(cut_table.size_from_mm_to_cm)
      end
    end
  end

  describe 'GET show' do
    render_views

    before do
      @cut_table = create(:cut_table)
    end

    it 'should ok status' do
      xhr :get, :show, id: @cut_table.id

      expect(response).to have_http_status(:ok)
    end

    it 'returns the cut table data' do
      xhr :get, :show, id: @cut_table.id

      expect(json['cut_table']['id']).to eq(@cut_table.id)
      expect(json['cut_table']['name']).to eq(@cut_table.name)
      expect(json['cut_table']['size']).to eq(@cut_table.size_from_mm_to_cm)
    end
  end

  describe 'DELETE destroy' do
    render_views

    before do
      @cut_table = create(:cut_table)
    end

    context 'with success' do
      it 'destroys the cut table' do
        expect {
          xhr :delete, :destroy, { id: @cut_table.id }
        }.to change(CutTable, :count).by(-1)
      end

      it 'returns no content status' do
        xhr :delete, :destroy, { id: @cut_table.id }

        expect(response).to have_http_status(:no_content)
      end
    end

    context 'with error' do
      let(:cut_table) {mock_model(CutTable, destroy: false, errors: double(:errors, full_messages: ['foo']))}

      before do
        allow(CutTable).to receive(:find).and_return(cut_table)
      end

      it 'does not destroy the cut table' do
        expect {
          xhr :delete, :destroy, { id: cut_table.id }
        }.to_not change(CutTable, :count)
      end

      it 'returns the bad request status' do
        xhr :delete, :destroy, { id: cut_table.id }

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the errors' do
        xhr :delete, :destroy, { id: cut_table.id }
        
        expect(json['errors']).to_not be_empty
      end
    end
  end
end
