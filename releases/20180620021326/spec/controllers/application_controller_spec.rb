require 'rails_helper'

RSpec.describe ApplicationController, type: :controller do

  controller do 
    def index
      head :ok
    end
  end

  before(:each) do
    @factory = FactoryGirl.create(:factory, name: 'F1', subdomain: 'factory-f1')
    FactoryGirl.create(:administrator, username: 'F1').tap do |administrator|
      administrator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @factory, user: administrator))
    end

    allow(controller).to receive(:current_user).and_return @factory.users.first
  end

  context 'with valid user workplace subdomain' do 
    before do 
      @request.host = "factory-f1.test.host"
    end

    it 'returns a success response' do 
      get :index
      expect(response).to have_http_status(:ok)
    end
  end

  context 'with invalid user workplace subdomain' do 
    before do 
      @request.host = "factory.test.host"
    end

    it 'returns 401' do 
      get :index

      expect(response).to have_http_status(:unauthorized)
    end
  end
end
