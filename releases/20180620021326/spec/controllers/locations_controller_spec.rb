# == Schema Information
#
# Table name: locations
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  category       :integer          not null
#  environment_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  width          :integer
#  height         :integer
#  quantity       :integer
#

require 'rails_helper'

RSpec.describe LocationsController, type: :controller do
  before(:each) do
    @factory = create(:factory, name: 'F1')
    create(:administrator, username: 'F1', maximum_discount: 50).tap do |administrator|
      administrator.update!(selected_workplace: create(:user_workplace, workplace: @factory, user: administrator))
    end

    sign_in :administrator, @factory.users.first

    @distributor = create(:distributor, name: 'D1')
    @store       = create(:store, distributor: @distributor, name: 'S1')
    @customer    = create(:customer, store_id: @store.id, name: 'C1')
    @estimate    = create(:estimate, seller: @store, client: @customer)
    @environment = create(:environment, estimate: @estimate)
  end

  describe 'GET index' do
    render_views

    before(:each) do
      @location_1 = create(:location, environment: @environment, width: 1000, height: 1200, quantity: 2)
      @location_2 = create(:location, environment: @environment)
      @location_3 = create(:location, environment: @environment)
    end

    it 'returns the ok status' do
      xhr :get, :index, {'environment_id' => "#{@environment.id}"}

      expect(response).to have_http_status(:ok)
    end

    it 'returns the locations for the environment' do
      xhr :get, :index, {'environment_id' => "#{@environment.id}"}

      expect(json['locations'].size).to eq(3)

      expect(json['locations'][0]['id']).to eq(@location_1.id)
      expect(json['locations'][0]['name']).to eq(@location_1.name)
      expect(json['locations'][0]['environment_id']).to eq(@location_1.environment_id)
      expect(json['locations'][0]['category']).to eq(Location.categories[@location_1.category])
      expect(json['locations'][0]['width']).to eq(@location_1.width_from_mm_to_cm)
      expect(json['locations'][0]['height']).to eq(@location_1.height_from_mm_to_cm)
      expect(json['locations'][0]['quantity']).to eq(@location_1.quantity)

      expect(json['locations'][1]['id']).to eq(@location_2.id)
      expect(json['locations'][1]['name']).to eq(@location_2.name)
      expect(json['locations'][1]['environment_id']).to eq(@location_2.environment_id)
      expect(json['locations'][1]['category']).to eq(Location.categories[@location_2.category])
      expect(json['locations'][1]['width']).to eq(@location_2.width_from_mm_to_cm)
      expect(json['locations'][1]['height']).to eq(@location_2.height_from_mm_to_cm)
      expect(json['locations'][1]['quantity']).to eq(@location_2.quantity)

      expect(json['locations'][2]['id']).to eq(@location_3.id)
      expect(json['locations'][2]['name']).to eq(@location_3.name)
      expect(json['locations'][2]['environment_id']).to eq(@location_3.environment_id)
      expect(json['locations'][2]['category']).to eq(Location.categories[@location_3.category])
      expect(json['locations'][2]['width']).to eq(@location_3.width_from_mm_to_cm)
      expect(json['locations'][2]['height']).to eq(@location_3.height_from_mm_to_cm)
      expect(json['locations'][2]['quantity']).to eq(@location_3.quantity)
    end
  end

  describe 'GET show' do
    render_views

    before(:each) do
      @location = create(:location, environment: @environment, width: 1000, height: 1200, quantity: 2)
    end

    it 'returns the ok status' do
      xhr :get, :show, {'id' => "#{@location.id}"}

      expect(response).to have_http_status(:ok)
    end

    it 'returns the location data' do
      xhr :get, :show, {'id' => "#{@location.id}"}

      expect(json['location']['id']).to eq(@location.id)
      expect(json['location']['name']).to eq(@location.name)
      expect(json['location']['width']).to eq(@location.width_from_mm_to_cm)
      expect(json['location']['height']).to eq(@location.height_from_mm_to_cm)
      expect(json['location']['quantity']).to eq(@location.quantity)
      expect(json['location']['environment_id']).to eq(@location.environment_id)
      expect(json['location']['category']).to eq(Location.categories[@location.category])
    end
  end

  describe 'POST create' do
    render_views

    context 'with invalid params' do
      let(:parameters) { {'category' => '', 'name' => 'Foo', 'environment_id' => "#{@environment.id}"} }

      it 'does not create a new Location' do
        expect {
          xhr :post, :create, parameters
        }.to change(Location, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :create, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) { {'category' => '0', 'name' => 'Foo', 'width' => '100', 'height' => '100', 'quantity' => '5',
        'environment_id' => "#{@environment.id}"} }

      let(:parsed_parameters) { {'category' => '0', 'name' => 'Foo', 'width' => 1000, 'height' => 1000, 'quantity' => '5',
        'environment_id' => "#{@environment.id}"} }

      it 'initializes the location service' do
        expect(LocationService).to receive(:new).with(parsed_parameters).and_call_original

        xhr :post, :create, parameters
      end

      it 'creates the location' do
        expect_any_instance_of(LocationService).to receive(:create).and_call_original
        
        xhr :post, :create, parameters
      end

      it 'returns created status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:created)
      end

      it 'renders template show' do
        xhr :post, :create, parameters

        expect(response).to render_template('locations/show')
      end
    end
  end

  describe 'PUT update' do
    render_views

    before(:each) do
      @location = create(:location, environment: @environment, name: 'L1')
    end

    context 'with invalid params' do
      let(:parameters) { {'category' => '', 'name' => 'Foo', 'environment_id' => "#{@environment.id}"} }

      it 'does not create a new Location' do
        expect {
          xhr :put, :update, parameters.merge({'id' => "#{@location.id}"})
        }.to_not change(Location, :count)
      end

      it 'returns unprocessable entity status' do
        xhr :put, :update, parameters.merge({'id' => "#{@location.id}"})
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :put, :update, parameters.merge({'id' => "#{@location.id}"})
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) { {'category' => '0', 'name' => 'Foo', 'width' => '100', 'height' => '100', 'quantity' => '5',
        'environment_id' => "#{@environment.id}"} }

      let(:parsed_parameters) { {'category' => '0', 'name' => 'Foo', 'width' => 1000, 'height' => 1000, 'quantity' => '5',
        'environment_id' => "#{@environment.id}"} }

      it 'initializes the location service' do
        expect(LocationService).to receive(:new).with(parsed_parameters).and_call_original

        xhr :put, :update, parameters.merge({'id' => "#{@location.id}"})
      end

      it 'updates the location' do
        expect_any_instance_of(LocationService).to receive(:update).with(@location.id.to_s).and_call_original
        
        xhr :put, :update, parameters.merge({'id' => "#{@location.id}"})
      end

      it 'returns ok status' do
        xhr :put, :update, parameters.merge({'id' => "#{@location.id}"})
        
        expect(response).to have_http_status(:ok)
      end

      it 'renders template show' do
        xhr :put, :update, parameters.merge({'id' => "#{@location.id}"})

        expect(response).to render_template('locations/show')
      end
    end
  end

  describe 'DELETE destroy' do
    before(:each) do
      @location = create(:location, environment: @environment)

      @auth_headers = @factory.users.first.create_new_auth_token
    end

    context 'with success' do
      it 'destroys the location' do
        expect {
          xhr :delete, :destroy, { id: @location.id }.merge(@auth_headers)
        }.to change(Location, :count).by(-1)
      end

      it 'returns no content status' do
        xhr :delete, :destroy, { id: @location.id }.merge(@auth_headers)

        expect(response).to have_http_status(:no_content)
      end
    end

    context 'with error' do
      let(:location) { mock_model(Location, destroy: false, errors: double(:errors, full_messages: ['foo'])) }

      before(:each) do
        allow(Location).to receive(:find).and_return(location)
      end

      it 'does not destroy the location' do
        expect {
          xhr :delete, :destroy, { id: location.id }
        }.to_not change(Location, :count)
      end

      it 'returns the bad request status' do
        xhr :delete, :destroy, { id: location.id }

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the errors' do
        xhr :delete, :destroy, { id: location.id }
        
        expect(json['errors']).to_not be_empty
      end
    end
  end

  describe 'DELETE reset' do
    before(:each) do
      @family = create(:family)
      @line = create(:line, family: @family)
      @location = create(:location, environment: @environment)

      @option_1 = create(:option, location: @location, line: @line)
      @option_2 = create(:option, location: @location, line: @line, selected: true)

      @auth_headers = @factory.users.first.create_new_auth_token
    end

    context 'with success' do
      it 'destroys the location options' do
        expect {
          xhr :delete, :destroy, { id: @location.id }.merge(@auth_headers)
        }.to change(Option, :count).by(-2)
      end

      it 'returns no content status' do
        xhr :delete, :destroy, { id: @location.id }.merge(@auth_headers)

        expect(response).to have_http_status(:no_content)
      end
    end

    context 'with error' do
      let(:location) { mock_model(Location, errors: double(:errors, full_messages: ['foo'])) }

      before(:each) do
        allow(location).to receive_message_chain(:options, :destroy_all).and_raise(ActiveRecord::RecordInvalid, location)
        allow(Location).to receive(:find).and_return(location)
      end

      it 'does not destroy the location options' do
        expect {
          xhr :delete, :destroy, { id: location.id }
        }.to_not change(Option, :count)
      end

      it 'returns the bad request status' do
        xhr :delete, :destroy, { id: location.id }

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the errors' do
        xhr :delete, :destroy, { id: location.id }
        
        expect(json['errors']).to_not be_empty
      end
    end
  end

  describe 'PUT apply_discount' do
    render_views

    before(:each) do
      create(:administrator, username: 'A1', maximum_discount: BigDecimal.new('12.50')).tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: @store, user: administrator))
      end

      sign_in :administrator, @store.users.first

      @family = create(:family)
      @line = create(:line, family: @family)
      @location = create(:location, environment: @environment)

      @option_1 = create(:option, location: @location, line: @line)
      @option_2 = create(:option, location: @location, line: @line, selected: true)
    end

    context 'with invalid data' do
      context 'for unallowed values' do
        let(:parameters) { {'discount' => '-1'}.merge('id' => "#{@location.id}") }

        it 'does not update the location\'s options discounts' do
          expect {
            xhr :put, :apply_discount, parameters
            @option_1.reload
          }.to_not change(@option_1, :discount)
        end

        it 'does not update the location\'s options discounts' do
          expect {
            xhr :put, :apply_discount, parameters
            @option_2.reload
          }.to_not change(@option_2, :discount)
        end

        it 'returns unprocessable entity status' do
          xhr :put, :apply_discount, parameters
          
          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'returns the errors' do
          xhr :put, :apply_discount, parameters
          
          expect(json['errors']).to_not be_empty
        end
      end

      context 'for discount greater than the allowed for the user' do
        let(:parameters) { {'discount' => '12.51'}.merge('id' => "#{@location.id}") }

        before do 
          @price_table = create(:price_table, owner: @store, default: true, type: :for_lines)
          create(:price_table_line, line: @line, price: '_CMV_*2', price_table: @price_table, maximum_discount: BigDecimal.new('50.1'))
          create(:price_table_client, price_table: @price_table, client: @customer)
        end

        it 'does not update the location\'s options discounts' do
          expect {
            xhr :put, :apply_discount, parameters
            @option_1.reload
          }.to_not change(@option_1, :discount)
        end

        it 'does not update the location\'s options discounts' do
          expect {
            xhr :put, :apply_discount, parameters
            @option_2.reload
          }.to_not change(@option_2, :discount)
        end

        it 'returns unprocessable entity status' do
          xhr :put, :apply_discount, parameters
          
          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'returns the errors' do
          xhr :put, :apply_discount, parameters
          
          expect(json['errors']).to_not be_empty
        end
      end
    end

    context 'with valid params' do
      let(:parameters) { {'discount' => '10.58'} }

      before do
        @price_table = create(:price_table, owner: @store, default: true, type: :for_lines)

        create(:price_table_line, line: @line, price: '_CMV_*2', price_table: @price_table, maximum_discount: BigDecimal.new('50.1'))
      end

      it 'initializes the location service' do
        expect(LocationService).to receive(:new).with(parameters).and_call_original

        xhr :put, :apply_discount, parameters.merge('id' => "#{@location.id}")
      end

      it 'should apply the discount to the location' do
        expect_any_instance_of(LocationService).to receive(:apply_discount).with(@location.id.to_s).and_call_original
        
        xhr :put, :apply_discount, parameters.merge('id' => "#{@location.id}")
      end

      it 'returns ok status' do
        xhr :put, :apply_discount, parameters.merge('id' => "#{@location.id}")
        
        expect(response).to have_http_status(:ok)
      end

      it 'renders template show' do
        xhr :put, :apply_discount, parameters.merge('id' => "#{@location.id}")

        expect(response).to render_template('locations/show')
      end
    end
  end
end
