# == Schema Information
#
# Table name: stores
#
#  id                           :integer          not null, primary key
#  deleted_at                   :datetime
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  distributor_id               :integer
#  commercial_classification_id :integer
#

require 'rails_helper'

RSpec.describe StoresController, type: :controller do  
  before do
    @factory = create(:factory, name: 'F1')
    @distributor = create(:distributor, name: 'D1')
    @distributor2 = create(:distributor, name: 'D2')
  
    @user = create(:administrator, username: 'F1').tap do |administrator|
      administrator.update!(selected_workplace: create(:user_workplace, workplace: @factory, user: administrator))
    end
  end

  describe 'third party access control' do
    before do
      store = create(:store, name: 'S1', distributor: @distributor)
      @user.update!(selected_workplace: create(:user_workplace, workplace: store, user: @user))

      sign_in :administrator, @user
    end

    it 'renders unauthorized if the user is not a distributor or factory' do
      xhr :get, :index

      expect(response).to have_http_status(:unauthorized)
    end
  end

  describe 'GET index' do
    render_views

    before do
      @store_1 = create(:store, name: 'aa', distributor: @distributor)
      @store_2 = create(:store, name: 'bb', distributor: @distributor)
      @store_3 = create(:store, name: 'cc', distributor: @distributor)

      sign_in :administrator, @user
    end

    it 'returns OK' do
      xhr :get, :index

      expect(response).to have_http_status(:ok)
    end

    context 'with datatable search parameters' do
      let(:params) {{'draw'=>'1', 'columns'=>{'0'=>{'data'=>'0', 'name'=>'', 'searchable'=>'false', 'orderable'=>'false', 
        'search'=>{'value'=>'', 'regex'=>'false'}}, '1'=>{'data'=>'1', 'name'=>'id', 'searchable'=>'true', 'orderable'=>'true', 
        'search'=>{'value'=>'', 'regex'=>'false'}}, '2'=>{'data'=>'2', 'name'=>'', 'searchable'=>'true', 'orderable'=>'true', 
        'search'=>{'value'=>'', 'regex'=>'false'}}}, 'order'=>{'0'=>{'column'=>'1', 'dir'=>'asc'}}, 'start'=>'0', 'length'=>'100', 
        'search'=>{'value'=>'', 'regex'=>'false'}, '_'=>'1456860760040'}}

      it 'initializes the store datatable' do
        expect(StoreDatatable).to receive(:new)

        xhr :get, :index, params
      end
    end

    context 'without search parameters' do
      it 'lists the stores' do
        xhr :get, :index

        expect(json['stores'].size).to eq(3)
        expect(json['stores'][0]['id']).to eq(@store_1.id)
        expect(json['stores'][0]['name']).to eq(@store_1.name)
        expect(json['stores'][1]['id']).to eq(@store_2.id)
        expect(json['stores'][1]['name']).to eq(@store_2.name)
        expect(json['stores'][2]['id']).to eq(@store_3.id)
        expect(json['stores'][2]['name']).to eq(@store_3.name)
      end
    end
  end

  describe 'POST create' do
    render_views

    before do
      @state = create(:state)
      @city  = create(:city, state: @state)
    end

    context 'with invalid params' do
      before do
        sign_in :administrator, @user
      end

      let(:parameters) { { 'name' => '' } }

      it 'does not create a new store' do
        expect {
          xhr :post, :create, parameters
        }.to change(Store, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :create, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      context 'with factory workplace' do
        let(:parameters) { { 'name' => 'Loja 1', 'identification' => '56.220.362/0001-42', 'zip_code' => '12244-888', 
          'street' => 'St. Foo', 'neighborhood' => 'foo', 'number' => 'SN', 'complement' => 'ABC', 'phone' => '(12)99222-8290', 'email' => 'foo@bar.com', 
          'corporate_name' => 'Abc', 'city_id' => "#{@city.id}", 'state_inscription' => '123', 'municipal_inscription' => '333',
          'distributor_id' => "#{@distributor2.id}", 'allow_special_discount' => true, 'special_discount' => '18.99', 'subdomain'=> 'loja-1',
          'current_account' => '1234-5', 'agency' => '1234', 'bank' => "#{ThirdParty.banks[:brasil]}"
        } }

        before do
          sign_in :administrator, @user
        end

        it 'initializes the store service with the selected distributor' do
          expect(StoreService).to receive(:new).with(parameters).and_call_original

          xhr :post, :create, parameters
        end

        it 'creates the store' do
          expect_any_instance_of(StoreService).to receive(:create).and_call_original
          
          xhr :post, :create, parameters
        end

        it 'returns created status' do
          xhr :post, :create, parameters
          
          expect(response).to have_http_status(:created)
        end
        it 'returns new store id ' do
          xhr :post, :create, parameters
          
          expect(json['store']['id']).to eq(Store.last.id)
        end
      end

      context 'with distributor workplace' do
        let(:parameters) { { 'name' => 'Loja 1', 'identification' => '56.220.362/0001-42', 'zip_code' => '12244-888', 
          'street' => 'St. Foo', 'neighborhood' => 'foo', 'number' => 'SN', 'complement' => 'ABC', 'phone' => '(12)99222-8290', 'email' => 'foo@bar.com', 
          'corporate_name' => 'Abc', 'city_id' => "#{@city.id}", 'state_inscription' => '123', 'municipal_inscription' => '333', 'allow_special_discount' => true, 'special_discount' => '18.99',
          'subdomain'=> 'loja-1', 'current_account' => '1234-5', 'agency' => '1234', 'bank' => "#{ThirdParty.banks[:brasil]}"
        } }

        before do
          @user.update!(selected_workplace: create(:user_workplace, workplace: @distributor, user: @user))
          
          sign_in :administrator, @user
        end

        it 'initializes the store service with the logged user distributor id' do
          expect(StoreService).to receive(:new).with(parameters.merge({'distributor_id' => @distributor.id})).and_call_original

          xhr :post, :create, parameters
        end

        it 'creates the store' do
          expect_any_instance_of(StoreService).to receive(:create).and_call_original
          
          xhr :post, :create, parameters
        end

        it 'returns created status' do
          xhr :post, :create, parameters
          
          expect(response).to have_http_status(:created)
        end
      end
    end
  end

  describe 'PUT upload_logo' do
    before do
      @state = create(:state)
      @city  = create(:city, state: @state)
      @store = create(:store, name: 'bb', city_id: @city.id, distributor: @distributor)
    end

    context 'when user is not administrator' do 
      let(:parameters) { { 'store' => { 'photo_file' => '' } }.merge(id: @store.id) }

      before do 
        @user = create(:operator)
        sign_in :operator, @user
      end

      it 'returns unauthorized status' do 
        xhr :put, :upload_logo, parameters
        expect(response).to have_http_status(:unauthorized)
      end
    end

    context 'when user is administrator' do 
      before do
        @user  = create(:administrator, username: 'store_user').tap do |administrator|
          administrator.update!(selected_workplace: create(:user_workplace, workplace: @factory, user: administrator))
        end

        sign_in :administrator, @user
      end

      context 'with service error' do
        let(:store_service) { double(:store_service, update: false, success: false, errors: ['foo']) }
        let(:parameters) { { 'store' => { 'photo_file' => fixture_file_upload('/files/avatar.jpg', 'image/jpg') } }.merge(id: @store.id) }

        before do
          allow(StoreService).to receive(:new).and_return(store_service)
        end

        it 'returns the unprocessable_entity status' do
          xhr :put, :upload_logo, parameters

          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'should return the errors' do
          xhr :put, :update, parameters

          expect(json['errors']).to_not be_empty
        end
      end

      context 'with empty parameters' do
        let(:parameters) { { 'store' => { 'photo_file' => '' } }.merge(id: @store.id) }

        it 'returns the ok status' do
          xhr :put, :upload_logo, parameters

          expect(response).to have_http_status(:ok)
        end

        it 'renders nothing' do
          xhr :put, :upload_logo, parameters

          expect(response.body).to be_blank
        end
      end

      context 'with valid parameters' do
        let(:parameters) { { 'store' => { 'photo_file' => fixture_file_upload('/files/avatar.jpg', 'image/jpg') } }.merge(id: @store.id) }


        it 'updates the store' do
          expect_any_instance_of(StoreService).to receive(:update).with(@store.id.to_s).and_call_original

          xhr :put, :upload_logo, parameters
        end

        it 'returns the ok status' do
          xhr :put, :upload_logo, parameters

          expect(response).to have_http_status(:ok)
        end

        it 'renders nothing' do
          xhr :put, :upload_logo, parameters

          expect(response.body).to be_blank
        end
      end
    end
  end

  describe 'GET show' do
    render_views

    before do
      @state = create(:state)
      @city  = create(:city, state: @state)
      @store = create(:store, name: 'bb', city_id: @city.id, distributor: @distributor, agency: '1234', current_account: '12345-6', bank: :brasil, allow_special_discount: true, special_discount: 3)
      @user  = create(:administrator, username: 'store_user').tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: @factory, user: administrator))
      end

      sign_in :administrator, @user
    end

    it 'should ok status' do
      xhr :get, :show, :id => @store.id

      expect(response).to have_http_status(:ok)
    end

    it 'returns the store data' do
      xhr :get, :show, :id => @store.id

      expect(json['store']['id']).to eq(@store.id)
      expect(json['store']['name']).to eq(@store.name)
      expect(json['store']['identification']).to eq(@store.identification)
      expect(json['store']['zip_code']).to eq(@store.zip_code)
      expect(json['store']['street']).to eq(@store.street)
      expect(json['store']['neighborhood']).to eq(@store.neighborhood)
      expect(json['store']['number']).to eq(@store.number)
      expect(json['store']['complement']).to eq(@store.complement)
      expect(json['store']['phone']).to eq(@store.phone)
      expect(json['store']['email']).to eq(@store.email)
      expect(json['store']['corporate_name']).to eq(@store.corporate_name)
      expect(json['store']['state_inscription']).to eq(@store.state_inscription)
      expect(json['store']['municipal_inscription']).to eq(@store.municipal_inscription)
      expect(json['store']['distributor_id']).to eq(@store.distributor_id)
      expect(json['store']['city']['id']).to eq(@store.city.id)
      expect(json['store']['city']['name']).to eq(@store.city.name)
      expect(json['store']['state']['id']).to eq(@store.state.id)
      expect(json['store']['state']['name']).to eq(@store.state.name)
      expect(json['store']['allow_special_discount']).to eq(@store.allow_special_discount?)
      expect(json['store']['special_discount']).to eq(@store.special_discount)
      expect(json['store']['subdomain']).to eq(@store.subdomain)
      expect(json['store']['logo_url']).to eq(@store.logo.url)
      expect(json['store']['logo_file_name']).to eq(@store.logo_file_name)
      expect(json['store']['current_account']).to eq(@store.current_account)
      expect(json['store']['agency']).to eq(@store.agency)
      expect(json['store']['bank']).to eq(ThirdParty.banks[@store.bank])
    end
  end

  describe 'PUT update' do
    render_views

    before do
      @store = create(:store, name: 'bb', distributor: @distributor)
      @state = create(:state)
      @city  = create(:city, state: @state)
    end

    context 'with invalid params' do
      let(:parameters) {{'name' => ''}.merge({'id' => "#{@store.id}"})}

      before do
        sign_in :administrator, @user
      end

      it 'does not create a new store' do
        expect {
          xhr :put, :update, parameters
        }.to change(Store, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :put, :update, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :put, :update, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      context 'with factory workplace' do
        let(:parameters) { {'name' => 'Loja 1', 'identification' => '56.220.362/0001-42', 'zip_code' => '12244-888', 
          'street' => 'St. Foo', 'neighborhood' => 'foo', 'number' => 'SN', 'complement' => 'ABC', 'phone' => '(12)99222-8290', 'email' => 'foo@bar.com', 
          'corporate_name' => 'Abc', 'city_id' => "#{@city.id}", 'state_inscription' => '123', 'municipal_inscription' => '333',
          'distributor_id' => "#{@distributor.id}", 'allow_special_discount' => true, 'special_discount' => '18.99',
          'current_account' => '1234-5', 'agency' => '1234', 'bank' => "#{ThirdParty.banks[:brasil]}"
        } }

        before do
          sign_in :administrator, @user
        end

        it 'initializes the store service with the selected distributor' do
          expect(StoreService).to receive(:new).with(parameters).and_call_original

          xhr :put, :update, parameters.merge({'id' => "#{@store.id}"})
        end

        it 'updates the store' do
          expect_any_instance_of(StoreService).to receive(:update).with(@store.id.to_s).and_call_original
          
          xhr :put, :update, parameters.merge({'id' => "#{@store.id}"})
        end

        it 'returns ok status' do
          xhr :put, :update, parameters.merge({'id' => "#{@store.id}"})
          
          expect(response).to have_http_status(:ok)
        end
      end

      context 'with distributor workplace' do
        let(:parameters) { {'name' => 'Loja 1', 'identification' => '56.220.362/0001-42', 'zip_code' => '12244-888', 
          'street' => 'St. Foo', 'neighborhood' => 'foo', 'number' => 'SN', 'complement' => 'ABC', 'phone' => '(12)99222-8290', 'email' => 'foo@bar.com', 
          'corporate_name' => 'Abc', 'city_id' => "#{@city.id}", 'state_inscription' => '123', 'municipal_inscription' => '333', 'allow_special_discount' => true, 'special_discount' => '18.99',
          'current_account' => '1234-5', 'agency' => '1234', 'bank' => "#{ThirdParty.banks[:brasil]}"
        } }

        before do
          @user.update!(selected_workplace: create(:user_workplace, workplace: @distributor, user: @user))
          
          sign_in :administrator, @user
        end

        it 'initializes the store service with the logged user distributor id' do
          expect(StoreService).to receive(:new).with(parameters.merge({'distributor_id' => @distributor.id})).and_call_original

          xhr :put, :update, parameters.merge({'id' => "#{@store.id}"})
        end

        it 'updates the store' do
          expect_any_instance_of(StoreService).to receive(:update).with(@store.id.to_s).and_call_original
          
          xhr :put, :update, parameters.merge({'id' => "#{@store.id}"})
        end

        it 'returns ok status' do
          xhr :put, :update, parameters.merge({'id' => "#{@store.id}"})
          
          expect(response).to have_http_status(:ok)
        end
      end
    end
  end

  describe 'DELETE destroy' do
    before do
      @store = create(:store, name: 'bb', distributor: @distributor)

      sign_in :administrator, @user
    end

    context 'with success' do
      it 'destroys the store' do
        expect {
          xhr :delete, :destroy, {:id => @store.id}
        }.to change(Store, :count).by(-1)
      end

      it 'returns no content status' do
        xhr :delete, :destroy, {:id => @store.id}

        expect(response).to have_http_status(:no_content)
      end
    end

    context 'with error' do
      before do
        allow(@store).to receive(:destroy).and_return(false)
        allow(@store).to receive(:errors).and_return(double(:errors, full_messages: ['foo']))

        allow(Store).to receive_message_chain(:joins, :all, :order, :find).and_return(@store)
      end

      it 'does not destroy the store' do
        expect {
          xhr :delete, :destroy, {:id => @store.id}
        }.to_not change(Store, :count)
      end

      it 'returns the bad request status' do
        xhr :delete, :destroy, {:id => @store.id}

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the errors' do
        xhr :delete, :destroy, {:id => @store.id}
        
        expect(json['errors']).to_not be_empty
      end
    end
  end

  describe 'DELETE batch_destroy' do
    render_views

    before do
      @store_1 = create(:store, name: 'S1', distributor: @distributor)
      @store_2 = create(:store, name: 'S2', distributor: @distributor)

      sign_in :administrator, @user
    end

    context 'with success' do
      it 'destroys the stores' do
        expect {
          xhr :delete, :batch_destroy, {:ids => [@store_1.id, @store_2.id]}
        }.to change(Store, :count).by(-2)
      end

      it 'returns no content status' do
        xhr :delete, :batch_destroy, {:ids => [@store_1.id, @store_2.id]}

        expect(response).to have_http_status(:no_content)
      end
    end

    context 'with error' do
      before do
        allow(@store_1).to receive(:destroy!).and_raise(StandardError)
        allow(Store).to receive_message_chain(:joins, :all, :order, :find).and_return(@store_1)
      end

      it 'does not destroy the stores' do
        expect {
          xhr :delete, :batch_destroy, {:ids => [@store_1.id]}
        }.to_not change(Store, :count)
      end

      it 'returns the bad request status' do
        xhr :delete, :batch_destroy, {:ids => [@store_1.id]}

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the errors' do
        xhr :delete, :batch_destroy, {:ids => [@store_1.id]}
        
        expect(json['errors']).to_not be_empty
      end
    end
  end

  describe 'POST apply_price_table' do
    before do
      @price_table = create(:price_table, name: 'ab', owner: @factory)

      @store_1 = create(:store, name: 'SA', distributor: @distributor)
      @store_2 = create(:store, name: 'SB', distributor: @distributor)
      @store_3 = create(:store, name: 'SC', distributor: @distributor)

      @price_table.clients << PriceTableClient.new(client: @distributor)

      sign_in :administrator, @user
    end

    context 'with invalid params' do
      let(:parameters) {{ 'ids' => [''] }}

      it 'returns unprocessable entity status' do
        xhr :post, :apply_price_table, parameters.merge({'price_table_id' => @price_table.id})
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :apply_price_table, parameters.merge({'price_table_id' => @price_table.id})
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) {{ 'ids' => ["#{@store_1.id}", "#{@store_2.id}", "#{@store_3.id}"] }}

      it 'initializes the Price Table Service' do
        expect(PriceTableService).to receive(:new).with({'owner' => @factory}).and_call_original

        xhr :post, :apply_price_table, parameters.merge({'price_table_id' => @price_table.id})
      end

      it 'should apply the price table' do
        expect_any_instance_of(PriceTableService).to receive(:apply_to_stores).with("#{@price_table.id}", parameters['ids']).and_call_original
        
        xhr :post, :apply_price_table, parameters.merge({'price_table_id' => @price_table.id})
      end

      it 'returns ok status' do
        xhr :post, :apply_price_table, parameters.merge({'price_table_id' => @price_table.id})
        
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'POST apply_commercial_classification' do
    before do
      @commercial_classification = create(:commercial_classification, name: 'ab')

      @store1 = create(:store, name: 'SA', distributor: @distributor)
      @store2 = create(:store, name: 'SB', distributor: @distributor)
      @store3 = create(:store, name: 'SC', distributor: @distributor)

      sign_in :administrator, @user
    end

    context 'with invalid params' do
      let(:parameters) { { 'ids' => [''] } }
      let(:service) { double(:service, success: false, errors: ['foo'], apply_to_stores: nil) }

      before do
        allow(CommercialClassificationService).to receive(:new).and_return(service)
      end

      it 'returns unprocessable entity status' do
        xhr :post, :apply_commercial_classification, parameters.merge({'commercial_classification_id' => @commercial_classification.id})
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :apply_commercial_classification, parameters.merge({'commercial_classification_id' => @commercial_classification.id})
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) { { 'ids' => ["#{@store1.id}", "#{@store2.id}", "#{@store3.id}"] } }

      it 'initializes the Commercial Classification Service' do
        expect(CommercialClassificationService).to receive(:new).and_call_original

        xhr :post, :apply_commercial_classification, parameters.merge({'commercial_classification_id' => @commercial_classification.id})
      end

      it 'applies the commercial classification' do
        expect_any_instance_of(CommercialClassificationService).to receive(:apply_to_stores).with("#{@commercial_classification.id}", parameters['ids']).and_call_original
        
        xhr :post, :apply_commercial_classification, parameters.merge({'commercial_classification_id' => @commercial_classification.id})
      end

      it 'returns ok status' do
        xhr :post, :apply_commercial_classification, parameters.merge({'commercial_classification_id' => @commercial_classification.id})
        
        expect(response).to have_http_status(:ok)
      end
    end
  end
end
