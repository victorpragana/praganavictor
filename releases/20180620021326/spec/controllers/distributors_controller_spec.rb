# == Schema Information
#
# Table name: distributors
#
#  id                           :integer          not null, primary key
#  deleted_at                   :datetime
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  commercial_classification_id :integer
#

require 'rails_helper'

RSpec.describe DistributorsController, type: :controller do
  before do
    @family_1       = create(:family, name: 'F1')
    @family_2       = create(:family, name: 'F2')
    @line_1         = create(:line, family: @family_1, name: 'L1')
    @line_2         = create(:line, family: @family_2, name: 'L2')

    @factory = create(:factory, name: 'F1')

    create(:administrator, username: 'F1').tap do |administrator|
      administrator.update!(selected_workplace: create(:user_workplace, workplace: @factory, user: administrator))
    end

    sign_in :administrator, @factory.users.first
  end
  
  describe 'GET index' do
    render_views

    before do
      @distributor1 = create(:distributor, name: 'aa')
      @distributor2 = create(:distributor, name: 'bb')
      @distributor3 = create(:distributor, name: 'cc')

      @price_table1 = create(:price_table, name: 'P1', owner: @factory)
      create(:price_table_client, price_table: @price_table1, client: @distributor1)

      @price_table2 = create(:price_table, name: 'P2', owner: @factory)
      create(:price_table_client, price_table: @price_table2, client: @distributor3)
    end

    it 'returns OK' do
      xhr :get, :index

      expect(response).to have_http_status(:ok)
    end

    context 'with datatable search parameters' do
      let(:params) { { 'draw'=>'1', 'columns'=>{'0'=>{'data'=>'0', 'name'=>'', 'searchable'=>'false', 'orderable'=>'false', 
        'search'=>{'value'=>'', 'regex'=>'false'}}, '1'=>{'data'=>'1', 'name'=>'id', 'searchable'=>'true', 'orderable'=>'true', 
        'search'=>{'value'=>'', 'regex'=>'false'}}, '2'=>{'data'=>'2', 'name'=>'', 'searchable'=>'true', 'orderable'=>'true', 
        'search'=>{'value'=>'', 'regex'=>'false'}}}, 'order'=>{'0'=>{'column'=>'1', 'dir'=>'asc'}}, 'start'=>'0', 'length'=>'100', 
        'search'=>{'value'=>'', 'regex'=>'false'}, '_'=>'1456860760040' } }

      it 'initializes the distributor datatable' do
        expect(DistributorDatatable).to receive(:new)

        xhr :get, :index, params
      end
    end

    context 'without search parameters' do
      it 'lists the distributors' do
        xhr :get, :index

        expect(json['distributors'].size).to eq(3)
        expect(json['distributors'][0]['id']).to eq(@distributor1.id)
        expect(json['distributors'][0]['name']).to eq(@distributor1.name)
        expect(json['distributors'][0]['price_table']).to eq(@price_table1.name)
        expect(json['distributors'][1]['id']).to eq(@distributor2.id)
        expect(json['distributors'][1]['name']).to eq(@distributor2.name)
        expect(json['distributors'][1]['price_table']).to be_nil
        expect(json['distributors'][2]['id']).to eq(@distributor3.id)
        expect(json['distributors'][2]['name']).to eq(@distributor3.name)
        expect(json['distributors'][2]['price_table']).to eq(@price_table2.name)
      end
    end
  end

  describe 'POST create' do
    render_views

    before do
      @state = create(:state)
      @city  = create(:city, state: @state)
    end

    context 'with invalid params' do
      let(:parameters) { { 'name' => '' } }

      it 'does not create a new distributor' do
        expect {
          xhr :post, :create, parameters
        }.to change(Distributor, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :create, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) { {'name' => 'Distributor 1', 'identification' => '56.220.362/0001-42', 'zip_code' => '12244-888', 
        'street' => 'St. Foo', 'neighborhood' => 'foo', 'number' => 'SN', 'complement' => 'ABC', 'phone' => '(12)99222-8290', 'email' => 'foo@bar.com', 
        'corporate_name' => 'Abc', 'city_id' => "#{@city.id}", 'state_inscription' => '123', 'municipal_inscription' => '333',
        'allow_special_discount' => true, 'special_discount' => '15.16', 'subdomain'=> 'distributor-1',
        'current_account' => '1234-5', 'agency' => '1234', 'bank' => "#{ThirdParty.banks[:brasil]}"
      } }

      it 'initializes the distributor service' do
        expect(DistributorService).to receive(:new).with(parameters).and_call_original

        xhr :post, :create, parameters
      end

      it 'creates the distributor' do
        expect_any_instance_of(DistributorService).to receive(:create).and_call_original
        
        xhr :post, :create, parameters
      end

      it 'returns created status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:created)
      end
    end
  end

  describe 'GET show' do
    render_views

    before do
      @state = create(:state)
      @city  = create(:city, state: @state)
      @distributor = create(:distributor, name: 'bb', city_id: @city.id, agency: '1234', current_account: '12345-6', bank: :brasil, allow_special_discount: true, special_discount: 10)
      @user = create(:administrator).tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: @distributor, user: administrator))
      end
    end

    it 'should ok status' do
      xhr :get, :show, id: @distributor.id

      expect(response).to have_http_status(:ok)
    end

    it 'returns the distributor data' do
      xhr :get, :show, id: @distributor.id

      expect(json['distributor']['id']).to eq(@distributor.id)
      expect(json['distributor']['name']).to eq(@distributor.name)
      expect(json['distributor']['identification']).to eq(@distributor.identification)
      expect(json['distributor']['zip_code']).to eq(@distributor.zip_code)
      expect(json['distributor']['street']).to eq(@distributor.street)
      expect(json['distributor']['neighborhood']).to eq(@distributor.neighborhood)
      expect(json['distributor']['number']).to eq(@distributor.number)
      expect(json['distributor']['complement']).to eq(@distributor.complement)
      expect(json['distributor']['phone']).to eq(@distributor.phone)
      expect(json['distributor']['email']).to eq(@distributor.email)
      expect(json['distributor']['corporate_name']).to eq(@distributor.corporate_name)
      expect(json['distributor']['state_inscription']).to eq(@distributor.state_inscription)
      expect(json['distributor']['municipal_inscription']).to eq(@distributor.municipal_inscription)
      expect(json['distributor']['city']['id']).to eq(@distributor.city.id)
      expect(json['distributor']['city']['name']).to eq(@distributor.city.name)
      expect(json['distributor']['state']['id']).to eq(@distributor.state.id)
      expect(json['distributor']['state']['name']).to eq(@distributor.state.name)
      expect(json['distributor']['allow_special_discount']).to eq(@distributor.allow_special_discount?)
      expect(json['distributor']['special_discount']).to eq(@distributor.special_discount)
      expect(json['distributor']['subdomain']).to eq(@distributor.subdomain)
      expect(json['distributor']['logo_url']).to eq(@distributor.logo.url)
      expect(json['distributor']['logo_file_name']).to eq(@distributor.logo_file_name)
      expect(json['distributor']['current_account']).to eq(@distributor.current_account)
      expect(json['distributor']['agency']).to eq(@distributor.agency)
      expect(json['distributor']['bank']).to eq(ThirdParty.banks[@distributor.bank])
    end
  end

  describe 'PUT upload_logo' do
    before do
      @distributor = create(:distributor, name: 'bb')
      @state = create(:state)
      @city  = create(:city, state: @state)
    end

    context 'when user is not administrator' do 
      let(:parameters) { { 'distributor' => { 'photo_file' => '' } }.merge(id: @distributor.id)}

      before do 
        sign_out  :administrator
        @operator = create(:operator)
        sign_in :operator, @operator
      end

      it 'returns unauthorized status' do 
        xhr :put, :upload_logo, parameters
        expect(response).to have_http_status(:unauthorized)
      end
    end

    context 'with service error' do
      let(:distributorservice) { double(:distributorservice, update: false, success: false, errors: ['foo']) }
      let(:parameters) {{ 'distributor' => { 'photo_file' => fixture_file_upload('/files/avatar.jpg', 'image/jpg') } }.merge(id: @distributor.id)}

      before do
        allow(DistributorService).to receive(:new).and_return(distributorservice)
      end

      it 'returns the unprocessable_entity status' do
        xhr :put, :upload_logo, parameters

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'should return the errors' do
        xhr :put, :update, parameters

        expect(json['errors']).to_not be_empty
      end
    end

    context 'with empty parameters' do
      let(:parameters) {{ 'distributor' => { 'photo_file' => ''} }.merge(id: @distributor.id)}

      it 'returns the ok status' do
        xhr :put, :upload_logo, parameters

        expect(response).to have_http_status(:ok)
      end

      it 'renders nothing' do
        xhr :put, :upload_logo, parameters

        expect(response.body).to be_blank
      end
    end

    context 'with valid parameters' do
      let(:parameters) {{ 'distributor' => {'photo_file' => fixture_file_upload('/files/avatar.jpg', 'image/jpg') } }.merge(id: @distributor.id)}


      it 'updates the distributor' do
        expect_any_instance_of(DistributorService).to receive(:update).with(@distributor.id.to_s).and_call_original

        xhr :put, :upload_logo, parameters
      end

      it 'returns the ok status' do
        xhr :put, :upload_logo, parameters

        expect(response).to have_http_status(:ok)
      end

      it 'renders nothing' do
        xhr :put, :upload_logo, parameters

        expect(response.body).to be_blank
      end
    end
  end

  describe 'PUT update' do
    render_views

    before do
      @distributor = create(:distributor, name: 'bb')
      @state = create(:state)
      @city  = create(:city, state: @state)
    end

    context 'with invalid params' do
      let(:parameters) {{'name' => ''}.merge({'id' => "#{@distributor.id}"})}

      it 'does not create a new distributor' do
        expect {
          xhr :put, :update, parameters
        }.to change(Distributor, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :put, :update, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :put, :update, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) { {'name' => 'Distributor 1', 'identification' => '56.220.362/0001-42', 'zip_code' => '12244-888', 
        'street' => 'St. Foo', 'neighborhood' => 'foo', 'number' => 'SN', 'complement' => 'ABC', 'phone' => '(12)99222-8290', 'email' => 'foo@bar.com', 
        'corporate_name' => 'Abc', 'city_id' => "#{@city.id}", 'state_inscription' => '123', 'municipal_inscription' => '333',
        'allow_special_discount' => true, 'special_discount' => '15.16', 'subdomain'=> 'distributor-1',
        'current_account' => '1234-5', 'agency' => '1234', 'bank' => "#{ThirdParty.banks[:brasil]}"
      } }

      it 'initializes the distributor service' do
        expect(DistributorService).to receive(:new).with(parameters).and_call_original

        xhr :put, :update, parameters.merge({'id' => "#{@distributor.id}"})
      end

      it 'updates the distributor' do
        expect_any_instance_of(DistributorService).to receive(:update).with(@distributor.id.to_s).and_call_original
        
        xhr :put, :update, parameters.merge({'id' => "#{@distributor.id}"})
      end

      it 'returns ok status' do
        xhr :put, :update, parameters.merge({'id' => "#{@distributor.id}"})
        
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'DELETE destroy' do
    before do
      @distributor = create(:distributor, name: 'bb')
    end

    context 'with success' do
      it 'destroys the distributor' do
        expect {
          xhr :delete, :destroy, { id: @distributor.id }
        }.to change(Distributor, :count).by(-1)
      end

      it 'returns no content status' do
        xhr :delete, :destroy, { id: @distributor.id }

        expect(response).to have_http_status(:no_content)
      end
    end

    context 'with error' do
      let(:distributor) {mock_model(Distributor, destroy: false, errors: double(:errors, full_messages: ['foo']))}

      before do
        allow(Distributor).to receive(:find).and_return(distributor)
      end

      it 'does not destroy the distributor' do
        expect {
          xhr :delete, :destroy, { id: distributor.id }
        }.to_not change(Distributor, :count)
      end

      it 'returns the bad request status' do
        xhr :delete, :destroy, { id: distributor.id }

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the errors' do
        xhr :delete, :destroy, { id: distributor.id }
        
        expect(json['errors']).to_not be_empty
      end
    end
  end

  describe 'DELETE batch_destroy' do
    render_views

    before do
      @distributor1 = create(:distributor, name: 'S1')
      @distributor2 = create(:distributor, name: 'S2')
    end

    context 'with success' do
      it 'destroys the distributors' do
        expect {
          xhr :delete, :batch_destroy, {:ids => [@distributor1.id, @distributor2.id]}
        }.to change(Distributor, :count).by(-2)
      end

      it 'returns no content status' do
        xhr :delete, :batch_destroy, {:ids => [@distributor1.id, @distributor2.id]}

        expect(response).to have_http_status(:no_content)
      end
    end

    context 'with error' do
      let(:distributor) {mock_model(Distributor, errors: double(:errors, full_messages: ['foo']))}

      before do
        allow(distributor).to receive(:destroy!).and_raise(StandardError)
        allow(Distributor).to receive(:find).and_return(distributor)
      end

      it 'does not destroy the distributors' do
        expect {
          xhr :delete, :batch_destroy, {:ids => [distributor.id]}
        }.to_not change(Distributor, :count)
      end

      it 'returns the bad request status' do
        xhr :delete, :batch_destroy, {:ids => [distributor.id]}

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the errors' do
        xhr :delete, :batch_destroy, {:ids => [distributor.id]}
        
        expect(json['errors']).to_not be_empty
      end
    end
  end

  describe 'POST apply_price_table' do
    before do
      @price_table = create(:price_table, name: 'ab', owner: @factory)

      @distributor1 = create(:distributor, name: 'DA')
      @distributor2 = create(:distributor, name: 'DB')
      @distributor3 = create(:distributor, name: 'DC')

      @store = create(:store, name: 'SA', distributor: @distributor1)

      @price_table.clients << PriceTableClient.new(client: @store)
    end

    context 'with invalid params' do
      let(:parameters) {{ 'ids' => [''] }}

      it 'returns unprocessable entity status' do
        xhr :post, :apply_price_table, parameters.merge({'price_table_id' => @price_table.id})
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :apply_price_table, parameters.merge({'price_table_id' => @price_table.id})
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) {{ 'ids' => ["#{@distributor1.id}", "#{@distributor2.id}", "#{@distributor3.id}"] }}

      it 'initializes the Price Table Service' do
        expect(PriceTableService).to receive(:new).with({'owner' => @factory}).and_call_original

        xhr :post, :apply_price_table, parameters.merge({'price_table_id' => @price_table.id})
      end

      it 'applies the price table' do
        expect_any_instance_of(PriceTableService).to receive(:apply_to_distributors).with("#{@price_table.id}", parameters['ids']).and_call_original
        
        xhr :post, :apply_price_table, parameters.merge({'price_table_id' => @price_table.id})
      end

      it 'returns ok status' do
        xhr :post, :apply_price_table, parameters.merge({'price_table_id' => @price_table.id})
        
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'POST apply_commercial_classification' do
    before do
      @commercial_classification = create(:commercial_classification, name: 'ab')

      @distributor1 = create(:distributor, name: 'DA')
      @distributor2 = create(:distributor, name: 'DB')
      @distributor3 = create(:distributor, name: 'DC')

      @store = create(:store, name: 'SA', distributor: @distributor1)
    end

    context 'with invalid params' do
      let(:parameters) { { 'ids' => [''] } }
      let(:service) { double(:service, success: false, errors: ['foo'], apply_to_distributors: nil) }

      before do
        allow(CommercialClassificationService).to receive(:new).and_return(service)
      end

      it 'returns unprocessable entity status' do
        xhr :post, :apply_commercial_classification, parameters.merge({'commercial_classification_id' => @commercial_classification.id})
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :apply_commercial_classification, parameters.merge({'commercial_classification_id' => @commercial_classification.id})
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) { { 'ids' => ["#{@distributor1.id}", "#{@distributor2.id}", "#{@distributor3.id}"] } }

      it 'initializes the Commercial Classification Service' do
        expect(CommercialClassificationService).to receive(:new).and_call_original

        xhr :post, :apply_commercial_classification, parameters.merge({'commercial_classification_id' => @commercial_classification.id})
      end

      it 'applies the commercial classification' do
        expect_any_instance_of(CommercialClassificationService).to receive(:apply_to_distributors).with("#{@commercial_classification.id}", parameters['ids']).and_call_original
        
        xhr :post, :apply_commercial_classification, parameters.merge({'commercial_classification_id' => @commercial_classification.id})
      end

      it 'returns ok status' do
        xhr :post, :apply_commercial_classification, parameters.merge({'commercial_classification_id' => @commercial_classification.id})
        
        expect(response).to have_http_status(:ok)
      end
    end
  end
end
