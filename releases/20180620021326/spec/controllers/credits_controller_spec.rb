require 'rails_helper'

RSpec.describe CreditsController, type: :controller do
  describe 'GET index' do
    render_views

    before do
      @factory = create(:factory, name: 'F1')
      @distributor = create(:distributor, name: 'D1')
      @store = create(:store, name: 'S1', distributor: @distributor)

      @user = create(:administrator).tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: @store, user: administrator))
      end

      @order = create(:order, seller: @store, client: @distributor, billing_client: @distributor, user: @user)

      @payment = create(:payment, installment_number: 1)
      @movimentation1 = create(:production_credit, owner: @store.third_party, destination: @distributor, direction: :input, date: 20.hours.ago, value: 200, reason: :credit, payment: @payment)
      @movimentation2 = create(:production_credit, owner: @store.third_party, destination: @distributor, direction: :input, date: 25.hours.ago, value: 300, order: @order, reason: :order_payment, payment: @payment)
      @movimentation3 = create(:production_credit, owner: @store.third_party, destination: @distributor, direction: :output, date: 2.hours.ago, value: -400, order: @order, reason: :send_to_factory)
      
      @movimentation4 = create(:production_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :input, date: 17.hours.ago, value: 200, reason: :send_to_factory)
      @movimentation5 = create(:production_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :output, date: 15.hours.ago, value: -50, order: @order, reason: :order_production)

      @payment2 = create(:payment, installment_number: 3)
      @movimentation6 = create(:production_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :input, date: 16.hours.ago, value: 100, reason: :credit, payment: @payment2)
      @movimentation7 = create(:production_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :input, date: 16.hours.ago + 1.month, value: 100, reason: :credit, payment: @payment2)
      @movimentation8 = create(:production_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :input, date: 16.hours.ago + 2.months, value: 100, reason: :credit, payment: @payment2, status: :antecipated)
    end

    context 'when administrator is at the factory' do
      let(:parameters) { { 'third_party_id' => nil, 'from' => nil, 'until' => nil, 'credit_type' => 'ProductionCredit', 'type' => nil } }

      before do
        @administrator = create(:administrator, name: 'admin', username: 'admin').tap do |administrator|
          administrator.update!(selected_workplace: create(:user_workplace, workplace: @factory, user: administrator))
        end
        
        sign_in :administrator, @administrator
      end

      it 'returns OK' do
        xhr :get, :index, parameters

        expect(response).to have_http_status(:ok)
      end

      it 'initializes the credits searcher' do
        expect(CreditSearcher).to receive(:new).with(parameters).and_call_original

        xhr :get, :index, parameters
      end

      it 'lists the credits for the current workplace' do
        expect_any_instance_of(CreditSearcher).to receive(:search).with(@administrator.workplace).and_call_original

        xhr :get, :index, parameters
      end

      it 'calculates the previous balance for the current workplace' do
        expect_any_instance_of(CreditSearcher).to receive(:previous_balance).with(@administrator.workplace).and_call_original

        xhr :get, :index, parameters
      end

      it 'lists renders the credits data' do
        xhr :get, :index, parameters

        expect(json['previous_balance']).to eq(0)

        expect(json['data'].size).to eq(5)

        expect(json['data'][0]['id']).to eq(@movimentation4.id)
        expect(json['data'][0]['date']).to eq(@movimentation4.date.strftime("%d/%m/%Y"))
        expect(json['data'][0]['owner']).to eq(@movimentation4.owner.name)
        expect(json['data'][0]['destination']).to eq(@movimentation4.destination.name)
        expect(json['data'][0]['value']).to eq(-1 * @movimentation4.value.to_f)
        expect(json['data'][0]['balance']).to eq(-200)
        expect(json['data'][0]['reason']).to eq(Movement.reasons[@movimentation4.reason])
        expect(json['data'][0]['order_id']).to be_nil
        expect(json['data'][0]['payment_method']).to be_nil
        expect(json['data'][0]['antecipated']).to be_falsy

        expect(json['data'][1]['id']).to eq(@movimentation6.id)
        expect(json['data'][1]['date']).to eq(@movimentation6.date.strftime("%d/%m/%Y"))
        expect(json['data'][1]['owner']).to eq(@movimentation6.owner.name)
        expect(json['data'][1]['destination']).to eq(@movimentation6.destination.name)
        expect(json['data'][1]['value']).to eq(-1 * @movimentation6.value.to_f)
        expect(json['data'][1]['balance']).to eq(-300)
        expect(json['data'][1]['reason']).to eq(Movement.reasons[@movimentation6.reason])
        expect(json['data'][1]['order_id']).to be_nil
        expect(json['data'][1]['payment_method']['installment']).to eq(@movimentation6.installment_number)
        expect(json['data'][1]['payment_method']['installment_number']).to eq(@payment2.installment_number)
        expect(json['data'][1]['antecipated']).to be_falsy

        expect(json['data'][2]['id']).to eq(@movimentation5.id)
        expect(json['data'][2]['date']).to eq(@movimentation5.date.strftime("%d/%m/%Y"))
        expect(json['data'][2]['owner']).to eq(@movimentation5.owner.name)
        expect(json['data'][2]['destination']).to eq(@movimentation5.destination.name)
        expect(json['data'][2]['value']).to eq(-1 * @movimentation5.value.to_f)
        expect(json['data'][2]['balance']).to eq(-250)
        expect(json['data'][2]['reason']).to eq(Movement.reasons[@movimentation5.reason])
        expect(json['data'][2]['order_id']).to eq(@order.id)
        expect(json['data'][2]['payment_method']).to be_nil
        expect(json['data'][2]['antecipated']).to be_falsy

        expect(json['data'][3]['id']).to eq(@movimentation7.id)
        expect(json['data'][3]['date']).to eq(@movimentation7.date.strftime("%d/%m/%Y"))
        expect(json['data'][3]['owner']).to eq(@movimentation7.owner.name)
        expect(json['data'][3]['destination']).to eq(@movimentation7.destination.name)
        expect(json['data'][3]['value']).to eq(-1 * @movimentation7.value.to_f)
        expect(json['data'][3]['balance']).to eq(-350)
        expect(json['data'][3]['reason']).to eq(Movement.reasons[@movimentation7.reason])
        expect(json['data'][3]['order_id']).to be_nil
        expect(json['data'][3]['payment_method']['installment']).to eq(@movimentation7.installment_number)
        expect(json['data'][3]['payment_method']['installment_number']).to eq(@payment2.installment_number)
        expect(json['data'][3]['antecipated']).to be_falsy

        expect(json['data'][4]['id']).to eq(@movimentation8.id)
        expect(json['data'][4]['date']).to eq(@movimentation8.date.strftime("%d/%m/%Y"))
        expect(json['data'][4]['owner']).to eq(@movimentation8.owner.name)
        expect(json['data'][4]['destination']).to eq(@movimentation8.destination.name)
        expect(json['data'][4]['value']).to eq(-1 * @movimentation8.value.to_f)
        expect(json['data'][4]['balance']).to eq(-450)
        expect(json['data'][4]['reason']).to eq(Movement.reasons[@movimentation8.reason])
        expect(json['data'][4]['order_id']).to be_nil
        expect(json['data'][4]['payment_method']['installment']).to eq(@movimentation8.installment_number)
        expect(json['data'][4]['payment_method']['installment_number']).to eq(@payment2.installment_number)
        expect(json['data'][4]['antecipated']).to be_truthy
      end
    end
  end

  describe 'POST create' do
    render_views

    let(:charge) { double(:charge, success: true, url: 'http://foo.com.br/bankslip', id: 123, invoice_id: 'FOOBAR1234') }

    before do
      @factory = create(:factory, name: 'F1')
      @state = create(:state)
      @city = create(:city, state: @state)
      @distributor = create(:distributor)
      @store = create(:store, city: @city, distributor: @distributor)

      @payment_condition = create(:payment_condition)

      @administrator = create(:administrator, name: 'admin', username: 'admin').tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: @distributor, user: administrator))
      end

      @order = create(:order, seller: @store, client: @distributor, billing_client: @distributor, user: @administrator)

      allow(Iugu::Charge).to receive(:create).and_return(charge)
      
      sign_in :administrator, @administrator
    end

    describe 'with credit card or bank slip' do
      context 'with invalid parameters' do
        let(:parameters) { { 'credit_card_iugu_token' => '32432', 'value' => '', 'payment_method' => '' } }

        it 'returns the unprocessable entity status' do
          xhr :post, :create, parameters

          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'returns the the errors' do
          xhr :post, :create, parameters

          expect(json['errors']).to_not be_empty
        end
      end

      context 'with valid parameters' do
        let(:parameters) {
          {
            'credit_card_iugu_token' => '32432', 'value' => '100', 'payment_method' => 'credit_card', 'order_id' => "#{@order.id}",
            'payment_condition_id' => "#{@payment_condition.id}", 'credit_type' => 'ProductionCredit'
          }.merge('third_party_id' => @administrator.workplace.third_party.id)
        }
        let(:payment_params) { parameters.except('order_id') }

        it 'initializes the payment service' do
          expect(PaymentService).to receive(:new).with(payment_params).and_call_original

          xhr :post, :create, parameters
        end

        it 'creates the payment' do
          expect_any_instance_of(PaymentService).to receive(:create).with(parameters['order_id']).and_call_original

          xhr :post, :create, parameters
        end

        it 'returns the created status' do
          xhr :post, :create, parameters

          expect(response).to have_http_status(:created)
        end

        describe 'paying with bankslip' do
          let(:parameters) { { 'value' => '100', 'payment_method' => 'bank_slip', 'credit_type' => 'ProductionCredit', 'payment_condition_id' => "#{@payment_condition.id}" } }

          it 'returns the bank_slip url' do
            xhr :post, :create, parameters

            expect(json['credit']['bank_slip_url']).to eq('http://foo.com.br/bankslip')
          end
        end
      end
    end
  end

  describe "POST antecipate" do
    before do
      @factory = create(:factory, name: 'F1')
      @distributor = create(:distributor, name: 'D1')
      @store = create(:store, name: 'S1', distributor: @distributor)

      @user = create(:administrator).tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: @store, user: administrator))
      end

      @order = create(:order, seller: @store, client: @distributor, billing_client: @distributor, user: @user)

      @movimentation3 = create(:production_credit, owner: @store.third_party, destination: @distributor, direction: :output, date: 1.month.from_now, value: -400, order: @order, reason: :send_to_factory)
      @movimentation4 = create(:production_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :input, date: 17.hours.ago, value: 200, reason: :send_to_factory)
      @movimentation5 = create(:production_credit, owner: @distributor.third_party, destination: @factory.third_party, direction: :output, date: 15.hours.ago, value: -50, order: @order, reason: :order_production)

      @payment_condition1 = create(:payment_condition, installments: 1, antecipation_tax: 10, credit_antecipation_tax: 11)
      @payment_condition2 = create(:payment_condition, installments: 2, antecipation_tax: 12, credit_antecipation_tax: 13)
      @payment_condition3 = create(:payment_condition, installments: 3, antecipation_tax: 14, credit_antecipation_tax: 15)

      @payment = create(:payment, installment_number: 1)
      @movimentation1 = create(:production_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 1.month.from_now, value: 200, reason: :credit, payment: @payment)
      @movimentation2 = create(:production_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 1.month.from_now, value: 300, order: @order, reason: :order_payment, payment: @payment)

      @payment2 = create(:payment, installment_number: 3)
      @movimentation6 = create(:money_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 1.month.from_now, value: 100, reason: :credit, payment: @payment2)
      @movimentation7 = create(:money_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 2.months.from_now, value: 100, reason: :credit, payment: @payment2)
      @movimentation8 = create(:money_credit, owner: @store.third_party, destination: @distributor.third_party, direction: :input, date: 3.months.from_now, value: 100, reason: :credit, payment: @payment2)

      sign_in :administrator, @user
    end

    context 'with invalid data' do
      let(:parameters) { { 'movement_ids' => [@movimentation4.id.to_s, @movimentation1.id.to_s] } }

      it 'returns the unprocessable entity status' do
        xhr :post, :antecipate, parameters

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the the errors' do
        xhr :post, :antecipate, parameters

        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid data' do
      let(:parameters) { { 'movement_ids' => [@movimentation1.id.to_s, @movimentation6.id.to_s] } }

      it 'initializes the credit antecipation service' do
        expect(CreditAntecipationService).to receive(:new).with(parameters['movement_ids'], @user.workplace.third_party.id).and_call_original

        xhr :post, :antecipate, parameters
      end

      it 'antecipates the movements' do
        expect_any_instance_of(CreditAntecipationService).to receive(:antecipate).and_call_original

        xhr :post, :antecipate, parameters
      end

      it 'returns the ok status' do
        xhr :post, :antecipate, parameters

        expect(response).to have_http_status(:ok)
      end
    end
  end
end