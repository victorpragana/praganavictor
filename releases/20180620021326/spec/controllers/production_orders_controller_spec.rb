require 'rails_helper'

RSpec.describe ProductionOrdersController, type: :controller do
  before do
    @factory = FactoryGirl.create(:factory, name: 'F1')
    FactoryGirl.create(:administrator, username: 'F1').tap do |administrator|
      administrator.update!(selected_workplace: FactoryGirl.create(:user_workplace, workplace: @factory, user: administrator))
    end

    sign_in :administrator, @factory.users.first
    
    @family = FactoryGirl.create(:family)
    
    @category = FactoryGirl.create(:category, name: 'C1')
    @category_2 = FactoryGirl.create(:category, name: 'C2')
    @category_3 = FactoryGirl.create(:category, name: 'C3')

    @line = FactoryGirl.create(:line, family: @family)

    @line.categories << FactoryGirl.build(:line_category, category: @category, required: true)
    @line.categories << FactoryGirl.build(:line_category, category: @category_3, required: false)

    @line.save!

    @component_1 = FactoryGirl.create(:component, unit: :cm, category: @category, erasable: true, cost: BigDecimal.new('32.28'), name: 'CC1')
    @component_2 = FactoryGirl.create(:component, unit: :cm, category: @category_3, erasable: true, name: 'CC2')
    @component_3 = FactoryGirl.create(:component, unit: :cm, category: @category_3, erasable: false, name: 'CC3')
    @component_4 = FactoryGirl.create(:component, unit: :cm, category: @category_2, erasable: false, name: 'CC4')

    @line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: 5..30, 
      total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_+2', width_consumption: '_ALTURA_*3', 
      height_consumption: '_LARGURA_ + 3', quantity_consumption: '_ALTURA_*2')
    @rule = FactoryGirl.create(:rule, line_rule: @line_rule, component: @component_1, control_method: Rule.control_methods[:width], 
      width: 5..30, total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_+2', width_consumption: '_ALTURA_*3', 
      height_consumption: '_LARGURA_ + 3', quantity_consumption: '_ALTURA_*2')

    @distributor = FactoryGirl.create(:distributor, name: 'D1')
    @store       = FactoryGirl.create(:store, distributor: @distributor, name: 'S1')
    @customer    = FactoryGirl.create(:customer, store_id: @store.id, name: 'C1')

    @order  = FactoryGirl.create(:order, environments: [], seller: @store, client: @customer, billing_client: @customer, user: @factory.users.first)
    @order2 = FactoryGirl.create(:order, environments: [], seller: @store, client: @customer, billing_client: @customer, user: @factory.users.first)
    @order3 = FactoryGirl.create(:order, environments: [], seller: @store, client: @customer, billing_client: @customer, user: @factory.users.first)
  end

  describe 'GET purchase_planning' do
    let(:service) { double(:service, generate: 'foo/bar') }

    before do
      allow(PurchasePlanningService).to receive(:new).and_return(service)
    end

    it 'initializes a new Purchase Planning service' do
      expect(PurchasePlanningService).to receive(:new).with([@order.id.to_s, @order2.id.to_s])

      xhr :get, :purchase_planning, { order_ids: [@order.id, @order2.id] }
    end

    it 'generates the purchase plan' do
      expect(service).to receive(:generate)

      xhr :get, :purchase_planning, { order_ids: [@order.id, @order2.id] }
    end

    context 'with invalid data' do
      let(:service) { double(:service, generate: nil) }

      it 'returns bad request' do
        xhr :get, :purchase_planning, { order_ids: [@order.id, @order2.id] }

        expect(response).to have_http_status(:bad_request)
      end
    end

    context 'with valid data' do
      it 'returns ok status' do
        xhr :get, :purchase_planning, { order_ids: [@order.id, @order2.id] }

        expect(response).to have_http_status(:ok)
      end

      it 'renders nothing' do
        xhr :get, :purchase_planning, { order_ids: [@order.id, @order2.id] }

        expect(json['path']).to eq('foo/bar')
      end
    end
  end
end
