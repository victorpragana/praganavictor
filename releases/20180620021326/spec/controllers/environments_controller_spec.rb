# == Schema Information
#
# Table name: environments
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  category    :integer          not null
#  estimate_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe EnvironmentsController, type: :controller do
  before(:each) do
    @factory = create(:factory, name: 'F1')
    create(:administrator, username: 'F1').tap do |administrator|
      administrator.update!(selected_workplace: create(:user_workplace, workplace: @factory, user: administrator))
    end

    sign_in :administrator, @factory.users.first

    @distributor = create(:distributor, name: 'D1')
    @store       = create(:store, distributor: @distributor, name: 'S1')
    @customer    = create(:customer, store_id: @store.id, name: 'C1')
    @estimate    = create(:estimate, seller: @store, client: @customer)
  end

  describe 'GET index' do
    render_views

    before(:each) do
      @environment_1 = create(:environment, estimate: @estimate)
      @environment_2 = create(:environment, estimate: @estimate)
      @environment_3 = create(:environment, estimate: @estimate)
    end

    it 'returns the ok status' do
      xhr :get, :index, {'estimate_id' => "#{@estimate.id}"}

      expect(response).to have_http_status(:ok)
    end

    it 'returns the environments for the estimate' do
      xhr :get, :index, {'estimate_id' => "#{@estimate.id}"}

      expect(json['environments'].size).to eq(3)

      expect(json['environments'][0]['id']).to eq(@environment_1.id)
      expect(json['environments'][0]['name']).to eq(@environment_1.name)
      expect(json['environments'][0]['estimate_id']).to eq(@environment_1.estimate_id)
      expect(json['environments'][0]['category']).to eq(Environment.categories[@environment_1.category])

      expect(json['environments'][1]['id']).to eq(@environment_2.id)
      expect(json['environments'][1]['name']).to eq(@environment_2.name)
      expect(json['environments'][1]['estimate_id']).to eq(@environment_2.estimate_id)
      expect(json['environments'][1]['category']).to eq(Environment.categories[@environment_2.category])

      expect(json['environments'][2]['id']).to eq(@environment_3.id)
      expect(json['environments'][2]['name']).to eq(@environment_3.name)
      expect(json['environments'][2]['estimate_id']).to eq(@environment_3.estimate_id)
      expect(json['environments'][2]['category']).to eq(Environment.categories[@environment_3.category])
    end
  end

  describe 'GET show' do
    render_views

    before(:each) do
      @environment = create(:environment, estimate: @estimate)
    end

    it 'returns the ok status' do
      xhr :get, :show, {'id' => "#{@environment.id}"}

      expect(response).to have_http_status(:ok)
    end

    it 'returns the environment data' do
      xhr :get, :show, {'id' => "#{@environment.id}"}

      expect(json['environment']['id']).to eq(@environment.id)
      expect(json['environment']['name']).to eq(@environment.name)
      expect(json['environment']['estimate_id']).to eq(@environment.estimate_id)
      expect(json['environment']['category']).to eq(Environment.categories[@environment.category])
    end
  end

  describe 'POST create' do
    render_views

    context 'with invalid params' do
      let(:parameters) {{'category' => '', 'name' => 'Foo', 'estimate_id' => "#{@estimate.id}"}}

      it 'does not create a new Environment' do
        expect {
          xhr :post, :create, parameters
        }.to change(Environment, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :create, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) {{'category' => '0', 'name' => 'Foo', 'estimate_id' => "#{@estimate.id}"}}

      it 'initializes the enviroment service' do
        expect(EnvironmentService).to receive(:new).with(parameters).and_call_original

        xhr :post, :create, parameters
      end

      it 'creates the estimate' do
        expect_any_instance_of(EnvironmentService).to receive(:create).and_call_original
        
        xhr :post, :create, parameters
      end

      it 'returns created status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:created)
      end

      it 'renders template show' do
        xhr :post, :create, parameters

        expect(response).to render_template('environments/show')
      end
    end
  end

  describe 'PUT update' do
    render_views

    before(:each) do
      @environment = create(:environment, estimate: @estimate)
    end

    context 'with invalid params' do
      let(:parameters) {{'category' => '', 'name' => 'Foo', 'estimate_id' => "#{@estimate.id}"}}

      it 'does not create a new Environment' do
        expect {
          xhr :put, :update, parameters.merge({'id' => "#{@environment.id}"})
        }.to_not change(Environment, :count)
      end

      it 'returns unprocessable entity status' do
        xhr :put, :update, parameters.merge({'id' => "#{@environment.id}"})
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :put, :update, parameters.merge({'id' => "#{@environment.id}"})
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) {{'category' => '0', 'name' => 'Foo', 'estimate_id' => "#{@estimate.id}"}}

      it 'initializes the environment service' do
        expect(EnvironmentService).to receive(:new).with(parameters).and_call_original

        xhr :put, :update, parameters.merge({'id' => "#{@environment.id}"})
      end

      it 'updates the environment' do
        expect_any_instance_of(EnvironmentService).to receive(:update).with(@environment.id.to_s).and_call_original
        
        xhr :put, :update, parameters.merge({'id' => "#{@environment.id}"})
      end

      it 'returns ok status' do
        xhr :put, :update, parameters.merge({'id' => "#{@environment.id}"})
        
        expect(response).to have_http_status(:ok)
      end

      it 'renders template show' do
        xhr :put, :update, parameters.merge({'id' => "#{@environment.id}"})

        expect(response).to render_template('environments/show')
      end
    end
  end

  describe 'DELETE destroy' do
    before(:each) do
      @environment = create(:environment, estimate: @estimate)

      @auth_headers = @factory.users.first.create_new_auth_token
    end

    context 'with success' do
      it 'destroys the environment' do
        expect {
          xhr :delete, :destroy, { id: @environment.id }.merge(@auth_headers)
        }.to change(Environment, :count).by(-1)
      end

      it 'returns no content status' do
        xhr :delete, :destroy, { id: @environment.id }.merge(@auth_headers)

        expect(response).to have_http_status(:no_content)
      end
    end

    context 'with error' do
      let(:environment) {mock_model(Environment, destroy: false, errors: double(:errors, full_messages: ['foo']))}

      before(:each) do
        allow(Environment).to receive(:find).and_return(environment)
      end

      it 'does not destroy the environment' do
        expect {
          xhr :delete, :destroy, {:id => environment.id}
        }.to_not change(Environment, :count)
      end

      it 'returns the bad request status' do
        xhr :delete, :destroy, {:id => environment.id}

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the errors' do
        xhr :delete, :destroy, {:id => environment.id}
        
        expect(json['errors']).to_not be_empty
      end
    end
  end
end
