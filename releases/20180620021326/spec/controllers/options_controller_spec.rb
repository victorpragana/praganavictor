# == Schema Information
#
# Table name: options
#
#  id                :integer          not null, primary key
#  line_id           :integer
#  location_id       :integer
#  width             :integer          not null
#  height            :integer          not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  selected          :boolean          default(FALSE), not null
#  factory_total     :decimal(10, 2)   default(0.0)
#  distributor_total :decimal(10, 2)   default(0.0)
#  store_total       :decimal(10, 2)   default(0.0)
#  customer_total    :decimal(10, 2)   default(0.0)
#  discount          :decimal(5, 2)    default(0.0)
#  questions         :json
#

require 'rails_helper'

RSpec.describe OptionsController, type: :controller do
  before do
    @factory = create(:factory, name: 'F1')

    create(:administrator, username: 'F1').tap do |administrator|
      administrator.update!(selected_workplace: create(:user_workplace, workplace: @factory, user: administrator))
    end

    sign_in :administrator, @factory.users.first
    
    @family = create(:family)
    
    @category = create(:category, name: 'C1')
    @category_2 = create(:category, name: 'C2')
    @category_3 = create(:category, name: 'C3')

    @line = create(:line, family: @family)

    @line.categories << build(:line_category, category: @category, required: true)
    @line.categories << build(:line_category, category: @category_3, required: false)

    @line.save!

    @component_1 = create(:component, category: @category, erasable: true, cost: BigDecimal.new('32.28'), name: 'C1')
    @component_2 = create(:component, category: @category_3, erasable: true, name: 'C2')
    @component_3 = create(:component, category: @category_3, erasable: false, name: 'C3')
    @component_4 = create(:component, category: @category_2, erasable: false, name: 'C4')

    @line_rule = create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: 5..30, 
      total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_+2', width_consumption: '_ALTURA_*3', height_consumption: '_LARGURA_ + 3')
    create(:rule, line_rule: @line_rule, component: @component_1, control_method: Rule.control_methods[:width], 
      width: 5..30, total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_+2', width_consumption: '_ALTURA_*3', 
      height_consumption: '_LARGURA_ + 3')
    create(:rule, line_rule: @line_rule, component: @component_3, control_method: Rule.control_methods[:width], 
      width: 5..30, total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_+2', width_consumption: '_ALTURA_*3', 
      height_consumption: '_LARGURA_ + 3')

    @distributor = create(:distributor, name: 'D1')
    @store       = create(:store, distributor: @distributor, name: 'S1')
    @customer    = create(:customer, store_id: @store.id, name: 'C1')
    @estimate    = create(:estimate, seller: @store, client: @customer)
    @environment = create(:environment, estimate: @estimate)
    @location    = create(:location, environment: @environment)
  end

  describe 'GET index' do
    render_views

    let(:questions) { { 'FOO' => true, 'BAR' => 'bar' } }

    before do
      @option_1 = create(:option, location: @location, line: @line, components: [
        build(:option_component, component_name: @component_1.name, component_characteristics: @component_1.characteristics, 
          category_name: @component_1.category.name, unit: @component_1.unit, total_consumption: 100, unit_value: 250, width_consumption: 20, 
          height_consumption: 30, quantity_consumption: 100, required: true, total: 25000)
      ])
      @option_2 = create(:option, location: @location, line: @line, selected: true, questions: questions)

      @question1 = create(:question, value: 'FOO', type: :boolean)
      @question2 = create(:question, value: 'BAR', type: :dropdown, options: [{ 'label' => 'Bar', 'value' => 'bar' }])
    end

    it 'returns the ok status' do
      xhr :get, :index, {'location_id' => "#{@location.id}"}

      expect(response).to have_http_status(:ok)
    end

    it 'returns the locations for the location' do
      xhr :get, :index, {'location_id' => "#{@location.id}"}

      expect(json['options'].size).to eq(2)

      expect(json['options'][0]['id']).to eq(@option_1.id)
      expect(json['options'][0]['line']['id']).to eq(@option_1.line.id)
      expect(json['options'][0]['line']['name']).to eq(@option_1.line.name)
      expect(json['options'][0]['line']['family']['id']).to eq(@option_1.line.family.id)
      expect(json['options'][0]['line']['family']['name']).to eq(@option_1.line.family.name)
      expect(json['options'][0]['line']['family']['color']).to eq(@option_1.line.family.color)
      expect(json['options'][0]['width']).to eq(@option_1.width_from_mm_to_cm)
      expect(json['options'][0]['height']).to eq(@option_1.height_from_mm_to_cm)
      expect(json['options'][0]['total'].to_f).to eq(@option_1.total)
      expect(json['options'][0]['selected']).to eq(@option_1.selected)
      expect(json['options'][0]['components']).to_not be_empty
      expect(json['options'][0]['components'].size).to eq(1)

      expect(json['options'][0]['components'][0]['component']['name']).to eq(@component_1.name)
      expect(json['options'][0]['components'][0]['component']['characteristics']).to eq(@component_1.characteristics)
      expect(json['options'][0]['components'][0]['component']['unit']).to eq(Component.units[@component_1.unit])
      expect(json['options'][0]['components'][0]['component']['category']['name']).to eq(@category.name)
      expect(json['options'][0]['components'][0]['total_consumption']).to eq(BigDecimal.new('100'))
      expect(json['options'][0]['components'][0]['width_consumption']).to eq(BigDecimal.new('20'))
      expect(json['options'][0]['components'][0]['height_consumption']).to eq(BigDecimal.new('30'))
      expect(json['options'][0]['components'][0]['quantity_consumption']).to eq(BigDecimal.new('100'))
      expect(json['options'][0]['components'][0]['unit_value']).to eq(BigDecimal.new('250'))
      expect(json['options'][0]['components'][0]['total']).to eq(BigDecimal.new('25000'))
      expect(json['options'][0]['components'][0]['required']).to eq(true)

      expect(json['options'][1]['id']).to eq(@option_2.id)
      expect(json['options'][1]['line']['id']).to eq(@option_2.line.id)
      expect(json['options'][1]['line']['name']).to eq(@option_2.line.name)
      expect(json['options'][1]['line']['family']['id']).to eq(@option_2.line.family.id)
      expect(json['options'][1]['line']['family']['name']).to eq(@option_2.line.family.name)
      expect(json['options'][1]['line']['family']['color']).to eq(@option_2.line.family.color)
      expect(json['options'][1]['width']).to eq(@option_2.width_from_mm_to_cm)
      expect(json['options'][1]['height']).to eq(@option_2.height_from_mm_to_cm)
      expect(json['options'][1]['total'].to_f).to eq(@option_2.total)
      expect(json['options'][1]['selected']).to eq(@option_2.selected)
      expect(json['options'][1]['questions'][0]['label']).to eq(@question1.label)
      expect(json['options'][1]['questions'][0]['value']).to eq(true)
      expect(json['options'][1]['questions'][1]['label']).to eq(@question2.label)
      expect(json['options'][1]['questions'][1]['value']).to eq(@question2.options[0]['value'])
    end
  end

  describe 'GET show' do
    render_views

    before do
      item_component_1 = build(:option_component, component_name: @component_1.name, component_characteristics: @component_1.characteristics, 
        category_name: @component_1.category.name, unit: @component_1.unit, total_consumption: 100, unit_value: 250, width_consumption: 20, 
        height_consumption: 30, quantity_consumption: 100, required: true, total: 25000)
      item_component_2 = build(:option_component, component_name: @component_2.name, component_characteristics: @component_2.characteristics, 
        category_name: @component_2.category.name, unit: @component_2.unit, total_consumption: 20, unit_value: 65, required: false, total: 1300)
      @option = create(:option, location: @location, line: @line, components: [item_component_1, item_component_2], selected: true)
    end

    it 'returns the ok status' do
      xhr :get, :show, {'id' => "#{@option.id}"}

      expect(response).to have_http_status(:ok)
    end

    it 'returns the option data' do
      xhr :get, :show, {'id' => "#{@option.id}"}

      expect(json['option']['id']).to eq(@option.id)
      expect(json['option']['location_id']).to eq(@option.location_id)
      expect(json['option']['line']['id']).to eq(@option.line.id)
      expect(json['option']['line']['name']).to eq(@option.line.name)
      expect(json['option']['line']['family']['id']).to eq(@option.line.family.id)
      expect(json['option']['line']['family']['name']).to eq(@option.line.family.name)
      expect(json['option']['line']['family']['color']).to eq(@option.line.family.color)
      expect(json['option']['width']).to eq(@option.width_from_mm_to_cm)
      expect(json['option']['height']).to eq(@option.height_from_mm_to_cm)
      expect(json['option']['total'].to_f).to eq(@option.total)
      expect(json['option']['selected']).to eq(@option.selected)
      expect(json['option']['components']).to_not be_empty
      expect(json['option']['components'].size).to eq(2)

      expect(json['option']['components'][1]['component']['name']).to eq(@component_1.name)
      expect(json['option']['components'][1]['component']['characteristics']).to eq(@component_1.characteristics)
      expect(json['option']['components'][1]['component']['unit']).to eq(Component.units[@component_1.unit])
      expect(json['option']['components'][1]['component']['category']['name']).to eq(@category.name)
      expect(json['option']['components'][1]['total_consumption']).to eq(BigDecimal.new('100'))
      expect(json['option']['components'][1]['width_consumption']).to eq(BigDecimal.new('20'))
      expect(json['option']['components'][1]['height_consumption']).to eq(BigDecimal.new('30'))
      expect(json['option']['components'][1]['quantity_consumption']).to eq(BigDecimal.new('100'))
      expect(json['option']['components'][1]['unit_value']).to eq(BigDecimal.new('250'))
      expect(json['option']['components'][1]['total']).to eq(BigDecimal.new('25000'))
      expect(json['option']['components'][1]['required']).to eq(true)
      
      expect(json['option']['components'][0]['component']['name']).to eq(@component_2.name)
      expect(json['option']['components'][0]['component']['characteristics']).to eq(@component_2.characteristics)
      expect(json['option']['components'][0]['component']['unit']).to eq(Component.units[@component_2.unit])
      expect(json['option']['components'][0]['component']['category']['name']).to eq(@category_3.name)
      expect(json['option']['components'][0]['total_consumption']).to eq(BigDecimal.new('20'))
      expect(json['option']['components'][0]['width_consumption']).to be_nil
      expect(json['option']['components'][0]['height_consumption']).to be_nil
      expect(json['option']['components'][0]['quantity_consumption']).to be_nil
      expect(json['option']['components'][0]['unit_value']).to eq(BigDecimal.new('65'))
      expect(json['option']['components'][0]['total']).to eq(BigDecimal.new('1300'))
      expect(json['option']['components'][0]['required']).to eq(false)
    end
  end

  describe 'POST create' do
    render_views

    context 'with invalid data' do
      let(:parameters) {{'line_id' => "#{@line.id}", 'width' => '10', 'height' => '12', 'location_id' => '',
        'components' => [
          {'component_ids' => ["#{@component_1.id}"]}, {'component_ids' => ["#{@component_3.id}"]}
        ]
      }}

      it 'does not create a new Option' do
        expect {
          xhr :post, :create, parameters
        }.to_not change(Option, :count)
      end

      it 'does not create a new Option Components' do
        expect {
          xhr :post, :create, parameters
        }.to_not change(OptionComponent, :count)
      end

      it 'returns unprocessable entity status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :create, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) { { 'line_id' => "#{@line.id}", 'width' => '10', 'height' => '12', 'quantity' => '1',
        'location_id' => "#{@location.id}", "questions" => { "LOCAL_INSTALACAO" => "banheiro", "MAO_DE_OBRA" => true, "FRETE" => true },
        'components' => [
          { 'component_ids' => ["#{@component_1.id}"], 'total_consumption' => '100' }, { 'component_ids' => ["#{@component_3.id}"] }
        ]
      } }

      let(:parsed_parameters) { { 'line_id' => "#{@line.id}", 'width' => 100, 'height' => 120, 'quantity' => '1',
        'location_id' => "#{@location.id}", "questions" => { "LOCAL_INSTALACAO" => "banheiro", "MAO_DE_OBRA" => true, "FRETE" => true },
        'components' => [
          { 'component_ids' => ["#{@component_1.id}"], 'total_consumption' => '100' }, { 'component_ids' => ["#{@component_3.id}"] }
        ]
      } }

      before do
        @question1 = create(:question, value: 'LOCAL_INSTALACAO', type: :dropdown, options: [{ 'label' => 'Banheiro', 'value' => 'banheiro' }, { 'label' => 'Cozinha', 'value' => 'cozinha' }])
        @question2 = create(:question, value: 'MAO_DE_OBRA', type: :boolean)
        @question3 = create(:question, value: 'FRETE', type: :boolean)
      end

      it 'initializes the option service' do
        expect(OptionService).to receive(:new).with(parsed_parameters).and_call_original

        xhr :post, :create, parameters
      end

      it 'creates the option' do
        expect_any_instance_of(OptionService).to receive(:create).and_call_original
        
        xhr :post, :create, parameters
      end

      it 'returns created status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:created)
      end

      it 'renders template show' do
        xhr :post, :create, parameters

        expect(response).to render_template('options/show')
      end
    end
  end

  describe 'PUT update' do
    render_views

    before do
      @option = create(:option, location: @location, line: @line)
    end

    context 'with invalid data' do
      let(:parameters) { { 'line_id' => "#{@line.id}", 'width' => '', 'height' => '12', 'location_id' => '', 
        'components' => [
          { 'component_ids' => ["#{@component_1.id}"] }, { 'component_ids' => ["#{@component_3.id}"] }
        ]
      }.merge('id' => "#{@option.id}") }

      it 'does not create a new Option' do
        expect {
          xhr :put, :update, parameters
        }.to_not change(Option, :count)
      end

      it 'does not create a new Option Components' do
        expect {
          xhr :put, :update, parameters
        }.to_not change(OptionComponent, :count)
      end

      it 'returns unprocessable entity status' do
        xhr :put, :update, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :put, :update, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) { { 'line_id' => "#{@line.id}", 'width' => '10', 'height' => '12', 'quantity' => '1', 
        'location_id' => "#{@location.id}", "questions" => { "LOCAL_INSTALACAO" => "banheiro", "MAO_DE_OBRA" => true, "FRETE" => true },
        'components' => [
          { 'component_ids' => ["#{@component_1.id}"] }, { 'component_ids' => ["#{@component_3.id}"] }
        ]
      } }

      let(:parsed_parameters)  { { 'line_id' => "#{@line.id}", 'width' => 100, 'height' => 120, 'quantity' => '1',
        'location_id' => "#{@location.id}", "questions" => { "LOCAL_INSTALACAO" => "banheiro", "MAO_DE_OBRA" => true, "FRETE" => true },
        'components' => [
          { 'component_ids' => ["#{@component_1.id}"] }, { 'component_ids' => ["#{@component_3.id}"] }
        ]
      } }

      before do
        @question1 = create(:question, value: 'LOCAL_INSTALACAO', type: :dropdown, options: [{ 'label' => 'Banheiro', 'value' => 'banheiro' }, { 'label' => 'Cozinha', 'value' => 'cozinha' }])
        @question2 = create(:question, value: 'MAO_DE_OBRA', type: :boolean)
        @question3 = create(:question, value: 'FRETE', type: :boolean)
      end

      it 'initializes the option service' do
        expect(OptionService).to receive(:new).with(parsed_parameters).and_call_original

        xhr :put, :update, parameters.merge({'id' => "#{@option.id}"})
      end

      it 'updates the option' do
        expect_any_instance_of(OptionService).to receive(:update).with(@option.id.to_s).and_call_original
        
        xhr :put, :update, parameters.merge({'id' => "#{@option.id}"})
      end

      it 'returns ok status' do
        xhr :put, :update, parameters.merge({'id' => "#{@option.id}"})
        
        expect(response).to have_http_status(:ok)
      end

      it 'renders template show' do
        xhr :put, :update, parameters.merge({'id' => "#{@option.id}"})

        expect(response).to render_template('options/show')
      end
    end
  end

  describe 'DELETE destroy' do
    before do
      @option = create(:option, location: @location, line: @line)

      @auth_headers = @factory.users.first.create_new_auth_token
    end

    context 'with success' do
      it 'destroys the option' do
        expect {
          xhr :delete, :destroy, { id: @option.id }.merge(@auth_headers)
        }.to change(Option, :count).by(-1)
      end

      it 'initializes the estimate service' do
        expect(EstimateService).to receive(:new).and_call_original

        xhr :delete, :destroy, { id: @option.id }.merge(@auth_headers)
      end

      it 'updates the estimate prices' do
        expect_any_instance_of(EstimateService).to receive(:update_prices).with(@estimate.id).and_call_original

        xhr :delete, :destroy, { id: @option.id }.merge(@auth_headers)
      end

      it 'returns no content status' do
        xhr :delete, :destroy, { id: @option.id }.merge(@auth_headers)

        expect(response).to have_http_status(:no_content)
      end
    end

    context 'with error' do
      let(:option) { mock_model(Option, destroy: false, errors: double(:errors, full_messages: ['foo'])) }

      before do
        allow(Option).to receive(:find).and_return(option)
      end

      it 'does not destroy the option' do
        expect {
          xhr :delete, :destroy, { id: option.id }
        }.to_not change(Option, :count)
      end

      it 'returns the bad request status' do
        xhr :delete, :destroy, { id: option.id }

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the errors' do
        xhr :delete, :destroy, { id: option.id }
        
        expect(json['errors']).to_not be_empty
      end
    end
  end
end
