# == Schema Information
#
# Table name: printers
#
#  id          :integer          not null, primary key
#  name        :string           not null
#  real_name   :string           not null
#  server_name :string           not null
#  server_id   :string           not null
#  default     :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe PrintersController, type: :controller do
  before do
    @factory = create(:factory, name: 'F1')

    create(:administrator, username: 'F1').tap do |administrator|
      administrator.update!(selected_workplace: create(:user_workplace, workplace: @factory, user: administrator))
    end

    sign_in :administrator, @factory.users.first
  end
  
  describe "GET index" do
    render_views

    before do
      @printer1 = create(:printer, name: 'b')
      @printer2 = create(:printer, name: 'a')
      @printer3 = create(:printer, name: 'c')
    end

    it "returns OK" do
      xhr :get, :index

      expect(response).to have_http_status(:ok)
    end
  end

  describe "POST create" do
    render_views

    context "with invalid params" do
      let(:parameters) { { "name" => "", "real_name" => "", 'server_name' => '', 'server_id' => '' } }

      it "does not create a new Printer" do
        expect {
          xhr :post, :create, parameters
        }.to_not change(Printer, :count)
      end

      it "returns unprocessable entity status" do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "returns the errors" do
        xhr :post, :create, parameters
        
        expect(json["errors"]).to_not be_empty
      end
    end

    context "with valid params" do
      let(:parameters) { { "name" => "Foo", "real_name" => "bar", 'server_name' => 'batz', 'server_id' => '123', 'default' => 'true' } }

      it "initializes the printer service" do
        expect(PrinterService).to receive(:new).with(parameters).and_call_original

        xhr :post, :create, parameters
      end

      it "creates the printer" do
        expect_any_instance_of(PrinterService).to receive(:create).and_call_original
        
        xhr :post, :create, parameters
      end

      it "returns created status" do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:created)
      end
    end
  end

  describe "GET show" do
    render_views

    before do
      @printer = create(:printer, name: 'b')
    end

    it "returns ok status" do
      xhr :get, :show, id: @printer.id

      expect(response).to have_http_status(:ok)
    end

    it "returns the printer data" do
      xhr :get, :show, id: @printer.id

      expect(json["printer"]["id"]).to eq(@printer.id)
      expect(json["printer"]["name"]).to eq(@printer.name)
      expect(json["printer"]["real_name"]).to eq(@printer.real_name)
      expect(json["printer"]["server_name"]).to eq(@printer.server_name)
      expect(json["printer"]["server_id"]).to eq(@printer.server_id)
      expect(json["printer"]["default"]).to eq(@printer.default?)
    end
  end

  describe "PUT update" do
    render_views

    before do
      @printer = create(:printer, name: 'b')
    end

    context "with invalid params" do
      let(:parameters) { { "name" => "", "real_name" => "", 'server_name' => '', 'server_id' => '' }.merge('id' => @printer.id.to_s) }

      it "does not create a new Printer" do
        expect {
          xhr :put, :update, parameters
        }.to change(Printer, :count).by(0)
      end

      it "returns unprocessable entity status" do
        xhr :put, :update, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "returns the errors" do
        xhr :put, :update, parameters
        
        expect(json["errors"]).to_not be_empty
      end
    end

    context "with valid params" do
      let(:parameters) { { "name" => "Bar", "real_name" => "batz", 'server_name' => 'faz', 'server_id' => 'ter' } }

      it "initializes the printer service" do
        expect(PrinterService).to receive(:new).with(parameters).and_call_original

        xhr :put, :update, parameters.merge({"id" => "#{@printer.id}"})
      end

      it "should update the printer" do
        expect_any_instance_of(PrinterService).to receive(:update).with(@printer.id.to_s).and_call_original
        
        xhr :put, :update, parameters.merge({"id" => "#{@printer.id}"})
      end

      it "returns ok status" do
        xhr :put, :update, parameters.merge({"id" => "#{@printer.id}"})
        
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe "DELETE destroy" do
    before do
      @printer = create(:printer, name: 'b')
    end

    context "with success" do
      it "destroys the printer" do
        expect {
          xhr :delete, :destroy, {id: @printer.id}
        }.to change(Printer, :count).by(-1)
      end

      it "returns no content status" do
        xhr :delete, :destroy, {id: @printer.id}

        expect(response).to have_http_status(:no_content)
      end
    end

    context "with error" do
      let(:printer) { mock_model(Printer, destroy: false, errors: double(:errors, full_messages: ["foo"])) }

      before do
        allow(Printer).to receive(:find).and_return(printer)
      end

      it "does not destroy the printer" do
        expect {
          xhr :delete, :destroy, { id: printer.id }
        }.to_not change(Printer, :count)
      end

      it "returns the bad request status" do
        xhr :delete, :destroy, { id: printer.id }

        expect(response).to have_http_status(:bad_request)
      end

      it "returns the errors" do
        xhr :delete, :destroy, { id: printer.id }
        
        expect(json["errors"]).to_not be_empty
      end
    end
  end
end
