# == Schema Information
#
# Table name: estimates
#
#  id                 :integer          not null, primary key
#  date               :date             not null
#  observation        :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  order_id           :integer
#  seller_id          :integer
#  seller_type        :string
#  client_id          :integer
#  client_type        :string
#  optimize           :boolean          default(FALSE)
#  discount           :decimal(5, 2)    default(0.0)
#  status             :integer          default(0), not null
#  job_id             :string
#  processing_message :text             default([]), is an Array
#

require 'rails_helper'

RSpec.describe EstimatesController, type: :controller do
  before do
    @factory = create(:factory, name: 'F1')
    create(:administrator, username: 'F1').tap do |administrator|
      administrator.update!(selected_workplace: create(:user_workplace, workplace: @factory, user: administrator))
    end

    sign_in :administrator, @factory.users.first

    @distributor = create(:distributor, name: 'D1')
    @store       = create(:store, name: 'S1', distributor: @distributor)
    @customer    = create(:customer, name: 'C1', store_id: @store.id)
  end

  describe 'GET index' do
    render_views

    before do
      @estimate_1 = create(:estimate, seller: @store, client: @customer)
      @estimate_2 = create(:estimate, seller: @store, client: @customer)
      @estimate_3 = create(:estimate, seller: @store, client: @customer)
      @estimate_4 = create(:estimate, seller: @store, client: @customer)
    end

    it 'returns OK' do
      xhr :get, :index

      expect(response).to have_http_status(:ok)
    end
  end

  describe 'GET show' do
    render_views

    before do
      @estimate = create(:estimate, seller: @store, client: @customer, status:  Estimate.statuses.to_a.sample[1])
    end

    it 'returns the ok status' do
      xhr :get, :show, {'id' => "#{@estimate.id}"}

      expect(response).to have_http_status(:ok)
    end

    it 'returns the estimate data' do
      xhr :get, :show, {'id' => "#{@estimate.id}"}

      expect(json['estimate']['id']).to eq(@estimate.id)
      expect(json['estimate']['observation']).to eq(@estimate.observation)
      expect(json['estimate']['order_id']).to eq(@estimate.order_id)
      expect(json['estimate']['seller_id']).to eq(@estimate.seller_id)
      expect(json['estimate']['seller_type']).to eq(@estimate.seller_type)
      expect(json['estimate']['client_id']).to eq(@estimate.client_id)
      expect(json['estimate']['client_type']).to eq(@estimate.client_type)
      expect(json['estimate']['date']).to eq(@estimate.date.strftime('%d/%m/%Y'))
      expect(json['estimate']['optimize']).to eq(@estimate.optimize)
      expect(json['estimate']['discount']).to eq(@estimate.discount.to_f)
      expect(json['estimate']['status']).to eq(Estimate.statuses[@estimate.status])
      expect(json['estimate']['message']).to eq(@estimate.processing_message)
    end
  end

  describe 'POST create' do
    render_views

    context 'with invalid params' do
      let(:parameters) { {'date' => '', 'client_id' => "#{@customer.id}", 'client_type' => 'Customer'} }

      it 'does not create a new Estimate' do
        expect {
          xhr :post, :create, parameters
        }.to change(Estimate, :count).by(0)
      end

      it 'returns unprocessable entity status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :create, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) { {'date' => '12/02/2016', 'client_id' => "#{@customer.id}", 'client_type' => 'Customer'} }

      it 'initializes the estimate service' do
        expect(EstimateService).to receive(:new).with(parameters.merge({'seller' => @factory})).and_call_original

        xhr :post, :create, parameters
      end

      it 'creates the estimate' do
        expect_any_instance_of(EstimateService).to receive(:create).and_call_original
        
        xhr :post, :create, parameters
      end

      it 'returns created status' do
        xhr :post, :create, parameters
        
        expect(response).to have_http_status(:created)
      end

      it 'renders template show' do
        xhr :post, :create, parameters

        expect(response).to render_template('estimates/show')
      end
    end
  end

  describe 'PUT update' do
    render_views

    before do
      @estimate = create(:estimate, seller: @factory, client: @customer)
    end

    context 'with invalid params' do
      let(:parameters) { {'date' => '', 'client_id' => "#{@customer.id}", 'client_type' => 'Customer'}.merge({'id' => "#{@estimate.id}"}) }

      it 'does not create a new Estimate' do
        expect {
          xhr :put, :update, parameters
        }.to_not change(Estimate, :count)
      end

      it 'returns unprocessable entity status' do
        xhr :put, :update, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :put, :update, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      let(:parameters) { {'date' => '12/02/2016', 'client_id' => "#{@customer.id}", 'client_type' => 'Customer', 'optimize' => 'true'} }

      it 'initializes the estimate service' do
        expect(EstimateService).to receive(:new).with(parameters.merge({'seller' => @factory})).and_call_original

        xhr :put, :update, parameters.merge({'id' => "#{@estimate.id}"})
      end

      it 'updates the estimate' do
        expect_any_instance_of(EstimateService).to receive(:update).with(@estimate.id.to_s).and_call_original
        
        xhr :put, :update, parameters.merge({'id' => "#{@estimate.id}"})
      end

      it 'returns ok status' do
        xhr :put, :update, parameters.merge({'id' => "#{@estimate.id}"})
        
        expect(response).to have_http_status(:ok)
      end

      it 'renders template show' do
        xhr :put, :update, parameters.merge({'id' => "#{@estimate.id}"})

        expect(response).to render_template('estimates/show')
      end
    end
  end

  describe 'PUT calculate' do
    render_views

    before do
      @estimate = create(:estimate, seller: @factory, client: @customer)
    end

    context 'with valid params' do
      context 'when the estimate is \'free\' or \'error\'' do
        before do
          @estimate.update(status: [Estimate.statuses[:free], Estimate.statuses[:error]].sample)

          allow(EstimatePriceCalculatorWorker).to receive(:perform_async).and_return('123AAA')
        end

        it 'initializes the estimate service' do
          expect(EstimateService).to receive(:new).with({'option_ids' => @estimate.option_ids, 'optimize' => true}).and_call_original

          xhr :put, :calculate, { 'id' => "#{@estimate.id}", 'optimize' => true }
        end

        it 'updates the estimate' do
          expect_any_instance_of(EstimateService).to receive(:update).with(@estimate.id.to_s).and_call_original
          
          xhr :put, :calculate, { 'id' => "#{@estimate.id}", 'optimize' => true }
        end

        it 'changes the estimate status to \'enqueued\'' do
          xhr :put, :calculate, { 'id' => "#{@estimate.id}", 'optimize' => true }

          expect(Estimate.statuses[@estimate.reload.status]).to eq(Estimate.statuses[:enqueued])
        end

        it 'updates the estimate job id' do
          expect do
            xhr :put, :calculate, { 'id' => "#{@estimate.id}", 'optimize' => true }
            @estimate.reload
          end.to change(@estimate, :job_id).to('123AAA')
        end

        it 'enqueues the estimate price calculation' do
          expect(EstimatePriceCalculatorWorker).to receive(:perform_async).with(@estimate.id)

          xhr :put, :calculate, { 'id' => "#{@estimate.id}", 'optimize' => true }
        end

        it 'returns ok status' do
          xhr :put, :calculate, { 'id' => "#{@estimate.id}", 'optimize' => true }

          expect(response).to have_http_status(:ok)
        end

        it 'returns the job id' do
          xhr :put, :calculate, { 'id' => "#{@estimate.id}", 'optimize' => true }

          expect(json['estimate']['job_id']).to eq('123AAA')
        end
      end

      context 'when the estimate is \'enqueued\'' do
        before do
          @estimate.update(status: Estimate.statuses[:enqueued])
        end

        it 'returns bad request status' do
          xhr :put, :calculate, { 'id' => "#{@estimate.id}", 'optimize' => true }

          expect(response).to have_http_status(:bad_request)
        end

        it 'returns the enqueued message' do
          xhr :put, :calculate, { 'id' => "#{@estimate.id}", 'optimize' => true }

          expect(json['errors']).to match_array([I18n.t('activerecord.errors.models.estimate.enqueued')])
        end
      end

      context 'when the estimate is \'processing\'' do
        before do
          @estimate.update(status: Estimate.statuses[:processing])
        end

        it 'returns bad request status' do
          xhr :put, :calculate, { 'id' => "#{@estimate.id}", 'optimize' => true }

          expect(response).to have_http_status(:bad_request)
        end

        it 'returns the processing message' do
          xhr :put, :calculate, { 'id' => "#{@estimate.id}", 'optimize' => true }

          expect(json['errors']).to match_array([I18n.t('activerecord.errors.models.estimate.processing')])
        end
      end
    end
  end

  describe 'POST duplicate' do
    render_views

    before do
      @estimate = create(:estimate, seller: @factory, client: @customer)
    end

    context 'with invalid params' do
      let(:service) { double(:service, duplicate: false, success: false, errors: ['foo']) }
      let(:parameters) { {'id' => "#{@estimate.id}"} }

      before do
        allow(EstimateService).to receive(:new).and_return(service)
      end

      it 'does not create a new Estimate' do
        expect {
          xhr :post, :duplicate, parameters
        }.to_not change(Estimate, :count)
      end

      it 'returns unprocessable entity status' do
        xhr :post, :duplicate, parameters
        
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the errors' do
        xhr :post, :duplicate, parameters
        
        expect(json['errors']).to_not be_empty
      end
    end

    context 'with valid params' do
      it 'initializes the estimate service' do
        expect(EstimateService).to receive(:new).and_call_original

        xhr :post, :duplicate, {'id' => "#{@estimate.id}"}
      end

      it 'should duplicate the estimate' do
        expect_any_instance_of(EstimateService).to receive(:duplicate).with(@estimate.id.to_s).and_call_original
        
        xhr :post, :duplicate, {'id' => "#{@estimate.id}"}
      end

      it 'returns ok status' do
        xhr :post, :duplicate, {'id' => "#{@estimate.id}"}
        
        expect(response).to have_http_status(:ok)
      end

      it 'renders template show' do
        xhr :post, :duplicate, {'id' => "#{@estimate.id}"}

        expect(response).to render_template('estimates/show')
      end
    end
  end

  describe 'PUT apply_discount' do
    render_views

    before do
      create(:administrator, username: 'A1', maximum_discount: BigDecimal.new('12.50')).tap do |administrator|
        administrator.update!(selected_workplace: create(:user_workplace, workplace: @store, user: administrator))
      end

      sign_in :administrator, @store.users.first

      @family = create(:family)
      @line = create(:line, family: @family)
      @estimate = create(:estimate, seller: @factory, client: @customer)
    end

    context 'with invalid data' do
      context 'for unallowed values' do
        let(:parameters) { {'discount' => '-1'}.merge('id' => "#{@estimate.id}") }

        it 'does not update the estimate discount' do
          expect {
            xhr :put, :apply_discount, parameters
            @estimate.reload
          }.to_not change(@estimate, :discount)
        end

        it 'returns unprocessable entity status' do
          xhr :put, :apply_discount, parameters
          
          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'returns the errors' do
          xhr :put, :apply_discount, parameters
          
          expect(json['errors']).to_not be_empty
        end
      end

      context 'for discount greater than the allowed for the user' do
        let(:parameters) { {'discount' => '12.51'}.merge('id' => "#{@estimate.id}") }

        it 'does not update the estimate discount' do
          expect {
            xhr :put, :apply_discount, parameters
            @estimate.reload
          }.to_not change(@estimate, :discount)
        end

        it 'returns unprocessable entity status' do
          xhr :put, :apply_discount, parameters
          
          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'returns the errors' do
          xhr :put, :apply_discount, parameters
          
          expect(json['errors']).to_not be_empty
        end
      end
    end

    context 'with valid params' do
      let(:parameters) { {'discount' => '10.58'} }

      it 'initializes the estimate service' do
        expect(EstimateService).to receive(:new).with(parameters).and_call_original

        xhr :put, :apply_discount, parameters.merge('id' => "#{@estimate.id}")
      end

      it 'should apply the discount to the estimate' do
        expect_any_instance_of(EstimateService).to receive(:apply_discount).with(@estimate.id.to_s).and_call_original
        
        xhr :put, :apply_discount, parameters.merge('id' => "#{@estimate.id}")
      end

      it 'returns no content status' do
        xhr :put, :apply_discount, parameters.merge('id' => "#{@estimate.id}")
        
        expect(response).to have_http_status(:no_content)
      end

      it 'renders nothing' do
        xhr :put, :apply_discount, parameters.merge('id' => "#{@estimate.id}")

        expect(response.body).to be_blank
      end
    end
  end
end
