# == Schema Information
#
# Table name: price_tables
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  owner_id   :integer
#  owner_type :string
#  name       :string           default(""), not null
#  type       :integer          default(0), not null
#  default    :boolean          default(FALSE)
#

require 'rails_helper'

RSpec.describe PriceTablesController, type: :controller do
  before do
    @factory = create(:factory, name: 'F1')
    create(:administrator, username: 'admin_f').tap do |administrator|
      administrator.update!(selected_workplace: create(:user_workplace, workplace: @factory, user: administrator))
    end

    @distributor = create(:distributor, name: 'D1')
    create(:administrator, username: 'admin_d').tap do |administrator|
      administrator.update!(selected_workplace: create(:user_workplace, workplace: @distributor, user: administrator))
    end

    @store = create(:store, name: 'S1', distributor: @distributor)
    create(:administrator, username: 'admin_s').tap do |administrator|
      administrator.update!(selected_workplace: create(:user_workplace, workplace: @store, user: administrator))
    end

    @user = [@factory.users.first, @distributor.users.first, @store.users.first].sample

    sign_in @user.class.to_s.underscore.to_sym, @user
  end

  describe 'GET index' do
    render_views

    before do
      @distributor_2 = create(:distributor, name: 'D2')

      @price_table_1 = create(:price_table, name: 'P1', owner: @user.workplace)
      @price_table_2 = create(:price_table, name: 'P2', owner: @user.workplace)
      @price_table_3 = create(:price_table, name: 'P3', owner: @distributor_2)
      @price_table_4 = create(:price_table, name: 'P4', owner: @user.workplace)
    end

    it 'returns OK' do
      xhr :get, :index

      expect(response).to have_http_status(:ok)
    end

    context 'with datatable search parameters' do
      let(:params) { {'draw'=>'1', 'columns'=>{'0'=>{'data'=>'0', 'name'=>'', 'searchable'=>'false', 'orderable'=>'false', 
        'search'=>{'value'=>'', 'regex'=>'false'}}, '1'=>{'data'=>'1', 'name'=>'id', 'searchable'=>'true', 'orderable'=>'true', 
        'search'=>{'value'=>'', 'regex'=>'false'}}, '2'=>{'data'=>'2', 'name'=>'', 'searchable'=>'true', 'orderable'=>'true', 
        'search'=>{'value'=>'', 'regex'=>'false'}}}, 'order'=>{'0'=>{'column'=>'1', 'dir'=>'asc'}}, 'start'=>'0', 'length'=>'100', 
        'search'=>{'value'=>'', 'regex'=>'false'}, '_'=>'1456860760040'} }

      it 'initializes the price table datatable' do
        expect(PriceTableDatatable).to receive(:new)

        xhr :get, :index, params
      end
    end

    context 'without search parameters' do
      it 'lists the price tables' do
        xhr :get, :index

        expect(json['price_tables'].size).to eq(3)
        expect(json['price_tables'][0]['id']).to eq(@price_table_1.id)
        expect(json['price_tables'][0]['name']).to eq(@price_table_1.name)
        expect(json['price_tables'][0]['default']).to eq(@price_table_1.default?)
        expect(json['price_tables'][0]['type']).to eq(PriceTable.types[@price_table_1.type])
        expect(json['price_tables'][1]['id']).to eq(@price_table_2.id)
        expect(json['price_tables'][1]['name']).to eq(@price_table_2.name)
        expect(json['price_tables'][1]['default']).to eq(@price_table_2.default?)
        expect(json['price_tables'][1]['type']).to eq(PriceTable.types[@price_table_2.type])
        expect(json['price_tables'][2]['id']).to eq(@price_table_4.id)
        expect(json['price_tables'][2]['name']).to eq(@price_table_4.name)
        expect(json['price_tables'][2]['default']).to eq(@price_table_4.default?)
        expect(json['price_tables'][2]['type']).to eq(PriceTable.types[@price_table_4.type])        
      end
    end
  end

  describe 'POST create' do
    render_views

    before do
      @family = create(:family)
    
      @line   = create(:line, family: @family, name: 'L1')
      @line_2 = create(:line, family: @family, name: 'L2')

      @item   = create(:item)
      @item_2 = create(:item)
    end

    context 'with invalid params' do
      context 'for lines only table' do
        let(:parameters) { {'name' => '', 'type' => '0', 'default' => 'true', 'price_table_lines' => 
          [
            {'price' => '', 'line' => {'id' => @line.id, 'name' => @line.name}},
            {'price' => '', 'line' => {'id' => @line_2.id, 'name' => @line_2.name}}
          ]
        } }

        it 'does not create a new price table' do
          expect {
            xhr :post, :create, parameters
          }.to change(PriceTable, :count).by(0)
        end

        it 'does not create any new price table line' do
          expect {
            xhr :post, :create, parameters
          }.to change(PriceTableLine, :count).by(0)
        end

        it 'returns unprocessable entity status' do
          xhr :post, :create, parameters
          
          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'returns the errors' do
          xhr :post, :create, parameters
          
          expect(json['errors']).to_not be_empty
        end
      end

      context 'for items only table' do
        let(:parameters) { {'name' => '', 'type' => '1', 'default' => 'true', 'price_table_items' => 
          [
            {'price' => '', 'item' => {'id' => @item.id, 'description' => @item.description}},
            {'price' => '', 'item' => {'id' => @item_2.id, 'description' => @item_2.description}}
          ]
        } }

        it 'does not create a new price table' do
          expect {
            xhr :post, :create, parameters
          }.to change(PriceTable, :count).by(0)
        end

        it 'does not create any new price table item' do
          expect {
            xhr :post, :create, parameters
          }.to change(PriceTableItem, :count).by(0)
        end

        it 'returns unprocessable entity status' do
          xhr :post, :create, parameters
          
          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'returns the errors' do
          xhr :post, :create, parameters
          
          expect(json['errors']).to_not be_empty
        end
      end
    end

    context 'with valid params' do
      context 'for lines only table' do
        let(:parameters) { {'name' => 'Foo', 'type' => '0', 'default' => 'true', 'price_table_lines' => 
          [
            {'price' => '2', 'maximum_discount' => '17', 'line' => {'id' => "#{@line.id}"}},
            {'price' => '2 * 2', 'maximum_discount' => '19', 'line' => {'id' => "#{@line_2.id}"}}
          ]
        } }

        it 'initializes the price table service' do
          expect(PriceTableService).to receive(:new).with(parameters.merge({'owner' => @user.workplace})).and_call_original

          xhr :post, :create, parameters
        end

        it 'creates the price table' do
          expect_any_instance_of(PriceTableService).to receive(:create).and_call_original
          
          xhr :post, :create, parameters
        end

        it 'returns created status' do
          xhr :post, :create, parameters
          
          expect(response).to have_http_status(:created)
        end
      end

      context 'for items only table' do
        let(:parameters) { {'name' => 'Foo', 'type' => '1', 'default' => 'true', 'price_table_items' => 
          [
            {'price' => '2', 'maximum_discount' => '5', 'item' => {'id' => "#{@item.id}"}},
            {'price' => '4', 'maximum_discount' => '19', 'item' => {'id' => "#{@item_2.id}"}}
          ]
        } }

        it 'initializes the price table service' do
          expect(PriceTableService).to receive(:new).with(parameters.merge({'owner' => @user.workplace})).and_call_original

          xhr :post, :create, parameters
        end

        it 'creates the price table' do
          expect_any_instance_of(PriceTableService).to receive(:create).and_call_original
          
          xhr :post, :create, parameters
        end

        it 'returns created status' do
          xhr :post, :create, parameters
          
          expect(response).to have_http_status(:created)
        end
      end
    end
  end

  describe 'GET show' do
    render_views

    context 'for lines only table' do
      before do
        @family = create(:family)
      
        @line   = create(:line, family: @family, name: 'L1')
        @line_2 = create(:line, family: @family, name: 'L2')

        @price_table = create(:price_table, name: 'Foo', owner: @user.workplace)

        create(:price_table_line, line_id: @line.id, price: '2', price_table: @price_table)
      end

      it 'should ok status' do
        xhr :get, :show, id: @price_table.id

        expect(response).to have_http_status(:ok)
      end

      it 'returns the price table data' do
        xhr :get, :show, id: @price_table.id

        expect(json['price_table']['id']).to eq(@price_table.id)
        expect(json['price_table']['name']).to eq(@price_table.name)
        expect(json['price_table']['default']).to eq(@price_table.default?)
        expect(json['price_table']['type']).to eq(PriceTable.types[@price_table.type])

        expect(json['price_table']['price_table_lines'].size).to eq(2)
        expect(json['price_table']['price_table_lines'][0]['price']).to eq(@price_table.lines[0].price)
        expect(json['price_table']['price_table_lines'][0]['maximum_discount']).to eq(@price_table.lines[0].maximum_discount)
        expect(json['price_table']['price_table_lines'][0]['line']['id']).to eq(@price_table.lines[0].line.id)
        expect(json['price_table']['price_table_lines'][0]['line']['name']).to eq(@price_table.lines[0].line.name)
        expect(json['price_table']['price_table_lines'][1]['price']).to eq('')
        expect(json['price_table']['price_table_lines'][1]['line']['id']).to eq(@line_2.id)
        expect(json['price_table']['price_table_lines'][1]['line']['name']).to eq(@line_2.name)
        expect(json['price_table']['price_table_items']).to be_empty
      end
    end

    context 'for items only table' do
      before do
        @item   = create(:item)
        @item_2 = create(:item)

        @price_table = create(:price_table, name: 'Foo', owner: @user.workplace, type: :for_items)

        create(:price_table_item, item_id: @item.id, price: '2', price_table: @price_table)
      end

      it 'should ok status' do
        xhr :get, :show, id: @price_table.id

        expect(response).to have_http_status(:ok)
      end

      it 'returns the price table data' do
        xhr :get, :show, id: @price_table.id

        expect(json['price_table']['id']).to eq(@price_table.id)
        expect(json['price_table']['name']).to eq(@price_table.name)
        expect(json['price_table']['default']).to eq(@price_table.default?)
        expect(json['price_table']['type']).to eq(PriceTable.types[@price_table.type])

        expect(json['price_table']['price_table_lines']).to be_empty

        expect(json['price_table']['price_table_items'].size).to eq(2)
        expect(json['price_table']['price_table_items'][0]['price']).to eq(@price_table.items[0].price)
        expect(json['price_table']['price_table_items'][0]['maximum_discount']).to eq(@price_table.items[0].maximum_discount)
        expect(json['price_table']['price_table_items'][0]['item']['id']).to eq(@price_table.items[0].item.id)
        expect(json['price_table']['price_table_items'][0]['item']['code']).to eq(@price_table.items[0].item.code)
        expect(json['price_table']['price_table_items'][0]['item']['description']).to eq(@price_table.items[0].item.description)
        expect(json['price_table']['price_table_items'][0]['item']['type']).to eq(Item.types[@price_table.items[0].item.type])
        expect(json['price_table']['price_table_items'][1]['price']).to eq('')
        expect(json['price_table']['price_table_items'][1]['item']['id']).to eq(@item_2.id)
        expect(json['price_table']['price_table_items'][1]['item']['code']).to eq(@item_2.code)
        expect(json['price_table']['price_table_items'][1]['item']['description']).to eq(@item_2.description)
        expect(json['price_table']['price_table_items'][1]['item']['type']).to eq(Item.types[@item_2.type])
      end
    end
  end

  describe 'PUT update' do
    render_views

    context 'for lines only table' do
      before do
        @family = create(:family)
      
        @line   = create(:line, family: @family, name: 'L1')
        @line_2 = create(:line, family: @family, name: 'L2')

        @price_table = create(:price_table, name: 'ab', owner: @user.workplace)
        create(:price_table_line, line_id: @line.id, price: '2+2', price_table: @price_table)
      end

      context 'with invalid params' do
        let(:parameters) { {'name' => '', 'type' => '0', 'default' => 'true', 'price_table_lines' => 
          [
            {'price' => '', 'line' => {'id' => @line.id, 'name' => @line.name}},
            {'price' => '', 'line' => {'id' => @line_2.id, 'name' => @line_2.name}}
          ]
        }.merge({'id' => @price_table.id}) }

        it 'does not create a new price table' do
          expect {
            xhr :put, :update, parameters
          }.to change(PriceTable, :count).by(0)
        end

        it 'does not create any new price table line' do
          expect {
            xhr :put, :update, parameters
          }.to change(PriceTableLine, :count).by(0)
        end

        it 'returns unprocessable entity status' do
          xhr :put, :update, parameters
          
          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'returns the errors' do
          xhr :put, :update, parameters
          
          expect(json['errors']).to_not be_empty
        end
      end

      context 'with valid params' do
        let(:parameters) { {'name' => 'Foo', 'type' => '0', 'default' => 'true', 'price_table_lines' => 
          [
            {'price' => '2', 'maximum_discount' => '12', 'line' => {'id' => "#{@line.id}"}},
            {'price' => '2 * 2', 'maximum_discount' => '2', 'line' => {'id' => "#{@line_2.id}"}}
          ]
        } }

        it 'initializes the price table service' do
          expect(PriceTableService).to receive(:new).with(parameters.merge({'owner' => @user.workplace})).and_call_original

          xhr :put, :update, parameters.merge({'id' => @price_table.id})
        end

        it 'updates the price table' do
          expect_any_instance_of(PriceTableService).to receive(:update).with("#{@price_table.id}").and_call_original
          
          xhr :put, :update, parameters.merge({'id' => @price_table.id})
        end

        it 'returns ok status' do
          xhr :put, :update, parameters.merge({'id' => @price_table.id})
          
          expect(response).to have_http_status(:ok)
        end
      end
    end

    context 'for items only table' do
      before do
        @item   = create(:item)
        @item_2 = create(:item)

        @price_table = create(:price_table, name: 'ab', owner: @user.workplace, type: :for_items)
        create(:price_table_item, item_id: @item.id, price: '2', price_table: @price_table)
      end

      context 'with invalid params' do
        let(:parameters) { {'name' => '', 'type' => '1', 'default' => 'true', 'price_table_items' => 
          [
            {'price' => '', 'item' => {'id' => @item.id, 'description' => @item.description}},
            {'price' => '', 'item' => {'id' => @item_2.id, 'description' => @item_2.description}}
          ]
        }.merge({'id' => @price_table.id}) }

        it 'does not create a new price table' do
          expect {
            xhr :put, :update, parameters
          }.to change(PriceTable, :count).by(0)
        end

        it 'does not create any new price table item' do
          expect {
            xhr :put, :update, parameters
          }.to change(PriceTableItem, :count).by(0)
        end

        it 'returns unprocessable entity status' do
          xhr :put, :update, parameters
          
          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'returns the errors' do
          xhr :put, :update, parameters
          
          expect(json['errors']).to_not be_empty
        end
      end

      context 'with valid params' do
        let(:parameters) { {'name' => 'Foo', 'type' => '1', 'default' => 'true', 'price_table_items' => 
          [
            {'price' => '2', 'maximum_discount' => '13', 'item' => {'id' => "#{@item.id}"}},
            {'price' => '2', 'maximum_discount' => '12', 'item' => {'id' => "#{@item_2.id}"}}
          ]
        } }

        it 'initializes the price table service' do
          expect(PriceTableService).to receive(:new).with(parameters.merge({'owner' => @user.workplace})).and_call_original

          xhr :put, :update, parameters.merge({'id' => @price_table.id})
        end

        it 'updates the price table' do
          expect_any_instance_of(PriceTableService).to receive(:update).with("#{@price_table.id}").and_call_original
          
          xhr :put, :update, parameters.merge({'id' => @price_table.id})
        end

        it 'returns ok status' do
          xhr :put, :update, parameters.merge({'id' => @price_table.id})
          
          expect(response).to have_http_status(:ok)
        end
      end
    end
  end

  describe 'DELETE destroy' do
    before do
      @price_table = create(:price_table, name: 'P1', owner: @user.workplace)
    end

    context 'with success' do
      it 'destroys the price table' do
        expect {
          xhr :delete, :destroy, {:id => @price_table.id}
        }.to change(PriceTable, :count).by(-1)
      end

      it 'returns no content status' do
        xhr :delete, :destroy, {:id => @price_table.id}

        expect(response).to have_http_status(:no_content)
      end
    end

    context 'with error' do
      before do
        allow(@price_table).to receive(:destroy).and_return(false)
        allow(@price_table).to receive(:errors).and_return(double(:errors, full_messages: ['foo']))

        allow_any_instance_of(@user.workplace.class).to receive_message_chain(:price_tables, :find).and_return(@price_table)
      end

      it 'does not destroy the price table' do
        expect {
          xhr :delete, :destroy, {:id => @price_table.id}
        }.to_not change(PriceTable, :count)
      end

      it 'returns the bad request status' do
        xhr :delete, :destroy, {:id => @price_table.id}

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the errors' do
        xhr :delete, :destroy, {:id => @price_table.id}
        
        expect(json['errors']).to_not be_empty
      end
    end
  end

  describe 'DELETE batch_destroy' do
    render_views

    before do
      @price_table_1 = create(:price_table, name: 'P1', owner: @user.workplace)
      @price_table_2 = create(:price_table, name: 'P2', owner: @user.workplace)
    end

    context 'with success' do
      it 'destroys the price tables' do
        expect {
          xhr :delete, :batch_destroy, {:ids => [@price_table_1.id, @price_table_2.id]}
        }.to change(PriceTable, :count).by(-2)
      end

      it 'returns no content status' do
        xhr :delete, :batch_destroy, {:ids => [@price_table_1.id, @price_table_2.id]}

        expect(response).to have_http_status(:no_content)
      end
    end

    context 'with error' do
      before do
        allow(@price_table_1).to receive(:destroy!).and_raise(StandardError)
        allow_any_instance_of(@user.workplace.class).to receive_message_chain(:price_tables, :find).and_return(@price_table)
      end

      it 'does not destroy the price tables' do
        expect {
          xhr :delete, :batch_destroy, {:ids => [@price_table_1.id]}
        }.to_not change(PriceTable, :count)
      end

      it 'returns the bad request status' do
        xhr :delete, :batch_destroy, {:ids => [@price_table_1.id]}

        expect(response).to have_http_status(:bad_request)
      end

      it 'returns the errors' do
        xhr :delete, :batch_destroy, {:ids => [@price_table_1.id]}
        
        expect(json['errors']).to_not be_empty
      end
    end
  end
end
