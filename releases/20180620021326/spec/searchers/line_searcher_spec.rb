require 'rails_helper'

RSpec.describe LineSearcher, type: :searcher do
  describe "initialization" do
    context "without parameters" do
      it "sets the width to nil" do
        expect(described_class.new.width).to be_nil
      end

      it "sets the height to nil" do
        expect(described_class.new.height).to be_nil
      end
    end

    context "with only width" do
      it "sets the width to nil" do
        expect(described_class.new({ width: "10" }).width).to be_nil
      end

      it "sets the height to nil" do
        expect(described_class.new({ width: "10" }).height).to be_nil
      end
    end

    context "with only height" do
      it "sets the width to nil" do
        expect(described_class.new({ height: "10" }).width).to be_nil
      end

      it "sets the height to nil" do
        expect(described_class.new({ height: "10 "}).height).to be_nil
      end
    end

    context "with both width and height parameters" do
      it "sets the width" do
        expect(described_class.new({ width: "12", height: "10" }).width).to eq(BigDecimal.new("12"))
      end

      it "sets the height" do
        expect(described_class.new({ width: "12", height: "10" }).height).to eq(BigDecimal.new("10"))
      end
    end
  end

  describe "search" do
    before do
      @category1 = create(:category, name: 'C1')
      @category2 = create(:category, name: 'C2')
      @category3 = create(:category, name: 'C3')

      @family = create(:family, name: 'F1')
      @family2 = create(:family, name: 'F2')

      @line1 = create(:line, family: @family, name: "L1", active: true)

      @line1.categories << build(:line_category, category: @category1)
      @line1.categories << build(:line_category, category: @category2)

      @line1.dimensions << build(:dimension, height: 10..50, width: 10..20)
      @line1.dimensions << build(:dimension, height: 70..90, width: 30..40)

      @line1.save!

      @line2 = create(:line, family: @family, name: "L2", active: false)

      @line2.dimensions << build(:dimension, height: 10..50, width: 21..30)

      @line2.save!

      @line3 = create(:line, family: @family2, name: "L3", active: true)

      @line4 = create(:line, family: @family, name: "L4", active: false)

      @line4.categories << build(:line_category, category: @category1)
      @line4.categories << build(:line_category, category: @category2)

      @line4.dimensions << build(:dimension, height: 10..50, width: 10..20)
      @line4.dimensions << build(:dimension, height: 70..90, width: 30..40)

      @value1 = build(:value, value: 'V1', comparison_value: 100)
      @value2 = build(:value, value: 'V2', comparison_value: 150)
      @value3 = build(:value, value: 'V3', comparison_value: 200)

      @specification = build(:specification, automatic: true)
      @specification.values = [@value1, @value2, @value3]
      @specification.save!

      @component1 = create(:component, name: 'C1', unit: :cm, category: @category1, erasable: true,
        cost: BigDecimal.new('10'), values: [@value1])

      @component2 = create(:component, name: 'C2', unit: :cm, category: @category1, erasable: true,
        cost: BigDecimal.new('10'), values: [@value3])

      @component3 = create(:component, name: 'C3', unit: :cm, category: @category1, erasable: true,
        cost: BigDecimal.new('10'))

      @line_rule = create(:line_rule, line: @line4, control_method: LineRule.control_methods[:width], 
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_',
        width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', automatic: true, category_id: @category1.id)
      create(:rule, line_rule: @line_rule, component: @component1, control_method: Rule.control_methods[:width], 
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_',
        width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', automatic: true)

      @line_rule = create(:line_rule, line: @line2, control_method: LineRule.control_methods[:width], 
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_',
        width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', automatic: true, category_id: @category2.id)
      create(:rule, line_rule: @line_rule, component: @component2, control_method: Rule.control_methods[:width], 
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_',
        width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', automatic: true)

      @line_rule = create(:line_rule, line: @line1, control_method: LineRule.control_methods[:width], 
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_',
        width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', automatic: true, category_id: nil)
      create(:rule, line_rule: @line_rule, component: @component3, control_method: Rule.control_methods[:width], 
        total_consumption: '_LARGURA_*2', cost: '_CUSTO_BASE_',
        width_consumption: '_LARGURA_', height_consumption: '_ALTURA_', automatic: true)

      @factory = create(:factory, name: 'F1')
    end

    context 'when the workplace is the factory' do
      it "returns all lines available" do
        expect(described_class.new.search(@factory)).to match_array([@line1, @line2, @line3, @line4])
      end
    end

    context 'when the workplace is a distributor' do
      before do
        classification = create(:commercial_classification, line_ids: [@line1.id, @line4.id])

        @distributor = create(:distributor, name: 'B1', commercial_classification: classification, subdomain: 'dist')
      end

      it "returns all lines available based on the distributor's commercial classification" do
        expect(described_class.new.search(@distributor)).to match_array([@line1, @line4])
      end
    end

    context 'when the workplace is a store' do
      before do
        classification = create(:commercial_classification, line_ids: [@line2.id, @line3.id])

        @distributor = create(:distributor, name: 'B1', subdomain: 'dist')
        @store = create(:store, name: 'c1', subdomain: 'store', distributor: @distributor, commercial_classification: classification)
      end

      it "returns all lines available based on the store's commercial classification" do
        expect(described_class.new.search(@store)).to match_array([@line2, @line3])
      end
    end

    context "without parameters" do
      it "returns all lines available" do
        expect(described_class.new.search(@factory)).to match_array([@line1, @line2, @line3, @line4])
      end
    end

    context "with dimension parameters" do
      let(:parameters) { { width: 20, height: 25 } }

      it "lists the lines that meet the criteria and are active" do
        expect(described_class.new(parameters).search(@factory)).to match_array([@line1])
      end
    end

    context "with specifications" do
      let(:parameters) { { specifications: ["#{@value1.id}"] } }

      it "lists the lines that have rules with components using the given values" do
        expect(described_class.new(parameters).search(@factory)).to match_array([@line4])
      end
    end

    context "with family" do
      let(:parameters) { { family_id: @family2.id } }

      it "lists the lines that belongs to the given family" do
        expect(described_class.new(parameters).search(@factory)).to match_array([@line3])
      end
    end

    context "with category" do
      let(:parameters) { { category_id: @category1.id } }

      it "lists the lines that have rules belonging to the given category or without category" do
        expect(described_class.new(parameters).search(@factory)).to match_array([@line1, @line4])
      end
    end

    context 'with active flag' do
      let(:parameters) { { active: true } }

      it 'lists only the active lines' do
        expect(described_class.new(parameters).search(@factory)).to match_array([@line1, @line3])
      end
    end
  end
end