require 'rails_helper'

RSpec.describe ComponentSearcher, type: :searcher do
  describe 'initialization' do
    context 'without parameters' do
      it 'sets the width to nil' do
        expect(described_class.new.width).to be_nil
      end

      it 'sets the height to nil' do
        expect(described_class.new.height).to be_nil
      end
    end

    context 'with only width' do
      it 'sets the width to nil' do
        expect(described_class.new({width: '10'}).width).to be_nil
      end

      it 'sets the height to nil' do
        expect(described_class.new({width: '10'}).height).to be_nil
      end
    end

    context 'with only height' do
      it 'sets the width to nil' do
        expect(described_class.new({height: '10'}).width).to be_nil
      end

      it 'sets the height to nil' do
        expect(described_class.new({height: '10'}).height).to be_nil
      end
    end

    context 'with both width and height parameters' do
      it 'sets the width' do
        expect(described_class.new({width: '12', height: '10'}).width).to eq(BigDecimal.new('12'))
      end

      it 'sets the height' do
        expect(described_class.new({width: '12', height: '10'}).height).to eq(BigDecimal.new('10'))
      end
    end
  end

  describe 'search_for_line' do
    before(:each) do
      @family = FactoryGirl.create(:family)
      @family_2 = FactoryGirl.create(:family, name: 'F2')

      @category = FactoryGirl.create(:category, name: 'C1')
      @category_2 = FactoryGirl.create(:category, name: 'C2')
      @category_3 = FactoryGirl.create(:category, name: 'C3')

      @line = FactoryGirl.create(:line, family: @family)

      @line.categories << FactoryGirl.build(:line_category, category: @category, required: true, order: 0)
      @line.categories << FactoryGirl.build(:line_category, category: @category_3, required: false, order: 3)

      @line.save!

      @specification = FactoryGirl.build(:specification, automatic: true)
      @specification.values << FactoryGirl.build(:value, value: 'V1', comparison_value: 20)
      @specification.values << FactoryGirl.build(:value, value: 'V2', comparison_value: 30)
      @specification.save!

      @component_1 = FactoryGirl.create(:component, category: @category, name: 'C1', families: [@family])
      @component_2 = FactoryGirl.create(:component, category: @category, name: 'C2', families: [@family])
      @component_3 = FactoryGirl.create(:component, category: @category_2, name: 'C3', families: [@family])
      @component_4 = FactoryGirl.create(:component, category: @category_3, name: 'C4', families: [@family_2])
      @component_5 = FactoryGirl.create(:component, category: @category_3, name: 'C5', families: [@family])

      @component_6 = Component.where(erasable: false, category_id: @category.id).first
      @component_7 = Component.where(erasable: false, category_id: @category_3.id).first

      @component_8 = FactoryGirl.create(:component, category: @category_3, name: 'C8', families: [@family], values: [@specification.values.first])
      @component_9 = FactoryGirl.create(:component, category: @category, name: 'C9', families: [@family], values: [@specification.values.last])
    end

    context 'without parameters' do
      it 'returns all components available for the family (excluding the not erasable) for the given line and its category' do
        expect(described_class.new.search_for_line(@line.id)).to match_array([@component_1, @component_2, @component_8, @component_9, @component_5])
      end
    end

    context 'with search parameters' do
      let(:parameters) { {width: 21, height: 25} }

      context 'for weighted dimensions' do
        before do
          @component_3 = FactoryGirl.create(:component, category: @category, name: 'C3', families: [@family])

          @line.dimensions << FactoryGirl.build(:dimension, width: 0..25, height: 0..30, weight: :general)
          @line.dimensions << FactoryGirl.build(:dimension, width: 0..25, height: 0..30, weight: :light)
          @line.dimensions << FactoryGirl.build(:dimension, width: 0..25, height: 0..20, weight: :medium)
          @line.dimensions << FactoryGirl.build(:dimension, width: 0..20, height: 0..30, weight: :heavy)

          @line.save!

          line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: 5..30, required: true, weight: :general)
          FactoryGirl.create(:rule, line_rule: line_rule, component: @component_1, control_method: Rule.control_methods[:width], width: 5..30, required: true, weight: :general)

          line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: 5..30, required: true, weight: :light)
          FactoryGirl.create(:rule, line_rule: line_rule, component: @component_2, control_method: Rule.control_methods[:width], width: 5..30, required: true, weight: :light)

          line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: 5..30, required: true, weight: :medium)
          FactoryGirl.create(:rule, line_rule: line_rule, component: @component_3, control_method: Rule.control_methods[:width], width: 5..30, required: true, weight: :medium)

          line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: 5..30, required: true, weight: :heavy)
          FactoryGirl.create(:rule, line_rule: line_rule, component: @component_4, control_method: Rule.control_methods[:width], width: 5..30, required: true, weight: :heavy)
        end

        it 'returns all components with weights that belongs to the dimensions that fit the width and height' do
          expect(described_class.new(parameters).search_for_line(@line.id)).to match_array([@component_1, @component_2, @component_7])
        end
      end

      context 'for width controlled components' do
        before do
          @line.dimensions << FactoryGirl.build(:dimension, width: 0..25, height: 0..30, weight: :general)
          @line.save!
        end

        context 'for non automatic rules' do
          before(:each) do
            line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: 5..30, required: false, )
            FactoryGirl.create(:rule, line_rule: line_rule, component: @component_1, control_method: Rule.control_methods[:width], width: 5..30, required: false)

            line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: 26..40, required: false)
            FactoryGirl.create(:rule, line_rule: line_rule, component: @component_2, control_method: Rule.control_methods[:width], width: 26..40, required: false)

            line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: 10..40, required: false)
            FactoryGirl.create(:rule, line_rule: line_rule, component: @component_3, control_method: Rule.control_methods[:width], width: 10..40, required: false)

            line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: 20..25, required: false)
            FactoryGirl.create(:rule, line_rule: line_rule, component: @component_4, control_method: Rule.control_methods[:width], width: 20..25, required: false)
          end

          it 'returns all components available for the given line and its category applying the rules' do
            expect(described_class.new(parameters).search_for_line(@line.id)).to match_array([@component_1, @component_7])
          end
        end

        context 'for automatic rules' do
          before(:each) do
            line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: 5..30, required: false)
            FactoryGirl.create(:rule, line_rule: line_rule, component: @component_1, control_method: Rule.control_methods[:width], width: 5..30, required: false)

            line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], required: false, automatic: true)
            FactoryGirl.create(:rule, line_rule: line_rule, component: @component_8, control_method: Rule.control_methods[:width], required: false, automatic: true)

            line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], required: false, automatic: true)
            FactoryGirl.create(:rule, line_rule: line_rule, component: @component_9, control_method: Rule.control_methods[:width], required: false, automatic: true)

            line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: 10..40, required: false)
            FactoryGirl.create(:rule, line_rule: line_rule, component: @component_3, control_method: Rule.control_methods[:width], width: 10..40, required: false)

            line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: 20..25, required: false)
            FactoryGirl.create(:rule, line_rule: line_rule, component: @component_4, control_method: Rule.control_methods[:width], width: 20..25, required: false)
          end

          it 'returns all components available for the given line and its category applying the rules but ignoring the width for the automatic rules' do
            expect(described_class.new(parameters).search_for_line(@line.id)).to match_array([@component_1, @component_9, @component_7])
          end
        end
      end

      context 'for height controlled components' do
        before do
          @line.dimensions << FactoryGirl.build(:dimension, width: 0..25, height: 0..30, weight: :general)
          @line.save!
        end

        context 'for non automatic rules' do
          before(:each) do
            line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:height], height: 5..30, required: false)
            FactoryGirl.create(:rule, line_rule: line_rule, component: @component_1, control_method: Rule.control_methods[:height], height: 5..30, required: false)

            line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:height], height: 24..40, required: false)
            FactoryGirl.create(:rule, line_rule: line_rule, component: @component_2, control_method: Rule.control_methods[:height], height: 24..40, required: false)

            line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:height], height: 10..40, required: false)
            FactoryGirl.create(:rule, line_rule: line_rule, component: @component_3, control_method: Rule.control_methods[:height], height: 10..40, required: false)

            line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:height], height: 20..24, required: false)
            FactoryGirl.create(:rule, line_rule: line_rule, component: @component_4, control_method: Rule.control_methods[:height], height: 20..24, required: false)
          end

          it 'returns all components available for the given line and its category applying the rules' do
            expect(described_class.new(parameters).search_for_line(@line.id)).to match_array([@component_1, @component_2, @component_7])
          end
        end

        context 'for automatic rules' do
          before(:each) do
            line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:height], height: 5..30, required: false)
            FactoryGirl.create(:rule, line_rule: line_rule, component: @component_1, control_method: Rule.control_methods[:height], height: 5..30, required: false)

            line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:height], required: false, automatic: true)
            FactoryGirl.create(:rule, line_rule: line_rule, component: @component_8, control_method: Rule.control_methods[:height], required: false, automatic: true)

            line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:height], required: false, automatic: true)
            FactoryGirl.create(:rule, line_rule: line_rule, component: @component_9, control_method: Rule.control_methods[:height], required: false, automatic: true)

            line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:height], height: 10..40, required: false)
            FactoryGirl.create(:rule, line_rule: line_rule, component: @component_3, control_method: Rule.control_methods[:height], height: 10..40, required: false)

            line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:height], height: 20..24, required: false)
            FactoryGirl.create(:rule, line_rule: line_rule, component: @component_4, control_method: Rule.control_methods[:height], height: 20..24, required: false)
          end

          it 'returns all components available for the given line and its category applying the rules using the comparison value for the automatic rule' do
            expect(described_class.new(parameters).search_for_line(@line.id)).to match_array([@component_1, @component_9, @component_7])
          end
        end
      end

      context 'for area controlled components' do
        before(:each) do
          @line.dimensions << FactoryGirl.build(:dimension, width: 0..25, height: 0..30, weight: :general)
          @line.save!

          line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:area], width: 5..30, height: 5..24)
          FactoryGirl.create(:rule, line_rule: line_rule, component: @component_1, control_method: Rule.control_methods[:area], width: 5..30, height: 5..24)

          line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:area], width: 15..21, height: 25..40)
          FactoryGirl.create(:rule, line_rule: line_rule, component: @component_2, control_method: Rule.control_methods[:area], width: 15..21, height: 25..40)

          line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:area], width: 5..30, height: 5..40)
          FactoryGirl.create(:rule, line_rule: line_rule, component: @component_3, control_method: Rule.control_methods[:area], width: 5..30, height: 5..40)

          line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:area], width: 10..25, height: 20..30)
          FactoryGirl.create(:rule, line_rule: line_rule, component: @component_4, control_method: Rule.control_methods[:area], width: 10..25, height: 20..30)
          FactoryGirl.create(:rule, line_rule: line_rule, component: @component_4, control_method: Rule.control_methods[:area], width: 10..20, height: 20..30)
        end

        it 'returns all components available for the given line and its category applying the rules' do
          expect(described_class.new(parameters).search_for_line(@line.id)).to match_array([@component_2, @component_7])
        end
      end

      context 'for unit controlled components' do
        before(:each) do
          @line.dimensions << FactoryGirl.build(:dimension, width: 0..25, height: 0..30, weight: :general)
          @line.save!

          line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:unit])
          FactoryGirl.create(:rule, line_rule: line_rule, component: @component_1, control_method: Rule.control_methods[:unit], required: true)

          line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:width], width: 10..20)
          FactoryGirl.create(:rule, line_rule: line_rule, component: @component_2, control_method: Rule.control_methods[:width], width: 10..20)

          line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:unit])
          FactoryGirl.create(:rule, line_rule: line_rule, component: @component_3, control_method: Rule.control_methods[:unit], required: true)

          line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:unit])
          FactoryGirl.create(:rule, line_rule: line_rule, component: @component_4, control_method: Rule.control_methods[:unit], required: true)

          line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:unit])
          FactoryGirl.create(:rule, line_rule: line_rule, component: @component_5, control_method: Rule.control_methods[:unit], required: true)
        end

        it 'returns all components available for the given line and its category applying the rules' do
          expect(described_class.new(parameters).search_for_line(@line.id)).to match_array([@component_1, @component_5])
        end
      end

      context 'for total area controlled components' do
        before(:each) do
          @line.dimensions << FactoryGirl.build(:dimension, width: 0..25, height: 0..30, weight: :general)
          @line.save!

          line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:total_area], area: 400..530)
          FactoryGirl.create(:rule, line_rule: line_rule, component: @component_1, control_method: Rule.control_methods[:total_area], area: 400..530)

          line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:total_area], area: 500..530)
          FactoryGirl.create(:rule, line_rule: line_rule, component: @component_2, control_method: Rule.control_methods[:total_area], area: 500..530)

          line_rule = FactoryGirl.create(:line_rule, line: @line, control_method: LineRule.control_methods[:total_area], area: 530..540)
          FactoryGirl.create(:rule, line_rule: line_rule, component: @component_3, control_method: Rule.control_methods[:total_area], area: 530..540)
        end

        it 'returns all components available for the given line and its category applying the rules' do
          expect(described_class.new(parameters).search_for_line(@line.id)).to match_array([@component_1, @component_2, @component_7])
        end
      end
    end
  end
end