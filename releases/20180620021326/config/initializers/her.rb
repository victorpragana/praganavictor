Her::API.setup url: "https://integrador.varejonline.com.br/apps/api" do |c|
  # Request
  c.use VpsaTokenAuthentication
  c.use Faraday::Request::UrlEncoded

  # Response
  c.use MyCustomParser
  #c.use Her::Middleware::DefaultParseJSON

  # Adapter
  c.use Faraday::Adapter::NetHttp
end