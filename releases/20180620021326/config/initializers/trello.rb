Trello.configure do |config|
  config.developer_public_key = Rails.application.secrets.trello["developer_public_key"]
  config.member_token = Rails.application.secrets.trello["member_token"]
end