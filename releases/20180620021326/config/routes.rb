# == Route Map
#
#                                       Prefix Verb   URI Pattern                                                                                Controller#Action
#                             new_user_session GET    /api/auth/sign_in(.:format)                                                                overrides/sessions#new {:format=>"json"}
#                                 user_session POST   /api/auth/sign_in(.:format)                                                                overrides/sessions#create {:format=>"json"}
#                         destroy_user_session DELETE /api/auth/sign_out(.:format)                                                               overrides/sessions#destroy {:format=>"json"}
#                      api_auth_validate_token GET    /api/auth/validate_token(.:format)                                                         overrides/token_validations#validate_token
#                api_admin_auth_validate_token GET    /api/admin_auth/validate_token(.:format)                                                   devise_token_auth/token_validations#validate_token
#              api_manager_auth_validate_token GET    /api/manager_auth/validate_token(.:format)                                                 devise_token_auth/token_validations#validate_token
#               api_seller_auth_validate_token GET    /api/seller_auth/validate_token(.:format)                                                  devise_token_auth/token_validations#validate_token
#             api_operator_auth_validate_token GET    /api/operator_auth/validate_token(.:format)                                                devise_token_auth/token_validations#validate_token
#                        change_workplace_user PUT    /api/users/:id/change_workplace(.:format)                                                  users#change_workplace {:format=>"json"}
#                                              PATCH  /api/users/:id/change_workplace(.:format)                                                  users#change_workplace {:format=>"json"}
#                                        users GET    /api/users(.:format)                                                                       users#index {:format=>"json"}
#                                              POST   /api/users(.:format)                                                                       users#create {:format=>"json"}
#                                         user GET    /api/users/:id(.:format)                                                                   users#show {:format=>"json"}
#                                              PATCH  /api/users/:id(.:format)                                                                   users#update {:format=>"json"}
#                                              PUT    /api/users/:id(.:format)                                                                   users#update {:format=>"json"}
#                                              DELETE /api/users/:id(.:format)                                                                   users#destroy {:format=>"json"}
#                                        menus GET    /api/menus(.:format)                                                                       menus#index {:format=>"json"}
#                                      clients GET    /api/clients(.:format)                                                                     clients#index {:format=>"json"}
#                           current_workplaces GET    /api/workplaces/current(.:format)                                                          workplaces#current {:format=>"json"}
#                                   workplaces GET    /api/workplaces(.:format)                                                                  workplaces#index {:format=>"json"}
#                               document_index PUT    /api/document(.:format)                                                                    document#update {:format=>"json"}
#                                              PATCH  /api/document(.:format)                                                                    document#update {:format=>"json"}
#                                              GET    /api/document(.:format)                                                                    document#index {:format=>"json"}
#                                     families GET    /api/families(.:format)                                                                    families#index {:format=>"json"}
#                                              POST   /api/families(.:format)                                                                    families#create {:format=>"json"}
#                                       family GET    /api/families/:id(.:format)                                                                families#show {:format=>"json"}
#                                              PATCH  /api/families/:id(.:format)                                                                families#update {:format=>"json"}
#                                              PUT    /api/families/:id(.:format)                                                                families#update {:format=>"json"}
#                              components_line GET    /api/lines/:id/components(.:format)                                                        lines#components {:format=>"json"}
#                                   rules_line GET    /api/lines/:id/rules(.:format)                                                             lines#rules {:format=>"json"}
#                               set_rules_line POST   /api/lines/:id/set_rules(.:format)                                                         lines#set_rules {:format=>"json"}
#                               duplicate_line POST   /api/lines/:id/duplicate(.:format)                                                         lines#duplicate {:format=>"json"}
#                              inactivate_line PUT    /api/lines/:id/inactivate(.:format)                                                        lines#inactivate {:format=>"json"}
#                                              PATCH  /api/lines/:id/inactivate(.:format)                                                        lines#inactivate {:format=>"json"}
#                                activate_line PUT    /api/lines/:id/activate(.:format)                                                          lines#activate {:format=>"json"}
#                                              PATCH  /api/lines/:id/activate(.:format)                                                          lines#activate {:format=>"json"}
#                          duplicate_rule_line POST   /api/lines/:id/duplicate_rule(.:format)                                                    lines#duplicate_rule {:format=>"json"}
#                            import_rules_line POST   /api/lines/:id/import_rules(.:format)                                                      lines#import_rules {:format=>"json"}
#                       select_line_components GET    /api/lines/:line_id/components/select(.:format)                                            components#select {:format=>"json"}
#                                        lines GET    /api/lines(.:format)                                                                       lines#index {:format=>"json"}
#                                              POST   /api/lines(.:format)                                                                       lines#create {:format=>"json"}
#                                         line GET    /api/lines/:id(.:format)                                                                   lines#show {:format=>"json"}
#                                              PATCH  /api/lines/:id(.:format)                                                                   lines#update {:format=>"json"}
#                                              PUT    /api/lines/:id(.:format)                                                                   lines#update {:format=>"json"}
#                                              DELETE /api/lines/:id(.:format)                                                                   lines#destroy {:format=>"json"}
#                     batch_destroy_components DELETE /api/components/batch_destroy(.:format)                                                    components#batch_destroy {:format=>"json"}
#                            export_components GET    /api/components/export(.:format)                                                           components#export {:format=>"json"}
#                            upload_components POST   /api/components/upload(.:format)                                                           components#upload {:format=>"json"}
#                          clone_kit_component PATCH  /api/components/:id/clone_kit(.:format)                                                    components#clone_kit {:format=>"json"}
#                                              PUT    /api/components/:id/clone_kit(.:format)                                                    components#clone_kit {:format=>"json"}
#                       batch_insert_component POST   /api/components/:id/batch_insert(.:format)                                                 components#batch_insert {:format=>"json"}
#                           activate_component PUT    /api/components/:id/activate(.:format)                                                     components#activate {:format=>"json"}
#                                              PATCH  /api/components/:id/activate(.:format)                                                     components#activate {:format=>"json"}
#                     really_destroy_component DELETE /api/components/:id/really_destroy(.:format)                                               components#really_destroy {:format=>"json"}
#                                   components GET    /api/components(.:format)                                                                  components#index {:format=>"json"}
#                                              POST   /api/components(.:format)                                                                  components#create {:format=>"json"}
#                                    component GET    /api/components/:id(.:format)                                                              components#show {:format=>"json"}
#                                              PATCH  /api/components/:id(.:format)                                                              components#update {:format=>"json"}
#                                              PUT    /api/components/:id(.:format)                                                              components#update {:format=>"json"}
#                                              DELETE /api/components/:id(.:format)                                                              components#destroy {:format=>"json"}
#                              generate_orders POST   /api/orders/generate(.:format)                                                             orders#generate {:format=>"json"}
#                                    pay_order POST   /api/orders/:id/pay(.:format)                                                              orders#pay {:format=>"json"}
#                            send_to_erp_order PUT    /api/orders/:id/send_to_erp(.:format)                                                      orders#send_to_erp {:format=>"json"}
#                                 cancel_order PUT    /api/orders/:id/cancel(.:format)                                                           orders#cancel {:format=>"json"}
#                  change_billing_client_order PUT    /api/orders/:id/change_billing_client(.:format)                                            orders#change_billing_client {:format=>"json"}
#                                              PATCH  /api/orders/:id/change_billing_client(.:format)                                            orders#change_billing_client {:format=>"json"}
#                update_special_discount_order PUT    /api/orders/:id/update_special_discount(.:format)                                          orders#update_special_discount {:format=>"json"}
#                                              PATCH  /api/orders/:id/update_special_discount(.:format)                                          orders#update_special_discount {:format=>"json"}
#                      production_status_order PUT    /api/orders/:id/production_status(.:format)                                                orders#production_status {:format=>"json"}
#                                              PATCH  /api/orders/:id/production_status(.:format)                                                orders#production_status {:format=>"json"}
#                            lines_label_order GET    /api/orders/:id/lines_label(.:format)                                                      orders#lines_label {:format=>"json"}
#                       components_label_order GET    /api/orders/:id/components_label(.:format)                                                 orders#components_label {:format=>"json"}
#                                cut_map_order GET    /api/orders/:id/cut_map(.:format)                                                          orders#cut_map {:format=>"json"}
#                                       orders GET    /api/orders(.:format)                                                                      orders#index {:format=>"json"}
#                                        order GET    /api/orders/:id(.:format)                                                                  orders#show {:format=>"json"}
#                                              DELETE /api/orders/:id(.:format)                                                                  orders#destroy {:format=>"json"}
#                           antecipate_credits POST   /api/credits/antecipate(.:format)                                                          credits#antecipate {:format=>"json"}
#                                      credits GET    /api/credits(.:format)                                                                     credits#index {:format=>"json"}
#                                              POST   /api/credits(.:format)                                                                     credits#create {:format=>"json"}
#          purchase_planning_production_orders GET    /api/production_orders/purchase_planning(.:format)                                         production_orders#purchase_planning {:format=>"json"}
#                            production_orders GET    /api/production_orders(.:format)                                                           production_orders#index {:format=>"json"}
#                                   categories GET    /api/categories(.:format)                                                                  categories#index {:format=>"json"}
#                                              POST   /api/categories(.:format)                                                                  categories#create {:format=>"json"}
#                                     category GET    /api/categories/:id(.:format)                                                              categories#show {:format=>"json"}
#                                              PATCH  /api/categories/:id(.:format)                                                              categories#update {:format=>"json"}
#                                              PUT    /api/categories/:id(.:format)                                                              categories#update {:format=>"json"}
#                                              DELETE /api/categories/:id(.:format)                                                              categories#destroy {:format=>"json"}
#                 for_filtering_specifications GET    /api/specifications/for_filtering(.:format)                                                specifications#for_filtering {:format=>"json"}
#                               specifications GET    /api/specifications(.:format)                                                              specifications#index {:format=>"json"}
#                                              POST   /api/specifications(.:format)                                                              specifications#create {:format=>"json"}
#                                specification GET    /api/specifications/:id(.:format)                                                          specifications#show {:format=>"json"}
#                                              PATCH  /api/specifications/:id(.:format)                                                          specifications#update {:format=>"json"}
#                                              PUT    /api/specifications/:id(.:format)                                                          specifications#update {:format=>"json"}
#                                              DELETE /api/specifications/:id(.:format)                                                          specifications#destroy {:format=>"json"}
#                      activate_base_component PUT    /api/base_components/:id/activate(.:format)                                                base_components#activate {:format=>"json"}
#                                              PATCH  /api/base_components/:id/activate(.:format)                                                base_components#activate {:format=>"json"}
#                really_destroy_base_component DELETE /api/base_components/:id/really_destroy(.:format)                                          base_components#really_destroy {:format=>"json"}
#                              base_components GET    /api/base_components(.:format)                                                             base_components#index {:format=>"json"}
#                                              POST   /api/base_components(.:format)                                                             base_components#create {:format=>"json"}
#                               base_component GET    /api/base_components/:id(.:format)                                                         base_components#show {:format=>"json"}
#                                              PATCH  /api/base_components/:id(.:format)                                                         base_components#update {:format=>"json"}
#                                              PUT    /api/base_components/:id(.:format)                                                         base_components#update {:format=>"json"}
#                                              DELETE /api/base_components/:id(.:format)                                                         base_components#destroy {:format=>"json"}
#                           calculate_estimate PUT    /api/estimates/:id/calculate(.:format)                                                     estimates#calculate {:format=>"json"}
#                                              PATCH  /api/estimates/:id/calculate(.:format)                                                     estimates#calculate {:format=>"json"}
#                           duplicate_estimate POST   /api/estimates/:id/duplicate(.:format)                                                     estimates#duplicate {:format=>"json"}
#                      apply_discount_estimate PUT    /api/estimates/:id/apply_discount(.:format)                                                estimates#apply_discount {:format=>"json"}
#                                              PATCH  /api/estimates/:id/apply_discount(.:format)                                                estimates#apply_discount {:format=>"json"}
#                        estimate_environments GET    /api/estimates/:estimate_id/environments(.:format)                                         environments#index {:format=>"json"}
#                               estimate_items GET    /api/estimates/:estimate_id/items(.:format)                                                estimate_items#index {:format=>"json"}
#                                    estimates GET    /api/estimates(.:format)                                                                   estimates#index {:format=>"json"}
#                                              POST   /api/estimates(.:format)                                                                   estimates#create {:format=>"json"}
#                                     estimate GET    /api/estimates/:id(.:format)                                                               estimates#show {:format=>"json"}
#                                              PATCH  /api/estimates/:id(.:format)                                                               estimates#update {:format=>"json"}
#                                              PUT    /api/estimates/:id(.:format)                                                               estimates#update {:format=>"json"}
#                        environment_locations GET    /api/environments/:environment_id/locations(.:format)                                      locations#index {:format=>"json"}
#                                 environments POST   /api/environments(.:format)                                                                environments#create {:format=>"json"}
#                                  environment GET    /api/environments/:id(.:format)                                                            environments#show {:format=>"json"}
#                                              PATCH  /api/environments/:id(.:format)                                                            environments#update {:format=>"json"}
#                                              PUT    /api/environments/:id(.:format)                                                            environments#update {:format=>"json"}
#                                              DELETE /api/environments/:id(.:format)                                                            environments#destroy {:format=>"json"}
#                               reset_location DELETE /api/locations/:id/reset(.:format)                                                         locations#reset {:format=>"json"}
#                      apply_discount_location PUT    /api/locations/:id/apply_discount(.:format)                                                locations#apply_discount {:format=>"json"}
#                                              PATCH  /api/locations/:id/apply_discount(.:format)                                                locations#apply_discount {:format=>"json"}
#                             location_options GET    /api/locations/:location_id/options(.:format)                                              options#index {:format=>"json"}
#                                    locations POST   /api/locations(.:format)                                                                   locations#create {:format=>"json"}
#                                     location GET    /api/locations/:id(.:format)                                                               locations#show {:format=>"json"}
#                                              PATCH  /api/locations/:id(.:format)                                                               locations#update {:format=>"json"}
#                                              PUT    /api/locations/:id(.:format)                                                               locations#update {:format=>"json"}
#                                              DELETE /api/locations/:id(.:format)                                                               locations#destroy {:format=>"json"}
#                                      options POST   /api/options(.:format)                                                                     options#create {:format=>"json"}
#                                       option GET    /api/options/:id(.:format)                                                                 options#show {:format=>"json"}
#                                              PATCH  /api/options/:id(.:format)                                                                 options#update {:format=>"json"}
#                                              PUT    /api/options/:id(.:format)                                                                 options#update {:format=>"json"}
#                                              DELETE /api/options/:id(.:format)                                                                 options#destroy {:format=>"json"}
#                 apply_discount_estimate_item PUT    /api/estimate_items/:id/apply_discount(.:format)                                           estimate_items#apply_discount {:format=>"json"}
#                                              PATCH  /api/estimate_items/:id/apply_discount(.:format)                                           estimate_items#apply_discount {:format=>"json"}
#                                              POST   /api/estimate_items(.:format)                                                              estimate_items#create {:format=>"json"}
#                                estimate_item DELETE /api/estimate_items/:id(.:format)                                                          estimate_items#destroy {:format=>"json"}
#                         batch_destroy_stores DELETE /api/stores/batch_destroy(.:format)                                                        stores#batch_destroy {:format=>"json"}
#                            upload_logo_store PUT    /api/stores/:id/upload_logo(.:format)                                                      stores#upload_logo {:format=>"json"}
#                                       stores GET    /api/stores(.:format)                                                                      stores#index {:format=>"json"}
#                                              POST   /api/stores(.:format)                                                                      stores#create {:format=>"json"}
#                                        store GET    /api/stores/:id(.:format)                                                                  stores#show {:format=>"json"}
#                                              PATCH  /api/stores/:id(.:format)                                                                  stores#update {:format=>"json"}
#                                              PUT    /api/stores/:id(.:format)                                                                  stores#update {:format=>"json"}
#                                              DELETE /api/stores/:id(.:format)                                                                  stores#destroy {:format=>"json"}
#                   batch_destroy_distributors DELETE /api/distributors/batch_destroy(.:format)                                                  distributors#batch_destroy {:format=>"json"}
#                      upload_logo_distributor PUT    /api/distributors/:id/upload_logo(.:format)                                                distributors#upload_logo {:format=>"json"}
#                                 distributors GET    /api/distributors(.:format)                                                                distributors#index {:format=>"json"}
#                                              POST   /api/distributors(.:format)                                                                distributors#create {:format=>"json"}
#                                  distributor GET    /api/distributors/:id(.:format)                                                            distributors#show {:format=>"json"}
#                                              PATCH  /api/distributors/:id(.:format)                                                            distributors#update {:format=>"json"}
#                                              PUT    /api/distributors/:id(.:format)                                                            distributors#update {:format=>"json"}
#                                              DELETE /api/distributors/:id(.:format)                                                            distributors#destroy {:format=>"json"}
#                      batch_destroy_customers DELETE /api/customers/batch_destroy(.:format)                                                     customers#batch_destroy {:format=>"json"}
#                                    customers GET    /api/customers(.:format)                                                                   customers#index {:format=>"json"}
#                                              POST   /api/customers(.:format)                                                                   customers#create {:format=>"json"}
#                                     customer GET    /api/customers/:id(.:format)                                                               customers#show {:format=>"json"}
#                                              PATCH  /api/customers/:id(.:format)                                                               customers#update {:format=>"json"}
#                                              PUT    /api/customers/:id(.:format)                                                               customers#update {:format=>"json"}
#                                              DELETE /api/customers/:id(.:format)                                                               customers#destroy {:format=>"json"}
#                                       states GET    /api/states(.:format)                                                                      states#index {:format=>"json"}
#                   batch_destroy_price_tables DELETE /api/price_tables/batch_destroy(.:format)                                                  price_tables#batch_destroy {:format=>"json"}
#               apply_price_table_distributors POST   /api/price_tables/:price_table_id/distributors/apply(.:format)                             distributors#apply_price_table {:format=>"json"}
#                     apply_price_table_stores POST   /api/price_tables/:price_table_id/stores/apply(.:format)                                   stores#apply_price_table {:format=>"json"}
#                  apply_price_table_customers POST   /api/price_tables/:price_table_id/customers/apply(.:format)                                customers#apply_price_table {:format=>"json"}
#                                 price_tables GET    /api/price_tables(.:format)                                                                price_tables#index {:format=>"json"}
#                                              POST   /api/price_tables(.:format)                                                                price_tables#create {:format=>"json"}
#                                  price_table GET    /api/price_tables/:id(.:format)                                                            price_tables#show {:format=>"json"}
#                                              PATCH  /api/price_tables/:id(.:format)                                                            price_tables#update {:format=>"json"}
#                                              PUT    /api/price_tables/:id(.:format)                                                            price_tables#update {:format=>"json"}
#                                              DELETE /api/price_tables/:id(.:format)                                                            price_tables#destroy {:format=>"json"}
#                          batch_destroy_items DELETE /api/items/batch_destroy(.:format)                                                         items#batch_destroy {:format=>"json"}
#                                        items GET    /api/items(.:format)                                                                       items#index {:format=>"json"}
#                                              POST   /api/items(.:format)                                                                       items#create {:format=>"json"}
#                                         item GET    /api/items/:id(.:format)                                                                   items#show {:format=>"json"}
#                                              PATCH  /api/items/:id(.:format)                                                                   items#update {:format=>"json"}
#                                              PUT    /api/items/:id(.:format)                                                                   items#update {:format=>"json"}
#                                              DELETE /api/items/:id(.:format)                                                                   items#destroy {:format=>"json"}
#                                  sellers_erp GET    /api/erp/sellers(.:format)                                                                 erp#sellers {:format=>"json"}
#                          category_levels_erp GET    /api/erp/category_levels(.:format)                                                         erp#category_levels {:format=>"json"}
#                       product_categories_erp GET    /api/erp/product_categories(.:format)                                                      erp#product_categories {:format=>"json"}
#                       payment_conditions_erp GET    /api/erp/payment_conditions(.:format)                                                      erp#payment_conditions {:format=>"json"}
#                                   job_status GET    /api/job_status/:id(.:format)                                                              job_status#show {:format=>"json"}
#                                              DELETE /api/job_status/:id(.:format)                                                              job_status#destroy {:format=>"json"}
#                       apply_total_order_item PUT    /api/order_items/:id/apply_total(.:format)                                                 order_items#apply_total {:format=>"json"}
#                                              PATCH  /api/order_items/:id/apply_total(.:format)                                                 order_items#apply_total {:format=>"json"}
#                 apply_value_order_loose_item PUT    /api/order_loose_items/:id/apply_value(.:format)                                           order_loose_items#apply_value {:format=>"json"}
#                                              PATCH  /api/order_loose_items/:id/apply_value(.:format)                                           order_loose_items#apply_value {:format=>"json"}
#                                   cut_tables GET    /api/cut_tables(.:format)                                                                  cut_tables#index {:format=>"json"}
#                                              POST   /api/cut_tables(.:format)                                                                  cut_tables#create {:format=>"json"}
#                                    cut_table GET    /api/cut_tables/:id(.:format)                                                              cut_tables#show {:format=>"json"}
#                                              PATCH  /api/cut_tables/:id(.:format)                                                              cut_tables#update {:format=>"json"}
#                                              PUT    /api/cut_tables/:id(.:format)                                                              cut_tables#update {:format=>"json"}
#                                              DELETE /api/cut_tables/:id(.:format)                                                              cut_tables#destroy {:format=>"json"}
#                                     printers GET    /api/printers(.:format)                                                                    printers#index {:format=>"json"}
#                                              POST   /api/printers(.:format)                                                                    printers#create {:format=>"json"}
#                                      printer GET    /api/printers/:id(.:format)                                                                printers#show {:format=>"json"}
#                                              PATCH  /api/printers/:id(.:format)                                                                printers#update {:format=>"json"}
#                                              PUT    /api/printers/:id(.:format)                                                                printers#update {:format=>"json"}
#                                              DELETE /api/printers/:id(.:format)                                                                printers#destroy {:format=>"json"}
#                           set_lines_question POST   /api/questions/:id/set_lines(.:format)                                                     questions#set_lines {:format=>"json"}
#                                    questions GET    /api/questions(.:format)                                                                   questions#index {:format=>"json"}
#                                              POST   /api/questions(.:format)                                                                   questions#create {:format=>"json"}
#                                     question GET    /api/questions/:id(.:format)                                                               questions#show {:format=>"json"}
#                                              PATCH  /api/questions/:id(.:format)                                                               questions#update {:format=>"json"}
#                                              PUT    /api/questions/:id(.:format)                                                               questions#update {:format=>"json"}
#                                              DELETE /api/questions/:id(.:format)                                                               questions#destroy {:format=>"json"}
# apply_commercial_classification_distributors POST   /api/commercial_classifications/:commercial_classification_id/distributors/apply(.:format) distributors#apply_commercial_classification {:format=>"json"}
#       apply_commercial_classification_stores POST   /api/commercial_classifications/:commercial_classification_id/stores/apply(.:format)       stores#apply_commercial_classification {:format=>"json"}
#                   commercial_classifications GET    /api/commercial_classifications(.:format)                                                  commercial_classifications#index {:format=>"json"}
#                                              POST   /api/commercial_classifications(.:format)                                                  commercial_classifications#create {:format=>"json"}
#                    commercial_classification GET    /api/commercial_classifications/:id(.:format)                                              commercial_classifications#show {:format=>"json"}
#                                              PATCH  /api/commercial_classifications/:id(.:format)                                              commercial_classifications#update {:format=>"json"}
#                                              PUT    /api/commercial_classifications/:id(.:format)                                              commercial_classifications#update {:format=>"json"}
#                                              DELETE /api/commercial_classifications/:id(.:format)                                              commercial_classifications#destroy {:format=>"json"}
#                           payment_conditions GET    /api/payment_conditions(.:format)                                                          payment_conditions#index {:format=>"json"}
#                                              POST   /api/payment_conditions(.:format)                                                          payment_conditions#create {:format=>"json"}
#                            payment_condition GET    /api/payment_conditions/:id(.:format)                                                      payment_conditions#show {:format=>"json"}
#                                              PATCH  /api/payment_conditions/:id(.:format)                                                      payment_conditions#update {:format=>"json"}
#                                              PUT    /api/payment_conditions/:id(.:format)                                                      payment_conditions#update {:format=>"json"}
#                                              DELETE /api/payment_conditions/:id(.:format)                                                      payment_conditions#destroy {:format=>"json"}
#             status_changed_webhooks_payments POST   /api/webhooks/payments/status_changed(.:format)                                            webhooks/payments#status_changed {:format=>"json"}
#                                  sidekiq_web        /sidekiq                                                                                   Sidekiq::Web
#

require 'sidekiq/web'
require 'sidekiq_status/web'

Rails.application.routes.draw do
  scope '/api', defaults: { format: 'json' } do  
    mount_devise_token_auth_for 'User', at: 'auth', skip: [:registrations, :passwords], controllers: {
      token_validations:  'overrides/token_validations',
      sessions: 'overrides/sessions'
    }
    mount_devise_token_auth_for 'Administrator', at: 'admin_auth', skip: [:sessions, :registrations, :passwords]
    mount_devise_token_auth_for 'Manager', at: 'manager_auth', skip: [:sessions, :registrations, :passwords]
    mount_devise_token_auth_for 'Seller', at: 'seller_auth', skip: [:sessions, :registrations, :passwords]
    mount_devise_token_auth_for 'Operator', at: 'operator_auth', skip: [:sessions, :registrations, :passwords]

    resources :users, only: [:index, :create, :update, :show, :destroy] do
      member do
        put :change_workplace
        patch :change_workplace
      end
    end

    resources :menus, only: [:index]
    resources :clients, only: [:index]
    resources :workplaces, only: [:index] do 
      collection do 
        get :current
      end
    end
    
    resources :document, only: [:index] do
      collection do
        put :update
        patch :update
      end
    end
      
    resources :families, only: [:index, :create, :update, :show]
    resources :lines, only: [:index, :create, :update, :show, :destroy] do
      member do
        get :components
        get :rules
        post :set_rules
        post :duplicate
        
        put   :inactivate
        patch :inactivate
        
        put   :activate
        patch :activate

        post :duplicate_rule
        post :import_rules
      end

      resources :components, only: [] do
        collection do
          get :select
        end
      end
    end
    
    resources :components, only: [:index, :create, :update, :show, :destroy] do
      collection do
        delete :batch_destroy
        get    :export
        post   :upload
      end

      member do
        patch :clone_kit
        put   :clone_kit
        post  :batch_insert
        put   :activate
        patch :activate
        delete :really_destroy
      end
    end

    resources :orders, only: [:index, :show, :destroy] do
      collection do
        post :generate
      end

      member do
        post  :pay
        put :send_to_erp
        put :cancel
        put   :change_billing_client
        patch :change_billing_client
        put   :update_special_discount
        patch :update_special_discount

        put   :production_status
        patch :production_status

        get :lines_label
        get :components_label
        get :cut_map
      end
    end

    resources :credits, only: [:index, :create] do
      collection do
        post :antecipate
      end
    end

    resources :production_orders, only: [:index] do
      collection do
        get :purchase_planning
      end
    end

    resources :categories, only: [:index, :create, :update, :destroy, :show]
    resources :specifications, only: [:index, :create, :update, :destroy, :show] do
      collection do
        get :for_filtering
      end
    end
    resources :base_components, only: [:index, :create, :update, :show, :destroy] do
      member do
        put   :activate
        patch :activate
        delete :really_destroy
      end
    end
    
    resources :estimates, only: [:index, :create, :update, :show] do
      member do
        put   :calculate
        patch :calculate
        post  :duplicate
        put   :apply_discount
        patch :apply_discount
      end

      resources :environments, only: [:index]
      resources :items, only: [:index], controller: 'estimate_items'
    end

    resources :environments, only: [:create, :update, :show, :destroy] do
      resources :locations, only: [:index]
    end

    resources :locations, only: [:create, :update, :show, :destroy] do
      member do
        delete :reset
        put   :apply_discount
        patch :apply_discount
      end
      resources :options, only: [:index]
    end
    
    resources :options, only: [:create, :update, :show, :destroy]

    resources :estimate_items, only: [:create, :destroy] do
      member do
        put   :apply_discount
        patch :apply_discount
      end
    end

    resources :stores, only: [:index, :create, :update, :show, :destroy] do
      collection do
        delete :batch_destroy
      end
      member do 
        put    :upload_logo
      end
    end

    resources :distributors, only: [:index, :create, :update, :show, :destroy] do
      collection do
        delete :batch_destroy
      end
      member do 
        put    :upload_logo
      end
    end

    resources :customers, only: [:index, :create, :update, :show, :destroy] do
      collection do
        delete :batch_destroy
      end
    end

    resources :states, only: [:index]

    resources :price_tables, only: [:index, :create, :update, :show, :destroy] do
      collection do
        delete :batch_destroy
      end

      resources :distributors, only: [] do
        collection do
          post :apply, to: 'distributors#apply_price_table'
        end
      end

      resources :stores, only: [] do
        collection do
          post :apply, to: 'stores#apply_price_table'
        end
      end

      resources :customers, only: [] do
        collection do
          post :apply, to: 'customers#apply_price_table'
        end
      end
    end

    resources :items, only: [:index, :create, :update, :show, :destroy] do
      collection do
        delete :batch_destroy
      end
    end

    resource :erp, controller: :erp, only: [] do
      collection do
        get :sellers
        get :category_levels
        get :product_categories
        get :payment_conditions
      end
    end

    resources :job_status, only: [:show, :destroy]

    resources :order_items, only:[] do
      member do
        put :apply_total
        patch :apply_total
      end
    end

    resources :order_loose_items, only:[] do
      member do
        put :apply_value
        patch :apply_value
      end
    end

    resources :cut_tables, only: [:index, :create, :update, :show, :destroy]

    resources :printers, only: [:index, :create, :update, :show, :destroy]

    resources :questions, only: [:index, :create, :update, :show, :destroy] do
      member do
        post :set_lines
      end
    end

    resources :commercial_classifications, only: [:index, :create, :update, :show, :destroy] do
      resources :distributors, only: [] do
        collection do
          post :apply, to: 'distributors#apply_commercial_classification'
        end
      end

      resources :stores, only: [] do
        collection do
          post :apply, to: 'stores#apply_commercial_classification'
        end
      end
    end

    resources :payment_conditions, only: [:index, :create, :update, :show, :destroy]

    namespace :webhooks do
      resources :payments, only: [] do
        collection do
          post :status_changed
        end
      end
    end
  end


  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
    ActiveSupport::SecurityUtils.secure_compare(::Digest::SHA256.hexdigest(username), ::Digest::SHA256.hexdigest('coyo')) &
    ActiveSupport::SecurityUtils.secure_compare(::Digest::SHA256.hexdigest(password), ::Digest::SHA256.hexdigest('C0y0@2014'))
  end if Rails.env.production?

  mount Sidekiq::Web, at: '/sidekiq'
end
