require 'byebug'
require 'box_packer'
require 'digest'
require 'active_support/core_ext/hash' 

class Fit
  AREA = 'area'
  WIDTH = 'width'
  HEIGHT = 'height'

  #SORT = nil
  SORT = AREA
  #SORT = WIDTH
  #SORT = HEIGHT

  REVERSE = false
  
  COMPONENTS = [200, 250, 300]
  COSTS      = {200 => 0.57330, 250 => 0.71663, 300 => 0.85995}

  CUT_TABLE_LIMIT = 1_000_00

  SLICE      = 6..6

  PRINT_RESULT = false

  def self.combine_array_elements(options, components_ids, el, line, combinations)
    line << el

    if options.size == 1
      options.product(components_ids).each do |other|
        combinations << (Array.new(line).push(other))
      end
    else
      [options.delete_at(0)].product(components_ids).each do |o|
        combine_array_elements(Array.new(options), components_ids, o, Array.new(line), combinations)
      end
    end
  end

  def self.calculate_min_dimension(orientation, lines_consumption_information, limit)
    @min_dimension = lines_consumption_information.min_by do |line_consumption|
      orientation == :height ? line_consumption[1] : line_consumption[0]
    end

    @min_dimension  = orientation == :height ? @min_dimension[1] : @min_dimension[0]
    @dimension      = ((limit + @min_dimension) / 2).round(2)
  end

  def self.optimized_pack(boxes, array, automatic_value, index)
    box = nil
    packing = 1
    lines_packed = array[automatic_value]

    limit = CUT_TABLE_LIMIT

    calculate_min_dimension(:height, array[automatic_value], limit)

    step = 1

    cache_key = Digest::MD5.hexdigest("#{automatic_value}_#{array[automatic_value].flatten.join('_')}")

    cached_array = @@cache[cache_key]

    if !cached_array.nil?
      boxes[automatic_value] = cached_array.dup
    else
      loop do
        lower_box = BoxPacker.container([automatic_value, @dimension - step], orientation: :width, packings_limit: packing, remove_exceeding: true)
        
        lines_consumption_information = array[automatic_value]

        lines_consumption_information.each do |line_consumption|
          lower_box.add_item [line_consumption[0], line_consumption[1]], label: "#{line_consumption[0]}x#{line_consumption[1]}_#{index}",
            allow_rotation: false, id: "#{line_consumption[0]}x#{line_consumption[1]}"
        end
        
        lower_box.pack!

        upper_box = BoxPacker.container([automatic_value, @dimension + step], orientation: :width, packings_limit: packing, remove_exceeding: true)

        lines_consumption_information.each do |line_consumption|
          upper_box.add_item [line_consumption[0], line_consumption[1]], label: "#{line_consumption[0]}x#{line_consumption[1]}_#{index}",
            allow_rotation: false, id: "#{line_consumption[0]}x#{line_consumption[1]}"
        end
        
        upper_box.pack!

        #print("\r############## Processing: COMPONENT: #{automatic_value} - #{@dimension} -- #{packing} ########")

        if @min_dimension == limit && !lower_box.packed_successfully && !upper_box.packed_successfully
          if (packing == lines_packed.count)
            box = nil
            break
          else
            packing = packing + 1
            calculate_min_dimension(:height, lines_consumption_information, CUT_TABLE_LIMIT)
          end
        else
          if (!lower_box.packed_successfully && !upper_box.packed_successfully)
            @min_dimension = @dimension
            @dimension = ((limit + @min_dimension) / 2).round(2)
          elsif (!lower_box.packed_successfully && upper_box.packed_successfully)
            box = upper_box
            break
          elsif (lower_box.packed_successfully && !upper_box.packed_successfully)
            box = lower_box
            break
          elsif (lower_box.packed_successfully && upper_box.packed_successfully)
            limit = @dimension
            @dimension = ((limit + @min_dimension) / 2).round(2)
          end
        end
      end

      if !box.nil? && box.packed_successfully
        boxes[automatic_value] = box
        @@cache[cache_key] = box
      end
    end
  end

  def self.optimized_pack_new(boxes, array, automatic_value, index)
    box = nil
    lines_packed = array[automatic_value]

    limit = CUT_TABLE_LIMIT

    calculate_min_dimension(:height, array[automatic_value], limit)

    step = 1

    cache_key = Digest::MD5.hexdigest("#{automatic_value}_#{array[automatic_value].flatten.join('_')}")

    cached_array = @@cache[cache_key]

    if !cached_array.nil?
      boxes[automatic_value] = cached_array.dup
    else
      box = BoxPacker.container([automatic_value, limit], orientation: :width, remove_exceeding: true)
      
      lines_consumption_information = array[automatic_value]

      lines_consumption_information.each do |line_consumption|
        box.add_item [line_consumption[0], line_consumption[1]], label: "#{line_consumption[0]}x#{line_consumption[1]}_#{index}",
          allow_rotation: false, id: "#{line_consumption[0]}x#{line_consumption[1]}"
      end
      
      box.pack!

      #print("\r############## Processing: COMPONENT: #{automatic_value} - #{@dimension} -- #{packing} ########")

      if !box.nil? && box.packed_successfully
        boxes[automatic_value] = box
        @@cache[cache_key] = box
      end
    end
  end

  def self.calculate_costs(k, box, options)
    #f = File.join(Dir.pwd, 'tmp', "m_#{k}")

    #box.draw!(f)

    tl  = box.used_width[0]
    ta  = box.used_height[0]
    ap  = k * box.used_height[0]
    ct  = ta * COSTS[k]
    as  = ap - options.inject(0) {|area, option| area = area + (option[0] * option[1])}
    ars = as.to_f / ap.to_f
    cs  = ct * ars

    ar  = {}
    co  = {}
    aur = {}
    cps = {}
    cf  = {}

    options.each do |option|
      ao          = (option[0].to_f * option[1].to_f)
      ar[option]  = ao / ap.to_f
      co[option]  = ct * ar[option]
      aur[option] = ao / (ap - as)
      cps[option] = cs * aur[option]
      cf[option]  = ct * ar[option] + cps[option]
    end

    if PRINT_RESULT
      p "############################### COMPONENTE #{k} #######################################"
      p "# TL #{tl} - TA #{ta} - AP #{ap} #"
      p "# CUSTO TOTAL #{ct} #"
      p "# AREAS RELATIVAS #{ar} #"
      p "# CUSTO OPÇÕES #{co} #"
      p "# ÁREA DA SOBRA #{as} - ÁREA RELATIVA SOBRA #{ars} #"
      p "# CUSTO DA SOBRA #{cs} #"
      p "# ÁREA ÚTIL RELATIVA #{aur} #"
      p "# CUSTO PROPORCIONAL SOBRA #{cps} #"
      p "# CUSTO FINAL #{cf} #"
    end

    return ct
  end

  def self.sort_options(options)
    _options = options.sort_by { |option| option[0] * option[1] } if Fit::SORT == Fit::AREA
    _options = options.sort_by { |option| option[1] } if Fit::SORT == Fit::HEIGHT
    _options = options.sort_by { |option| option[0] } if Fit::SORT == Fit::WIDTH

    return _options.reverse if Fit::REVERSE
    return _options
  end

  def self.simple_fit(_options, new_pack = false)
    @@cache = {}
    start = Time.now.to_f

    packs = {}

    COMPONENTS.each_with_index do |component_width, index|
      _pack = {}

      _pack[component_width] = _options

      if new_pack
        self.optimized_pack_new(packs, _pack, component_width, index)
      else
        self.optimized_pack(packs, _pack, component_width, index)
      end
    end

    cts = {}

    options = sort_options(_options.clone)

    packs.each do |k, box|
      ct = calculate_costs(k, box, options)

      cts[k] = ct
    end

    min_ct = cts.min_by do |k, value|
      #p "COMPONENTE: #{k} -- VALOR: #{value}"
      value
    end

    box = packs[min_ct[0]]

    box.draw!(File.join(Dir.pwd, 'tmp', "simple_fit_componente_#{min_ct[0]}"))

    p "# SELEÇÃO FINAL SIMPLE FIT #"
    p "# COMPONENTE: #{min_ct[0]} -- VALOR TOTAL: #{min_ct[1]} #"
    p "# TEMPO TOTAL: #{Time.now.to_f - start} #"

    return min_ct
  end

  def self.full_fit(_options, new_pack)
    Fit::SLICE.each do |slice|
      @@cache = {}
      start = Time.now.to_f

      @combinations = []

      original = _options.dup

      options = sort_options(_options.clone)

      total = 0

      options.each_slice(slice) do |lines_info|
        combinations = []

        _combinations = []

        options = Array.new(lines_info)

        if options.size == 1
          _combinations = COMPONENTS.map do |component|
            [[options[0], component]]
          end
        else
          [options.delete_at(0)].product(COMPONENTS).each do |o|
            combine_array_elements(Array.new(options), COMPONENTS, o, [], _combinations)
          end
        end

        combinations.concat(_combinations)

        cts = {}

        all_boxes = []

        #p "########################### COUNT #{combinations.size} ##############################"

        combinations.each_with_index do |c, index|
          #STDOUT.flush
          #print "\r################################ Montagem #{index + 1} ################################"

          grouped_data = c.group_by{|e| e[1]}

          array = {}

          grouped_data.keys.each do |key|
            array[key] = grouped_data[key].map do |value|
              value[0]
            end
          end

          boxes = {}

          array.keys.each do |automatic_value|
            if new_pack
              optimized_pack_new(packs, _pack, component_width, index)
            else
              optimized_pack(packs, _pack, component_width, index)
            end
          end

          pack_cts = {}

          if array.keys == boxes.keys
            boxes.sort_by{|k, box| k }.each do |k, box|
              #f = File.join(Dir.pwd, 'tmp', "m_#{index + 1}_#{k}")

              #box.draw!(f)

              options_in_box = original.select do |option|
                box.packing.find do |item|
                  item.id == "#{option[0]}x#{option[1]}"
                end
              end

              pack_cts[k] = calculate_costs(k, box, options_in_box)
            end

            cts[all_boxes.size] = pack_cts if !pack_cts.empty?
            all_boxes << boxes
          end
        end

        index = 1

        min_ct = cts.min_by do |k, values|
          v = values.map {|component, total| total}.reduce(:+)

          #p "MONTAGEM #{index};#{v}"
          index = index + 1

          v
        end

        pack_combination = {}

        all_boxes[min_ct[0]].sort_by{|k, box| k }.each do |k, box|
          pack_combination[k] = box.packing.map(&:id)

          box.draw!(File.join(Dir.pwd, 'tmp', "full_fit_montagem_#{min_ct[0] + 1}_#{k}"))
        end

        total = total + min_ct[1].map {|component, total| total}.reduce(:+)

        #p "############################### SELEÇÃO FINAL FULL FIT Grupos de #{slice} #######################################"
        #p "############################### MONTAGEM: #{min_ct[0] + 1} ##############################"
        #p "# VALORES: #{min_ct[1]} -- VALOR TOTAL: #{min_ct[1].map {|component, total| total}.reduce(:+)} #"
        #p "# COMBINAÇÃO: #{pack_combination} #"
      end
      p "############################### SELEÇÃO FINAL FULL FIT Grupos de #{slice} #######################################"
      p "# VALOR FINAL: #{total} #"
      p "# TEMPO TOTAL: #{Time.now.to_f - start} #"
    end
  end
end

OPTIONS_5   = [[160.0, 242.0], [162.0, 242.0], [76.0, 242.0], [72.0, 153.0], [96.0, 153.0]]

OPTIONS_6   = [[160.0, 242.0], [162.0, 242.0], [76.0, 242.0], [72.0, 153.0], [96.0, 153.0], [95, 159]]

OPTIONS_7   = [[160.0, 242.0], [162.0, 242.0], [76.0, 242.0], [72.0, 153.0], [96.0, 153.0], [95, 159], [74, 245]]

OPTIONS_8    = [[112, 257], [122, 257], [140, 257], [130, 257], [150, 257], [118, 257], [107, 257], [97, 257]]

OPTIONS_9    = [[112, 257], [122, 257], [140, 257], [130, 257], [150, 257], [118, 257], [107, 257], [97, 257], [99, 255]]

OPTIONS_10    = [[112, 257], [122, 257], [140, 257], [130, 257], [150, 257], [118, 257], [107, 257], [97, 257], [99, 255], [98, 226]]

OPTIONS_60    = [[180, 90], [240, 110], [280, 125]] * 20

OPTIONS_92   = [[245.0, 270.0], [245.0, 270.0], [245.0, 270.0], [245.0, 270.0], [245.0, 270.0], [245.0, 270.0], [245.0, 270.0], [245.0, 270.0], [245.0, 270.0], [245.0, 270.0], [245.0, 270.0], [245.0, 270.0], [245.0, 270.0], [245.0, 270.0], [150.0, 270.0], [150.0, 270.0], [150.0, 270.0], [150.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [210.0, 270.0], [210.0, 270.0], [210.0, 270.0], [210.0, 270.0], [210.0, 270.0], [210.0, 270.0], [210.0, 270.0], [210.0, 270.0], [210.0, 270.0], [210.0, 270.0], [210.0, 270.0], [210.0, 270.0], [210.0, 270.0], [210.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [180.0, 270.0]]

OPTIONS_93    = [[245.0, 270.0], [245.0, 270.0], [245.0, 270.0], [245.0, 270.0], [245.0, 270.0], [245.0, 270.0], [245.0, 270.0], [245.0, 270.0], [245.0, 270.0], [245.0, 270.0], [245.0, 270.0], [245.0, 270.0], [245.0, 270.0], [245.0, 270.0], [150.0, 270.0], [150.0, 270.0], [150.0, 270.0], [150.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [215.0, 270.0], [210.0, 270.0], [210.0, 270.0], [210.0, 270.0], [210.0, 270.0], [210.0, 270.0], [210.0, 270.0], [210.0, 270.0], [210.0, 270.0], [210.0, 270.0], [210.0, 270.0], [210.0, 270.0], [210.0, 270.0], [210.0, 270.0], [210.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [190.0, 270.0], [180.0, 270.0], [180.0, 270.0]]

OPTIONS_154   = [[120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 174.0], [120.0, 220.0], [120.0, 220.0], [120.0, 220.0], [120.0, 220.0], [120.0, 220.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 220.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 327.0], [120.0, 220.0], [120.0, 220.0], [120.0, 220.0], [120.0, 220.0], [120.0, 220.0], [120.0, 220.0], [120.0, 220.0], [120.0, 220.0], [120.0, 220.0], [120.0, 220.0], [120.0, 220.0], [120.0, 220.0], [120.0, 220.0], [120.0, 220.0], [120.0, 220.0], [120.0, 220.0], [120.0, 220.0], [120.0, 220.0], [120.0, 220.0], [120.0, 220.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0], [120.0, 208.0]]

OPTIONS_326 = [[134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0], [134.0, 230.0]]

#[OPTIONS_5, OPTIONS_6, OPTIONS_7, OPTIONS_8, OPTIONS_9, OPTIONS_10, OPTIONS_60, OPTIONS_92, OPTIONS_93].each do |options|
[OPTIONS_154].each do |options|
  p "############################### PROCESSANDO #{options.size} ITENS ########################"
  Fit.simple_fit(options, false)
  Fit.full_fit(options, false)
  p ""
  p ""
  Fit.simple_fit(options, true)
  Fit.full_fit(options, true)
end