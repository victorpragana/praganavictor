# LUSS

Versão do Ruby: 2.3.0

Versão do Rails: 4.2.6

Versão do NodeJS: 4.8.0

## Installation
 - ```bundle install```
 - ```rake db:create```
 - ```rake db:migrate```
 - ```rake db:seed```
 - ```cd client```
 - ```npm install -g bower```
 - ```bower install```
 - ```npm install -g grunt-cli```

## Subindo o projeto
 - ```rails s puma```
 - ```cd client```
 - ```grunt serve```
 - Acessar pelo endereço http://localhost:9000
 - Usuário padrão: admin
 - Senha padrão: coyo@2014

## Executando o print server: ```bundle exec thin start -R faye.ru -p 9191```

## Impressão local configurada para development:  config.local_label_printer = false

## Iugu doc: http://support.iugu.com/hc/pt-br/articles/206858953-Como-identificar-o-erro-da-tentativa-de-pagamento-com-cart%C3%A3o-de-cr%C3%A9dito-falha-
## Iugu cartões de teste: https://support.iugu.com/hc/pt-br/articles/212456346-Usar-cart%C3%B5es-de-teste-em-modo-de-teste

## Para executar o teste de mesa: ```ruby fit.rb``` (certifique-se que a gem do box_packer está instalada globalmente, via ```gem install```)