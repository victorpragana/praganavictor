SELECT value, COUNT(*)
FROM values
WHERE comparison_value is not null
GROUP BY value, comparison_value
HAVING count(*) > 1;

update components_values set value_id = 898 where value_id = 926;
update components_values set value_id = 907 where value_id = 922;
update components_values set value_id = 899 where value_id = 920;
update components_values set value_id = 911 where value_id = 923;

update base_component_values set value_id = 898 where value_id = 926;
update base_component_values set value_id = 907 where value_id = 922;
update base_component_values set value_id = 899 where value_id = 920;
update base_component_values set value_id = 911 where value_id = 923;

delete from values where id in (926, 922, 920, 923);