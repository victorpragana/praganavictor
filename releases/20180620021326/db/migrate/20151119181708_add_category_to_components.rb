class AddCategoryToComponents < ActiveRecord::Migration
  def change
    remove_column :components, :category
    
    add_reference :components, :category, index: true, foreign_key: true
  end
end
