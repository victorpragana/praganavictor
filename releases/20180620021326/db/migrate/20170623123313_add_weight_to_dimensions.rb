class AddWeightToDimensions < ActiveRecord::Migration
  def change
    add_column :dimensions, :weight, :integer, null: false, default: 0
    add_index :dimensions, :weight
  end
end
