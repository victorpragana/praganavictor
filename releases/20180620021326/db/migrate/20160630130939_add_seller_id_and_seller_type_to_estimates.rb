class AddSellerIdAndSellerTypeToEstimates < ActiveRecord::Migration
  def change
    add_column :estimates, :seller_id, :integer
    add_index :estimates, :seller_id
    add_column :estimates, :seller_type, :string
    add_index :estimates, :seller_type
  end
end
