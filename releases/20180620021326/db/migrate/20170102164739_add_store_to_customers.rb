class AddStoreToCustomers < ActiveRecord::Migration
  def up
    add_reference :customers, :store, index: true, foreign_key: true

    Customer.reset_column_information

    Customer.with_deleted.find_each do |customer|
      result = ActiveRecord::Base.connection.execute("SELECT store_id FROM customers_stores WHERE customer_id = #{customer.id}")

      if result.first
        store_ids = result.first.values

        customer.update(store_id: store_ids.shift)

        if store_ids.count > 1
          store_ids.each do |store_id|
            new_customer = customer.deep_clone

            new_customer.store_id = store_id

            new_customer.save!
          end
        end
      end
    end
  end

  def down
    remove_reference :customers, :store, index: true, foreign_key: true
  end
end
