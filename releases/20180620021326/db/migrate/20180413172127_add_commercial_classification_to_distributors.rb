class AddCommercialClassificationToDistributors < ActiveRecord::Migration
  def change
    add_reference :distributors, :commercial_classification, index: true, foreign_key: true
  end
end
