class AddTotalToOrderItemComponents < ActiveRecord::Migration
  def change
    add_column :order_item_components, :total, :decimal, precision: 10, scale: 2, default: 0
  end
end
