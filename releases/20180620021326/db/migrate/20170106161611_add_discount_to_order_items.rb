class AddDiscountToOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :discount, :decimal, precision: 5, scale: 2, default: 0
  end
end
