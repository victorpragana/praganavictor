class AddMaximumDiscountToUsers < ActiveRecord::Migration
  def change
    add_column :users, :maximum_discount, :decimal, precision: 5, scale: 2, default: 0
  end
end
