class AddCostToRules < ActiveRecord::Migration
  def change
    add_column :rules, :cost, :string, :default => "", :null => false
  end
end
