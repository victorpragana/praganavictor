class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :label, null: false
      t.string :value, null: false
      t.integer :type, null: false
      t.json :options

      t.timestamps null: false
    end
  end
end
