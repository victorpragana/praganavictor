class AddErpIdToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :erp_id, :integer
  end
end
