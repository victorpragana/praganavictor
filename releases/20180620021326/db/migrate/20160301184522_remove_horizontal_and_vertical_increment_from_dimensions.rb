class RemoveHorizontalAndVerticalIncrementFromDimensions < ActiveRecord::Migration
  def change
    remove_column :dimensions, :horizontal_increment
    remove_column :dimensions, :vertical_increment
  end
end
