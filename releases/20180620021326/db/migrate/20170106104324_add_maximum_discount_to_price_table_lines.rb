class AddMaximumDiscountToPriceTableLines < ActiveRecord::Migration
  def change
    add_column :price_table_lines, :maximum_discount, :decimal, precision: 5, scale: 2, default: 0
  end
end
