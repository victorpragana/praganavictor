class AddQuantityConsumptionToRules < ActiveRecord::Migration
  def change
    add_column :rules, :quantity_consumption, :string
  end
end
