class AddSalePriceToBaseComponents < ActiveRecord::Migration
  def change
    add_column :base_components, :sale_price, :decimal, :precision => 8, :scale => 3, :null => false, :default => 0
  end
end
