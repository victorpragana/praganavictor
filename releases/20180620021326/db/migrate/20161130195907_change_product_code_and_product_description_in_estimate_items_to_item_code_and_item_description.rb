class ChangeProductCodeAndProductDescriptionInEstimateItemsToItemCodeAndItemDescription < ActiveRecord::Migration
  def change
    rename_column :estimate_items, :product_code, :item_code
    rename_column :estimate_items, :product_description, :item_description
  end
end
