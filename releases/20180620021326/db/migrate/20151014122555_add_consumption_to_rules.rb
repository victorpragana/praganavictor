class AddConsumptionToRules < ActiveRecord::Migration
  def change
    add_column :rules, :consumption, :string, :default => "", :null => false
  end
end
