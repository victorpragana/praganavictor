class RemoveUserFromThirdParties < ActiveRecord::Migration
  def change
    remove_reference :third_parties, :user, index: true, foreign_key: true
  end
end
