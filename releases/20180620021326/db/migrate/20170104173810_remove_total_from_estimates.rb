class RemoveTotalFromEstimates < ActiveRecord::Migration
  def change
    remove_column :estimates, :total, :decimal
  end
end
