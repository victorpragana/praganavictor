class ChangeRuleColumnTypes < ActiveRecord::Migration
  def up
    change_column :rules, :width, "int4range USING null"
    change_column :rules, :height, "int4range USING null"
  end

  def down
    change_column :rules, :width, "numrange USING null"
    change_column :rules, :height, "numrange USING null"
  end
end
