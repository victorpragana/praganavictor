class ChangeProductIdInPriceTableItems < ActiveRecord::Migration
  def change
    rename_column :price_table_items, :product_id, :item_id
  end
end
