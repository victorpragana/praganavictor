class CreateBaseComponentValues < ActiveRecord::Migration
  def change
    create_table :base_component_values do |t|
      t.belongs_to :base_component, index: true, foreign_key: true
      t.belongs_to :value, index: true, foreign_key: true
      t.integer :base_component_value_id

      t.timestamps null: false
    end
  end
end
