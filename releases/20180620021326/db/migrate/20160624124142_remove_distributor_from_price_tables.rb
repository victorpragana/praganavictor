class RemoveDistributorFromPriceTables < ActiveRecord::Migration
  def change
    remove_column :price_tables, :distributor_id, :integer
  end
end
