class ChangeTotalConsumptionUnitValueWidthConsumptionHeightConsumptionAndQUantityConsumptionInOrderItemComponents < ActiveRecord::Migration
  def change
    change_column :order_item_components, :total_consumption, :decimal, precision: 10, scale: 5, null: false
    change_column :order_item_components, :unit_value, :decimal, precision: 10, scale: 5, null: false
    change_column :order_item_components, :width_consumption, :decimal, precision: 10, scale: 5
    change_column :order_item_components, :height_consumption, :decimal, precision: 10, scale: 5
    change_column :order_item_components, :quantity_consumption, :decimal, precision: 10, scale: 5
  end
end
