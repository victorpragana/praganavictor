class ChangeDateInMovements < ActiveRecord::Migration
  def change
    change_column :movements, :date, :datetime
  end
end
