class CreateFamilies < ActiveRecord::Migration
  def change
    create_table :families do |t|
      t.string :name, null: false
      t.string :color, limit: 7, null: false

      t.timestamps null: false
    end
  end
end
