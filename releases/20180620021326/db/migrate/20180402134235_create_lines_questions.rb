class CreateLinesQuestions < ActiveRecord::Migration
  def change
    create_table :lines_questions, id: false do |t|
      t.references :line, :question
    end

    add_index :lines_questions, [:line_id, :question_id],
      name: "lines_questions_index",
      unique: true
  end
end
