class AddUserToThirdParty < ActiveRecord::Migration
  def change
    change_table :third_parties do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.actable
    end
  end
end
