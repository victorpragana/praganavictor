class RenameConsumptionToTotalConsumptionInOrderItems < ActiveRecord::Migration
  def change
    rename_column :order_items, :consumption, :total_consumption
  end
end
