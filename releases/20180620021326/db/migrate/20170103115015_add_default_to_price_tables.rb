class AddDefaultToPriceTables < ActiveRecord::Migration
  def change
    add_column :price_tables, :default, :boolean, default: false
  end
end
