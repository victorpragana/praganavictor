class AddTaxAndOriginalValueToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :tax, :decimal, precision: 5, scale: 2, default: 0
    add_column :payments, :original_value, :decimal, precision: 8, scale: 2, default: 0

    Payment.reset_column_information

    Payment.find_each do |payment|
      payment.update_attribute(:original_value, payment.value)
    end

    change_column :payments, :original_value, :decimal, precision: 8, scale: 2, null: false
  end
end
