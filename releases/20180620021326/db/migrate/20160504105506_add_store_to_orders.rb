class AddStoreToOrders < ActiveRecord::Migration
  def change
    add_reference :orders, :store, index: true, foreign_key: true

    Order.reset_column_information

    Order.find_each do |order|
      order.store_id = order.estimate.store_id
      order.save!
    end
  end
end
