class CreateEstimateProducts < ActiveRecord::Migration
  def change
    create_table :estimate_products do |t|
      t.belongs_to :estimate, index: true, foreign_key: true
      t.string :product_code
      t.string :product_description, null: false
      t.integer :unit, null: false
      t.decimal :quantity, null: false, precision: 8, scale: 2
      t.decimal :value, null: false, precision: 8, scale: 2

      t.timestamps null: false
    end
  end
end
