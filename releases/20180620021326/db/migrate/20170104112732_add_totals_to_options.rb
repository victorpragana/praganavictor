class AddTotalsToOptions < ActiveRecord::Migration
  def change
    add_column :options, :factory_total, :decimal, precision: 10, scale: 2, default: 0
    add_column :options, :distributor_total, :decimal, precision: 10, scale: 2, default: 0
    add_column :options, :store_total, :decimal, precision: 10, scale: 2, default: 0
    add_column :options, :customer_total, :decimal, precision: 10, scale: 2, default: 0
  end
end
