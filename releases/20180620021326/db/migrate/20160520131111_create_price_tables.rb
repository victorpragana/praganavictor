class CreatePriceTables < ActiveRecord::Migration
  def change
    create_table :price_tables do |t|
      t.belongs_to :line, index: true, foreign_key: true
      t.belongs_to :distributor, index: true, foreign_key: true
      t.string :price

      t.timestamps null: false
    end
  end
end
