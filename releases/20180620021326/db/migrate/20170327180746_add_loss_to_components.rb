class AddLossToComponents < ActiveRecord::Migration
  def change
    add_column :components, :loss, :decimal, precision: 10, scale: 5
  end
end
