class CreateComponentsComponents < ActiveRecord::Migration
  def change
    create_table :components_components, :id => false do |t|
      t.references :parent
      t.references :child
    end

    add_index :components_components, [:parent_id, :child_id],
      name: "components_components_index",
      unique: true
  end
end
