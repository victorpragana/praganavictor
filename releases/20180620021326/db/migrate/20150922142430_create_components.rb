class CreateComponents < ActiveRecord::Migration
  def change
    create_table :components do |t|
      t.string :name, :null => false, :limit => 255
      t.integer :category, :null => false

      t.timestamps null: false
    end
  end
end
