class AddAllowRotationFlagToBaseComponents < ActiveRecord::Migration
  def change
    add_column :base_components, :allow_rotation, :boolean, default: false
  end
end
