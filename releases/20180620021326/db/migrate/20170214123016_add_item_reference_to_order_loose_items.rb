class AddItemReferenceToOrderLooseItems < ActiveRecord::Migration
  def change
    add_reference :order_loose_items, :item, index: true, foreign_key: true

    OrderLooseItem.reset_column_information

    OrderLooseItem.find_each do |oli|
      oli.item_id = Item.with_deleted.find_by_code(oli.item_code).id

      oli.save!
    end
  end
end
