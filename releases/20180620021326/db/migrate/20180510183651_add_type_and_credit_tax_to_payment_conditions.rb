class AddTypeAndCreditTaxToPaymentConditions < ActiveRecord::Migration
  def change
    add_column :payment_conditions, :type, :integer, null: false, default: 0
    add_column :payment_conditions, :credit_tax, :decimal, precision: 5, scale: 2, default: 0
  end
end
