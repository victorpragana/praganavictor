class AddSelectedWorkplaceToUsers < ActiveRecord::Migration
  def change
    add_reference :users, :selected_workplace, references: :user_workplaces, index: true
    add_foreign_key :users, :user_workplaces, column: :selected_workplace_id

    User.reset_column_information

    User.find_each do |user|
      workplace = UserWorkplace.find_by!(user_id: user.id)

      say "Updating user #{user.username} to workplace ##{workplace.id}"

      user.update_attribute(:selected_workplace_id, workplace.id)
    end

    remove_column :users, :workplace_id, :integer
    remove_column :users, :workplace_type, :string
  end
end
