class AddCutTableToComponents < ActiveRecord::Migration
  def change
    add_reference :components, :cut_table, index: true, foreign_key: true
  end
end
