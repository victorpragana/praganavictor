class AddProcessingMessageToEstimates < ActiveRecord::Migration
  def change
    add_column :estimates, :processing_message, :text, array: true, default: []
  end
end
