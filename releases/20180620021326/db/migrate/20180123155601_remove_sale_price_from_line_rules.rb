class RemoveSalePriceFromLineRules < ActiveRecord::Migration
  def change
    remove_column :line_rules, :sale_price, :string, null: false
  end
end
