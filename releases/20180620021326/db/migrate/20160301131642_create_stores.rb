class CreateStores < ActiveRecord::Migration
  def change
    create_table :stores do |t|
      t.string :name, :null => false
      t.datetime :deleted_at, :index => true

      t.timestamps null: false
    end
  end
end
