class AddTypeToUsers < ActiveRecord::Migration
  def change
    add_column :users, :type, :string

    User.reset_column_information
  end
end
