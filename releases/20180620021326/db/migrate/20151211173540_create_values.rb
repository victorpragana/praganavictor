class CreateValues < ActiveRecord::Migration
  def change
    create_table :values do |t|
      t.string :value, :null => false
      t.belongs_to :specification, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
