class AddTotalsToOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :factory_total, :decimal, precision: 10, scale: 2, default: 0
    add_column :order_items, :distributor_total, :decimal, precision: 10, scale: 2, default: 0
    add_column :order_items, :store_total, :decimal, precision: 10, scale: 2, default: 0
    add_column :order_items, :customer_total, :decimal, precision: 10, scale: 2, default: 0
  end
end
