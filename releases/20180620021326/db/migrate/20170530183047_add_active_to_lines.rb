class AddActiveToLines < ActiveRecord::Migration
  def change
    add_column :lines, :active, :boolean, default: true
    add_index :lines, :active
  end
end
