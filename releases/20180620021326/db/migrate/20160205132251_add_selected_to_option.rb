class AddSelectedToOption < ActiveRecord::Migration
  def change
    add_column :options, :selected, :boolean, :null => false, :default => false
  end
end
