class AddSubdomainToThirdParties < ActiveRecord::Migration
  def up
    add_column :third_parties, :subdomain, :string

    ThirdParty.where("actable_type != 'Customer'").each do |third_party|
      third_party.update_column(:subdomain, third_party.name.parameterize)
    end
  end

  def down
    remove_column :third_parties, :subdomain
  end
end
