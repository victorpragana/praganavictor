class AddWidthHeightQuantityToLocations < ActiveRecord::Migration
  def change
    add_column :locations, :width, :integer
    add_column :locations, :height, :integer
    add_column :locations, :quantity, :integer
  end
end
