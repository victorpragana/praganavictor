class AddAttachmentPackingToOptionComponents < ActiveRecord::Migration
  def self.up
    change_table :option_components do |t|
      t.attachment :packing
    end
  end

  def self.down
    remove_attachment :option_components, :packing
  end
end
