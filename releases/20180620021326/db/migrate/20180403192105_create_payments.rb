class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.decimal :value, precision: 8, scale: 2, null: false
      t.string  :credit_card_iugu_token
      t.integer :status, null: false, default: 0
      t.integer :payment_method, null: false, default: 0
      t.integer :installment_number, null: false, default: 1
      t.string  :bank_slip_url, limit: 255
      t.timestamps
    end
  end
end