class CreatePackingConfigurations < ActiveRecord::Migration
  def change
    create_table :packing_configurations do |t|
      t.references :packable, polymorphic: true, index: true
      t.integer :items, array: true, index: true
      t.decimal :automatic_dimension, precision: 10, scale: 5, null: false
      t.decimal :component_dimension, precision: 10, scale: 5, null: false
      t.string :component_name, null: false
      t.integer :unit, null: false
      t.attachment :packing

      t.timestamps null: false
    end
  end
end
