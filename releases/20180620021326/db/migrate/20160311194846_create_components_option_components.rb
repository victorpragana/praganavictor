class CreateComponentsOptionComponents < ActiveRecord::Migration
  def change
    create_table :components_option_components, :id => false do |t|
      t.references :component, :option_component
    end

    add_index :components_option_components, [:component_id, :option_component_id],
      name: "components_option_components_index",
      unique: true
  end
end
