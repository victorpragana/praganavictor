class AddReasonToMovements < ActiveRecord::Migration
  def change
    add_column :movements, :reason, :integer, null: false, default: 0
  end
end
