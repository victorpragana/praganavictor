class CreateStoresUsers < ActiveRecord::Migration
  def change
    create_table :stores_users, :id => false do |t|
      t.references :store, :user
    end

    add_index :stores_users, [:store_id, :user_id], name: "stores_users_index", unique: true

    Administrator.find_each do |administrator|
      administrator.stores = Store.all
    end

    Operator.find_each do |operator|
      operator.stores = Store.all
    end
  end
end
