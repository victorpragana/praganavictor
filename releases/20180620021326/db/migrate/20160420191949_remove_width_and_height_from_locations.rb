class RemoveWidthAndHeightFromLocations < ActiveRecord::Migration
  def change
    remove_column :locations, :width, :integer
    remove_column :locations, :height, :integer
  end
end
