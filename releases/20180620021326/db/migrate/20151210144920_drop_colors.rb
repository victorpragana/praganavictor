class DropColors < ActiveRecord::Migration
  def change
    drop_table :colors_components if ActiveRecord::Base.connection.table_exists? 'colors_components'
    drop_table :colors_rules if ActiveRecord::Base.connection.table_exists? 'colors_rules'
    drop_table :colors if ActiveRecord::Base.connection.table_exists? 'colors'
  end
end
