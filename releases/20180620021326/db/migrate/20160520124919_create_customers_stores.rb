class CreateCustomersStores < ActiveRecord::Migration
  def change
    create_table :customers_stores, :id => false do |t|
      t.references :customer, :store
    end

    add_index :customers_stores, [:customer_id, :store_id],
      name: "customers_stores_index",
      unique: true
  end
end
