class AddAutomaticToSpecifications < ActiveRecord::Migration
  def change
    add_column :specifications, :automatic, :boolean, :default => false
  end
end
