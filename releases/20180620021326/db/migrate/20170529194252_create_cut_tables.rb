class CreateCutTables < ActiveRecord::Migration
  def change
    create_table :cut_tables do |t|
      t.string :name, null: false
      t.integer :size, null: false

      t.timestamps null: false
    end
  end
end
