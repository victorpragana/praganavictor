class AddIuguInvoiceIdToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :iugu_invoice_id, :string
  end
end
