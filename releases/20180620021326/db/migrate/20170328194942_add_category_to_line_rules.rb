class AddCategoryToLineRules < ActiveRecord::Migration
  def change
    add_reference :line_rules, :category, index: true, foreign_key: true
  end
end
