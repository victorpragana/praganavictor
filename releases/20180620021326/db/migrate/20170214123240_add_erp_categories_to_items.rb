class AddErpCategoriesToItems < ActiveRecord::Migration
  def change
    add_column :items, :erp_categories, :json
  end
end
