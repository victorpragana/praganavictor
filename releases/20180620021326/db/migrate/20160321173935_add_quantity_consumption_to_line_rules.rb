class AddQuantityConsumptionToLineRules < ActiveRecord::Migration
  def change
    add_column :line_rules, :quantity_consumption, :string
  end
end
