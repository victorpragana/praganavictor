class AddQuestionsToOptions < ActiveRecord::Migration
  def change
    add_column :options, :questions, :json
  end
end
