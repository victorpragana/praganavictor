class CreateOptionComponents < ActiveRecord::Migration
  def change
    create_table :option_components do |t|
      t.belongs_to :option, index: true, foreign_key: true
      t.decimal :total_consumption, :precision => 10, :scale => 3, :null => false
      t.decimal :unit_value, :precision => 10, :scale => 2, :null => false
      t.string :component_name, :null => false, :limit => 255
      t.integer :unit, :null => false
      t.text :component_characteristics
      t.string :category_name, :null => false, :limit => 255
      t.decimal :width_consumption, :precision => 10, :scale => 3
      t.decimal :height_consumption, :precision => 10, :scale => 3
      t.decimal :quantity_consumption, :precision => 10, :scale => 3

      t.timestamps null: false
    end
  end
end
