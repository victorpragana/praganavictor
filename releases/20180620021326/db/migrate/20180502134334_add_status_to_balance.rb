class AddStatusToBalance < ActiveRecord::Migration
  def change
    add_column :balances, :status, :integer, null: false, default: 1
  end
end
