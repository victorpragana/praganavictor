class AddAttachmentLogoToThirdParties < ActiveRecord::Migration
  def self.up
    change_table :third_parties do |t|
      t.attachment :logo
    end
  end

  def self.down
    remove_attachment :third_parties, :logo
  end
end
