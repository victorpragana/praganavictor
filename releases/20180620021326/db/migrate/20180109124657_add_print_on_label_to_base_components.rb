class AddPrintOnLabelToBaseComponents < ActiveRecord::Migration
  def change
    add_column :base_components, :print_on_label, :boolean, default: false
  end
end
