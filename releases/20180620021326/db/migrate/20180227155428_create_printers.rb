class CreatePrinters < ActiveRecord::Migration
  def change
    create_table :printers do |t|
      t.string :name, null: false
      t.string :real_name, null: false
      t.string :server_name, null: false
      t.string :server_id, null: false
      t.boolean :default

      t.timestamps null: false
    end
  end
end
