class AddCustomerToEstimates < ActiveRecord::Migration
  def change
    add_reference :estimates, :customer, index: true, foreign_key: true
  end
end
