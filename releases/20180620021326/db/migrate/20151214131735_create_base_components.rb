class CreateBaseComponents < ActiveRecord::Migration
  def change
    create_table :base_components do |t|
      t.string :name, :null => false, :limit => 255
      t.decimal :cost, :null => false, :precision => 8, :scale => 3, :default => 0
      t.belongs_to :category, index: true, foreign_key: true
      t.text :characteristics
      t.string :code, :limit => 20
      t.integer :unit, null: false, default: BaseComponent.units[:un]

      t.timestamps null: false
    end
  end
end
