class AddTypeToOrderLooseItems < ActiveRecord::Migration
  def change
    add_column :order_loose_items, :type, :integer, default: 0
  end
end
