class CreateDimensions < ActiveRecord::Migration
  def change
    create_table :dimensions do |t|
      t.numrange :width, :null => false
      t.numrange :height, :null => false
      t.decimal :vertical_increment, :null => false, :precision => 5, :scale => 2
      t.decimal :horizontal_increment, :null => false, :precision => 5, :scale => 2
      t.belongs_to :line, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
