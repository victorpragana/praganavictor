class DropQuantityConsumptionsTable < ActiveRecord::Migration
  def change
    drop_table :quantity_consumptions
  end
end
