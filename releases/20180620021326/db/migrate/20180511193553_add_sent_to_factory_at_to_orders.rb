class AddSentToFactoryAtToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :sent_to_factory_at, :datetime
  end
end
