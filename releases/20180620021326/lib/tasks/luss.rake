namespace :luss do
  CODE_INDEX                = 0
  CATEGORY_INDEX            = 1
  UNIT_INDEX                = 2
  NAME_INDEX                = 3
  PRIMARY_DESCRIPTION_INDEX = 4
  FAMILIES_INDEX            = 5
  AUTOMATIC_ATTRIBUTE_INDEX = 6
  ATTRIBUTES_START_INDEX    = 7

  desc "Import the XLSX seed"
  task import_seed: :environment do
    xlsx = Roo::Excelx.new(Rails.root.join('lib', 'assets', 'seed.xlsx').to_s)

    @not_imported = []

    create_families(xlsx)
    create_categories(xlsx)
    create_attributes_and_values(xlsx)
    create_automatic_attribute_and_values(xlsx)

    specifications = []

    xlsx.row(1).each_with_index do |specification, index|
      next if index < ATTRIBUTES_START_INDEX

      specifications << {:index => index, :name => specification}
    end

    Kernel.puts "Creating the base components and components"
    xlsx.each_row_streaming({pad_cells: true, offset: 1}) do |row|
      component_name = "#{normalize_string(row[NAME_INDEX].value)}"
      component_name = "#{component_name} #{normalize_string(row[PRIMARY_DESCRIPTION_INDEX].value)}" unless row[PRIMARY_DESCRIPTION_INDEX].blank? || row[PRIMARY_DESCRIPTION_INDEX].value.blank?

      Kernel.print "\rBase component: #{component_name}..."
      category = Category.find_by_name(normalize_string(row[CATEGORY_INDEX].value))

      base_component = BaseComponent.find_or_create_by!(category_id: category.id, unit: BaseComponent.units[row[UNIT_INDEX].value], 
        name: component_name) do |bc|
        bc.families = row[FAMILIES_INDEX].value.split(",").map do |family|
          Family.find_by_name(normalize_string(family))
        end
      end

      used_values = []

      specifications.each_with_index do |specification, index|
        next if row[specification[:index]].blank? || row[specification[:index]].value.blank?

        value = Specification.find_by_name(normalize_string(specification[:name])).values.find_by_value(normalize_string(row[specification[:index]].value))

        base_component_value = BaseComponentValue.find_or_create_by!({base_component_id: base_component.id, value_id: value.id, base_component_value_id: used_values.last})

        used_values << base_component_value.id
      end

      unless row[AUTOMATIC_ATTRIBUTE_INDEX].blank? || row[AUTOMATIC_ATTRIBUTE_INDEX].value.blank?
        automatic_value_attribute_name = xlsx.row(1)[AUTOMATIC_ATTRIBUTE_INDEX]

        value = Specification.find_by_name(normalize_string(automatic_value_attribute_name)).values.find_by_comparison_value(row[AUTOMATIC_ATTRIBUTE_INDEX].value * conversion_factor(row[UNIT_INDEX].value))

        base_component_value = BaseComponentValue.find_or_create_by!({base_component_id: base_component.id, value_id: value.id, base_component_value_id: used_values.last})

        used_values << base_component_value.id
      end

      used_values.each do |bcv_id|
        value = BaseComponentValue.find(bcv_id).value

        component_name = "#{component_name} #{value.value}"
      end

      component_code = normalize_string(row[CODE_INDEX].value) unless row[CODE_INDEX].blank? || row[CODE_INDEX].value.blank?

      Kernel.print "\rComponent: #{component_name}..."
      component = Component.new({name: component_name, category_id: category.id, unit: Component.units[row[UNIT_INDEX].value], base_component_id: base_component.id, code: component_code})
      component.value_ids = BaseComponentValue.where(id: used_values).pluck(:value_id)
      component.families = row[FAMILIES_INDEX].value.split(",").map do |family|
        Family.find_by_name(normalize_string(family))
      end

      unless component.save
        @not_imported << "Component: #{component_name} - #{component.errors.full_messages.join(", ")}"
      end
    end

    Kernel.puts "Done!"

    Kernel.puts "---------------- NOT IMPORTED ------------------"
    @not_imported.each do |ni|
      Kernel.puts ni
    end
  end

  private
    def create_families(xlsx)
      Kernel.puts "Creating the families"

      xlsx.each(families: "Famílias") do |data|
        data[:families].split(",").each do |family|
          next if "#{normalize_string(family)}" == "Famílias"

          Kernel.print "\rFamily: #{normalize_string(family)}..."

          family = Family.find_or_create_by({name: normalize_string(family)}) do |family|
            family.color = ("#" + "%06x" % (rand * 0xffffff))
          end

          unless family.persisted?
            @not_imported << "Family: #{family.name} - #{family.errors.full_messages.join(", ")}"
          end
        end
      end

      Kernel.puts "Done!"
    end

    def create_categories(xlsx)
      Kernel.puts "Creating the categories"

      xlsx.each(category: "Categoria") do |data|
        next if "#{normalize_string(data[:category])}" == "Categoria"

        Kernel.print "\rCategory: #{normalize_string(data[:category])}..."

        unless Category.create(name: normalize_string(data[:category]))
          @not_imported << "Category: #{normalize_string(data[:category])}"
        end
      end

      Kernel.puts "Done!"
    end

    def create_attributes_and_values(xlsx)
      Kernel.puts "Creating the attributes and values"
      specifications = xlsx.row(1)[ATTRIBUTES_START_INDEX..-1]

      specifications.each do |specification_name|
        Kernel.puts "Attribute: #{normalize_string(specification_name)}"
        specification = Specification.find_or_create_by(name: normalize_string(specification_name))

        xlsx.each(specification: specification_name).each_with_index do |data, index|
          next if index == 0 || data[:specification].blank?

          Kernel.print "\rValue: #{normalize_string(data[:specification])}..."

          Value.find_or_create_by!({value: normalize_string(data[:specification]), specification_id: specification.id})
        end
        Kernel.puts "Done!"
      end
    end

    def create_automatic_attribute_and_values(xlsx)
      Kernel.puts "Creating the automatic attribute and values"
      specification_name = xlsx.row(1)[AUTOMATIC_ATTRIBUTE_INDEX]
      unit_name = xlsx.row(1)[UNIT_INDEX]

      Kernel.puts "Attribute: #{normalize_string(specification_name)}"
      specification = Specification.find_or_create_by!(name: normalize_string(specification_name)) do |spec|
        spec.automatic = true
      end

      xlsx.each(specification: specification_name, unit: unit_name).each_with_index do |data, index|
        next if index == 0 || data[:specification].blank?

        Kernel.print "\rValue: #{data[:specification]} #{data[:unit]}..."

        Value.find_or_create_by!({value: "#{data[:specification]} #{data[:unit]}", specification_id: specification.id}) do |value|
          value.comparison_value = data[:specification] * conversion_factor(data[:unit])
        end
      end

      Kernel.puts "Done!"
    end

    def normalize_string(data)
      data.to_s.strip.capitalize
    end

    def conversion_factor(unit)
      case unit
      when "cm"
        10
      when "m"
        1000
      else
        1
      end
    end
end
