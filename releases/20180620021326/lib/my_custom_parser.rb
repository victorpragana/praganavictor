class MyCustomParser < Her::Middleware::FirstLevelParseJSON
  def on_complete(env)
    case env[:method]
      when :put, :post
        json = MultiJson.load(env[:body], symbolize_keys: true)
        env[:body] = {
          data: {},
          errors: {},
          metadata: {}
        }
      else
        env[:body] = case env[:status]
        when 204
          parse('{}')
        else
          parse(env[:body])
        end
    end
  end
end