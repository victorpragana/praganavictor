class VpsaTokenAuthentication < Faraday::Middleware
  def call(env)
    case env[:method]
      when :get
        query_string = URI.decode_www_form(env[:url].query || '')
        query_string << ["classes", "CLIENTE"]
        query_string << ["token", RequestStore.store[:vpsa_token]]
        env[:url].query = URI.encode_www_form(query_string)
      else
        query_string = URI.decode_www_form(env[:url].query || '')
        query_string << ["token", RequestStore.store[:vpsa_token]]
        env[:url].query = URI.encode_www_form(query_string)
    end

    @app.call(env)
  end

end